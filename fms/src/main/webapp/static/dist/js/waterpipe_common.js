

/**
 * 공통 데이터피커 사용 함수
 * 
*/
var setDatepicker = function() {
	$(".datepicker" ).datepicker({
	    dateFormat: 'yy-mm-dd',
	    prevText: '이전 달',
	    nextText: '다음 달',
	    monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
	    monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
	    dayNames: ['일','월','화','수','목','금','토'],
	    dayNamesShort: ['일','월','화','수','목','금','토'],
	    dayNamesMin: ['일','월','화','수','목','금','토'],
	    showMonthAfterYear: true,
	    yearSuffix: '년'
	});
}


/**
 * String prototype getBytes
*/
String.prototype.getBytes = function() {
    var contents = this;
    var str_character;
    var int_char_count;
    var int_contents_length;
 
    int_char_count = 0;
    int_contents_length = contents.length;
 
    for (k = 0; k < int_contents_length; k++) {
        str_character = contents.charAt(k);
        if (escape(str_character).length > 4)
            int_char_count += 2;
        else
            int_char_count++;
    }
    return int_char_count;
}

String.prototype.cutStringBytes = function( byteLength ) {
    var contents = this;
    var str_character;
    var int_char_count;
    var int_contents_length;
     
    var returnValue = "";
     
    int_char_count = 0;
    int_contents_length = contents.length;
     
    for (k = 0; k < int_contents_length; k++) {
        str_character = contents.charAt(k);
        if (escape(str_character).length > 4)
            int_char_count += 2;
        else
            int_char_count++;
         
        if ( int_char_count > byteLength ) {
            break;
        }
        returnValue += str_character;
    }
    return returnValue;
}

$(document).ready(function(){
	$('.not-used').each(function(k,el){
		$(el).attr("disabled", "disabled");
	});
});

/* byte를 이용한 입력크기 제한 */
$(document).on('keyup',"input.limit-bytes", function(e){
	/*if(e.which.keyCode == 40){
   		alert();
	}*/
	if( $(this).val().getBytes() > parseInt($(this).attr("maxbytes")) ) {
		$(this).val( $(this).val().cutStringBytes(parseInt($(this).attr("maxbytes"))) );
		alert("최대 " + parseInt($(this).attr("maxbytes")) + "바이트까지 입력 가능합니다.");
	}
});

var openPopup = function() {
	$(".popup-link").click(function(e) {
		e.preventDefault();
		var centerPos = parseInt( $(screen).get(0).width ) / 2;
		var windowCenterPos = 858 / 2;
		var leftPos = centerPos - windowCenterPos;
		var _win = "";
		if($(this).attr("tab-flag")=="tab") {
			var href="";
			if(this.href==""||this.href==null) {
				href=$(this).attr("href");
			} else {
				href=this.href;
			}
			_win = window.open(href, this.title, "left="+leftPos+",top=100,"+$(this).attr("popup-data") + ", menubar=no, location=no, resizable=no, toolbar=no, status=no");
			_win.focus();
		} else {
			try{ opener.closeChild(); }catch(e){}
			_win = window.open($(this).attr("href"), "child", "left="+leftPos+",top=100,"+$(this).attr("popup-data") + ", menubar=no, location=no, resizable=no, toolbar=no, status=no");
			_win.focus();
			try{ opener._winChild = _win; }catch(e){}
		}
	});	
}
var waterpipePrint = function() {			
	$("#printBtn").click(function () {
		$('#qrImage').attr('style', 'text-align:center;');
		var msg = window.print();
		$('#qrImage').attr('style', 'display:none; text-align:center;');
	});
}