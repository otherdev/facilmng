var uisSource;

// 시설물 이동 함수
function moveMap(uisLayerNm, ogrFid){
	
	// 기존 레이어 지우기
	map.removeLayer(uisLayer);
	if (ogrFid =='') {
		 return "이동할 좌표가 없습니다.";
	}
	
	//var url = gisWfsUrl + "request=GetFeature&outputFormat=application/json&typename="+ uisLayerNm + "&CQL_FILTER=OGR_FID="+ ogrFid;
	
	var url = gisWfsUrl + "request=GetFeature&outputFormat=application/json&typename="+ uisLayerNm + "&featureID="+ ogrFid;
	uisSource = new ol.source.Vector({
		format: new ol.format.GeoJSON(),
		url: function(extent, projection) {
			//console.log("1 : " + extent.join(','));
			return url;
		}/* ,
			loader: function(extent, resolution, projection) {
				var url =  'http://183.98.24.4:9200/geoserver/yuni/wfs?service=WFS&version=1.1.0&request=GetFeature&'+
			'typename=yuni%3AWTL_BDST_AS&outputFormat=application/json&format_options=callback:loadFeatures&'+
			'srsname=EPSG:5187&bbox=' + extent.join(',') + ',EPSG:5187';
				
				$.ajax({url: url, dataType: 'jsonp', jsonp: true});
			},*/ 
		, strategy: ol.loadingstrategy.bbox
	});
	// 시설물 이동 레이어
	var uisLayer = new ol.layer.Vector({
		title : 'uisLayer',
		name : 'uisLayer',
		//visible : false,
		source: uisSource,
		style : new ol.style.Style({
			stroke : new ol.style.Stroke({
				color : '#FF0000',
				width : 2
			}),
			image : new ol.style.Circle({
				radius : 7,
				fill : new ol.style.Fill({
					color : '#FF0000'
				})
			})
		})
	});

	map.addLayer(uisLayer);
	
	// 지도 이동 및 줌
	uisSource.once('change', function(evt){
		var source = evt.target;
		if(source.getState() === 'ready'){ //'undefined', 'loading', 'ready' or 'error'
			var extent = uisSource.getExtent();
			if (extent[0] == 'Infinity') {
				alert("좌표가 없습니다.");
			} else {
				var center = ol.extent.getCenter(extent);
				map.getView().setCenter(center);
				map.getView().setZoom(15);
			}
		}
	});
}
