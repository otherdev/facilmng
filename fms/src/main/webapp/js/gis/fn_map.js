/**
 * OL맵객체 초기화 
 */

var mainLayer = new ol.layer.Image({
	title : 'BML_GADM_AS',
	name: 'BML_GADM_AS',
	source : new ol.source.ImageWMS({
		ratio : 1,
		url : gisWmsUrl,
		params : {
			'FORMAT' : 'image/png',
			'VERSION' : '1.1.1',
			STYLES : '',
			LAYERS : 'BML_GADM_AS',
		}
	})
});

var searchLayer = new ol.layer.Vector({
	title : 'search',
	name: 'search',
	source: new ol.source.Vector(),
	style: new ol.style.Style({
		fill: new ol.style.Fill({
			color: 'rgba(247, 0, 21, 0.8)',
		}),
		stroke: new ol.style.Stroke({
			color: '#ff0000',
			width: 2,
			lineDash: [4,4]
			
			}),
			image: new ol.style.Circle({
			radius: 7,
			fill: new ol.style.Fill({
				color: '#ff0000'
			})
		})
	})
});

var source = new ol.source.Vector();

var vector = new ol.layer.Vector({
	title : 'measure',
	name: 'measure',
	source: source,
	style: new ol.style.Style({
	fill: new ol.style.Fill({
		color: 'rgba(255, 255, 255, 0.2)'
	}),
	stroke: new ol.style.Stroke({
		color: '#ffcc33',
		width: 2
	}),
	image: new ol.style.Circle({
		radius: 7,
		fill: new ol.style.Fill({
		color: '#ffcc33'
		})
	})
	})
});

//속성정보를 위한 변수
var container = document.getElementById('map_layer');

// 속성정보 (map 선언 할 때 선언)
var overlay = new ol.Overlay(/** @type {olx.OverlayOptions} */ ({
	element: container,
	autoPan: true,
	autoPanAnimation: {
	duration: 250
	}
}));

var scaleLineControl = new ol.control.ScaleLine();
map = new ol.Map({
	renderer: 'dom',
	target: 'map',
	controls: ol.control.defaults({
		attributionOptions: /** @type {olx.control.AttributionOptions} */ ({
			collapsible: false
		})
	}).extend([
		//scaleLineControl
	]),
	interactions : ol.interaction.defaults({doubleClickZoom :false}),
	layers: [
	         emap_layer,
	         mainLayer,
	         searchLayer,
		WTL_FIRE_PS_Vector,WTL_FIRE_PS_Layer,WTL_FIRE_PS_Edit,WTL_FIRE_PS_Layer_SA118,WTL_FIRE_PS_Layer_SA119
		,WTL_FLOW_PS_Vector,WTL_FLOW_PS_Layer,WTL_FLOW_PS_Edit
		,WTL_GAIN_PS_Vector,WTL_GAIN_PS_Layer,WTL_GAIN_PS_Edit
		,WTL_HEAD_PS_Vector,WTL_HEAD_PS_Layer,WTL_HEAD_PS_Edit
		//,WTL_LEAK_PS_Vector,WTL_LEAK_PS_Layer,WTL_LEAK_PS_Edit
		,WTL_MANH_PS_Vector,WTL_MANH_PS_Layer,WTL_MANH_PS_Edit
		,WTL_META_PS_Vector,WTL_META_PS_Layer,WTL_META_PS_Edit
		,WTL_PIPE_LM_Vector,WTL_PIPE_LM_Layer,WTL_PIPE_LM_Edit
		//,WTL_PIPE_PS_Vector,WTL_PIPE_PS_Layer,WTL_PIPE_PS_Edit	
		,WTL_PRES_PS_Vector,WTL_PRES_PS_Layer,WTL_PRES_PS_Edit
		,WTL_PRGA_PS_Vector,WTL_PRGA_PS_Layer,WTL_PRGA_PS_Edit
		,WTL_PURI_AS_Vector,WTL_PURI_AS_Layer,WTL_PURI_AS_Edit
		,WTL_RSRV_PS_Vector,WTL_RSRV_PS_Layer,WTL_RSRV_PS_Edit
		,WTL_SERV_PS_Vector,WTL_SERV_PS_Layer,WTL_SERV_PS_Edit
		,WTL_SPLY_LS_Vector,WTL_SPLY_LS_Layer,WTL_SPLY_LS_Edit
		,WTL_VALV_PS_Vector,WTL_VALV_PS_Layer,WTL_VALV_PS_Edit,WTL_VALV_PS_Layer_SA200,WTL_VALV_PS_Layer_SA201,WTL_VALV_PS_Layer_SA202,WTL_VALV_PS_Layer_SA203,WTL_VALV_PS_Layer_SA204,WTL_VALV_PS_Layer_SA205,WTL_VALV_PS_Layer_SA206,WTL_VALV_PS_Layer_SA910
		//,WTL_WPMP_AS_Vector,WTL_WPMP_AS_Layer,WTL_WPMP_AS_Edit
		],
	
	view: new ol.View({
		projection : "EPSG:5187",
		//		extent: bounds,
		//		resolutions : _resolutions,
		//		center: new ol.geom.Point([139.93, 47.71]).transform('EPSG:4326', 'EPSG:5187').getCoordinates(),
        center: ol.proj.fromLonLat([126.41, 36.82]),
		maxZoom : 19,
		minZoom : 6,
		zoom: 13,
	}),
	overlays: [overlay],
	logo:true
});

map.addLayer(vector);	// measure Layer
map.getView().fit(bounds, map.getSize());


