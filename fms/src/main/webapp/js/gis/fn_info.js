var flag = 0;
//선택 피처 선언
var selectObject = new ol.interaction.Select({
	condition: ol.events.condition.click
	//, style : overlayStyle2
});
//오버한 객체 표시
var highlighter = new ol.interaction.Select({
	condition: ol.events.condition.pointerMove
});


// 정보보기 클릭 이벤트
function viewInfo(){

	// 체크된 레이어가 있는지 확인
	var chked_val = "";
	$(":checkbox[name='chkList']:checked").each(function(idx, val){
		var _val = val.value;
		chked_val += ","+_val;
	});
	if (chked_val=="") {
		 alert("선택된 레이어가 없습니다.");
	} else { 

		$("#map_layer").toggle();
		// 위치이동 레이어 지우기
		map.removeLayer(uisLayer);
		
		selectObject.getFeatures().clear();
		
		// 전역변수의 레이어를 - 나중에 
		// 선택되어있는  ImageLayer와 같은 Vector 레이어(***_***_***_V)를  visible : false -> ture
		// Edit레이어 지우기
		map.getLayers().forEach(function (layer) {
			var layerNm = layer.get('name');
			if ( layerNm != undefined ) {
				if (layerNm.indexOf("_Edit") != -1) {
					layer.setVisible(false);
				}
			}
		});
		// 마지막 선택된 레이어 정보만 보여주기!! - 나중에
		
		if(!$('.top_menu_icon03 li.li07 a').hasClass('select_active')) {	
			$('.top_menu_icon03 li.li07 a').addClass('select_active');
			flag = 0;
		} else {
			$('.top_menu_icon03 li.li07 a.select_active').removeClass('select_active');
			flag = 1;
		}
		// 커서 변경
		switch(flag) {
		case 0 :
			// on
			flag = 1;
			map.removeInteraction(selectObject);
			map.removeInteraction(highlighter);
			map.addInteraction(selectObject);
			map.addInteraction(highlighter);
			chkLayer(chked_val);
			break;
		case 1 :
			// off
			flag = 0;
			chkLayer(chked_val);
			map.removeInteraction(selectObject);
			map.removeInteraction(highlighter);
			break;
		}
	}
}

//체크 한 백터 레이어 활성화
function chkLayer(chked_val) {
	
	if(chked_val!="")chked_val = chked_val.substring(1);
	
	var chkLayerList = chked_val.split(",");
	// 체크 한 레이어 setVisible true
	if(chked_val != "") {
		for(var i=0; chkLayerList.length>i; i++) {
			var name =chkLayerList[i] + '_Vec';
			map.getLayers().forEach(function (layer) {
				if (layer.get('name') == name) {
				  var visibility = layer.getVisible();
				  if (visibility == false) {
					layer.setVisible(true);
				  }
				  else if (visibility == true) {
					  return 0;
				  }
				}
			});
		}
	}
	
	// 체크 한 레이어 없을 시 백터 레이어 제거
	else {
		map.getLayers().forEach(function (layer) {
			var layerNm = layer.get('name');
			if ( layerNm != undefined ) {
				map.removeInteraction(selectObject);
				if (layerNm.indexOf("_Vec") != -1) {
					layer.setVisible(false);
				}
			}
//			map.removeInteraction(selectObject);
		})
	}
};

//피처 클릭 했을 때 속성정보 표출
selectObject.on("select", function (evt) {
	
	if(flag == 1) {
		var feature = map.forEachFeatureAtPixel(evt.mapBrowserEvent.pixel, function(feature, layer) {
			return feature;
		});
		if (feature) {
			// 레이어 ID
			var layerId = feature.getId().split('.');
			//console.log(feature);
	//		console.log(evt);
			// 좌표
			var coordinate = evt.mapBrowserEvent.coordinate;
			var layerId = layerId[0];
			var data = feature.S;
			var idenData = "";
			
	//		console.log("coordinate ::: " + coordinate);
				
			$.ajax({
				url : webPath + "/map/getCodeNm.do",
				type : 'POST',
				dataType : 'JSON',
				data : {
					ftrCde : data.FTR_CDE,
					mngCde : data.MNG_CDE,
					mofCde : data.MOF_CDE,
					pipCde : data.PIP_CDE,
					hjdCde : data.HJD_CDE
				},
				success : function(res) {
					console.log(res);
					var codeData = res.data;
					var mngNm = "";
					var ftrNm = "";
					var mofNm = "";
					var pipNm = "";
					var hjdNm = "";
					
					if(codeData.mngNm.length != 0) {
						mngNm = codeData.mngNm[0].cdNm;
					}
					else {
						mngNm = "데이터 없음";
					}
					
					if(codeData.ftrNm.length != 0) {
						ftrNm = codeData.ftrNm[0].cdNm;
					}
					else {
						ftrNm = "데이터 없음";
					}
					
					if(codeData.mofNm.length != 0) {
						mofNm = codeData.mofNm[0].cdNm;
					}
					else {
						mofNm = "데이터 없음";
					}
					
					if(codeData.pipNm.length != 0) {
						pipNm = codeData.pipNm[0].cdNm;
					}
					else {
						pipNm = "데이터 없음";
					}
					
					if(codeData.hjdNm.length != 0) {
						hjdNm = codeData.hjdNm[0].hjdNam;
					}
					else {
						hjdNm = "데이터 없음";
					}
					
					if(!data.HOM_NUM) {
						data.HOM_NUM = "데이터 없음";
					}
					
					// 설치일자
					var secDate = data.IST_YMD;
					var istDate;
	
					if (secDate != undefined ) {
						var year = secDate.substr(0,4);
						var month = secDate.substr(4,2);
						var day = secDate.substr(6,2);
						istDate = year + "-" + month  + "-" + day;
					} else {
						istDate = "0000-00-00";
					}
					
					// 준공일자
					var secDate2 = data.FNS_YMD
					var fnsDate;
	
					if (secDate2 != undefined ) {
						var year = secDate2.substr(0,4);
						var month = secDate2.substr(4,2);
						var day = secDate2.substr(6,2);
						fnsDate = year + "-" + month  + "-" + day;
					} else {
						fnsDate = "0000-00-00";
					}
					
					/*idenData = '<table cellpadding="0" cellspacing="0">' +
					'<colgroup>' +
					'<col width="116px"></col>' +
					'<col width="146px"></col>' +					
					'</colgroup>';*/
					
					
					switch(layerId) {
					
					// 소방시설
					case 'WTL_FIRE_PS':
						idenData += 
						'<tr>' +
						'<th>지형지물부호</th>' +
						'<td class="white"><p>'+ ftrNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>관리번호</th>' +
						'<td class="white"><p>'+ data.FTR_IDN + '</td>' +
						'</tr>' +
	//					'<tr>' +
						'<th>행정읍면동코드</th>' +
						'<td class="white"><p>'+ hjdNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>관리기관</th>' +
						'<td class="white"><p>'+ mngNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>설치일자</th>' +
						'<td class="white"><p>'+ istDate + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>수용가번호</th>' +
						'<td class="white"><p>'+ data.HOM_NUM + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>형식</th>' +
						'<td class="white"><p>'+ mofNm + 
						'<div style="display:none" id="divSubmit" layerType="pipe" ftrCde="' + data.FTR_CDE +'", ftrIdn="'+ data.FTR_IDN +'"></div>' +
						'</td>' +
						'</tr>' ;
						break;
						
					// 유량계	
					case 'WTL_FLOW_PS': 
						idenData += 
						'<tr>' +
						'<th>지형지물부호</th>' +
						'<td class="white"><p>'+ ftrNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>관리번호</th>' +
						'<td class="white"><p>'+ data.FTR_IDN + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>행정읍면동코드</th>' +
						'<td class="white"><p>'+ hjdNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>도엽번호</th>' +
						'<td class="white"><p>'+ data.SHT_NUM + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>관리기관</th>' +
						'<td class="white"><p>'+ mngNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>설치일자</th>' +
						'<td class="white"><p>'+ istDate + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>유량계종류</th>' +
						'<td class="white"><p>'+ data.GAG_CDE + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>형식</th>' +
						'<td class="white"><p>'+ mofNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>관로지형지물부호</th>' +
						'<td class="white"><p>'+ pipNm + 
						'<div style="display:none" id="divSubmit" layerType="flow" ftrCde="' + data.FTR_CDE +'", ftrIdn="'+ data.FTR_IDN +'"></div>' +
						'</td>' +
						'</tr>' ;
						break;
						
					// 취수장	
					case 'WTL_GAIN_PS': 
						idenData += 
						'<tr>' +
						'<th>지형지물부호</th>' +
						'<td class="white"><p>'+ ftrNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>관리번호</th>' +
						'<td class="white"><p>'+ data.FTR_IDN + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>행정읍면동코드</th>' +
						'<td class="white"><p>'+ hjdNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>도엽번호</th>' +
						'<td class="white"><p>'+ data.SHT_NUM + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>관리기관</th>' +
						'<td class="white"><p>'+ mngNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>준공일자</th>' +
						'<td class="white"><p>'+ fnsDate + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>수원구분</th>' +
						'<td class="white"><p>'+ data.WSR_CDE + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>수계명</th>' +
						'<td class="white"><p>'+ data.WSS_NAM + 
						'<div style="display:none" id="divSubmit" layerType="gain" ftrCde="' + data.FTR_CDE +'", ftrIdn="'+ data.FTR_IDN +'"></div>' +
						'</td>' +
						'</tr>' ;
						break;
						
					// 수원지	
					case 'WTL_HEAD_PS': 
						idenData += 
						'<tr>' +
						'<th>지형지물부호</th>' +
						'<td class="white"><p>'+ ftrNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>관리번호</th>' +
						'<td class="white"><p>'+ data.FTR_IDN + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>행정읍면동코드</th>' +
						'<td class="white"><p>'+ hjdNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>도엽번호</th>' +
						'<td class="white"><p>'+ data.SHT_NUM + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>관리기관</th>' +
						'<td class="white"><p>'+ mngNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>준공일자</th>' +
						'<td class="white"><p>'+ fnsDate + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>수원지명</th>' +
						'<td class="white"><p>'+ data.HEA_NAM + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>수원구분</th>' +
						'<td class="white"><p>'+ data.WSR_CDE + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>유효저수량</th>' +
						'<td class="white"><p>'+ data.RSV_VOL + 
						'<div style="display:none" id="divSubmit" layerType="head" ftrCde="' + data.FTR_CDE +'", ftrIdn="'+ data.FTR_IDN +'"></div>' +
						'</td>' +
						'</tr>' ;
						break;
					
					// 누수지점 및 복구내역
					case 'WTL_LEAK_PS': 
						idenData += 
						'<tr>' +
						'<th>지형지물부호</th>' +
						'<td class="white"><p>'+ ftrNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>관리번호</th>' +
						'<td class="white"><p>'+ data.FTR_IDN + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>행정읍면동코드</th>' +
						'<td class="white"><p>'+ hjdNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>도엽번호</th>' +
						'<td class="white"><p>'+ data.SHT_NUM + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>관리기관</th>' +
						'<td class="white"><p>'+ mngNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>설치일자</th>' +
						'<td class="white"><p>'+ istDate + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>규격</th>' +
						'<td class="white"><p>'+ data.DPG_STD + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>맨홀종류</th>' +
						'<td class="white"><p>'+ data.SOM_CDE + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>맨홀형태</th>' +
						'<td class="white"><p>'+ data.MHS_CDE + 
						'<div style="display:none" id="divSubmit" layerType="leak" ftrCde="' + data.FTR_CDE +'", ftrIdn="'+ data.FTR_IDN +'"></div>' +
						'</td>' +
						'</tr>' ;
						break;
					
					// 상수맨홀
					case 'WTL_MANH_PS': 
						idenData += 
						'<tr>' +
						'<th>지형지물부호</th>' +
						'<td class="white"><p>'+ ftrNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>관리번호</th>' +
						'<td class="white"><p>'+ data.FTR_IDN + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>행정읍면동코드</th>' +
						'<td class="white"><p>'+ hjdNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>도엽번호</th>' +
						'<td class="white"><p>'+ data.SHT_NUM + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>민원접수번호</th>' +
						'<td class="white"><p>'+ data.RCV_NUM + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>누수일자</th>' +
						'<td class="white"><p>'+ data.LEK_YMD + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>누수위치설명</th>' +
						'<td class="white"><p>'+ data.LEK_LOC + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>관로지형지물</th>' +
						'<td class="white"><p>'+ pipNm + 
						'<div style="display:none" id="divSubmit" layerType="manh" ftrCde="' + data.FTR_CDE +'", ftrIdn="'+ data.FTR_IDN +'"></div>' +
						'</td>' +
						'</tr>' ;
						break;
						
					// 급수전계량기
					case 'WTL_META_PS': 
						idenData += 
						'<tr>' +
						'<th>지형지물부호</th>' +
						'<td class="white"><p>'+ ftrNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>관리번호</th>' +
						'<td class="white"><p>'+ data.FTR_IDN + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>행정읍면동코드</th>' +
						'<td class="white"><p>'+ hjdNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>법정읍면동코드</th>' +
						'<td class="white"><p>'+ data.BJD_CDE + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>도엽번호</th>' +
						'<td class="white"><p>'+ data.SHT_NUM + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>설치일자</th>' +
						'<td class="white"><p>'+ istDate + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>수용가번호</th>' +
						'<td class="white"><p>'+ data.HOM_NUM + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>수용가성명</th>' +
						'<td class="white"><p>'+ data.HOM_NAM + 
						'<div style="display:none" id="divSubmit" layerType="meta" ftrCde="' + data.FTR_CDE +'", ftrIdn="'+ data.FTR_IDN +'"></div>' +
						'</td>' +
						'</tr>' ;
						break;
						
					// 상수관로
					case 'WTL_PIPE_LM': 
						idenData += 
							'<tr>' +
							'<th>지형지물부호</th>' +
							'<td >'+ ftrNm + '</td>' +
							'</tr>' +
							'<tr>' +
							'<th>관리번호</td>' +
							'<td >'+ data.FTR_IDN + '</td>' +
							'</tr>' +
							'<tr>' +
							'<th>행정읍면동코드</th>' +
							'<td >'+ hjdNm + '</td>' +
							'</tr>' +
							'<tr>' +
							'<th>도엽번호</th>' +
							'<td >'+ data.SHT_NUM + '</td>' +
							'</tr>' +
							'<tr>' +
							'<th>설치일자</th>' +
							'<td >'+ istDate + '</td>' +
							'</tr>' +
							'<tr>' +
							'<th>연장</th>' +
							'<td >'+ data.BYC_LEN + '</td>' +
							'</tr>' +
							'<tr>' +
							'<th>관라벨</th>' +
							'<td >'+ data.PIP_LBL + '</td>' +
							'</tr>' +
							'<tr>' +
							'<th>개통상태</th>' +
							'<td>'+ data.SOO_CDE + 
							'<div style="display:none" id="divSubmit" layerType="pipe" ftrCde="' + data.FTR_CDE +'", ftrIdn="'+ data.FTR_IDN +'"></div>' +
							'</td>' +
							'</tr>' ;
						break;
						
					// 상수관로심도
					case 'WTL_PIPE_PS': 
						idenData += 
						'<tr>' +
						'<th>지형지물부호</th>' +
						'<td class="white"><p>'+ ftrNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>심도</th>' +
						'<td class="white"><p>'+ data.PIP_DEP + 
						'<div style="display:none" id="divSubmit" layerType="valv" ftrCde="' + data.FTR_CDE +'", ftrIdn="'+ data.FTR_IDN +'"></div>' +
						'</td>' +
						'</tr>' ;
						break;
						
					// 가압펌프장
					case 'WTL_PRES_PS': 
						idenData += 
						'<tr>' +
						'<th>지형지물부호</th>' +
						'<td class="white"><p>'+ ftrNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>관리번호</th>' +
						'<td class="white"><p>'+ data.FTR_IDN + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>행정읍면동코드</th>' +
						'<td class="white"><p>'+ hjdNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>도엽번호</th>' +
						'<td class="white"><p>'+ data.SHT_NUM + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>관리기관</th>' +
						'<td class="white"><p>'+ mngNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>준공일자</th>' +
						'<td class="white"><p>'+ fnsDate + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>가압장명</th>' +
						'<td class="white"><p>'+ data.PRS_NAM + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>관리방법</th>' +
						'<td class="white"><p>'+ data.SAG_CDE + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>가압구역</th>' +
						'<td class="white"><p>'+ data.PRS_ARE +
						'<div style="display:none" id="divSubmit" layerType="pres" ftrCde="' + data.FTR_CDE +'", ftrIdn="'+ data.FTR_IDN +'"></div>' +
						'</td>' + 
						'</tr>';
						break;
						
					// 수압계
					case 'WTL_PRGA_PS':
						idenData += 
						'<tr>' +
						'<th>지형지물부호</th>' +
						'<td class="white"><p>'+ ftrNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>관리번호</th>' +
						'<td class="white"><p>'+ data.FTR_IDN + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>행정읍면동코드</th>' +
						'<td class="white"><p>'+ hjdNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>도엽번호</th>' +
						'<td class="white"><p>'+ data.SHT_NUM + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>관리기관</th>' +
						'<td class="white"><p>'+ mngNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>설치일자</th>' +
						'<td class="white"><p>'+ istDate + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>수압계종류</th>' +
						'<td class="white"><p>'+ data.PGA_CDE + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>형식</th>' +
						'<td class="white"><p>'+ mofNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>관로지형지물부호</th>' +
						'<td class="white"><p>'+ data.PIP_CDE + 
						'<div style="display:none" id="divSubmit" layerType="prga" ftrCde="' + data.FTR_CDE +'", ftrIdn="'+ data.FTR_IDN +'"></div>' +
						'</td>' +
						'</tr>' ;
						break;
						
					// 정수장
					case 'WTL_PURI_AS':
						idenData += 
						'<tr>' +
						'<th>지형지물부호</th>' +
						'<td class="white"><p>'+ ftrNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>관리번호</th>' +
						'<td class="white"><p>'+ data.FTR_IDN + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>행정읍면동코드</th>' +
						'<td class="white"><p>'+ hjdNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>도엽번호</th>' +
						'<td class="white"><p>'+ data.SHT_NUM + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>관리기관</th>' +
						'<td class="white"><p>'+ mngNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>준공일자</th>' +
						'<td class="white"><p>'+ fnsDate + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>정수장명</th>' +
						'<td class="white"><p>'+ data.WSR_CDE + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>수원구분</th>' +
						'<td class="white"><p>'+ data.WSR_CDE + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>처리용량</th>' +
						'<td class="white"><p>'+ data.PUR_VOL + 
						'<div style="display:none" id="divSubmit" layerType="puri" ftrCde="' + data.FTR_CDE +'", ftrIdn="'+ data.FTR_IDN +'"></div>' +
						'</td>' +
						'</tr>' ;
						break;
						
					// 저수조
					case 'WTL_RSRV_PS':
						idenData += 
						'<tr>' +
						'<th>지형지물부호</th>' +
						'<td class="white"><p>'+ ftrNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>관리번호</th>' +
						'<td class="white"><p>'+ data.FTR_IDN + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>행정읍면동코드</th>' +
						'<td class="white"><p>'+ hjdNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>도엽번호</th>' +
						'<td class="white"><p>'+ data.SHT_NUM + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>관리기관</th>' +
						'<td class="white"><p>'+ mngNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>건물유형</th>' +
						'<td class="white"><p>'+ data.BLS_CDE + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>저수조명</th>' +
						'<td class="white"><p>'+ data.RSR_NAM + 
						'<div style="display:none" id="divSubmit" layerType="rsrv" ftrCde="' + data.FTR_CDE +'", ftrIdn="'+ data.FTR_IDN +'"></div>' +
						'</td>' +
						'</tr>' ;
						break;
						
					// 배수지
					case 'WTL_SERV_PS':
						idenData += 
						'<tr>' +
						'<th>지형지물부호</th>' +
						'<td class="white"><p>'+ ftrNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>관리번호</th>' +
						'<td class="white"><p>'+ data.FTR_IDN + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>행정읍면동코드</th>' +
						'<td class="white"><p>'+ hjdNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>도엽번호</th>' +
						'<td class="white"><p>'+ data.SHT_NUM + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>관리기관</th>' +
						'<td class="white"><p>'+ mngNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>배수지명</th>' +
						'<td class="white"><p>'+ data.SRV_NAM + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>관리방법</th>' +
						'<td class="white"><p>'+ data.SAG_CDE + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>시설용량</th>' +
						'<td class="white"><p>'+ data.SRV_VOL + 
						'<div style="display:none" id="divSubmit" layerType="serv" ftrCde="' + data.FTR_CDE +'", ftrIdn="'+ data.FTR_IDN +'"></div>' +
						'</td>' +
						'</tr>' ;
						break;
						
					// 급수관로
					case 'WTL_SPLY_LS':
						idenData += 
						'<tr>' +
						'<th>지형지물부호</th>' +
						'<td class="white"><p>'+ ftrNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>관리번호</th>' +
						'<td class="white"><p>'+ data.FTR_IDN + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>행정읍면동코드</th>' +
						'<td class="white"><p>'+ hjdNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>도엽번호</th>' +
						'<td class="white"><p>'+ data.SHT_NUM + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>관리기관</th>' +
						'<td class="white"><p>'+ mngNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>설치일자</th>' +
						'<td class="white"><p>'+ istDate + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>관라벨</th>' +
						'<td class="white"><p>'+ data.PIP_LBL + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>개통상태</th>' +
						'<td class="white"><p>'+ data.SOO_CDE + 
						'<div style="display:none" id="divSubmit" layerType="sply" ftrCde="' + data.FTR_CDE +'", ftrIdn="'+ data.FTR_IDN +'"></div>' +
						'</td>' +
						'</tr>' ;
						break;
						
					// 변류시설
					case 'WTL_VALV_PS':
						idenData += 
							'<tr>' +
							'<th>지형지물부호</th>' +
							'<td >'+ ftrNm + '</td>' +
							'</tr>' +
							'<tr>' +
							'<th>관리번호</th>' +
							'<td >'+ data.FTR_IDN + '</td>' +
							'</tr>' +
							'<tr>' +
							'<th>행정읍면동코드</th>' +
							'<td >'+ hjdNm + '</td>' +
							'</tr>' +
							'<tr>' +
							'<th>도엽번호</th>' +
							'<td >'+ data.SHT_NUM + '</td>' +
							'</tr>' +
							'<tr>' +
							'<th>관리기관</th>' +
							'<td >'+ mngNm + '</td>' +
							'</tr>' +
							'<tr>' +
							'<th>설치일자</th>' +
							'<td >'+ istDate + '</td>' +
							'</tr>' +
							'<tr>' +
							'<th>시설물형태</th>' +
							'<td >'+ data.FOR_CDE + '</td>' +
							'</tr>' +
							'<tr>' +
							'<th>관로지형지물부호</th>' +
							'<td >'+ data.PIP_CDE + '</td>' +
							'</tr>' +
							'<tr>' +
							'<th>이상상태</th>' +
							'<td >'+ data.CST_CDE + 
							'<div style="display:none" id="divSubmit" layerType="valv" ftrCde="' + data.FTR_CDE +'", ftrIdn="'+ data.FTR_IDN +'"></div>' +
							'</td>' +
							'</tr>' ;
							//'<div style="float:right;"><input type="submit" value="대장보기" name="save" class="submit" onclick=\'openInfoView("valv", "' + data.FTR_CDE +'", "'+ data.FTR_IDN +'");\'></div>' +
						break;
						
					// 양수장
					case 'WTL_WPMP_AS':
						idenData += 
						'<tr>' +
						'<th>지형지물부호</th>' +
						'<td class="white"><p>'+ ftrNm + '</td>' +
						'</tr>' +
						'<tr>' +
						'<th>양수장명</th>' +
						'<td class="white"><p>'+ data.WPM_NAM + 
						'<div style="display:none" id="divSubmit" layerType="valv" ftrCde="' + data.FTR_CDE +'", ftrIdn="'+ data.FTR_IDN +'"></div>' +
						'</td>' +
						'</tr>' ;
						break;
						
					default : idenData +=
						'<tr>' +
						'<td class="white"><p>서비스 준비 중 입니다.</th>'
						'</tr>'+
						'</tbody>' +
						'</table>'
						
						break;
					}
					/*idenData +=	
						'</tbody>' +
						'</table>';*/
					
					$("#map_layer").find("#map_layer_tb").html(idenData);
					if(!$("#map_layer").is(":visible")){
						$("#map_layer").show();
					}
					overlay.setPosition(coordinate);
				},
				error : function() {
					alert('오류가 발생 하였습니다.');
				}
				
			})
		}

	} else {
		map.removeInteraction(selectObject);
		map.removeInteraction(highlighter);
	}
});


function featureInfoClose() {

	overlay.setPosition(undefined);
	$('#map_layer_close').blur();
	
	flag = 0;
	map.removeInteraction(selectObject);
	map.removeInteraction(highlighter);
	return false;
}