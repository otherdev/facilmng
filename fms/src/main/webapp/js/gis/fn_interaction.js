
/*****************************************************
 * Draw가 실행될 벡터레이어 선언 - Collection 레이어
 */
var drawFeatures = new ol.Collection();
var featureOverlay = new ol.layer.Vector({
	source : new ol.source.Vector({
		features : drawFeatures
	}),
	style : new ol.style.Style({
		fill : new ol.style.Fill({
			color : 'rgba(255, 255, 255, 0.2)'
		}),
		stroke : new ol.style.Stroke({
			color : '#ffcc33',
			width : 2
		}),
		image : new ol.style.Circle({
			radius : 7,
			fill : new ol.style.Fill({
				color : '#ffcc33'
			})
		})
	})
});




/**
 * Draw 객체
 */
var lineDraw = new ol.interaction.Draw({
	features : drawFeatures,
	type : "LineString"
}); // global so we can remove it later

var polyDraw = new ol.interaction.Draw({
	features : drawFeatures,
	type : "Polygon"
}); // global so we can remove it later

var pointDraw = new ol.interaction.Draw({
	features : drawFeatures,
	type : "Point"
}); // global so we can remove it later


/**
 * Draw객체의 이벤트 설정 - 로딩전이므로 런타임에 설정해야할듯
 */
/*
lineDraw.on('drawend', function (evt) {
	$('input[name="coordinates"]').val(evt.feature.getGeometry().getCoordinates());
});
polyDraw.on('drawend', function (evt) {
	$('input[name="coordinates"]').val(evt.feature.getGeometry().getCoordinates());
});
pointDraw.on('drawend', function (evt) {
	$('input[name="coordinates"]').val(evt.feature.getGeometry().getCoordinates());
});
*/








/*****************************************************
 * Modify select 오버레이 스타일
 */
var overlayStyle = (function() {
	var styles = {};
	styles['Polygon'] = [ new ol.style.Style({
		// 선택했을떄
		stroke : new ol.style.Stroke({
			color : [ 0, 153, 255, 1 ],
			width : 5
		}),
		fill : new ol.style.Fill({
			color : [ 255, 255, 255, 0.1 ]
		})
	}), new ol.style.Style({
		stroke : new ol.style.Stroke({
			color : [ 0, 153, 255, 1 ],
			width : 5
		}),
		image: new ol.style.Circle({
			radius: 5,
			fill: new ol.style.Fill({
			color: 'orange'
			})
		}),
		geometry: function(feature) {
			// return the coordinates of the first ring of the polygon
			var coordinates = feature.getGeometry().getCoordinates()[0];
			return new ol.geom.MultiPoint(coordinates[0]);
		}
	
	})];
	styles['MultiPolygon'] = styles['Polygon'];

	styles['LineString'] = [ new ol.style.Style({
		stroke : new ol.style.Stroke({
			color : [ 0, 153, 255, 1 ],
			width : 5
		})
	}), new ol.style.Style({
		stroke : new ol.style.Stroke({
			color : [ 0, 153, 255, 1 ],
			width : 1
		}),
		image: new ol.style.Circle({
			radius: 5,
			fill: new ol.style.Fill({
			color: 'orange'
			})
		}),
		geometry: function(feature) {
			// return the coordinates of the first ring of the polygon
			var coordinates = feature.getGeometry().getCoordinates();
			return new ol.geom.MultiPoint(coordinates);
		}
	}) ];
	styles['MultiLineString'] = [ new ol.style.Style({
		stroke : new ol.style.Stroke({
			color : [ 0, 153, 255, 1 ],
			width : 5
		})
	}), new ol.style.Style({
		stroke : new ol.style.Stroke({
			color : [ 0, 153, 255, 1 ],
			width : 1
		}),
		image: new ol.style.Circle({
			radius: 5,
			fill: new ol.style.Fill({
			color: 'orange'
			})
		}),
		geometry: function(feature) {
			// return the coordinates of the first ring of the polygon
			var coordinates = feature.getGeometry().getCoordinates()[0];
			return new ol.geom.MultiPoint(coordinates);
		}
	}) ];;

	styles['Point'] = [ new ol.style.Style({
		image: new ol.style.Circle({
			radius: 5,
			fill: new ol.style.Fill({
			color: 'orange'
			})
		}),
		zIndex : 100000
	}) ];
	styles['MultiPoint'] = styles['Point'];

	styles['GeometryCollection'] = styles['Polygon'].concat(styles['Point']);
	

	return function(feature) {
		return styles[feature.getGeometry().getType()];
	};
})();


/** 
 * Modify select 객체
 */
var select = new ol.interaction.Select({
	style : overlayStyle,
	hitTolerance : 5
});

var modify = new ol.interaction.Modify({
	features : select.getFeatures(),
	style : overlayStyle,
	/* insertVertexCondition : function() {
		// prevent new vertices to be added to the polygons
		return !this.features_.getArray().every(function(feature) {
			return feature.getGeometry().getType().match(/Polygon/);
		});
	}, */

	//features : new ol.Collection(featureArray),
	deleteCondition : function(evt) {
		return ol.events.condition.shiftKeyOnly(evt)
				&& ol.events.condition.singleClick(evt);
	}
});

var originalCoordinates = {};



/***************************************
 * drag , del 객체
 */

var drag = new app.Drag();
var del = new ol.interaction.Select({
	style : overlayStyle
});

















//interaction 초기화
function removeInteraction() {

	select.getFeatures().clear();
	del.getFeatures().clear();
	
	map.removeInteraction(select);
	map.removeInteraction(modify);
	map.removeInteraction(drag);
	map.removeInteraction(del);

	map.removeInteraction(lineDraw);
	map.removeInteraction(polyDraw);
	map.removeInteraction(pointDraw);
	
}





