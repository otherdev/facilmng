/**
 * 툴바기능 관련
 */




// 전체확대
function ExtentZoom() {
		map.getView().fit(bounds, map.getSize()); 
};


//줌 확대
function plusZoom() {
	map.getView().setZoom(map.getView().getZoom() + 1);
	console.log(map.getView().calculateExtent(map.getSize()));
}


// 줌 축소
function minusZoom() {
	map.getView().setZoom(map.getView().getZoom() - 1);
}





var mDraw;
//거리재기(선,면적)	
function addMeasure(value) {
	map.removeInteraction(mDraw);
	map.on('pointermove', pointerMoveHandler);
	
	var type = (value == 'area' ? 'Polygon' : 'LineString');
	mDraw = new ol.interaction.Draw({
		source: source,
		type: /** @type {ol.geom.GeometryType} */ (type),
		style: new ol.style.Style({
			fill: new ol.style.Fill({
			color: 'rgba(255, 255, 255, 0.2)'
			}),
			stroke: new ol.style.Stroke({
			color: 'rgba(0, 0, 0, 0.5)',
			lineDash: [10, 10],
			width: 2
			})
		})
	});
	
	map.addInteraction(mDraw);

	createMeasureTooltip();
	createHelpTooltip();

	mDraw.on('drawstart', function(evt) {
		// set sketch
		sketch = evt.feature;
	}, this);

	mDraw.on('drawend', function(evt) {
		measureTooltipElement.className = 'tooltip tooltip-static';
		measureTooltip.setOffset([0, -7]);
		
		// unset sketch
		sketch = null;
		// unset tooltip so that a new one can be created
		measureTooltipElement = null;
		createMeasureTooltip();
		
		// Interaction 초기화
		map.removeInteraction(mDraw);
		//setTimeout("map.removeInteraction(mDraw)", 3000);

		
		
	}, this);

}


//정보보기 객체 처리
function featureInfoView() {
	if (flag ==1 ) {
		var chked_val = "";
		$(":checkbox[name='chkList']:checked").each(function(pi,po){
		  chked_val += ","+po.value;
		});
		if (chked_val=="") {
			 alert("선택된 레이어가 없습니다.");
			 return;
		} 
	
		map.removeInteraction(selectObject);
		map.removeInteraction(highlighter);
		//map.getInteractions().extend([selectObject]);
		map.addInteraction(selectObject);
		map.addInteraction(highlighter);
		
		chkLayer(chked_val);
	}
}




//자식 레이어 제어
function layerSwitch(layer_nm, chkObj){
	map.getLayers().forEach(function (layer) {
		// 레이어 On
		if (layer.get('name') == layer_nm) {
			
			if(chkObj.checked == true) {
				layer.setVisible(true);
			}
			if(chkObj.checked == false) {
				layer.setVisible(false);
			}
		}
		
		// 레이어 Off
		if ( layer.get('name') != undefined ) {
			map.removeInteraction(selectObject);
			if (layer.get('name').indexOf("_Vec") != -1) {
				layer.setVisible(false);
			}
		}
	});
	// 정보보기 객체 처리 - 이걸왜하지..
	//featureInfoView();
	
}
// 부모 레이어 제어
function layerSwitchM(name,id){
	map.getLayers().forEach(function (layer) {
		var olMapLayers = layer.get('name');
		// 레이어 On
		if (olMapLayers.indexOf(name+"_SA") != -1) {
			if(id.checked == true) {
				layer.setVisible(true);
			}
			if(id.checked == false) {
				layer.setVisible(false);
			}
		}
		
		// 레이어 Off
		if ( olMapLayers != undefined ) {
			map.removeInteraction(selectObject);
			if (olMapLayers.indexOf("_Vec") != -1) {
				layer.setVisible(false);
			}
		}
		
	});
	// 정보보기 객체 처리
	featureInfoView();
}
// 전체 레이어 제어
function layerSwitchG(id){
	map.getLayers().forEach(function (layer) {
		var olMapLayers = layer.get('name');
		// img레이어 모두 On
		if (olMapLayers.length == 11 || olMapLayers.length == 17) {
			if (olMapLayers !="WTL_FIRE_PS" && olMapLayers !="WTL_VALV_PS" ) {
				if(id.checked == true) {
					layer.setVisible(true);
				}
				if(id.checked == false) {
					layer.setVisible(false);
				}
			}
		} 
		
		// 레이어 Off
		if ( olMapLayers != undefined ) {
			map.removeInteraction(selectObject);
			if (olMapLayers.indexOf("_Vec") != -1) {
				layer.setVisible(false);
			}
		}
		
	});
	// 정보보기 객체 처리
	featureInfoView();
}



//지우기(레이어 초기화)
function layerRefresh() {
	// 그린 Vector Layer 지우기
	vector.getSource().clear();
	// 거리, 면적팝업레이어 지우기
	removeTooltip();
	// 시설물 이동 레이어
	map.removeLayer(uisLayer);
	// Interaction 초기화
	map.removeInteraction(mDraw);
}



