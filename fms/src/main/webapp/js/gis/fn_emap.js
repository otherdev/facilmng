/**
 * 바로e맵 레이어 설정
 */

var _resolutions = [
	1954.5973888747778, 
	977.2986945, 
	488.64934725,
	244.324673625, 
	122.1623368125, 
	61.08116840625,
	30.540584203125, 
	15.2702921015625, 
	7.63514605078125,
	3.817573025390625, 
	1.9087865126953125, 
	0.9543932563476563,
	0.47719662817382813, 
	0.23859831408691407 
];
var _levels = ['L06', 'L07', 'L08', 'L09', 'L10', 'L11', 'L12', 'L13', 'L14', 'L15', 'L16', 'L17', 'L18', 'L19' ];

var source = new ol.source.XYZ({
	projection : "EPSG:5179",
	tileUrlFunction : function(tileCoord) {
		var bounds = this.tileGrid.getTileCoordExtent(tileCoord);
		var origin = this.tileGrid.getOrigin();
		var resolution = this.tileGrid.getResolution(tileCoord[0]);
		var tileSize = this.tileGrid.getTileSize();
		var x = Math.round((bounds[0] - origin[0]) / (resolution * tileSize));
		var y = Math.round((bounds[1] - origin[1]) / (resolution * tileSize));
		y = y + Math.pow(2, tileCoord[0] + 6); // Correction factor
		var mapUri = eMapUrl + "{z}/{x}/{y}.png";
		
		var _uri = mapUri.replace('{z}',
									_levels[tileCoord[0]].toString()).replace('{x}',
									x.toString()).replace('{y}', y.toString()
								 );
		return _uri; 
	},
	tileGrid: new ol.tilegrid.TileGrid({
		tileSize : 256,
		resolutions : _resolutions,
		origin : [ -200000, 4000000 ]
	}),
	wrapX : false
});

var emap_layer = new ol.layer.Tile({
	title : 'eMap',
	name: 'eMap',
	source : source,
	layerName : "baroE_korea",
	layerCategory : 'background'
});