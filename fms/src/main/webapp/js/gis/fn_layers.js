/**
 * 시설물 레이어 벡터정보
 */


var WTL_FIRE_PS_Url = gisWfsUrl + "request=GetFeature&typename=WTL_FIRE_PS&outputFormat=application/json";//&CQL_FILTER=MNG_CDE='MNG106'";
var WTL_FLOW_PS_Url = gisWfsUrl + "request=GetFeature&typename=WTL_FLOW_PS&outputFormat=application/json";	
var WTL_GAIN_PS_Url = gisWfsUrl + "request=GetFeature&typename=WTL_GAIN_PS&outputFormat=application/json";	
var WTL_HEAD_PS_Url = gisWfsUrl + "request=GetFeature&typename=WTL_HEAD_PS&outputFormat=application/json";	
var WTL_LEAK_PS_Url = gisWfsUrl + "request=GetFeature&typename=WTL_LEAK_PS&outputFormat=application/json";	
var WTL_MANH_PS_Url = gisWfsUrl + "request=GetFeature&typename=WTL_MANH_PS&outputFormat=application/json";	
var WTL_PRES_PS_Url = gisWfsUrl + "request=GetFeature&typename=WTL_PRES_PS&outputFormat=application/json";	
var WTL_PRGA_PS_Url = gisWfsUrl + "request=GetFeature&typename=WTL_PRGA_PS&outputFormat=application/json";	
var WTL_PURI_AS_Url = gisWfsUrl + "request=GetFeature&typename=WTL_PURI_AS&outputFormat=application/json";	
var WTL_RSRV_PS_Url = gisWfsUrl + "request=GetFeature&typename=WTL_RSRV_PS&outputFormat=application/json";	
var WTL_SERV_PS_Url = gisWfsUrl + "request=GetFeature&typename=WTL_SERV_PS&outputFormat=application/json";	
var WTL_VALV_PS_Url = gisWfsUrl + "request=GetFeature&typename=WTL_VALV_PS&outputFormat=application/json";	
var WTL_WPMP_AS_Url = gisWfsUrl + "request=GetFeature&typename=WTL_WPMP_AS&outputFormat=application/json";
var WTL_PIPE_LM_Url = gisWfsUrl + "request=GetFeature&typename=WTL_PIPE_LM&outputFormat=application/json";
var WTL_SPLY_LS_Url = gisWfsUrl + "request=GetFeature&typename=WTL_SPLY_LS&outputFormat=application/json";
var WTL_META_PS_Url = gisWfsUrl + "request=GetFeature&typename=WTL_META_PS&outputFormat=application/json";
var WTL_PIPE_PS_Url = gisWfsUrl + "request=GetFeature&typename=WTL_PIPE_PS&outputFormat=application/json";
var WTL_CLPI_LM_Url = gisWfsUrl + "request=GetFeature&typename=WTL_CLPI_LM&outputFormat=application/json";

var vectorStyle = (function() {
	var styles = {};
	styles['Polygon'] = [ new ol.style.Style({
		stroke : new ol.style.Stroke({
			color : [ 0, 153, 255, 1 ],
			width : 1
		}),
		fill : new ol.style.Fill({
			color : [ 255, 255, 255, 0.2 ]
		})
	}) ];
	styles['MultiPolygon'] = styles['Polygon'];

	styles['LineString'] = [ new ol.style.Style({
		stroke : new ol.style.Stroke({
			color : [ 255, 255, 255, 0.5 ],
			width : 2
		})
	}), new ol.style.Style({
		stroke : new ol.style.Stroke({
			color : [ 255, 255, 255, 0.5 ],
			width : 2
		}),
		geometry: function(feature) {
			// return the coordinates of the first ring of the polygon
			var coordinates = feature.getGeometry().getCoordinates()[0];
			return new ol.geom.MultiPoint(coordinates);
		}
	}) ];
	styles['MultiLineString'] = styles['LineString'];

	styles['Point'] = [ new ol.style.Style({
		image : new ol.style.Circle({
			radius : 5,
			stroke : new ol.style.Stroke({
				color : [ 255, 255, 255, 0.1]
			})
		}),
		zIndex : 100000
	}) ];
	styles['MultiPoint'] = styles['Point'];

	styles['GeometryCollection'] = styles['Polygon'].concat(styles['Point']);
	

	return function(feature) {
		return styles[feature.getGeometry().getType()];
	};
})();


// 1. WTL_FIRE_PS
var WTL_FIRE_PS_Edit = new ol.layer.Vector({
	title : 'WTL_FIRE_PS_Edit',
	name: 'WTL_FIRE_PS_Edit',
	visible : false,
	source : new ol.source.Vector({
		format : new ol.format.GeoJSON(),
		url : function(extent, projection) {
			return WTL_FIRE_PS_Url +"&bbox=" + extent.join(',') + ",EPSG:5187";
		},
		strategy : ol.loadingstrategy.bbox
	})
});
var WTL_FIRE_PS_Vector = new ol.layer.Vector({
	title : 'WTL_FIRE_PS_Vec',
	name: 'WTL_FIRE_PS_Vec',
	visible : false,
	style: vectorStyle,
	source : new ol.source.Vector({
		format : new ol.format.GeoJSON(),
		url : function(extent, projection) {
			return WTL_FIRE_PS_Url+"&bbox=" + extent.join(',') + ",EPSG:5187";
		},
		strategy : ol.loadingstrategy.bbox
	})
});
var WTL_FIRE_PS_Layer = new ol.layer.Image({
	title : 'WTL_FIRE_PS',
	name: 'WTL_FIRE_PS',
	visible : false,
	source : new ol.source.ImageWMS({
		ratio : 1,
		url : gisWmsUrl,
		params : {
			'FORMAT' : 'image/png',
			'VERSION' : '1.1.1',
			STYLES : 'WTL_FIRE_PS',
			LAYERS : 'WTL_FIRE_PS',
		}
	})
});

var WTL_FIRE_PS_Layer_SA118 = new ol.layer.Image({
	title : 'WTL_FIRE_PS_SA118',
	name: 'WTL_FIRE_PS_SA118',
	visible : false,
	source : new ol.source.ImageWMS({
		ratio : 1,
		url : gisWmsUrl,
		params : {
			'FORMAT' : 'image/png',
			'VERSION' : '1.1.1',
			STYLES : 'WTL_FIRE_PS_SA118',
			LAYERS : 'WTL_FIRE_PS',
		}
	})
});

var WTL_FIRE_PS_Layer_SA119 = new ol.layer.Image({
	title : 'WTL_FIRE_PS_SA119',
	name: 'WTL_FIRE_PS_SA119',
	visible : false,
	source : new ol.source.ImageWMS({
		ratio : 1,
		url : gisWmsUrl,
		params : {
			'FORMAT' : 'image/png',
			'VERSION' : '1.1.1',
			STYLES : 'WTL_FIRE_PS_SA119',
			LAYERS : 'WTL_FIRE_PS',
		}
	})
});
// 2. WTL_FLOW_PS
var WTL_FLOW_PS_Edit = new ol.layer.Vector({
	title : 'WTL_FLOW_PS_Edit',
	name: 'WTL_FLOW_PS_Edit',
	visible : false,
	source : new ol.source.Vector({
		format : new ol.format.GeoJSON(),
		url : function(extent, projection) {
			return WTL_FLOW_PS_Url +"&bbox=" + extent.join(',') + ",EPSG:5187";
		},
		strategy : ol.loadingstrategy.bbox
	})
});
var WTL_FLOW_PS_Vector = new ol.layer.Vector({
	title : 'WTL_FLOW_PS_Vec',
	name: 'WTL_FLOW_PS_Vec',
	visible : false,
	style: vectorStyle,
	source : new ol.source.Vector({
		format : new ol.format.GeoJSON(),
		url : function(extent, projection) {
			return WTL_FLOW_PS_Url +"&bbox=" + extent.join(',') + ",EPSG:5187";
		},
		strategy : ol.loadingstrategy.bbox
	})
});
var WTL_FLOW_PS_Layer = new ol.layer.Image({
	title : 'WTL_FLOW_PS',
	name: 'WTL_FLOW_PS',
	visible : false,
	source : new ol.source.ImageWMS({
		ratio : 1,
		url : gisWmsUrl,
		params : {
			'FORMAT' : 'image/png',
			'VERSION' : '1.1.1',
			STYLES : 'WTL_FLOW_PS',
			LAYERS : 'WTL_FLOW_PS',
		}
	})
});

// 3. WTL_GAIN_PS
var WTL_GAIN_PS_Edit = new ol.layer.Vector({
	title : 'WTL_GAIN_PS_Edit',
	name: 'WTL_GAIN_PS_Edit',
	visible : false,
	source : new ol.source.Vector({
		format : new ol.format.GeoJSON(),
		url : function(extent, projection) {
			return WTL_GAIN_PS_Url +"&bbox=" + extent.join(',') + ",EPSG:5187";
		},
		strategy : ol.loadingstrategy.bbox
	})
});
var WTL_GAIN_PS_Vector = new ol.layer.Vector({
	title : 'WTL_GAIN_PS_Vec',
	name: 'WTL_GAIN_PS_Vec',
	visible : false,
	style: vectorStyle,
	source : new ol.source.Vector({
		format : new ol.format.GeoJSON(),
		url : function(extent, projection) {
			return WTL_GAIN_PS_Url +"&bbox=" + extent.join(',') + ",EPSG:5187";
		},
		strategy : ol.loadingstrategy.bbox
	})
});
var WTL_GAIN_PS_Layer = new ol.layer.Image({
	title : 'WTL_GAIN_PS',
	name: 'WTL_GAIN_PS',
	visible : false,
	source : new ol.source.ImageWMS({
		ratio : 1,
		url : gisWmsUrl,
		params : {
			'FORMAT' : 'image/png',
			'VERSION' : '1.1.1',
			STYLES : 'WTL_GAIN_PS',
			LAYERS : 'WTL_GAIN_PS',
		}
	})
});
// 4. WTL_HEAD_PS
var WTL_HEAD_PS_Edit = new ol.layer.Vector({
	title : 'WTL_HEAD_PS_Edit',
	name: 'WTL_HEAD_PS_Edit',
	visible : false,
	source : new ol.source.Vector({
		format : new ol.format.GeoJSON(),
		url : function(extent, projection) {
			return WTL_HEAD_PS_Url +"&bbox=" + extent.join(',') + ",EPSG:5187";
		},
		strategy : ol.loadingstrategy.bbox
	})
});
var WTL_HEAD_PS_Vector = new ol.layer.Vector({
	title : 'WTL_HEAD_PS_Vec',
	name: 'WTL_HEAD_PS_Vec',
	visible : false,
	style: vectorStyle,
	source : new ol.source.Vector({
		format : new ol.format.GeoJSON(),
		url : function(extent, projection) {
			return WTL_HEAD_PS_Url +"&bbox=" + extent.join(',') + ",EPSG:5187";
		},
		strategy : ol.loadingstrategy.bbox
	})
});
var WTL_HEAD_PS_Layer = new ol.layer.Image({
	title : 'WTL_HEAD_PS',
	name: 'WTL_HEAD_PS',
	visible : false,
	source : new ol.source.ImageWMS({
		ratio : 1,
		url : gisWmsUrl,
		params : {
			'FORMAT' : 'image/png',
			'VERSION' : '1.1.1',
			STYLES : 'WTL_HEAD_PS',
			LAYERS : 'WTL_HEAD_PS',
		}
	})
});

// 5. WTL_LEAK_PS
var WTL_LEAK_PS_Edit = new ol.layer.Vector({
	title : 'WTL_LEAK_PS_Edit',
	name: 'WTL_LEAK_PS_Edit',
	visible : false,
	source : new ol.source.Vector({
		format : new ol.format.GeoJSON(),
		url : function(extent, projection) {
			return WTL_LEAK_PS_Url +"&bbox=" + extent.join(',') + ",EPSG:5187";
		},
		strategy : ol.loadingstrategy.bbox
	})
});
var WTL_LEAK_PS_Vector = new ol.layer.Vector({
	title : 'WTL_LEAK_PS_Vec',
	name: 'WTL_LEAK_PS_Vec',
	style: vectorStyle,
	visible : false,
	source : new ol.source.Vector({
		format : new ol.format.GeoJSON(),
		url : function(extent, projection) {
			return WTL_LEAK_PS_Url +"&bbox=" + extent.join(',') + ",EPSG:5187";
		},
		strategy : ol.loadingstrategy.bbox
	})
});
var WTL_LEAK_PS_Layer = new ol.layer.Image({
	title : 'WTL_LEAK_PS',
	name: 'WTL_LEAK_PS',
	visible : false,
	source : new ol.source.ImageWMS({
		ratio : 1,
		url : gisWmsUrl,
		params : {
			'FORMAT' : 'image/png',
			'VERSION' : '1.1.1',
			STYLES : 'WTL_LEAK_PS',
			LAYERS : 'WTL_LEAK_PS',
		}
	})
});

// 6. WTL_MANH_PS
var WTL_MANH_PS_Edit = new ol.layer.Vector({
	title : 'WTL_MANH_PS_Edit',
	name: 'WTL_MANH_PS_Edit',
	visible : false,
	source : new ol.source.Vector({
		format : new ol.format.GeoJSON(),
		url : function(extent, projection) {
			return WTL_MANH_PS_Url +"&bbox=" + extent.join(',') + ",EPSG:5187";
		},
		strategy : ol.loadingstrategy.bbox
	})
});
var WTL_MANH_PS_Vector = new ol.layer.Vector({
	title : 'WTL_MANH_PS_Vec',
	name: 'WTL_MANH_PS_Vec',
	visible : false,
	style: vectorStyle,
	source : new ol.source.Vector({
		format : new ol.format.GeoJSON(),
		url : function(extent, projection) {
			return WTL_MANH_PS_Url +"&bbox=" + extent.join(',') + ",EPSG:5187";
		},
		strategy : ol.loadingstrategy.bbox
	})
});
var WTL_MANH_PS_Layer = new ol.layer.Image({
	title : 'WTL_MANH_PS',
	name: 'WTL_MANH_PS',
	visible : false,
	source : new ol.source.ImageWMS({
		ratio : 1,
		url : gisWmsUrl,
		params : {
			'FORMAT' : 'image/png',
			'VERSION' : '1.1.1',
			STYLES : 'WTL_MANH_PS',
			LAYERS : 'WTL_MANH_PS',
		}
	})
});


// . WTL_META_PS
var WTL_META_PS_Edit = new ol.layer.Vector({
	title : 'WTL_META_PS_Edit',
	name: 'WTL_META_PS_Edit',
	visible : false,
	source : new ol.source.Vector({
		format : new ol.format.GeoJSON(),
		url : function(extent, projection) {
			return WTL_META_PS_Url +"&bbox=" + extent.join(',') + ",EPSG:5187";
		},
		strategy : ol.loadingstrategy.bbox
	})
});
var WTL_META_PS_Vector = new ol.layer.Vector({
	title : 'WTL_META_PS_Vec',
	name: 'WTL_META_PS_Vec',
	visible : false,
	style: vectorStyle,
	source : new ol.source.Vector({
		format : new ol.format.GeoJSON(),
		url : function(extent, projection) {
			return WTL_META_PS_Url +"&bbox=" + extent.join(',') + ",EPSG:5187";
		},
		strategy : ol.loadingstrategy.bbox
	})
});
var WTL_META_PS_Layer = new ol.layer.Image({
	title : 'WTL_META_PS',
	name: 'WTL_META_PS',
	visible : false,
	source : new ol.source.ImageWMS({
		ratio : 1,
		url : gisWmsUrl,
		params : {
			'FORMAT' : 'image/png',
			'VERSION' : '1.1.1',
			STYLES : 'WTL_META_PS',
			LAYERS : 'WTL_META_PS',
		}
	})
});

// 14. WTL_PIPE_LM 
var WTL_PIPE_LM_Edit = new ol.layer.Vector({
	title : 'WTL_PIPE_LM_Edit',
	name: 'WTL_PIPE_LM_Edit',
	visible : false,
	source : new ol.source.Vector({
		format : new ol.format.GeoJSON(),
		url : function(extent, projection) {
			return WTL_PIPE_LM_Url +"&bbox=" + extent.join(',') + ",EPSG:5187";
		},
		strategy : ol.loadingstrategy.bbox
	})
});
var WTL_PIPE_LM_Vector = new ol.layer.Vector({
	title : 'WTL_PIPE_LM_Vec',
	name: 'WTL_PIPE_LM_Vec',
	visible : false,
	style: vectorStyle,
	source : new ol.source.Vector({
		format : new ol.format.GeoJSON(),
		url : function(extent, projection) {
			return WTL_PIPE_LM_Url +"&bbox=" + extent.join(',') + ",EPSG:5187";
		},
		strategy : ol.loadingstrategy.bbox
	})
});

var WTL_PIPE_LM_Layer = new ol.layer.Image({
	title : 'WTL_PIPE_LM',
	name: 'WTL_PIPE_LM',
	visible : false,
	source : new ol.source.ImageWMS({
		ratio : 1,
		url : gisWmsUrl,
		params : {
			'FORMAT' : 'image/png',
			'VERSION' : '1.1.1',
			STYLES : 'WTL_PIPE_LM',
			LAYERS : 'WTL_PIPE_LM',
		}
	})
});

// 15. WTL_PIPE_PS
var WTL_PIPE_PS_Edit = new ol.layer.Vector({
	title : 'WTL_PIPE_PS_Edit',
	name: 'WTL_PIPE_PS_Edit',
	visible : false,
	source : new ol.source.Vector({
		format : new ol.format.GeoJSON(),
		url : function(extent, projection) {
			return WTL_PIPE_PS_Url +"&bbox=" + extent.join(',') + ",EPSG:5187";
		},
		strategy : ol.loadingstrategy.bbox
	})
});
var WTL_PIPE_PS_Vector = new ol.layer.Vector({
	title : 'WTL_PIPE_PS_Vec',
	name: 'WTL_PIPE_PS_Vec',
	visible : false,
	style: vectorStyle,
	source : new ol.source.Vector({
		format : new ol.format.GeoJSON(),
		url : function(extent, projection) {
			return WTL_PIPE_PS_Url +"&bbox=" + extent.join(',') + ",EPSG:5187";
		},
		strategy : ol.loadingstrategy.bbox
	})
});
var WTL_PIPE_PS_Layer = new ol.layer.Image({
	title : 'WTL_PIPE_PS',
	name: 'WTL_PIPE_PS',
	visible : false,
	source : new ol.source.ImageWMS({
		ratio : 1,
		url : gisWmsUrl,
		params : {
			'FORMAT' : 'image/png',
			'VERSION' : '1.1.1',
			STYLES : 'WTL_PIPE_PS',
			LAYERS : 'WTL_PIPE_PS',
		}
	})
});

// 7. WTL_PRES_PS
var WTL_PRES_PS_Edit = new ol.layer.Vector({
	title : 'WTL_PRES_PS_Edit',
	name: 'WTL_PRES_PS_Edit',
	visible : false,
	source : new ol.source.Vector({
		format : new ol.format.GeoJSON(),
		url : function(extent, projection) {
			return WTL_PRES_PS_Url +"&bbox=" + extent.join(',') + ",EPSG:5187";
		},
		strategy : ol.loadingstrategy.bbox
	})
});
var WTL_PRES_PS_Vector = new ol.layer.Vector({
	title : 'WTL_PRES_PS_Vec',
	name: 'WTL_PRES_PS_Vec',
	visible : false,
	style: vectorStyle,
	source : new ol.source.Vector({
		format : new ol.format.GeoJSON(),
		url : function(extent, projection) {
			return WTL_PRES_PS_Url +"&bbox=" + extent.join(',') + ",EPSG:5187";
		},
		strategy : ol.loadingstrategy.bbox
	})
});
var WTL_PRES_PS_Layer = new ol.layer.Image({
	title : 'WTL_PRES_PS',
	name: 'WTL_PRES_PS',
	visible : false,
	source : new ol.source.ImageWMS({
		ratio : 1,
		url : gisWmsUrl,
		params : {
			'FORMAT' : 'image/png',
			'VERSION' : '1.1.1',
			STYLES : 'WTL_PRES_PS',
			LAYERS : 'WTL_PRES_PS',
		}
	})
});

// 8. WTL_PRGA_PS
var WTL_PRGA_PS_Edit = new ol.layer.Vector({
	title : 'WTL_PRGA_PS_Edit',
	name: 'WTL_PRGA_PS_Edit',
	visible : false,
	source : new ol.source.Vector({
		format : new ol.format.GeoJSON(),
		url : function(extent, projection) {
			return WTL_PRGA_PS_Url +"&bbox=" + extent.join(',') + ",EPSG:5187";
		},
		strategy : ol.loadingstrategy.bbox
	})
});
var WTL_PRGA_PS_Vector = new ol.layer.Vector({
	title : 'WTL_PRGA_PS_Vec',
	name: 'WTL_PRGA_PS_Vec',
	visible : false,
	style: vectorStyle,
	source : new ol.source.Vector({
		format : new ol.format.GeoJSON(),
		url : function(extent, projection) {
			return WTL_PRGA_PS_Url +"&bbox=" + extent.join(',') + ",EPSG:5187";
		},
		strategy : ol.loadingstrategy.bbox
	})
});
var WTL_PRGA_PS_Layer = new ol.layer.Image({
	title : 'WTL_PRGA_PS',
	name: 'WTL_PRGA_PS',
	visible : false,
	source : new ol.source.ImageWMS({
		ratio : 1,
		url : gisWmsUrl,
		params : {
			'FORMAT' : 'image/png',
			'VERSION' : '1.1.1',
			STYLES : 'WTL_PRGA_PS',
			LAYERS : 'WTL_PRGA_PS',
		}
	})
});

// 9. WTL_PURI_AS
var WTL_PURI_AS_Edit = new ol.layer.Vector({
	title : 'WTL_PURI_AS_Edit',
	name: 'WTL_PURI_AS_Edit',
	visible : false,
	style: vectorStyle,
	source : new ol.source.Vector({
		format : new ol.format.GeoJSON(),
		url : function(extent, projection) {
			return WTL_PURI_AS_Url +"&bbox=" + extent.join(',') + ",EPSG:5187";
		},
		strategy : ol.loadingstrategy.bbox
	})
});
var WTL_PURI_AS_Vector = new ol.layer.Vector({
	title : 'WTL_PURI_AS_Vec',
	name: 'WTL_PURI_AS_Vec',
	visible : false,
	style: vectorStyle,
	source : new ol.source.Vector({
		format : new ol.format.GeoJSON(),
		url : function(extent, projection) {
			return WTL_PURI_AS_Url +"&bbox=" + extent.join(',') + ",EPSG:5187";
		},
		strategy : ol.loadingstrategy.bbox
	})
});
var WTL_PURI_AS_Layer = new ol.layer.Image({
	title : 'WTL_PURI_AS',
	name: 'WTL_PURI_AS',
	visible : false,
	source : new ol.source.ImageWMS({
		ratio : 1,
		url : gisWmsUrl,
		params : {
			'FORMAT' : 'image/png',
			'VERSION' : '1.1.1',
			STYLES : 'WTL_PURI_AS',
			LAYERS : 'WTL_PURI_AS',
		}
	})
});

// 10. WTL_RSRV_PS
var WTL_RSRV_PS_Edit = new ol.layer.Vector({
	title : 'WTL_RSRV_PS_Edit',
	name: 'WTL_RSRV_PS_Edit',
	visible : false,
	source : new ol.source.Vector({
		format : new ol.format.GeoJSON(),
		url : function(extent, projection) {
			return WTL_RSRV_PS_Url +"&bbox=" + extent.join(',') + ",EPSG:5187";
		},
		strategy : ol.loadingstrategy.bbox
	})
});
var WTL_RSRV_PS_Vector = new ol.layer.Vector({
	title : 'WTL_RSRV_PS_Vec',
	name: 'WTL_RSRV_PS_Vec',
	visible : false,
	style: vectorStyle,
	source : new ol.source.Vector({
		format : new ol.format.GeoJSON(),
		url : function(extent, projection) {
			return WTL_RSRV_PS_Url +"&bbox=" + extent.join(',') + ",EPSG:5187";
		},
		strategy : ol.loadingstrategy.bbox
	})
});
var WTL_RSRV_PS_Layer = new ol.layer.Image({
	title : 'WTL_RSRV_PS',
	name: 'WTL_RSRV_PS',
	visible : false,
	source : new ol.source.ImageWMS({
		ratio : 1,
		url : gisWmsUrl,
		params : {
			'FORMAT' : 'image/png',
			'VERSION' : '1.1.1',
			STYLES : 'WTL_RSRV_PS',
			LAYERS : 'WTL_RSRV_PS',
		}
	})
});

// 11. WTL_SERV_PS
var WTL_SERV_PS_Edit = new ol.layer.Vector({
	title : 'WTL_SERV_PS_Edit',
	name: 'WTL_SERV_PS_Edit',
	visible : false,
	source : new ol.source.Vector({
		format : new ol.format.GeoJSON(),
		url : function(extent, projection) {
			return WTL_SERV_PS_Url +"&bbox=" + extent.join(',') + ",EPSG:5187";
		},
		strategy : ol.loadingstrategy.bbox
	})
});
var WTL_SERV_PS_Vector = new ol.layer.Vector({
	title : 'WTL_SERV_PS_Vec',
	name: 'WTL_SERV_PS_Vec',
	visible : false,
	style: vectorStyle,
	source : new ol.source.Vector({
		format : new ol.format.GeoJSON(),
		url : function(extent, projection) {
			return WTL_SERV_PS_Url +"&bbox=" + extent.join(',') + ",EPSG:5187";
		},
		strategy : ol.loadingstrategy.bbox
	})
});
var WTL_SERV_PS_Layer = new ol.layer.Image({
	title : 'WTL_SERV_PS',
	name: 'WTL_SERV_PS',
	visible : false,
	source : new ol.source.ImageWMS({
		ratio : 1,
		url : gisWmsUrl,
		params : {
			'FORMAT' : 'image/png',
			'VERSION' : '1.1.1',
			STYLES : 'WTL_SERV_PS',
			LAYERS : 'WTL_SERV_PS',
		}
	})
});

// 12. WTL_VALV_PS
var WTL_VALV_PS_Edit = new ol.layer.Vector({
	title : 'WTL_VALV_PS_Edit',
	name: 'WTL_VALV_PS_Edit',
	visible : false,
	source : new ol.source.Vector({
		format : new ol.format.GeoJSON(),
		url : function(extent, projection) {
			return WTL_VALV_PS_Url +"&bbox=" + extent.join(',') + ",EPSG:5187";
		},
		strategy : ol.loadingstrategy.bbox
	})
});
var WTL_VALV_PS_Vector = new ol.layer.Vector({
	title : 'WTL_VALV_PS_Vec',
	name: 'WTL_VALV_PS_Vec',
	visible : false,
	style: vectorStyle,
	source : new ol.source.Vector({
		format : new ol.format.GeoJSON(),
		url : function(extent, projection) {
			return WTL_VALV_PS_Url +"&bbox=" + extent.join(',') + ",EPSG:5187";
		},
		strategy : ol.loadingstrategy.bbox
	})
});
var WTL_VALV_PS_Layer = new ol.layer.Image({
	title : 'WTL_VALV_PS',
	name: 'WTL_VALV_PS',
	visible : false,
	source : new ol.source.ImageWMS({
		ratio : 1,
		url : gisWmsUrl,
		params : {
			'FORMAT' : 'image/png',
			'VERSION' : '1.1.1',
			STYLES : 'WTL_VALV_PS',
			LAYERS : 'WTL_VALV_PS',
		}
	})
});

var WTL_VALV_PS_Layer_SA117 = new ol.layer.Image({
	title : 'WTL_VALV_PS_SA117',
	name: 'WTL_VALV_PS_SA117',
	visible : false,
	source : new ol.source.ImageWMS({
		ratio : 1,
		url : gisWmsUrl,
		params : {
			'FORMAT' : 'image/png',
			'VERSION' : '1.1.1',
			STYLES : 'WTL_VALV_PS_SA117',
			LAYERS : 'WTL_VALV_PS',
		}
	})
});
var WTL_VALV_PS_Layer_SA119 = new ol.layer.Image({
	title : 'WTL_VALV_PS_SA119',
	name: 'WTL_VALV_PS_SA119',
	visible : false,
	source : new ol.source.ImageWMS({
		ratio : 1,
		url : gisWmsUrl,
		params : {
			'FORMAT' : 'image/png',
			'VERSION' : '1.1.1',
			STYLES : 'WTL_VALV_PS_SA119',
			LAYERS : 'WTL_VALV_PS',
		}
	})
});
var WTL_VALV_PS_Layer_SA121 = new ol.layer.Image({
	title : 'WTL_VALV_PS_SA121',
	name: 'WTL_VALV_PS_SA121',
	visible : false,
	source : new ol.source.ImageWMS({
		ratio : 1,
		url : gisWmsUrl,
		params : {
			'FORMAT' : 'image/png',
			'VERSION' : '1.1.1',
			STYLES : 'WTL_VALV_PS_SA121',
			LAYERS : 'WTL_VALV_PS',
		}
	})
});
var WTL_VALV_PS_Layer_SA124 = new ol.layer.Image({
	title : 'WTL_VALV_PS_SA124',
	name: 'WTL_VALV_PS_SA124',
	visible : false,
	source : new ol.source.ImageWMS({
		ratio : 1,
		url : gisWmsUrl,
		params : {
			'FORMAT' : 'image/png',
			'VERSION' : '1.1.1',
			STYLES : 'WTL_VALV_PS_SA124',
			LAYERS : 'WTL_VALV_PS',
		}
	})
});
var WTL_VALV_PS_Layer_SA200 = new ol.layer.Image({
	title : 'WTL_VALV_PS_SA200',
	name: 'WTL_VALV_PS_SA200',
	visible : false,
	source : new ol.source.ImageWMS({
		ratio : 1,
		url : gisWmsUrl,
		params : {
			'FORMAT' : 'image/png',
			'VERSION' : '1.1.1',
			STYLES : 'WTL_VALV_PS_SA200',
			LAYERS : 'WTL_VALV_PS',
		}
	})
});
var WTL_VALV_PS_Layer_SA201 = new ol.layer.Image({
	title : 'WTL_VALV_PS_SA201',
	name: 'WTL_VALV_PS_SA201',
	visible : false,
	source : new ol.source.ImageWMS({
		ratio : 1,
		url : gisWmsUrl,
		params : {
			'FORMAT' : 'image/png',
			'VERSION' : '1.1.1',
			STYLES : 'WTL_VALV_PS_SA201',
			LAYERS : 'WTL_VALV_PS',
		}
	})
});
var WTL_VALV_PS_Layer_SA202 = new ol.layer.Image({
	title : 'WTL_VALV_PS_SA202',
	name: 'WTL_VALV_PS_SA202',
	visible : false,
	source : new ol.source.ImageWMS({
		ratio : 1,
		url : gisWmsUrl,
		params : {
			'FORMAT' : 'image/png',
			'VERSION' : '1.1.1',
			STYLES : 'WTL_VALV_PS_SA202',
			LAYERS : 'WTL_VALV_PS',
		}
	})
});
var WTL_VALV_PS_Layer_SA203 = new ol.layer.Image({
	title : 'WTL_VALV_PS_SA203',
	name: 'WTL_VALV_PS_SA203',
	visible : false,
	source : new ol.source.ImageWMS({
		ratio : 1,
		url : gisWmsUrl,
		params : {
			'FORMAT' : 'image/png',
			'VERSION' : '1.1.1',
			STYLES : 'WTL_VALV_PS_SA203',
			LAYERS : 'WTL_VALV_PS',
		}
	})
});
var WTL_VALV_PS_Layer_SA204 = new ol.layer.Image({
	title : 'WTL_VALV_PS_SA204',
	name: 'WTL_VALV_PS_SA204',
	visible : false,
	source : new ol.source.ImageWMS({
		ratio : 1,
		url : gisWmsUrl,
		params : {
			'FORMAT' : 'image/png',
			'VERSION' : '1.1.1',
			STYLES : 'WTL_VALV_PS_SA204',
			LAYERS : 'WTL_VALV_PS',
		}
	})
});
var WTL_VALV_PS_Layer_SA205 = new ol.layer.Image({
	title : 'WTL_VALV_PS_SA205',
	name: 'WTL_VALV_PS_SA205',
	visible : false,
	source : new ol.source.ImageWMS({
		ratio : 1,
		url : gisWmsUrl,
		params : {
			'FORMAT' : 'image/png',
			'VERSION' : '1.1.1',
			STYLES : 'WTL_VALV_PS_SA205',
			LAYERS : 'WTL_VALV_PS',
		}
	})
});
var WTL_VALV_PS_Layer_SA206 = new ol.layer.Image({
	title : 'WTL_VALV_PS_SA206',
	name: 'WTL_VALV_PS_SA206',
	visible : false,
	source : new ol.source.ImageWMS({
		ratio : 1,
		url : gisWmsUrl,
		params : {
			'FORMAT' : 'image/png',
			'VERSION' : '1.1.1',
			STYLES : 'WTL_VALV_PS_SA206',
			LAYERS : 'WTL_VALV_PS',
		}
	})
});
var WTL_VALV_PS_Layer_SA910 = new ol.layer.Image({
	title : 'WTL_VALV_PS_SA910',
	name: 'WTL_VALV_PS_SA910',
	visible : false,
	source : new ol.source.ImageWMS({
		ratio : 1,
		url : gisWmsUrl,
		params : {
			'FORMAT' : 'image/png',
			'VERSION' : '1.1.1',
			STYLES : 'WTL_VALV_PS_SA910',
			LAYERS : 'WTL_VALV_PS',
		}
	})
});

// 13. WTL_WPMP_AS
var WTL_WPMP_AS_Edit = new ol.layer.Vector({
	title : 'WTL_WPMP_AS_Edit',
	name: 'WTL_WPMP_AS_Edit',
	visible : false,
	source : new ol.source.Vector({
		format : new ol.format.GeoJSON(),
		url : function(extent, projection) {
			return WTL_WPMP_AS_Url +"&bbox=" + extent.join(',') + ",EPSG:5187";
		},
		strategy : ol.loadingstrategy.bbox
	})
});
var WTL_WPMP_AS_Vector = new ol.layer.Vector({
	title : 'WTL_WPMP_AS_Vec',
	name: 'WTL_WPMP_AS_Vec',
	visible : false,
	style: vectorStyle,
	source : new ol.source.Vector({
		format : new ol.format.GeoJSON(),
		url : function(extent, projection) {
			return WTL_WPMP_AS_Url +"&bbox=" + extent.join(',') + ",EPSG:5187";
		},
		strategy : ol.loadingstrategy.bbox
	})
});
var WTL_WPMP_AS_Layer = new ol.layer.Image({ 
	title : 'WTL_WPMP_AS',
	name: 'WTL_WPMP_AS',
	visible : false,
	source : new ol.source.ImageWMS({
		ratio : 1,
		url : gisWmsUrl,
		params : {
			'FORMAT' : 'image/png',
			'VERSION' : '1.1.1',
			STYLES : 'WTL_WPMP_AS',
			LAYERS : 'WTL_WPMP_AS',
		}
	})
});
//16. WTL_SPLY_LS
var WTL_SPLY_LS_Edit = new ol.layer.Vector({
	title : 'WTL_SPLY_LS_Edit',
	name: 'WTL_SPLY_LS_Edit',
	visible : false,
	source : new ol.source.Vector({
		format : new ol.format.GeoJSON(),
		url : function(extent, projection) {
			return WTL_SPLY_LS_Url +"&bbox=" + extent.join(',') + ",EPSG:5187";
		},
		strategy : ol.loadingstrategy.bbox
	})
});
var WTL_SPLY_LS_Vector = new ol.layer.Vector({
	title : 'WTL_SPLY_LS_Vec',
	name: 'WTL_SPLY_LS_Vec',
	visible : false,
	style: vectorStyle,
	source : new ol.source.Vector({
		format : new ol.format.GeoJSON(),
		url : function(extent, projection) {
			return WTL_SPLY_LS_Url +"&bbox=" + extent.join(',') + ",EPSG:5187";
		},
		strategy : ol.loadingstrategy.bbox
	})
});
var WTL_SPLY_LS_Layer = new ol.layer.Image({
	title : 'WTL_SPLY_LS',
	name: 'WTL_SPLY_LS',
	visible : false,
	source : new ol.source.ImageWMS({
		ratio : 1,
		url : gisWmsUrl,
		params : {
			'FORMAT' : 'image/png',
			'VERSION' : '1.1.1',
			STYLES : 'WTL_SPLY_LS',
			LAYERS : 'WTL_SPLY_LS',
		}
	})
});