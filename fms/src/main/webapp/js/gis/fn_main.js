/**
 * GIS 연동처리 스크립트 
 */

var editSaveYN;		// 편집여부 (편집시작=N, 무효/저장=Y)

//시설물위치등록 커서활성화
var extent = new ol.interaction.Extent({
    condition: ol.events.condition.platformModifierKeyOnly
});
















/***************************************************
	시설물상세 정보레이어 관련 
***************************************************/

/**
 * 
 * wtype = 영역코드4자리 소문자
 * ftrcde = 지형지물 코드
 * vcode = 관리번호 or OGRFID or G2ID
 * 
 * ex) openInfoView("pipe", "SA001", "52205");
*/
var openInfoView = function(wtype, ftrcde, vcode) {
	var centerPos = parseInt( $(screen).get(0).width ) / 2;
	var windowCenterPos = 858 / 2;
	var leftPos = centerPos - windowCenterPos;
	var ctxRoot = "/fms";
	
	/*상수관로*/
	if(wtype=="pipe") {
		try{ closeChild(); }catch(e){}
		var _win = window.open(ctxRoot+"/pms/pipen/wtpipMngDtl.do?ftrCde="+ftrcde+"&ftrIdn="+vcode, "view", "left="+leftPos+",top=100, width=1100, height=680, menubar=no, location=no, resizable=no, toolbar=no, status=no");
		_win.focus();
		try{ _winChild = _win; }catch(e){}
	} 
	
	/*변류시설*/
	if(wtype=="valv") {		
		try{ closeChild(); }catch(e){}
		var _win = window.open(ctxRoot+"/pms/pipen/valvFac/valvFacView.do?ftrCde="+ftrcde+"&ftrIdn="+vcode, "view", "left="+leftPos+",top=100, width=1100, height=720, menubar=no, location=no, resizable=no, toolbar=no, status=no");
		_win.focus();
		try{ _winChild = _win; }catch(e){}
	} 
	
	/*소방시설*/
	if(wtype=="fire") {		
		try{ closeChild(); }catch(e){}
		var _win = window.open(ctxRoot+"/pms/pipen/fireFac/fireFacView.do?ftrCde="+ftrcde+"&ftrIdn="+vcode, "view", "left="+leftPos+",top=100, width=1100, height=607, menubar=no, location=no, resizable=no, toolbar=no, status=no");
		_win.focus();
		try{ _winChild = _win; }catch(e){}
	} 
	
	/*상수맨홀*/
	if(wtype=="manh") {		
		try{ closeChild(); }catch(e){}
		var _win = window.open(ctxRoot+"/pms/pipen/wtsmnho/wtsMnhoView.do?ftrCde="+ftrcde+"&ftrIdn="+vcode, "view", "left="+leftPos+",top=100, width=1100, height=547, menubar=no, location=no, resizable=no, toolbar=no, status=no");
		_win.focus();
		try{ _winChild = _win; }catch(e){}
	} 
	
	/*유량계*/
	if(wtype=="flow") {		
		try{ closeChild(); }catch(e){}
		var _win = window.open(ctxRoot+"/pms/pipen/flowMt/flowMtView.do?ftrCde="+ftrcde+"&ftrIdn="+vcode, "view", "left="+leftPos+",top=100, width=1100, height=606, menubar=no, location=no, resizable=no, toolbar=no, status=no");
		_win.focus();
		try{ _winChild = _win; }catch(e){}
	} 
	
	/*스탠드파이프*/
	if(wtype=="stpi") {		
		try{ closeChild(); }catch(e){}
		var _win = window.open(ctxRoot+"/pms/pipen/stndPi/stndPiView.do?ftrCde="+ftrcde+"&ftrIdn="+vcode, "view", "left="+leftPos+",top=100, width=1100, height=575, menubar=no, location=no, resizable=no, toolbar=no, status=no");
		_win.focus();
		try{ _winChild = _win; }catch(e){}
	} 
	
	/*수압계*/
	if(wtype=="prga") {		
		try{ closeChild(); }catch(e){}
		var _win = window.open(ctxRoot+"/pms/pipen/wtprMt/wtprMtView.do??ftrCde="+ftrcde+"&ftrIdn="+vcode, "view", "left="+leftPos+",top=100, width=1100, height=623, menubar=no, location=no, resizable=no, toolbar=no, status=no");
		_win.focus();
		try{ _winChild = _win; }catch(e){}
	} 
	
	/*수원지*/
	if(wtype=="head") {		
		try{ closeChild(); }catch(e){}
		var _win = window.open(ctxRoot+"/pms/wtsFclt/wtrSour/wtrSourView.do?ftrCde="+ftrcde+"&ftrIdn="+vcode, "view", "left="+leftPos+",top=100, width=1100, height=650, menubar=no, location=no, resizable=no, toolbar=no, status=no");
		_win.focus();
		try{ _winChild = _win; }catch(e){}
	} 
	
	/*취수장*/
	if(wtype=="gain") {		
		try{ closeChild(); }catch(e){}
		var _win = window.open(ctxRoot+"/pms/wtsFclt/intkSt/intkStView.do?ftrCde="+ftrcde+"&ftrIdn="+vcode, "view", "left="+leftPos+",top=100, width=1100, height=628, menubar=no, location=no, resizable=no, toolbar=no, status=no");
		_win.focus();
		try{ _winChild = _win; }catch(e){}
	} 
	
	/*배수지*/
	if(wtype=="serv") {		
		try{ closeChild(); }catch(e){}
		var _win = window.open(ctxRoot+"/fms/pms/wtsFclt/wtrSup/wtrSupView.do?ftrCde="+ftrcde+"&ftrIdn="+vcode, "view", "left="+leftPos+",top=100, width=1100, height=626, menubar=no, location=no, resizable=no, toolbar=no, status=no");
		_win.focus();
		try{ _winChild = _win; }catch(e){}
	} 
	
	/*정수장*/
	if(wtype=="puri") {		
		try{ closeChild(); }catch(e){}
		var _win = window.open(ctxRoot+"/pms/wtsFclt/filtPlt/filtPltView.do?ftrCde="+ftrcde+"&ftrIdn="+vcode, "view", "left="+leftPos+",top=100, width=1100, height=600, menubar=no, location=no, resizable=no, toolbar=no, status=no");
		_win.focus();
		try{ _winChild = _win; }catch(e){}
	} 
	
	/*가압펌프장*/
	if(wtype=="pres") {		
		try{ closeChild(); }catch(e){}
		var _win = window.open(ctxRoot+"/pms/wtsFclt/prsPmp/prsPmpView.do?ftrCde="+ftrcde+"&ftrIdn="+vcode, "view", "left="+leftPos+",top=100, width=1100, height=600, menubar=no, location=no, resizable=no, toolbar=no, status=no");
		_win.focus();
		try{ _winChild = _win; }catch(e){}
	} 
	
	/*급수관로*/
	if(wtype=="sply") {		
		try{ closeChild(); }catch(e){}
		var _win = window.open(ctxRoot+"/sply/splyLsView.do?ftrCde="+ftrcde+"&ftrIdn="+vcode, "view", "left="+leftPos+",top=100, width=1100, height=642, menubar=no, location=no, resizable=no, toolbar=no, status=no");
		_win.focus();
		try{ _winChild = _win; }catch(e){}
	} 
	
	/*급수전계량기*/
	if(wtype=="meta") {		
		try{ closeChild(); }catch(e){}
		var _win = window.open(ctxRoot+"/pms/acmFclt/supDut/supDutView.do?ftrCde="+ftrcde+"&ftrIdn="+vcode, "view", "left="+leftPos+",top=100, width=1100, height=650, menubar=no, location=no, resizable=no, toolbar=no, status=no");
		_win.focus();
		try{ _winChild = _win; }catch(e){}
	} 
	
	/*저수조*/
	if(wtype=="rsrv") {		
		try{ closeChild(); }catch(e){}
		var _win = window.open(ctxRoot+"/pms/acmFclt/wtrTrk/wtrTrkView.do?ftrCde="+ftrcde+"&ftrIdn="+vcode, "view", "left="+leftPos+",top=100, width=1100, height=646, menubar=no, location=no, resizable=no, toolbar=no, status=no");
		_win.focus();
		try{ _winChild = _win; }catch(e){}
	}
	
};










/***************************************************
	편집레이어 관련
***************************************************/

//편집레이어 활성화 처리
function setEditLayer(eLayer,ftrCde,type, obj) {
	var zoom = map.getView().getZoom(); 
	if(zoom <= 14) {
		alert("현재 레벨에서는 레이어를 활성화 할 수 없습니다. \n (15레벨 부터 활성화) 현재레벨 : " + zoom);
		$('.layer-facility-list>ul>li').removeClass('on');
		//$('.layer-facility-list>ul>li').addClass('off');
	} else {
		if (editSaveYN == "N") {
			alert("이전레이어 작업내역을 저장하신 후 다른레이어를 작업하시기 바랍니다.");
			return;
		}
		// 이전 벡터 지우기
		map.getLayers().forEach(function (layer) {
			var layerNm = layer.get('name');
			if ( layerNm != undefined ) {
				if (layerNm.indexOf("_Vec") > -1) {
					layer.setVisible(false);
				}
			}
		}); 

		//이전레이어
		var layerNm = $('input[name="layerNm"]').val();
		
		
		// 작업대상 저장
		$('input[name="layerNm"]').val(eLayer);
		$('input[name="ftrCde"]').val(ftrCde);
		$('input[name="editType"]').val(type);
		

		// 레이어선택 토글처리
		$(".layer-facility-list>ul>li").not($(obj)).removeClass('on');
		$(obj).toggleClass('on');

		// 벡터 생성(newName, oldName)
		editLayerSwitch(eLayer,layerNm);
	}
}





/**
 * 편집레이어 변경
 */
function editLayerSwitch(name,backName){

	if(!event.cancelBubble) { //중복클릭 방지
		
		// 첫 클릭 or 다른레이어 클릭
		if(name != backName) {
			map.getLayers().forEach(function (layer) {
				
				// 이전 레이어 Off
				if ( layer.get('name') == backName ) {
					layer.setVisible(false);
				}
				
				if (layer.get('name') == name) {
					layer.setVisible(true);
				}
			});
		}

		// 레이어 off
		else {
			map.getLayers().forEach(function (layer) {
				if (layer.get('name') == name) {
					
					var visibility = layer.getVisible();
					if (visibility == false) {
						layer.setVisible(true);
					}
					else if (visibility == true) {
						layer.setVisible(false);
						
						//편집정보초기화
						$(".layer-facility-list>ul>li").removeClass('on');
						editSaveYN = "Y";
						$("input[name='layerNm']").val("");
						$("input[name='ftrCde']").val("");
						$("input[name='ftrIdn']").val("");
						$("input[name='coordinates']").val("");
						$("input[name='editType']").val("");
						$("input[name='insertYN']").val("N");
					}
				}
			});
		}
		event.stopPropagation(); ///
		event.cancelBubble = true; ///
	}
}


/**
 * 편집저장(시설물 위치등록, 위치변경) 
 */
var geomSave = function() {
	editSaveYN = "Y";
	removeInteraction();
	// 그리기한 피처 초기화
	drawFeatures.clear();
	// 저장 컨트롤 호출하기
	//$('#editSave').submit();
	
	$.ajax({
		url : webPath + "/map/updateGeom.do",
		type : 'POST',
		dataType : 'JSON',
		data : {
			layerNm : $('#layerNm').val(),
			ftrCde : $('#ftrCde').val(),
			ftrIdn : $('#ftrIdn').val(),
			coordinates : $('#coordinates').val(),
			insertYn : $('input[name="insertYN"]').val()
		},
		success : function(res) {
			if (res.data.result == 1) {
				reset();
				alert("저장하였습니다.");
			} else {
				alert("저장에 실패 하였습니다.");
			}
		},
		error : function() {
			alert("트랜잭션 오류가 발생하였습니다.");
		}
		
	});
	
}




/**
 * 편집취소
 */
function reset() {
	
	var layerNm = $('input[name="layerNm"]').val();

	map.getLayers().forEach(function (layer) {

		if (layer.get('name') == layerNm) {
			layer.setVisible(false);
			
			try{
				layer.getSource().clear();
			}catch(e){}
		}
	});

	//편집정보초기화
	$(".layer-facility-list>ul>li").removeClass('on');
	editSaveYN = "Y";
	$("input[name='layerNm']").val("");
	$("input[name='ftrCde']").val("");
	$("input[name='ftrIdn']").val("");
	$("input[name='coordinates']").val("");
	$("input[name='editType']").val("");
	$("input[name='insertYN']").val("N");
	
	
	//커서비활성화
	extent.setActive(false);
	map.removeInteraction(extent);
	
	// 그리기한 피처 초기화
	drawFeatures.clear();
	
	removeInteraction();
	
	
}





//시설물 대장등록
/*[상수관망] 7
PIPE_LM 상수관로 
VALV_PS 변류시설 
FIRE_PS 소방시설
MANH_PS 상수맨홀
FLOW_PS 유량계
STPI_PS 스탠드파이프 - 레이어 없음.
PRGA_PS 수압계
[상수부속시설] 5
HEAD_PS 수원지
GAIN_PS 취수장
SERV_PS 배수지
PURI_AS 정수장
PRES_PS 가압펌프장
[수용가시설] 3
SPLY_LS 급수관로
META_PS 급수전계량기
RSRV_PS 저수조 */
function waterPipeView() {
	
	var href, title, size;
	var layerNm = $('input[name="layerNm"]').val();
	var ftrCde = $('input[name="ftrCde"]').val();
	
	if(gfn_isNull(layerNm)){
		alert("시설물을 선택해주세요.");
		return;
	}
	
	if (layerNm == "WTL_PIPE_LM_Edit") {
		title = "상수관로등록";
		// lmList_popup.jsp 파일 이름이 제각각이라 찾기 어려움. 왜 통일을 안하는지 모르겠네요.
		href = webPath+'/pms/pipen/pipeLmCreate.do?type=edit';
		// 업무단에 하드코딩 되어있음. 
		size = "width=1100, height=350";
	} else if (layerNm == "WTL_VALV_PS_Edit") {
		title = "변류시설등록";
		href = webPath+'/pms/pipen/valvFac/valvFacCreate.do?type=edit';
		// 업무단에 하드코딩 되어있음.
		size = "width=1100, height=400";
	} else if (layerNm == "WTL_FIRE_PS_Edit") {
		title = "소방시설등록";						// firePsList
		href = webPath+'/pms/pipen/fireFac/fireFacCreate.do?type=edit';
		// 업무단에 하드코딩 되어있음.
		size = "width=1100, height=345";
	} else if (layerNm == "WTL_MANH_PS_Edit") {
		title = "상수맨홀등록";						
		href = webPath+'/pms/pipen/wtsmnho/wtsMnhoCreate.do?type=edit';
		// 업무단에 하드코딩 되어있음.
		size = "width=1100, height=323";
	} else if (layerNm == "WTL_FLOW_PS_Edit") {
		title = "유량계등록";
		href = webPath+'/pms/pipen/flowMt/flowMtCreate.do?type=edit';
		// 업무단에 하드코딩 되어있음.
		size = "width=1100, height=340";
	} else if (layerNm == "WTL_STPI_PS_Edit") {
		title = "스탠드파이프등록";						// prgaPsList
		href = webPath+'/pms/pipen/stndPi/stndPiCreate.do?type=edit';
		// 업무단에 하드코딩 되어있음.
		size = "width=1100, height=349";						
	} else if (layerNm == "WTL_PRGA_PS_Edit") {
		title = "수압계등록";						// prgaPsList
		href = webPath+'/pms/pipen/wtprMt/wtprMtCreate.do?type=edit';
		// 업무단에 하드코딩 되어있음.
		size = "width=1100, height=370";						// 상수관망 End (6)
	} else if (layerNm == "WTL_HEAD_PS_Edit") {
		title = "수원지등록";						// psList
		href = webPath+'/pms/wtsFclt/wtrSour/wtrSourCreate.do?type=edit';
		// 업무단에 하드코딩 되어있음.
		size = "width=1100, height=400	";
	} else if (layerNm == "WTL_GAIN_PS_Edit") {
		title = "취수장등록";
		href = webPath+'/pms/wtsFclt/intkSt/intkStCreate.do?type=edit';
		// 업무단에 하드코딩 되어있음.
		size = "width=1100, height=401";
	} else if (layerNm == "WTL_SERV_PS_Edit") {
		title = "배수지등록";
		href = webPath+'/pms/wtsFclt/wtrSup/wtrSupCreate.do?type=edit';
		// 업무단에 하드코딩 되어있음.
		size = "width=1100, height=380";
	} else if (layerNm == "WTL_PURI_AS_Edit") {
		title = "정수장등록";
		href = webPath+'/pms/wtsFclt/filtPlt/filtPltCreate.do?type=edit';
		// 업무단에 하드코딩 되어있음.
		size = "width=1100, height=360";
	} else if (layerNm == "WTL_PRES_PS_Edit") {
		title = "가압펌프장등록";
		href = webPath+'/pms/wtsFclt/prsPmp/prsPmpCreate.do?type=edit';
		// 업무단에 하드코딩 되어있음.
		size = "width=1100, height=330";						// 상수부속시설 End (5)
	} else if (layerNm == "WTL_SPLY_LS_Edit") {
		title = "급수관로등록";
		href = webPath+'/pms/acmFclt/supDut/supDutCreate.do?type=edit';
		// 업무단에 하드코딩 되어있음.
		size = "width=1100, height=380";
	} else if (layerNm == "WTL_META_PS_Edit") {
		title = "급수전계량기등록";
		href = webPath+'/pms/acmFclt/hydtMetr/hydtMetrCreate.do?type=edit';
		// 업무단에 하드코딩 되어있음.
		size = "width=1100, height=370";
	} else if (layerNm == "WTL_RSRV_PS_Edit") {
		title = "저수조등록";
		href = webPath+'/pms/acmFclt/wtrTrk/wtrTrkCreate.do?type=edit';
		// 업무단에 하드코딩 되어있음.
		size = "width=1100, height=380";						// 수용가시설 End (3)
	}
	
	
	href += "&layerNm="+layerNm+"&ftrCde="+ftrCde;
	
	//e.preventDefault();
	var centerPos = parseInt( $(screen).get(0).width ) / 2;
	var windowCenterPos = 858 / 2;
	var leftPos = centerPos - windowCenterPos;
	
	var _win = window.open(href, title, "left="+leftPos+",top=100," + size + ", menubar=no, location=no, resizable=no, toolbar=no, status=no");
	_win.focus();
	
}




/**
 * 위치등록
 */
function waterPipeList() {
	var layerNm = $('input[name="layerNm"]').val();
	if (layerNm=='WTL_FIRE_PS_Edit') {
		window.open('${ctxRoot}/fire/firePsList.do?gisLayerNm=WTL_FIRE_PS', "_blank", 'status=no, width=1100, height=530, left='+ popupX + ', top='+ popupY + ', screenX='+ popupX + ', screenY= '+ popupY);
	} else if (layerNm=='WTL_FLOW_PS_Edit') {
		window.open('${ctxRoot}/flow/flowPsList.do?gisLayerNm=WTL_FLOW_PS', "_blank", 'status=no, width=1100, height=530, left='+ popupX + ', top='+ popupY + ', screenX='+ popupX + ', screenY= '+ popupY);
	} else if (layerNm=='WTL_GAIN_PS_Edit') {
		window.open('${ctxRoot}/gain/gainPsList.do?gisLayerNm=WTL_GAIN_PS', "_blank", 'status=no, width=1100, height=530, left='+ popupX + ', top='+ popupY + ', screenX='+ popupX + ', screenY= '+ popupY);
	} else if (layerNm=='WTL_HEAD_PS_Edit') {
		window.open('${ctxRoot}/head/headPsList.do?gisLayerNm=WTL_HEAD_PS', "_blank", 'status=no, width=1100, height=530, left='+ popupX + ', top='+ popupY + ', screenX='+ popupX + ', screenY= '+ popupY);
	} else if (layerNm=='WTL_MANH_PS_Edit') {
		window.open('${ctxRoot}/pms/pipen/wtsmnho/wtsMnhoList.do?gisLayerNm=WTL_MANH_PS', "_blank", 'status=no, width=1100, height=530, left='+ popupX + ', top='+ popupY + ', screenX='+ popupX + ', screenY= '+ popupY);
	} else if (layerNm=='WTL_META_PS_Edit') {
		window.open('${ctxRoot}/meta/metaPsList.do?gisLayerNm=WTL_META_PS', "_blank", 'status=no, width=1100, height=530, left='+ popupX + ', top='+ popupY + ', screenX='+ popupX + ', screenY= '+ popupY);
	} else if (layerNm=='WTL_PIPE_LM_Edit') {
		window.open('${ctxRoot}/pipe/pipeLmList.do?gisLayerNm=WTL_PIPE_LM', "_blank", 'status=no, width=1100, height=530, left='+ popupX + ', top='+ popupY + ', screenX='+ popupX + ', screenY= '+ popupY);
	} else if (layerNm=='WTL_PRES_PS_Edit') {
		window.open('${ctxRoot}/pres/presPsList.do?gisLayerNm=WTL_PRES_PS', "_blank", 'status=no, width=1100, height=530, left='+ popupX + ', top='+ popupY + ', screenX='+ popupX + ', screenY= '+ popupY);
	} else if (layerNm=='WTL_PRGA_PS_Edit') {
		window.open('${ctxRoot}/prga/prgaPsList.do?gisLayerNm=WTL_PRGA_PS', "_blank", 'status=no, width=1100, height=530, left='+ popupX + ', top='+ popupY + ', screenX='+ popupX + ', screenY= '+ popupY);
	} else if (layerNm=='WTL_PURI_AS_Edit') {
		window.open('${ctxRoot}/puri/puriAsList.do?gisLayerNm=WTL_PURI_AS', "_blank", 'status=no, width=1100, height=530, left='+ popupX + ', top='+ popupY + ', screenX='+ popupX + ', screenY= '+ popupY);
	} else if (layerNm=='WTL_RSRV_PS_Edit') {
		window.open('${ctxRoot}/rsrv/rsrvPsList.do?gisLayerNm=WTL_RSRV_PS', "_blank", 'status=no, width=1100, height=530, left='+ popupX + ', top='+ popupY + ', screenX='+ popupX + ', screenY= '+ popupY);
	} else if (layerNm=='WTL_SERV_PS_Edit') {
		window.open('${ctxRoot}/serv/servPsList.do?gisLayerNm=WTL_SERV_PS', "_blank", 'status=no, width=1100, height=530, left='+ popupX + ', top='+ popupY + ', screenX='+ popupX + ', screenY= '+ popupY);
	} else if (layerNm=='WTL_SPLY_LS_Edit') {
		window.open('${ctxRoot}/sply/splyLsList.do?gisLayerNm=WTL_SPLY_LS', "_blank", 'status=no, width=1100, height=530, left='+ popupX + ', top='+ popupY + ', screenX='+ popupX + ', screenY= '+ popupY);
	} else if (layerNm=='WTL_VALV_PS_Edit') {
		window.open('${ctxRoot}/valv/valvPsList.do?gisLayerNm=WTL_VALV_PS', "_blank", 'status=no, width=1100, height=530, left='+ popupX + ', top='+ popupY + ', screenX='+ popupX + ', screenY= '+ popupY);
	}
	
	/* if (layerNm=='WTL_FLOW_PS_Edit') {
		window.open('${ctxRoot}/flow/flowPsList.do?gisLayerNm=WTL_FLOW_PS', "_blank", 'status=no, width=861, height=549, left='+ popupX + ', top='+ popupY + ', screenX='+ popupX + ', screenY= '+ popupY);
	} else if (layerNm=='WTL_PURI_AS_Edit') {
		window.open('${ctxRoot}/puri/puriAsList.do?gisLayerNm=WTL_PURI_AS', "_blank", 'status=no, width=861, height=549, left='+ popupX + ', top='+ popupY + ', screenX='+ popupX + ', screenY= '+ popupY);
	} else if (layerNm=='WTL_PIPE_LM_Edit') {
		window.open('${ctxRoot}/pipe/pipeLmList.do?gisLayerNm=WTL_PIPE_LM', "_blank", 'status=no, width=861, height=549, left='+ popupX + ', top='+ popupY + ', screenX='+ popupX + ', screenY= '+ popupY);
	} */
}
	







