
// 선택된 점검시설결과 항목


//보수구분
var RPR_CAT_CDE_LIST = [];
//보수사유구분
var RPR_CUZ_CDE_LIST = [];
//소모품 코드
var PDH_LIST = [];
var PDH_LIST2 = [];

var lst = []; //그리드데이터 - 점검결과
var lst2 = []; //그리드데이터 - 소모품
var lst3 = []; //그리드데이터 - 주유량

var ftrObj = {}; //점검결과 시설물 선택 리턴데이터


//점검시설결과 그리드 객체
var fnObj = {
        pageStart: function () {
            fnObj.grid.bind();
        },
        grid: {
            target: new AXGrid(),
            bind: function () {
                window.myGrid = fnObj.grid.target;

                myGrid.setConfig({
                        targetID: "grdResult",
                        fitToWidth: true,
                        sort: false,
                        //height:"auto",
                        //fixedColSeq: 3,
                        passiveMode:true,	//그리드에 데이터 그대로 남겨놓기
                        passiveRemoveHide:false,                            
                        colGroup: [
                            {	key: "no", label: "선택", width: "80", align: "center", formatter: "checkbox", formatterLabel: " 전체"
                            },
                            {	key: "status", label: "상태", width: "40", align: "center"
                            	, formatter: function(){
                            		
                            		if(this.item._CUD == "C"){
                                        return '<img width="20px" height="20px" src="/fms/images/gridAdd.png"></img>' ;
                                    }else if(this.item._CUD == "D"){
                                        return '<img width="20px" height="20px" src="/fms/images/gridDel.png"></img>' ;
                                    }
                            	}
                        	},
                            {	key: "ftrNam", label: "지형지물명", width: "150", align: "center",
                        		formatter: function () {
                        			return ('<div style="width:100px;float:left;">'+this.item.ftrNam+'&nbsp;</div> <img src="/fms/images/icon_co_edit.png" onclick="fn_SelFtrPop();" width="16" height="16" />');
                        		},
                                validate : function(){
                                	if(gfn_isNull(this.value)){
                                		alert("지형지물명은 필수 입력 항목 입니다." );
                                		return false;
                                	} else {
                                		return true;
                                	}
                                },    
                                afterUpdate: function (val) { // 수정이 처리된 후
                                	try{
                                		this.item.ftrIdn = ftrObj.ftrIdn; 
                                		myGrid.dataSync();
                                	}catch(e){}
                                }
                                
                        	},
                            {	key: "ftrIdn", label: "관리번호", width: "150", align: "center"},
                            {	key: "hjdNam", label: "행정읍/면/동", width: "200", align: "left"},
                            {
                            	key: "rprYmd", label: "보수일자", width: "150", align:"center"
    							,editor: {
                                    type: "calendar",
                                    config: {
                                        separator: "-"
                                    },
                                    updateWith: ["_CUD"]
                                }
                            },
                            {
                                key: "rprCatCde", label: "보수구분", width: "100", align: "center", 
                                editor: {
                                    type: "select",
                                    optionValue: "dtlCd",
                                    optionText: "cdNm",
                                    options: RPR_CAT_CDE_LIST,
                                    
                                    beforeUpdate: function (val) { // 수정이 되기전 value를 처리 할 수 있음.
                                        return val;
                                    },
                                    afterUpdate: function (val) { // 수정이 처리된 후
                                    }
                                }
                            },
                            {
                                key: "rprCuzCde", label: "보수사유", width: "150", align: "center", 
                                editor: {
                                    type: "select",
                                    optionValue: "dtlCd",
                                    optionText: "cdNm",
                                    options: RPR_CUZ_CDE_LIST,
                                    
                                    beforeUpdate: function (val) { // 수정이 되기전 value를 처리 할 수 있음.
                                        return val;
                                    },
                                    afterUpdate: function (val) { // 수정이 처리된 후
                                    }
                                }
                            },
                            {	key: "sclNum", label: "", width: "50", display:false},
                            {	key: "seq", label: "", width: "50", display:false},
                        ],
                        colHeadAlign: "center", // 헤드의 기본 정렬 값 ( colHeadAlign 을 지정하면 colGroup 에서 정의한 정렬이 무시되고 colHeadAlign : false 이거나 없으면 colGroup 에서 정의한 속성이 적용됩니다.
                        body: {
                            onclick: function(){
                            	//선택row 전역변수로 보유하기
                            	cur_row = this.index;
                            	cur_ftrCde = this.item.ftrCde;	
                            	cur_ftrIdn = this.item.ftrIdn;	
                            	cur_seq = this.item.seq;
                            	cur_filSeq = this.item.filSeq;
                            	//해당 서브항목 재조회
                                fn_setRstDtl(cur_row); 
                            },
                            oncheck: function(){
                            	try{
                            		if(!this.item.___checked[0]){ //삭제풀면 상태원복
                            			this.item._CUD = "U";
                            		}
                            		myGrid.dataSync();
                            	}catch(e){
                            		//전체체크 처리
                            	}
                            },                                      
                            addClass: function(){
                            },
                            onchangeScroll: function(){
                                //console.log(this);
//                                for(var i=this.startIndex;i<this.endIndex+1;i++){
//                                    $("#AXdate_"+i).bindDate({align:"right", valign:"top",
//                                        onchange:function(){
//                                            console.log(this.objID);
//                                            var myi = this.objID.substr(this.objID.lastIndexOf("_").number()+1);
//                                            myGrid.list[myi].rprYmd = this.value;
//                                        }
//                                    });
//                                }
                            },
                        }
                        ,
                        page: {
                            paging: false
                        },
                        onkeyup: function(event, element){
                            //this.list
                            //this.item
                            //this.element
                        },                        
                    }
            );

            myGrid.setList(lst);
            //console.log(myGrid.getSortParam());
            try{
            	myGrid.click(0);
            }catch(e){};

        },
        
        getList: function () {
            //console.log(this.target.getList());
            return this.target.getList();
        },
        getSelectedItem: function () {
            console.log(this.target.getSelectedItem());
            //toast.push('콘솔창에 데이터를 출력하였습니다.');
        }
        ,
        append: function () {
        	this.target.pushList(
        			{
        				no: false,
        				ftrNam: '',
        				rprCatCde: '',
        				rprCuzCde: '',
        				rprYmd: '',
        				sclNum: sclNum,
        				seq: '',
        			}
        	);
        	this.target.setFocus(this.target.list.length - 1);
        	
        }
        ,
        remove: function () {
            var checkedList = myGrid.getCheckedListWithIndex(0);// colSeq
            if (checkedList.length == 0) {
                alert("선택된 목록이 없습니다. 삭제하시려는 목록을 체크하세요");
                return;
            }
            this.target.removeListIndex(checkedList);
            // 전달한 개체와 비교하여 일치하는 대상을 제거 합니다. 이때 고유한 값이 아닌 항목을 전달 할 때에는 에러가 발생 할 수 있습니다.
            
            
        }
    }
};





//소모품사용 그리드 객체
var fnObj2 = {
        pageStart: function () {
            fnObj2.grid.bind();
        },
        grid: {
            target: new AXGrid(),
            bind: function () {
                window.myGrid2 = fnObj2.grid.target;

                myGrid2.setConfig({
						targetID: "grdPdh",
                        fitToWidth: true,
                        sort: false,
                        //height:"auto",
                        //fixedColSeq: 3,
                        passiveMode:true,	//그리드에 데이터 그대로 남겨놓기
                        passiveRemoveHide:false,                            
                        colGroup: [
                            {	key: "no", label: "선택", width: "80", align: "center", formatter: "checkbox", formatterLabel: " 전체"
                            },
                            {	key: "status", label: "상태", width: "40", align: "center"
                            	, formatter: function(){
                            		
                            		if(this.item._CUD == "C"){
                                        return '<img width="20px" height="20px" src="/fms/images/gridAdd.png"></img>' ;
                                    }else if(this.item._CUD == "D"){
                                        return '<img width="20px" height="20px" src="/fms/images/gridDel.png"></img>' ;
                                    }
                            	}
                        	},
                            {
                                key: "pdhNum", label: "소모품명", width: "200", align: "center",
                                editor: {
                                    type: "select",
                                    optionValue: "pdhNum",
                                    optionText: "pdtNam",
                                    options: PDH_LIST,
                                    
                                    beforeUpdate: function (val) { // 수정이 되기전 value를 처리 할 수 있음.
                                        return val;
                                    },
                                    afterUpdate: function (val) { // 수정이 처리된 후
                                    	
                                    	try{
                                    		var obj = gfn_findData(PDH_LIST,{colNm: 'pdhNum', value: val})
                                    		this.item.pdtMdlStd = obj.pdtMdlStd; 
                                    		myGrid2.dataSync();
                                    	}catch(e){}
                                    }
                                }
                            },
                            {	key: "pdtMdlStd", label: "모델/규격", width: "200", align: "left"},
                            {
                                key: "pdhCnt", label: "사용개수", width: "150", align: "right",
                                formatter: "money",
                                editor: {
                                    type: "number",
                                    updateWith: ["_CUD"],
                                    range: { // 숫자일 경우 숫자자릿수와 소수점 자릿수 지정
                                        val: "2,1"
                                        ,msg : '숫자일 경우 숫자자릿수와 소수점 자릿수 지정' // 자릿수를 초과 했을대 보여줄 메시지.
                                    }
                                },
                                validate : function(){
                                	if(gfn_isNull(this.value)){
                                		alert("사용개수는 필수 입력 항목 입니다.");
                                		return false;
                                	} else {
                                		return true;
                                	}
                                }    

                            
                            },
                            {	key: "pdhHtNum", label: "", width: "50", display:false},
                            {	key: "sclNum", label: "", width: "50", display:false},
                            {	key: "seq", label: "", width: "50", display:false},
                            {	key: "ftrCde", label: "", width: "50", display:false},
                            {	key: "ftrIdn", label: "", width: "50", display:false},
                            {	key: "pdtCatCde", label: "", width: "50", display:false},
                        ],
                        colHeadAlign: "center", // 헤드의 기본 정렬 값 ( colHeadAlign 을 지정하면 colGroup 에서 정의한 정렬이 무시되고 colHeadAlign : false 이거나 없으면 colGroup 에서 정의한 속성이 적용됩니다.
                        body: {
                            onclick: function(){
                                //toast.push(Object.toJSON({index:this.index, r:this.r, c:this.c, item:this.item}));
                            },
                            oncheck: function(){
                            	try{
                            		if(!this.item.___checked[0]){ //삭제풀면 상태원복
                            			this.item._CUD = "U";
                            		}
                            		myGrid2.dataSync();
                            	}catch(e){
                            		//전체체크 처리
                            	}
                            },                                      
                            addClass: function(){
                            },
                            onchangeScroll: function(){
                                //console.log(this);
                            },
                        }
                        ,
                        page: {
                            paging: false
                        },
                        onkeyup: function(event, element){
                            //this.list
                            //this.item
                            //this.element
                        },
                    }
            );

            myGrid2.setList(lst2);
            //console.log(myGrid2.getSortParam());

        },
        
        getList: function () {
            //console.log(this.target.getList());
            return this.target.getList();
        },
        getSelectedItem: function () {
            console.log(this.target.getSelectedItem());
            //toast.push('콘솔창에 데이터를 출력하였습니다.');
        }
        ,
        append: function () {
        	if(lst.length < 1){
        		gfn_alert("점검시설을 먼저 등록하세요","W");
        		return false;
        	}
            this.target.pushList(
                    {
                        no: false,
                        pdhNum: gfn_isNull(PDH_LIST)? '' : PDH_LIST[0].pdhNum,
                		pdtMdlStd: gfn_isNull(PDH_LIST)? '' : PDH_LIST[0].pdtMdlStd,
                        pdhCnt: 0,
                        sclNum: sclNum,
                        seq: cur_seq,
                        ftrCde: cur_ftrCde,
                        ftrIdn: cur_ftrIdn,
                        pdtCatCde: 'PDT_CAT_CDE001',
                    }
                    
            );
            this.target.setFocus(this.target.list.length - 1);
        }
        ,
        remove: function () {
            var checkedList = myGrid2.getCheckedListWithIndex(0);// colSeq
            if (checkedList.length == 0) {
                alert("선택된 목록이 없습니다. 삭제하시려는 목록을 체크하세요");
                return;
            }
            this.target.removeListIndex(checkedList);
            // 전달한 개체와 비교하여 일치하는 대상을 제거 합니다. 이때 고유한 값이 아닌 항목을 전달 할 때에는 에러가 발생 할 수 있습니다.
        }
    }
};






//주유량 그리드객체 PDT_CAT_CDE pdtCatCde
var fnObj3 = {
        pageStart: function () {
            fnObj3.grid.bind();
        },
        grid: {
            target: new AXGrid(),
            bind: function () {
                window.myGrid3 = fnObj3.grid.target;

                myGrid3.setConfig({
						targetID: "grdPdh2",
                        fitToWidth: true,
                        sort: false,
                        //height:"auto",
                        //fixedColSeq: 3,
                        passiveMode:true,	//그리드에 데이터 그대로 남겨놓기
                        passiveRemoveHide:false,                            
                        colGroup: [
                            {	key: "no", label: "선택", width: "80", align: "center", formatter: "checkbox", formatterLabel: " 전체"
                            },
                            {	key: "status", label: "상태", width: "40", align: "center"
                            	, formatter: function(){
                            		
                            		if(this.item._CUD == "C"){
                                        return '<img width="20px" height="20px" src="/fms/images/gridAdd.png"></img>' ;
                                    }else if(this.item._CUD == "D"){
                                        return '<img width="20px" height="20px" src="/fms/images/gridDel.png"></img>' ;
                                    }
                            	}
                        	},
                            {
                                key: "pdhNum", label: "품명", width: "200", align: "center",
                                editor: {
                                    type: "select",
                                    optionValue: "pdhNum",
                                    optionText: "pdtNam",
                                    options: PDH_LIST2,
                                    
                                    beforeUpdate: function (val) { // 수정이 되기전 value를 처리 할 수 있음.
                                        return val;
                                    },
                                    afterUpdate: function (val) { // 수정이 처리된 후
                                    }
                                }
                            },
                            {
                                key: "pdhAmt", label: "사용량", width: "150", align: "right",
                                formatter: "money",
                                editor: {
                                    type: "number",
                                    updateWith: ["_CUD"],
                                    range: { // 숫자일 경우 숫자자릿수와 소수점 자릿수 지정
                                        val: "2,1"
                                        ,msg : '숫자일 경우 숫자자릿수와 소수점 자릿수 지정' // 자릿수를 초과 했을대 보여줄 메시지.
                                    }
                                },
                                validate : function(){
                                	if(gfn_isNull(this.value)){
                                		alert("사용량은 필수 입력 항목 입니다.");
                                		return false;
                                	} else {
                                		return true;
                                	}
                                }    
                                
                            },
                            {	key: "pdhHtNum", label: "", width: "50", display:false},
                            {	key: "sclNum", label: "", width: "50", display:false},
                            {	key: "seq", label: "", width: "50", display:false},
                            {	key: "ftrCde", label: "", width: "50", display:false},
                            {	key: "ftrIdn", label: "", width: "50", display:false},
                            {	key: "pdtCatCde", label: "", width: "50", display:false},
                        ],
                        colHeadAlign: "center", // 헤드의 기본 정렬 값 ( colHeadAlign 을 지정하면 colGroup 에서 정의한 정렬이 무시되고 colHeadAlign : false 이거나 없으면 colGroup 에서 정의한 속성이 적용됩니다.
                        body: {
                            onclick: function(){
                                //toast.push(Object.toJSON({index:this.index, r:this.r, c:this.c, item:this.item}));
                            },
                            oncheck: function(){
                            	try{
                            		if(!this.item.___checked[0]){ //삭제풀면 상태원복
                            			this.item._CUD = "U";
                            		}
                            		myGrid3.dataSync();
                            	}catch(e){
                            		//전체체크 처리
                            	}
                            },                                      
                            addClass: function(){
                            },
                            onchangeScroll: function(){
                                //console.log(this);
                            },
                        }
                        ,
                        page: {
                            paging: false
                        },
                        onkeyup: function(event, element){
                            //this.list
                            //this.item
                            //this.element
                        },
                    }
            );

            myGrid3.setList(lst3);
            //console.log(myGrid3.getSortParam());

        },
        
        getList: function () {
            //console.log(this.target.getList());
            return this.target.getList();
        },
        getSelectedItem: function () {
            console.log(this.target.getSelectedItem());
            //toast.push('콘솔창에 데이터를 출력하였습니다.');
        }
        ,
        append: function () {
        	if(lst.length < 1){
        		gfn_alert("점검시설을 먼저 등록하세요","W");
        		return false;
        	}
        	
            this.target.pushList(
                    {
                        no: false,
                        pdhNum: gfn_isNull(PDH_LIST2)? '' : PDH_LIST2[0].pdhNum,
                        pdhAmt: 0,
                        sclNum: sclNum,
                        seq: cur_seq,
                        ftrCde: cur_ftrCde,
                        ftrIdn: cur_ftrIdn,
                        pdtCatCde: 'PDT_CAT_CDE002',
                    }
                    
            );
            this.target.setFocus(this.target.list.length - 1);
        }
        ,
        remove: function () {
            var checkedList = myGrid3.getCheckedListWithIndex(0);// colSeq
            if (checkedList.length == 0) {
                alert("선택된 목록이 없습니다. 삭제하시려는 목록을 체크하세요");
                return;
            }
            this.target.removeListIndex(checkedList);
            // 전달한 개체와 비교하여 일치하는 대상을 제거 합니다. 이때 고유한 값이 아닌 항목을 전달 할 때에는 에러가 발생 할 수 있습니다.
        }
    }
};


