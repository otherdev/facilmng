var keyPdhNum = "";

//var pageID = "inline-edit";
var PDT_CAT_LIST = [
    {dtlCd: 'PDT_CAT_CDE001', cdNm: "소모품"},
];

var lst  = []; //그리드데이터 - 소모품 목록

//소모품목록 그리드 객체
var fnObj = {
        pageStart: function () {           
            fnObj.grid.bind(); 
        },
        grid: {
            target: new AXGrid(),
            bind: function () {
                window.myGrid = fnObj.grid.target;
                lstGrid = window.myGrid;

                myGrid.setConfig({
                        targetID: "grdPay",
                        fitToWidth: true,
                        sort: false,
                        //height:"auto",
                        //fixedColSeq: 3,
                        passiveMode:true,	//그리드에 데이터 그대로 남겨놓기
                        passiveRemoveHide:false,                            
                        colGroup: [
                            {	key: "no", label: "선택", width: "60", align: "center", formatter: "checkbox", formatterLabel: " 전체"
                            },
                            {	key: "status", label: "상태", width: "40", align: "center"
                               ,formatter: function(){
                            		
                            		if(this.item._CUD == "C"){
                                        return '<img width="20px" height="20px" src="/fms/images/gridAdd.png"></img>' ;
                                    }else if(this.item._CUD == "D"){
                                        return '<img width="20px" height="20px" src="/fms/images/gridDel.png"></img>' ;
                                    }
                            	}
                        	},
                        	{	key: "pdtCatCde", label: "구분", width: "60", align:"center" 
                        	   ,editor: {
                                     type: "select",
                                     optionValue: "dtlCd", 
                                     optionText: "cdNm", 
                                     options: PDT_CAT_LIST
                                 }
                        	   ,updateWith: ["_CUD"]       	   
                        	   //수정이 되기전 value를 처리 할 수 있음.
                               ,beforeUpdate: function (val) {
                                    // 선택된 값은
                                    console.log(val);
                                    return val;
                                }
                                // 수정이 처리된 후
                               ,afterUpdate: function (val) { 
                                    // 수정이 된 후 액션.
                                    console.log(this);
                                }                       

                            },
                        	{	key: "pdtNam", label: "품명", width: "140",editor: {type: "text",updateWith: ["_CUD"]}},
                        	{	key: "pdtMdlStd", label: "모델및규격", width: "100",editor: {type: "text",updateWith: ["_CUD"]}},
                        	{	key: "pdtUnt", label: "단위", width: "80",editor: {type: "text",updateWith: ["_CUD"]}},
                        	{	key: "pdtMnf", label: "제조사", width: "120",editor: {type: "text",updateWith: ["_CUD"]}},                        	
                        	{
                                key: "useYn", label: "사용여부", width: "80", align: "center",
                                editor: {
                                    type: "checkbox",
                                    updateWith: ["_CUD"],
                                    beforeUpdate: function (val) {
                                        return (val == true) ? "Y" : "N";
                                    }
                                }
                            },
                        	{	key: "pdhNum", label: "", width: "0", display:false},
                        ],
                        colHeadAlign: "center", // 헤드의 기본 정렬 값 ( colHeadAlign 을 지정하면 colGroup 에서 정의한 정렬이 무시되고 colHeadAlign : false 이거나 없으면 colGroup 에서 정의한 속성이 적용됩니다.
                        body: {
                            onclick: function(){
                                //toast.push(Object.toJSON({index:this.index, r:this.r, c:this.c, item:this.item}));
                            	keyPdhNum = this.item.pdhNum;
                            },
                            /* ondblclick 선언하면 onclick 이벤트가 0.25 초 지연 발생 됩니다. 주의 하시기 바람니다. */
                            ondblclick: function () {
                                //toast.push(Object.toJSON({"event": "dblclick", "index": this.index, "r": this.r, "c": this.c, "item": this.item}));
                            	keyPdhNum = this.item.pdhNum;
                            	fn_inCrtPop(keyPdhNum);
                            },

                            oncheck: function(){
                            	try{
                            		if(!this.item.___checked[0]){ //삭제풀면 상태원복
                            			this.item._CUD = "U";
                            		}
                            		myGrid.dataSync();
                            	}catch(e){
                            		//전체체크 처리
                            	}
                            },                                      
                            addClass: function(){
                            },
                            onchangeScroll: function(){
                                //console.log(this);
                                for(var i=this.startIndex;i<this.endIndex+1;i++){
                                    $("#AXdate_"+i).bindDate({align:"right", valign:"top",
                                        onchange:function(){
                                            console.log(this.objID);
                                            var myi = this.objID.substr(this.objID.lastIndexOf("_").number()+1);
                                            myGrid.list[myi].payYmd = this.value;
                                        }
                                    });
                                }
                            },
                        }
                        ,
                        page: {
                            paging: false
                           ,onchange: function(pageNo){
                               // dialog.push(Object.toJSON(this));
                                //trace(this, pageNo);
                            }
                        },
                        onkeyup: function(event, element){
                            //this.list
                            //this.item
                            //this.element
                        },
                    }
            );

            myGrid.setList(lst);            
            //console.log(myGrid.getSortParam());
            var _obj ={
                    message_type:"",
                    document_list: lst,
                    page_navigation:{
                        total_count:23, total_page:2, cur_page:1, page_count:2, first_page:1, last_page:2, point:0
                    },
                    error:0, message:"success"
                };


            var gridData = {
                    list: _obj.document_list,
                    page:{
                        pageNo: _obj.page_navigation.cur_page,
                        pageSize: 10,
                        pageCount: _obj.page_navigation.page_count,
                        listCount: _obj.page_navigation.total_count
                    }
             };

            myGrid.setData(gridData);

        },
        
        getList: function () {
            //console.log(this.target.getList());
            return this.target.getList();
        },
        getSelectedItem: function () {
            console.log(this.target.getSelectedItem());
            //toast.push('콘솔창에 데이터를 출력하였습니다.');
        }
        ,
        append: function () {
        	
            this.target.pushList(
                    {
                         no: false
                        ,pdtCatCde: 'PDT_CAT_CDE001'
                        ,pdeNam: ''
                        ,pdtMdlStd: ''            
                        ,pdtUnt: ''
                        ,pdtMnf: ''
                        ,useYn:'Y'
                        ,pdhNum: ''
                    }
            );
            this.target.setFocus(this.target.list.length - 1);
        }
        ,
        remove: function () {
            var checkedList = myGrid.getCheckedListWithIndex(0);// colSeq
            if (checkedList.length == 0) {
                alert("선택된 목록이 없습니다. 삭제하시려는 목록을 체크하세요");
                return;
            }
            this.target.removeListIndex(checkedList);
            // 전달한 개체와 비교하여 일치하는 대상을 제거 합니다. 이때 고유한 값이 아닌 항목을 전달 할 때에는 에러가 발생 할 수 있습니다.
        }
    }   
        

};




