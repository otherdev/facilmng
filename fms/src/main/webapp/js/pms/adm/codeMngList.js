
var _mstCd = ""; //선택된 코드마스터
var lst = [];
var lst2 = [];




// 코드마스터그리드 객체
var fnObj = {
        pageStart: function () {
            fnObj.grid.bind();
        },
        grid: {
            target: new AXGrid(),
            bind: function () {
                window.myGrid = fnObj.grid.target;

                myGrid.setConfig({
                            targetID: "grdMstCd",
                            fitToWidth: true,
                            sort: false,
                            //fixedColSeq: 3,
                            passiveMode:true,	//그리드에 데이터 그대로 남겨놓기
                            passiveRemoveHide:false,                            
                            colGroup: [
                                {	key: "no", label: "선택", width: "60", align: "center", formatter: "checkbox" , formatterLabel: " 전체"
                            	},
                                {	key: "status", label: "상태", width: "40", align: "center"
                                	, formatter: function(){
                                		if(this.item._CUD == "C"){
                                            return '<img width="20px" height="20px" src="/fms/images/gridAdd.png"></img>' ;
                                        }else if(this.item._CUD == "D"){
                                            return '<img width="20px" height="20px" src="/fms/images/gridDel.png"></img>' ;
                                        }
                                	}
                            	},
                                //{ 	key: "_CUD", label: "S", width: "30", align: "center" },
                                {	key: "mstCd", label: "그룹코드", width: "100", align: "left"
                                    ,editor: {
                                        type: "input",
                                        notEmpty: true,
                                        maxLength: 8,
                                    }
	                                ,validate : function(){
	                                	if(this.value != ""){
	                                		alert("그룹코드는 필수 입력 항목 입니다.");
	                                		return false;
	                                	} else {
	                                		return true;
	                                	}
	                                }    
                                },
                                {	key: "cdNm", label: "그룹코드명", width: "150", align: "left"
                                	,editor: {
                                        type: "text",
                                        notEmpty: true,
                                        maxLength: 16,
                                	}
	                                ,validate : function(){
	                                	if(this.value != ""){
	                                		alert("그룹코드명은 필수 입력 항목 입니다.");
	                                		return false;
	                                	} else {
	                                		return true;
	                                	}
	                                }    
                                },
                                {
                                	key: "ord", label: "순번", width: "60", align: "center",
                                	formatter: "money",
                                	editor: {
                                		type: "number",
                                		updateWith: ["money", "_CUD"]
                                	}
                                },
                                {	key: "etc", label: "비고", width: "150", align: "left"
                                	,editor: {
                                		type: "input",
                                	}
                                },
                                {	key: "useYn", label: "사용여부", width: "80", align: "center", formatterLabel: "사용여부"
                                    ,editor: {
                                        type: "checkbox",
                                        beforeUpdate: function (val) {
                                            return (val == true) ? "Y" : "N";
                                        }                                        
                                    }
                                },
                            ],
                            colHeadAlign: "center", // 헤드의 기본 정렬 값 ( colHeadAlign 을 지정하면 colGroup 에서 정의한 정렬이 무시되고 colHeadAlign : false 이거나 없으면 colGroup 에서 정의한 속성이 적용됩니다.
                            body: {
                                oncheck: function(){
                                	try{
                                		if(!this.item.___checked[0]){ //삭제풀면 상태원복
                                			this.item._CUD = "U";
                                		}
                                		myGrid.dataSync();
                                	}catch(e){
                                		//전체체크 처리
                                	}
                                },                                      
                                onclick: function(){
                                    //alert("onclick - " + JSON.stringify(Object.toJSON({index:this.index, r:this.r, c:this.c, item:this.item})));
                                	_mstCd = this.item.mstCd;
                                    fnSearch2(this.item.mstCd);
                                },
                                addClass: function(){
	                                if(this.item._CUD == "D"){
	                                    return "grey";
	                                } else{
	                                    return "";
	                                }
	                            },
                                onchangeScroll: function(){
                                    //console.log(this);
                                }
                            }
                            ,
                            page: {
                                paging: false
                            },
                            onkeyup: function(event, element){
                                //this.list
                                //this.item
                                //this.element
                            },
							/*                                 
                            request: {
                                //ajaxUrl :"saveGrid.php",
                                //ajaxPars:"param1=1¶m2=2"
                            },
                            response: function(){ // ajax 응답에 대해 예외 처리 필요시 response 구현
                                // response에서 처리 할 수 있는 객체 들
                                //console.log({res:this.res, index:this.index, insertIndex:this.insertIndex, list:this.list, page:this.page});
                                if(this.error){
                                    console.log(this);
                                    return;
                                }
                            },
							 */                                
                        }
                );

                myGrid.setList(lst);
                //console.log(myGrid.getSortParam());

            },
            
            getList: function () {
                //console.log(this.target.getList());
                return this.target.getList();
            },
            getSelectedItem: function () {
                //alert("getSelectedItem - " + this.target.getSelectedItem());
                //toast.push('콘솔창에 데이터를 출력하였습니다.');
            }
            ,
            append: function () {
                this.target.pushList(
                        {
                            no: this.target.list.length,
                            mstCd: '',
                            cdNm: '',
                            etc: '',
                            useYn: 'Y',
                            ord: 0,
                        }
                );
                this.target.setFocus(this.target.list.length - 1);
            }
            ,
            remove: function () {
                var checkedList = myGrid.getCheckedListWithIndex(0);// colSeq
                if (checkedList.length == 0) {
                    alert("선택된 목록이 없습니다. 삭제하시려는 목록을 체크하세요");
                    return;
                }
                this.target.removeListIndex(checkedList);
                // 전달한 개체와 비교하여 일치하는 대상을 제거 합니다. 이때 고유한 값이 아닌 항목을 전달 할 때에는 에러가 발생 할 수 있습니다.
            }
        }
    };






//팀원그리드 객체
var fnObj2 = {
        pageStart: function () {
            fnObj2.grid.bind();
        },
        grid: {
            target: new AXGrid(),
            bind: function () {
                window.myGrid2 = fnObj2.grid.target;

                myGrid2.setConfig({
                            targetID: "grdDtlCd",
                            fitToWidth: true,
                            sort: false,
                            //fixedColSeq: 3,
                            passiveMode:true,	//그리드에 데이터 그대로 남겨놓기
                            passiveRemoveHide:false,                            
                            colGroup: [
                                {	key: "no", label: "선택", width: "60", align: "center", formatter: "checkbox", formatterLabel: " 전체"
                            	},
                                {	key: "status", label: "상태", width: "40", align: "center"
                                	, formatter: function(){
                                		if(this.item._CUD == "C"){
                                            return '<img width="20px" height="20px" src="/fms/images/gridAdd.png"></img>' ;
                                        }else if(this.item._CUD == "D"){
                                            return '<img width="20px" height="20px" src="/fms/images/gridDel.png"></img>' ;
                                        }
                                	}
                            	},
                                {	key: "mstNm", label: "그룹코드명", width: "130", align: "left"},
                                {	key: "dtlCd", label: "상세코드", width: "130", align: "left"
                                	,editor: {
                                		type: "input",
                                        maxLength: 20,
                                        notEmpty: true,
                                	}
	                                ,validate : function(){
	                                	if(this.value != ""){
	                                		alert("그룹코드명은 필수 입력 항목 입니다.");
	                                		return false;
	                                	} else {
	                                		return true;
	                                	}
	                                }    
                                },
                                {	key: "cdNm", label: "상세코드명", width: "150", align: "left"
                                	,editor: {
                                        type: "input",
                                        maxLength: 16,
                                        notEmpty: true,
                                	}
	                                ,validate : function(){
	                                	if(this.value != ""){
	                                		alert("그룹코드명은 필수 입력 항목 입니다.");
	                                		return false;
	                                	} else {
	                                		return true;
	                                	}
	                                }    
                                },
                                {
                                	key: "ord", label: "순번", width: "60", align: "center",
                                	formatter: "money",
                                	editor: {
                                		type: "number",
                                		updateWith: ["money", "_CUD"]
                                	}
                                },
                                {	key: "etc", label: "비고", width: "150", align: "left"
                                	,editor: {
                                		type: "input",
                                	}
                                },
                                {	key: "useYn", label: "사용여부", width: "80", align: "center", formatterLabel: "사용여부"
                                    ,editor: {
                                        type: "checkbox",
                                        beforeUpdate: function (val) {
                                            return (val == true) ? "Y" : "N";
                                        }                                        
                                    }
                                },
                                {	key: "mstCd", label: "", width: "50", display:false},
                            ],
                            colHeadAlign: "center", // 헤드의 기본 정렬 값 ( colHeadAlign 을 지정하면 colGroup 에서 정의한 정렬이 무시되고 colHeadAlign : false 이거나 없으면 colGroup 에서 정의한 속성이 적용됩니다.
                            body: {
                                oncheck: function(){
                                	try{
                                		if(!this.item.___checked[0]){ //삭제풀면 상태원복
                                			this.item._CUD = "U";
                                		}
                                		myGrid2.dataSync();
                                	}catch(e){
                                		//전체체크 처리
                                	}
                                },                                      
                                onclick: function(){
                                    //toast.push(Object.toJSON({index:this.index, r:this.r, c:this.c, item:this.item}));
                                },
                                addClass: function(){
	                                if(this.item._CUD == "D"){
	                                    return "grey";
	                                } else{
	                                    return "";
	                                }
	                            },
                                onchangeScroll: function(){
                                }
                            }
                            ,
                            page: {
                                paging: false
                            },
                            onkeyup: function(event, element){
                                //this.list
                                //this.item
                                //this.element
                            },
                        }
                );

                myGrid2.setList(lst2);
                //console.log(myGrid2.getSortParam());

            },
            
            getList: function () {
                //console.log(this.target.getList());
                return this.target.getList();
            },
            getSelectedItem: function () {
                console.log(this.target.getSelectedItem());
                //toast.push('콘솔창에 데이터를 출력하였습니다.');
            }
            ,
            append: function () {
                this.target.pushList(
                        {
                            no: this.target.list.length,
                            dtlCd: '',
                            cdNm: '',
                            ord: 0,
                            etc: '',
                            useYn: 'Y',
                            mstCd: _mstCd,
                        }
                );
                this.target.setFocus(this.target.list.length - 1);
            }
            ,
            remove: function () {
                var checkedList = myGrid2.getCheckedListWithIndex(0);// colSeq
                if (checkedList.length == 0) {
                    alert("선택된 목록이 없습니다. 삭제하시려는 목록을 체크하세요");
                    return;
                }
                this.target.removeListIndex(checkedList);
                // 전달한 개체와 비교하여 일치하는 대상을 제거 합니다. 이때 고유한 값이 아닌 항목을 전달 할 때에는 에러가 발생 할 수 있습니다.
            }
        }
    };

