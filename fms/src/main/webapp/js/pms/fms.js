

/**
 * 공통 데이터피커 사용 함수
 * 
*/
var setDatepicker = function() {
	$(".datepicker" ).datepicker({
	    dateFormat: 'yy-mm-dd',
	    prevText: '이전 달',
	    nextText: '다음 달',
	    monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
	    monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
	    dayNames: ['일','월','화','수','목','금','토'],
	    dayNamesShort: ['일','월','화','수','목','금','토'],
	    dayNamesMin: ['일','월','화','수','목','금','토'],
	    showMonthAfterYear: true,
	    yearSuffix: '년'
	});
}


/**
 * String prototype getBytes
*/
String.prototype.getBytes = function() {
    var contents = this;
    var str_character;
    var int_char_count;
    var int_contents_length;
 
    int_char_count = 0;
    int_contents_length = contents.length;
 
    for (k = 0; k < int_contents_length; k++) {
        str_character = contents.charAt(k);
        if (escape(str_character).length > 4)
            int_char_count += 2;
        else
            int_char_count++;
    }
    return int_char_count;
}

String.prototype.cutStringBytes = function( byteLength ) {
    var contents = this;
    var str_character;
    var int_char_count;
    var int_contents_length;
     
    var returnValue = "";
     
    int_char_count = 0;
    int_contents_length = contents.length;
     
    for (k = 0; k < int_contents_length; k++) {
        str_character = contents.charAt(k);
        if (escape(str_character).length > 4)
            int_char_count += 2;
        else
            int_char_count++;
         
        if ( int_char_count > byteLength ) {
            break;
        }
        returnValue += str_character;
    }
    return returnValue;
}

$(document).ready(function(){
	$('.not-used').each(function(k,el){
		$(el).attr("disabled", "disabled");
	});
});

/* byte를 이용한 입력크기 제한 */
$(document).on('keyup',"input.limit-bytes", function(e){
	/*if(e.which.keyCode == 40){
   		alert();
	}*/
	if( $(this).val().getBytes() > parseInt($(this).attr("maxbytes")) ) {
		$(this).val( $(this).val().cutStringBytes(parseInt($(this).attr("maxbytes"))) );
		alert("최대 " + parseInt($(this).attr("maxbytes")) + "바이트까지 입력 가능합니다.");
	}
});

var openPopup = function() {
	$(".popup-link").click(function(e) {
		e.preventDefault();
		var centerPos = parseInt( $(screen).get(0).width ) / 2;
		var windowCenterPos = 858 / 2;
		var leftPos = centerPos - windowCenterPos;
		var _win = "";
		if($(this).attr("tab-flag")=="tab") {
			var href="";
			if(this.href==""||this.href==null) {
				href=$(this).attr("href");
			} else {
				href=this.href;
			}
			_win = window.open(href, this.title, "left="+leftPos+",top=100,"+$(this).attr("popup-data") + ", menubar=no, location=no, resizable=no, toolbar=no, status=no");
			_win.focus();
		} else {
			try{ opener.closeChild(); }catch(e){}
			_win = window.open($(this).attr("href"), "child", "left="+leftPos+",top=100,"+$(this).attr("popup-data") + ", menubar=no, location=no, resizable=no, toolbar=no, status=no");
			_win.focus();
			try{ opener._winChild = _win; }catch(e){}
		}
	});	
}
var waterpipePrint = function() {			
	$("#printBtn").click(function () {
		$('#qrImage').attr('style', 'text-align:center;');
		var msg = window.print();
		$('#qrImage').attr('style', 'display:none; text-align:center;');
	});
}

function gfn_print(){
	$('#qrImage').attr('style', 'text-align:center;');
	var msg = window.print();
	$('#qrImage').attr('style', 'display:none; text-align:center;');
}



/* 금액입력시 숫자 외 문자 제거 */
function onlyNumber (obj) {
	$(obj).keyup(function () {
		$(this).val($(this).val().replace(/[^0-9]/g,''));
	});
}


/* 금액입력시 숫자 외 문자 제거 
function onlyNumber (obj) {
    
    var num01;
    var num02;
    num01 = obj.value;
    num02 = num01.replace(/\D/g,""); //숫자가 아닌것을 제거, 
                                     //즉 [0-9]를 제외한 문자 제거; /[^0-9]/g 와 같은 표현
    num01 = setComma(num02); //콤마 찍기
    obj.value =  num01;
}
 */

/* 한글입력 방지 */
function noHanPress (obj)
{
    //좌우 방향키, 백스페이스, 딜리트, 탭키에 대한 예외
    if(event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39
    || event.keyCode == 46 ) return;
    //obj.value = obj.value.replace(/[\a-zㄱ-ㅎㅏ-ㅣ가-힣]/g, '');

    $(obj).keyup(function () {
    	$(this).val($(this).val().replace(/[\ㄱ-ㅎㅏ-ㅣ가-힣]/g, ''));
    });
}
/* 한글입력 방지 */

function removeComma() {
	var obj = $('.num_style');	
	if (obj.length > 0) {
		for (var i in obj) {
			if (obj[i].value != undefined) {				
				obj[i].value = obj[i].value.replace(/,/g,"");
			}
		}
	}
}
function removeCommaN(n) {  
    if ( typeof n == "undefined" || n == null || n == "" ) {
        return "";
    }
    var txtNumber = '' + n;
    return txtNumber.replace(/(,)/g, "");
}


function getNumber(obj){
    
    var num01;
    var num02;
    num01 = obj.value;
    num02 = num01.replace(/\D/g,""); //숫자가 아닌것을 제거, 
                                     //즉 [0-9]를 제외한 문자 제거; /[^0-9]/g 와 같은 표현
    num01 = setComma(num02); //콤마 찍기
    obj.value =  num01;
}
function setComma(n) {
    var reg = /(^[+-]?\d+)(\d{3})/;   // 정규식
    n += '';                          // 숫자를 문자열로 변환         
    while (reg.test(n)) {
       n = n.replace(reg, '$1' + ',' + '$2');
    }         
    return n;
}


function comma(num){
    var len, point, str; 
       
    num = num + ""; 
    point = num.length % 3 ;
    len = num.length; 
   
    str = num.substring(0, point); 
    while (point < len) { 
        if (str != "") str += ","; 
        str += num.substring(point, point + 3); 
        point += 3; 
    } 
     
    return str;
 
}


function numStyleRemoveComma() {
	$('.num_style').each(function(k, el) {
		$(el).val( $(el).val().replace(/,/g,"") ); 
	});
}
function strRemoveComma(s) {
	return ( typeof s == "undefined" || s == null || s == ""  ? "" : s.replace(/,/g,"") );
}



function winPopUPCenter(url, winName, pwidth, pheight, scrollYN, resizeYN) {
	var win = null;
	var winL = (screen.width - pwidth) / 2;
	var winT = (screen.height - pheight) / 2;
	var spec = 'toolbar=no,';
	spec += 'status=no,';
	spec += 'location=no,';
	spec += 'height=' + pheight + ',';
	spec += 'width=' + pwidth + ',';
	spec += 'top=' + winT + ',';
	spec += 'left=' + winL + ',';
	spec += 'scrollbars=' + (scrollYN == undefined ? "no" : scrollYN) + ',';
	spec += 'scrollbars=' + (resizeYN == undefined ? "no" : resizeYN);
	var bbox = map.getView().calculateExtent(map.getSize());
	// bbox, layerList 넘겨주기
	var layerList;
	map.getLayers().forEach(function(layer) {
		var visibility = layer.getVisible();
		if (visibility == true) {
			layerList = layerList + "," + layer.get('name');
		}

	});
	url = url + "?layerList=" + layerList + "&bbox=" + bbox;
	win = window.open(url, winName, spec);
	if (parseInt(navigator.appVersion) >= 4) {
		win.window.focus();
	}
}
