
/*****************************************************************************************/
// 업무공통 관련
/*****************************************************************************************/

//공통초기화 처리
function gfn_init(){
	

	// 필수표시
	$("th.req").append("<img src='/fms/images/icon_table_required.png' width='12' height='12' alt=''/>");
	$("th .req").append("<img src='/fms/images/icon_table_required.png' width='12' height='12' alt=''/>");

	// 정수형 데이터 콤마처리
	$("form .numVal").number(true);
	$("form .numVal").css("text-align","right");
	$("form .numVal").css("padding-right","5px");	
	$("form .numVal .left").css("text-align","left");

	$("td.numVal").number(true);
	$("td.numVal").css("text-align","right");
	$("td.numVal").css("padding-right","5px");	
	$("td.numVal.left").css("text-align","left");

	// 실수형 데이터 콤마처리
	$("form .numVal2").number(true,2);
	$("form .numVal2").css("text-align","right");
	$("form .numVal2").css("padding-right","5px");	
	$("form .numVal2 .left").css("text-align","left");

	$("td.numVal2").number(true);
	$("td.numVal2").css("text-align","right");
	$("td.numVal2").css("padding-right","5px");	
	$("td.numVal2.left").css("text-align","left");
	
}




/**
 * gfn_selectList
 * 단순조회
 * @param sqlId
 * @param 조회조건
 * @param callback(list) - 최종조회 리스트
 */
function gfn_selectList(param, callback){
	

	$.ajax({
		url : "/fms/cmm/selectList.do",
		data : param,
		dataType : 'JSON',
		type : 'POST',
		async : false,
		success : function(ret) {
			
			if(typeof callback === "function"){
				callback(ret);
			}
		},
		error : function(xhr, status, error) {
			alert("ERROR : " + error);
		}
	});
	
}








/** 
 * gfn_saveLst
 * json string array로 저장처리!! 
 * @param url
 * @param param - lst(json배열)
 * @param callback
 */
function gfn_saveList(url, param, callback) {

	//	if(proc){
	//		gfn_toast("처리중입니다.");
	//		return;
	//	}
	//	proc = true;
	//	$("#progress_popup").click();
	$.ajax({
		url : url,
		contentType : 'application/json',
		data : JSON.stringify(param),
		dataType : 'JSON',
		type : 'POST',
		success : function(data) {
			//			proc = false;
			//			closeModal();
			if(typeof callback === "function"){
				callback(data);
			}
			
		},
		error : function(xhr, status, error) {
			//			proc = false;
			//			closeModal();
			alert("ERROR : " + error);
		}
	});
}






/**
 * gfn_saveCmm
 * 단건데이터 저장 - 컨트롤러없는 단순처리
 * @param sqlId 쿼리아이디
 * @param callback
 */
function gfn_saveCmm(param, callback) {

	
	$.ajax({
		url : "/fms/pms/upd.do",
		contentType : 'application/json',
		data : JSON.stringify(param),
		dataType : 'JSON',
		type : 'POST',
		success : function(data) {

			if(typeof callback === "function"){
				callback(data);
			}
			
		},
		error : function(xhr, status, error) {
			alert("ERROR gfn_saveCmm : " + error);
		}
	});
}

/**
 * gfn_saveFormCmm
 * form 형태의 단건데이터 저장 - 컨트롤러없는 단순처리
 * @param formId
 * @param sqlId 쿼리아이디
 * @param callback
 */
function gfn_saveFormCmm_old(formId, sqlId, callback) {

	var form = new FormData(document.getElementById(formId));
	form.append("sqlId",sqlId);
	
	/// 숫자인 경우만 콤마제거, 나머지는 원래 데이터로 전송한다
	var data = {};
	
	try{
		var it = form.keys();	
		while(!data.done){
			data = it.next();		
			var amt = "";
			try{
				amt = new String(form.get(data.value));
			}catch(e){
				continue;
			}
			// 숫자인 경우만 콤마제거, 나머지는 원래 데이터로 전송한다
			if(gfn_isNum(amt.replace(/,/g,""))){
				form.set(data.value, parseInt(amt.replace(/,/g,"")));
			}
			
			// NAN 처리
			if(form.get(data.value) == 'NaN' || form.get(data.value) == NaN  ){
				form.set(data.value,"");
			}
		}
	}catch(e){
		console.log(e.toString());
	}
	
	$.ajax({
		url : "/fms/pms/formUpd.do",
		data : form,
		dataType : 'JSON',
		processData : false,
		contentType : false,
		type : 'POST',
		success : function(data) {

			if(typeof callback === "function"){
				callback(data);
			}
		},
		error : function(xhr, status, error) {
			alert("ERROR gfn_saveFormCmm : " + error);
		}
	});
}


/**
 * gfn_saveForm
 * form 형태의 단건데이터 저장
 * @param formId
 * @param url 컨트롤러 호출명
 * @param callback
 */
function gfn_saveForm_old(formId, url, callback) {
	
	var form = new FormData(document.getElementById(formId));
	
	/// 숫자인 경우만 콤마제거, 나머지는 원래 데이터로 전송한다
	var data = {};
	
	try{
		var it = form.keys();	
		while(!data.done){
			data = it.next();		
			var amt = "";
			try{
				amt = new String(form.get(data.value));
			}catch(e){
				continue;
			}
			// 숫자인 경우만 콤마제거, 나머지는 원래 데이터로 전송한다
			if(gfn_isNum(amt.replace(/,/g,""))){
				form.set(data.value, parseInt(amt.replace(/,/g,"")));
			}
			
			// NAN 처리
			if(form.get(data.value) == 'NaN' || form.get(data.value) == NaN  ){
				form.set(data.value,"");
			}
		}
	}catch(e){
		console.log(e.toString());
	}


	
	$.ajax({
		url : url,
		data : form,
		dataType : 'JSON',
		processData : false,
		contentType : false,
		type : 'POST',
		success : function(data) {

			if(typeof callback === "function"){
				callback(data);
			}
		},
		error : function(xhr, status, error) {
			alert("ERROR gfn_saveForm : " + error);
		}
	});
}

/**
 * gfn_saveFormCmm
 * form 형태의 단건데이터 저장 - 컨트롤러없는 단순처리
 * @param formId
 * @param sqlId 쿼리아이디
 * @param callback
 */
function gfn_saveFormCmm(formId, sqlId, callback) {
	
	// 숫자인 경우만 콤마제거, 나머지는 원래 데이터로 전송한다
	var data = {sqlId:sqlId};
	var param = new Array();
	try{
				
		var objForm = $("#" +formId);
		var arr = objForm.serializeArray();
		
		if(arr){ 
			$.each(arr, function(nm, obj) { 
		
				// 숫자인 경우만 콤마제거, 나머지는 원래 데이터로 전송한다
				if($("#" + obj.name).attr("class") != "datepicker" && gfn_isNum((obj.value).replace(/,/g,""))){	
					data[obj.name] = (obj.value).replace(/,/g,"");
				}else{
					data[obj.name] = obj.value;
				}
			}); 
		}
	}catch(e){
		console.log(e.toString());
	}
	
	param.push(data);

	$.ajax({
		url : "/fms/pms/formUpd.do",
		data : data,
		dataType : 'JSON',
		type : 'POST',
		success : function(data) {

			if(typeof callback === "function"){
				callback(data);
			}
		},
		error : function(xhr, status, error) {
			alert("ERROR gfn_saveFormCmm : " + error);
		}
	});
}


/**
 * gfn_saveForm
 * form 형태의 단건데이터 저장
 * @param formId
 * @param url 컨트롤러 호출명
 * @param callback
 */
function gfn_saveForm(formId, url, callback) {
	
	// 숫자인 경우만 콤마제거, 나머지는 원래 데이터로 전송한다
	var data = {};
	var param = new Array();
	try{
				
		var objForm = $("#" +formId);
		var arr = objForm.serializeArray();
		
		if(arr){ 
			$.each(arr, function(nm, obj) { 
		
				// 숫자인 경우만 콤마제거, 나머지는 원래 데이터로 전송한다
				if($("#" + obj.name).attr("class") != "datepicker" && gfn_isNum((obj.value).replace(/,/g,""))){	
					data[obj.name] = (obj.value).replace(/,/g,"");
				}else{
					data[obj.name] = obj.value;
				}
			}); 
		}
	}catch(e){
		console.log(e.toString());
	}
	
	param.push(data);
	
	$.ajax({
		url : url,
		data : data,
		dataType : 'JSON',
		type : 'POST',
		success : function(data) {

			if(typeof callback === "function"){
				callback(data);
			}
		},
		error : function(xhr, status, error) {
			alert("ERROR gfn_saveForm : " + error);
		}
	});
}

/**
 * gfn_saveForm2
 * form 형태의 단건데이터 저장
 * @param param - formId 
 * @param param - 기타다른 파라미터 
 * @param url 컨트롤러 호출명
 * @param callback
 */
function gfn_saveForm2(param, url, callback) {

	var form = new FormData(document.getElementById(param.formId));
	// 나머지파라미터 추가
	$.each(param, function(key, value){
		if(key != "formId"){
			try{
				form.append(key, value);
			}catch(e){}
		}
	});	
	console.log("gfn_saveForm2 form - " + JSON.stringify(form));
	$.ajax({
		//url : "<c:url value='/"+url+"' />",
		url : url,
		data : form,
		dataType : 'JSON',
		processData : false,
		contentType : false,
		type : 'POST',
		success : function(data) {

			if(typeof callback === "function"){
				callback(data);
			}
		},
		error : function(xhr, status, error) {
			alert("ERROR gfn_saveForm2 : " + error);
		}
	});
}





/**
 * gfn_saveCmmList
 * 리스트데이터 저장 - 컨트롤러없는 단순처리
 * @param formId
 * @param sqlId 쿼리아이디
 * @param callback
 */
function gfn_saveCmmList(param, callback) {
	
	//	if(proc){
	//		gfn_toast("처리중입니다.");
	//		return;
	//	}
	//
	//	proc = true;
	//	$("#progress_popup").click();
	
	$.ajax({
		url : "/fms/pms/updList.do",
		data : JSON.stringify(param),
		contentType : 'application/json',
		dataType : 'JSON',
		type : 'POST',
		success : function(data) {
			//			proc = false;
			//			closeModal();
			
			if(typeof callback === "function"){
				callback(data);
			}
			
		},
		error : function(xhr, status, error) {
			//			proc = false;
			//			closeModal();
			alert("ERROR gfn_saveCmmList : " + error);
			
		}
	});
}











/**
 * 공통코드 콤보 생성
 * @param id 콤보아이디
 * @param grpCd 그룹코드
 * @param dtlCd 초기값
 * @param all 전체여부 Y
 */
function gfn_setCombo(id, grpCd, dtlCd, all, callback){
	var list = []; 
	
	gfn_selectList({sqlId: 'selectCmmCd', mstCd:grpCd}, function(ret){
		list = ret.data;

		if(!gfn_isNull(list)){
			var html = "";
			if(!gfn_isNull(all)){
				if(all == "Y"){
					html += "<option value=''>전체</option>";
				}
				else{
					html += "<option value=''>"+all+"</option>";
				}
			}
			
			for(var i=0;i<list.length;i++){
				if(!gfn_isNull(dtlCd) && list[i].dtlCd == dtlCd){
					html += "<option value='" + list[i].dtlCd + "' selected >" + list[i].cdNm + "</option>";
				}
				else{
					html += "<option value='" + list[i].dtlCd + "'>" + list[i].cdNm + "</option>";
				}
			}
			$("#"+id).html(html);
		}
	});

	if(typeof callback === "function"){
		callback();
	}
}



/**
 *	일반콤보 만들기 
 *	obj - 콤보아이디 
 *	sqlId - 쿼리아이디
 *	sCd, sNm - 코드,명 매핑
 * 	defaultVal - 초기선택값 
 *	title 
 */
function gfn_setCbOption(obj, param, callback){
	
	//조건관련 항목이 있으면 추가
	/*	if(cond != null){
			$.each(cond, function(k,v){
				param[k] = v;
			});
		}
	*/	
	
	gfn_selectList(param, function(ret){
		var lst = ret.data;
		
		var html = "";
		if(!gfn_isNull(param.title)){
			html += "<option value='' >[ "+param.title+" ]</span></option>";
		}
		
		if(!gfn_isNull(lst)){		
			
			for(var i=0;i<lst.length;i++){
				var _value = eval("lst[" + i + "]." + param.sCd);
				var _name = eval("lst[" + i + "]." + param.sNm);
				if(!gfn_isNull(param.defaultVal) && _value == param.defaultVal){
					html += "<option value='" + _value + "' selected >" + _name + "</option>";
				}
				else{
					html += "<option value='" + _value + "'>" + _name + "</option>";
				}
			}			
		}
		
		obj.html(html);
		try{
			obj.select(0);
			obj.trigger("change");
		}catch(e){
			console.log("trigger - " + e);
		}
	});	

	if(typeof callback === "function"){
		callback();
	}
	
}












/*****************************************************************************************/
// Validation 관련
/*****************************************************************************************/


/**
 * form 형태 타입별 체크
 */
var gfn_formValid = function(frm){
	var isValid = true;
	var $t;
	
	//필수체크
	$(".reqVal").each(function(idx){
		if(gfn_isNull($.trim($(this).val()))){
			$t = $(this);
			gfn_alert( $(".req:eq("+idx+")").text().replace("*","") +" 는(은) 필수입력 항목입니다.", "W", function(){
				$t.focus();
			});
			isValid = false;
			return false;
		}
	});
	
	//숫자체크
	$(".numVal").each(function(idx){
		
		if(!gfn_isNum($(this).val())){  
			$t = $(this);
			
			gfn_alert( $(".num:eq("+idx+")").text().replace("*","") +" 는(은) 숫자 항목입니다.", "W", function(){
				$t.focus();
			});
			isValid = false;
			return false;
		}
	});
	
	//실수체크
	$(".numVal2").each(function(idx){
		if(!gfn_isNum2($(this).val()) ){
			$t = $(this);
			var pre = "";
			try{
				pre = $(".num2:eq("+idx+")").text().replace("*","");
			}catch(e){}
			gfn_alert( gfn_isNull(pre)? "숫자로 입력하세요." : pre + " 는(은) 숫자 항목입니다.", "W", function(){
				$t.focus();
			});
			isValid = false;
			return false;
		}
	});
	
	return isValid;
};








/**
 * 그리드 필수항목 체크 
 * - columnLayout에서 req:true 로 설정
 */
var gfn_gridReqValid = function(_grid){

	var _data = AUIGrid.getGridData(_grid);
	
	// 필수컬럼 항목들
	var reqCols = [];
	
	for(var i=0; i<columnLayout.length; i++){
		$.each(columnLayout[i], function(k,v){
			if(k == "req" && v == true){
				reqCols.push(columnLayout[i]);
				return false;
			}
		});
	};
	
	
	// 전체데이터 row 별로 체크
	var invalidRowIndex = -1;
	var invalidColIndex = -1;
	var invalidColName = "";
	var invalid = false;
	for(var i=0; i<_data.length; i++){
		// 필수항목별로 체크
		//reqCols.forEach(function(val, idx){
		//reqCols.every(function(val, idx){
		$.each(reqCols, function(idx, val){

			//숫자타입은 0도 널로 취급
			if( val.dataType == "numeric" ){
				
				if(typeof _data[i][val.dataField] == "undefined" || gfn_isNull(_data[i][val.dataField]) ||  _data[i][val.dataField] == 0 ) {
					
					invalidRowIndex = i;
					invalidColIndex = AUIGrid.getColumnIndexByDataField(_grid, val.dataField);
					invalidColName = val.headerText;
					invalid = true;
					return false;
				}
				return true;
			}
			else{
				if(typeof _data[i][val.dataField] == "undefined" || gfn_isNull(_data[i][val.dataField]) ) {
					
					invalidRowIndex = i;
					invalidColIndex = AUIGrid.getColumnIndexByDataField(_grid, val.dataField);
					invalidColName = val.headerText;
					invalid = true;
					return false;
				}
				return true;
			}
			
		});
		
	}
	
	if(invalid){
		gfn_alert(invalidColName + "은(는) 필수입력 항목입니다.","W",function(){
			AUIGrid.setSelectionByIndex(_grid, invalidRowIndex, invalidColIndex);
		});
		return false;

//			alert(invalidColName + "은(는) 필수입력 항목입니다.");
//			AUIGrid.setSelectionByIndex(_grid, invalidRowIndex, invalidColIndex);
//			return false;
	}
	
	
	return true;
};








/*****************************************************************************************/
// 업무처리함수
/*****************************************************************************************/

// 해당사업이 내가속한 사업인지여부
var gfn_isMyBiz = function(bizSeq, id){
	var isMyBiz = false;
	gfn_selectList({sqlId: 'seletMyBiz', id:id}, function(ret){
		var lst = ret.data;
		
		$.each(lst, function(idx, map){
			if( map.bizSeq == bizSeq)	isMyBiz = true;
		});
	});	
	return isMyBiz;
}







/*****************************************************************************************/
// 미사용
/*****************************************************************************************/




/**
 * gfn_searchGrid
 * 그리드 조회
 * @param sqlId : 쿼리아이디
 * @param param : 파리미터
 * @param callback : 콜백함수
 */
function gfn_searchGrid(sqlId, gridId, param, callback){

	if(proc){
		gfn_toast("처리중입니다.");
		return;
	}

	/// 로딩바표시
	proc = true;
	$("#progress_popup").click();
	
	// 쿼리아이디 전달
	param.sqlId = sqlId;
	
	//그리드객체 항달
	var _grid= null;
	try{
		_grid = eval(gridId);
	}catch(e){
		alert("그리드를 찾을수 없습니다.");
		return;
	}
	
	$.ajax({
		url : '../common/selectCommList.do',
		data : param,
		dataType : 'JSON',
		type : 'POST',
		//async : false,
		success : function(ret) {
			proc = false;
			closeModal();
			
			if(ret.result){
				if(!gfn_isNull(ret.data)){
					AUIGrid.setGridData(_grid, ret.data);
				}
				else{
					AUIGrid.setGridData(_grid, []);
				}
								
			}else{
				alert("트랜잭션 오류.. " + gfn_isNull(ret.error) ? "" : ret.error);
				return
			}
			
			if(typeof callback === "function"){
				callback(ret.data);
			}
		},
		error : function(xhr, status, error) {
			proc = false;
			closeModal();
			alert("ERROR : " + error);
		}
	});	
			
}






/**
 * gfn_saveData
 * 단건데이터 저장
 * @param param 전송데이터
 * @param url 컨트롤러 호출명
 * @param callback
 */
function gfn_saveData(param, url, callback) {

	if(proc){
		gfn_toast("처리중입니다.");
		return;
	}

	proc = true;
	$("#progress_popup").click();
	
	
	$.ajax({
		//url : "<c:url value='/"+url+"' />",
		url : "../"+url,
		data : param,
		dataType : 'JSON',
		type : 'POST',
		success : function(data) {
			proc = false;
			closeModal();

			if(typeof callback === "function"){
				callback(data);
			}
			
		},
		error : function(xhr, status, error) {
			proc = false;
			closeModal();
			alert("ERROR gfn_saveData : " + error);
		}
	});
}


/**
 * gfn_saveGrid
 * 그리드의 CRUD 일괄저장처리
 * @param url
 * @param param
 * @param gridId
 * @param callback
 */
function gfn_saveGrid(url, param, gridId, callback) {

	if(proc){
		gfn_toast("처리중입니다.");
		return;
	}
	var _grid = eval(gridId);
	var list1 = gfn_toStatusList(AUIGrid.getAddedRowItems(_grid),"I");
	var list2 = gfn_toStatusList(AUIGrid.getEditedRowItems(_grid),"U");
	var list3 = gfn_toStatusList(AUIGrid.getRemovedItems(_grid),"D");
	 
	var list = list1.concat(list2).concat(list3);
	
	// pk값이 없으면 넣어준다.
	if(!gfn_isNull(param.pk)){
		list = gfn_chgColValue(list, param.pk);
	}
	
	// 행별 적용플래그가 있으면 적용
	if(!gfn_isNull(param.chkCond)){
		
		$.each(list, function(idx, map){
			$.each(map, function(k, v){
				if(k == param.chkCond.col && v != param.chkCond.val){
					map["STAT"] = "N"; //체크컬럼이 특정값이 아닌것들은 저장무효처리
				}
			});
		});
	}		
	
	proc = true;
	$.ajax({
		url : "../"+url,
		data : {
			list : list,
			sqlIdIns : param.sqlIdIns,
			sqlIdUpd : param.sqlIdUpd,
			sqlIdDel : param.sqlIdDel,
		},
		dataType : 'JSON',
		type : 'POST',
		success : function(data) {
			proc = false;
			closeModal();
			if(typeof callback === "function"){
				callback(data);
			}
		},
		error : function(xhr, status, error) {
			proc = false;
			closeModal();
			alert("ERROR gfn_saveGrid : " + error);
		}
	});		
}




/**
 * 엑셀다운로드
 * @param title
 * @param sqlId
 */
function gfn_ExcelDown(title, sqlId) {

	var url = '/fms/pms/excel/download.do';
	var html = '';
	var bodyHtml = '';
	
	html += '<form id="excelForm" name="excelForm" method="post" action='+ url +' >';
	html += '	<input type="hidden" name="searchCondition1" value= "' + $('#searchCondition1').val() + '" />';
	html += '	<input type="hidden" name="searchCondition2" value= "' + $('#searchCondition2').val() + '" />';
	html += '	<input type="hidden" name="searchCondition3" value= "' + $('#searchCondition3').val() + '" />';
	html += '	<input type="hidden" name="searchCondition4" value= "' + $('#searchCondition4').val() + '" />';
	html += '	<input type="hidden" name="searchCondition5" value= "' + $('#searchCondition5').val() + '" />';
	html += '	<input type="hidden" name="searchCondition6" value= "' + $('#searchCondition6').val() + '" />';
	html += '	<input type="hidden" name="searchCondition7" value= "' + $('#searchCondition7').val() + '" />';
	html += '	<input type="hidden" name="searchCondition8" value= "' + $('#searchCondition8').val() + '" />';
	html += '	<input type="hidden" name="searchCondition9" value= "' + $('#searchCondition9').val() + '" />';
	html += '	<input type="hidden" name="searchCondition10" value= "' + $('#searchCondition10').val() + '" />';
	html += '	<input type="hidden" name="searchCondition11" value= "' + $('#searchCondition11').val() + '" />';

	html += '	<input type="hidden" name="title" value= "' + title + '" />';
	html += '	<input type="hidden" name="sqlId" value= "' + sqlId + '" />';
	$("#excel_head").find("th").each(function(idx,val){
		html += '	<input type="hidden" name="aryTit" value= "' + $(this).attr("tit") + '" />';
		html += '	<input type="hidden" name="aryTyp" value= "' + $(this).attr("typ") + '" />';
		html += '	<input type="hidden" name="aryTitle" value= "' + $(this).text() + '" />';
	});
	html += '</form>';
	
	$('#wrap').append(html);
	//$("#excelForm").submit();
	
	 var param = $("#excelForm").serializeArray();
	 
	 //로딩 이미지
	 gfn_startLoading();
	 $.fileDownload(url, {
		 httpMethod: 'POST',
         dataType:"json", // data type of response
         contentType:"application/json",
         data: param,
         successCallback: function (url) {
        	 //로딩 이미지 종료
        	 gfn_endLoading();
         },
         failCallback: function (responseHtml, url) {
        	//로딩 이미지 종료
        	 gfn_endLoading();          
         }
     });
		
}













//시작일자 체크 
function _jsDateCheck(srtDt, endDt) {	
	var arySrtDt = srtDt.split("-"); // ex) 시작일자(2007-10-09)	
	var aryEndDt = endDt.split("-"); // ex) 종료일자(2007-12-05)	
	var startDt = new Date(Number(arySrtDt[0]), Number(arySrtDt[1]) - 1, Number(arySrtDt[2]));	
	var endDt = new Date(Number(aryEndDt[0]), Number(aryEndDt[1]) - 1, Number(aryEndDt[2]));	
	resultDt = Math.floor(endDt.valueOf() / (24 * 60 * 60 * 1000) - startDt.valueOf() / (24 * 60 * 60 * 1000));	
	if (resultDt < 0) { 
		gfn_alert("시작일자가 더 큽니다.","W");  
		return false; 
	}
	else{
		return true;
	}
}
//날짜 차이 체크 및 시작일자 체크 
function _jsDateDiffCheck(srtDt, endDt, rngDay) {
	
	var arySrtDt = srtDt.split("-"); // ex) 시작일자(2007-10-09)	
	var aryEndDt = endDt.split("-"); // ex) 종료일자(2007-12-05)	
	var startDt = new Date(Number(arySrtDt[0]), Number(arySrtDt[1]) - 1, Number(arySrtDt[2]));	
	var endDt = new Date(Number(aryEndDt[0]), Number(aryEndDt[1]) - 1, Number(aryEndDt[2]));	
	resultDt = Math.floor(endDt.valueOf() / (24 * 60 * 60 * 1000) - startDt.valueOf() / (24 * 60 * 60 * 1000));	
	if (resultDt < 0) { alert("시작일자가 더 큽니다."); return false }	
	if (resultDt > rngDay) { alert("선택가능한 기간은 최대 " + rngDay + "일 입니다."); return false; }	
	return true;
	
}

function gfn_startLoading() {
    //화면의 높이와 너비를 구한다.
    var maskHeight = $(document).height(); 
//  var maskWidth = $(document).width();
    var maskWidth = window.document.body.clientWidth;
     
    var mask = "<div id='mask' style='position:absolute; z-index:9000; background-color:#000000; left:0; top:0;'></div>";
    var loadingImg = '';
     
    loadingImg += "<div id='loadingImg' style='position:absolute; left:50%; top:40%;  margin-left: -90px; display:none; z-index:10000;'>";
    loadingImg += " <img src='/fms/images/viewLoading.gif'/>";
    loadingImg += "</div>";  
 
    //화면에 레이어 추가
    $('body')
        .append(mask)
        .append(loadingImg)
       
    //마스크의 높이와 너비를 화면 것으로 만들어 전체 화면을 채운다.
    $('#mask').css({
            'width' : maskWidth
            , 'height': maskHeight
            , 'opacity' : '0.3'
    }); 
 
    //마스크 표시
    $('#mask').show();   
 
    //로딩중 이미지 표시
    $('#loadingImg').show();
}

function gfn_endLoading() {
    $('#mask, #loadingImg').hide();
    $('#mask, #loadingImg').remove();  
}



