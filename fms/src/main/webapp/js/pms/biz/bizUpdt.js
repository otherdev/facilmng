
//var pageID = "inline-edit";
var PROC_LIST = [
                 {dtlCd: 'PROC01', cdNm: "PROC01NM"},
                 {dtlCd: 'PROC02', cdNm: "PROC02NM"},
];
var POSI_LIST = [
                 {dtlCd: 'POSI01', cdNm: "POSINM"},
                 {dtlCd: 'POSI02', cdNm: "POSINM"},
];
var USER_LIST = [];



var lst = []; //공정그리드데이터
var lst2 = [];//팀원그리드데이터




// 공정그리드 객체
var fnObj = {
        pageStart: function () {
            fnObj.grid.bind();
        },
        grid: {
            target: new AXGrid(),
            bind: function () {
                window.myGrid = fnObj.grid.target;

                myGrid.setConfig({
                            targetID: "grdSched",
                            fitToWidth: true,
                            sort: false,
                            //fixedColSeq: 3,
                            passiveMode:true,	//그리드에 데이터 그대로 남겨놓기
                            passiveRemoveHide:false,                            
                            colGroup: [
                                {	key: "no", label: "선택", width: "80", align: "center", formatter: "checkbox", formatterLabel: " 전체"
                                },
                                //{ 	key: "_CUD", label: "S", width: "30", align: "center" },
                                {	key: "status", label: "상태", width: "40", align: "center"
                                	, formatter: function(){
                                		
                                		if(this.item._CUD == "C"){
                                            return '<img width="20px" height="20px" src="/fms/images/gridAdd.png"></img>' ;
                                        }else if(this.item._CUD == "D"){
                                            return '<img width="20px" height="20px" src="/fms/images/gridDel.png"></img>' ;
                                        }
                                	}
                            	},
                                {
                                    key: "schedCat", label: "공정구분", width: "100", align: "center", 
                                    editor: {
                                        type: "select",
                                        optionValue: "dtlCd",
                                        optionText: "cdNm",
                                        options: PROC_LIST,
                                        
                                        beforeUpdate: function (val) { // 수정이 되기전 value를 처리 할 수 있음.
                                            return val;
                                        },
                                        afterUpdate: function (val) { // 수정이 처리된 후
                                        }
                                    }
                                },
                                {	key: "schedNm", label: "공정명", width: "150", align: "left"
                                    ,editor: {
                                        type: "input",
                                        notEmpty: true,
                                    }
	                                ,validate : function(){
	                                	if(this.value != ""){
	                                		alert("공정명은 필수 입력 항목 입니다.");
	                                		return false;
	                                	} else {
	                                		return true;
	                                	}
	                                }    
                                	
                                },
                                {
                                    key: "schedStartDt", label: "시작일자", width: "150", align:"center"
                                    , formatter:function(){
                                        return '<input type="text" name="" id="AXdate_'+this.index+'" class="AXInput W100 AXdate" value="'+ this.value +'" />';
                                    }
                                },
                                {
                                    key: "schedEndDt", label: "종료일자", width: "150", align:"center"
                                  	, formatter:function(){
                                    	return '<input type="text" name="" id="AXdate__'+this.index+'" class="AXInput W100 AXdate" value="'+ this.value +'" />';
                                  	}
                                
                                },
                                {
                                    key: "planMpw", label: "투입공수(M/M)", width: "150", align: "right",
                                    formatter: "money",
                                    editor: {
                                        type: "number",
                                        updateWith: ["_CUD"],
                                        range: { // 숫자일 경우 숫자자릿수와 소수점 자릿수 지정
	                                        val: "2,1"
	                                        ,msg : '숫자일 경우 숫자자릿수와 소수점 자릿수 지정' // 자릿수를 초과 했을대 보여줄 메시지.
                                        }
                                    }
                                },
                                {
                                    key: "planRate", label: "비율(%)", width: "150", align: "right",
                                    formatter: "money",
                                    editor: {
                                        type: "number",
                                        updateWith: ["money", "_CUD"]
                                    }
                                },
                                {	key: "bizSeq", label: "", width: "50", display:false},
                            ],
                            colHeadAlign: "center", // 헤드의 기본 정렬 값 ( colHeadAlign 을 지정하면 colGroup 에서 정의한 정렬이 무시되고 colHeadAlign : false 이거나 없으면 colGroup 에서 정의한 속성이 적용됩니다.
                            body: {
                                onclick: function(){
                                    //toast.push(Object.toJSON({index:this.index, r:this.r, c:this.c, item:this.item}));
                                },
                                oncheck: function(){
                                	try{
                                		if(!this.item.___checked[0]){ //삭제풀면 상태원복
                                			this.item._CUD = "U";
                                		}
                                		myGrid.dataSync();
                                	}catch(e){
                                		//전체체크 처리
                                	}
                                },                                      
                                addClass: function(){
                                },
                                onchangeScroll: function(){
                                    //console.log(this);
                                    for(var i=this.startIndex;i<this.endIndex+1;i++){
                                        $("#AXdate_"+i).bindDate({align:"right", valign:"top",
                                            onchange:function(){
                                                console.log(this.objID);
                                                var myi = this.objID.substr(this.objID.lastIndexOf("_").number()+1);
                                                myGrid.list[myi].schedStartDt = this.value;
                                            }
                                        });
                                        $("#AXdate__"+i).bindDate({align:"right", valign:"top",
                                            onchange:function(){
                                                console.log(this.objID);
                                                var myi = this.objID.substr(this.objID.lastIndexOf("_").number()+1);
                                                myGrid.list[myi].schedEndDt = this.value;
                                            }
                                        });
                                    }
                                },
                            }
                            ,
                            page: {
                                paging: false
                            },
                            onkeyup: function(event, element){
                                //this.list
                                //this.item
                                //this.element
                            },
							/*                                 
							                                request: {
							                                    //ajaxUrl :"saveGrid.php",
							                                    //ajaxPars:"param1=1¶m2=2"
							                                },
							                                response: function(){ // ajax 응답에 대해 예외 처리 필요시 response 구현
							                                    // response에서 처리 할 수 있는 객체 들
							                                    //console.log({res:this.res, index:this.index, insertIndex:this.insertIndex, list:this.list, page:this.page});
							                                    if(this.error){
							                                        console.log(this);
							                                        return;
							                                    }
							                                },
							 */
                        }
                );

                myGrid.setList(lst);
                //console.log(myGrid.getSortParam());

            },
            
            getList: function () {
                //console.log(this.target.getList());
                return this.target.getList();
            },
            getSelectedItem: function () {
                console.log(this.target.getSelectedItem());
                //toast.push('콘솔창에 데이터를 출력하였습니다.');
            }
            ,
            append: function () {
            	//console.log("append gfn_today - " + gfn_today('yyyy-MM-dd'));
                this.target.pushList(
                        {
                            no: false,
                            schedCat: 'PROC01',
                            schedStartDt: gfn_today('yyyy-MM-dd'),
                            schedEndDt: gfn_today('yyyy-MM-dd'),
                            planMpw: 0,
                            planRage: 0,
                            bizSeq: '${bizSeq}',
                        }
                );
                this.target.setFocus(this.target.list.length - 1);
            }
            ,
            remove: function () {
                var checkedList = myGrid.getCheckedListWithIndex(0);// colSeq
                if (checkedList.length == 0) {
                    alert("선택된 목록이 없습니다. 삭제하시려는 목록을 체크하세요");
                    return;
                }
                this.target.removeListIndex(checkedList);
                // 전달한 개체와 비교하여 일치하는 대상을 제거 합니다. 이때 고유한 값이 아닌 항목을 전달 할 때에는 에러가 발생 할 수 있습니다.
            }
        }
    };






//팀원그리드 객체
var fnObj2 = {
        pageStart: function () {
            fnObj2.grid.bind();
        },
        grid: {
            target: new AXGrid(),
            bind: function () {
                window.myGrid2 = fnObj2.grid.target;

                myGrid2.setConfig({
                            targetID: "grdTeam",
                            fitToWidth: true,
                            sort: false,
                            //fixedColSeq: 3,
                            passiveMode:true,	//그리드에 데이터 그대로 남겨놓기
                            passiveRemoveHide:false,                            
                            colGroup: [
                                {	key: "no", label: "선택", width: "80", align: "center", formatter: "checkbox", formatterLabel: " 전체"
                                },
                                {	key: "status", label: "상태", width: "40", align: "center"
                                	, formatter: function(){
                                		if(this.item._CUD == "C"){
                                            return '<img width="20px" height="20px" src="/fms/images/gridAdd.png"></img>' ;
                                        }else if(this.item._CUD == "D"){
                                            return '<img width="20px" height="20px" src="/fms/images/gridDel.png"></img>' ;
                                        }
                                	}
                            	},
                                {
                                    key: "mpwId", label: "팀원", width: "100", align: "center",
                                    editor: {
                                        type: "select",
                                        optionValue: "userId",
                                        optionText: "userNm",
                                        options: USER_LIST,
                                        
                                        beforeUpdate: function (val) { // 수정이 되기전 value를 처리 할 수 있음.
                                            return val;
                                        },
                                        afterUpdate: function (val) { // 수정이 처리된 후
                                        	
                                        	try{
                                        		var obj = gfn_findData(USER_LIST,{colNm: 'userId', value: val})
                                        		this.item.ofcNm = obj.ofcNm; 
                                        		myGrid2.dataSync();
                                        	}catch(e){}
                                        }
                                    }
                                },
                                {	key: "ofcNm", label: "회사명", width: "150", align: "center"},
								
                                {
                                    key: "posCd", label: "직책", width: "100", align: "center",
                                    editor: {
                                        type: "select",
                                        optionValue: "dtlCd",
                                        optionText: "cdNm",
                                        options: POSI_LIST,
                                        
                                        beforeUpdate: function (val) { // 수정이 되기전 value를 처리 할 수 있음.
                                            return val;
                                        },
                                        afterUpdate: function (val) { // 수정이 처리된 후
                                        }
                                    }
                                },
                                {	key: "mngYn", label: "관리권한", width: "100", align: "center", formatter: "checkbox", formatterLabel: " 관리권한"
                                    ,editor: {
                                        type: "checkbox",
                                        beforeUpdate: function (val) {
                                            return (val == true) ? "Y" : "N";
                                        }                                        
                                    }
                                },
                                {	key: "etc", label: "기타", width: "200", align: "left"
//                                	, formatter: function(){
//                                		return '<input type="text" name="" id="etc_'+this.index+'" class="AXInput" value="'+ this.value +'" style="width:100%;box-sizing:border-box;height:23px;" />';
//                                	}
                                    ,editor: {
                                        type: "input",
                                        //updateWith: ["money", "_CUD"]
                                    }
                                	
                                },
								
								
                                {	key: "bizSeq", label: "", width: "50", display:false},
                            ],
                            colHeadAlign: "center", // 헤드의 기본 정렬 값 ( colHeadAlign 을 지정하면 colGroup 에서 정의한 정렬이 무시되고 colHeadAlign : false 이거나 없으면 colGroup 에서 정의한 속성이 적용됩니다.
                            body: {
                                onclick: function(){
                                    //toast.push(Object.toJSON({index:this.index, r:this.r, c:this.c, item:this.item}));
                                },
                                oncheck: function(){
                                	try{
                                		if(!this.item.___checked[0]){ //삭제풀면 상태원복
                                			this.item._CUD = "U";
                                		}
                                		myGrid2.dataSync();
                                	}catch(e){
                                		//전체체크 처리
                                	}
                                },                                      
                                addClass: function(){
                                },
                                onchangeScroll: function(){
                                }
                            },
                            page: {
                                paging: false
                            },
                            onkeyup: function(event, element){
                                //this.list
                                //this.item
                                //this.element
                            },
                            
                        }
                );

                myGrid2.setList(lst2);
                //console.log(myGrid2.getSortParam());

            },
            
            getList: function () {
                //console.log(this.target.getList());
                return this.target.getList();
            },
            getSelectedItem: function () {
                console.log(this.target.getSelectedItem());
                //toast.push('콘솔창에 데이터를 출력하였습니다.');
            }
            ,
            append: function () {
            	//console.log("USER_LIST[0] - " + JSON.stringify(USER_LIST[0]));
                this.target.pushList(
                        {
                            no: false,
                            mpwId: gfn_isNull(USER_LIST)? '' : USER_LIST[0].userId,
                            ofcCd: gfn_isNull(USER_LIST)? '' : USER_LIST[0].ofcCd,
                    		ofcNm: gfn_isNull(USER_LIST)? '' : USER_LIST[0].ofcNm,
                            posCd: gfn_isNull(POSI_LIST)? '' : POSI_LIST[0].dtlCd,
                            mngYn: 'N',
                            etc: '',
                            bizSeq: '${bizSeq}',
                        }
                );
                this.target.setFocus(this.target.list.length - 1);
            }
            ,
            remove: function () {
                var checkedList = myGrid2.getCheckedListWithIndex(0);// colSeq
                if (checkedList.length == 0) {
                    alert("선택된 목록이 없습니다. 삭제하시려는 목록을 체크하세요");
                    return;
                }
                this.target.removeListIndex(checkedList);
                // 전달한 개체와 비교하여 일치하는 대상을 제거 합니다. 이때 고유한 값이 아닌 항목을 전달 할 때에는 에러가 발생 할 수 있습니다.
            }
        }
    };

