var amChartColorList = [
	  {color:'#FF0F00'}
	 ,{color:'#FF6600'}
	 ,{color:'#FF9E01'}
	 ,{color:'#FCD202'}
	 ,{color:'#F8FF01'}
	 ,{color:'#B0DE09'}
	 ,{color:'#04D215'}
	 ,{color:'#0D8ECF'}
	 ,{color:'#0D52D1'}
	 ,{color:'#2A0CD0'}
	 ,{color:'#8A0CCF'}
	 ,{color:'#CD0D74'}
	 ,{color:'#754DEB'}
	 ,{color:'#DDDDDD'}
	 ,{color:'#333333'}
];

function dash_search1(sDiv, chartType, sYm){
	
	var yyyyMM = (new Date()).format("yyyyMM");
	
	if(sYm == null){
		sYm = yyyyMM;
	}else{
		if(sYm.length != 6){
			sYm = yyyyMM;
		}
	}	
		
	$("#" + sDiv +  "Title").html("민원 현황");
	gfn_selectList({sqlId:  'selectDashChart1List',	//민원 현황
		            pYm : sYm
	   			   }, function(ret){
		
		/*$.each(ret.data, function(num, obj){
   			obj["color"] = amChartColorList[num].color;
	   	});*/
	   				   
		if(chartType == "A"){
			var cmpl_chart = AmCharts.makeChart(sDiv, {
				"hideCredits":true,
				"type": "serial",
				"theme": "dark",
				"categoryField": "aplCdeNam",
				"legend": {
					"enabled": true,
				    "useGraphSettings": true	//차트외부영역 범례 보기
				},
				"rotate": false,	//항목위치 => true:left, false:bottom
				"categoryAxis": {
					"gridPosition": "start",
					"position": "left",
					"labelRotation": 0,	//카달로그 기울기 각도
					//"autoWrap" : true	//자동 각도 설정
				},
				"chartScrollbar": {
					"enabled": true,
					"offset": 30,
					"oppositeAxis": false
				},
				"trendLines": [],
				"graphs": [
					{
						"balloonText": "상수공사::[[category]]([[value]])",
						"fillAlphas": 0.8,
						"id": "AmGraph-1",
						"lineAlpha": 0.2,
						"title": "상수공사",
						"type": "column",
						"valueField": "cnt",
						"fillColorsField": "color",
					},
					{ 
						"balloonText": "급수공사::[[category]]([[value]])",
						"fillAlphas": 0.8,
						"id": "AmGraph-2",
						"lineAlpha": 0.2,
						"title": "급수공사",
						"type": "column",
						"valueField": "cnt2",
						"fillColorsField": "color",
					}
				],
				"guides": [],
				"valueAxes": [
					{
						"id": "ValueAxis-1",
						"position": "left",	/*left, right, top, bottom 결과값 위치*/
						"axisAlpha": 0,						
						//"title": "건수" 
					}
				],
				"allLabels": [],
				"balloon": {},
				"titles": [],
				"dataProvider": ret.data,
				//다운로드 기능
				"export": {
					"enabled": false	
				 }
				
				}); 
		}else if(chartType == "B"){
			var cmpl_chart = AmCharts.makeChart(sDiv, {
				"hideCredits":true,
				"type": "serial",
				"theme": "light",
				"categoryField": "aplCdeNam",
				"legend": {
				    "useGraphSettings": true
				},
				"rotate": false,	//항목위치 => true:left, false:bottom
				"categoryAxis": {
				    "parseDates": false,
				    "axisColor": "#DADADA",
				    "minorGridEnabled": true
				},
				"dataProvider": ret.data,
				"synchronizeGrid":true,
				"valueAxes": [{
				    "id":"v1",
				    "axisColor": "#FF6600",
				    "axisThickness": 2,
				    "axisAlpha": 1,
				    "position": "left"
				}, {
				    "id":"v2",
				    "axisColor": "#FCD202",
				    "axisThickness": 2,
				    "axisAlpha": 1,
				    "position": "right"
				}],
				"graphs": [{
				    "valueAxis": "v1",
				    "lineColor": "#FF6600",
				    "bullet": "round",
				    "bulletBorderThickness": 1,
				    "hideBulletsCount": 30,
				    "title": "상수공사",
				    "valueField": "cnt",
					"fillAlphas": 0
				}, {
				    "valueAxis": "v2",
				    "lineColor": "#FCD202",
				    "bullet": "square",
				    "bulletBorderThickness": 1,
				    "hideBulletsCount": 30,
				    "title": "급수공사",
				    "valueField": "cnt2",
					"fillAlphas": 0
				}],
				"chartScrollbar": {
					"enabled": true,
					"offset": 30,
					"oppositeAxis": false
				},
				"chartCursor": {
				    "cursorPosition": "mouse"
				},			
				//다운로드 기능
				"export": {
					"enabled": false,
				    "position": "bottom-right"
				 }
			});
		}
	});	
}




