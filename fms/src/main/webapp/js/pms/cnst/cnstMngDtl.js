
//var pageID = "inline-edit";
var PTY_CDE_LIST = [
                 {codeCode: 'PTY000', codeAlias: "미분류"},
];



var lst = []; //그리드데이터 - 공사지급내역
var lst2 = []; //그리드데이터 - 설계변경내역
var lst3 = []; //그리드데이터 - 하도급내역
var lst4 = []; //그리드데이터 - 하자보수


//지급내역 그리드 객체
var fnObj = {
        pageStart: function () {
            fnObj.grid.bind();
        },
        grid: {
            target: new AXGrid(),
            bind: function () {
                window.myGrid = fnObj.grid.target;

                myGrid.setConfig({
                        targetID: "grdPay",
                        fitToWidth: true,
                        sort: false,
                        //height:"auto",
                        //fixedColSeq: 3,
                        passiveMode:true,	//그리드에 데이터 그대로 남겨놓기
                        passiveRemoveHide:false,                            
                        colGroup: [
                            {	key: "no", label: "선택", width: "80", align: "center", formatter: "checkbox", formatterLabel: " 전체"
                            },
                            {	key: "status", label: "상태", width: "40", align: "center"
                            	, formatter: function(){
                            		
                            		if(this.item._CUD == "C"){
                                        return '<img width="20px" height="20px" src="/fms/images/gridAdd.png"></img>' ;
                                    }else if(this.item._CUD == "D"){
                                        return '<img width="20px" height="20px" src="/fms/images/gridDel.png"></img>' ;
                                    }
                            	}
                        	},
                            {
                                key: "ptyCde", label: "지급구분", width: "100", align: "center", 
                                editor: {
                                    type: "select",
                                    optionValue: "codeCode", 
                                    optionText: "codeAlias", 
                                    options: PTY_CDE_LIST,
                                    
                                    beforeUpdate: function (val) { // 수정이 되기전 value를 처리 할 수 있음.
                                        return val;
                                    },
                                    afterUpdate: function (val) { // 수정이 처리된 후
                                    }
                                }
                            },
                            {
                                key: "payYmd", label: "지급일자", width: "150", align:"center"
                                , formatter:function(){
                                    return '<input type="text" name="" id="AXdate_'+this.index+'" class="AXInput W100 AXdate" value="'+ this.value +'" />';
                                }
                            },
                            {
                                key: "payAmt", label: "지급금액", width: "150", align: "right",
                                formatter: "money",
                                editor: {
                                    type: "number",
                                    updateWith: ["_CUD"],
                                    range: { // 숫자일 경우 숫자자릿수와 소수점 자릿수 지정
                                        val: "2,1"
                                        ,msg : '숫자일 경우 숫자자릿수와 소수점 자릿수 지정' // 자릿수를 초과 했을대 보여줄 메시지.
                                    }
                                }
                            },
                            {	key: "cntNum", label: "", width: "50", display:false},
                            {	key: "payNum", label: "", width: "50", display:false},
                            {	key: "costSeq", label: "", width: "50", display:false},
                        ],
                        colHeadAlign: "center", // 헤드의 기본 정렬 값 ( colHeadAlign 을 지정하면 colGroup 에서 정의한 정렬이 무시되고 colHeadAlign : false 이거나 없으면 colGroup 에서 정의한 속성이 적용됩니다.
                        body: {
                            onclick: function(){
                                //toast.push(Object.toJSON({index:this.index, r:this.r, c:this.c, item:this.item}));
                            },
                            oncheck: function(){
                            	try{
                            		if(!this.item.___checked[0]){ //삭제풀면 상태원복
                            			this.item._CUD = "U";
                            		}
                            		myGrid.dataSync();
                            	}catch(e){
                            		//전체체크 처리
                            	}
                            },                                      
                            addClass: function(){
                            },
                            onchangeScroll: function(){
                                //console.log(this);
                                for(var i=this.startIndex;i<this.endIndex+1;i++){
                                    $("#AXdate_"+i).bindDate({align:"right", valign:"top",
                                        onchange:function(){
                                            console.log(this.objID);
                                            var myi = this.objID.substr(this.objID.lastIndexOf("_").number()+1);
                                            myGrid.list[myi].payYmd = this.value;
                                        }
                                    });
                                }
                            },
                        }
                        ,
                        page: {
                            paging: false
                        },
                        onkeyup: function(event, element){
                            //this.list
                            //this.item
                            //this.element
                        },
                    }
            );

            myGrid.setList(lst);
            //console.log(myGrid.getSortParam());

        },
        
        getList: function () {
            //console.log(this.target.getList());
            return this.target.getList();
        },
        getSelectedItem: function () {
            console.log(this.target.getSelectedItem());
            //toast.push('콘솔창에 데이터를 출력하였습니다.');
        }
        ,
        append: function () {
        	
            this.target.pushList(
                    {
                        no: false,
                        ptyCde: 'PTY000',
                        payYmd: gfn_today('yyyy-MM-dd'),
                        payAmt: 0,
                        
                        payNum: '',
                        cntNum: '${wttConsMa.cntNum}',
                        costSeq: ''
                    }
            );
            this.target.setFocus(this.target.list.length - 1);
        }
        ,
        remove: function () {
            var checkedList = myGrid.getCheckedListWithIndex(0);// colSeq
            if (checkedList.length == 0) {
                alert("선택된 목록이 없습니다. 삭제하시려는 목록을 체크하세요");
                return;
            }
            this.target.removeListIndex(checkedList);
            // 전달한 개체와 비교하여 일치하는 대상을 제거 합니다. 이때 고유한 값이 아닌 항목을 전달 할 때에는 에러가 발생 할 수 있습니다.
        }
    }
};





//설계변경그리드 객체
var fnObj2 = {
        pageStart: function () {
            fnObj2.grid.bind();
        },
        grid: {
            target: new AXGrid(),
            bind: function () {
                window.myGrid2 = fnObj2.grid.target;

                myGrid2.setConfig({
						targetID: "grdChg",
                        fitToWidth: true,
                        sort: false,
                        //height:"auto",
                        //fixedColSeq: 3,
                        passiveMode:true,	//그리드에 데이터 그대로 남겨놓기
                        passiveRemoveHide:false,                            
                        colGroup: [
                            {	key: "no", label: "선택", width: "80", align: "center", formatter: "checkbox", formatterLabel: " 전체"
                            },
                            {	key: "status", label: "상태", width: "40", align: "center"
                            	, formatter: function(){
                            		
                            		if(this.item._CUD == "C"){
                                        return '<img width="20px" height="20px" src="/fms/images/gridAdd.png"></img>' ;
                                    }else if(this.item._CUD == "D"){
                                        return '<img width="20px" height="20px" src="/fms/images/gridDel.png"></img>' ;
                                    }
                            	}
                        	},
                            {
                                key: "chgYmd", label: "변경일자", width: "130", align:"center"
                                , formatter:function(){
                                    return '<input type="text" name="" id="AXdate1_'+this.index+'" class="AXInput W100 AXdate" value="'+ this.value +'" />';
                                }
                            },

                            {
                                key: "incAmt", label: "증감금액", width: "150", align: "right",
                                formatter: "money",
                                editor: {
                                    type: "number",
                                    updateWith: ["_CUD"],
                                    range: { // 숫자일 경우 숫자자릿수와 소수점 자릿수 지정
                                        val: "2,1"
                                        ,msg : '숫자일 경우 숫자자릿수와 소수점 자릿수 지정' // 자릿수를 초과 했을대 보여줄 메시지.
                                    }
                                }
                            },
                            {
                                key: "igvAmt", label: "증감관금금액", width: "150", align: "right",
                                formatter: "money",
                                editor: {
                                    type: "number",
                                    updateWith: ["_CUD"],
                                    range: { // 숫자일 경우 숫자자릿수와 소수점 자릿수 지정
                                        val: "2,1"
                                        ,msg : '숫자일 경우 숫자자릿수와 소수점 자릿수 지정' // 자릿수를 초과 했을대 보여줄 메시지.
                                    }
                                }
                            },
                            {
                                key: "chgAmt", label: "변경공사금액", width: "150", align: "right",
                                formatter: "money",
                                editor: {
                                    type: "number",
                                    updateWith: ["_CUD"],
                                    range: { // 숫자일 경우 숫자자릿수와 소수점 자릿수 지정
                                        val: "2,1"
                                        ,msg : '숫자일 경우 숫자자릿수와 소수점 자릿수 지정' // 자릿수를 초과 했을대 보여줄 메시지.
                                    }
                                }
                            },
                            {
                                key: "attTim", label: "최종수정일", width: "130", align:"center"
                                , formatter:function(){
                                    return '<input type="text" name="" id="AXdate2_'+this.index+'" class="AXInput W100 AXdate" value="'+ this.value +'" />';
                                }
                            },
                            {	
                                key: "cgvDes", label: "변경관급량", width: "150", align: "left"
                                , editor: {
                                    type: "input",
                                }
                            },
                            {	key: "cntNum", label: "", width: "50", display:false},
                            {	key: "chgNum", label: "", width: "50", display:false},
                            {	key: "chngSeq", label: "", width: "50", display:false},
                        ],
                        colHeadAlign: "center", // 헤드의 기본 정렬 값 ( colHeadAlign 을 지정하면 colGroup 에서 정의한 정렬이 무시되고 colHeadAlign : false 이거나 없으면 colGroup 에서 정의한 속성이 적용됩니다.
                        body: {
                            onclick: function(){
                                //toast.push(Object.toJSON({index:this.index, r:this.r, c:this.c, item:this.item}));
                            },
                            oncheck: function(){
                            	try{
                            		if(!this.item.___checked[0]){ //삭제풀면 상태원복
                            			this.item._CUD = "U";
                            		}
                            		myGrid2.dataSync();
                            	}catch(e){
                            		//전체체크 처리
                            	}
                            },                                      
                            addClass: function(){
                            },
                            onchangeScroll: function(){
                                //console.log(this);
                                for(var i=this.startIndex;i<this.endIndex+1;i++){
                                    $("#AXdate1_"+i).bindDate({align:"right", valign:"top",
                                        onchange:function(){
                                            console.log(this.objID);
                                            var myi = this.objID.substr(this.objID.lastIndexOf("_").number()+1);
                                            myGrid2.list[myi].chgYmd = this.value;
                                        }
                                    });
                                    $("#AXdate2_"+i).bindDate({align:"right", valign:"top",
                                        onchange:function(){
                                            console.log(this.objID);
                                            var myi = this.objID.substr(this.objID.lastIndexOf("_").number()+1);
                                            myGrid2.list[myi].attTim = this.value;
                                        }
                                    });
                                }
                            },
                        }
                        ,
                        page: {
                            paging: false
                        },
                        onkeyup: function(event, element){
                            //this.list
                            //this.item
                            //this.element
                        },
                    }
            );

            myGrid2.setList(lst2);
            //console.log(myGrid2.getSortParam());

        },
        
        getList: function () {
            //console.log(this.target.getList());
            return this.target.getList();
        },
        getSelectedItem: function () {
            console.log(this.target.getSelectedItem());
            //toast.push('콘솔창에 데이터를 출력하였습니다.');
        }
        ,
        append: function () {
        	
            this.target.pushList(
                    {
                        no: false,
                        chgYmd: gfn_today('yyyy-MM-dd'),
                        incAmt: 0,
                        igvAmt: 0,
                        chgAmt: 0,
                        attTim: gfn_today('yyyy-MM-dd'),
                        chgAmt: 0,

						chgNum: '',		
                        cntNum: cntNum,
                        chngSeq: ''
                    }
            );
            this.target.setFocus(this.target.list.length - 1);
        }
        ,
        remove: function () {
            var checkedList = myGrid2.getCheckedListWithIndex(0);// colSeq
            if (checkedList.length == 0) {
                alert("선택된 목록이 없습니다. 삭제하시려는 목록을 체크하세요");
                return;
            }
            this.target.removeListIndex(checkedList);
            // 전달한 개체와 비교하여 일치하는 대상을 제거 합니다. 이때 고유한 값이 아닌 항목을 전달 할 때에는 에러가 발생 할 수 있습니다.
        }
    }
};



//하도급그리드 객체





//하도급 그리드객체


var fnObj3 = {
        pageStart: function () {
            fnObj3.grid.bind();
        },
        grid: {
            target: new AXGrid(),
            bind: function () {
                window.myGrid3 = fnObj3.grid.target;

                myGrid3.setConfig({
						targetID: "grdSub",
                        fitToWidth: true,
                        sort: false,
                        //height:"auto",
                        //fixedColSeq: 3,
                        passiveMode:true,	//그리드에 데이터 그대로 남겨놓기
                        passiveRemoveHide:false,                            
                        colGroup: [
                            {	key: "no", label: "선택", width: "80", align: "center", formatter: "checkbox", formatterLabel: " 전체"
                            },
                            {	key: "status", label: "상태", width: "40", align: "center"
                            	, formatter: function(){
                            		
                            		if(this.item._CUD == "C"){
                                        return '<img width="20px" height="20px" src="/fms/images/gridAdd.png"></img>' ;
                                    }else if(this.item._CUD == "D"){
                                        return '<img width="20px" height="20px" src="/fms/images/gridDel.png"></img>' ;
                                    }
                            	}
                        	},
                            {	
                                key: "subNam", label: "하도급자", width: "150", align: "left"
                                , editor: {
                                    type: "input",
                                }
                            },
                            {	
                                key: "psbNam", label: "하도급 대표자", width: "150", align: "left"
                                , editor: {
                                    type: "input",
                                }
                            },
                            {	
                                key: "subTel", label: "하도급 전화번호", width: "150", align: "left"
                                , editor: {
                                    type: "input",
                                }
                            },
                            {	
                                key: "subAdr", label: "하도급자 주소", width: "150", align: "left"
                                , editor: {
                                    type: "input",
                                }
                            },
                            {	key: "cntNum", label: "", width: "50", display:false},
                            {	key: "subNum", label: "", width: "50", display:false},
                            {	key: "subcSeq", label: "", width: "50", display:false},
                        ],
                        colHeadAlign: "center", // 헤드의 기본 정렬 값 ( colHeadAlign 을 지정하면 colGroup 에서 정의한 정렬이 무시되고 colHeadAlign : false 이거나 없으면 colGroup 에서 정의한 속성이 적용됩니다.
                        body: {
                            onclick: function(){
                                //toast.push(Object.toJSON({index:this.index, r:this.r, c:this.c, item:this.item}));
                            },
                            oncheck: function(){
                            	try{
                            		if(!this.item.___checked[0]){ //삭제풀면 상태원복
                            			this.item._CUD = "U";
                            		}
                            		myGrid3.dataSync();
                            	}catch(e){
                            		//전체체크 처리
                            	}
                            },                                      
                            addClass: function(){
                            },
                            onchangeScroll: function(){
                                //console.log(this);
                            },
                        }
                        ,
                        page: {
                            paging: false
                        },
                        onkeyup: function(event, element){
                            //this.list
                            //this.item
                            //this.element
                        },
                    }
            );

            myGrid3.setList(lst3);
            //console.log(myGrid3.getSortParam());

        },
        
        getList: function () {
            //console.log(this.target.getList());
            return this.target.getList();
        },
        getSelectedItem: function () {
            console.log(this.target.getSelectedItem());
            //toast.push('콘솔창에 데이터를 출력하였습니다.');
        }
        ,
        append: function () {
        	
            this.target.pushList(
                    {
                        no: false,
                        subNam: '',
                        psbNam: '',
                        subTel: '',
                        subAdr: '',

						subNum: '',		
                        cntNum: '${wttConsMa.cntNum}',
                        subcSeq: ''
                    }
            );
            this.target.setFocus(this.target.list.length - 1);
        }
        ,
        remove: function () {
            var checkedList = myGrid3.getCheckedListWithIndex(0);// colSeq
            if (checkedList.length == 0) {
                alert("선택된 목록이 없습니다. 삭제하시려는 목록을 체크하세요");
                return;
            }
            this.target.removeListIndex(checkedList);
            // 전달한 개체와 비교하여 일치하는 대상을 제거 합니다. 이때 고유한 값이 아닌 항목을 전달 할 때에는 에러가 발생 할 수 있습니다.
        }
    }
};



//하자보수 그리드객체
var fnObj4 = {
      pageStart: function () {
          fnObj4.grid.bind();
      },
      grid: {
          target: new AXGrid(),
          bind: function () {
              window.myGrid4 = fnObj4.grid.target;

              myGrid4.setConfig({
						targetID: "grdRpr",
                      fitToWidth: true,
                      sort: false,
                      //height:"auto",
                      //fixedColSeq: 3,
                      passiveMode:true,	//그리드에 데이터 그대로 남겨놓기
                      passiveRemoveHide:false,                            
                      colGroup: [
                          {	key: "no", label: "선택", width: "80", align: "center", formatter: "checkbox", formatterLabel: " 전체"
                          },
                          {	key: "status", label: "상태", width: "40", align: "center"
                          	, formatter: function(){
                          		
                          		if(this.item._CUD == "C"){
                                      return '<img width="20px" height="20px" src="/fms/images/gridAdd.png"></img>' ;
                                  }else if(this.item._CUD == "D"){
                                      return '<img width="20px" height="20px" src="/fms/images/gridDel.png"></img>' ;
                                  }
                          	}
                      	},
                          {
                              key: "flaYmd", label: "하자발생일자", width: "130", align:"center"
                              , formatter:function(){
                                  return '<input type="text" name="" id="AXdate3_'+this.index+'" class="AXInput W100 AXdate" value="'+ this.value +'" />';
                              }
                          },

                          {
                              key: "rprYmd", label: "하자보수일자", width: "130", align:"center"
                              , formatter:function(){
                                  return '<input type="text" name="" id="AXdate4_'+this.index+'" class="AXInput W100 AXdate" value="'+ this.value +'" />';
                              }
                          },
                          {	
                              key: "rprDes", label: "하자보수내용", width: "150", align: "left"
                              , editor: {
                                  type: "input",
                              }
                          },
                          {	key: "cntNum", label: "", width: "50", display:false},
                          {	key: "rprNum", label: "", width: "50", display:false},
                          {	key: "flawSeq", label: "", width: "50", display:false},
                      ],
                      colHeadAlign: "center", // 헤드의 기본 정렬 값 ( colHeadAlign 을 지정하면 colGroup 에서 정의한 정렬이 무시되고 colHeadAlign : false 이거나 없으면 colGroup 에서 정의한 속성이 적용됩니다.
                      body: {
                          onclick: function(){
                              //toast.push(Object.toJSON({index:this.index, r:this.r, c:this.c, item:this.item}));
                          },
                          oncheck: function(){
                          	try{
                          		if(!this.item.___checked[0]){ //삭제풀면 상태원복
                          			this.item._CUD = "U";
                          		}
                          		myGrid4.dataSync();
                          	}catch(e){
                          		//전체체크 처리
                          	}
                          },                                      
                          addClass: function(){
                          },
                          onchangeScroll: function(){
                              //console.log(this);
                              for(var i=this.startIndex;i<this.endIndex+1;i++){
                                  $("#AXdate3_"+i).bindDate({align:"right", valign:"top",
                                      onchange:function(){
                                          console.log(this.objID);
                                          var myi = this.objID.substr(this.objID.lastIndexOf("_").number()+1);
                                          myGrid4.list[myi].flaYmd = this.value;
                                      }
                                  });
                                  $("#AXdate4_"+i).bindDate({align:"right", valign:"top",
                                      onchange:function(){
                                          console.log(this.objID);
                                          var myi = this.objID.substr(this.objID.lastIndexOf("_").number()+1);
                                          myGrid4.list[myi].rprYmd = this.value;
                                      }
                                  });
                              }
                          },
                      }
                      ,
                      page: {
                          paging: false
                      },
                      onkeyup: function(event, element){
                          //this.list
                          //this.item
                          //this.element
                      },
                  }
          );

          myGrid4.setList(lst4);
          //console.log(myGrid4.getSortParam());

      },
      
      getList: function () {
          //console.log(this.target.getList());
          return this.target.getList();
      },
      getSelectedItem: function () {
          console.log(this.target.getSelectedItem());
          //toast.push('콘솔창에 데이터를 출력하였습니다.');
      }
      ,
      append: function () {
      	
          this.target.pushList(
                  {
                      no: false,
                      flaYmd: gfn_today('yyyy-MM-dd'),
                      rprYmd: gfn_today('yyyy-MM-dd'),
                      rprDes: '',
					  rprNum: '',		
					  cntNum: '${wttConsMa.cntNum}',
					  flawSeq: ''
                  }
          );
          this.target.setFocus(this.target.list.length - 1);
      }
      ,
      remove: function () {
          var checkedList = myGrid4.getCheckedListWithIndex(0);// colSeq
          if (checkedList.length == 0) {
              alert("선택된 목록이 없습니다. 삭제하시려는 목록을 체크하세요");
              return;
          }
          this.target.removeListIndex(checkedList);
          // 전달한 개체와 비교하여 일치하는 대상을 제거 합니다. 이때 고유한 값이 아닌 항목을 전달 할 때에는 에러가 발생 할 수 있습니다.
      }
  }
};





