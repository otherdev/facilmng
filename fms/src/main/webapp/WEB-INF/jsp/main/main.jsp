<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>


<link rel="stylesheet" href="${cssGisUrl}/ol/ol.css" type="text/css">
<link rel="stylesheet" href="${cssGisUrl}/us/us.css" type="text/css">

<link href="/fms/css/common_popup.css" rel="stylesheet" />
<link href="/fms/css/style.css" rel="stylesheet" />

<style>
#rbar {
	width: 80px;
	height: 100%;
	background: transparent;
	z-index: 1999;
	position: absolute;
	top: 0;
	right: 0;
}

#btnTog {
	width: 15px;
	height: 60px;
	background: #35495f;
	position: absolute;
	top: 400px;
	right: 60px;
	border-radius: 5px 0px 0px 5px;
	color: #76818e;
	padding: 45px 0 0 4px;
	border: 1px solid #13202e;
	border-right: 0;
}

#bar {
	width: 60px;
	height: 100%;
	background: #ccc;
	position: absolute;
	top: 0;
	right: 0;
}

#mydiv {
	/* 	    position: absolute; */
	z-index: 990;
}

#mydivheader {
	cursor: move;
	/* 	    z-index: 991; */
}

#fcltdiv {
	/* 	    position: absolute; */
	z-index: 990;
}

#fcltdivheader {
	cursor: move;
	/* 	    z-index: 991; */
}

/* 토글툴바컨트롤 스타일	 */
.layer-facility>li>ul {
	display: none;
}
.layer-facility>li.active>ul {
	display: block;
}




.layer-facility-list>ul>li.on{
	background-color:#99ccff;
} 



</style>


<!-- The line below is only needed for old environments like Internet Explorer and Android 4.x -->
<script type="text/javascript" src="${jsGisUrl}/ol/ol.js"></script>
<script type="text/javascript" src="${jsGisUrl}/ol/proj4.js"></script>

<script type="text/javascript" src="<c:url value='/js/gis/fn_main.js' />"></script>



<script>
var popupX = 0;
var popupY= 0;
var _winList;
var _winChild;
var closeWindows = function() {
	try{ _winList.close(); }catch(e){}
	try{ _winChild.close(); }catch(e){}
}


//바로 e맵 좌표계
proj4
		.defs(
				'EPSG:5179',
				'+proj=tmerc +lat_0=38 +lon_0=127.5 +k=0.9996 +x_0=1000000 +y_0=2000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs');
// 울산 좌표계
proj4
		.defs(
				"EPSG:5187",
				"+proj=tmerc +lat_0=38 +lon_0=129 +k=1 +x_0=200000 +y_0=600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

var map;
// 초기 및 전체보기 셋팅 값
var bounds = [ 185398.73, 283713.48, 242079.2, 347374.27 ];
var center = [ 223738.96500000003, 325543.875 ];
var geoServerUrl = "${geoServerUrl}";
var eMapServerUrl = "${eMapServerUrl}";

var gisWfsUrl = geoServerUrl
		+ "/wfs?service=WFS&version=1.1.0&srsname=EPSG:5187&";
var gisWmsUrl = geoServerUrl + "/wms";
var eMapUrl = eMapServerUrl + "/eMap/";

// 시설물 이동 레이어
var uisLayer;

var webPath = "${pageContext.request.contextPath }";




</script>




<script>
$(document).ready(function() {
	/***************************************
		GIS 초기화 관련
	***************************************/
	//정보보기 팝업 닫기버튼
	$('#map_layer_close').click( function() {
		$("#map_layer").toggle();
		featureInfoClose();
	});

	
	
	
	
	
	
	/***************************************
		화멘 초기화 관련
	***************************************/
	popupX = (screen.availWidth-660)/2;
	popupY= (screen.availHeight-1130)/3;
	
	
	
	
	
	
	
	
	
	
	/***************************************
		버튼 이벤트 설정 
	***************************************/
		

		
		
		
		
		
		/**
		 * 편집DIV 동작 스크립트 
		 */
		dragElement(document.getElementById(("mydiv")));
		$("#mydiv").css('top', '200px');
		//  	 $("#mydiv").css('right','200px');
		
		$("#mydivheader").find("img.close").click(function(e) {
			
			if($("#mydivBelow").is(":visible")){
				$(this).attr("src","/fms/images/icon_layer_ful.png");
			}
			else{
				$(this).attr("src","/fms/images/icon_layer_close.png");
			}
			$("#mydivBelow").slideToggle();
		});
		

		dragElement(document.getElementById(("fcltdiv")));
		$("#fcltdiv").css('top', '200px');
		//  	 $("#fcltdiv").css('right','200px');
		$("#fcltdivheader").find("img.close").click(function(e) {
			
			if($("#fcltdivBelow").is(":visible")){
				$(this).attr("src","/fms/images/icon_layer_ful.png");
			}
			else{
				$(this).attr("src","/fms/images/icon_layer_close.png");
			}
			$("#fcltdivBelow").slideToggle();
		});

		$("#btnTog").click(function() {
			favorToggle();
		});

		
		
		
		/// 시설물레이어 체크 이벤트 - GIS 레이어 표시처리
		$('li > input[type="checkbox"]').click(function () {
			var _layer = $(this).attr('id');

			if( _layer == "WTL_ALL" ){
				layerSwitchG(this);
				}
			else{
				layerSwitch(_layer,this);			
			}
		});
		
		
		
		
		/**
		 *	시설물DIV 접힘 제어	 
		 */
		$(".layer-facility>li").click(function(e){
			
			var _level = gfn_isNull(e.target.getAttribute('class')) ? "" : e.target.getAttribute('class');
			var _type = gfn_isNull(e.target.getAttribute('type')) ? "" : e.target.getAttribute('type');
						
			
			//1레벨(체크박스제외) - 메뉴토글
			if( _level.indexOf('lvl1') > -1 &&  _type.indexOf('checkbox') < 0 ){
				
				$(".layer-facility>li").not(this).removeClass("active");
				if($(this).hasClass('active')){
					$(this).removeClass("active");
				}
				else{
					$(this).addClass("active");
				}
				//$(this).toggleClass("active",400);
				
				
				//체크박스 체크처리(하위체크박스도..)
				if($(this).hasClass("active")){
					$(this).find("input[type='checkbox']").prop("checked",true);
				}
				else{
					$(this).find("input[type='checkbox']").prop("checked",false);
				}
				
				//강제클릭이벤트
				var _layer = $(this).find("input[type='checkbox']").first().attr('id');
				layerSwitch(_layer, $(this).find("input[type='checkbox']").first()[0]);			

			}

			//1레벨 (체크박스)
			else if( _level.indexOf('lvl1') > -1 &&  _type.indexOf('checkbox') > -1 ){
				
				//하위체크박스 allcheck
				var parent_chk = $(this).find("input[type='checkbox']").first()[0].checked;
				$(this).find("input[type='checkbox']").each(function(idx){
					if($(this).hasClass('lvl1'))	
						return true; //자기자신 건너뛰고
					
					if($(this).is(":checked")){
						$(this).prop("checked",parent_chk);
					}
					else{
						$(this).prop("checked",parent_chk);
					}
				});
			}
			
			//전체레벨 (체크박스클릭)
			else if($(this).hasClass('all') && _type.indexOf('checkbox') > -1 ){
				
				if($("#WTL_ALL").is(":checked")){
					$(".layer-facility").find("input[type='checkbox']").not("#WTL_ALL").prop("checked",true);
				}
				else{
					$(".layer-facility").find("input[type='checkbox']").not("#WTL_ALL").prop("checked",false);
				}

				//강제클릭이벤트
				//1단계레이어
				$('.layer-facility>li.lvl1').find("input[type='checkbox']").each(function(){
					var _layer = $(this).attr('id');  
					layerSwitch(_layer, this);			
				});
			}
			
			//전체레벨 (라벨클릭) - 강제이벤트
			else if($(this).hasClass('all') && _type.indexOf('checkbox') < 0 ){
				
				if($("#WTL_ALL").is(":checked")){
					$(".layer-facility").find("input[type='checkbox']").prop("checked",false);
				}
				else{
					$(".layer-facility").find("input[type='checkbox']").prop("checked",true);
				}

				//강제클릭이벤트
				//1.전체레이어
				layerSwitchG($(this).find("input[type='checkbox']").first()[0]); 
				//2.1단계레이어
				$('.layer-facility>li.lvl1').find("input[type='checkbox']").each(function(){
					var _layer = $(this).attr('id');  
					layerSwitch(_layer,this);			
				});
				
				
				
			}
			
			//2레벨
			else{
				//1레벨 체크박스 풀기
				$(this).find("input[type='checkbox']").first().prop("checked",false);

				//1단계 레이어처리					
				var parent_layer = $(this).find("input[type='checkbox']").first().attr('id');
				layerSwitch(parent_layer, $(this).find("input[type='checkbox']").first()[0]);
				
				//체크박스 아이닌 경우 체크박스처리
				if( _type.indexOf('checkbox') < 0 ){ 

					
					//체크박스 체크처리(하위체크박스만)
 					$(e.target).find("input[type='checkbox']").each(function(idx){
						if($(this).is(":checked")){
							$(this).prop("checked",false);
						}
						else{
							$(this).prop("checked",true);
						}
					});
					
					
					
					//강제클릭이벤트 - 2단계레이어
					var _layer = e.target.firstChild.id;
					layerSwitch(_layer, e.target.firstChild);			
				
				}
				//체크박스인 경우 
				else{
					
				}
				
			}
			
			
			
		});
		
		
		
		
		
		
		
		/* 토글툴바 링크 호출 이벤트 */
		$('.toglnk').click(function () {
			var url = '${pageContext.request.contextPath}' + $(this).attr('abbr');
			
			//메뉴별 화면사이즈
			var popsize = "width=1200, height=700";
			switch($(this).attr('abbr')){
				case "/pms/fcltMnt/chkSchList.do" : 
					popsize = "width=1100, height=770";
					break;
				default : 
					popsize = "width=1100, height=540";
					break;
			}
			closeWindows(); _winList=window.open(url, "list", 'status=no, ' + popsize + ', left='+ popupX + ', top='+ popupY + ', screenX='+ popupX + ', screenY= '+ popupY);
		});
		
		
		
		
	
	/* 편집레이어 버튼이벤트*/
	
	//시설물등록
	$("#btnAddFac").click(function(){
		if (editSaveYN == "N") {
			alert("이전레이어 작업내역을 저장하신 후 다른레이어를 작업하시기 바랍니다.");
			return;
		}
		waterPipeView();
	});
	
	
	//위치등록 - Point, LineString, Polygon
	$("#btnAddPos").click(function(){
		//waterPipeList();

		if(gfn_isNull($("input[name='ftrIdn']").val())){
			alert("먼저  시설물을 등록하세요.");
			return;
		}
		if (editSaveYN == "N") {
			alert("이전레이어 작업내역을 저장하신 후 다른레이어를 작업하시기 바랍니다.");
			return;
		}

		$('input[name="insertYN"]').val("U");
		editSaveYN = "N";

		removeInteraction();
		featureOverlay.setMap(map);//?????????????
		
				
		switch($('input[name="editType"]').val()){
		
			case "LineString" : 
				lineDraw.on('drawend', function (evt) {
					$('input[name="coordinates"]').val(evt.feature.getGeometry().getCoordinates());
				});
				map.addInteraction(lineDraw);
				break;
				
			case "Polygon" :
				polyDraw.on('drawend', function (evt) {
					$('input[name="coordinates"]').val(evt.feature.getGeometry().getCoordinates());
				});
				map.addInteraction(polyDraw);
				break;
				
			case "Point" :
				pointDraw.on('drawend', function (evt) {
					$('input[name="coordinates"]').val(evt.feature.getGeometry().getCoordinates());
				});
				map.addInteraction(pointDraw);
				break;
				
			default : 				
				break;
		
		}
	
	});
	
	
	//위치등록 - Point만 
	$("#_btnAddPos").click(function(){
		//waterPipeList();

		if(gfn_isNull($("input[name='ftrIdn']").val())){
			alert("먼저  시설물을 등록하세요.");
			return;
		}
		if (editSaveYN == "N") {
			alert("이전레이어 작업내역을 저장하신 후 다른레이어를 작업하시기 바랍니다.");
			return;
		}

		$('input[name="insertYN"]').val("U");

		//위치커서보이기
		map.addInteraction(extent);
		extent.setActive(true);
		map.getInteractions().extend([ drag ]);	//드래그 interaction 추가
		
	});
	
	
	
	//도형이동
	$("#btnShift").click(function(){
		
		var layerNm = $('input[name="layerNm"]').val();
		if(gfn_isNull(layerNm)){
			alert("시설물을 선택해주세요.");
			return;
		}
		

		editSaveYN = "N";
		removeInteraction();
		$('input[name="insertYN"]').val("U");
		

		//위치등록커서비활성화
		extent.setActive(false);
		map.removeInteraction(extent);
		map.getInteractions().extend([ drag ]);	//드래그 interaction 추가
		
	});
	
	

	
	
	//시설물삭제
	$("#btnDelPos").click(function(){
		editSaveYN = "N";
		removeInteraction();
		$('input[name="insertYN"]').val("D");
		
		var layerNm = $('input[name="layerNm"]').val();	
		del.on('select', function(e) {
			var getSource;
			map.getLayers().forEach(function (layer) {
				if (layer.get('name') == layerNm) {
					getSource = layer.getSource();	
				}
			});
			
			var features = e.target.getFeatures();
			features.forEach(function(feature) {
				$('input[name="ftrIdn"]').val(feature.getProperties().FTR_IDN);
				$('input[name="ftrCde"]').val(feature.getProperties().FTR_CDE);
				getSource.removeFeature(feature);
			});
		});
		map.getInteractions().extend([del]);
	});
	
	
	

	/// 위치등록 편집저장
	$("#btnEditSave").click(function(){
		if(gfn_isNull($("input[name='ftrIdn']").val())){
			alert("선택된 지형지물이 없습니다.");
			return;
		}
		
		//커서비활성화
		extent.setActive(false);
		map.removeInteraction(extent);
		
		geomSave();
		//setEditLayerButton();
	});

	//편집무효
	$("#btnEdidCancel").click(function(){
		reset();
	});
	
	
	
	
	/* 시설물대장 상세정보 레이어 버튼*/
	$("#btnSubmit").click(function(){
		
		openInfoView($("#divSubmit").attr("layerType"), $("#divSubmit").attr("ftrCde"), $("#divSubmit").attr("ftrIdn"));		
	});	
	
	
});

	
	
	
/**
 * 툴바관련 레이어 부분
 */
//Make the DIV element draggagle:
function dragElement(elmnt) {
	var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
	if (document.getElementById(elmnt.id + "header")) {
		/* if present, the header is where you move the DIV from:*/
		document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
	} else {
		/* otherwise, move the DIV from anywhere inside the DIV:*/
		elmnt.onmousedown = dragMouseDown;
	}

	function dragMouseDown(e) {
		e = e || window.event;
		// get the mouse cursor position at startup:
		pos3 = e.clientX;
		pos4 = e.clientY;
		document.onmouseup = closeDragElement;
		// call a function whenever the cursor moves:
		document.onmousemove = elementDrag;
	}

	function elementDrag(e) {
		e = e || window.event;
		// calculate the new cursor position:
		pos1 = pos3 - e.clientX;
		pos2 = pos4 - e.clientY;
		pos3 = e.clientX;
		pos4 = e.clientY;
		// set the element's new position:
		elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
		elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
	}

	function closeDragElement() {
		/* stop moving when mouse button is released:*/
		document.onmouseup = null;
		document.onmousemove = null;
	}
}

/// 토글바 접힘
var favorToggle = function() {
	if ($("#bar").is(':visible')) {
		$("#btnTog").css('right', '0');
		$("#btnTog").html("◀");
		$("#bar").toggle("slide", {
			direction : 'right'
		});
	} else {
		$("#bar").toggle("slide", {
			direction : 'right'
		}, function() {
			$("#btnTog").css('right', '60');
			$("#btnTog").html("▶");
		});
	}

	// 	if($( "#rbar" )[0].offsetLeft == "1619"){
	// 		$( "#rbar" ).animate({right:'-60'},500);
	// 	}
	// 	else{
	// 		$( "#rbar" ).animate({right:'0'},500);
	// 	}
};







</script>



<!-- Main Content -->
<div class="page-wrapper" style="position: relative">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="map_wrap">
					<div class="map_box">
						<div id="map" class="map"
							style="diplay: none; width: 100%; height: 100%; padding-left: 0px;"></div>
						<div id="scale-line" class="scale-line"></div>
					</div>
				</div>
				<!-- /map_wrap -->




				<!-- 시설물 위치 레이어 팝업 추가부분 -->
				<div id="map_layer" class="">
				    <div class="regist-box " style="width: 400px; ">
				      <h2 ><span>시설물정보</span>
				      </h2>
				      <div class="view ">
				        <table border="0" cellpadding="0" cellspacing="0"  class="table table-view-regist " >
				          <colgroup>
				          <col width="150px">
				          <col width="*">
				          </colgroup>
				          <tbody id="map_layer_tb">
				          </tbody>
				        </table>
				      </div>
				        <div class="btn_group m-b-5 right "  >
				          <button id="btnSubmit" type="button" class="btn btn-default " > <img src="/fms/images/icon_btn_layer_02.png" width="16" height="16" alt=""/> 대장보기 </button>
				          <button id="map_layer_close" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_delete.png" width="16" height="16" alt=""/> 닫기 </button>
				        </div>
				    </div>
				</div>
				<!-- /map_layer -->
				<!-- /레이어 팝업 추가부분 -->


			</div>
		</div>


	</div>


</div>


<!--편집layer-->
<div class="layer-box" style="display: display;" id="mydiv" >
	<div class="layer-header" id="mydivheader">
		편집레이어<a href="#" class="close" ><img src="/fms/images/icon_layer_close.png" class="close" 
			alt="편집물레이어창닫기 아이콘"> </a>
	</div>
	<div class="layer-content" id="mydivBelow">
		<div class="btn_group m-b-5 " style="width: 100%; text-align: center; display: display;">
			<button id="btnEditSave" class="btn btn-edit-layer" style="width:70px; margin-right: 5px;">Save</button>
			<button id="btnEdidCancel" class="btn btn-edit-layer" style="width:70px; ">Cancel</button>
		</div>
		<div class="layer-facility-list">
			<ul>
				<li id="WTL_FIRE_PS" onclick="javascript:setEditLayer('WTL_FIRE_PS_Edit','SA118','Point',this);">소방시설</li> 
				<li id="WTL_FLOW_PS" onclick="javascript:setEditLayer('WTL_FLOW_PS_Edit','SA117','Point',this);">유량계</li>
				<li id="WTL_GAIN_PS" onclick="javascript:setEditLayer('WTL_GAIN_PS_Edit','SA112','Point',this);">취수장</li>
				<li id="WTL_HEAD_PS" onclick="javascript:setEditLayer('WTL_HEAD_PS_Edit','SA110','Point',this);">수원지</li>
				<li id="WTL_MANH_PS" onclick="javascript:setEditLayer('WTL_MANH_PS_Edit','SA100','Point',this);">상수맨홀</li>
				<li id="WTL_META_PS" onclick="javascript:setEditLayer('WTL_META_PS_Edit','SA122','Point',this);">급수전계량기</li>
				<li id="WTL_CLPI_LM" onclick="javascript:setEditLayer('WTL_PIPE_LM_Edit','SA001','LineString',this);">상수관로</li>
				<li id="WTL_PRES_PS" onclick="javascript:setEditLayer('WTL_PRES_PS_Edit','SA206','Point',this);">가압펌프장</li>
				<li id="WTL_PRGA_PS" onclick="javascript:setEditLayer('WTL_PRGA_PS_Edit','SA121','Point',this);">수압계</li>
				<li id="WTL_PURI_AS" onclick="javascript:setEditLayer('WTL_PURI_AS_Edit','SA113','Polygon',this);">정수장</li>
				<li id="WTL_RSRV_PS" onclick="javascript:setEditLayer('WTL_RSRV_PS_Edit','SA120','Point',this);">저수조</li>
				<li id="WTL_SERV_PS" onclick="javascript:setEditLayer('WTL_SERV_PS_Edit','SA114','Point',this);">배수지</li>
				<li id="WTL_SPLY_LS" onclick="javascript:setEditLayer('WTL_SPLY_LS_Edit','SA001','LineString',this);">급수관로</li>
				<li id="WTL_VALV_PS" onclick="javascript:setEditLayer('WTL_VALV_PS_Edit','SA117','Point',this);">변류시설</li>
			</ul>
		</div>
		<div class="btn_group m-t-10 " style="width: 100%; text-align: center">
			<p
				style="padding: 5px 0 5px; border-top: 1px solid #19d882; font-size: 15px;">작업</p>
			<button id="btnAddFac" class="btn btn-edit-layer btn-block  "><img src="/fms/images/icon_layer_edit_01.png" width="16" height="16" alt="시설물등록"/> 
				시설물 등록</button>
			<button id="btnAddPos" class="btn btn-edit-layer btn-block  "><img src="/fms/images/icon_layer_edit_02.png" width="16" height="16" alt="위치등록"/> 
				시설물 위치등록</button>
			<button id="btnShift" class="btn btn-edit-layer btn-block  "><img src="/fms/images/icon_layer_edit_04.png" width="16" height="16" alt="도형이동"/> 
				시설물 이동</button>
			<button id="btnDelPos" class="btn btn-edit-layer btn-block  "><img src="/fms/images/icon_layer_edit_05.png" width="16" height="16" alt="위치삭제"/> 
				시설물 삭제</button>
		</div>
	</div>
</div>


<!-- 시설물layer -->
<div class="layer-box layer-box-edit" id="fcltdiv">
	<div class="layer-header" id="fcltdivheader">
		시설물레이어<a href="#" class="close"><img src="/fms/images/icon_layer_close.png" class="close" 
			alt="시설물레이어창닫기 아이콘"> </a>
	</div>
	<div class="layer-content " style="padding: 0; height: 516px;"
		id="fcltdivBelow">
		<div class="layer-scroll">
			<ul class="layer-facility ">
				<li class="all"><input name="" type="checkbox" value="WTL_FLOW_PS" id="WTL_ALL">
					전체레이어 선택</li>
				<li class="lvl1"><input class="lvl1"  type="checkbox" name="chkList" value="WTL_PIPE_LM" id="WTL_PIPE_LM" > <img class="lvl1"
					src="/fms/images/icon_layer_map_01.png" alt=""> 상수관로</li>
				<li class="lvl1"><input class="lvl1"  type="checkbox" name="chkList" value="WTL_VALV_PS" id="WTL_VALV_PS" >
					<img src="/fms/images/icon_layer_map_folder.png" alt="" > 변류시설
					
					<ul >
						<li class="lvl2"><input name="chkList" type="checkbox" value="WTL_VALV_PS" id="WTL_VALV_PS_SA200">
							<img src="/fms/images/icon_layer_map_17.png" alt=""> 제수밸브</li>
						<li class="lvl2"><input name="chkList" type="checkbox" value="WTL_VALV_PS" id="WTL_VALV_PS_SA201">
							<img src="/fms/images/icon_layer_map_18.png" alt=""> 역지밸브</li>
						<li class="lvl2"><input name="chkList" type="checkbox" value="WTL_VALV_PS" id="WTL_VALV_PS_SA202">
							<img src="/fms/images/icon_layer_map_19.png" alt=""> 이토밸브</li>
						<li class="lvl2"><input name="chkList" type="checkbox" value="WTL_VALV_PS" id="WTL_VALV_PS_SA203">
							<img src="/fms/images/icon_layer_map_20.png" alt=""> 배기밸브</li>
						<li class="lvl2"><input name="chkList" type="checkbox" value="WTL_VALV_PS" id="WTL_VALV_PS_SA204">
							<img src="/fms/images/icon_layer_map_21.png" alt=""> 감압밸브</li>
						<li class="lvl2"><input name="chkList" type="checkbox" value="WTL_VALV_PS" id="WTL_VALV_PS_SA205">
							<img src="/fms/images/icon_layer_map_22.png" alt=""> 안전밸브</li>
						<li class="lvl2"><input name="chkList" type="checkbox" value="WTL_VALV_PS" id="WTL_VALV_PS_SA206">
							<img src="/fms/images/icon_layer_map_09.png" alt=""> 지수밸브</li>
						<li class="lvl2"><input name="chkList" type="checkbox" value="WTL_VALV_PS" id="WTL_VALV_PS_SA910">
							<img src="/fms/images/icon_layer_map_16.png" alt=""> 지수전</li>
					</ul>
					
					</li>
				<li class="lvl1"><input class="lvl1"  type="checkbox" name="chkList" value="WTL_FIRE_PS" id="WTL_FIRE_PS" >
					<img src="/fms/images/icon_layer_map_folder.png" alt="" > 소방시설
					<ul >
						<li class="lvl2"><input name="chkList" type="checkbox" value="WTL_FIRE_PS_SA118" id="WTL_FIRE_PS_SA118">
							<img src="/fms/images/icon_layer_map_14.png" alt=""> 급수탑</li>
						<li class="lvl2"><input name="chkList" type="checkbox" value="WTL_FIRE_PS_SA119" id="WTL_FIRE_PS_SA119">
							<img src="/fms/images/icon_layer_map_15.png" alt=""> 소화전</li>
					</ul></li>
				<li class="lvl1"><input class="lvl1"  type="checkbox" name="chkList" value="WTL_MANH_PS" id="WTL_MANH_PS">
					<img src="/fms/images/icon_layer_map_02.png" alt="" > 상수맨홀</li>
				<li class="lvl1"><input class="lvl1"  type="checkbox" name="chkList" value="WTL_FLOW_PS" id="WTL_FLOW_PS">
					<img src="/fms/images/icon_layer_map_03.png" alt="" > 유량계</li>
				<li class="lvl1"><input class="lvl1"  type="checkbox" name="chkList" value="WTL_PRGA_PS" id="WTL_PRGA_PS">
					<img src="/fms/images/icon_layer_map_05.png" alt="" > 수압계</li>
				<li class="lvl1"><input class="lvl1"  type="checkbox" name="chkList" value="WTL_HEAD_PS" id="WTL_HEAD_PS">
					<img src="/fms/images/icon_layer_map_04.png" alt="" > 수원지</li>
				<li class="lvl1"><input class="lvl1"  type="checkbox" name="chkList" value="WTL_GAIN_PS" id="WTL_GAIN_PS">
					<img src="/fms/images/icon_layer_map_06.png" alt="" > 취수장</li>
				<li class="lvl1"><input class="lvl1"  type="checkbox" name="chkList" value="WTL_SERV_PS" id="WTL_SERV_PS">
					<img src="/fms/images/icon_layer_map_07.png" alt="" > 배수지</li>
				<li class="lvl1"><input class="lvl1"  type="checkbox" name="chkList" value="WTL_PURI_AS" id="WTL_PURI_AS">
					<img src="/fms/images/icon_layer_map_08.png" alt="" > 정수장</li>
				<li class="lvl1"><input class="lvl1"  type="checkbox" name="chkList" value="WTL_PRES_PS" id="WTL_PRES_PS">
					<img src="/fms/images/icon_layer_map_09.png" alt="" > 가압펌프장</li>
				<li class="lvl1"><input class="lvl1"  type="checkbox" name="chkList" value="WTL_SPLY_LS" id="WTL_SPLY_LS">
					<img src="/fms/images/icon_layer_map_10.png" alt="" > 급수관로</li>
				<li class="lvl1"><input class="lvl1"  type="checkbox" name="chkList" value="WTL_META_PS" id="WTL_META_PS">
					<img src="/fms/images/icon_layer_map_11.png" alt="" > 급수전계량기</li>
				<li class="lvl1"><input class="lvl1"  type="checkbox" name="chkList" value="WTL_RSRV_PS" id="WTL_RSRV_PS">
					<img src="/fms/images/icon_layer_map_12.png" alt="" > 저수조</li>
			</ul>
		</div>
	</div>
</div>

<!-- 툴바관련 레이어 부분 -->
<div class="toggle-box" style="" id="rbar">
	<div id="btnTog">▶</div>
	<div class="toggle-menu" style="border-left: 1px solid #13202e;"
		id="bar">
		<ul class="toggle-menu">
			<li><a href="#" class="toglnk" abbr="/pms/cnst/cnstMngList.do" ><img
					src="/fms/images/icon_left_menu_01.png" alt=""><span class="tooltiptext2">상수공사</span> </a></li>
			<li><a href="#" class="toglnk" abbr="/pms/pipen/wtpipMngList.do" ><img
					src="/fms/images/icon_left_menu_02.png" alt=""> <span class="tooltiptext2">상수관로</span></a></li>
			<li><a href="#" class="toglnk" abbr="/pms/wtsFclt/intkSt/intkStList.do" ><img
					src="/fms/images/icon_left_menu_03.png" alt=""> <span class="tooltiptext2">취수장</span></a></li>
			<li><a href="#" class="toglnk" abbr="/pms/acmFclt/supDut/supDutList.do" ><img
					src="/fms/images/icon_left_menu_04.png" alt=""> <span class="tooltiptext2">급수관로</span></a></li>
			<li><a href="#" class="toglnk" abbr="/pms/fcltMnt/chkSchList.do" ><img
					src="/fms/images/icon_left_menu_05.png" alt=""> <span class="tooltiptext2">점검관리</span></a></li>
			<li><a href="#" class="toglnk" abbr="/pms/cmpl/cnstCmplList.do" ><img
					src="/fms/images/icon_left_menu_06.png" alt=""> <span class="tooltiptext2">민원관리</span></a></li>
			<li><a href="#" class="toglnk" abbr="/pms/stats/statCmplList.do" ><img
					src="/fms/images/icon_left_menu_07.png" alt=""> <span class="tooltiptext2">민원통계</span></a></li>
		</ul>
	</div>
</div>

<!-- /#wrapper -->


<div style="position: absolute; bottom: 100px; display: none;">
<form id="editSave" name="editSave" method="post"	action="${ctxRoot}/map/updateGeom.do">
	<input type="text" id="layerNm" name="layerNm" value="" /> 
	<input type="text" id="ftrCde" name="ftrCde" value="" /> 
	<input type="text" id="ftrIdn" name="ftrIdn" value="" /> 
	<input type="text" id="coordinates" name="coordinates" value="" /> 
	<input type="text" id="editType" name="editType" value="" /> 
	<input type="text" id="insertYN" name="insertYN" value="" />
</form>
</div>


<script>
	$("#map_layer, #address_layer").draggable();
</script>


<!-- 바로e맵 -->
<script type="text/javascript" src="${jsGisUrl}/fn_emap.js"></script>

<script type="text/javascript" src="${jsGisUrl}/fn_layers.js"></script>
<script type="text/javascript" src="${jsGisUrl}/fn_map.js"></script>
<script type="text/javascript" src="${jsGisUrl}/print/html2canvas.js"></script>

<!-- 편집 피쳐 이동 -->
<script type="text/javascript" src="${jsGisUrl}/ol/drag-features.js"></script>
<script type="text/javascript" src="${jsGisUrl}/fn_interaction.js"></script>

<script type="text/javascript" src="${jsGisUrl}/fn_measure.js"></script>
<script type="text/javascript" src="${jsGisUrl}/fn_tools.js"></script>
<script type="text/javascript" src="${jsGisUrl}/fn_info.js"></script>
<script type="text/javascript" src="${jsGisUrl}/fn_find.js"></script>

