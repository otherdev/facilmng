<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<link rel="stylesheet" href="${cssGisUrl}/ol/ol.css" type="text/css">
<script type="text/javascript" src="${jsGisUrl}/ol/ol.js"></script>
<script type="text/javascript" src="${jsGisUrl}/ol/proj4.js"></script>




<script>
// 바로 e맵 좌표계
proj4.defs('EPSG:5179', '+proj=tmerc +lat_0=38 +lon_0=127.5 +k=0.9996 +x_0=1000000 +y_0=2000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs');
// 울산 좌표계
proj4.defs("EPSG:5187","+proj=tmerc +lat_0=38 +lon_0=129 +k=1 +x_0=200000 +y_0=600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

var map;
var bounds = [<c:out value='${bbox}'/>];
var center = [230946.54287, 327498.47239];

var geoServerUrl = "${geoServerUrl}";
var eMapServerUrl = "${eMapServerUrl}";

var gisWfsUrl = geoServerUrl + "/wfs?service=WFS&version=1.1.0&srsname=EPSG:5187&";
var gisWmsUrl = geoServerUrl + "/wms";
var eMapUrl = eMapServerUrl + "/eMap/";

</script> 



<body>

<div id="wrap" class="wrap" > 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1 >상수관로 유지관리 시스템</h1>
  </div>
  <!-- // header --> 

  <!-- container -->
  <div id="container"  class="content"  > 
    

    <!-- list -->
    <div class="regist-box " style="width: 97%;">
      <h2 ><span >지도정보</span>
        <div class="btn_group m-b-5 right "  >
		  <button   type="button" class="btn btn-default " onclick="pagePrintPreview();"><img src="/fms/images/icon_btn_print.png" width="16" height="16" alt=""/> 인쇄</button>
        </div>
      </h2>
	
     
      <div class="view ">
		<div class="print_map">
			<div id="map" class="map" style="width:785px;height:480px; padding: 0"  ></div>
		</div>
      </div>
      
    </div>
    <!-- //list --> 

    <div class="regist-box " style="width: 97%;">

      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table table-view-regist " >
          <colgroup>
          <col width="150">
          <col width="*">
          </colgroup>
          <tbody>
            <tr>
              	<th>내용 </th>
              	<td >									
              		<textarea cols="102" rows="5"></textarea>
			  	</td>
            </tr>
          </tbody>
        </table>
      </div>

	</div>
	
	
  </div>
  
  <!-- //container --> 
</div>
<!-- //UI Object -->

</body>


<!-- 바로e맵 -->
<script type="text/javascript" src="${jsGisUrl}/us/ol-emap.js"></script>
<script type="text/javascript" src="${jsGisUrl}/us/ol-layers.js"></script>
<script type="text/javascript" src="${jsGisUrl}/us/ol-map.js"></script>

<script>

var layerList ="<c:out value='${layerList}'/>";
var layerArray = layerList.split(",");

map.getLayers().forEach(function (layer) {
	for (var i=0; i<layerArray.length; i++) {
		if (layer.get('name') == layerArray[i]) {
			layer.setVisible(true);
		}
	}

});
function myFunction(event){
	   //some stuff 
	   console.log("clicked");   
	}

map.getViewport().removeEventListener('click',myFunction);


map.getView().fit(bounds, map.getSize());

function pagePrintPreview(){
	var browser = navigator.userAgent.toLowerCase();
	if ( -1 != browser.indexOf('chrome') ){
		window.print();
	} else if ( -1 != browser.indexOf('trident') ){
		try {
			//참고로 IE 5.5 이상에서만 동작함
			//웹 브라우저 컨트롤 생성
			var webBrowser = '<OBJECT ID="previewWeb" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
			//웹 페이지에 객체 삽입
			document.body.insertAdjacentHTML('beforeEnd', webBrowser);
			//ExexWB 메쏘드 실행 (7 : 미리보기 , 8 : 페이지 설정 , 6 : 인쇄하기(대화상자))
			previewWeb.ExecWB(7, 1);
			//객체 해제
			previewWeb.outerHTML = "";
		} catch (e) {
			alert("- 도구 > 인터넷 옵션 > 보안 탭 > 신뢰할 수 있는 사이트 선택\n   1. 사이트 버튼 클릭 > 사이트 추가\n   2. 사용자 지정 수준 클릭 > 스크립팅하기 안전하지 않은 것으로 표시된 ActiveX 컨트롤 (사용)으로 체크\n\n※ 위 설정은 프린트 기능을 사용하기 위함임");
		}
	}
}
</script>
</html>