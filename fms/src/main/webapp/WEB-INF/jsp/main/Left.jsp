<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>시설물관리시스템</title>

<link href="/fms/css/common.css" rel="stylesheet" />

<style type="text/css">

body{
	background-color: #509374;
}

.left-menu {
	float: left;
	width: 219px;
	height:100%;
	margin: 0;
	border: 0;
	
}


.side-menu {
	margin-bottom: 0px;
}

.side-menu li img{ vertical-align: middle
}
.side-menu>ul {
	margin-bottom: 0px;
}
.side-menu>li {
	display: block;
	color: #fff;
	border-bottom: 1px solid #478367;
	font-weight: normal;
	text-align: left;
	padding: 0px 0px 0px 0px;
	background:#509374
}
.side-menu>li a {
	font-size: 14px;
	color: #fff;
	
	line-height: 50px;
	padding-left:20px
	
}
.side-menu li a:hover, .side-menu li a:focus {
	background: transparent;
	outline: none;
}
.side-menu>li a.active {
	color: #fff;
	background: url(/fms/images/bul_sidemenu_active.png) no-repeat 0 center;
}

.side-menu .treeview-menu {
/* 	background: #1d765c; */
	background: #3E7B76;
}
.side-menu .treeview-menu li{ 
	border-top: 1px solid #376B69;
	background: url(/fms/images/bullet_left_menu_white.png) no-repeat 50px center;
}
.side-menu .treeview-menu li a{ 
	color: #fff;
	padding-left: 60px
}


.side-menu .treeview-menu li a:hover, .side-menu .treeview-menu li a:focus {
	background: transparent;
	outline: none;
}

/* 메뉴컨트롤 스타일	 */
/* 메뉴컨트롤 스타일	 */
.side-menu>li>ul {
	display: none;
}
.side-menu>li.active>ul {
	display: block;
}

.side-menu .treeview-menu li:last-child  {
border-bottom: 1px solid #1a6a53;}

</style>

<script type="text/javascript" src="/fms/js/lib/jquery-1.12.4.js" ></script>
<script type="text/javascript" src="/fms/js/lib/jquery-ui.js" ></script>
<script type="text/javascript">

	var _winList;
	var _winChild;

	var closeWindows = function() {
		try{ _winList.close(); }catch(e){}
		try{ _winChild.close(); }catch(e){}
	}


	
	
	
	$(function () {
	   
		/* 메뉴접힘 제어  */
		$(".side-menu>li").click(function(e){
			try{
				if( e.target.getAttribute('class').indexOf('lvl1') > -1){
					//1레벨 메뉴 클릭				
					$(".side-menu>li").not(this).removeClass("active");
					$(this).toggleClass("active",400);
				}
				//$(this).addClass("active",400);	
			}catch(e){}
		});
		
		
		
		var popupX = (screen.availWidth-660)/2;
		var popupY= (screen.availHeight-1130)/3;

		/* 메뉴클릭 팝업화면 호출 이벤트 */
		$('.lvl2').click(function () {
			var url = '${pageContext.request.contextPath}' + $(this).attr('abbr');
			
			//메뉴별 화면사이즈
			var popsize = "width=1200, height=700";
			switch($(this).attr('abbr')){
				case "/pms/pipen/wtpipMngList.do" : 
					popsize = "width=1100, height=550";
					break;
				case "/pms/fcltMnt/chkSchList.do" : 
					popsize = "width=1100, height=680";
					break;
				default : 
					popsize = "width=1100, height=540";
					break;
			}
			
			closeWindows(); _winList=window.open(url, "list", 'status=no, ' + popsize + ', left='+ popupX + ', top='+ popupY + ', screenX='+ popupX + ', screenY= '+ popupY);
		});
	   
	});
	
	
		
	

	
</script>
</head>

<div class="left-menu"   >
  		
          <ul class="side-menu">
			
			<c:forEach var="result" items="${list_menulist}" varStatus="status">
				
				<c:choose>
					<c:when test="${result.lvl == '1' }">
						<c:set var="sub_cnt" value="${result.cnt}" ></c:set>
						<c:set var="sub_idx" value="${0}" ></c:set>
						
						<li class="lvl1"><a href="#" class="lvl1"><img src="${result.chkUrl}"  alt="시설관리시스템"> ${result.menuNm}</a>
							<ul class=" treeview-menu" >
					</c:when>
					
					<c:otherwise>
						<c:set var="sub_idx" value="${sub_idx+1}" ></c:set>
						<li><a href="#" class="lvl2" abbr="${result.chkUrl}">${result.menuNm}</a></li>
						
						<c:if test="${sub_idx == sub_cnt}">
				              </ul>
				            </li>
						</c:if>
					</c:otherwise>
					
				</c:choose>

				
      		</c:forEach>
          
          </ul>
          
          
</div>
 







</html>