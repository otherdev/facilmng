<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>시설물관리시스템</title>



<link href="/fms/css/common.css" rel="stylesheet" />

<style type="text/css">


#header {
	margin: 0px;
	background: #21855f;
	width: 100%;
	height: 90px;
	position:fixed;
	top:0;
	left:0;
	z-index:1000
	
}

.index_navbar {
	width: 100%;
	
	text-align: right
}
.index_navbar img {
	float: left; margin-left:20px ; margin-top:14px
}

.member  {
	padding: 0px 0px 13px 20px;
	color: rgba(255,255,255,0.75);
	float:right;
	margin-top:10px;
	background:url(/fms/images/icon_top_welcome.png) no-repeat 0  10px;
 	
}
.member span{font-weight:700; color:#fff}
.member a {
	border-left:1px solid rgba(255,255,255,0.5) ;
	margin-left:10px;
	
	padding: 0px 20px;
	
}
.member a img{ margin-top:10px; vertical-align:bottom}



/* top_menu */
.top_nav { float:left;  display: inline-block;  margin:0; padding:0; height:38px; width:100%;  background:#35495f; border:none;  border-top: 1px solid #243446; border-bottom:1px solid #243446 ; color:#fff; }
.top_nav img{ vertical-align:middle; padding:2px  }

.menu_sidebar{background:#243446; height:38px; line-height:38px;  width:220px; float:left;  text-align:right}
.menu_sidebar a img{ vertical-align:middle; padding:12px 6px; }
.menu_sidebar a{height:38px; line-height:38px; padding:14px 12px; margin-top:14px }
.menu_sidebar a:hover, .menu_sidebar a:focus {
	background: #;
	outline: none;

}


.menu_nav{ float:left; }
.menu_nav ul {
	list-style: none;
	margin: 0;
	padding: 0;
	font-size:0
}
.menu_nav > li {
	display: inline-block; 
	position: relative; height:38px; line-height:36px; padding: 0px;  border-right:1px solid  #243446;
}
.menu_nav > li img{ margin-bottom:3px}
.menu_nav > li + li {
	
}

.menu_nav a {
	color: #fff;
	display: block;
	padding: 0px 30px;
	text-decoration: none;
	
	font-size: 13px;
	font-weight: normal;
	text-align: center;
	font-family: 'Noto Sans KR';

     height:38px; line-height:36px;
	
}


.menu_nav a:hover, .menu_nav a:focus {
	background: #243d58;
	outline: none;

}

/* top_menu */
.map_nav { float:right  }

.map_nav ul {
	list-style: none;
	margin: 0;
	padding: 0;
	font-size:0
}
.map_nav > li {
	display: inline-block;
	position: relative;height:38px; line-height:38px;  padding:0px ; margin:0px;
	background:#27384b; border:0; margin-left:1px

}
.map_nav > li + li { 
	
}

.map_nav a {
	color: #fff;
	display: block;
	padding: 0px 12px;
	text-decoration: none;
	
	
	font-size: 13px;
	font-weight: normal;
	text-align: center;
	vertical-align:bottom;
	
	height:38px; line-height:32px;
	
}
.map_nav a img {
	margin-top:5px; border:0px;
	

}

.map_nav a:hover, .map_nav a:focus {
	background:#243d58;
	outline: none;
}


/* 툴팁관련 */
.lnk span.tooltiptext {
    visibility: hidden;
    width: 100%;
    height:30px;
    background-color: #555;
    color: #fff;
    text-align: center;
    border-radius: 4px;
    padding: 1px 0;

    /* Position the tooltip */
	position: absolute; 
	top: -33px; 
	left: 0px; 
	opacity: 0.7;
    z-index: 9900;
}
.lnk:hover .tooltiptext {
    visibility: visible;
}



</style>

<script type="text/javascript" src="/fms/js/lib/jquery-1.12.4.js" ></script>



<script>
//접기
var folden = false;

var popupX = (screen.availWidth-660)/2;
var popupY= (screen.availHeight-1130)/3;
var _winList;
var _winChild;
var closeWindows = function() {
	try{ _winList.close(); }catch(e){}
	try{ _winChild.close(); }catch(e){}
}


$(document).ready(function() {
	
	$(".menu_sidebar").click(function(e){
		
        
		if(!folden){
			//parent.frames["mid"].cols = "0, *";
			folden = true;
			
		    e.preventDefault();
	        window.parent.minimize();
	        return false;
		}
		else{
			//parent.frames["mid"].cols = "200, *";
			//$("#dummy").animate({width: '0px'},1000);
			folden = false;
			
		    e.preventDefault();
	        window.parent.maximize();
	        return false;
		}
	});
	
	
	
	/// 탑링크 클릭이벤트
	$('.lnk').click(function () {
		//var _content = parent.frames["mid"]["content"];
		var _content = parent.document.getElementById('content').contentWindow;
		
		var url = '${pageContext.request.contextPath}';
		var popsize = "width=1100, height=750";

		switch($(this).attr('id')){
			
			case "btnHome" :
				parent.location.reload();
				break;
			
			case "btnDash" : 
				url += '/pms/dash/index.do';
				popsize = "width=1100, height=750";
				closeWindows(); _winList=window.open(url, "dash", 'resizable=yes, status=no, ' + popsize + ', left='+ popupX + ', top='+ popupY + ', screenX='+ popupX + ', screenY= '+ popupY);
				break;
				
			case "btnExtent" : 
				_content.ExtentZoom();		
				break;
			
			case "btnZup" : 
				_content.plusZoom();		
				break;
			
			case "btnZdown" : 
				_content.minusZoom();		
				break;
			
			case "btnDist" : 
				
				_content.addMeasure('length');		
				break;
			
			case "btnArea" : 
				_content.addMeasure('area');		
				break;
			
			case "btnErase" : 
				_content.layerRefresh();		
				break;
			
			case "btnView" : 
				_content.viewInfo();		
				break;
			
			case "btnPrint" : 
				var url = "/fms/printMap.do";
 				//_content.winPopUPCenter(url, '',815, 680, 'no', 'no');;		
				break;
			
			default : 
				popsize = "width=1100, height=540";
				break;
		}
		
	});
	
});

</script>

</head>

 <!-- header -->
  <div id="header" class="header"  >
    <div class="index_navbar"> <a href="#"> <img src="/fms/images/ci_infofms_w.png"  alt="시설관리시스템"> </a> </div>
    <div class="member" ><span>${sessionScope.name}</span> 님 로그인 <a href="#"> <img src="/fms/images/top_logout.png" onclick="javascript:window.top.location.href='/fms/uat/uia/actionLogout.do?login_error=-1';" alt="로그아웃버튼"> </a></div>


  <!--topmenu-->
  <div class="top_nav ">
    <div class="menu_sidebar"  > <a href="#" ><img src="/fms/images/icon_top_menu.png"  alt="전체메뉴"></a> </div>
    <div class="menu_nav "  >
      <ul  class="menu_nav " >
        <li><a href="#" class="lnk" id="btnDash"><img src="/fms/images/icon_top_menu_01.png"  alt="시설관리시스템"> 상황판</a></li>
        <li><a href="#" class="lnk" id="btnHome"><img src="/fms/images/icon_top_menu_02.png"  alt="시설관리시스템"> GIS </a></li>
      </ul>
    </div>
    <div class="map_nav "   >
      <ul  class="map_nav " >
        <li><a href="#" class="lnk" id="btnExtent"><img src="/fms/images/icon_top_map_01.png"  alt="시설관리시스템"> <span class="tooltiptext">원본사이즈</span></a></li>
        <li><a href="#" class="lnk" id="btnZup"><img src="/fms/images/icon_top_map_02.png"  alt="시설관리시스템"> <span class="tooltiptext">확대</span></a></li>
        <li><a href="#" class="lnk" id="btnZdown"><img src="/fms/images/icon_top_map_03.png"  alt="시설관리시스템"><span class="tooltiptext">축소</span> </a></li>
        <li><a href="#" class="lnk" id="btnDist"><img src="/fms/images/icon_top_map_04.png"  alt="시설관리시스템"><span class="tooltiptext">거리재기</span></a></li>
        <li><a href="#" class="lnk" id="btnArea"><img src="/fms/images/icon_top_map_05.png"  alt="시설관리시스템"><span class="tooltiptext">면적재기</span></a></li>
        <li><a href="#" class="lnk" id="btnErase"><img src="/fms/images/icon_top_map_06.png"  alt="시설관리시스템"><span class="tooltiptext">지우기</span></a></li>
        <li><a href="#" class="lnk" id="btnView"><img src="/fms/images/icon_top_map_07.png"  alt="시설관리시스템"><span class="tooltiptext">정보보기</span></a></li>
		<!--         <li><a href="#" class="lnk" id="btnPrint"><img src="/fms/images/icon_top_map_08.png"  alt="시설관리시스템"><span class="tooltiptext">출력</span></a></li> -->
      </ul>
    </div>
  </div>  
  <!--topmenu--> 
    </div>
  <!-- //header --> 


</html>