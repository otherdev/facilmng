<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>시설물관리시스템</title>
<link rel="shortcut icon" href="/fms/images/favicon.ico">

</head>

<script type="text/javascript" src="/fms/js/lib/jquery-1.12.4.js" ></script>
<script type="text/javascript">

var stop = 0;
var stop2 = 220;
var pace = 10;

var minimizeInterval = null;
var current = 220;

var maximizeInterval = null;
var current2 = 0;


$(document).ready(function() {

	window.minimize = function () {
		minimizeInterval = null;
		current = 220;
	    minimizeInterval = setInterval(function () {
	    	//console.log('minimizing...');
	        $('#mid').attr('cols', current + ',*');
	        current -= pace;
	        if (current < stop) 
	            clearInterval(minimizeInterval);
	    },5);
	};

	window.maximize = function () {
		maximizeInterval = null;
		current2 = 0;
		maximizeInterval = setInterval(function () {
	    	//console.log('minimizing...');
	        $('#mid').attr('cols', current2 + ',*');
	        current2 += pace;
	        if (current2 > stop2) 
	            clearInterval(maximizeInterval);
	    },5);
	};
});

</script>

<frameset frameborder="0" framespacing="0" rows="90, *, 30">
	<frame name="_top" src="<c:url value='/EgovPageLink.do?link=main/Top' />" scrolling="no" title="헤더">
	<frameset id="mid" name="_mid" frameborder="0" framespacing="0" cols="220, *" >
		<frame name="_left" src="<c:url value='/pms/main/Left.do' />" scrolling="no" title="메뉴페이지" >
		<frame id="content" name="_content" src="/fms/cons/main.do" title="화면컨테이너" scrolling="no" style="border-left: 1px solid #35495f;">
	</frameset>
	<frame name="_bottom" src="<c:url value='/EgovPageLink.do?link=main/Bottom' />" scrolling="no" title="푸터">
</frameset>


