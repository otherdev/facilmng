<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<title>시설물관리시스템</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<link href="/fms/css/style.css" rel="stylesheet" />
<link href="/fms/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<link rel="shortcut icon" href="/fms/images/favicon.ico">

<script type="text/javascript" src="<c:url value='/js/lib/jquery-1.12.4.js' />" ></script>
<script type="text/javascript" src="<c:url value='/js/pms/cmm.js' />" ></script> 


<style type="text/css">
body.intro-body { 

background-color:#35495f; 

	
}
.login-wrap{width: 100%;height: 100%;vertical-align: middle;text-align: center;}
.login-content{width:1240px; height:720px;background:url(/fms/images/bg_login.png) no-repeat;margin: 8% auto  0}
.login-group{width:450px; margin-left:85px; margin-top:5px;}


.login-box {width:450px; height:140px; border-radius:8px;  background:#35495f; text-align:center; padding:0; margin:0; 
}



.table-login{border:none; margin:0 auto;  width:390px; margin-top:30px;  }
	
.table-login>tbody>tr>td{border:none;padding:4px }
.table-login>tbody>tr>td input{ border-radius:6px}

 .table-login>tbody>tr>th {
	border:none;
	font-size:16px; color:#fff;
	text-align:left;
	padding:4px
}
 .table-login>tbody>tr>th p {
	 background:#88a3be;
	 border-radius:15px;
	 color:#fff;
	border-bottom-width: 0px
}


.btn-login {
	
	background-color: #243446;
	border-color: #07365f; width:90px; height:72px; font-size: 16px;
	color:rgba(255,255,255,0.8)
}
.btn-login.active, .btn-login.focus, .btn-login:active, .btn-login:focus, .btn-login:hover, .open>.dropdown-toggle.btn-login {
	color: #fff;
	background-color: ##07365f;
	
}
.btn-login.active, .btn-login:active, .open>.dropdown-toggle.btn-login {
	background-image: none
}
.btn-login.disabled, .btn-login.disabled.active, .btn-login.disabled.focus, .btn-login.disabled:active, .btn-login.disabled:focus, .btn-login.disabled:hover, .btn-login[disabled], .btn-login[disabled].active, .btn-login[disabled].focus, .btn-login[disabled]:active, .btn-login[disabled]:focus, .btn-login[disabled]:hover, fieldset[disabled] .btn-login, fieldset[disabled] .btn-login.active, fieldset[disabled] .btn-login.focus, fieldset[disabled] .btn-login:active, fieldset[disabled] .btn-login:focus, fieldset[disabled] .btn-login:hover {
	background-color: #69c5bc;
	border-color: #55a9a5
}

</style>



<script type="text/javascript">
<!--
$(document).ready(function() {
	
	//서버메세지체크
    var message = document.loginForm.message.value;
    if (!gfn_isNull(message)) {
        alert(message);
    }
    //getid(document.loginForm);

    
    $("#userId").focus();
});

function actionLogin() {

    if ($.trim(document.loginForm.userId.value) =="") {
        alert("아이디를 입력하세요");
        return false;
    } else if ($.trim(document.loginForm.userPwd.value) =="") {
        alert("비밀번호를 입력하세요");
        return false;
    } else {
        document.loginForm.action="<c:url value='/uat/uia/actionSecurityLogin.do'/>";
        //document.loginForm.j_username.value = document.loginForm.userSe.value + document.loginForm.username.value;
        //document.loginForm.action="<c:url value='/j_spring_security_check'/>";
        document.loginForm.submit();
    }
    
}

function setCookie (name, value, expires) {
    document.cookie = name + "=" + escape (value) + "; path=/; expires=" + expires.toGMTString();
}

function getCookie(Name) {
    var search = Name + "="
    if (document.cookie.length > 0) { // 쿠키가 설정되어 있다면
        offset = document.cookie.indexOf(search)
        if (offset != -1) { // 쿠키가 존재하면
            offset += search.length
            // set index of beginning of value
            end = document.cookie.indexOf(";", offset)
            // 쿠키 값의 마지막 위치 인덱스 번호 설정
            if (end == -1)
                end = document.cookie.length
            return unescape(document.cookie.substring(offset, end))
        }
    }
    return "";
}

function saveid(form) {
    var expdate = new Date();
    // 기본적으로 30일동안 기억하게 함. 일수를 조절하려면 * 30에서 숫자를 조절하면 됨
    if (form.checkId.checked)
        expdate.setTime(expdate.getTime() + 1000 * 3600 * 24 * 30); // 30일
    else
        expdate.setTime(expdate.getTime() - 1); // 쿠키 삭제조건
    setCookie("saveid", form.userId.value, expdate);
}

function getid(form) {
    form.checkId.checked = ((form.userId.value = getCookie("saveid")) != "");
}

//-->
</script>
</head>

<body class="intro-body">
<form id="loginForm" name="loginForm" method="post">


<div class="login-wrap">
    <div class="login-content"  >
      <div class="login-group"  >
        <p style="text-align:center; padding-bottom:20px"> <img src="/fms/images/ci_login.png"  alt="시설물관리시스템"></p>
        <div class="login-box "  >
          <table class="table table-login"  >
            <colgroup>
            <col width="70px">
            <col width="200px">
            <col width="80px">
            </colgroup>
            <tbody>
              <tr>
                <th>아이디</th>
                <td><input type="text" class="form-control" value="" style="width:190px" id="userId" name="userId" tabindex=1  ></td>
                <td rowspan="2"><button id="button" class="btn btn-login " onclick="javascript:actionLogin()" tabindex=3 >
			                  <p><i class="fa fa-lock " aria-hidden="true"></i></p>
			                  		로그인</button></td>
              </tr>
              <tr>
                <th>비밀번호</th>
                <td><input type="password" class="form-control" value="" style="width:190px" title="비밀번호를 입력하세요." id="userPwd" name="userPwd" tabindex=2  ></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
</div>

<input type="hidden" name="message" value="${message}" />
<input type="hidden" name="userSe"  value="USR"/>
<input name="j_username" type="hidden"/>
</form>

</body>
</html>