<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<link href="<c:url value='/css/fileStyle.css' />" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<c:url value='/js/EgovMultiFile.js'/>"></script> 


<script type="text/javaScript" language="javascript" >
var sEduMngYn = "${sessionScope.isAdmYn}";

	
$(document).ready(function() {
	
	
	/*******************************************
	 * 화면초기화
	 *******************************************/

	 // 메시지 처리 
	var sResultMsg = '<c:if test="${!empty resultMsg}"><spring:message code="${resultMsg}" /></c:if>';
	if(sResultMsg.length != 0 ) {
		gfn_alert(sResultMsg);
	}
	
	gfn_init();

	

	var defOpt = "";
	if('${mode}' == 'ADD'){
		defOpt = "--선택하세요--";
	}
	// 구분 콤보 공통코드
	gfn_setCombo("faqCatCde", "FAQ_CAT_CDE", "${eduDataModVO.faqCatCde}" , defOpt, function(){
		// 원인구분 콤보 공통코드
		gfn_setCombo("faqCuzCde", "FAQ_CUZ_CDE", "${eduDataModVO.faqCuzCde}" , defOpt, function(){
		});
	});
	
	
	
	
	
	
	
	/*******************************************
	 * 이벤트설정
	 *******************************************/
	
	
	$("#btnUpdate").hide();
	$("#btnDelete").hide();
	$("#replArea").hide();
	
	if( sEduMngYn == 'Y' ) {
		$("#btnUpdate").show();
		if($("input[name='mode']").val() == 'UPDATE' ){
			$("#btnDelete").show();
		}
		if(sEduMngYn == 'Y'){
			$("#replArea").show();
		} 	
	} 
});



function fnRefresh(){

    document.bizModVO.action = "<c:url value='/pms/fcltMnt/eduFaqInfo.do'/>";
    document.bizModVO.submit();	
}

function fn_search(){
    document.eduDataModVO.mode.value = "VIEW";
    document.eduDataModVO.action = "<c:url value='/pms/fcltMnt/eduFaqList.do'/>";
    document.eduDataModVO.submit();	
}
function fnEduDelete(nSeq){
	gfn_confirm("삭제하시겠습니까?", function(result) {
		if(result){
			document.eduDataModVO.seq.value = nSeq;
		    document.eduDataModVO.mode.value = "DELETE";
		    document.eduDataModVO.action = "<c:url value='/pms/fcltMnt/eduFaqDelete.do'/>";
		    document.eduDataModVO.submit();	
		}
	});
// 	if(!confirm("삭제하시겠습니까?")) return;
	
//     document.eduDataModVO.seq.value = nSeq;
//     document.eduDataModVO.mode.value = "DELETE";
//     document.eduDataModVO.action = "<c:url value='/pms/fcltMnt/eduFaqDelete.do'/>";
//     document.eduDataModVO.submit();	
	
}
function fnEduUpdate(nSeq){
    if(!gfn_formValid())	return;

	gfn_confirm("저장하시겠습니까?", function(result) {
		if(result){
			document.eduDataModVO.seq.value = nSeq;
		    document.eduDataModVO.mode.value = $("input[name='mode']").val(); // "UPDATE";
		    document.eduDataModVO.action = "<c:url value='/pms/fcltMnt/eduFaqUpdate.do'/>";
		    document.eduDataModVO.submit();	
		}
	});
// 	if(!confirm("저장하시겠습니까?")) return;
	
//     document.eduDataModVO.seq.value = nSeq;
//     document.eduDataModVO.mode.value = $("input[name='mode']").val(); // "UPDATE";
//     document.eduDataModVO.action = "<c:url value='/pms/fcltMnt/eduFaqUpdate.do'/>";
//     document.eduDataModVO.submit();		
}

</script>

</head>
<body>
<div id="wrap" class="wrap" >  
  	<!-- header -->
	<div id="header" class="header"  >
		<h1 >FAQ 수정</h1>
  	</div>
  	<!-- // header --> 
				
	    <!-- write -->
	    <form  action="${pageContext.request.contextPath}/pms/fcltMnt/eduFaqUp.do" name="eduDataModVO" method="post" enctype="multipart/form-data"  >
			<!-- 상세정보 작업 삭제시 prameter 전달용 input -->
			<div class="search-group">
				<!-- 	        검색조건 유지 -->
				<input name="mode"            type="hidden" value="<c:out value='${eduDataModVO.mode}'/>" /> 
				<input name="seq"             type="hidden" value="<c:out value='${eduDataModVO.seq}'/>" />
				<input name="pageIndex"       type="hidden" value="<c:out value='${eduDataModVO.pageIndex}'/>" />
				<input name="searchCondition" type="hidden" value="<c:out value='${eduDataModVO.searchCondition}'/>"/>
		        <input name="searchKeyword"   type="hidden" value="<c:out value='${eduDataModVO.searchKeyword}'/>"/>
		        <input name="pageIndex"       type="hidden" value="<c:out value='${eduDataModVO.pageIndex}'/>"/>
	        </div>
	        
	        
	        
		<!-- container -->
		<div id="container"  class="content"  >
		
		    <div class="view_box">
		      <h2 ><span style="float:left">FAQ 수정</span>
		        <div class="btn_group m-b-5 right "  >
				            <button id="btnDelete" class="btn btn-default" onClick="javascript:fnEduDelete('<c:out value="${eduDataModVO.seq}"/>'); return false;"> <img src="/fms/images/icon_btn_delete.png" width="16" height="16" alt=""/> 삭제</button>
				            <button id="btnUpdate" class="btn btn-default" onClick="javascript:fnEduUpdate('<c:out value="${eduDataModVO.seq}"/>'); return false;"> <img src="/fms/images/icon_btn_save.png" width="16" height="16" alt=""/> 저장</button>
				            <button id="button" class="btn btn-default" onClick="javascript:fn_search(); return false;"> <img src="/fms/images/icon_btn_reset.png" width="16" height="16" alt=""/> 목록</button>
		        </div>
		      </h2>
		    
		      <div class="view ">
		        <table border="0" cellpadding="0" cellspacing="0"  class="table table-view-regist " >
		          <colgroup>
		          <col width="114px">
		          <col width="*">
		          <col width="114px">
		          <col width="*">
		      
		          </colgroup>
		            <tbody>
		              <tr>
		                <th class="req">FAQ구분</th>
		                <td>
		                    <select class="reqVal" style="width:410px;"  value="${eduDataModVO.faqCatCde}" id="faqCatCde" name="faqCatCde" onchange="javascript:fnRefresh();"     >
								<c:if test="${mode == 'ADD'}">
			                        <option value="" label="--선택하세요--"/>
		                    	</c:if>
		                    </select>
		                  </td>
		                <th class="">원인구분</th>
		                <td>
		                    <select class="" style="width:410px;"  value="${eduDataModVO.faqCuzCde}" id="faqCuzCde" name="faqCuzCde" onchange="javascript:fnRefresh();"     >
								<c:if test="${mode == 'ADD'}">
			                        <option value="" label="--선택하세요--"/>
		                    	</c:if>
		                    </select>
		                  </td>
		              </tr>
		              <tr>
		                 <th class="req">제목</th>
		                 <td >
		                    <input value="${eduDataModVO.ttl}" id="ttl" name="ttl" class="  reqVal"  size="20"  maxlength="60"  style="width: 410px;"/>
	               		 </td>
		                <th class="">지형지물</th>
		                <td >
							<select name="ftrCde" id="ftrCde" style="width: 410px;" >
								<option value="">전체</option>
								<c:forEach var="cmtFtrcMaList" items="${cmtFtrcMaList}" varStatus="status">
									<option value="${cmtFtrcMaList.ftrCde }" ${eduDataModVO.ftrCde == cmtFtrcMaList.ftrCde ? "selected" : "" } >${cmtFtrcMaList.ftrNam}</option>
								</c:forEach>	
							</select>
		                  </td>
		              </tr>
		              <tr>
		                <th>내용</th>
		                <td colspan="3">
		                    <textarea  id="question" name="question" class="" style="height:180px; width: 943px" rows="60" maxLength="500" >${eduDataModVO.question}</textarea>
		                </td>
		              </tr>
		              <tr id="replArea" >
		                <th>답변</th>
		                <td colspan="3">
		                    <textarea  id="repl" name="repl" class="" style="height:180px; width: 943px" rows="60" maxLength="500" >${eduDataModVO.repl}</textarea>
		                </td>
		              </tr>
		            </tbody>
		        </table>
		      </div>
		    </div>
		    <!-- //list --> 	        

          
		</div>  
  		<!-- //container --> 
	</form>
  
</div>
<!-- //UI Object -->

</body>
</html>



          
          
