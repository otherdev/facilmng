<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>
    
<title>시설물태그매핑관리</title>
<script type="text/javascript">	
var popupX = (screen.availWidth-660)/2;
var popupY = (screen.availHeight-1130)/3;

$(document).ready(function() {
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();
	
	/*******************************************
	 * 이벤트 설정
	 *******************************************/
	
	$('#btnReset').click(function () {
        var frm = $('#tagMapFrm');
        frm.find('input[type=text]').val('');
        frm.find('select').val('');
    });

	$('#btnSearch').click(function () {
		fn_search(1);
	});	
		
	$('#btnExcelDown').click(function() {
		var html = '';
		html += '<form id="excelForm" method="post">';
		html += '	<input type="hidden" name="searchCondition1" value= "' + $('#searchCondition1').val() + '" />';
		html += '	<input type="hidden" name="searchCondition2" value= "' + $('#searchCondition2').val() + '" />';
		html += '	<input type="hidden" name="searchCondition3" value= "' + $('#searchCondition3').val() + '" />';
		html += '	<input type="hidden" name="searchCondition4" value= "' + $('#searchCondition4').val() + '" />';		
		html += '</form>';
		$('#wrap').append(html);
		var url = '${ctxRoot}/pms/fcltMnt/tagMapList/excel/download.do';
		$('#excelForm').attr('action', url);
		$('#excelForm').submit();
	});
	
	$('#btnSave').click(function() {
		
		if("${empty resultList}" == "true"){
			gfn_alert('처리할 내용이 없습니다.');
			return;
		}

		var param = {sqlId:"updateFtrTagMap"};
		
		
		var NumRows = $("#tbList > tbody tr").length
		var value = "";
		var name = "";
		var cols = ["g2Id","ftrCde","ftrIdn","tagId","tagYn"];
		
		var bAddFlag = false;
		var _lst = new Array(); 
		for (var rows = 1; rows <=  NumRows; rows++)
		{ 			
			var ary = new Object();
			
			bAddFlag = false;
			for(var i=0;i<cols.length;i++){
				name  = cols[i];
				value = $("#tbList tr").eq(rows).find("#" + cols[i]).val();				
				ary[name] = (value);
			
				if((name == "tagYn" && value == 'Y') && bAddFlag == false  ){
					bAddFlag = true;
				}
				if((name == "tagId" && (value != '' && value != null)) && bAddFlag == false ){
					bAddFlag = true;
				}				
			}
			
			if(bAddFlag == true){
				_lst.push(ary);
			}
			
		}
       
		/*alert(JSON.stringify(_lst));*/

		gfn_confirm('저장하시겠습니까?',function(ret){			
			if(!ret) return;
			var form = new FormData(document.getElementById($('#tbList')));
					
			
			 gfn_saveCmmList({lst:_lst, sqlId : "updateFtrTagMap"}, function(data){
				if(data.result){
					gfn_alert("데이터가 저장되었습니다.","",function(){
						fn_search();
					});
				}else{
					gfn_alert("데이터 저장에 실패하였습니다. : " + data.error,"E");
				}
			});  
			
		});
	});
});

/* pagination 페이지 링크 function */
function fn_search(pageNo){   		
	if(gfn_isNull(pageNo)){
		pageNo = 1;
	}
   	$('form[name=tagMapFrm] input[name=pageIndex]').val(pageNo);
	$('form[name=tagMapFrm]').submit();
}

function captureReturnKey(e) {
    if(e.keyCode==13 && e.srcElement.type != 'textarea'){
    	removeComma();
       	fn_search();
    }
}
</script>

<style>
.popup_layer {margin: 0px auto 0 auto;}
#popup_layer01{width:100%; left:0; top:0; margin-top:0; margin-left:0;}
</style>

<body>

<div id="wrap" class="wrap" > 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1 >시설물 태그매핑 검색목록</h1>
  </div>
  <!-- // header --> 
  
  
<form name="tagMapFrm" id="tagMapFrm" method="post" action="${ctxRoot}/pms/fcltMnt/tagMapList.do">
  <input type="hidden" name="pageIndex" value="1"/>
  
  <!-- container -->
  <div id="container"  class="content"  > 
    
    <!-- seach -->
    <div class="seach_box" >
      <h2 >검색항목 </h2>
      <div>
        <table border="0" cellpadding="0" cellspacing="0" class="table table-search">
          <caption>
          조회양식
          </caption>
          <colgroup>
          <col width="114px">
          <col width="*">
          </colgroup>
          <tbody>
            <tr>
              <th>관리번호</th>
              <td>
              	<input type="text" name="searchCondition1" id="searchCondition1" maxlength="10" value="${param.searchCondition1 }" onkeypress="captureReturnKey(event)" />
              </td>
            </tr>
            <tr>
              <th>가압장명</th>
              <td>
				<input type="text" name="searchCondition2" id="searchCondition2" maxlength="50" value="${param.searchCondition2 }" onkeypress="captureReturnKey(event)" />
              </td>
            </tr>
            <tr>
              <th>세부시설명</th>
              <td>
				<input type="text" name="searchCondition3" id="searchCondition3" maxlength="50" value="${param.searchCondition3 }" onkeypress="captureReturnKey(event)" />
              </td>
            </tr>
            <tr>
              <th>태그ID</th>
              <td>
				<input type="text" name="searchCondition4" id="searchCondition4" maxlength="50" value="${param.searchCondition4 }" onkeypress="captureReturnKey(event)" />
              </td>
            </tr>
            <tr>
              <th>-</th>
              <td>
				<input type="text" id="" name="" readonly class="disable" />
              </td>
            </tr>
            <tr>
              <th>-</th>
              <td>
              	<input type="text" id="" name="" readonly class="disable" />
              </td>
            </tr>
            <tr>
               <th>-</th>
              <td>
              	<input type="text" id="" name="" readonly class="disable" />
              </td>
            </tr>            
            <tr>
               <th>-</th>
              <td>
              	<input type="text" id="" name="" readonly class="disable" />
              </td>
            </tr>
            <tr>
               <th>-</th>
              <td>
              	<input type="text" id="" name="" readonly class="disable" />
              </td>
            </tr>
            <tr>
               <th>-</th>
              <td>
              	<input type="text" id="" name="" readonly class="disable" />
              </td>
            </tr>
            <tr>
              <th>-</th>
              <td>
              	<input type="text" id="" name="" readonly class="disable" />
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="btn_group m-t-10 "  >
        <button id="btnReset" class="btn btn-search "  type="button" style="width:114px"><img src="/fms/images/icon_btn_reset.png" width="16" height="16" alt=""/> 초기화 </button>
        <button id="btnSearch" class="btn btn-search " type="button" style="width:132px"><img src="/fms/images/icon_btn_search.png" width="16" height="16" alt=""/> 검색 </button>
      </div>
    </div>
    <!-- //seach --> 
    
    <!-- list -->
    <div class="list_box">
      <h2 ><span style="float:left">시설물 태그매핑 관리(${paginationInfo.totalRecordCount }건)</span>
        <div class="btn_group m-b-5 right " >
          <button id="btnSave" type="button" class="btn btn-default " > <img src="/fms/images/icon_btn_save.png" width="16" height="16" alt=""/>저장</button>
          <button id="btnExcelDown" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_download.png" width="16" height="16" alt=""/> 다운로드 </button>
        </div>
      </h2>
      <div class="list " >      	
        <table id="tbList" name="tbList" border="0" cellpadding="0" cellspacing="0"  class="table  table-striped table-list center " style="white-space: nowrap;">
          <colgroup>
          <col width="80px">
          <col width="100px">
          <col width="80px">
          <col width="100px">
          <col width="100px">
          <col width="*">
          </colgroup>
          <thead>
            <tr>
            	<th>일련번호</th>
				<th>지형지물명</th>
				<th>관리번호</th>
				<th>가압장명</th>
				<th>세부시설명</th>
				<th>태그ID</th>
            </tr>
          </thead>
          <tbody>
			<c:forEach var="list" items="${resultList}" varStatus="listIndex">									
				<tr>	
					<td>${list.g2Id}</td>
					<td>${list.ftrNam}</td>
					<td>${list.ftrIdn}</td>
					<td class="left">${list.prsNam}</td>
					<td class="left">${list.attNam}</td>
					<td>
						<input type="hidden" id="g2Id" name="g2Id" value="${list.g2Id}"/>
						<input type="hidden" id="ftrCde" name="ftrCde" value="${list.ftrCde}"/>
						<input type="hidden" id="ftrIdn" name="ftrIdn" value="${list.ftrIdn}"/>
						<input type="text" id="tagId" style="width:100%" maxlength="30" name="tagId" value="${list.tagId}"/>
						<input type="hidden" id="tagYn" name="tagYn" value="${list.tagYn}"/>
					</td>
				</tr>
			</c:forEach>
			<c:if test="${empty resultList}">
				<tr>
					<td colspan="6" style="color:black;text-align:center;">검색 결과가 없습니다.</td>
				</tr>
			</c:if>         
          </tbody>
        </table>
      </div>
		<!-- Paging -->
      <div style="width:100%; text-align:center">
        <ul class="pagination pagination-sm  " >
			<ui:pagination paginationInfo = "${paginationInfo}"  type="image" jsFunction="fn_search" />
        </ul>    
      </div>      
    </div>
    <!-- //list -->    
  </div>  
  <!-- //container -->  
</form>  
</div>
</body>
</html>