<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

	<script type="text/javascript">
	var currentInitialize = function() {

	};
	</script>
	
	
	
<div id="wrap" class="wrap" > 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1 >시설물장애관리</h1>
  </div>
  <!-- // header --> 

		<form method="" action="">
		
		
		
  <!-- container -->
  <div id="container"  class="content"  > 
    
    <!-- list -->
    <div class="regist-box2 ">
      <h2 ><span>시설물장애 정보</span>
        <div class="btn_group m-b-5 right "  >         
        </div>
      </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table table-view-regist " >
          <colgroup>
          <col width="170px">
          <col width="*">
          <col width="170px">
          <col width="*">
      
          </colgroup>
          <tbody>
							<tr>
								<th>지형지물</th>
								<td>
									<select id="ftrCde" name="ftrCde" disabled >
										<c:forEach var="cmtFtrcMaList" items="${cmtFtrcMaList}" varStatus="status">
											<option value="${cmtFtrcMaList.ftrCde }" ${resultVO.ftrCde == cmtFtrcMaList.ftrCde ? "selected" : "" } >${cmtFtrcMaList.ftrNam}</option>
										</c:forEach>
									</select>
								</td>
								<th>관리기관</th>
								<td>
									<select id="mngCde" name="mngCde">
										<option value="">전체</option>
										<c:forEach var="cdeList" items="${dataCodeList}" varStatus="status">
											<option value="${cdeList.codeCode }" ${resultVO.mngCde == cdeList.codeCode ? "selected" : "" } >${cdeList.codeAlias}</option>
										</c:forEach>	
									</select>
								</td>
							</tr>
							<tr>
								<th>처리자</th>
								<td>
									<input type="text" id="retUsr" name="retUsr" value="${resultVO.retUsr}"/>
								</td>
								<th>지역/설비</th>
								<td>
									<select>
										<option></option>
									</select>
								</td>
							</tr>
							<tr>
								<th>처리자 연락처</th>
								<td>
									<input type="text" id="retTel" name="retTel" value="${resultVO.retTel}"/>
								</td>
								<th>처리일시</th>
								<td>
									<input type="text" id="retYmd" name="retYmd" class="datepicker" value="${resultVO.retYmd}"/>
								</td>
							</tr>
							<tr>
								<th>원인</th>
								<td colspan="3">
									<input type="text" id="cauMsg" name="cauMsg" value="${resultVO.cauMsg}"/>
								</td>
							</tr>
							<tr>
								<th>해결방안</th>
								<td colspan="2">
									<input type="text" id="" name="" />
								</td>
								<td style="border:none; position:relative">
									<a href="javascript:;" style="position:absolute;right:40px;top:5px"><img src="${rscRoot}/dist/images/popup/search_btn.png" alt="해결방안검색" /></a>
								</td>
							</tr>
						</tbody>
          </tbody>
        </table>
      </div>
    </div>
    <!-- //list --> 
    
  </div>
  
  <!-- //container --> 
</form>
  
</div>
<!-- //UI Object -->

</body>
</html>
