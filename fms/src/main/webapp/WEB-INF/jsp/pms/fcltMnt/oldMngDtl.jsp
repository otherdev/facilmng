<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<script type="text/javascript">

$(document).ready(function () {
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();
			
	//주요도
	gfn_setCombo("impCde", "IMP_CDE", "","선택", function(){});	
	$('#impCde').val('${oldMngInfo.impCde}');
	fn_chkPrdCde('${oldMngInfo.impCde}');
	/*******************************************
	 * 이벤트 설정
	 *******************************************/
	 
	$("#btnSave").click(function(e) {				
		// 저장
		gfn_confirm("저장하시겠습니까?", function(ret){
			if(!ret) return;
			//저장처리
			gfn_saveCmm(
				{ sqlId: 'updateOldMng'
			     ,data:{ g2Id:'${param.g2Id}'
			     	    ,impCde : $('#impCde').val()
			           }
			    }
				, function(data){
					if (data.result) {
						gfn_alert("저장되었습니다.");						
						fn_reload();
						opener.fn_search();
					}
					else{
						var err_msg = "저장에 실패하였습니다.";
						try{
							err_msg = data.error;
						}catch(e){}
						gfn_alert(err_msg,"E");
						fn_reload();
						opener.fn_search();
						return;
					}
			});
		});
	});
});

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode == 46 || charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    
    // Textbox value
    var _value = event.srcElement.value; 
    var val = $("#chkPrdAmt").val();

    // 소수점(.)이 두번 이상 나오지 못하게
    var _pattern0 = /^\d*[.]\d*$/; // 현재 value값에 소수점(.) 이 있으면 . 입력불가

    if (_pattern0.test(_value)) {
        if (charCode == 46) {
            return false;
        }
    }
    
    // 2자리 숫자만 입력가능
    var _pattern1 = /^\d{2}$/; // 현재 value값이 3자리 숫자이면 . 만 입력가능
    if (_pattern1.test(_value)) {
        if (charCode != 46) {
        	alert("두자리 숫자만 입력가능합니다.");
            return false;
        }

    }

    // 소수점 둘째자리까지만 입력가능
    var _pattern2 = /^\d*[.]\d{0}$/; // 현재 value값이 소수점 둘째짜리 숫자이면 더이상 입력 불가
    if (_pattern2.test(_value)){
        alert("소수점 둘째자리까지만 입력가능합니다.");
        return false;

    }
    return true;
}

function fn_chkPrdCde(val){
		
	if(val == "" || val == null || val == "undefined" ){
		$("#chkPrdAmt").val("");
		$("#chkPrdAmt").prop( "disabled", true );
	}else{
		$("#chkPrdAmt").prop( "disabled", false );
	}
}

var fn_reload = function(){
	location.reload();
}

</script>
<body>

<div id="wrap" class="wrap" >  
  	<!-- header -->
	<div id="header" class="header"  >
		<h1 >시설물 가동시간 및 점검이력</h1>
  	</div>
  	<!-- // header --> 

	<form name="oldMngSaveFrm" id="oldMngSaveFrm" method="post">
		<div id="frmdiv"></div>
		<input type="hidden" id="g2Id" name="g2Id" value="${param.g2Id}" />
		<input type="hidden" id="ftrCde" name="ftrCde" value="${param.ftrCde}" />
		<input type="hidden" id="ftrIdn" name="ftrIdn" value="${param.ftrIdn}" />
			   
		<!-- container  -->
		<div id="container"  class="content"  >    
	    	<div class="regist-box2 ">
	    		<h2 ><span>시설물정보</span>
			        <div class="btn_group m-b-5 right "  >         
			          <button id="btnSave" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_save.png" width="16" height="16" alt=""/> 저장 </button>
			        </div>
			     </h2>
		      <div class="view ">
		        <table border="0" cellpadding="0" cellspacing="0" class="table table-view-regist " >
		          <colgroup>
			          <col width="120px">
			          <col width="200px">
			          <col width="120px">
			          <col width="200px">		      
		          </colgroup>
		          <tbody>
					<tr>
						<th>지형지물명</th>
						<td>
						    ${oldMngInfo.ftrNam}
						</td>
						<th>관리기관</th>
						<td>
							${oldMngInfo.ftrIdn}
						</td>						
					</tr>
					<tr>
						<th>주요도</th>
						<td>
							<select id="impCde" name="impCde" style="width:80px">
								<option value="">선택</option>
							</select>		
						</td>
						<th>행정읍</th>
						<td>
							${oldMngInfo.hjdNam}			
						</td>
					</tr>
					<tr>
						<th>내구연한</th>
						<td>
							${oldMngInfo.lmtYear}
						</td>
						<th>설치년도</th>
						<td>
							${oldMngInfo.creYy}			
						</td>
					</tr>
					<tr>
						<th>노후등급</th>
						<td>
							${oldMngInfo.oldLvl}
						</td>
						<th>점검주기</th>
						<td>${oldMngInfo.chkPrd}</td>
					</tr>
		          </tbody>
		        </table>
		      </div>
		    </div>
		    <!-- //list --> 
    	    <!-- table 2 -->
		    <div class="regist-box2 ">
		    	<div class="btn_group m-b-5 m-r-5 m-t-5 left " >
		        	<h2 class="m-b-5">점검이력</h2>
		      	</div>
			    <!--tab list-->
			    <div class="list-regist">			     
			    	<table border="0" cellpadding="0" cellspacing="0"  class="table table-list-regist " >
			        	<colgroup>
			           		<col width="80px">
			          		<col width="100px">
			           		<col width="100px">          
			           		<col width="*">
			          	</colgroup>
			          	<thead>
				            <tr>
				              	<th>일련번호</th>
				              	<th>점검일자</th>
				              	<th>점검구분</th>
				              	<th>점검내용</th>
				            </tr>
			          	</thead>
						<tbody id="sub_table">
							<c:if test="${fn:length(oldMngList) == 0}">
								<tr class="no-rows"><td colspan="4"> 조회내역이 없습니다. </td></tr>
							</c:if>
							<c:forEach var="list" items="${oldMngList}" varStatus="listIndex">
								<tr class="viewPopup">
									<td>${list.sclNum }</td>
									<td>${list.chkResultYmd}</td>
									<td>${list.sclNam}</td>
									<td class="left">${list.chkCtnt}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>   
			    </div>
			</div>    
		    <!-- table 3 --> 
		</div>  
  		<!-- //container --> 
	</form>  
</div>
<!-- //UI Object -->

</body>
</html>

