<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<body>	
<script type="text/javascript">
var addLink = '${ctxRoot}/pms/fcltMnt/sppExpdMngAdd.do';
var viewLink = '${ctxRoot}/pms/fcltMnt/sppExpdMngDtl.do';
var listLink = '${ctxRoot}/pms/fcltMnt/sppExpdMngList.do';

$(document).ready(function() {
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();
	
	
	 /*******************************************
	 * 이벤트 설정
	 *******************************************/
	$('#searchCondition4').datepicker();
    $('#searchCondition4').datepicker("option", "maxDate", $ ("#searchCondition5").val());
    $('#searchCondition4').datepicker("option", "onClose", function ( selectedDate ) {
    	$("#searchCondition5").datepicker( "option", "minDate", selectedDate );
    });
 
    $('#searchCondition5').datepicker();
    $('#searchCondition5').datepicker("option", "minDate", $ ("#searchCondition4").val());
    $('#searchCondition5').datepicker("option", "onClose", function ( selectedDate ) {
    	$("#searchCondition4").datepicker( "option", "maxDate", selectedDate );
    });

	$("#btnReset").click(function () {
		var frm = $('#pdjtMaListfrm');
		frm.find('input[type=text]').val('');
		frm.find('select').val('');
	});
	
	$('#btnSearch').click(function () {
		fn_search(1);
	});
	
	$('#btnCrtPop').click(function(e) {
		var centerPos = parseInt( $(screen).get(0).width ) / 2;
		var windowCenterPos = 858 / 2;
		var leftPos = centerPos - windowCenterPos;
		
		var _win = window.open(addLink, this.title, "left="+leftPos+",top=100, width=690, height=440, menubar=no, location=no, resizable=no, toolbar=no, status=no");
		_win.focus();
	});
	
	$('#btnExcelDown').click(function() {
		var html = '';
		html += '<form id="excelForm" method="post">';
		html += '	<input type="hidden" name="searchCondition1" value= "' + $('#searchCondition1').val() + '" />';
		html += '	<input type="hidden" name="searchCondition2" value= "' + $('#searchCondition2').val() + '" />';
		html += '	<input type="hidden" name="searchCondition3" value= "' + $('#searchCondition3').val() + '" />';
		html += '	<input type="hidden" name="searchCondition4" value= "' + $('#searchCondition4').val() + '" />';
		html += '	<input type="hidden" name="searchCondition5" value= "' + $('#searchCondition5').val() + '" />';
		html += '	<input type="hidden" name="searchCondition6" value= "' + $('#searchCondition6').val() + '" />';
		html += '	<input type="hidden" name="searchCondition7" value= "' + $('#searchCondition7').val() + '" />';
		html += '	<input type="hidden" name="searchCondition8" value= "' + $('#searchCondition8').val() + '" />';
		html += '</form>';
		$('#wrap').append(html);
		$('#excelForm').attr('action', '${ctxRoot}/pdjt/excel/download.do');
		$('#excelForm').submit();
	});
	
	//상세팝업
	$(".dtlPopup").click(function(event){

		var dtlLink = '${ctxRoot}/pms/fcltMnt/sppExpdMngDtl.do?pdhNum='+$(this).attr('pdhNum');
		var centerPos = parseInt( $(screen).get(0).width ) / 2;
		var windowCenterPos = 858 / 2;
		var leftPos = centerPos - windowCenterPos;

		var _win = "";
		try{ opener.closeChild(); }catch(e){}
		_win = window.open(dtlLink, "child", "left="+leftPos+",top=100, width=690, height=410, menubar=no, location=no, resizable=no, toolbar=no, status=no");
		_win.focus();
		try{ opener._winChild = _win; }catch(e){}
		
	});
});

/* pagination 페이지 링크 function */
function fn_search(pageNo){
	if(gfn_isNull(pageNo)){
		pageNo = 1;
	}	
	$('form[name=pdjtMaListfrm] input[name=pageIndex]').val(pageNo);
	$('#pdjtMaListfrm').attr('action', listLink);
	$('form[name=pdjtMaListfrm]').submit();
}

</script>
	
<div id="wrap" class="wrap" > 
  <div id="header" class="header"  >
    <h1 >예비품/소모품관리 검색목록</h1>
  </div>
	
		<form:form  commandName="searchVO" id="pdjtMaListfrm" name="pdjtMaListfrm" method="post" action="${ctxRoot}/pms/fcltMnt/sppExpdMngList.do">
			<input type="hidden" name="pageIndex" value="1"/>
			
			
  <!-- container -->
  <div id="container"  class="content"  > 
				
				
    <div class="seach_box" >
      <h2 >검색항목 </h2>
      <div>
        <table border="0" cellpadding="0" cellspacing="0" class="table table-search">
          <colgroup>
          <col width="114px">
          <col width="*">
          </colgroup>
          
			<tbody>
				<tr>
					<th>관리기관</th>
					<td>
						<select name="searchCondition1" id="searchCondition1">
							<option value="">전체</option>
							<c:forEach var="cdeList" items="${dataCodeList}" varStatus="status">
								<option value="${cdeList.codeCode }" ${param.searchCondition1 == cdeList.codeCode ? "selected" : "" } >${cdeList.codeAlias}</option>
							</c:forEach>	
						</select>
					</td>
				</tr>
				<tr>
					<th>지형지물</th>
					<td>
						<select name="searchCondition2" id="searchCondition2">
							<option value="">전체</option>
							<c:forEach var="cmtFtrcMaList" items="${cmtFtrcMaList}" varStatus="status">
								<option value="${cmtFtrcMaList.ftrCde }" ${param.searchCondition2 == cmtFtrcMaList.ftrCde ? "selected" : "" } >${cmtFtrcMaList.ftrNam}</option>
							</c:forEach>	
						</select>
					</td>
				</tr>
				<tr>
					<th>행정동</th>
					<td>
						<select name="searchCondition3" id="searchCondition3">
							<option value="">전체</option>
							<c:forEach var="cmtList" items="${cmtAdarMaList}" varStatus="status">
								<option value="${cmtList.hjdCde }" ${param.searchCondition3 == cmtList.hjdCde ? "selected" : "" } >${cmtList.hjdNam}</option>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<th>입출고일자[이상]</th>
					<td>
						<!-- css쪽으로 설정 이동 할것 -->
						<input type="text" class="datepicker"  name="searchCondition4" id="searchCondition4" value="${param.searchCondition4}"/>
					</td>
				</tr>
				<tr>
					<th>입출고일자[이하]</th>
					<td>
						<input type="text" class="datepicker"  name="searchCondition5" id="searchCondition5" value="${param.searchCondition5}"/>
					</td>
				</tr>
				<tr>
					<th>입출고 구분</th>
					<td>
						<select name="searchCondition6" id="searchCondition6">
							<option value="">전체</option>
						<option value="I" ${param.searchCondition6 == 'I' ? "selected" : "" }>입고</option>
						<option value="O" ${param.searchCondition6 == 'O' ? "selected" : "" }>출고</option>
							 	
						</select>
					</td>
				</tr>
				<tr>
					<th>소모품/예비품 명</th>
					<td>
						<input type="text" id="searchCondition7" name="searchCondition7" maxlength="50"  value="${param.searchCondition7}"/>
					</td>
				</tr>
				<tr>
					<th>-</th>
					<td>	
						<input type="text" id="" name="" readonly class="disable" />																					
					</td>
				</tr>
				<tr>
					<th>-</th>
					<td>	
						<input type="text" id="" name="" readonly class="disable" />																					
					</td>
				</tr>
				<tr>
					<th>-</th>
					<td>	
						<input type="text" id="" name="" readonly class="disable" />																					
					</td>
				</tr>
				<tr>
					<th>-</th>
					<td>	
						<input type="text" id="" name="" readonly class="disable" />																					
					</td>
				</tr>
			</tbody>
		</table>

      <div class="btn_group m-t-10 "  >
        <button id="btnReset" class="btn btn-search "   style="width:114px"><img src="/fms/images/icon_btn_reset.png" width="16" height="16" alt=""/> 초기화 </button>
        <button id="btnSearch" class="btn btn-search "  style="width:132px"><img src="/fms/images/icon_btn_search.png" width="16" height="16" alt=""/> 검색 </button>
      </div>
      
	</div>
	</div>
	<!-- /box_left -->



    <div class="list_box">
      <h2 ><span style="float:left">목록 | 예비품/소모품 관리(${totCnt}건)</span>
        <div class="btn_group m-b-5 right "  >
          <button id="btnCrtPop" type="button" class="btn btn-default " > <img src="/fms/images/icon_btn_regist.png" width="16" height="16" alt=""/>등록 </button>
          <button id="btnExcelDown" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_download.png" width="16" height="16" alt=""/> 다운로드 </button>
        </div>
      </h2>
						
	  <div class="list ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-striped table-list center " style="white-space: nowrap;">
				<thead>
					<tr>
						<th>입출고번호</th> 
						<th>소모품/예비품 명</th>
						<th>입출고 구분</th>
						<th>단위</th>
						<th>수량</th>
						<th>입출고일시</th>
						<th>행정동</th>
						<th>관리기관</th>
						<th>지형지물</th>
						<th>사용자</th>
					</tr>
				</thead>
				
				<tbody>
					<c:forEach var="list" items="${resultList}" varStatus="listIndex">																			
						<!-- tr class="${listIndex.index % 2 == 0 ? '' : 'tr_back01' } popup-link" href="${ctxRoot}/pms/fcltMnt/sppExpdMngDtl.do?pdhNum=${list.pdhNum}" popup-data="width=690, height=410"  -->
						<tr>
							<td class="dtlPopup line_right" style="cursor:pointer" pdhNum="${list.pdhNum}" >${list.pdhNum}</td>
							<td class="dtlPopup" style="cursor:pointer" pdhNum="${list.pdhNum}" >${list.pdtNam}</td>
							<td class="dtlPopup" style="cursor:pointer" pdhNum="${list.pdhNum}" >
								<c:choose>
									<c:when test="${list.iotCde eq 'I'}">
										 입고
									</c:when>
									<c:when test="${list.iotCde eq 'O'}">
										 출고
									</c:when>
								</c:choose>
							</td>
							<td class="dtlPopup" style="cursor:pointer" pdhNum="${list.pdhNum}" >${list.pdtUnt}</td>
							<td class="dtlPopup" style="cursor:pointer" pdhNum="${list.pdhNum}" >${list.pdhCnt}</td>
							<td class="dtlPopup" style="cursor:pointer" pdhNum="${list.pdhNum}" >${list.iotYmd}</td>
							<td class="dtlPopup" style="cursor:pointer" pdhNum="${list.pdhNum}" >${mapAdarMa[list.hjdCde]}</td>
							<td class="dtlPopup" style="cursor:pointer" pdhNum="${list.pdhNum}" >${mapMngCde[list.mngCde]}</td>
							<td class="dtlPopup" style="cursor:pointer" pdhNum="${list.pdhNum}" >${mapFtrcMa[list.ftrCde]}</td>
							<td class="dtlPopup line_right" style="cursor:pointer" pdhNum="${list.pdhNum}" >${list.iotUsr}</td>
						</tr>
					</c:forEach>
	
					<c:if test="${empty resultList}">
						<tr>
							<td colspan="10" style="color:black;text-align:center;">검색 결과가 없습니다.</td>
						</tr>
					</c:if>
				</tbody>
			</table>
		</div>

      <div style="width:100%; text-align:center">
        <ul class="pagination pagination-sm  " >
			<ui:pagination paginationInfo = "${paginationInfo}"  type="image" jsFunction="fn_search" />
        </ul>
      </div>
		
	</div>
	<!-- /box_right -->


	</div>
</form:form>
</div>


</body>

</html>
