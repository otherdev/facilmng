<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<script type="text/javascript">
	
	var deleteLinkFlgp = '${ctxRoot}/cons/flgpMaDelete.do';
	
	if (location.search.indexOf('action=added') > -1) {
		alert('저장했습니다.');
		window.opener.location.reload();
		window.close();
	} else if (location.search.indexOf('action=upded') > -1) {
		alert('수정했습니다.');
		window.opener.location.reload();
	} else if (location.search.indexOf('action=deleted') > -1) {
		alert('삭제했습니다.');
		window.opener.location.reload();
		window.close();
	}	
	
	var setAddMode = function() {
		$('div.sub_table_wrap').css("height", "260px");
		$('#chkwDtViewUpdateForm').hide();
	}
	
	$(document).ready(function(){
		
		/*******************************************
		 * 화면초기화
		 *******************************************/
		gfn_init();
			
		 /*******************************************
		 * 이벤트 설정
		 *******************************************/
		
		if("${sclNum}" == "") {
			setAddMode();
		}
		
		if($('select[name=ckmCde]').val() != "") {
			changeCkmCde($('select[name=ckmCde]').val());
		}
		
		$('#inspAddBtn').click(function () {
			window.open('${ctxRoot}/insp/inspMaAdd.do?sclNum=${wttChscMa.sclNum}', "작업자등록", 'status=no, width=565, height=330');
		});
		
		$('#chkwAddBtn').click(function () {
			if (!'${wttChscMa.sclNum}') {
				gfn_alert('일반정보를 먼저 저장해주세요.');
				return false;
			}
			
			window.open('${ctxRoot}/chkw/chkwDtAdd.do?sclNum=${wttChscMa.sclNum}&ftrIdn=${wttChscMa.ftrIdn}', "작업추가", 'status=no, width=500, height=352');
		});
				
		// 작업내역 삭제
		$('#btnSubDel').click(function () {
			if (confirm('정말 삭제하시겠습니까?')) {
				$('#chkwDtViewDeleteForm').submit();		
			}		
			
		});
		
		// 작업결과 업데이트
		$('#btnSubSave').click(function () {
			var viewForm = $('#chkwDtViewUpdateForm');
			var inhNum = viewForm.find('#inhNum').val();

			if ($("#ftrCde").val()=='' || $("#ftrIdn").val()=='') {
				gfn_alert('작업내역을 선택해주세요.','',function(){
					if($("#ftrCde").val()=='') {
						$('#ftrCde').focus();
					} else if($("#ftrIdn").val()=='') {
						$('#ftrIdn').focus();
					} 
				});				
				return false;
			}
			
			if ($("#chkYmd").val()=='' || $("#chkYmd").val()==' ') {
				gfn_alert('점검일자를 입력해주세요.','',function(){
					$('#chkYmd').focus();
				});				
				return false;
			}
			
			var worDat = viewForm.find('input:radio[name="chkStt"]:checked').val();
			if (!worDat) {
				gfn_alert('점검상태 항목을 선택해 주세요.','',function(){
					$('input:radio[name="chkStt"]').focus();
				});				
				return false;	
			} 
			var infTfs = viewForm.find('input:radio[name="chkDst"]:checked').val();
			if (!infTfs) {
				gfn_alert('노후화상태 항목을 선택해 주세요.','',function(){
					$('input:radio[name="chkDst"]').focus();
				});				
				return false;
			}
			
			var inhStt = viewForm.find('input:radio[name="chkDlv"]:checked').val();
			if($('input:radio[name="chkDst"]:checked').val()=="1" ) {				
				if (!inhStt) {
					gfn_alert('노후화정도 항목을 선택해 주세요.','',function(){
						$('input:radio[name="chkDst"]').focus();	
					});					
					return false;
				}
			}
			
			
/* 			$('#worDat').val(worDat);
			$('#infTfs').val(infTfs);
			$('#inhStt').val(inhStt); */
			$.ajax({
				url: $('#chkwDtViewUpdateForm').attr('action'),
				type:'post',
				data: $('#chkwDtViewUpdateForm').serialize(),
				success:function(data) {
					try{
					
						if(data.data.msg == "true") {
							changeCkmCde($('select[name=ckmCde]').val());
							gfn_alert('저장되었습니다.');
						}
					
					}catch(e) {}

				}
			});
			//viewForm.submit();			
			
		});
		
		$('#chscSaveBtn').click(function () {
						
			if (!$('#titNam').val()) {
				gfn_alert('제목을 입력하세요.','',function(){
					$('#titNam').focus();
				});				
				return false;
			}
			
			if (!$('#staYmd').val()) {
				gfn_alert('점검시작일을 입력하세요.','',function(){
					$('#staYmd').focus();
				});				
				return false;
			}
			
			if (!$('#endYmd').val()) {
				gfn_alert('점검종료일을 입력하세요.','',function(){
					$('#endYmd').focus();
				});
				return false;
			}
			
			if (new Date($('#staYmd').val()) > new Date($('#endYmd').val())) {
				gfn_alert('점검종료일이 점검시작일 보다 전일 수 없습니다.','',function(){
					$('#staYmd').focus();
				});
				
				return false;
			}
			
			if ($('#cycle').val() != 'one' && !$('#cycleEnd').val()) {
				gfn_alert('점검 반복 생성은 점검주기종료일을 입력해야합니다.','',function(){
					$('#cycle').focus();
				});				
				return false;
			}
			
			if ($('#cycleEnd').val() && new Date($('#endYmd').val()) > new Date($('#cycleEnd').val())) {
				gfn_alert('점검주기종료일이 점검종료일 보다 전일 수 없습니다.','',function(){
					$('#cycleEnd').focus();
				});				
				return false;
			}
			
			var date1 = new Date($('#staYmd').val());
			var date2 = new Date($('#endYmd').val());
			var timeDiff = Math.abs(date2.getTime() - date1.getTime());
			var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
			
			if ($('#cycle').val() == 'week' && diffDays >= 7) {
				gfn_alert('매 주 반복 점검은 시작일 부터 종료일이 7일을 넘길 수 없습니다.','',function(){
					$('#cycle').focus();
				});				
				return false;
			}
			
			if ($('#cycle').val() == 'week' && diffDays > 7) {
				gfn_alert('매 주 반복 점검은 시작일 부터 종료일이 7일을 넘길 수 없습니다.','',function(){
					$('#cycle').focus();
				});
				return false;
			}
			
			if ($('#cycle').val() == 'month' && diffDays >= 28) {
				gfn_alert('매 월 반복 점검은 시작일 부터 종료일이 28일을 넘길 수 없습니다.','',function(){
					$('#cycle').focus();
				});				
				return false;
			}
			
			gfn_confirm("저장하시겠습니까?",function(ret){
				if(!ret) return;
				
				$.ajax({
					url: $('#chscMaUpdForm').attr('action'),
					type:'post',
					data: $('#chscMaUpdForm').serialize(),
					success:function(data) {
						try{
							gfn_alert("데이터가 저장되었습니다.","",function(){
								window.opener.location.reload();
								window.close();
							});							
						
						}catch(e) {
							gfn_alert("데이터 저장에 실패하였습니다.");
						}

					}
				});				
			});
		});
		
		$('#chscDeleteBtn').click(function () {
			
			gfn_confirm("정말 삭제하시겠습니까?",function(ret){
				if(!ret) return;
				
				$.ajax({
					url: $('#chscMaDelForm').attr('action'),
					type:'post',
					data: $('#chscMaDelForm').serialize(),
					success:function(data) {
						try{
							gfn_alert("삭제되었습니다.","",function(){
								window.opener.location.reload();
								window.close();
							});							
						
						}catch(e) {
							gfn_alert("삭제 작업이 실패하였습니다.");
						}

					}
				});				
			});
		});
		
		//이미지 업로드
		$('#fileForm input[name=location]').val(location.href);
		
		$('input[name=files]').change(function () {
			$('#fileForm').submit();
		});

		
		//이미지 삭제
		$('.imgDelBtn').click(function () {
			if (confirm('삭제하시겠습니까?')) {
				var html = '<form id="deleteFlgpMaFrm" method="post">';
					html += '<input type="hidden" name="location" value="' + location.href + '"/>';
					html += '<input type="hidden" name="cntNum" value="${wttChscMa.sclNum}"/>';
					html += '<input type="hidden" name="figNums" value="' + $(this).closest('.item').attr('figNum') + '"/>';
					html += '</form>';
					
				$('#popup_layer44').append(html);
				
				$('#deleteFlgpMaFrm').attr('action', deleteLinkFlgp);
				$('#deleteFlgpMaFrm').submit();
			}
		});
		
		$(".validation").hide();
			
		// 노후화상태에 따른 노후화 정도 validation
		$("input:radio[name=chkDst]").click(function() {
			if($("input:radio[name=chkDst]:checked").val()=="1") {
				$(".validation").show();
			} else {
				$(".validation").hide();
			}
			
		});
	});
		
	function setInsps(usrIdns, inpNams) {
		$('#usrIdns').val(usrIdns);
		$('#inspAddBtn').val(inpNams);
	}
	
	function changeCkmCde(ckmCde) {
		$.ajax({
			url:'${ctxRoot}/chsc/chscCkmList.do',
			type:'post',
			data:{
				sclNum: '${sclNum}',
				ckmCde: ckmCde
			},
			success:function(loadHtml) {
				$("#chscCkmListTable").html(loadHtml);		
			}
		});
	}
	</script>
<style type="text/css">
	.radio_box{
		overflow:hidden;
		padding:5px 0
	}
	.radio_box label,
	.radio_box input{
		display:block;
		float:left;
		
	
	}
	.radio_box label{
		color:#333;margin-right:5px;
	}
	.radio_box input{
		margin-top:2px;
		margin-left:0px;
		margin-right:2px;
		
	}
</style>

<body>



<div id="wrap" class="wrap" > 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1 >일정관리</h1>
  </div>
  <!-- // header --> 

	<form method="POST" action="${ctxRoot }/chsc/chscMaDelete.do" name="chscMaDelForm" id="chscMaDelForm">
		<input type="hidden" name="sclNum" id="sclNum" value="${wttChscMa.sclNum}" />
	</form>
	
	<form method="POST" action="${ctxRoot }/chsc/chscMaSave.do" name="chscMaUpdForm" id="chscMaUpdForm">
		<input type="hidden" name="usrIdns" id="usrIdns" value="${usrIdns }"/>


  <!-- container -->
  <div id="container"  class="content"  > 
    
    <!-- list -->
    <div class="view_box">
      <h2 ><span style="float:left">일반정보</span>
        <div class="btn_group m-b-5 right "  >
          <button id="chscSaveBtn"  type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_save.png" width="16" height="16" alt=""/> 저장 </button>
        </div>
      </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
          <colgroup>
          <col width="114px">
          <col width="154px">
          <col width="114px">
          <col width="154px">
          <col width="114px">
          <col width="153px">
          <col width="114px">
          <col width="153px">
          </colgroup>
          <tbody>
						<tr>
							<th>제목</th>
							<td colspan="7">
								<input type="text" name="titNam" id="titNam" value="${wttChscMa.titNam }" style="width:940px"/>
							</td>
						</tr>
						<tr>
							<th>점검주기설정</th>
							<td>
								<select name="cycle" id="cycle" ${!empty wttChscMa.sclNum ? 'disabled="disabled"' : ''}>
									<option value="one">한번</option>
									<option value="week">매주</option>
									<option value="month">매월</option>
								</select>
							</td>
							<th>점검주기종료일</th>
							<td>
								<input type="text" class="datepicker" name="cycleEnd" id="cycleEnd" value="" ${!empty wttChscMa.sclNum ? 'disabled="disabled"' : ''}/>
							</td>
							<th>점검시작일</th>
							<td>
								<input type="text" class="datepicker" name="staYmd" id="staYmd" value="${wttChscMa.staYmd }"/>
							</td>
							<th>관리기관</th>
							<td>
								<select name="mngCde" id="mngCde">
									<c:forEach var="cdeList" items="${dataCodeList}" varStatus="status">
										<option value="${cdeList.codeCode }" ${wttChscMa.mngCde == cdeList.codeCode ? "selected" : "" }>${cdeList.codeAlias}</option>
									</c:forEach>
								</select>
							</td>
						</tr>
						<tr>
							<th></th>
							<td>
								<input type="text" id="inspAddBtn" name="inpNams" value="${inpNams }" readonly="readonly" />
							</td>
							<th>점검종료일</th>
							<td>
								<input type="text" class="datepicker" name="endYmd" id="endYmd" value="${wttChscMa.endYmd }"/>
							</td>
							<th>점검장소</th>
							<td>
								<input type="text" id="chkPlc" name="chkPlc" value="${wttChscMa.chkPlc }"/>
							</td>
							<th>점검그룹</th>
							<td>
								<select name="ckmCde" id="ckmCde" onchange="changeCkmCde(this.value);">
									<option value="">미지정</option>
									<c:forEach var="ckmList" items="${cmtCkmcMaList}" varStatus="listIndex">
									<option value="${ckmList.ckmCde}" ${wttChscMa.ckmCde == ckmList.ckmCde ? "selected" : "" }>${ckmList.ckmNam}</option>
									</c:forEach>
								</select>
							</td>
						</tr>
						<tr>
							<th>비고</th>
							<td colspan="7">
								<input type="text" id="chkEtc" name="chkEtc" value="${wttChscMa.chkEtc }" class="input-lg" />
							</td>
						</tr>
          </tbody>
        </table>
      </div>
    </div>
    <!-- //list --> 




	
    <!-- list -->
    <div class="view_box">
      <h2 class="m-b-5 ">점검 대상 </h2>
      <div class="tab-list active" style="padding-top:0; 	max-height:170px;">
       <table border="0" cellpadding="0" cellspacing="0"  class="table table-tab  " style="white-space: nowrap;">
          <colgroup>
          <col width="80px">
          <col width="*">
         <col width="*">
          <col width="*">
         <col width="100px">
          <col width="100px">
          <col width="*">
            <col width="*">
           <col width="100px">

          </colgroup>
          
          <thead>
						<tr>
							<th style="line-height:28px; height:28px">순번</th>
							<th>행정동</th>
							<th>설비명</th>
							<th>지형지물</th>
							<th>관리번호</th>
							<th>점검일자</th>
							<th>점검상태</th>
							<th>노후화상태</th>
							<th>점검자</th>
						</tr>
					</thead>
					
				<tbody id="chscCkmListTable" class="sub_table">
				</tbody>         
        </table>
        
      </div>
    </div>
    <!-- //list --> 
    			
	<form action="${ctxRoot }/chkw/chkwDtViewUpdate.do" method="POST" name="chkwDtViewUpdateForm" id="chkwDtViewUpdateForm">
<!-- 		<div class="layer_box layer_box_b" id="chkwDtDiv"> -->
			<input type="hidden" name="chkNum" id="chkNum" value="" />
			<input type="hidden" name="sclNum" id="sclNum" value="${wttChscMa.sclNum}" />
			
    <div class="view_box" id="chkwDtDiv">
      <h2 ><span style="float:left">작업결과</span>
        <div class="btn_group m-b-5 right "  >
          <button id="btnSubDel" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_delete.png" width="16" height="16" alt=""/> 삭제</button>
          <button id="btnSubSave" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_save.png" width="16" height="16" alt=""/> 저장 </button>
        </div>
      </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
          <colgroup>
          <col width="114px">
          <col width="158px">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          </colgroup>
          <tbody>
					<tr>
						<th>지형지물 <img src="/fms/images/icon_table_required.png" width="12" height="12" alt=""/></th>
						<td>
							<input type="hidden" name="ftrCde" value=""/>
							<select id="ftrCde" readonly class="disable" disabled>
								<option value="">선택안함</option>
								<c:forEach var="cmtFtrcMaList" items="${cmtFtrcMaList}" varStatus="status">
									<option value="${cmtFtrcMaList.ftrCde }"  >${cmtFtrcMaList.ftrNam}</option>
								</c:forEach>
							</select>
						</td>
						<th>관리번호 <img src="/fms/images/icon_table_required.png" width="12" height="12" alt=""/></th>
						<td>
							<input type="text" id="ftrIdn" name="ftrIdn" value="" readonly class="disable"/>
						</td>
						<th>점검일자 <img src="/fms/images/icon_table_required.png" width="12" height="12" alt=""/></th>
						<td>
							<input type="text" class="datepicker" name="chkYmd" id="chkYmd" value="" readonly />
						</td>
						
						<th>점검자</th>
						<td>
							<input type="text" id="chkUsr" name="chkUsr" value=""/>
						</td>
						
					</tr>
					<tr>
					
					<th>점검상태 <img src="/fms/images/icon_table_required.png" width="12" height="12" alt=""/></th>
						<td>
							<div class="radio_box">
								<input type="radio" id="chkStt0" name="chkStt" value="0"/>
								<label for="chkStt0">정상</label>
								<input type="radio" id="chkStt1" name="chkStt" value="1" />
								<label for="chkStt1">고장</label>
								<input type="radio" id="chkStt2" name="chkStt" value="2" />
								<label for="chkStt2">처분</label>
							</div>
						</td>
						
						<th>노후화상태 <img src="/fms/images/icon_table_required.png" width="12" height="12" alt=""/></th>
						<td>
							<div class="radio_box">
								<input type="radio" id="chkDst0" name="chkDst" value="0"/>
								<label for="chkDst0">양호</label>
								<input type="radio" id="chkDst1" name="chkDst" value="1" />
								<label for="chkDst1">노후화</label>
							</div>
						</td>
						<th>노후화정도 <img src="/fms/images/icon_table_required.png" width="12" height="12" alt=""/></th>
						<td>
							<div class="radio_box">
								<input type="radio" id="chkDlv0" name="chkDlv" value="0"/>
								<label for="chkDlv0">약함</label>
								<input type="radio" id="chkDlv1" name="chkDlv" value="1"/>
								<label for="chkDlv1">중간</label>
								<input type="radio" id="chkDlv2" name="chkDlv" value="2"/>
								<label for="chkDlv2">심함</label>
							</div>
						</td>
						<th>점검내용</th>
						<td >
							<input type="text" id="chkDat" name="chkDat" value=""/>
 						</td>
						
						
					</tr>
				
          </tbody>
        </table>
        
      </div>
    </div>
    <!-- //list --> 
	</form>
	<!-- /layer_box -->    
    
    
    
    <!-- gallery_list -->
    <div class="gallery-box " >
      <div class="gallery-list" style="height: 150px;">
		<div id="owl_demo_2" class="owl-carousel owl-theme owl-loaded owl-drag">
		<!--         <div class="gallery-btn-left"><a href="#"><img src="/fms/images/img_galley_left.png" alt=""/></a></div> -->
		<!--         <div class="gallery-cont" > <img src="/fms/images/img_noimage.png" style="vertical-align:middle" alt=""/></div> -->



        <div  class="gallery-cont" style="width: 163.6px; margin-right:0"> <img src="/fms/images/img_noimage.png" alt=""/></div>
      </div>
      
      
			<form name="fileForm" id="fileForm" method="post" action="${ctxRoot}/cons/consMaDrawingAdd/save.do" enctype="multipart/form-data">
				<input type="hidden" name="titNam" value="picture"/>
				<input type="hidden" name="cntNum" value="${wttChscMa.sclNum }"/>
				<input type="hidden" name="grpExp" value="picture"/>
				<input type="hidden" name="filVer" value="1.0.0"/>
				<input type="hidden" name="location" value=""/>
					<div class="table_btn table_btn_c">
						<%-- <a href="javascript:;"><img src="${rscRoot}/dist/images/popup/up_btn.png" alt="업로드" /></a>
						<input type="file" name="files">
						--%>
					</div>
			</form>
      
    </div>
    <!-- //gallery_list --> 
    
   
    
  </div>
  <!-- //container --> 
  
</form>   
<!-- 작업결과 삭제기( inhNum가 없음 삭제 안됨.)-->
<form action="${ctxRoot }/chkw/chkwDtDelete.do" method="POST" name="chkwDtViewDeleteForm" id="chkwDtViewDeleteForm">
	<input type="hidden" name="inhNum" id="inhNum" value="${wttChkwDt.inhNum}" />
	<input type="hidden" name="sclNum" id="sclNum" value="${wttChscMa.sclNum}" />
</form>
</div>
<!-- //UI Object -->

</body>
</html>

