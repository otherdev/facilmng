<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<script type="text/javascript" src="<c:url value='/js/pms/fcltMnt/pdjtMaMngList.js' />" ></script>

<body>	
<script type="text/javascript">
var addLink = '${ctxRoot}/pms/fcltMnt/pdjtMaMngInCrt.do';

$(document).ready(function() {
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();
	
	// 공통코드 가져오기
	gfn_selectList({sqlId: 'selectCmmCd', mstCd:'PDT_CAT_CDE'}, function(ret){
		PDT_CAT_LIST = ret.data;
		fn_search();
	});	
	
	//소모품구분
	gfn_setCombo("searchCondition4", "PDT_CAT_CDE", "","Y", function(){});
	
	/*******************************************
	 * 이벤트 설정
	 *******************************************/
	/*검색항목 start*/

	$("#btnReset").click(function () {
		var frm = $('#pdjtMaListfrm');
		frm.find('input[type=text]').val('');
		frm.find('select').val('');
	});
	
	$('#btnSearch').click(function () {
		fn_search();
	});
	
	$('#btnSave').click(function () {
		myGrid.editCellClear(0,0,0);
		myGrid.selectClear();

		// ajax 저장 
		var _lst = fnObj.grid.getList();
		
	    //그리드필수체크
		myGrid.editCellClear(0,0,0);
		myGrid.selectClear();
		if(!myGrid.validateCheck('C') && !myGrid.validateCheck('U') ){
			return false;
		}	 
		
		// 저장
		gfn_confirm("저장하시겠습니까?", function(ret){
			if(!ret) return;
			
			//저장처리
			gfn_saveList(
				"${ctxRoot}/pms/fcltMnt/savePdjtMaMngList.do"
				,{lst: _lst}
				, function(data){
					if (data.result) {
						gfn_alert("저장되었습니다.");						
						fn_search();	
					}
					else{
						var err_msg = "저장에 실패하였습니다.";
						try{
							err_msg = data.error;
						}catch(e){}
						gfn_alert(err_msg,"E");
						fn_search();
						return;
					}
			});			
		});
	});
	
	$('#btnInCrtPop').click(function(e) {
		debugger;
		if(gfn_isNull(keyPdhNum)){
			gfn_alert("소모품 목록의 입고등록 대상을 선택하세요.\n(신규건은 저장 후 사용가능)");
			return;
		}
		fn_inCrtPop(keyPdhNum);
	});
	
	$('#btnExcelDown').click(function() {
		var html = '';
		html += '<form id="excelForm" method="post">';
		html += '	<input type="hidden" name="searchCondition1" value= "' + $('#searchCondition1').val() + '" />';
		html += '	<input type="hidden" name="searchCondition2" value= "' + $('#searchCondition2').val() + '" />';
		html += '	<input type="hidden" name="searchCondition3" value= "' + $('#searchCondition3').val() + '" />';
		html += '	<input type="hidden" name="searchCondition4" value= "' + $('#searchCondition4').val() + '" />';
		html += '</form>';
		$('#wrap').append(html);
		$('#excelForm').attr('action', '${ctxRoot}/pdjt/excel/download.do');
		$('#excelForm').submit();
	});	

});

function fn_inCrtPop(pdhNum){
	
	if(pdhNum == null || pdhNum == ''){		
		return;
	}
	
	var centerPos = parseInt( $(screen).get(0).width ) / 2;
	var windowCenterPos = 858 / 2;
	var leftPos = centerPos - windowCenterPos;
	
	var _win = window.open(addLink + "?pdhNum=" + pdhNum, this.title, "left="+leftPos+",top=100, width=660, height=500, menubar=no, location=no, resizable=no, toolbar=no, status=no");
	_win.focus();
}

/* pagination 페이지 링크 function */
function fn_search(){
	
	keyPdhNum = "";
	var param = {
			 sqlId: 'selectPdjtMaMngList'
		    ,searchCondition1 : $("#searchCondition1").val()
		    ,searchCondition2 : $("#searchCondition2").val() 
		    ,searchCondition3 : $("#searchCondition3").val() 
		    ,searchCondition4 :	$("#searchCondition4").val() 		    
	        };
	
	gfn_selectList(param, function(ret){
		lst = ret.data;		
		// 그리드 초기화		
		fnObj.pageStart();
	});	
}

function captureReturnKey(e) {
    if(e.keyCode==13 && e.srcElement.type != 'textarea'){
    	removeComma();
       	fn_search();
    }
}
</script>
	
<div id="wrap" class="wrap" > 
	<div id="header" class="header"  >
	  <h1 >예비품/소모품관리 검색목록</h1>
	</div>	
	<form id="pdjtMaListfrm" name="pdjtMaListfrm" method="post" action="#">			
		<!-- container -->
		<div id="container"  class="content"> 				
				
		    <div class="seach_box" >
				<h2 >검색항목 </h2>
				<div>
					<table border="0" cellpadding="0" cellspacing="0" class="table table-search">			  
					    <colgroup>
					    <col width="114px">
					    <col width="*">
					    </colgroup>			          
						<tbody>
							<tr>
								<th>품명</th>
								<td>
									<input type="text" name="searchCondition1" id="searchCondition1" value="${param.searchCondition1}" onkeypress="captureReturnKey(event)"/>						
							</td>
						</tr>
						<tr>
							<th>모델 및 규격</th>
							<td>
								<input type="text" name="searchCondition2" id="searchCondition2" value="${param.searchCondition2}" onkeypress="captureReturnKey(event)"/>
							</td>
						</tr>
						<tr>
							<th>제조사</th>
							<td>
								<input type="text" name="searchCondition3" id="searchCondition3" value="${param.searchCondition3}" onkeypress="captureReturnKey(event)"/>
							</td>
						</tr>
						<tr>
							<th>구분</th>
							<td>
								<select name="searchCondition4" id="searchCondition4">
									<option value="">전체</option>						 	
								</select>
							</td>
						</tr>
						<tr>
							<th>-</th>
							<td>
								<input type="text" id="" name="" readonly class="disable" />
							</td>
						</tr>
						<tr>
							<th>-</th>
							<td>
								<input type="text" id="" name="" readonly class="disable" />
							</td>
						</tr>
						<tr>
							<th>-</th>
							<td>
								<input type="text" id="" name="" readonly class="disable" />
							</td>
						</tr>
						<tr>
							<th>-</th>
							<td>	
								<input type="text" id="" name="" readonly class="disable" />																					
							</td>
						</tr>
						<tr>
							<th>-</th>
							<td>	
								<input type="text" id="" name="" readonly class="disable" />																					
							</td>
						</tr>
						<tr>
							<th>-</th>
							<td>	
								<input type="text" id="" name="" readonly class="disable" />																					
							</td>
						</tr>
						<tr>
							<th>-</th>
							<td>	
								<input type="text" id="" name="" readonly class="disable" />																					
							</td>
						</tr>
					</tbody>
				</table>
				
				<div class="btn_group m-t-10 "  >
				  	<button type="button" id="btnReset" class="btn btn-search "  style="width:114px"><img src="/fms/images/icon_btn_reset.png" width="16" height="16" alt=""/> 초기화 </button>
					<button type="button" id="btnSearch" class="btn btn-search " style="width:132px"><img src="/fms/images/icon_btn_search.png" width="16" height="16" alt=""/> 검색 </button>
				</div>
			</div>
		</div>
		<!-- seach_box -->

	    <div class="list_box">
			<h2 ><span style="float:left">목록 | 예비품/소모품 관리</span>
			  <div class="btn_group m-b-5 right " >
			  	<p style="height:27px"></p>			    
			  </div>
			</h2>
							
		  	<div class="list" style="overflow:hidden;border:0;">
		        <div class="btn_group" style="padding:0px 0px 10px 0px;">
		            <input type="button" value="추가" class="AXButton Green" onclick="fnObj.grid.append();"/>
		            <input type="button" value="삭제" class="AXButton Green" onclick="fnObj.grid.remove();"/>		            
		            <input type="button" id="btnSave" value="저장" class="AXButton Blue" style="float:right;"/>
		            <p style="float:right;">&nbsp;</p>		            
		            <button id="btnInCrtPop" type="button" class="AXButton Blue" style="float:right;"><img src="/fms/images/icon_btn_regist.png" style="vertical-align:middle;" width="16" height="16" alt=""/>입고등록 </button>		            		            	        	
		        </div>					  	
				<div id="grdPay" class="table table-striped table-list center" style="width:100%;"></div>
			</div>		
		</div>
		<!-- list_box -->
		</div>
	</form>
</div>
</body>

</html>
