<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<script type="text/javaScript" language="javascript" defer="defer">
var sEduMngYn = "${sessionScope.isAdmYn}";
var sRegId = "<c:out value='${eduDataModVO.regId}'/>";

$(document).ready(function() {
	/*******************************************
	 * 화면초기화
	 *******************************************/

	 // 메시지 처리 
	var sResultMsg = '<c:if test="${!empty resultMsg}"><spring:message code="${resultMsg}" /></c:if>';
	if(sResultMsg.length != 0 ) {
		gfn_alert(sResultMsg);
	}
	


	/*******************************************
	 * 이벤트설정
	 *******************************************/
	$("#btnUpdate").hide();
	$("#btnDelete").hide();
	
	if( sEduMngYn == 'Y'  ) {
		$("#btnUpdate").show();
		$("#btnDelete").show();
	} 
	var objTextarea = $("textarea");
	
	$.each(objTextarea , function(i,e){
		fn_TextareaResize(this);
	});
});



function fnRefresh(){

    document.eduDataModVO.action = "<c:url value='/pms/fcltMnt/eduFaqInfo.do'/>";
    document.eduDataModVO.submit();	
}
function fn_search(){
    document.eduDataModVO.mode.value = "VIEW";
    document.eduDataModVO.action = "<c:url value='/pms/fcltMnt/eduFaqList.do'/>";
    document.eduDataModVO.submit();	
}
function fnEduFaqDelete(nSeq){
	gfn_confirm("삭제하시겠습니까?", function(result) {
		if(result){
			document.eduDataModVO.seq.value = nSeq;
		    document.eduDataModVO.mode.value = "DELETE";
		    document.eduDataModVO.action = "<c:url value='/pms/fcltMnt/eduFaqDelete.do'/>";
		    document.eduDataModVO.submit();	
		}
	});
// 	if(!confirm("삭제하시겠습니까?")) return;
	
//     document.eduDataModVO.seq.value = nSeq;
//     document.eduDataModVO.mode.value = "DELETE";
//     document.eduDataModVO.action = "<c:url value='/pms/fcltMnt/eduFaqDelete.do'/>";
//     document.eduDataModVO.submit();	
	
}
function fnEduFaqUpdate(nSeq){
    document.eduDataModVO.seq.value = nSeq;
    document.eduDataModVO.mode.value = "UPDATE";
    document.eduDataModVO.action = "<c:url value='/pms/fcltMnt/eduFaqUp.do'/>";
    document.eduDataModVO.submit();		
}
function fn_TextareaResize(obj){
	obj.style.height = "1px";
	console.log(obj.id + " = " + (12+obj.scrollHeight));
	obj.style.height = (12+obj.scrollHeight)+"px";
}
</script>

</head>
<body>


<!-- container -->
<div id="wrap" class="wrap" >  
  	<!-- header -->
	<div id="header" class="header"  >
		<h1 >FAQ 정보</h1>
  	</div>
  	<!-- // header --> 
				
	    <!-- write -->
	    <form  action="${pageContext.request.contextPath}/pms/fcltMnt/eduDataInfo.do" name="eduDataModVO" method="post" >
				<input name="mode"            type="hidden" value="<c:out value='${eduDataModVO.mode}'/>"/> 
				<input name="seq"             type="hidden" value="<c:out value='${eduDataModVO.seq}'/>" />
				<input name="faqCatCde"           type="hidden" value="<c:out value='${eduDataModVO.faqCatCde}'/>"/>
				<input name="faqCuzCde"           type="hidden" value="<c:out value='${eduDataModVO.faqCuzCde}'/>"/>
				<input name="pageIndex"       type="hidden" value="<c:out value='${eduDataModVO.pageIndex}'/>" />
				<input name="searchCondition" type="hidden" value="<c:out value='${eduDataModVO.searchCondition}'/>"/>
		        <input name="searchKeyword"   type="hidden" value="<c:out value='${eduDataModVO.searchKeyword}'/>"/>
		        <input name="pageIndex"       type="hidden" value="<c:out value='${eduDataModVO.pageIndex}'/>"/>
	    
	    
		<!-- container -->
		<div id="container"  class="content"  >
		
		    <div class="view_box">
		        <div class="btn_group m-b-5 right "  >
				            <button id="btnDelete" class="btn btn-default" onClick="javascript:fnEduFaqDelete('<c:out value="${eduDataModVO.seq}"/>'); return false;"> <img src="/fms/images/icon_btn_delete.png" width="16" height="16" alt=""/> 삭제</button>
				            <button id="btnUpdate" class="btn btn-default" onClick="javascript:fnEduFaqUpdate('<c:out value="${eduDataModVO.seq}"/>'); return false;"> <img src="/fms/images/icon_btn_save.png" width="16" height="16" alt=""/> 수정</button>
				            <button id="button" class="btn btn-default" onClick="javascript:fn_search(); return false;"> <img src="/fms/images/icon_btn_reset.png" width="16" height="16" alt=""/> 목록</button>
		        </div>

		      <div class="view ">
		        <table border="0" cellpadding="0" cellspacing="0"  class="table table-view-regist " >
		          <colgroup>
		          <col width="*">
		          </colgroup>
		            <tbody>
		                <tr>
		                  <th ><c:out value="${eduDataModVO.ttl2}"/></th>
		                </tr>
		                <tr>
		                  <td  class="view-q" ><textarea readonly style="overflow:hidden; border:0; min-height: 180px; width:100%;" >[질문] <c:out value='${eduDataModVO.question}'/> </textarea></td>
		                </tr>
		                <tr>
		                  <td  class="view-top" > <textarea readonly style="overflow:hidden; border:0; min-height: 180px; width:100%;" ><c:out value='${eduDataModVO.repl}'/> </textarea></td>
		                </tr>
		            </tbody>
		        </table>
		      </div>
		    </div>
		    <!-- //list --> 	        

          
		</div>  
  		<!-- //container --> 
	</form>
  
</div>
<!-- //UI Object -->

</body>
</html>



          
          
