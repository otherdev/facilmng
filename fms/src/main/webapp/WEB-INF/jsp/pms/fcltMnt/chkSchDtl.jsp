<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<style>
	.item{
		width:240px;
		height:160px;
	}
</style>

<script type="text/javascript" src="<c:url value='/js/pms/fcltMnt/chkSchDtl.js' />" ></script>

<script type="text/javascript">
var sclNum = '${sclNum}';

var file_list = []; //사진리스트
//var owl;			//갤러리 object
var cur_row = -1;		//선택결과row
var cur_filSeq = "";	//선택파일순번
var cur_ftrCde = "";	//선택시설물결과
var cur_ftrIdn = "";	//선택시설물결과
var cur_seq = "";		//선택시설물결과순번

var popupX = (screen.availWidth-660)/2;
var popupY= (screen.availHeight-1130)/3;



$(document).ready(function(){
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();

	var msg = '${msg}';
	
	if(msg) {
		alert(msg);			
		try { opener.selfRefresh(); }catch(e){}
		location.href="${ctxRoot}/pms/fcltMnt/chkSchList.do";
	}

	//달력초기화
	//setDatepicker();
	$('.datepicker').each(function(idx, val){
		$(this).val(gfn_toDate($(this).val(),'-'));
	});
	
	
	//갤러리초기화 - 사진불러온 콜백에서 처리
	  
	
	
	// 점검구분 콤보 공통코드
	gfn_setCombo("sclCde", "SCL_CDE", "${chscMaDtl.sclCde}" ,"--선택하세요--", function(){
	});
	
	
	
	//점검결과 그리드 초기화
	gfn_selectList({sqlId: 'selectCmmCd', mstCd:'RPR_CAT_CDE'}, function(ret){
		RPR_CAT_CDE_LIST = ret.data;

		gfn_selectList({sqlId: 'selectCmmCd', mstCd:'RPR_CUZ_CDE'}, function(ret){
			RPR_CUZ_CDE_LIST = ret.data;

			// 점검결과리스트	
			gfn_selectList({sqlId: 'selectChscResultList', 	sclNum:'${sclNum}'}, function(ret){
				lst = ret.data;
				// 그리드 초기화
				fnObj.pageStart();

				//점검결과선택 후처리
				fn_initRstDtl();
			});	
		});	
	});	

	// 갤러리세팅
    $(".galery").on("error", function(){
        $(this).attr('src', '${ctxRoot}/images/img_noimage.png');
    });	
	
	 /*******************************************
	 * 이벤트 설정
	 *******************************************/
	
	/* ========탭처리 관련 =========== */
	
	
	/// 탭선택이벤트
	$(".tab-menu>li>a").click(function(){
	    
		//탭타이틀 처리
		$(".tab-menu>li>a").not(this).removeClass("active");
		if($(this).not(":visible")){
			$(this).addClass("active");	
		}
		
		//탭 본문처리
		$(".tab-list").removeClass("active");
		$("#"+$(this).attr("abbr")).addClass("active");
		
		//그리드 초기화
	    var target_id = $(this).attr("abbr");
		
		if ((target_id == 'tab01')) {
				
			//상세정보세팅
			try{
				$("#rprUsrNm").val(lst[cur_row].rprUsrNm); 
				$("#rprCtnt").val(lst[cur_row].rprCtnt); 
				
			}catch(e){
				console.log("시공자 없음");
			}
			
	    } 
	    else if ((target_id == 'tab02')) {
	    	
			fn_getPdhGrid(cur_row);
    	} 
	    else if ((target_id == 'tab03')) {
	    	
			fn_getPdhGrid2(cur_row);
    	} 
	});
	


	
	
	
	
	
	/* =========점검시설결과 그리드 ========== */
	
	// 점검일정 마스터 저장하기 
	$('#btnSave').click(function () {
		
		if(!gfn_formValid("frm"))	return;
	    

	    gfn_confirm("저장하시겠습니까?", function(ret){
	    	
			if(!ret) return;
			
			gfn_saveFormCmm("frm", "updateChscMa", function(data){
				if(data.result){
					gfn_alert("데이터가 저장되었습니다.","",function(){
						fn_reload();
						//window.close();
					});
				}else{
					gfn_alert("데이터 저장에 실패하였습니다. : " + data.error,"E");
				}
			});	
		});
		
	});	

	
	// 점검일정 삭제하기 Event - 컨트롤러 추가해서 변경!
	$('#btnDel').click(function () {
		
		// 1.점검결과 체크 
		gfn_selectList({sqlId: 'selectChscResultList', 	sclNum:'${sclNum}'}, function(ret){
			if(ret.data != null && ret.data.length > 0){
				gfn_alert("점검결과 내용이 있습니다.","W");
				return false;
			}
			
			//삭제처리			
			gfn_confirm("삭제하시겠습니까?", function(ret){
			    if(!ret)	return;
				
				gfn_saveCmm({sqlId:"deleteChscMa", data:{sclNum: sclNum}}, function(data){
					if (data.result) {
						gfn_alert("삭제 되었습니다.","",function(){
							opener.fn_search();
							window.close();
						});
					}		
					else{
						gfn_alert("삭제에 실패하였습니다."+ data.error,"E");
						return;
					}
				});
			});
		});
		
		
	});
	
	
	
	
	
	
	/* =========점검시설결과 그리드========== */

	// 점검결과 그리드저장
	$("#btnSaveGrid").click(function(){
		if(lst.length < 1){
			gfn_alert("저장할 내용이 없습니다.","W");
			return;
		}
		
		// ajax 저장 
		var _lst = fnObj.grid.getList();
		
	    //그리드필수체크
		myGrid.editCellClear(0,0,0);
		myGrid.selectClear();
		if(!myGrid.validateCheck('C') || !myGrid.validateCheck('U') ){
			return false;
		}	 


		// 저장
		gfn_confirm("저장하시겠습니까?", function(ret){
			if(!ret) return;
			//저장처리
			gfn_saveCmmList(
				{lst: _lst, sqlId: 'saveChscResult', lst2: [{sclNum:'${sclNum}'}], sqlId2: 'updateChscMaCmp', }
				, function(data){
					if (data.result) {
						gfn_alert("저장되었습니다.",function(){
							fn_reload();
							opener.fn_search();
						});
				}
					else{
						var err_msg = "저장에 실패하였습니다.";
						try{
							err_msg = data.error;
						}catch(e){}
						gfn_alert(err_msg,"E");
						fn_reload();
					}
			});
		});
		
	});


	
	
		
	/* =========소모품내역 그리드========== */
	

	//그리드저장
	$("#btnSaveGrid2").click(function(){
		if(lst.length < 1 || cur_row < 0 ){
			gfn_alert("선택된 점검시설결과가 없습니다.","W");
				return false;
		}
		
		
		myGrid2.editCellClear(0,0,0);
		myGrid2.selectClear();

		// ajax 저장 
		var _lst2 = fnObj2.grid.getList();
		
	    //그리드필수체크
		myGrid2.editCellClear(0,0,0);
		myGrid2.selectClear();
		if(!myGrid2.validateCheck('C') || !myGrid2.validateCheck('U') ){
			return false;
		}	 


		// 저장
		gfn_confirm("저장하시겠습니까?", function(ret){
			if(!ret) return;
			//저장처리
			gfn_saveCmmList(
				{lst: _lst2, sqlId: 'savePdhUse', }
				, function(data){
					if (data.result) {
						gfn_alert("저장되었습니다.");
						gfn_selectList({sqlId: 'selectPdhUseList', sclNum:'${sclNum}', ftrCde: cur_ftrCde, ftrIdn: cur_ftrIdn, seq: cur_seq, pdtCatCde: 'PDT_CAT_CDE001'}, function(ret){
							lst2 = ret.data;
							// 그리드 초기화
							fnObj2.pageStart();	
						});	
					}
					else{
						var err_msg = "저장에 실패하였습니다.";
						try{
							err_msg = data.error;
						}catch(e){}
						gfn_alert(err_msg,"E");
						gfn_selectList({sqlId: 'selectPdhUseList', sclNum:'${sclNum}', ftrCde: cur_ftrCde, ftrIdn: cur_ftrIdn, seq: cur_seq, pdtCatCde: 'PDT_CAT_CDE001'}, function(ret){
							lst2 = ret.data;
							// 그리드 초기화
							fnObj2.pageStart();	
						});	
						return;
					}
			});
		});
		
	});



	/* =========소모품(오일)내역 그리드========== */
	
	//그리드저장
	$("#btnSaveGrid3").click(function(){
		if(lst.length < 1 || cur_row < 0 ){
			gfn_alert("선택된 점검시설결과가 없습니다.","W");
				return false;
		}

    	myGrid3.editCellClear(0,0,0);
		myGrid3.selectClear();

		// ajax 저장 
		var _lst3 = fnObj3.grid.getList();
		
	    //그리드필수체크
		myGrid3.editCellClear(0,0,0);
		myGrid3.selectClear();
		if(!myGrid3.validateCheck('C') || !myGrid3.validateCheck('U') ){
			return false;
		}	 


		// 저장
		gfn_confirm("저장하시겠습니까?", function(ret){
			if(!ret) return;
			//저장처리
			gfn_saveCmmList(
				{lst: _lst3, sqlId: 'savePdhUse', }
				, function(data){
					if (data.result) {
						gfn_alert("저장되었습니다.");
						gfn_selectList({sqlId: 'selectPdhUseList', sclNum:'${sclNum}', ftrCde: cur_ftrCde, ftrIdn: cur_ftrIdn, seq: cur_seq, pdtCatCde: 'PDT_CAT_CDE002'}, function(ret){
							lst3 = ret.data;
							// 그리드 초기화
							fnObj3.pageStart();	
						});	
					}
					else{
						var err_msg = "저장에 실패하였습니다.";
						try{
							err_msg = data.error;
						}catch(e){}
						gfn_alert(err_msg,"E");
						gfn_selectList({sqlId: 'selectPdhUseList', sclNum:'${sclNum}', ftrCde: cur_ftrCde, ftrIdn: cur_ftrIdn, seq: cur_seq, pdtCatCde: 'PDT_CAT_CDE002'}, function(ret){
							lst3 = ret.data;
							// 그리드 초기화
							fnObj3.pageStart();	
						});	
						return;
					}
			});
		});
		
	});


	// 점검승인
	$("#btnAppr").click(function(){
	
		gfn_confirm("점검승인 하시겠습니까?", function(ret){
	    	
			if(!ret) return;
			
			gfn_saveCmm({sqlId:"updateChscMaAppr", data: {sclNum: sclNum}}, function(data){
				if(data.result){
					gfn_alert("데이터가 저장되었습니다.","",function(){
						fn_reload();
					});
				}else{
					gfn_alert("데이터 저장에 실패하였습니다. : " + data.error,"E");
				}
			});	
		});
	});

	
	// 시설결과 부가정보 저장
	$("#btnSaveRstDtl").click(function(){

		if(lst.length < 1 || cur_row < 0 ){
			gfn_alert("선택된 점검시설결과가 없습니다.","W");
				return false;
		}
		
		gfn_confirm("저장하시겠습니까?", function(ret){
	    	
			if(!ret) return;
			
			var map = lst[cur_row];//현재선택된 그리드데이터
			map.rprUsrNm = $("#rprUsrNm").val(); 
			map.rprCtnt = $("#rprCtnt").val(); 
			
			gfn_saveCmm({sqlId:"saveChscResult", data: map}, function(data){
				if(data.result){
					gfn_alert("데이터가 저장되었습니다.","",function(){
						fn_reload();
					});
				}else{
					gfn_alert("데이터 저장에 실패하였습니다. : " + data.error,"E");
				}
			});	
		});
	});
	
	

	
	// 사진첨부등록
	$("#btnAddPhoto").click(function(){
	
		if(lst.length < 1 || cur_row < 0 ){
			gfn_alert("선택된 점검시설결과가 없습니다.","W");
				return false;
		}
		var _filSeq = lst[cur_row].filSeq;
		window.open('${ctxRoot}/pms/file/lnkSclPhotoDtl.do?sclNum=${sclNum}&filSeq='+_filSeq, '점검일정 사진 추가', 'width=680, height=410, left='+ popupX + ', top='+ popupY + ', status=no, resizable=no');
		return false;
	});
	
});	//document ready







//화면갱신
var fn_reload = function(){
	location.reload();
}


//시설물팝업 선택 콜백
var callback_selFtr = function(_ftrCde, _ftrIdn, _ftrNam, _hjdCde, _hjdNam){
	
	lst[cur_row].ftrCde = _ftrCde;
	lst[cur_row].ftrIdn = _ftrIdn;
	lst[cur_row].ftrNam = _ftrNam;
	lst[cur_row].hjdCde = _hjdCde;
	lst[cur_row].hjdNam = _hjdNam;
	lst[cur_row].rprCatCde = 'RPR_CAT_CDE01';
	lst[cur_row].rprCuzCde = 'RPR_CUZ_CDE01';
	lst[cur_row].rprYmd = gfn_today('yyyyMMdd');
	lst[cur_row].filSeq = '';
	myGrid.dataSync();
}


//점검시설결과  업무파일아이디 저장 - 파일첨부콜백
var callback_saveResultFile = function(_filSeq){
	
	gfn_saveCmm({sqlId:"updateFileResult", data:{sclNum: '${sclNum}', ftrCde: cur_ftrCde, ftrIdn: cur_ftrIdn, seq: cur_seq, filSeq: _filSeq }  }
		,function(data){
			fn_reload();
	});
}



// 사진상세 삭제
var fn_fileDel = function(filSeq, seq){
	
	gfn_confirm("사진을 삭제하시겠습니까?", function(ret){
    	
		if(!ret) return;
		
		gfn_saveCmm({sqlId:"deleteFileDtl", data: {filSeq: filSeq, seq: seq}}, function(data){
			if(data.result){
				gfn_alert("삭제되었습니다.","",function(){
					fn_reload();
				});
			}else{
				gfn_alert("데이터 저장에 실패하였습니다. : " + data.error,"E");
			}
		});	
	});
}


//점검결과 선택 후 상세정보 갱신	
var fn_setRstDtl = function(row){
	
	if(lst.length < 1){
		console.log("점검시설결과 없음");
		return;
	}
	
	// 0.상세정보세팅
	try{
		$("#rprUsrNm").val(lst[row].rprUsrNm); 
		$("#rprCtnt").val(lst[row].rprCtnt); 
		
	}catch(e){
		console.log("시공자 없음");
	}
	
	
	fn_getPhoto(row);
	fn_getPdhGrid(row);
	fn_getPdhGrid2(row);
	
}


//점검결과 하위 그리드 초기화
var fn_initRstDtl = function(){
	
	if(lst.length > 0){
		
		//상세정보
		$("#rprUsrNm").val(lst[0].rprUsrNm); 
		$("#rprCtnt").val(lst[0].rprCtnt);
		
		//사진정보
		fn_getPhoto(0);
	}
	
		
	//소모품그리드
	gfn_selectList({sqlId: 'selectPdhList', pdtCatCde: "PDT_CAT_CDE001"}, function(ret){
		PDH_LIST = ret.data;

		gfn_selectList({sqlId: 'selectPdhUseList' , sclNum: '${sclNum}', ftrCde: cur_ftrCde, ftrIdn: cur_ftrIdn, seq: cur_seq, pdtCatCde: 'PDT_CAT_CDE001'} , function(ret){
			lst2 = ret.data;
			// 그리드 초기화
			fnObj2.pageStart();
		});
	});
	
	gfn_selectList({sqlId: 'selectPdhList', pdtCatCde: "PDT_CAT_CDE002"}, function(ret){
		PDH_LIST2 = ret.data;
		
		gfn_selectList({sqlId: 'selectPdhUseList' , sclNum: '${sclNum}', ftrCde: cur_ftrCde, ftrIdn: cur_ftrIdn, seq: cur_seq, pdtCatCde: 'PDT_CAT_CDE002'} , function(ret){
			lst3 = ret.data;
			// 그리드 초기화
			fnObj3.pageStart();
		});
	});
	
	
}


//점검결과 선택 후 사진목록 갱신	
var fn_getPhoto = function(row){

	// 1.사진정보세팅
	try{
		_filSeq = lst[row].filSeq; 
		//점검결과 선택row 사진리스트
		gfn_selectList({sqlId: 'selectFileDtlList', filSeq: _filSeq, filSeq: _filSeq}, function(ret){
			file_list = ret.data;

			var html = "";
			var html2 = "";
			$("#photoList > li").remove();
			$("#owl-chkPhoto > div").remove();
			
			$.each(file_list, function(idx, map){
				//파일리스트 만들기
				html += '<li>'+map.dwnNam+'  <img src="/fms/images/icon_file_delete.png" onclick="fn_fileDel('+map.filSeq+','+map.seq+');" width="18" height="18" /></li>';				
				
				//파일갤러리 만들기
				html2 += '<div class="item"><img class="galery" src="${ctxRoot}/pms/file/downloadImg.do?seq='+map.seq+'&filSeq='+map.filSeq+'"/></div>';
			})
			$("#photoList").append(html);
			$("#owl-chkPhoto").append(html2);
			
			//갤러리초기화
			 $("#owl-chkPhoto").owlCarousel('destroy'); 
 			 $("#owl-chkPhoto").owlCarousel({
 			      items : 3, //10 items above 1000px browser width
 			      itemsDesktop : [1000,5], //5 items between 1000px and 901px
 			      itemsDesktopSmall : [900,3], // betweem 900px and 601px
 			      itemsTablet: [600,2], //2 items between 600 and 0
 			      itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
				  autoplay:true,
				  margin:20
			  });  
			
 			 
 			// 갤러리 no image 세팅
 		    $(".galery").on("error", function(){
 		        $(this).attr('src', '${ctxRoot}/images/img_noimage.png');
 		    });	
 			 
		});
	}catch(e){
		console.log("사진없음");
	}
	
	
}	
	
//소모품그리드 	
var fn_getPdhGrid = function(row){
	if(lst.length < 1){
		console.log("점검시설결과 없음");
		return;
	}


	// 소모품사용내역리스트	
	gfn_selectList({sqlId: 'selectPdhUseList' , sclNum: '${sclNum}', ftrCde: cur_ftrCde, ftrIdn: cur_ftrIdn, seq: cur_seq, pdtCatCde: 'PDT_CAT_CDE001'} , function(ret){
		lst2 = ret.data;
		// 그리드 초기화
		fnObj2.pageStart();
	});
}

//소모품(오일)그리드 	
var fn_getPdhGrid2 = function(row){
	if(lst.length < 1){
		console.log("점검시설결과 없음");
		return;
	}

	// 소모품사용내역리스트	
	gfn_selectList({sqlId: 'selectPdhUseList' , sclNum: '${sclNum}', ftrCde: cur_ftrCde, ftrIdn: cur_ftrIdn, seq: cur_seq, pdtCatCde: 'PDT_CAT_CDE002'} , function(ret){
		lst3 = ret.data;
		// 그리드 초기화
		fnObj3.pageStart();
	});
}	

//점결시설결과 시설물코드 선택팝업
var fn_SelFtrPop = function(){
	
	window.open('${ctxRoot}/pms/link/lnkFtrList.do', "ftrSelList", 'status=no, width=1100, height=540 , left='+ popupX + ', top='+ popupY + ', screenX='+ popupX + ', screenY= '+ popupY);
	
}

//점검시설결과 삭제
var fn_delResult = function(){
	var checkedList = myGrid.getCheckedListWithIndex(0);// colSeq	
    if (checkedList.length < 1) {
        gfn_alert("선택된 목록이 없습니다. 삭제하시려는 목록을 체크하세요","W");
        return;
    }
    else if (checkedList.length > 1) {
        gfn_alert("삭제하려는 항목을 하나만 체크하세요","W");
        return;
    }
	
	var _ftrCde = checkedList[0].item.ftrCde;
	var _ftrIdn = checkedList[0].item.ftrIdn;
	var _seq = checkedList[0].item.seq;
	//하위데이터 체크
	if(lst2.length > 0 || lst3.length > 0 ){
		gfn_alert("소모품(오일)사용내역 데이터가 있습니다.","W");
		return false;
	}
	

	gfn_confirm("삭제하시겠습니까?", function(ret){
	    if(!ret)	return;
		
		gfn_saveCmm({sqlId:"deleteChscResult", data:{sclNum: sclNum, ftrCde: _ftrCde, ftrIdn: _ftrIdn, seq: _seq}  }
		,function(data){
			if (data.result) {
				gfn_alert("삭제 되었습니다.","",function(){
					fn_reload();
				});
			}		
			else{
				gfn_alert("삭제에 실패하였습니다."+ data.error,"E");
				return;
			}
		});
	});
	
}

</script>
</head>



<body>

	
					
<div id="wrap" class="wrap" > 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1 >점검일정 상세정보</h1>
  </div>
  <!-- // header --> 



	<form name="frm" id="frm" method="post">
  		<input type="hidden" name="sclNum" value="${sclNum}">
  
  <!-- container -->
  <div id="container"  class="content"  > 
    
    <!-- list -->
    <div class="view_box">
      <h2 ><span style="float:left">일반정보</span>
        <div class="btn_group m-b-5 right "  >
          <button id="btnAppr" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_w_regist.png" width="16" height="16" alt=""/> 점검승인</button>
          <button id="btnDel" type="button" class="btn btn-default " > <img src="/fms/images/icon_btn_delete.png" width="16" height="16" alt=""/> 삭제 </button>
          <button id="btnSave" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_save.png" width="16" height="16" alt=""/> 저장 </button>
        </div>
      </h2>
      
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
          <colgroup>
	          <col width="150px">
	          <col width="130px">
	          <col width="150px">
	          <col width="130px">
	          <col width="150px">
	          <col width="130px">
	          <col width="150px">
	          <col width="130px">
          </colgroup>
					
			<tbody>
				
				<tr>
					<th class="req">관리기관</th>
					<td>
						<select name="mngCde" id="mngCde"  class="reqVal" >
							<option value="">--선택하세요--</option>
							<c:forEach var="cntList" items="${dataCodeList }">
								<option value="${cntList.codeCode }"${chscMaDtl.mngCde == cntList.codeCode ? "selected" : "" }>${cntList.codeAlias}</option>
							</c:forEach>
						</select>
					</td>
					<th class="req">점검구분</th>
					<td>
	                    <select class="reqVal"   name="sclCde" id="sclCde"   class="reqVal">
	                    </select>
					</td>
					<th class="req">점검명</th>
					<td colspan="3">
						<input type="text" name="titNam" id="titNam" value="${chscMaDtl.titNam}"  class="reqVal" style="width: 380px;"/>
					</td>
				</tr>
				<tr>
					<th>점검시작일자</th>
					<td>
						<input type="text" class="datepicker" name="staYmd" id="staYmd" value="${chscMaDtl.staYmd}" class="reqVal" />
					</td>
					<th class="req">점검종료일자</th>
					<td >
						<input type="text" class="datepicker" name="endYmd" id="endYmd" value="${chscMaDtl.endYmd}" class="reqVal" />
					</td>
					<th class="req">점검그룹(자)</th>
					<td colspan="3">
						<input type="text" name="ckmPeo" id="ckmPeo" value="${chscMaDtl.ckmPeo}"  style="width: 380px;"/>
					</td>
				</tr>
				<tr>
					<th>점검승인상태</th>
					<td>
						<span>${chscMaDtl.sclStatNm}</span>
					</td>
					<th >점검승인일자</th>
					<td >
						<span>${chscMaDtl.chkAprYmd}</span>
					</td>
					<th class="req" rowspan="2">점검내용</th>
					<td colspan="3" rowspan="2">
						<textarea name="chkCtnt" id="chkCtnt"   rows="3" cols="60" >${chscMaDtl.chkCtnt}</textarea>
					</td>
				</tr>
				<tr>
					<th >점검승인자</th>
					<td >
						<span>${chscMaDtl.chkAprUsrNm} &nbsp;</span>
					</td>
					<th >점검완료일</th>
					<td >
						<span>${chscMaDtl.chkResultYmd} &nbsp;</span>
					</td>
				</tr>
				
        </tbody>
        
        </table>
      </div>
    </div>
    <!-- //list --> 



	<br>
    <div class="view_box">
    
    


      <h2 ><span style="float:left">점검결과</span>
        <div class="btn_group m-b-5 right "  >
		          <input type="button" value="추가" class="AXButton Green" onclick="fnObj.grid.append();"/>
		          <input type="button" value="삭제" class="AXButton Green" onclick="fn_delResult();"/>
		          <input type="button" id="btnSaveGrid" value="저장" class="AXButton Blue" />
        </div>
      </h2>

      
	     <div class="view ">
		   	  <div id="grdResult" style="height:200px;"></div>
	     </div>
     
    </div>

     


	<!-- 탭영역	 -->
	<div class=" tab-box">
      
        <div class="tab-menu">
        <ul class="tab-menu">
          <li><a href="#" abbr="tab01" class="active">점검결과상세</a></li>
          <li><a href="#" abbr="tab02">소모품사용</a></li>
          <li><a href="#" abbr="tab03">주유/오일 사용 </a></li>
          
        </ul>
      </div> 

	<div id="tab01" class="tab-list active"  >

		<div style="display:inline-block; width:300px; height:200px;">
		
			<div class="view" style="margin-top: 10px; margin-left: 10px;">
			  <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
				<colgroup>
				  <col width="70px">
				  <col width="200px">
			   </colgroup>
				<tbody>
				  <tr>
					<th>시공자명</th>
					<td><input type="text" class="" id="rprUsrNm" value="${chscMaDtl.rprUsrNm}" style="width:210px;" ></td>
				  </tr>
				  <tr>
					<th >보수내용</th>
					<td > <textarea name="textarea" id="rprCtnt" cols="30" rows="3" class="" style="padding: 4px 5px;" >${chscMaDtl.rprCtnt}</textarea></td>
				  </tr>
				  <tr>
					<th >파일목록</th>
					<td >
						<div style="height: 100px; overflow: auto;">
							<ul id="photoList">
							</ul> 
						</div>
					</td>
				  </tr>
				</tbody>
			  </table>
			</div>		
		
		</div>

		
		<div style="display:inline-block; width:750px; height:200px; background-color:#f2f2f2; position:absolute; margin:0px 5px 10px; text-align: center;">
	         <div class="btn_group" style="padding:4px 0px 3px; text-align: right; border:0px solid #ffff00;">
				<button id="btnSaveRstDtl" type="button" class="btn btn-default " style="float: left;" ><img src="/fms/images/icon_btn_save.png" width="16" height="16" alt=""/> 결과저장 </button>		
				<button id="btnAddPhoto" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_add.png" width="16" height="16" alt=""/> 사진등록</button>
			 </div>		
		
			<!-- 갤러리영역 -->
			<div id="owl-chkPhoto" class="owl-carousel owl-theme" style="width:740px; height:140px; padding:0px; margin:0px 0px 0px 5px;  border:0px solid #ffff00;">
			</div>    
		
		</div>

	</div>
	
	
	<div  id="tab02" class="tab-list"  >

              <div class="btn_group" style=" margin:5px;">
                  <input type="button" value="추가" class="AXButton Green" onclick="fnObj2.grid.append();"/>
                  <input type="button" value="삭제" class="AXButton Green" onclick="fnObj2.grid.remove();"/>
                  <input type="button" id="btnSaveGrid2" value="저장" class="AXButton Blue" style="float:right;"/>
              </div>					  	
	  	<div id="grdPdh" style="height:202px;"></div>
	</div>					
	
	
	
	<div  id="tab03" class="tab-list"  >

              <div class="btn_group" style=" margin:5px;">
                  <input type="button" value="추가" class="AXButton Green" onclick="fnObj3.grid.append();"/>
                  <input type="button" value="삭제" class="AXButton Green" onclick="fnObj3.grid.remove();"/>
                  <input type="button" id="btnSaveGrid3" value="저장" class="AXButton Blue" style="float:right;"/>
              </div>					  	
	  	<div id="grdPdh2" style="height:202px;"></div>
	
	</div>


 </div>	




    
  </div>
  <!-- //container --> 
</form>  

</div>
<!-- //UI Object -->

</body>
</html>