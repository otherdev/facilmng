<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<script type="text/javascript">
	var currentInitialize = function() {
		$(".validation").hide();
		
		var msg = '${msg}';
	
		if(msg) {
			alert(msg);
			try { opener.selfRefresh(); }catch(e){}
			window.close();
		}
				
		$("#btnSave").click(function(e) {
			e.preventDefault();
			
			if($("#iotCde").val()=="O"){
				if($("#hjdCde").val()=="" || $("#hjdCde").val()==" " || $("#hjdCde").val()==null) {
					alert('행정동을 선택해주세요.');
					$('#hjdCde').focus();
					
					return false;
				}
				if($("#mngCde").val()=="" || $("#mngCde").val()==" " || $("#mngCde").val()==null) {
					alert('관리기관을 선택해주세요.');
					$('#mngCde').focus();
					
					return false;
				}
				if($("#ftrCde").val()=="" || $("#ftrCde").val()==" " || $("#ftrCde").val()==null) {
					alert('지형지물을 선택해주세요.');
					$('#ftrCde').focus();
					
					return false;
				}
				if($("#ftrIdn").val()=="" || $("#ftrIdn").val()==" " || $("#ftrIdn").val()==null) {
					alert('관리번호를 입력해주세요');
					$('#ftrIdn').focus();
					
					return false;
				}
				if($("#sbjCde").val()=="" || $("#sbjCde").val()==" " || $("#sbjCde").val()==null) {
					alert('유지보수 사유를 선택해주세요.');
					$('#sbjCde').focus();
					
					return false;
				}
				if($("#repCde").val()=="" || $("#repCde").val()==" " || $("#repCde").val()==null) {
					alert('유지보수 구분을 선택해주세요.');
					$('#repCde').focus();
					
					return false;
				}
				if($("#repDes").val()=="" || $("#repDes").val()==" " || $("#repDes").val()==null) {
					alert('유지보수 내용을 입력해주세요');
					$('#repDes').focus();
					
					return false;
				}
				if($("#oprNam").val()=="" || $("#oprNam").val()==" " || $("#oprNam").val()==null) {
					alert('시공자를 입력해주세요.');
					$('#oprNam').focus();
					
					return false;
				}
			}
			if($("#pddCde").val()=="" || $("#pddCde").val()==" " || $("#pddCde").val()==null) {
				alert('소모품/예비품 선택해주세요.');
				$('#pddCde').focus();
				
				return false;
			}
			if($("#pdtNam").val()=="" || $("#pdtNam").val()==" " || $("#pdtNam").val()==null) {
				alert('품명(영문)을 입력해주세요.');
				$('#pdtNam').focus();
				
				return false;
			}
			if($("#iotCde").val()=="" || $("#iotCde").val()==" " || $("#iotCde").val()==null) {
				alert('입출고 구분을 선택해주세요.');
				$('#iotCde').focus();
				
				return false;
			}
			if($("#pdhCnt").val()=="" || $("#pdhCnt").val()==" " || $("#pdhCnt").val()==null) {
				alert('입출고재고를 입력해주세요');
				$('#pdhCnt').focus();
				
				return false;
			}
			if($("#iotYmd").val()=="" || $("#iotYmd").val()==" " || $("#iotYmd").val()==null) {
				alert('입출고일자를 입력해주세요');
				$('#iotYmd').focus();
				
				return false;
			}
				
			$("#pdjtMaAddFrm").submit();
		});
	};
	
	var calcCnt = function(a, b, total) {
		
		var intA = (parseInt(a.value)>0 ? parseInt(a.value) : 0);
		var intB = (parseInt(b.value)>0 ? parseInt(b.value) : 0) ;
		var intT = 0;
		if($("#iotCde").val()=="I"){
			intT = intA + intB;			
		} else {
			intT = intB - intA;			
		}
		total.value = (parseInt(intT)>0 ? parseInt(intT) : 0);	
	}
	
	var loadPdtNams = function(n) {
		if(n != "") {
			if(n == "0") {
				$('option.opt1').show();
				$('option.opt2').hide();
			}
			if(n == "1") {
				$('option.opt2').show();
				$('option.opt1').hide();
			}
		}else {
			$('option.opt1').hide();
			$('option.opt2').hide();
		}
		$("#pdtNam").val("");
		$("#pddCnt").val('0');
		calcCnt(pdhCnt, pddCnt, afterCnt);
	}
	
	var chkIO = function(n) {
		if(n == "I") {
			$("#hjdCde").val("");
			$("#hjdCde").addClass("disable");
			$("#hjdCde").attr("disabled", true);
			$("#mngCde").val("");
			$("#mngCde").addClass("disable");
			$("#mngCde").attr("disabled", true);
			$("#ftrCde").val("");
			$("#ftrCde").addClass("disable");
			$("#ftrCde").attr("disabled", true);
			$("#ftrIdn").val("");
			$("#ftrIdn").addClass("disable");
			$("#ftrIdn").attr("disabled", true);
			$("#oprNam").val("");
			$("#oprNam").addClass("disable");
			$("#oprNam").attr("disabled", true);
			$("#repYmd").val("");
			$("#repYmd").addClass("disable");
			$("#repYmd").attr("disabled", true);
			$("#sbjCde").val("");
			$("#sbjCde").addClass("disable");
			$("#sbjCde").attr("disabled", true);
			$("#repCde").val("");
			$("#repCde").addClass("disable");
			$("#repCde").attr("disabled", true);
			$("#repDes").val("");
			$("#repDes").addClass("disable");
			$("#repDes").attr("disabled", true);
			
			$(".validation").hide();
		}
		if(n == "O") {
			$("#hjdCde").removeClass("disable");
			$("#hjdCde").attr("disabled", false);
			$("#mngCde").removeClass("disable");
			$("#mngCde").attr("disabled", false);
			$("#ftrCde").removeClass("disable");
			$("#ftrCde").attr("disabled", false);
			$("#ftrIdn").removeClass("disable");
			$("#ftrIdn").attr("disabled", false);
			$("#oprNam").removeClass("disable");
			$("#oprNam").attr("disabled", false);
			$("#repYmd").removeClass("disable");
			$("#repYmd").attr("disabled", false);
			$("#sbjCde").removeClass("disable");
			$("#sbjCde").attr("disabled", false);
			$("#repCde").removeClass("disable");
			$("#repCde").attr("disabled", false);
			$("#repDes").removeClass("disable");
			$("#repDes").attr("disabled", false);
			
			$(".validation").show();
			
		}
	}
	function getNumber(obj){    
	    var num01;
	    var num02;
	    num01 = obj.value;
	    num02 = num01.replace(/\D/g,""); //숫자가 아닌것을 제거, 
	                                     //즉 [0-9]를 제외한 문자 제거; /[^0-9]/g 와 같은 표현
	    // num01 = setComma(num02); //콤마 찍기
	    // obj.value =  num01;
	    obj.value =  num02;
	}
	function setComma(n) {
	    var reg = /(^[+-]?\d+)(\d{3})/;   // 정규식
	    n += '';                          // 숫자를 문자열로 변환         
	    while (reg.test(n)) {
	       n = n.replace(reg, '$1' + ',' + '$2');
	    }         
	    return n;
	}
</script>



<div id="wrap" class="wrap" > 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1 >소모품/예비품 대장 등록정보</h1>
  </div>
  <!-- // header --> 

  <form id="pdjtMaAddFrm" name="pdjtMaAddFrm" method="post" action="${ctxRoot}/pdjt/addPdjtMaSave.do">


  <!-- container -->
  <div id="container"  class="content"  > 
    
    <!-- list -->
    <div class="regist-box2 ">
      <h2 ><span>소모품/예비품 정보</span>
        <div class="btn_group m-b-5 right "  >         
          <button id="btnSave" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_save.png" width="16" height="16" alt=""/> 저장 </button>
      </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table table-view-regist " >
          <colgroup>
          <col width="170px">
          <col width="*">
          <col width="170px">
          <col width="*">
      
          </colgroup>
          <tbody>
							<tr>
								<th>행정동 <img src="/fms/images/icon_table_required.png" width="12" height="12" alt=""/> </th>
								<td>
									<select name="hjdCde" id="hjdCde" disabled class="disable">
										<option value="">선택안함</option>
										<c:forEach var="cmtList" items="${cmtAdarMaList}" varStatus="status">
											<option value="${cmtList.hjdCde }">${cmtList.hjdNam}</option>
										</c:forEach>
									</select>
								</td>
								<th> 관리기관 <img src="/fms/images/icon_table_required.png" width="12" height="12" alt=""/></th>
								<td>
									<select id="mngCde" name="mngCde" disabled class="disable">
										<option value="">선택안함</option>
										<c:forEach var="cdeList" items="${dataCodeList}" varStatus="status">
											<option value="${cdeList.codeCode }">${cdeList.codeAlias}</option>
										</c:forEach>	
									</select>
								</td>
							</tr>
							<tr>
								<th> 지형지물 <img src="/fms/images/icon_table_required.png" width="12" height="12" alt=""/></th>
								<td>
									<select id="ftrCde" name="ftrCde" disabled class="disable">
									<option value="">선택안함</option>
										<c:forEach var="cmtFtrcMaList" items="${cmtFtrcMaList}" varStatus="status">
											<option value="${cmtFtrcMaList.ftrCde }">${cmtFtrcMaList.ftrNam}</option>
										</c:forEach>
									</select>
								</td>
								<th> 관리번호 <img src="/fms/images/icon_table_required.png" width="12" height="12" alt=""/></th>
								<td>
									<input type="text" id="ftrIdn" name="ftrIdn" onchange="getNumber(this);" onkeyup="getNumber(this);" maxlength="10"  disabled class="disable"/>
								</td>
							</tr>
							<tr>
								<th> 유지보수 사유 <img src="/fms/images/icon_table_required.png" width="12" height="12" alt=""/></th>
								<td>
									<select name="sbjCde" id="sbjCde" disabled class="disable">
										<option value="">선택안함</option>
										<c:forEach var="sbjCdeList" items="${sbjCdeList}">
											<option value="${sbjCdeList.codeCode }">${sbjCdeList.codeAlias}</option>
										</c:forEach>
									</select>
								</td>							
								<th> 유지보수 구분 <img src="/fms/images/icon_table_required.png" width="12" height="12" alt=""/></th>
								<td>
									<select name="repCde" id="repCde" disabled class="disable">
										<option value="">선택안함</option>	
										<c:forEach var="repCdeList" items="${repCdeList}">
											<option value="${repCdeList.codeCode }">${repCdeList.codeAlias}</option>
										</c:forEach>
									</select>
								</td>
							</tr>
							<tr>							
								<th> 유지보수 내용 <img src="/fms/images/icon_table_required.png" width="12" height="12" alt=""/></th>
								<td colspan="3">
									<input type="text" name="repDes" id="repDes" disabled class="disable" />
								</td>
							</tr>
							<tr>	
								<th>소모품/예비품 <img src="/fms/images/icon_table_required.png" width="12" height="12" alt=""/></th>
								<td>
									<select id="pddCde" name="pddCde" onchange="loadPdtNams(this.value);">
										<option value="">-선택-</option>
										<option value="0">소모품</option>
										<option value="1">예비품</option>
									</select>
								</td>
								<th> 시공자 <img src="/fms/images/icon_table_required.png" width="12" height="12" alt=""/></th>
								<td>
									<input type="text" name="oprNam" id="oprNam" disabled class="disable"/>
								</td>
							</tr>
							<tr>
								<th> 픔명(영문) <img src="/fms/images/icon_table_required.png" width="12" height="12" alt=""/></th>
								<td>
									<script type="text/javascript">
									var setSelectPdt = function(opt) {
										$('#pddCnt').val( opt.attr('data-cnt') );
										$('#pdtStd').val( opt.attr('data-std') );
										$('#pdtMnf').val( opt.attr('data-mnf') );
										$('#pdtMdl').val( opt.attr('data-mdl') );
										$('#pdtUnt').val( opt.attr('data-unt') );
										$('#pdtNum').val( opt.attr('data-pdt-num') );
										$('#pddNum').val( opt.attr('data-pdd-num') );
									}
									</script>
									<input type="hidden" id="pdtNum" name="pdtNum" value="" />
									<input type="hidden" id="pddNum" name="pddNum" value="" />
									<select id="pdtNam" name="pdtNam" onchange="setSelectPdt( $($('#pdtNam').children().get(this.selectedIndex)) ); calcCnt(pdhCnt, pddCnt, afterCnt);">
										<option value=""
											data-cnt="0"
											data-std=""
											data-mnf=""
											data-mdl=""
											data-unt=""
											data-pdt-num=""
											data-pdd-num="">-선택-</option>
										<!-- 소모품 -->
										<c:forEach var="pdjtDt0" items="${pdjtDtList0}" varStatus="index">
											<option class="opt1" value="${pdjtDt0.pdtNam}"
												 data-cnt="${pdjtDt0.pddCnt}"
												 data-std="${pdjtDt0.pdtStd}"
												 data-mnf="${pdjtDt0.pdtMnf}"
												 data-mdl="${pdjtDt0.pdtMdl}"
												 data-unt="${pdjtDt0.pdtUnt}"
												 data-pdt-num="${pdjtDt0.pdtNum}"
												 data-pdd-num="${pdjtDt0.pddNum}">${pdjtDt0.pdtNam}
											</option>
										</c:forEach>
										<!-- 예비품 -->
										<c:forEach var="pdjtDt1" items="${pdjtDtList1}" varStatus="index">
											<option class="opt2" value="${pdjtDt1.pdtNam}"
												 data-cnt="${pdjtDt1.pddCnt}"
												 data-std="${pdjtDt1.pdtStd}"
												 data-mnf="${pdjtDt1.pdtMnf}"
												 data-mdl="${pdjtDt1.pdtMdl}"
												 data-unt="${pdjtDt1.pdtUnt}"
												 data-pdt-num="${pdjtDt1.pdtNum}"
												 data-pdd-num="${pdjtDt1.pddNum}">${pdjtDt1.pdtNam}
											 </option>
										</c:forEach>
									</select>
								</td>
								<th>소모품/예비품 규격</th>
								<td>
									<input type="text" id="pdtStd" name="pdtStd" disabled class="disable" />
								</td>
							</tr>
							<tr>							
								<th>단위</th>
								<td>
									<input type="text" id="pdtUnt" name="pdtUnt" disabled class="disable" />
								</td>
								<th>모델</th>
								<td>
									<input type="text" id="pdtMdl" name="pdtMdl" disabled class="disable" />
								</td>
							</tr>
							<tr>
								<th>제조사</th>
								<td>
									<input type="text" id="pdtMnf" name="pdtMnf" disabled class="disable" />
								</td>
								<th>기존재고</th>
								<td>
									<input type="text" id="pddCnt" name="pddCnt" value="0" onkeyup="calcCnt(pdhCnt, pddCnt, afterCnt);" readonly class="disable" />
								</td>
							</tr>
							<tr>
								<th>입출고 구분 <img src="/fms/images/icon_table_required.png" width="12" height="12" alt=""/></th>
								<td>
									<select id="iotCde" name="iotCde" onchange="chkIO(this.value); calcCnt(pdhCnt, pddCnt, afterCnt);">
										<option value="I">입고</option>
										<option value="O">출고</option>
									</select>
								</td>
								<th>입출고재고 <img src="/fms/images/icon_table_required.png" width="12" height="12" alt=""/></th>
								<td>
									<input type="text" id="pdhCnt" name="pdhCnt" onkeyup="calcCnt(pdhCnt, pddCnt, afterCnt);" />
								</td>
							</tr>
							<tr>
								<th>입출고일 <img src="/fms/images/icon_table_required.png" width="12" height="12" alt=""/></th>
								<td>
									<input type="text" id="iotYmd" name="iotYmd" class="datepicker" />
								</td>
								<th>현재고</th>
								<td>
									<input type="text" id="afterCnt" name="" value="0" disabled class="disable"/>
								</td>
							</tr>
          </tbody>
        </table>
      </div>
    </div>
    <!-- //list --> 
    
  </div>
  
  <!-- //container --> 
</form>
  
</div>
<!-- //UI Object -->

</body>
</html>


