<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

	<script type="text/javascript">
		var currentInitialize = function() {
			
			var msg = '${msg}';
		
			if(msg) {
				alert(msg);
				try { opener.selfRefresh(); }catch(e){}
				location.href="${ctxRoot}/pms/fcltMnt/sppExpdMngDtl.do?pdhNum=${param.pdhNum}";
			}	
			
				
			$("#btnSave").click(function(e) {
				e.preventDefault();
			
				if($("#iotCde").val()=="O") {
					if($(".sbjCde").val()=="" || $(".sbjCde").val()==" " || $(".sbjCde").val()==null) {
					alert('유지보수 사유를 선택해주세요.');
					$('.sbjCde').focus();
					
					return false;
					}
					if($(".repCde").val()=="" || $(".repCde").val()==" " || $(".repCde").val()==null) {
						alert('유지보수 구분을 선택해주세요.');
						$('.repCde').focus();
						
						return false;
					}
					if($("#pdhCnt").val()=="" || $("#pdhCnt").val()==" " || $("#pdhCnt").val()==null) {
						alert('입출고재고를 입력해주세요');
						$('#pdhCnt').focus();
						
						return false;
					}
					if($("#iotYmd").val()=="" || $("#iotYmd").val()==" " || $("#iotYmd").val()==null) {
						alert('입출고일자를 입력해주세요');
						$('#iotYmd').focus();
						
						return false;
					}
				}
				$("#sbjCde").val($(".sbjCde").val());
				$("#repCde").val($(".repCde").val());
				$("#pdjtMaEditFrm").submit();
			});
			
			if($("#iotCde").val()) {
				chkIO($("#iotCde").val());
			}
			
		};	
		
		var chkIO = function(n) {
			if(n == "I") {				
				$(".sbjCde").val("");
				$(".sbjCde").addClass("disable");
				$(".sbjCde").attr("disabled", true);
				$(".repCde").val("");
				$(".repCde").addClass("disable");
				$(".repCde").attr("disabled", true);
				$(".validation").hide();
			}
			if(n == "O") {
				$(".sbjCde").removeClass("disable");
				$(".sbjCde").attr("disabled", false);
				$(".repCde").removeClass("disable");
				$(".repCde").attr("disabled", false);
				
				$(".validation").show();
				
			}
		}
		
		var calcCnt = function(a, b, total) {
			
			var intA = (parseInt(a.value)>0 ? parseInt(a.value) : 0);
			var intB = (parseInt(b.value)>0 ? parseInt(b.value) : 0) ;
			var intT = 0;
			if($("#iotCde").val()=="I"){
				intT = intA + intB;			
			} else {
				intT = intB - intA;			
			}
			total.value = (parseInt(intT)>0 ? parseInt(intT) : 0);	
		}
		
		var loadPdtNams = function(n) {
			if(n != "") {
				if(n == "0") {
					$('option.opt1').show();
					$('option.opt2').hide();
				}
				if(n == "1") {
					$('option.opt2').show();
					$('option.opt1').hide();
				}
			}else {
				$('option.opt1').hide();
				$('option.opt2').hide();
			}
			$("#pdtNam").val("");
			$("#pddCnt").val('0');
			calcCnt(pdhCnt, pddCnt, afterCnt);
		}
	</script>



<div id="wrap" class="wrap" > 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1 >소모품/예비품 대장 상세정보</h1>
  </div>
  <!-- // header --> 

		<form id="pdjtMaEditFrm" name="pdjtMaEditFrm" method="post" action="${ctxRoot}/pdjt/updatePdjtMa.do?pdtNum=${resultVO.pdtNum}&pddNum=${resultVO.pddNum}&pdhNum=${resultVO.pdhNum}">


  <!-- container -->
  <div id="container"  class="content"  > 
    
    <!-- list -->
    <div class="regist-box2 ">
      <h2 ><span>소모품/예비품 정보</span>
        <div class="btn_group m-b-5 right "  >         
          <button id="btnSave" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_save.png" width="16" height="16" alt=""/> 저장 </button>
        </div>
      </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table table-view-regist " >
          <colgroup>
          <col width="170px">
          <col width="*">
          <col width="170px">
          <col width="*">
      
          </colgroup>
          <tbody>
							<tr>
								<th>행정동</th>
								<td>
									<select name="hjdCde" id="hjdCde"  disabled class="disable" >
										<option value="">선택안함</option>
										<c:forEach var="cmtList" items="${cmtAdarMaList}" varStatus="status">
											<option value="${cmtList.hjdCde }" ${resultVO.hjdCde == cmtList.hjdCde ? "selected" : "" } >${cmtList.hjdNam}</option>
										</c:forEach>
									</select>
								</td>
								<th>관리기관</th>
								<td>
									<select id="mngCde" name="mngCde"  disabled class="disable">
										<option value="">선택안함</option>
										<c:forEach var="cdeList" items="${dataCodeList}" varStatus="status">
											<option value="${cdeList.codeCode }" ${resultVO.mngCde == cdeList.codeCode ? "selected" : "" } >${cdeList.codeAlias}</option>
										</c:forEach>	
									</select>
								</td>
							</tr>
							<tr>
								<th>지형지물</th>
								<td>
									<select id="ftrCde" name="ftrCde" disabled class="disable" >
										<c:forEach var="cmtFtrcMaList" items="${cmtFtrcMaList}" varStatus="status">
											<option value="${cmtFtrcMaList.ftrCde }" ${resultVO.ftrCde == cmtFtrcMaList.ftrCde ? "selected" : "" } >${cmtFtrcMaList.ftrNam}</option>
										</c:forEach>
									</select>
								</td>
								<th>관리번호</th>
								<td>
									<input type="text" id="ftrIdn" name="ftrIdn"  readonly class="disable" value="${resultVO.ftrIdn}"/>
								</td>
							</tr>
							<tr>
								<th> 유지보수 사유 <img src="/fms/images/icon_table_required.png" width="12" height="12" alt=""/></th>
								<td>
									<input type="hidden" id="sbjCde" name="sbjCde" value=""/>
									<select class="sbjCde" disabled class="disable">
										<option value="">선택안함</option>
										<c:forEach var="sbjCdeList" items="${sbjCdeList}">
											<option value="${sbjCdeList.codeCode }"  ${wutlResultVO.sbjCde == sbjCdeList.codeCode ? "selected" : "" }>${sbjCdeList.codeAlias}</option>
										</c:forEach>
									</select>
								</td>							
								<th> 유지보수 구분  <img src="/fms/images/icon_table_required.png" width="12" height="12" alt=""/></th>
								<td>
									<input type="hidden" id="repCde" name="repCde" value=""/>
									<select class="repCde" disabled class="disable">
										<option value="">선택안함</option>	
										<c:forEach var="repCdeList" items="${repCdeList}">
											<option value="${repCdeList.codeCode }" ${repCdeList.codeCode == wutlResultVO.repCde ? "selected" : ""}>${repCdeList.codeAlias}</option>
										</c:forEach>
									</select>
								</td>
							</tr>
							<tr>							
								<th>유지보수 내용</th>
								<td colspan="3">
									<input type="text" name="repDes" id="repDes" disabled class="disable" value="${wutlResultVO.repDes}" />
								</td>
							</tr>
							<tr>
								<th>소모품/예비품</th>
								<td>
									<select id="pddCde" name="pddCde" disabled class="disable">
										<option value="0" ${resultVO.pddCde == '0' ? "selected" : "" }>소모품</option>
										<option value="1" ${resultVO.pddCde == '1' ? "selected" : "" }>예비품</option>
									</select>
								</td>
								<th>시공자</th>
								<td>
									<input type="text" name="oprNam" id="oprNam" disabled class="disable" value="${wutlResultVO.oprNam}"/>
								</td>
							</tr>
							<tr>
								<th>픔명(영문)</th>
								<td>
									<input type="text" id="pdtNam" name="pdtNam"  value="${resultVO.pdtNam }"  disabled class="disable"/>
								</td>
								<th>소모품/예비품 규격</th>
								<td>
									<input type="text" id="pdtStd" name="pdtStd" placeholder="" value="${resultVO.pdtStd }" disabled class="disable"/>
								</td>
							</tr>
							<tr>							
								<th>단위</th>
								<td>
									<input type="text" id="pdtUnt" name="pdtUnt" placeholder=""  value="${resultVO.pdtUnt }" disabled class="disable"/>
								</td>
								<th>모델</th>
								<td>
									<input type="text" id="pdtMdl" name="pdtMdl" placeholder="" value="${resultVO.pdtMdl }" disabled class="disable"/>
								</td>
							</tr>
							<tr>
								<th>제조사</th>
								<td>
									<input type="text" id="pdtMnf" name="pdtMnf" placeholder="" value="${resultVO.pdtMnf }" disabled class="disable"/>
								</td>
								<th>입출고재고 <img src="/fms/images/icon_table_required.png" width="12" height="12" alt=""/></th>
								<td>
									<input type="text" id="pdhCnt" name="pdhCnt" value="${resultVO.pdhCnt }"/>
								</td>
							</tr>
							<tr>
								<th>입출고 구분</th>
								<td>
									<input type="hidden" id="iotCde" name="iotCde" value="${resultVO.iotCde}" />
									<select disabled class="disable">
										<option value="I" ${resultVO.iotCde == 'I' ? "selected" : "" }>입고</option>
										<option value="O" ${resultVO.iotCde == 'O' ? "selected" : "" }>출고</option>
									</select>
								</td>
								<th>입출고일 <img src="/fms/images/icon_table_required.png" width="12" height="12" alt=""/></th>
								<td>
									<input type="text" id="iotYmd" name="iotYmd" class="datepicker"  value="${resultVO.iotYmd }"/>
								</td>
							</tr>
          </tbody>
        </table>
      </div>
    </div>
    <!-- //list --> 
    
  </div>
  
  <!-- //container --> 
</form>
  
</div>
<!-- //UI Object -->

</body>
</html>
