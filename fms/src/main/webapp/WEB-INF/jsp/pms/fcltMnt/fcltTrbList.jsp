<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<body>	
<script type="text/javascript">
var listLink = '${ctxRoot}/pms/fcltMnt/fcltTrbList.do';

$(document).ready(function() {
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();
	
	
	 /*******************************************
	 * 이벤트 설정
	 *******************************************/   
	$("#check_layer, #edit_layer").hide();
					    
	$('#btnReset').click(function () {
           var frm = $('#perrListFrm');
           frm.find('input[type=text]').val('');
           frm.find('select').val('');
        });
	
	$('#btnSearch').click(function () {
		fn_search(1);
	});
	
	$('#btnExcelDown').click(function() {
		var html = '';
		html += '<form id="excelForm" method="post">';
		html += '	<input type="hidden" name="searchCondition1" value= "' + $('#searchCondition1').val() + '" />';
		html += '	<input type="hidden" name="searchCondition2" value= "' + $('#searchCondition2').val() + '" />';
		html += '	<input type="hidden" name="searchCondition3" value= "' + $('#searchCondition3').val() + '" />';
		html += '	<input type="hidden" name="searchCondition4" value= "' + $('#searchCondition4').val() + '" />';
		html += '	<input type="hidden" name="searchCondition5" value= "' + $('#searchCondition5').val() + '" />';
		html += '</form>';
		$('#wrap').append(html);
		$('#excelForm').attr('action', '${ctxRoot}/perr/excel/download.do');
		$('#excelForm').submit();
	});
	
	setDatepicker();
	
	$ ('#searchCondition5').datepicker();
    $ ('#searchCondition5').datepicker("option", "maxDate", $ ("#searchCondition6").val());
    $ ('#searchCondition5').datepicker("option", "onClose", function ( selectedDate ) {
        $ ("#searchCondition6").datepicker( "option", "minDate", selectedDate );
    });
    
    $ ('#searchCondition6').datepicker();
    $ ('#searchCondition6').datepicker("option", "minDate", $ ("#searchCondition5").val());
    $ ('#searchCondition6').datepicker("option", "onClose", function ( selectedDate ) {
        $ ("#searchCondition5").datepicker( "option", "maxDate", selectedDate );
    });
	
	$('#reset').click(function () {
	    $('#searchCondition1').prop('selectedIndex', 0);
	    $('#searchCondition2').prop('selectedIndex', 0);
		$('#searchCondition3').val('');
		$('#searchCondition4').val('');
		$('#searchCondition5').prop('selectedIndex', '');
	});
	
	$('.edit_mode2').click(function () {
		if ($(this).find('input').size() == 0) {
			var html = '';
			html += '<input type="text" class="datepicker" style="height:24px;" id="" name="durYmd" /> ';
			$(this).html(html);
			
			$('#saveBtn').show();
		}
		
	});
	
	$('#saveBtn').click(function() {
		if ($('#durYmd').val() == '') {
			alert('교체예정일을 입력하세요.');
			return false;
		}
		var url = '${ctxRoot}/perr/pres/update.do';
		$('#perrListFrm').attr('action', url);
		$('#perrListFrm').submit();
	});
	
	//상세팝업
	$(".dtlPopup").click(function(event){
		var dtlLink = '${ctxRoot}/pms/fcltMnt/fcltTrbDtl.do?ftrCde='+$(this).attr('ftrCde')+'&ftrIdn='+$(this).attr('ftrIdn')+'&errNum='+$(this).attr('errNum');
		var centerPos = parseInt( $(screen).get(0).width ) / 2;
		var windowCenterPos = 858 / 2;
		var leftPos = centerPos - windowCenterPos;

		var _win = "";
		try{ opener.closeChild(); }catch(e){}
		_win = window.open(dtlLink, "child", "left="+leftPos+",top=100, width=700, height=285, menubar=no, location=no, resizable=no, toolbar=no, status=no");
		_win.focus();
		try{ opener._winChild = _win; }catch(e){}
		
	});
});

function fn_search(pageNo){
	if(gfn_isNull(pageNo)){
		pageNo = 1;
	}
   	$('form[name=perrListFrm] input[name=pageIndex]').val(pageNo);
   	$('#perrListFrm').attr('action', listLink);
	$('form[name=perrListFrm]').submit();
}
</script>
	
	
	
		
<div id="wrap" class="wrap" > 
  <div id="header" class="header"  >
    <h1 >시설물장애관리 검색목록</h1>
  </div>
		<form:form  commandName="searchVO" id="perrListFrm" name="perrListFrm" method="post" action="${ctxRoot}/pms/fcltMnt/fcltTrbList.do">
			<input type="hidden" name="pageIndex" value="1"/>
			
			
  <!-- container -->
  <div id="container"  class="content"  > 
				
				
    <div class="seach_box" >
      <h2 >검색항목 </h2>
      <div>
        <table border="0" cellpadding="0" cellspacing="0" class="table table-search">
          <colgroup>
          <col width="114px">
          <col width="*">
          </colgroup>
          
			<tbody>
				<tr>
					<th>지형지물</th>
					<td>
						<select name="searchCondition1" id="searchCondition1">
							<option value="">전체</option>
							<c:forEach var="cmtFtrcMaList" items="${cmtFtrcMaList}" varStatus="status">
								<option value="${cmtFtrcMaList.ftrCde }" ${param.searchCondition1 == cmtFtrcMaList.ftrCde ? "selected" : "" } >${cmtFtrcMaList.ftrNam}</option>
							</c:forEach>	
						</select>
					</td>
				</tr>
				<tr>
					<th>관리기관</th>
					<td>
						<select name="searchCondition2" id="searchCondition2">
							<option value="">전체</option>
							<c:forEach var="cdeList" items="${dataCodeList}" varStatus="status">
								<option value="${cdeList.codeCode }" ${param.searchCondition2 == cdeList.codeCode ? "selected" : "" } >${cdeList.codeAlias}</option>
							</c:forEach>	
						</select>
					</td>
				</tr>
				<tr>
					<th>행정동</th>
					<td>
						<select name="searchCondition3" id="searchCondition3">
							<option value="">전체</option>
							<c:forEach var="cmtList" items="${cmtAdarMaList}" varStatus="status">
								<option value="${cmtList.hjdCde }" ${param.searchCondition3 == cmtList.hjdCde ? "selected" : "" } >${cmtList.hjdNam}</option>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<th>장애유형</th>
					<td>
						<select name="searchCondition4" id="searchCondition4">
							<option value="">전체</option>
							<option value="001" ${param.searchCondition4 == 001 ? "selected" : "" }>정전상태</option>
							<option value="002" ${param.searchCondition4 == 002 ? "selected" : "" }>부품교체필요</option>
							<option value="003" ${param.searchCondition4 == 003 ? "selected" : "" }>일시적가동중단</option>
							<option value="004" ${param.searchCondition4 == 004 ? "selected" : "" }>노후부품교체알림</option>
							<option value="005" ${param.searchCondition4 == 005 ? "selected" : "" }>정기점검</option>
						</select>
					</td>
				</tr>
				<tr>
					<th>장애발생일자[이상]</th>
					<td>
						<!-- css쪽으로 설정 이동 할것 -->
						<input type="text" class="datepicker"  name="searchCondition5" id="searchCondition5" value="${param.searchCondition5}"/>
					</td>
				</tr>
				<tr>
					<th>장애발생일자[이하]</th>
					<td>
						<input type="text" class="datepicker"  name="searchCondition6" id="searchCondition6" value="${param.searchCondition6}"/>
					</td>
				</tr>
				<tr>
					<th>-</th>
					<td>	
						<input type="text" id="" name="" readonly class="disable" />																					
					</td>
				</tr>
				<tr>
					<th>-</th>
					<td>	
						<input type="text" id="" name="" readonly class="disable" />																					
					</td>
				</tr>
				<tr>
					<th>-</th>
					<td>	
						<input type="text" id="" name="" readonly class="disable" />																					
					</td>
				</tr>
				<tr>
					<th>-</th>
					<td>	
						<input type="text" id="" name="" readonly class="disable" />																					
					</td>
				</tr>
				<tr>
					<th>-</th>
					<td>	
						<input type="text" id="" name="" readonly class="disable" />																					
					</td>
				</tr>
			</tbody>
		</table>

      <div class="btn_group m-t-10 "  >
        <button id="btnReset" class="btn btn-search "   style="width:114px"><img src="/fms/images/icon_btn_reset.png" width="16" height="16" alt=""/> 초기화 </button>
        <button id="btnSearch" class="btn btn-search "  style="width:132px"><img src="/fms/images/icon_btn_search.png" width="16" height="16" alt=""/> 검색 </button>
      </div>
      
	</div>
	</div>
	<!-- /box_left -->
					
    <div class="list_box">
      <h2 ><span style="float:left">목록 | 소모품관리(${totCnt}건)</span>
        <div class="btn_group m-b-5 right "  >
          <button id="btnExcelDown" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_download.png" width="16" height="16" alt=""/> 다운로드 </button>
        </div>
      </h2>
						
	  <div class="list ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-striped table-list center " style="white-space: nowrap;">
			<thead>
				<tr>
					<th>순번</th>
					<th>관리기관</th>
					<th>지형지물</th>
					<th>행정동</th>
					<th>장애유형</th>
					<th>장애발생일자</th>
				</tr>
			</thead>			
			<tbody>
				<c:forEach var="list" items="${resultList}" varStatus="listIndex">																	
					<!-- tr class="viewPopup ${listIndex.index % 2 == 0 ? '' : 'tr_back01' }"  -->
					<tr>	
						<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' errNum='${list.errNum}' >
							<input type="hidden" id="ftrCde" name="ftrCde" value="${mapFtrcMa[list.ftrCde]}"/>
							<input type="hidden" id="ftrIdn" name="ftrIdn" value="${list.ftrIdn}" />
							${listIndex.index + 1}
						</td>
						<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' errNum='${list.errNum}' >${mapMngCde[list.mngCde]}</td>
						<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' errNum='${list.errNum}' >${mapFtrcMa[list.ftrCde]}</td>
						<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' errNum='${list.errNum}' >${mapAdarMa[list.hjdNum]}</td>
						<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' errNum='${list.errNum}' >
							<c:if test="${list.errPar == 001}">${list.errMsg}</c:if>
							<c:if test="${list.errPar == 002}">${list.errMsg}</c:if>
							<c:if test="${list.errPar == 003}">${list.errMsg}</c:if>
							<c:if test="${list.errPar == 004}">${list.errMsg}</c:if>
							<c:if test="${list.errPar == 005}">${list.errMsg}</c:if>							
						</td>
						<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' errNum='${list.errNum}' >${list.errYmd}</td>
					</tr>
				</c:forEach>

				<c:if test="${empty resultList}">
					<tr>
						<td colspan="7" style="color:black;text-align:center;">검색 결과가 없습니다.</td>
					</tr>
				</c:if>
			</tbody>
		</table>
	</div>

      <div style="width:100%; text-align:center">
        <ul class="pagination pagination-sm  " >
			<ui:pagination paginationInfo = "${paginationInfo}"  type="image" jsFunction="fn_search" />
        </ul>
      </div>
		
	</div>
	<!-- /box_right -->


	</div>
</form:form>
</div>


</body>

</html>
