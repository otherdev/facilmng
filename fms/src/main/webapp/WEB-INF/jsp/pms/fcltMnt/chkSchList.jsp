<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<!-- Calendar CSS -->
<link href="/fms/static/vendors/bower_components/fullcalendar/dist/fullcalendar.css" rel="stylesheet" type="text/css"/>
<!-- Custom CSS 
-->
<link href="/fms/css/common_popup.css" rel="stylesheet" />

<style>
	body {overflow-y: none;}
	div #calendar {
	    padding-top: 10px;
    	padding-left: 15px;
    	padding-right: 15px;
    	padding-bottom : 15px
    }   
    
#info.table>tbody>tr>td, .table>tbody>tr>th{
	padding: 0px;
}     
</style>

<script type="text/javascript">

$(document).ready(function(){
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();

	// 점검구분 콤보 공통코드
	gfn_setCombo("sclCde", "SCL_CDE", "" ,"전체", function(){
		// 진행상태 콤보 공통코드
		gfn_setCombo("sclStatCde", "SCL_STAT_CDE", "" ,"전체", function(){
		});
	});
	
	
	var sYear, sMonth, sDate;

	//서버에서 리턴받는 날짜가 있으면 그날짜 조회
	if ('${param.staYmd}') {
		var paramDate = new Date('${param.staYmd}');
		paramDate.setMonth(paramDate.getMonth() + 1);
		sYear = paramDate.getFullYear();
		sMonth = paramDate.getMonth() + 1;
		sMonth = sMonth < 10 ? '0' + sMonth : sMonth;
		sDate = '01';
	}
	//아니면 이번달..
	else {
		var now = new Date();
		sYear = now.getFullYear();
		sMonth = now.getMonth() + 1;
		sMonth = sMonth < 10 ? '0' + sMonth : sMonth;
		sDate = now.getDate() < 10 ? '0' + now.getDate() : now.getDate();
	}
	
	$('#ymdYear').val(sYear);
	$('#ymdMonth').val(sMonth);

	
	
	
	 /*******************************************
	 * 이벤트 설정
	 *******************************************/
		
	/**
		칼렌터초기화
	*/
	$('#calendar').fullCalendar({
      	header: {
          left: 'prev ',
          center: 'title ',
          right: 'today, next '
        },		
		lang: 'ko',
		defaultDate: sYear + '-' + sMonth + '-' + sDate,
        businessHours: true, // display business hours
        eventLimit: true, // allow "more" link when too many events

        events : function (start, end, timezone, callback) {
			$.ajax({
				url : "/fms/cmm/selectList.do",
				data : {sqlId:'selectChscMaList'
					, staYmd: $('#staYmd').val(), endYmd: $('#endYmd').val()
					, mngCde: $('#mngCde').val(), sclCde: $('#sclCde').val(), sclStatCde: $('#sclStatCde').val(), titNam: $('#titNam').val(), ckmPeo: $('#ckmPeo').val(), 
				},
				dataType : 'JSON',
				type : 'POST',
				async : false,
				success : function(ret) {
                    var events = [];
                    $.each(ret.data, function (idx, val) {
                        events.push({
                            title: val.titNam,
                            start: val.staYmd,
                            end: val.endYmd+'T23:59:59',
                            color: '#'+val.color,
                            sclNum: val.sclNum,
                            sclNm: val.sclNm,
                            sclStatNm: val.sclStatNm,
                            ckmPeo: val.ckmPeo,
                        });
                    });
                    
                    callback(events);
				},
				error : function(xhr, status, error) {
					alert("ERROR : " + error);
				}
			});
		}, 
		
		eventClick : function (event) {
			if (event.sclNum) {
				window.open('${ctxRoot}/pms/fcltMnt/chkSchDtl.do?sclNum='+ event.sclNum , "_blank", 'scrollbars=yes,status=no, width=1100, height=850');
			}
		},
		eventMouseover: function(calEvent, jsEvent) {
		    var tooltip = '<div class="tooltipevent" style="width:200px;height:100px;position:absolute;z-index:10001;"><div class="table-list-regist  "><table id="info" border="0" cellpadding="0" cellspacing="0"  class="table table-list-regist  " style="white-space: nowrap; ">  <tbody>	<tr>	  <th>점검구분</th>	  <td >'+ calEvent.sclNm +'</td>	</tr>	<tr>	  <th>점검명</th>	  <td >'+ calEvent.title +'</td>	</tr>	<tr>	  <th>점검자</th>	  <td >'+ calEvent.ckmPeo +'</td>	</tr>	<tr>	  <th>진행상태</th>	  <td >'+ calEvent.sclStatNm +'</td>	</tr>  </tbody></table></div></div>';
		    $("body").append(tooltip);
		    $(this).mouseover(function(e) {
		        $(this).css('z-index', 10000);
		        $('.tooltipevent').fadeIn('500');
		        $('.tooltipevent').fadeTo('10', 1.9);
		    }).mousemove(function(e) {
		        $('.tooltipevent').css('top', e.pageY + 10);
		        $('.tooltipevent').css('left', e.pageX + 20);
		    });
		},

		eventMouseout: function(calEvent, jsEvent) {
		     $(this).css('z-index', 8);
		     $('.tooltipevent').remove();
		},
	});
	
	
	
	//검색
	$('#btnSearch').click(function () {
		fn_search();
	});
	

	//초기화
	$('#btnReset').click(function () {
		var frm = $('.seach_box');
		frm.find('input[type="text"]').val('');
		frm.find('select').val('');
		$('#ymdYear').val(sYear);
		$('#ymdMonth').val(sMonth);
	});
		
	// 등록
	$('#btnAdd').click(function () {		
		window.open('${ctxRoot}/pms/fcltMnt/chkSchCreate.do', "일정관리", 'scrollbars=yes, status=no, width=680, height=350');
	});
		
	
	//오늘버튼
	$('button.fc-today-button').click(function () {
		var now = new Date();
		var year = now.getFullYear();
		var month = now.getMonth() + 1;
		month = month < 10 ? '0' + month : month;
		var date = now.getDate() < 10 ? '0' + now.getDate() : now.getDate();
		
		$('#ymdYear').val(year);
		$('#ymdMonth').val(month);
		
		$('#btnSearch').click();
	});
	
	//이전버튼
	$('button.fc-prev-button').click(function() {
	    e.preventDefault();
		var date = new Date();
		date.setFullYear(sYear);
		date.setMonth(parseInt(sMonth) - 1);
		date.setMonth(date.getMonth() - 1);
		
		var year = date.getFullYear();
		var month = date.getMonth() + 1;
		month = month < 10 ? '0' + month : month;
		
		
		if (year < $('#ymdYear option:first()').val()) {
			gfn_alert('해당 년도는 조회할 수 없습니다.');
		} else {
			$('#ymdYear').val(year);
			$('#ymdMonth').val(month);
		}
		
		$('#btnSearch').click();
    });

	//다음버튼
    $('button.fc-next-button').click(function() {
	    e.preventDefault();
    	var date = new Date();
		date.setFullYear(sYear);
		date.setMonth(parseInt(sMonth) - 1);
		date.setMonth(date.getMonth() + 1);
		
		var year = date.getFullYear();
		var month = date.getMonth() + 1;
		month = month < 10 ? '0' + month : month;
		
		
		if (year > $('#ymdYear option:last()').val()) {
			gfn_alert('해당 년도는 조회할 수 없습니다.');
		} else {
			$('#ymdYear').val(year);
			$('#ymdMonth').val(month);
		}
		
		$('#btnSearch').click();
    });
    	    	
    // 검색조건에 인원추가
	$('#usrNam').click(function () {
		window.open('${ctxRoot}/insp/inspMaAdd.do?sclNum=${wttChscMa.sclNum}', "작업자등록", 'status=no, width=560, height=330');
	});
	
});
	
	
//조회함수	
var fn_search =  function(){
	
	var staDate = new Date();
	staDate.setFullYear(parseInt($('#ymdYear').val()));
	staDate.setMonth(parseInt($('#ymdMonth').val()) - 1);
	staDate.setDate(1);
	staDate.setDate(staDate.getDate() - 7); //저번달 1주 데이터도 달력에 보여줌
	
	var staMonth = staDate.getMonth() + 1;
	staMonth = staMonth < 10 ? '0' + staMonth : staMonth;
	
	var staD = staDate.getDate() < 10 ? '0' + staDate.getDate() : staDate.getDate();
	
	var endDate = new Date();
	endDate.setFullYear(parseInt($('#ymdYear').val()));
	endDate.setMonth(parseInt($('#ymdMonth').val()) - 1);
	endDate.setMonth(endDate.getMonth() + 1);
	endDate.setDate(14); //다음달 2주 데이터도 달력에 보여줌
	
	var endMonth = endDate.getMonth() + 1;
	endMonth = endMonth < 10 ? '0' + endMonth : endMonth;
	
	var endD = endDate.getDate() < 10 ? '0' + endDate.getDate() : endDate.getDate();
	$('#staYmd').val(staDate.getFullYear() + '-' + staMonth + '-' + staD);
	$('#endYmd').val(endDate.getFullYear() + '-' + endMonth + '-' + endD);
	
    $('#calendar').fullCalendar('refetchEvents');
	/* 	
	    var events = [];
		$.ajax({
			url : "/fms/cmm/selectList.do",
			data : {sqlId:'selectChscMaList'
				, staYmd: $('#staYmd').val(), endYmd: $('#endYmd').val()
				, mngCde: $('#mngCde').val(), sclCde: $('#sclCde').val(), sclStatCde: $('#sclStatCde').val(), titNam: $('#titNam').val(), ckmPeo: $('#ckmPeo').val(), 
			},
			dataType : 'JSON',
			type : 'POST',
			async : false,
			success : function(ret) {
	            $.each(ret.data, function (idx,val) {
	                events.push({
	                    title: val.titNam,
	                    start: val.staYmd,
	                    end: val.endYmd,
	                    color: '#'+val.color,
	                    sclNum: val.sclNum,
	                });
	            });
	            //callback(events);
	            //$('#calendar').fullCalendar('removeEvents');
	            //$('#calendar').fullCalendar('renderEvents', events);
	            $('#calendar').fullCalendar('refetchEvents');
			},
			error : function(xhr, status, error) {
				alert("ERROR : " + error);
			}
		});
	 */	
};

function captureReturnKey(e) {
    if(e.keyCode==13 && e.srcElement.type != 'textarea'){
       	fn_search();
    }
}
	
</script>

	
	
<body>
	
<div id="wrap" class="wrap" > 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1 >점검일정조회</h1>
  </div>
  <!-- // header --> 

			
	<input type="hidden" name="staYmd" id="staYmd"/>
	<input type="hidden" name="endYmd" id="endYmd"/>


  <!-- container -->
  <div id="container"  class="content" > 
    
    <!-- list -->
    <div class="seach_box">
      <br><br>
      <h2 >검색항목 </h2>
    
      <div class="" >
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-search " >
          <colgroup>
          <col width="116px">
          <col width="134px">
          </colgroup>
          <tbody>
		<tbody>
			<tr>
				<th>관리기관</th>
				<td>
					<select name="mngCde" id="mngCde" style="width: 140px;">
						<option value="">전체</option>
						<c:forEach var="cdeList" items="${dataCodeList}" varStatus="status">
							<option value="${cdeList.codeCode }" ${param.mngCde == cdeList.codeCode ? "selected" : "" } >${cdeList.codeAlias}</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<th>점검구분</th>
				<td>
                    <select style="width:140px"  name="sclCde" id="sclCde"   >
                    </select>
				</td>
			</tr>
			<tr>
				<th>점검명</th>
				<td>								
					<input type="text" name="titNam" id="titNam" value="" style="width:140px" class="reqVal" onkeypress="captureReturnKey(event)"/>
				</td>
			</tr>
			<tr>
				<th>점검일자</th>
				<td>
					<select id="ymdYear" style="width: 70px;">
						<c:forEach var="year" begin="2017" step="1" end="2027">
							<option value="${year }">${year }</option>
						</c:forEach>
					</select>											
					<select id="ymdMonth" style="width: 40px;">
						<c:forEach var="month" begin="1" step="1" end="12">
							<c:if test="${month < 10 }">
								<option value="0${month }">0${month }</option>
							</c:if>
							<c:if test="${month >= 10 }">
								<option value="${month }">${month }</option>
							</c:if>
						</c:forEach>
					</select> <span>월</span>
				</td>
			</tr>	
			<tr>
				<th>점검자</th>
				<td>								
					<input type="text" name="ckmPeo" id="ckmPeo" value="" style="width:140px" onkeypress="captureReturnKey(event)"/>
				</td>
			</tr>
			<tr>
				<th>진행상태</th>
				<td>
                    <select style="width:140px"  name="sclStatCde" id="sclStatCde"   >
                    </select>
				</td>
			</tr>									
			<tr><th>&nbsp;</th><td></td></tr>								
			<tr><th>&nbsp;</th><td></td></tr>								
			<tr><th>&nbsp;</th><td></td></tr>								
			<tr><th>&nbsp;</th><td></td></tr>								
          </tbody>
        </table>
      </div>
      
      <div class="btn_group m-t-10 "  >
          	<button id="btnReset" type="button" class="btn btn-search "   style="width:80px"><img src="/fms/images/icon_btn_reset.png" width="16" height="16" alt=""/> 초기화 </button>
        	<button id="btnSearch" type="button" class="btn btn-search "  style="width:80px"><img src="/fms/images/icon_btn_search.png" width="16" height="16" alt=""/> 검색 </button>
        	<button id="btnAdd" type="button" class="btn btn-default "  style="width:80px"><img src="/fms/images/icon_btn_regist.png" width="16" height="16" alt=""/> 등록</button>
      </div>
      
      
      
    </div>
    <!-- //list -->  
    
    <!-- list -->
    <div class="list_box" style="border:0">
    
      <!--calendar box-->
      <div id="calendar" class="calenda-cont fc fc-unthemed fc-ltr" style="overflow-y: hidden">

      </div>
      <!--// -->
    </div>
    
     
      
  </div>
  
  <!-- //container --> 
  
</div>
<!-- //UI Object -->

<!-- Calender JavaScripts -->
<script src="${rscRoot}/vendors/bower_components/moment/min/moment.min.js"></script>
<script src="${rscRoot}/vendors/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="${rscRoot}/vendors/bower_components/fullcalendar/dist/lang-all.js"></script>
<script src="${rscRoot}/dist/js/fullcalendar-data.js"></script>

</body>
</html>



	

	
		
