<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<script type="text/javascript">

$(document).ready(function () {
	
    	
});

</script>
<body>

<div id="wrap" class="wrap" >  
  	<!-- header -->
	<div id="header" class="header"  >
		<h1 >소모품 상세 내역</h1>
  	</div>
  	<!-- // header --> 

	<form name="pjtMaHtSaveFrm" id="pjtMaHtSaveFrm" method="post">
		<div id="frmdiv"></div>
		<input type="hidden" id="pdhNum" name="pdhNum" value="${pdjtMaHtInfo.pdhNum}" />
			   
		<!-- container -->
		<div id="container"  class="content"  >    
	    	<div class="regist-box ">
		    	<div class="btn_group m-b-5 m-r-5 m-t-5 left">
		        	<h2 class="m-b-5">소모품정보</h2>
		      	</div>
		      <div class="view ">
		        <table border="0" cellpadding="0" cellspacing="0"  class="table table-view-regist " >
		          <colgroup>
		          <col width="114px">
		          <col width="*">
		          <col width="114px">
		          <col width="*">
		      
		          </colgroup>
		          <tbody>
					<tr>
						<th>관리기관</th>
						<td>
							${pdjtMaHtInfo.pdhNum}
						</td>
						<th>구분</th>
						<td>
						    ${pdjtMaHtInfo.pdtCatCdeNm}
						</td>
					</tr>
					<tr>
						<th>품명</th>
						<td>
							${pdjtMaHtInfo.pdtNam}
						</td>
						<th>모델 및 규격</th>
						<td>
							${pdjtMaHtInfo.pdtMdlStd}			
						</td>
					</tr>
					<tr>
						<th>단위</th>
						<td>
							${pdjtMaHtInfo.pdtUnt}
						</td>
						<th>제조사</th>
						<td>
							${pdjtMaHtInfo.pdtMnf}
						</td>
					</tr>
		          </tbody>
		        </table>
		      </div>
		    </div>
		    <!-- //list --> 
    
		    <!-- table 2 -->
		    <div class="regist-box ">
		    	<div class="btn_group m-b-5 m-r-5 m-t-5 left " >
		        	<h2 class="m-b-5">사용현황</h2>
		      	</div>
			    <!--tab list-->
			    <div class="list-regist">
			     
			    	<table border="0" cellpadding="0" cellspacing="0"  class="table table-list-regist  " style="white-space: nowrap; ">
			        	<colgroup>
			          		<col width="*">
			           		<col width="60px">
			          		<col width="120px">
			           		<col width="120px">          
			           		<col width="80px">
			          	</colgroup>
			          	<thead>
				            <tr>
				              	<th>지형지물명</th>
				              	<th>관리번호</th>
				              	<th>보수구분</th>
				              	<th>사용일자</th>
				              	<th>사용개수</th>
				            </tr>
			          	</thead>
						<tbody id="sub_table" class="td_add_pdjt">
							<c:if test="${fn:length(pdjtMaHtList) == 0}">
								<tr class="no-rows"><td colspan="5"> 조회내역이 없습니다. </td></tr>
							</c:if>
							<c:forEach var="list" items="${pdjtMaHtList}" varStatus="listIndex">
								<tr class="viewPopup">
									<td>${list.ftrNam }</td>
									<td>${list.ftrIdn}</td>
									<td>${list.rprCatCdeNm}</td>
									<td>${list.useYmd}</td>
									<td class="right">
										<fmt:formatNumber type="number" maxIntegerDigits="15" value="${list.useCnt}" />
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>   
			    </div>
			</div>    
		    <!-- table 2 -->    
		</div>  
  		<!-- //container --> 
	</form>
  
</div>
<!-- //UI Object -->

</body>
</html>

