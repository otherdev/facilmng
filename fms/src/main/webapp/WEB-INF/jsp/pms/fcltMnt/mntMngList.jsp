<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<script type="text/javascript">
var addLink = '${ctxRoot}/pms/link/lnkMntReg.do?ftrCde=${ftrCde}&ftrIdn=${ftrIdn}';
var listLink = '${ctxRoot}/pms/fcltMnt/mntMngList.do';

$(document).ready(function() {
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();
	
	
	 /*******************************************
	 * 이벤트 설정
	 *******************************************/
	
	$ ('#searchCondition6').datepicker();
    $ ('#searchCondition6').datepicker("option", "maxDate", $ ("#searchCondition7").val());
    $ ('#searchCondition6').datepicker("option", "onClose", function ( selectedDate ) {
        $ ("#searchCondition7").datepicker( "option", "minDate", selectedDate );
    });
 
    $ ('#searchCondition7').datepicker();
    $ ('#searchCondition7').datepicker("option", "minDate", $ ("#searchCondition6").val());
    $ ('#searchCondition7').datepicker("option", "onClose", function ( selectedDate ) {
        $ ("#searchCondition6").datepicker( "option", "maxDate", selectedDate );
    });
	
	$("#check_layer, #edit_layer").hide();
	
	$("#btnCrt").click(function(e) {
		e.preventDefault();
		var centerPos = parseInt( $(screen).get(0).width ) / 2;
		var windowCenterPos = 858 / 2;
		var leftPos = centerPos - windowCenterPos;
		
		var _win = window.open(addLink, this.title, "left="+leftPos+",top=100, width=563, height=590, menubar=no, location=no, resizable=no, toolbar=no, status=no");
		_win.focus();
	});	
		
	$('#btnReset').click(function () {
           var frm = $('#wutlHtListFrm');
           frm.find('input[type=text]').val('');
           frm.find('select').val('');
        });
	
	$('#btnSearch').click(function () {
		fn_search(1);
	});
	
	$('#wutlReset').click(function () {
	    $('#searchCondition1').prop('selectedIndex', 0);
	    $('#searchCondition2').prop('selectedIndex', 0);
		$('#searchCondition3').prop('selectedIndex', 0);
		$('#searchCondition4').val('');
		$('#searchCondition5').val('');
		$('#searchCondition6').val('');
	});
	
	$('#btnExcelDown').click(function() {
		var html = '';
		html += '<form id="excelForm" method="post">';
		html += '	<input type="hidden" name="searchCondition1" value= "' + $('#searchCondition1').val() + '" />';
		html += '	<input type="hidden" name="searchCondition2" value= "' + $('#searchCondition2').val() + '" />';
		html += '	<input type="hidden" name="searchCondition3" value= "' + $('#searchCondition3').val() + '" />';
		html += '	<input type="hidden" name="searchCondition4" value= "' + $('#searchCondition4').val() + '" />';
		html += '	<input type="hidden" name="searchCondition5" value= "' + $('#searchCondition5').val() + '" />';
		html += '	<input type="hidden" name="searchCondition6" value= "' + $('#searchCondition6').val() + '" />';
		html += '	<input type="hidden" name="searchCondition7" value= "' + $('#searchCondition7').val() + '" />';
		html += '</form>';
		$('#wrap').append(html);
		var url = '${ctxRoot}/wutl/excel/download.do';
		$('#excelForm').attr('action', url);
		$('#excelForm').submit();
	});
	
	/* 상세 클릭 Event */
	$(".dtlPopup").click(function(event){
		
		var dtlLink = '${ctxRoot}/pms/fcltMnt/mntMngDtl.do?repNum='+$(this).attr('repNum')+'&g2Id='+$(this).attr('g2Id')+'&ftrCde='+$(this).attr('ftrCde')+'&ftrIdn='+$(this).attr('ftrIdn');
		var centerPos = parseInt( $(screen).get(0).width ) / 2;
		var windowCenterPos = 858 / 2;
		var leftPos = centerPos - windowCenterPos;

		var _win = "";
		try{ opener.closeChild(); }catch(e){}
		_win = window.open(dtlLink, "child", "left="+leftPos+",top=100, width=563, height=590, menubar=no, location=no, resizable=no, toolbar=no, status=no");
		_win.focus();
		try{ opener._winChild = _win; }catch(e){}
		
	});
});

/* pagination 페이지 링크 function */
function fn_search(pageNo){   		
	if(gfn_isNull(pageNo)){
		pageNo = 1;
	}
   	$('form[name=wutlHtListFrm] input[name=pageIndex]').val(pageNo);
	$('form[name=wutlHtListFrm]').submit();
}

</script>
	
<body>
<div id="wrap" class="wrap" > 
  <div id="header" class="header"  >
    <h1 >유지보수대장 검색항목</h1>
  </div>
		
	<form:form  commandName="searchVO" id="wutlHtListFrm" name="wutlHtListFrm" method="post" action="${ctxRoot}/pms/fcltMnt/mntMngList.do">
		<input type="hidden" name="pageIndex" value="1"/>
			
    <div class="seach_box" >
      <h2 >검색항목 </h2>
      <div>
        <table border="0" cellpadding="0" cellspacing="0" class="table table-search">
          <colgroup>
          <col width="120px">
          <col width="134px">
          </colgroup>          
			<tbody>
				<tr>
					<th>지형지물</th>
					<td>
						<select name="searchCondition1" id="searchCondition1">
							<option value="">전체</option>
							<c:forEach var="cmtFtrcMaList" items="${cmtFtrcMaList}" varStatus="status">
								<option value="${cmtFtrcMaList.ftrCde }" ${param.searchCondition1 == cmtFtrcMaList.ftrCde ? "selected" : "" } >${cmtFtrcMaList.ftrNam}</option>
							</c:forEach>	
						</select>
					</td>
				</tr>
				<tr>
					<th>관리번호</th>
					<td>
						<input type="text" id="searchCondition2" name="searchCondition2" maxlength="10" value="${param.searchCondition2}"/>
					</td>
				</tr>
				<tr>
					<th>구분</th>
					<td>
						<select name="searchCondition3" id="searchCondition3">
							<option value="">전체</option>
							<c:forEach var="repCdeList" items="${repCdeList}" varStatus="status">
								<option value="${repCdeList.codeCode }" ${param.searchCondition3 == repCdeList.codeCode ? "selected" : "" } >${repCdeList.codeAlias}</option>
							</c:forEach>	
						</select>
					</td>
				</tr>
				<tr>
					<th>사유</th>
					<td>
						<select name="searchCondition4" id="searchCondition4">
							<option value="">전체</option>
								<c:forEach var="sbjCdeList" items="${sbjCdeList}" varStatus="status">
									<option value="${sbjCdeList.codeCode }" ${param.searchCondition4 == sbjCdeList.codeCode ? "selected" : "" } >${sbjCdeList.codeAlias}</option>
								</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<th>시공자</th>
					<td>	
						<input type="text" id="searchCondition5" name="searchCondition5" value="${param.searchCondition5 }"/>																					
					</td>
				</tr>
				<tr>
					<th>유지보수일자[이상]</th>
					<td>
						<!-- css쪽으로 설정 이동 할것 -->
						<input type="text" class="datepicker"  name="searchCondition6" id="searchCondition6" value="${param.searchCondition6}"/>
					</td>
				</tr>
				<tr>
					<th>유지보수일자[이하]</th>
					<td>
						<input type="text" class="datepicker"  name="searchCondition7" id="searchCondition7" value="${param.searchCondition7}"/>
					</td>
				</tr>
				<tr>
					<th>-</th>
					<td>	
						<input type="text" id="" name="" readonly class="disable" />																					
					</td>
				</tr>
				<tr>
					<th>-</th>
					<td>	
						<input type="text" id="" name="" readonly class="disable" />																					
					</td>
				</tr>
				<tr>
					<th>-</th>
					<td>	
						<input type="text" id="" name="" readonly class="disable" />																					
					</td>
				</tr>
				<tr>
					<th>-</th>
					<td>	
						<input type="text" id="" name="" readonly class="disable" />																					
					</td>
				</tr>
			</tbody>
		</table>

      <div class="btn_group m-t-10 "  >
        <button id="btnReset" class="btn btn-search "   style="width:114px"><img src="/fms/images/icon_btn_reset.png" width="16" height="16" alt=""/> 초기화 </button>
        <button id="btnSearch" class="btn btn-search "  style="width:132px"><img src="/fms/images/icon_btn_search.png" width="16" height="16" alt=""/> 검색 </button>
      </div>
      
	</div>
	</div>
	<!-- /box_left -->


    <div class="list_box">
      <h2 ><span style="float:left">목록 | 유지보수대장(${totCnt}건)</span>
        <div class="btn_group m-b-5 right "  >
			<button id="btnCrt" type="button" class="btn btn-default "><img src="/fms/images/icon_btn_regist.png" width="16" height="16"alt="" />등록</button>
			<button id="btnExcelDown" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_download.png" width="16" height="16" alt=""/> 다운로드 </button>
        </div>
      </h2>
						
	  <div class="list ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-striped table-list center " style="white-space: nowrap;">
			<thead>
				<tr>
					<th>순번</th>
					<th>지형지물</th>
					<th>관리번호</th>
					<th>구분</th>
					<th>사유</th>
					<th>시공자</th>
					<th>유지보수일자</th>
					<th>유지보수내용</th>
				</tr>
			</thead>
			
			<tbody>
				<c:forEach var="list" items="${resultList}" varStatus="listIndex">																	
					<!-- tr class="viewPopup ${listIndex.index % 2 == 0 ? '' : 'tr_back01' }" popup-data="width=563, height=570" -->
					<tr>	
						<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' repNum='${list.repNum}' g2Id='${list.g2Id}'>${listIndex.index + 1}</td>
						<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' repNum='${list.repNum}' g2Id='${list.g2Id}'>${mapFtrcMa[list.ftrCde]}</td>
						<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' repNum='${list.repNum}' g2Id='${list.g2Id}'>${list.ftrIdn}</td>
						<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' repNum='${list.repNum}' g2Id='${list.g2Id}'>${list.repCde}</td>
						<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' repNum='${list.repNum}' g2Id='${list.g2Id}'>${list.sbjCde}</td>
						<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' repNum='${list.repNum}' g2Id='${list.g2Id}'>${list.oprNam}</td>
						<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' repNum='${list.repNum}' g2Id='${list.g2Id}'>${list.repYmd}</td>
						<td class="td_left dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' repNum='${list.repNum}' g2Id='${list.g2Id}'>${list.repDes}</td>
					</tr>
				</c:forEach>

				<c:if test="${empty resultList}">
					<tr>
						<td colspan="7" style="color:black;text-align:center;">검색 결과가 없습니다.</td>
					</tr>
				</c:if>
			</tbody>
		</table>
	</div>

      <div style="width:100%; text-align:center">
        <ul class="pagination pagination-sm  " >
			<ui:pagination paginationInfo = "${paginationInfo}"  type="image" jsFunction="fn_search" />
        </ul>
      </div>
		
	</div>
	<!-- /box_right -->


	</div>
</form:form>
</div>


</body>

</html>
