<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>
    
<title>시설물 노후도관리</title>
<script type="text/javascript">	
var popupX = (screen.availWidth-660)/2;
var popupY = (screen.availHeight-1130)/3;

$(document).ready(function() {
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();
	
	/*******************************************
	 * 이벤트 설정
	 *******************************************/	
	
	$('#btnReset').click(function () {
        var frm = $('#optMtFrm');
        frm.find('input[type=text]').val('');
        frm.find('select').val('');
    });

	$('#btnSearch').click(function () {
		fn_search(1);
	});	
		
	$('#btnExcelDown').click(function() {
		var html = '';
		html += '<form id="excelForm" method="post">';
		html += '	<input type="hidden" name="searchCondition1" value= "' + $('#searchCondition1').val() + '" />';
		html += '	<input type="hidden" name="searchCondition2" value= "' + $('#searchCondition2').val() + '" />';
		html += '	<input type="hidden" name="searchCondition3" value= "' + $('#searchCondition3').val() + '" />';
	
		html += '</form>';
		$('#wrap').append(html);
		var url = '${ctxRoot}/pms/fcltMnt/oldMngList/excel/download.do';
		$('#excelForm').attr('action', url);
		$('#excelForm').submit();
	});
	
	/* 상세 클릭 Event */
	$(".dtlPopup").click(function(event){
		
		var dtlLink = '${ctxRoot}/pms/fcltMnt/oldMngDtl.do?g2Id='+$(this).attr('g2Id')+'&ftrCde='+$(this).attr('ftrCde')+'&ftrIdn='+$(this).attr('ftrIdn');
		var centerPos = parseInt( $(screen).get(0).width ) / 2;
		var windowCenterPos = 858 / 2;
		var leftPos = centerPos - windowCenterPos;

		var _win = "";
		try{ opener.closeChild(); }catch(e){}
		_win = window.open(dtlLink, "child", "left="+leftPos+",top=100, width=680, height=460, menubar=no, location=no, resizable=no, toolbar=no, status=no");
		_win.focus();
		try{ opener._winChild = _win; }catch(e){}
		
	});
		
});

/* pagination 페이지 링크 function */
function fn_search(pageNo){   		
	if(gfn_isNull(pageNo)){
		pageNo = 1;
	}
   	$('form[name=optMtFrm] input[name=pageIndex]').val(pageNo);
	$('form[name=optMtFrm]').submit();
}

function captureReturnKey(e) {
    if(e.keyCode==13 && e.srcElement.type != 'textarea'){
    	removeComma();
       	fn_search();
    }
}
</script>

<style>
.popup_layer {margin: 0px auto 0 auto;}
#popup_layer01{width:100%; left:0; top:0; margin-top:0; margin-left:0;}
</style>

<body>

<div id="wrap" class="wrap" > 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1 >시설물 노후도 검색목록</h1>
  </div>
  <!-- // header --> 
  
  
<form name="optMtFrm" id="optMtFrm" method="post">
  <input type="hidden" name="pageIndex" value="1"/>
  
  <!-- container -->
  <div id="container"  class="content"  > 
    
    <!-- seach -->
    <div class="seach_box" >
      <h2 >검색항목 </h2>
      <div>
        <table border="0" cellpadding="0" cellspacing="0" class="table table-search">
          <caption>조회양식</caption>
          <colgroup>
          <col width="114px">
          <col width="*">
          </colgroup>
          <tbody>
            <tr>
              <th>관리번호</th>
              <td>
              	<input type="text" name="searchCondition1" id="searchCondition1" maxlength="10" value="${param.searchCondition1 }" onkeypress="captureReturnKey(event)"/>
              </td>
            </tr>
            <tr>
              <th>가압장명</th>
              <td>
				<input type="text" name="searchCondition2" id="searchCondition2" maxlength="50" value="${param.searchCondition2 }" onkeypress="captureReturnKey(event)"/>
              </td>
            </tr>
            <tr>
              <th>세부시설명</th>
              <td>
				<input type="text" name="searchCondition3" id="searchCondition3" maxlength="50" value="${param.searchCondition3 }" onkeypress="captureReturnKey(event)"/>
              </td>
            </tr>
            <tr>
              <th>-</th>
              <td>
				<input type="text" id="" name="" readonly class="disable" />
              </td>
            </tr>
            <tr>
              <th>-</th>
              <td>
				<input type="text" id="" name="" readonly class="disable" />
              </td>
            </tr>
            <tr>
              <th>-</th>
              <td>
              	<input type="text" id="" name="" readonly class="disable" />
              </td>
            </tr>
            <tr>
               <th>-</th>
              <td>
              	<input type="text" id="" name="" readonly class="disable" />
              </td>
            </tr>            
            <tr>
               <th>-</th>
              <td>
              	<input type="text" id="" name="" readonly class="disable" />
              </td>
            </tr>
            <tr>
               <th>-</th>
              <td>
              	<input type="text" id="" name="" readonly class="disable" />
              </td>
            </tr>
            <tr>
               <th>-</th>
              <td>
              	<input type="text" id="" name="" readonly class="disable" />
              </td>
            </tr>
            <tr>
              <th>-</th>
              <td>
              	<input type="text" id="" name="" readonly class="disable" />
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="btn_group m-t-10 "  >
        <button id="btnReset" class="btn btn-search "  type="button" style="width:114px"><img src="/fms/images/icon_btn_reset.png" width="16" height="16" alt=""/> 초기화 </button>
        <button id="btnSearch" class="btn btn-search " type="button" style="width:132px"><img src="/fms/images/icon_btn_search.png" width="16" height="16" alt=""/> 검색 </button>
      </div>
    </div>
    <!-- //seach --> 
    
    <!-- list -->
    <div class="list_box">
      <h2 ><span style="float:left">목록 | 시설물 노후도(${paginationInfo.totalRecordCount }건)</span>
        <div class="btn_group m-b-5 right " >
          <button id="btnExcelDown" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_download.png" width="16" height="16" alt=""/> 다운로드 </button>
        </div>
      </h2>
      <div class="list " >      	
        <table id="tbList" name="tbList" border="0" cellpadding="0" cellspacing="0"  class="table  table-striped table-list center " style="white-space: nowrap;">
          <colgroup>
          <col width="60px">
          <col width="100px">
          <col width="60px">
          <col width="100px">
          <col width="*">
          <col width="60px">
          <col width="60px">
          <col width="100px">
          </colgroup>
          <thead>
            <tr>
            	<th>일련번호</th>
				<th>지형지물명</th>
				<th>관리번호</th>
				<th>주요도</th>
				<th>내구연한</th>
				<th>설치년도</th>
				<th>노후도</th>
				<th>노후등급</th>
				<th>점검주기</th>
				<th>최근점검일자</th>
            </tr>
          </thead>
          <tbody>
			<c:forEach var="list" items="${resultList}" varStatus="listIndex">									
				<tr>	
					<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' g2Id='${list.g2Id}'>
						${list.g2Id}
					</td>
					<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' g2Id='${list.g2Id}'>
						${list.ftrNam}
					</td>
					<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' g2Id='${list.g2Id}'>
						${list.ftrIdn}
					</td>
					<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' g2Id='${list.g2Id}'>
						${list.impNam}
					</td>
					<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' g2Id='${list.g2Id}'>
						${list.lmtYear}
					</td>
					<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' g2Id='${list.g2Id}'>
						${list.creYy}
					</td>	
					<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' g2Id='${list.g2Id}'>
						${list.oldRate}
					</td>
					<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' g2Id='${list.g2Id}'>
						${list.oldLvl}
					</td>					
					<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' g2Id='${list.g2Id}'>						
						${list.chkPrd}						
					</td>
					<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' g2Id='${list.g2Id}'>
						${list.chkResultYmd}
					</td>					
				</tr>
			</c:forEach>
			<c:if test="${empty resultList}">
				<tr>
					<td colspan="10" style="color:black;text-align:center;">검색 결과가 없습니다.</td>
				</tr>
			</c:if>         
          </tbody>
        </table>
      </div>
		<!-- Paging -->
      <div style="width:100%; text-align:center">
        <ul class="pagination pagination-sm  " >
			<ui:pagination paginationInfo = "${paginationInfo}"  type="image" jsFunction="fn_search" />
        </ul>    
      </div>      
    </div>
    <!-- //list -->    
  </div>  
  <!-- //container -->  
</form>  
</div>
</body>
</html>