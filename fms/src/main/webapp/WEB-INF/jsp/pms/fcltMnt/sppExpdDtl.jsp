<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<script type="text/javascript">
$(document).ready(function() {
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();
		
	 /*******************************************
	 * 이벤트 설정
	 *******************************************/	 
	$("#btnDel").click(function(e) {
		gfn_confirm("삭제하시겠습니까?",function(ret){
			if(!ret) return;
									
			var pdtNum = "${resultVO.pdtNum}";
						
			var form = new FormData();
			form.append("pdtNum", pdtNum);

			$.ajax({
				url : "${ctxRoot}/pdjt/deletePdjtDt.do",
				data : form,
				//dataType : 'JSON',
				processData : false,
				contentType : false,
				type : 'POST',
				success : function(data) {
					gfn_alert('데이터가 삭제되었습니다.','',function(){
						opener.fn_search();
						window.close();
					});
				},
				error : function(xhr, status, error) {
					gfn_alert("데이터 삭제에 실패하였습니다. : " + error,"E");
				}
			});
		});
		
	});
	
	
	$("#btnSave").click(function(e) {
		e.preventDefault();
		
		if($("#pdtNam").val()=="" || $("#pdtNam").val()==" " || $("#pdtNam").val()==null) {
			gfn_alert('품명을 입력해주세요.','',function(){
				$('#pdtNam').focus();	
			});			
			return false;
		}
		
		if($("#pdtStd").val()=="" || $("#pdtStd").val()==" " || $("#pdtStd").val()==null) {
			gfn_alert('소모품/예비품 규격을 입력해주세요.','',function(){
				$('#pdtStd').focus();	
			});			
			$('#pdtStd').focus();
			
			return false;
		}
		
		if($("#pdeNam").val()=="" || $("#pdeNam").val()==" " || $("#pdeNam").val()==null) {
			gfn_alert('품명(영문)을 입력해주세요.','',function(){
				$('#pdtNam').focus();	
			});			
			$('#pdeNam').focus();
			
			return false;
		}
		
		if($("#pdtUnt").val()=="" || $("#pdtUnt").val()==" " || $("#pdtUnt").val()==null) {
			gfn_alert('단위를 입력해주세요.','',function(){
				$('#pdtNam').focus();	
			});			
			$('#pdtUnt').focus();
			
			return false;
		}
		
		if($("#pdtMnf").val()=="" || $("#pdtMnf").val()==" " || $("#pdtMnf").val()==null) {
			alert('제조사를 입력해주세요.','',function(){
				$('#pdtNam').focus();	
			});			
			$('#pdtMnf').focus();
			
			return false;
		}
		
		if($("#pdtMdl").val()=="" || $("#pdtMdl").val()==" " || $("#pdtMdl").val()==null) {
			alert('모델을 입력해주세요.','',function(){
				$('#pdtMdl').focus();	
			});	
			
			return false;
		}
		
		//$("#pdjtDtEditFrm").submit();
		
		gfn_confirm("저장하시겠습니까?",function(ret){
			if(!ret) return;
    		
			var form = new FormData(document.getElementById("pdjtDtEditFrm"));
			
			$.ajax({
				url : $("#pdjtDtEditFrm").attr('action'),
				data : form,
				//dataType : 'JSON',
				processData : false,
				contentType : false,
				type : 'POST',
				success : function(data) {
					gfn_alert('데이터가 저장되었습니다.','',function(){
						opener.fn_search();
						fn_reload();
					});
				},
				error : function(xhr, status, error) {
					gfn_alert("데이터 저장에 실패하였습니다. : " + error,"E");
				}
			});
		});
	});
	
	$("#pdtUnt").val(<c:out value="${resultVO.pdtUnt}"/>);
		
});

var fn_reload = function(){
	location.reload();
}
</script>
	
	
	
	
<div id="wrap" class="wrap" > 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1 >소모품/예비품 대장</h1>
  </div>
  <!-- // header --> 

		<form id="pdjtDtEditFrm" name="pdjtDtEditFrm" method="post" action="${ctxRoot}/pdjt/updatePdjtDt.do?pdtNum=${resultVO.pdtNum}">
		
		
  <!-- container -->
  <div id="container"  class="content"  > 
    
    <!-- list -->
    <div class="regist-box2 ">
      <h2 ><span>소모품/예비품 정보</span>
        <div class="btn_group m-b-5 right "  >         
          <button id="btnDel" type="button" class="btn btn-default " > <img src="/fms/images/icon_btn_delete.png" width="16" height="16" alt=""/> 삭제</button>
          <button id="btnSave" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_save.png" width="16" height="16" alt=""/> 저장 </button>
        </div>
      </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table table-view-regist " >
          <colgroup>
          <col width="170px">
          <col width="*">
          <col width="170px">
          <col width="*">
      
          </colgroup>
          <tbody>
							<tr>
								<th>품명  <img src="/fms/images/icon_table_required.png" width="12" height="12" alt=""/></th>
								<td>
									<input type="text" id="pdtNam" name="pdtNam" value="${resultVO.pdtNam}" />
								</td>
								<th>소모품/예비품 규격  <img src="/fms/images/icon_table_required.png" width="12" height="12" alt=""/></th>
								<td>
									<input type="text" id="pdtStd" name="pdtStd" value="${resultVO.pdtStd}"/>
								</td>
							</tr>
							<tr>
								<th>품명(영문)  <img src="/fms/images/icon_table_required.png" width="12" height="12" alt=""/></th>
								<td>
									<input type="text" id="pdeNam" name="pdeNam" value="${resultVO.pdeNam}"/>
								</td>
								<th>단위  <img src="/fms/images/icon_table_required.png" width="12" height="12" alt=""/></th>
								<td>
									<input type="text" id="pdtUnt" name="pdtUnt" value="${resultVO.pdtUnt }">
								</td>
							</tr>
							<tr>
								<th>제조사  <img src="/fms/images/icon_table_required.png" width="12" height="12" alt=""/></th>
								<td>
									<input type="text" id="pdtMnf" name="pdtMnf" value="${resultVO.pdtMnf}"/>
								</td>
								<th>모델  <img src="/fms/images/icon_table_required.png" width="12" height="12" alt=""/></th>
								<td>
									<input type="text" id="pdtMdl" name="pdtMdl" value="${resultVO.pdtMdl}"/>
								</td>
							</tr>
						</tbody>
          </tbody>
        </table>
      </div>
    </div>
    <!-- //list --> 
    
  </div>
  
  <!-- //container --> 
</form>
  
</div>
<!-- //UI Object -->

</body>
</html>
