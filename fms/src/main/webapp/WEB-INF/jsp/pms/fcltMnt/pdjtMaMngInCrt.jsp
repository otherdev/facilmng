<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<script type="text/javascript" src="<c:url value='/js/pms/fcltMnt/pdjtMaMngInCrt.js' />" ></script>

<script type="text/javascript">
$(document).ready(function() {
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();
	
	fn_search();	
	/*******************************************
	 * 이벤트 설정
	 *******************************************/
	$('#btnSave').click(function () {
		myGrid.editCellClear(0,0,0);
		myGrid.selectClear();

		// ajax 저장 
		var _lst = fnObj.grid.getList();
		
	    //그리드필수체크
		myGrid.editCellClear(0,0,0);
		myGrid.selectClear();
		if(!myGrid.validateCheck('C') || !myGrid.validateCheck('U') ){
			return false;
		}	 

		// 저장
		gfn_confirm("저장하시겠습니까?", function(ret){
			if(!ret) return;
			//저장처리
			gfn_saveCmmList(
				{lst: _lst, sqlId: 'updatePdjtInHtPop'}
				, function(data){
					if (data.result) {
						gfn_alert("저장되었습니다.");						
						fn_search();							
					}
					else{
						var err_msg = "저장에 실패하였습니다.";
						try{
							err_msg = data.error;
						}catch(e){}
						gfn_alert(err_msg,"E");
						fn_search();
						return;
					}
			});
		});
	});		
});

/* 조회 function */
function fn_search(){

	keyPdhNum = '${param.pdhNum}';
	gfn_selectList({sqlId: 'selectPdjtInHtPopList', pdhNum : '${param.pdhNum}'}, function(ret){
		lst = ret.data;		
		// 그리드 초기화		
		fnObj.pageStart();
	});	
}

</script>

<div id="wrap" class="wrap" > 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1 >예비품/소모품관리</h1>
  </div>
  <!-- // header --> 

  <form id="pdjtMaCrtFrm" name="pdjtMaCrtFrm" method="post" action="#">

  <!-- container -->
  <div id="container"  class="content"> 
 	
	<div class="regist-box2 ">
		<h2><span>입고등록</span>
	   	<div class="btn_group m-b-5 right "  >  			
  			<div class="view ">
  				<div border="1" class="btn_group" style="padding-bottom:5px;padding-right:15px;">
		            <input type="button" value="추가" class="AXButton Green" onclick="fnObj.grid.append();"/>
		            <input type="button" value="삭제" class="AXButton Green" onclick="fnObj.grid.remove();"/>		            
		            <input type="button" id="btnSave" value="저장" class="AXButton Green"/>          		            		            	        	
		        </div>					  	
				<div id="grdPay" class="table table-striped table-list center" style="width:100%;height:400px;margin-left:-15px;"></div>
  			</div>
	   	</div></h2>	   	
  	</div>
  </div>
  
  <!-- //container --> 
</form>
  
</div>
<!-- //UI Object -->

</body>
</html>


