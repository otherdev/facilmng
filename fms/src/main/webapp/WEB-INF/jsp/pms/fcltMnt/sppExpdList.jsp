<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>


<script type="text/javascript">	
	
var addLink = '${ctxRoot}/pms/fcltMnt/sppExpdAdd.do';
var popupX = (screen.availWidth-660)/2;
var popupY= (screen.availHeight-1130)/3;

$(document).ready(function() {
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();
	
	
	 /*******************************************
	 * 이벤트 설정
	 *******************************************/
	$('#btnCrtPop').click(function () {
		window.open(addLink, "_blank", 'status=no, width=690, height=220, left='+ popupX + ', top='+ popupY + ', resizable=no');
	});
	 
	$('#btnReset').click(function () {
		var frm = $('#pdjtDtListfrm');
		frm.find('input[type=text]').val('');
		frm.find('select').val('');
	});
	
	$('#btnSearch').click(function () {
		fn_search(1);
	});

	$('#btnExcelDown').click(function() {
		var html = '';
		html += '<form id="excelForm" method="post">';
		html += '	<input type="hidden" name="searchCondition1" value= "' + $('#searchCondition1').val() + '" />';
		html += '	<input type="hidden" name="searchCondition2" value= "' + $('#searchCondition2').val() + '" />';
		html += '	<input type="hidden" name="searchCondition3" value= "' + $('#searchCondition3').val() + '" />';
		html += '	<input type="hidden" name="searchCondition4" value= "' + $('#searchCondition4').val() + '" />';
		html += '	<input type="hidden" name="searchCondition5" value= "' + $('#searchCondition5').val() + '" />';
		html += '	<input type="hidden" name="searchCondition6" value= "' + $('#searchCondition6').val() + '" />';
		html += '	<input type="hidden" name="searchCondition7" value= "' + $('#searchCondition7').val() + '" />';
		html += '</form>';
		$('#wrap').append(html);
		$('#excelForm').attr('action', '${ctxRoot}/pdjtDt/excel/download.do');
		$('#excelForm').submit();
	});
	
	//상세팝업
	$(".dtlPopup").click(function(event){

		var dtlLink = '${ctxRoot}/pms/fcltMnt/sppExpdDtl.do?pdtNum='+$(this).attr('pdtNum')+'&pddNum='+$(this).attr('pddNum');
		var centerPos = parseInt( $(screen).get(0).width ) / 2;
		var windowCenterPos = 858 / 2;
		var leftPos = centerPos - windowCenterPos;

		var _win = "";
		try{ opener.closeChild(); }catch(e){}
		_win = window.open(dtlLink, "child", "left="+leftPos+",top=100, width=690, height=220, menubar=no, location=no, resizable=no, toolbar=no, status=no");
		_win.focus();
		try{ opener._winChild = _win; }catch(e){}
		
	});
});	
	
/* pagination 페이지 링크 function */
function fn_search(pageNo){
	if(gfn_isNull(pageNo)){
		pageNo = 1;
	}	
	document.getElementById("pdjtDtListfrm").pageIndex.value = pageNo;
   	document.getElementById("pdjtDtListfrm").submit();
}

</script>
	
	
<body>
	
	
<div id="wrap" class="wrap" > 
	<div id="header" class="header"  >
	  <h1 >예비품/소모품 현황</h1>
	</div>
	  
	<form:form  commandName="searchVO" id="pdjtDtListfrm" name="pdjtDtListfrm" method="post" action="${ctxRoot}/pms/fcltMnt/sppExpdList.do">
	<input type="hidden" name="pageIndex" value="1"/>			
    <div class="seach_box" >
      <h2 >검색항목 </h2>
      <div>
        <table border="0" cellpadding="0" cellspacing="0" class="table table-search">
          <colgroup>
          <col width="114px">
          <col width="*">
          </colgroup>
          
			<tbody>
				<tr>
					<th>구분</th>
					<td>
						<select name="searchCondition1" id="searchCondition1">
							<option value="">전체</option>
							<option value="0" ${param.searchCondition1 == '0' ? "selected" : "" }>소모품</option>
							<option value="1" ${param.searchCondition1 == '1' ? "selected" : "" }>예비품</option>
						</select>
					</td>
				</tr>
				<tr>
					<th>품명</th>
					<td>
						<input type="text" id="searchCondition2" name="searchCondition2" maxlength="50" value="${param.searchCondition2}"/>
					</td>
				</tr>
				<tr>
					<th>규격</th>
					<td>
						<input type="text" id="searchCondition3" name="searchCondition3" maxlength="10" value="${param.searchCondition3}"/>
					</td>
				</tr>
				<tr>
					<th>제조사</th>
					<td>
						<input type="text" id="searchCondition4" name="searchCondition4" maxlength="50"  value="${param.searchCondition4}"/>
					</td>
				</tr>
				<tr>
					<th>모델</th>
					<td>
						<input type="text" id="searchCondition7" name="searchCondition7" maxlength="10" value="${param.searchCondition7}"/>
					</td>
				</tr>
				<tr>
					<th>-</th>
					<td>	
						<input type="text" id="" name="" readonly class="disable" />																					
					</td>
				</tr>
				<tr>
					<th>-</th>
					<td>	
						<input type="text" id="" name="" readonly class="disable" />																					
					</td>
				</tr>
				<tr>
					<th>-</th>
					<td>	
						<input type="text" id="" name="" readonly class="disable" />																					
					</td>
				</tr>
				<tr>
					<th>-</th>
					<td>	
						<input type="text" id="" name="" readonly class="disable" />																					
					</td>
				</tr>
				<tr>
					<th>-</th>
					<td>	
						<input type="text" id="" name="" readonly class="disable" />																					
					</td>
				</tr>
				<tr>
					<th>-</th>
					<td>	
						<input type="text" id="" name="" readonly class="disable" />																					
					</td>
				</tr>
			</tbody>
		</table>

      <div class="btn_group m-t-10 "  >
        <button id="btnReset" class="btn btn-search "   style="width:114px"><img src="/fms/images/icon_btn_reset.png" width="16" height="16" alt=""/> 초기화 </button>
        <button id="btnSearch" class="btn btn-search "  style="width:132px"><img src="/fms/images/icon_btn_search.png" width="16" height="16" alt=""/> 검색 </button>
      </div>
      
	</div>
	</div>
	<!-- /box_left -->

    <div class="list_box">
      <h2 ><span style="float:left">목록 | 예비품/소모품 현황(${totCnt}건)</span>
        <div class="btn_group m-b-5 right "  >
          <button id="btnCrtPop" type="button" class="btn btn-default " > <img src="/fms/images/icon_btn_regist.png" width="16" height="16" alt=""/>등록 </button>
          <button id="btnExcelDown" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_download.png" width="16" height="16" alt=""/> 다운로드 </button>
        </div>
      </h2>
						
	  <div class="list ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-striped table-list center " style="white-space: nowrap;">
							
			<thead>
				<tr>
					<th>구분</th>
					<th>품명</th>
					<th>규격</th>
					<th>제조사</th>
					<th>모델</th>
					<th>단위</th>
					<th>재고</th>
					<th>입고예정일</th>
				</tr>
			</thead>
			
			<tbody>
			<c:forEach var="list" items="${resultList}" varStatus="listIndex">				
				<!-- tr class="viewPopup ${listIndex.index % 2 == 0 ? '' : 'tr_back01' } popup-link" href="${ctxRoot}/pms/fcltMnt/sppExpdDtl.do?pdtNum=${list.pdtNum}&pddNum=${list.pddNum}" popup-data="width=690, height=220" -->
				<tr>
					<td class="dtlPopup" style="cursor:pointer" pdtNum='${list.pdtNum}' pddNum='${list.pddNum}' >																	
						<c:choose>
							<c:when test="${list.pddCde eq '0'}">
								 소모품
							</c:when>
							<c:when test="${list.pddCde eq '1'}">
								 예비품
							</c:when>
						</c:choose>
					</td>
					<td class="dtlPopup" style="cursor:pointer" pdtNum='${list.pdtNum}' pddNum='${list.pddNum}' >${list.pdtNam}</td>
					<td class="dtlPopup" style="cursor:pointer" pdtNum='${list.pdtNum}' pddNum='${list.pddNum}' >${list.pdtStd}</td>
					<td class="dtlPopup" style="cursor:pointer" pdtNum='${list.pdtNum}' pddNum='${list.pddNum}' >${list.pdtMnf}</td>
					<td class="dtlPopup" style="cursor:pointer" pdtNum='${list.pdtNum}' pddNum='${list.pddNum}' >${list.pdtMdl}</td>
					<td class="dtlPopup" style="cursor:pointer" pdtNum='${list.pdtNum}' pddNum='${list.pddNum}' >${list.pdtUnt}</td>
					<td class="dtlPopup line_right" style="cursor:pointer" pdtNum='${list.pdtNum}' pddNum='${list.pddNum}' >${list.pddCnt}</td>
					<td class="dtlPopup line_right" style="cursor:pointer" pdtNum='${list.pdtNum}' pddNum='${list.pddNum}' ></td>
				</tr>
			</c:forEach>

				<c:if test="${empty resultList}">
					<tr>
						<td colspan="7" style="color:black;text-align:center;">검색 결과가 없습니다.</td>
					</tr>
				</c:if>
			</tbody>
		</table>
	</div>

      <div style="width:100%; text-align:center">
        <ul class="pagination pagination-sm  " >
			<ui:pagination paginationInfo = "${paginationInfo}"  type="image" jsFunction="fn_search" />
        </ul>
      </div>
		
	</div>
	<!-- /box_right -->


	</div>
</form:form>
</div>


</body>

</html>
