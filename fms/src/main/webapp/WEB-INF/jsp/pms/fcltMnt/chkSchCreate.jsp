<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<script type="text/javascript">

$(document).ready(function() {
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();
		
	// 점검구분 콤보 공통코드
	gfn_setCombo("sclCde", "SCL_CDE", "" ,"--선택하세요--", function(){
	});
	
	 /*******************************************
	 * 이벤트 설정
	 *******************************************/	
					
	$("#btnSave").click(function(e) {
		
	    if(!gfn_formValid("frm"))	return;
		
		gfn_confirm("저장하시겠습니까?",function(ret){
			if(!ret) return;
    		
			gfn_saveFormCmm("frm", "insertChscMa", function(data){
				if(data.result){
					gfn_alert("데이터가 저장되었습니다.","",function(){
						self.close();
						opener.fn_search();
					});
				}else{
					gfn_alert("데이터 저장에 실패하였습니다. : " + data.error,"E");
				}
			});	
		});
	});
	 
	$("#btnClose").click(function(e) {
		self.close();
	});
});
</script>
	
	
<div id="wrap" class="wrap" > 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1 >점검일정 등록</h1>
  </div>
  <!-- // header --> 
	<form id="frm" name="frm" method="post" >
  
  
  <!-- container -->
  <div id="container"  class="content"  > 
    
    <!-- list -->
    <div class="regist-box2 ">
      <h2 ><span>점검일정 정보</span>
        <div class="btn_group m-b-5 right "  >         
          <button id="btnSave" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_save.png" width="16" height="16" alt=""/> 저장 </button>
          <button id="btnClose" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_delete.png" width="16" height="16" alt=""/> 닫기</button>
        </div>
      </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table table-view-regist " >
          <colgroup>
          <col width="170px">
          <col width="*">
          <col width="170px">
          <col width="*">
      
          </colgroup>
          <tbody>
				<tr>
					<th class="req">점검명 </th>
					<td colspan="3">
						<input type="text" name="titNam" id="titNam" value="" style="width:500px" class="reqVal"/>
					</td>
				</tr>
				<tr>
					<th  class="req">관리기관 </th>
					<td>
						<select name="mngCde" id="mngCde"  class="reqVal" style="width:157px">
							<option value="">--선택하세요--</option>
							<c:forEach var="cntList" items="${dataCodeList }">
								<option value="${cntList.codeCode }"${wttConsMa.cntCde == cntList.codeCode ? "selected" : "" }>${cntList.codeAlias}</option>
							</c:forEach>
						</select>
					</td>
					<th class="req">점검구분</th>
					<td>
	                    <select class="reqVal" style="width:157px"  name="sclCde" id="sclCde"   class="reqVal">
	                    </select>
					</td>
				</tr>
				<tr>
					<th class="req">점검시작일자</th>
					<td>
						<input type="text" class="datepicker" name="staYmd" id="staYmd" value="" class="reqVal" style="width:157px"/>
					</td>
					<th class="req">점검종료일자</th>
					<td>
						<input type="text" class="datepicker" name="endYmd" id="endYmd" value="" class="reqVal" style="width:157px"/>
					</td>
				</tr>
				<tr>
					<th>점검그룹(자) </th>
					<td colspan="3">
						<input type="text" name="ckmPeo" id="ckmPeo" value="" style="width:500px"/>
					</td>
				</tr>
				<tr>
					<th>점검내용 </th>
					<td colspan="3">
						<textarea name="chkCtnt" id="chkCtnt"   rows="5" cols="80" ></textarea>
					</td>
				</tr>
          </tbody>
        </table>
      </div>
    </div>
    <!-- //list --> 
    
  </div>
  
  <!-- //container --> 
</form>
  
</div>
<!-- //UI Object -->

</body>
</html>
