<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>


<script type="text/javascript">
var sMode  = "<c:out value='${eduVO.mode}'/>";
var sSeq   = "<c:out value='${eduVO.seq}'/>";
var sPageIndex = "<c:out value='${eduVO.pageIndex}'/>";
var sEduMngYn = "${sessionScope.isAdmYn}";


$(document).ready(function() {
	
	
	/*******************************************
	 * 화면초기화
	 *******************************************/

	 // 메시지 처리 
	var sResultMsg = '<c:if test="${!empty resultMsg}"><spring:message code="${resultMsg}" /></c:if>';
	if(sResultMsg.length != 0 ) {
		gfn_alert(sResultMsg);
	}
	
	gfn_init();

	

	// 구분 콤보 공통코드
	gfn_setCombo("faqCatCde", "FAQ_CAT_CDE", "${eduVO.faqCatCde}" ,"전체", function(){
		// 원인구분 콤보 공통코드
		gfn_setCombo("faqCuzCde", "FAQ_CUZ_CDE", "${eduVO.faqCuzCde}" ,"전체", function(){
		});
	});
	
	
	
	
	
	
	
	/*******************************************
	 * 이벤트설정
	 *******************************************/
	
	
	$("#btnAdd").hide();
	if(sEduMngYn == 'Y'){
		$("#btnAdd").show();		
	}
	
	if( sMode.length == 0 ){
		$('[name="mode"]').val("VIEW");
	} else {
		$('[name="mode"]').val(sMode);
	}
	if( sSeq.length == 0) {
		$('[name="seq"]').val("-1");
	} else {
		$('[name="seq"]').val(sSeq);
	}
	if( sPageIndex.length == 0  ) {
		$('[name="pageIndex"]').val("1");
	} else {
		$('[name="pageIndex"]').val(sPageIndex);
	}
	
	
	
	$('#btnExcelDown').click(function() {
		return;
		var html = '';
		html += '<form id="excelForm" method="post">';
		html += '	<input type="hidden" name="searchCondition1" value= "' + $('#searchCondition1').val() + '" />';
		html += '	<input type="hidden" name="faqCatCde" value= "' + $('#faqCatCde').val() + '" />';
		html += '	<input type="hidden" name="faqCuzCde" value= "' + $('#faqCuzCde').val() + '" />';
		html += '	<input type="hidden" name="ttl" value= "' + $('#ttl').val() + '" />';
		html += '</form>';
		$('#wrap').append(html);
		var url = '${ctxRoot}/wutl/excel/download.do';
		$('#excelForm').attr('action', url);
		$('#excelForm').submit();
	});
	
	
	//검색	
	$('#btnSearch').click(function () {
		fn_search();
    });
	//초기화	
	$('#btnReset').click(function () {
        var frm = $('form[name="eduVO"]');
        frm.find('input[type=text]').val('');
        frm.find('select').val('');
    });
	
});


// 재조회 
function fn_search(pageNo){
	if(gfn_isNull(pageNo)){
		pageNo = 1;
	}
    document.eduVO.pageIndex.value = pageNo;
    document.eduVO.action = "<c:url value='/pms/fcltMnt/eduFaqList.do'/>";
    document.eduVO.submit();
}

//신규등록모드로 상세페이지 호출
function fnEduFaqAdd() {
 // document.listForm.seq.value = -1;
 document.eduVO.mode.value = "ADD";
 document.eduVO.action = "<c:url value='/pms/fcltMnt/eduFaqUp.do'/>";
 document.eduVO.submit();	
}

// 자료정보 상세 뷰화면으로 이동 
function fnEduFaqInfo(seq){
	
    document.eduVO.seq.value = seq;
    document.eduVO.mode.value = "VIEW";
    document.eduVO.action = "<c:url value='/pms/fcltMnt/eduFaqInfo.do'/>";
    document.eduVO.submit();	
}

</script>

</head>
<body>

<div id="wrap" class="wrap" > 
  <div id="header" class="header"  >
    <h1 >FAQ</h1>
  </div>
  
	<form name="eduVO" method="post" >
	  <input type="hidden" name="pageIndex" value="1"/>
	  <input type="hidden" name="seq"  />
	  <input type="hidden" name="mode"  />
      <input type="submit" style="display: none;" />


  <!-- container -->
  <div id="container"  class="content"  > 
				
				
    <div class="seach_box" >
      <h2 >검색항목 </h2>
      <div>
        <table border="0" cellpadding="0" cellspacing="0" class="table table-search">
          <colgroup>
          <col width="114px">
          <col width="*">
          </colgroup>
          
			<tbody>
				<tr>
					<th>구분</th>
					<td>
                    <select style="width:140px"  name="faqCatCde" id="faqCatCde"   >
                    </select>
					</td>
				</tr>
				<tr>
					<th>지형지물</th>
					<td>
						<select name="searchCondition1" id="searchCondition1"  style="width:140px">
							<option value="">전체</option>
							<c:forEach var="cmtFtrcMaList" items="${cmtFtrcMaList}" varStatus="status">
								<option value="${cmtFtrcMaList.ftrCde }" ${param.searchCondition1 == cmtFtrcMaList.ftrCde ? "selected" : "" } >${cmtFtrcMaList.ftrNam}</option>
							</c:forEach>	
						</select>
					</td>
				</tr>
				<tr>
					<th>원인구분</th>
					<td>
                    <select style="width:140px"  name="faqCuzCde" id="faqCuzCde"   >
                    </select>
					</td>
				</tr>
				<tr>
					<th>제목 </th>
					<td>
						<input type="text" name="ttl" id="ttl"  style="width:140px" value="${eduVO.ttl}" />
					</td>
				</tr>

		</tbody>
	</table>
	
	
      <div class="btn_group m-t-10 "  >
        <button id="btnReset" type="button" class="btn btn-search "   style="width:114px"><img src="/fms/images/icon_btn_reset.png" width="16" height="16" alt=""/> 초기화 </button>
        <button id="btnSearch" type="button" class="btn btn-search "  style="width:132px"><img src="/fms/images/icon_btn_search.png" width="16" height="16" alt="" /> 검색 </button>
      </div>
      
	</div>
	</div>
	<!-- /box_left -->



    <div class="list_box">
      <h2 ><span style="float:left">목록 | FAQ(${paginationInfo.totalRecordCount}건)</span>
        <div class="btn_group m-b-5 right "  >
			<button id="btnCrt" type="button" class="btn btn-default " onclick="fnEduFaqAdd(); return false;"><img src="/fms/images/icon_btn_regist.png" width="16" height="16"alt="" />등록</button>
			<button id="btnExcelDown" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_download.png" width="16" height="16" alt=""/> 다운로드 </button>
        </div>
      </h2>
      
	  <div class="list ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-striped table-list center " style="white-space: nowrap;">
			<thead>
				<tr>
					<th>순번</th>
					<th>구분</th>
					<th>지형지물</th>
					<th>원인구분</th>
					<th>제목</th>
					<th>등록일자</th>
				</tr>
			</thead>
      
			<tbody>
				<c:forEach var="list" items="${resultList}" varStatus="listIndex">																	
					<tr onclick="javascript:fnEduFaqInfo('${list.seq}');">	
						<td  style="cursor:pointer" >${listIndex.index + 1}</td>
						<td  style="cursor:pointer" >${list.faqCatNm}</td>
						<td  style="cursor:pointer" >${list.ftrNam}</td>
						<td  style="cursor:pointer" >${list.faqCuzNm}</td>
						<td class="left" style="cursor:pointer" >${list.ttl}</td>
						<td  style="cursor:pointer" >${list.regDt}</td>
					</tr>
				</c:forEach>

				<c:if test="${empty resultList}">
					<tr>
						<td colspan="6" style="color:black;text-align:center;">검색 결과가 없습니다.</td>
					</tr>
				</c:if>
			</tbody>
		</table>
	</div>

		

	      
      <div style="width:100%; text-align:center">
        <ul class="pagination pagination-sm  " >
			<ui:pagination paginationInfo = "${paginationInfo}"  type="image" jsFunction="fn_search" />
        </ul>
      </div>
		
	</div>
	<!-- /box_right -->


	</div>
</form>
</div>


</body>

</html>
