<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>
    
<script type="text/javascript">
var listLink = '${ctxRoot}/pms/fcltMnt/fcltOpList.do';
	
$(document).ready(function() {
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();
	
	
	 /*******************************************
	 * 이벤트 설정
	 *******************************************/
	$("#check_layer, #edit_layer").hide();
	
	$('#search').click(function() {
		uptmMaList(1);
	});
	
	$('#btnExcelDown').click(function() {
		var html = '';
		html += '<form id="excelForm" method="post">';
		html += '	<input type="hidden" name="searchCondition1" value= "' + $('#searchCondition1').val() + '" />';
		html += '	<input type="hidden" name="searchCondition2" value= "' + $('#searchCondition2').val() + '" />';
		html += '	<input type="hidden" name="searchCondition3" value= "' + $('#searchCondition3').val() + '" />';
		html += '	<input type="hidden" name="searchCondition4" value= "' + $('#searchCondition4').val() + '" />';
		html += '</form>';
		$('#wrap').append(html);
		var url = '${ctxRoot}/uptm/excel/download.do';
		$('#excelForm').attr('action', url);
		$('#excelForm').submit();
	});
	
	$ ('#searchCondition5').datepicker();
    $ ('#searchCondition5').datepicker("option", "maxDate", $ ("#searchCondition6").val());
    $ ('#searchCondition5').datepicker("option", "onClose", function ( selectedDate ) {
        $ ("#searchCondition6").datepicker( "option", "minDate", selectedDate );
    });
 
    $ ('#searchCondition6').datepicker();
    $ ('#searchCondition6').datepicker("option", "minDate", $ ("#searchCondition5").val());
    $ ('#searchCondition6').datepicker("option", "onClose", function ( selectedDate ) {
        $ ("#searchCondition5").datepicker( "option", "maxDate", selectedDate );
    });	
	
	$ ('#searchCondition7').datepicker();
    $ ('#searchCondition7').datepicker("option", "maxDate", $ ("#searchCondition8").val());
    $ ('#searchCondition7').datepicker("option", "onClose", function ( selectedDate ) {
        $ ("#searchCondition8").datepicker( "option", "minDate", selectedDate );
    });
 
    $ ('#searchCondition8').datepicker();
    $ ('#searchCondition8').datepicker("option", "minDate", $ ("#searchCondition7").val());
    $ ('#searchCondition8').datepicker("option", "onClose", function ( selectedDate ) {
        $ ("#searchCondition7").datepicker( "option", "maxDate", selectedDate );
    });
				    
	$('#btnReset').click(function () {
           var frm = $('#uptmListFrm');
           frm.find('input[type=text]').val('');
           frm.find('select').val('');
        });
	
	$('#btnSearch').click(function () {
		fn_search(1);
	});
	
	$('.edit_mode2').click(function () {
		if ($(this).find('input').size() == 0) {
			var html = '';
			html += '<input type="text" class="datepicker" style="height:24px;" id="" name="durYmd" /> ';
			$(this).html(html);
			
			$('#saveBtn').show();
		}
		
	});
	
	$('#saveBtn').click(function() {
		if ($('#durYmd').val() == '') {
			alert('교체예정일을 입력하세요.');
			return false;
		}
		var url = '${ctxRoot}/uptm/pres/update.do';
		$('#uptmListFrm').attr('action', url);
		$('#uptmListFrm').submit();
	});
});

function fn_search(pageNo){
	if(gfn_isNull(pageNo)){
		pageNo = 1;
	}
   	$('form[name=uptmListFrm] input[name=pageIndex]').val(pageNo);
   	$('#uptmListFrm').attr('action', listLink);
	$('form[name=uptmListFrm]').submit();
}
</script>
	
	
		
<div id="wrap" class="wrap" > 
  <div id="header" class="header"  >
    <h1 >시설물 가동시간 관리</h1>
  </div>
	
<form:form  commandName="searchVO" id="uptmListFrm" name="uptmListFrm" method="post" action="${ctxRoot}/pms/fcltMnt/fcltOpList.do">
	<input type="hidden" name="pageIndex" value="1"/>
			
  <!-- container -->
  <div id="container"  class="content"  > 
				
				
    <div class="seach_box" >
      <h2 >검색항목 </h2>
      <div>
        <table border="0" cellpadding="0" cellspacing="0" class="table table-search">
          <colgroup>
          <col width="114px">
          <col width="*">
          </colgroup>
          
			<tbody>
				<tr>
					<th>지형지물</th>
					<td>
						<select name="searchCondition1" id="searchCondition1">
							<option value="">전체</option>
							<c:forEach var="cmtFtrcMaList" items="${cmtFtrcMaList}" varStatus="status">
								<option value="${cmtFtrcMaList.ftrCde }" ${param.searchCondition1 == cmtFtrcMaList.ftrCde ? "selected" : "" } >${cmtFtrcMaList.ftrNam}</option>
							</c:forEach>	
						</select>
					</td>
				</tr>
				<tr>
					<th>관리기관</th>
					<td>
						<select name="searchCondition2" id="searchCondition2">
							<option value="">전체</option>
							<c:forEach var="cdeList" items="${dataCodeList}" varStatus="status">
								<option value="${cdeList.codeCode }" ${param.searchCondition2 == cdeList.codeCode ? "selected" : "" } >${cdeList.codeAlias}</option>
							</c:forEach>	
						</select>
					</td>
				</tr>
				<tr>
					<th>행정동</th>
					<td>
						<select name="searchCondition3" id="searchCondition3">
							<option value="">전체</option>
							<c:forEach var="cmtList" items="${cmtAdarMaList}" varStatus="status">
								<option value="${cmtList.hjdCde }" ${param.searchCondition3 == cmtList.hjdCde ? "selected" : "" } >${cmtList.hjdNam}</option>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<th>상태</th>
					<td>
						<select name="searchCondition4" id="searchCondition4">
							<option value="">전체</option>
							<option value="1" ${param.searchCondition4 == 1 ? "selected" : "" }>가동중</option>
							<option value="2" ${param.searchCondition4 == 2 ? "selected" : "" }>가동중지</option>
						</select>
					</td>
				</tr>
				<tr>
					<th>가동시작일자 [이상]</th>
					<td>
						<!-- css쪽으로 설정 이동 할것 -->
						<input type="text" class="datepicker"  name="searchCondition5" id="searchCondition5" value="${param.searchCondition5}"/>
					</td>
				</tr>
				<tr>
					<th>가동시작일자 [이하]</th>
					<td>
						<input type="text" class="datepicker"  name="searchCondition6" id="searchCondition6" value="${param.searchCondition6}"/>
					</td>
				</tr>
				<tr>
					<th>가동종료일자 [이상]</th>
					<td>
						<!-- css쪽으로 설정 이동 할것 -->
						<input type="text" class="datepicker"  name="searchCondition7" id="searchCondition7" value="${param.searchCondition7}"/>
					</td>
				</tr>
				<tr>
					<th>가동종료일자 [이하]</th>
					<td>
						<input type="text" class="datepicker"  name="searchCondition8" id="searchCondition8" value="${param.searchCondition8}"/>
					</td>
				</tr>
				<tr>
					<th>-</th>
					<td>	
						<input type="text" id="" name="" readonly class="disable" />																					
					</td>
				</tr>
				<tr>
					<th>-</th>
					<td>	
						<input type="text" id="" name="" readonly class="disable" />																					
					</td>
				</tr>
				<tr>
					<th>-</th>
					<td>	
						<input type="text" id="" name="" readonly class="disable" />																					
					</td>
				</tr>
			</tbody>
		</table>

      <div class="btn_group m-t-10 "  >
        <button id="btnReset" class="btn btn-search "   style="width:114px"><img src="/fms/images/icon_btn_reset.png" width="16" height="16" alt=""/> 초기화 </button>
        <button id="btnSearch" class="btn btn-search "  style="width:132px"><img src="/fms/images/icon_btn_search.png" width="16" height="16" alt=""/> 검색 </button>
      </div>
      
	</div>
	</div>
	<!-- /box_left -->



    <div class="list_box">
      <h2 ><span style="float:left">목록 | 가동시간관리(${totCnt}건)</span>
        <div class="btn_group m-b-5 right "  >
          <button id="btnExcelDown" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_download.png" width="16" height="16" alt=""/> 다운로드 </button>
        </div>
      </h2>
						
	  <div class="list ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-striped table-list center " style="white-space: nowrap;">
				<thead>
					<tr>
						<th>지형지물</th>
						<th>관리기관</th>
						<th>행정동</th>
						<th>상태</th>
						<th>가동시작일자</th>
						<th>가동종료일자</th>
						<th>가동일</th>
						<th>교체예정일</th>
						<th>등급 &nbsp; &nbsp; </th>
					</tr>
				</thead>
				
				<tbody>
					<c:forEach var="list" items="${resultList}" varStatus="listIndex">																	
						<tr>	
							<td>
								<input type="hidden" id="ftrCde" name="ftrCde" value="${mapFtrcMa[list.ftrCde]}"/>
								<input type="hidden" id="ftrIdn" name="ftrIdn" value="${list.ftrIdn}" />
								<a href="javascript:;">${mapFtrcMa[list.ftrCde]}</a></td>
							<td><a href="javascript:;">${mapMngCde[list.mngCde]}</a></td>
							<td><a href="javascript:;">${mapAdarMa[list.hjdCde]}</a></td>
							<td>
								<a href="javascript:;">
									<c:if test="${list.utpSta == 1}">가동중</c:if>
									<c:if test="${list.utpSta == 2}">가동중지</c:if>
								</a>
							</td>
							<td><a href="javascript:;">${list.utsYmd}</a></td>
							<td><a href="javascript:;">${list.upeYmd}</a></td>
							<td><a href="javascript:;">${list.difDay} 일 </a></td>
							<td><a href="javascript:;">${list.durYmd}</a></td>
							<td><a href="javascript:;">${list.curSta}</a></td>
						</tr>
					</c:forEach>
	
					<c:if test="${empty resultList}">
						<tr>
							<td colspan="10" style="color:black;text-align:center;">검색 결과가 없습니다.</td>
						</tr>
					</c:if>
				</tbody>
			</table>
		</div>

      <div style="width:100%; text-align:center">
        <ul class="pagination pagination-sm  " >
			<ui:pagination paginationInfo = "${paginationInfo}"  type="image" jsFunction="fn_search" />
        </ul>
      </div>
		
	</div>
	<!-- /box_right -->


	</div>
</form:form>
</div>


</body>

</html>
