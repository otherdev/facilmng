<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<script type="text/javascript">
var saveLink = '${ctxRoot}/wutl/wutlHtSave.do';
var deleteLink = '${ctxRoot}/wutl/deleteWutlHt.do?ftrCde=${wttWutlHt.ftrCde}&ftrIdn=${wttWutlHt.ftrIdn}&repNum=${wttWutlHt.repNum}&g2Id=${wttWutlHt.g2Id}';

$(document).ready(function () {
	
    var now = new Date;
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
    if('${mode}'=='add') {
        $('#searchCondition2, #searchCondition8').val(today);
    }

	
    var msg = '${msg}';
	
	if(msg) {
		alert(msg);
		try { opener.selfRefresh(); }catch(e){}
		location.href="${ctxRoot}/wutl/view/popup.do?repNum=${param.reqNum}&g2Id=${param.g2Id}&mode=edit";
	}
	
	$("#btnDel").click(function(e) {
		if(!confirm("삭제하시겠습니까?"))	rerturn;
		
		var ftrCde = '${wttWutlHt.ftrCde}';
		var ftrIdn = '${wttWutlHt.ftrIdn}';
		var repNum = '${wttWutlHt.repNum}';
		var g2Id   = '${wttWutlHt.g2Id}';
		
		gfn_confirm("삭제하시겠습니까?", function(ret){
	    	if(!ret)   return;
	         
			gfn_saveCmm({ sqlId:"deleteWttConsMa"
				       	 ,data:{ ftrCde: ftrCde
				       		    ,ftrIdn: ftrIdn
				       		    ,repNum: repNum
				       		    ,g2Id: g2Id }}, function(data){
				if (data.result) {
				   gfn_alert("삭제 되었습니다.","",function(){
				      opener.fn_search();
				      window.close();
				   });
				}      
				else{
				   gfn_alert("저장에 실패하였습니다.","E");
				   return;
				}
			});
	    });
		
		//location.href='${ctxRoot}/wutl/deleteWutlHt.do?ftrCde=${wttWutlHt.ftrCde}&ftrIdn=${wttWutlHt.ftrIdn}&repNum=${wttWutlHt.repNum}&g2Id=${wttWutlHt.g2Id}';
	});
	
	$("#btnSave").click(function(e) {
		if ($('#searchCondition11').val() == '') {
			gfn_alert('관리번호를 입력하세요.','',function(){
				$('#searchCondition11').focus();
			});
			return false;
		}
		if ($('#searchCondition2').val() == '') {
			alert('유지보수일을 등록하세요.','',function(){
				$('#searchCondition2').focus();
			});
			return false;
		}
		if ($('#searchCondition3').val() == '') {
			alert('시공자를 입력하세요.','',function(){
				$('#searchCondition3').focus();
			});
			return false;
		}
		if ($('#searchCondition6').val() == '') {
			alert('유지보수 내용을 입력하세요.','',function(){
				$('#searchCondition6').focus();
			});
			return false;
		}
		
		e.preventDefault();
		
		var html = '';		
		var z = 0;
		$('#sub_table>tr.viewPopup').each(function (i) {
			if($('#statVal_'+i).val() == "New"){				
				html += '<input type="hidden" name="wttHtList[' + z + '].pdtNum" value= "' + $('#pdtNum_'+i).val() + '" />';
				html += '<input type="hidden" name="wttHtList[' + z + '].iotYmd" value= "' + $('#iotYmd_'+i).val() + '" />';
				html += '<input type="hidden" name="wttHtList[' + z + '].pdhCnt" value= "' + $('#pdhCnt_'+i).val() + '" />';
				html += '<input type="hidden" name="wttHtList[' + z + '].pddNum" value= "' + $('#pddNum_'+i).val() + '" />';
				z++;
			}			
		});
		
		$('#frmdiv').html("");
		$('#frmdiv').append(html);		
		
		gfn_confirm("저장하시겠습니까?", function(ret){		    	
			if(!ret) return;
			$('#wutlHtSaveFrm').attr('action', saveLink);		
			$('#wutlHtSaveFrm').submit();
		});
	});
	
	$('#searchCondition7').change(function() {
		$('#searchCondition7 option:selected').each(function () {
			$.ajax({
				url : '${ctxRoot}/pdjt/pdjtCnt.do',
				type : 'get',
			 	contentType: "application/json",
				dataType : 'json',
				data : {pdtNum:$(this).val(), pddNum:$("#searchCondition7 option:selected").attr("pddNum")},
				success : function(res) {
					$('#searchCondition9').val(res.data.pddCnt);
				},
				error: function(request, status, error) {
					console.log("호출에 실패하였습니다.");
				}
			});
		});
	});
	
	/* 추가하기 */
	$('#btnGrdAdd').click(function() {
		if($.trim( $("select[name=searchCondition7]").val() ) == "") {
			gfn_alert("소모품을 선택하세요.","",function(){
				$("select[name=searchCondition7]").focus();	
			});			
			return;
		}
		if($.trim( $("input[name=searchCondition8]").val() ) == "") {
			gfn_alert("사용일자를 입력하세요.","",function(){
				$("input[name=searchCondition8]").focus();
			});						
			return;
		}
		if($.trim( $("input[name=searchCondition10]").val() ) == "") {
			gfn_alert("사용수량을 입력하세요.","",function(){
				$("input[name=searchCondition10]").focus();
			});
			return;
		}
		var storeCnt = parseInt( $.trim( $("input[name=searchCondition9]").val()) );
		var useCnt = parseInt( $.trim( $("input[name=searchCondition10]").val()) );
		if(storeCnt < useCnt) {
			if(storeCnt == 0) {
				gfn_alert("현재 재고가 없습니다.","",function(){
					$("input[name=searchCondition10]").val("");
					$("input[name=searchCondition10]").focus();
				});
			}else {
				gfn_alert("사용수량이 현재 재고를 초과합니다.\n\n" + storeCnt + " 이하로 입력하세요.","",function(){
					$("input[name=searchCondition10]").val("");
					$("input[name=searchCondition10]").focus();
				});
			}			
			return;
		}
		
		$('.no-rows').hide();
		var rowI = $('input[name=pdjt_ht_check]').size();
		var html = '';
		html += '<tr class="viewPopup">';
		html += '<td><input type="checkbox" name="pdjt_ht_check" id="pdjt_ht_check_'+rowI+'" /></td>';
		html += '<td class="left">' + $('#searchCondition7 option:selected').text() + '</td>';
		html += '<td><span class="iotYmd">' + $('#searchCondition8').val() + '</span></td>';
		html += '<td class="right"><span class="pdhCnt">' + $('#searchCondition10').val() + '</span>';		
		html += '<input type="hidden" name="statVal" id="statVal_'+rowI+'" value="New" />';
		html += '<input type="hidden" name="pdtNum" id="pdtNum_'+rowI+'" value="'+$('#searchCondition7').val()+'" />';
		html += '<input type="hidden" name="pdhCnt" id="pdhCnt_'+rowI+'" value="'+$('#searchCondition10').val()+'" />';
		html += '<input type="hidden" name="iotYmd" id="iotYmd_'+rowI+'" value="'+$('#searchCondition8').val()+'" />';
		html += '<input type="hidden" name="pddNum" id="pddNum_'+rowI+'" value="'+$("#searchCondition7 option:selected").attr("pddNum")+'" />';
		html += '</td></tr>';
				
		
	    $('#sub_table:last').append(html);

	    /* 재고 차감 */
		var newCnt = storeCnt - useCnt;
		$("input[name=searchCondition9]").val(newCnt);
		/* 날짜 초기화 */
		$("input[name=searchCondition8]").val("");
		/* 수량 초기화 */
		$("input[name=searchCondition10]").val("");
	});
	
	$('#btnGrdDel').click(function() {
		rowCheDel();
	});
	
	function rowCheDel(){
		var $obj = $("input[name='pdjt_ht_check']");
 		var checkCount = $obj.size();
 		
 		if (checkCount == 0) { 			
			gfn_alert('삭제할 정보가 없습니다.');
 			return false;
 		}
 		
		var checkedCnt = 0;
		checkedCnt = checkedCnt + $('#sub_table .pdjt_ht_check:checked').length;
		
		if (checkedCnt>0) {			
			gfn_confirm("선택된 항목들을 삭제하시겠습니까?", function(ret){			    	
				if(!ret) return;
								
				var html = '<form id="deletePdjtHtFrm" method="post"><input type="hidden" id="repNum" name="repNum" value="${param.repNum}"/><input type="hidden" id="g2Id" name="g2Id" value="${param.g2Id}"/>';
					$('#sub_table .pdjt_ht_check:checked').each(function (i) {
						html += '<input type="hidden" name="wttPdjtHtVOList[' + i + '].repNum" value="${param.reqNum}"/>';
						html += '<input type="hidden" name="wttPdjtHtVOList[' + i + '].g2Id" value="${param.g2Id}"/>';
						html += '<input type="hidden" name="wttPdjtHtVOList[' + i + '].pdhNum" value="' + $(this).attr("pdhNum") + '"/>';
					});
					html += '</form>';
					$('#wrap').append(html);
				var pdjtHtdel = "${ctxRoot}/wutl/deletePdjtHt.do";
				/* 데이터 삭제시 */
				if( $('#sub_table .pdjt_ht_check:checked').length > 0) {
					$('#deletePdjtHtFrm').attr('action', pdjtHtdel);					
					$('#deletePdjtHtFrm').submit();
				}		
			});
		}
		
		/* for (var i=0; i<checkCount; i++){
	  		if($obj.eq(i).is(":checked")){
	  			$obj.eq(i).parent().parent().remove();
	  		}
 		}  */
 				
	}
});

</script>
<body>

<div id="wrap" class="wrap" > 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1 >유지보수대장  상세정보 </h1>
  </div>
  <!-- // header --> 

	<form name="wutlHtSaveFrm" id="wutlHtSaveFrm" method="post">
		<div id="frmdiv"></div>
		<input type="hidden" id="repNum" name="repNum" value="${wttWutlHt.repNum}" />
		<input type="hidden" id="g2Id" name="g2Id" value="${wttWutlHt.g2Id}" />
		<input type="hidden" id="searchCondition1" name="searchCondition1" value="${wttWutlHt.ftrCde}" />
	    <c:set var="idnchk" value="${ftrIdn}" />
		<c:choose>
		    <c:when test="${idnchk eq ''}">
		       <input type="hidden" id="ftrIdn" name="ftrIdn" onchange="getNumber(this);" onkeyup="getNumber(this);" maxlength="10" value="${wttWutlHt.ftrIdn }" />
		    </c:when>
		    <c:when test="${empty idnchk}">
		       <input type="hidden" id="ftrIdn" name="ftrIdn" onchange="getNumber(this);" onkeyup="getNumber(this);" maxlength="10" value="${wttWutlHt.ftrIdn  }" />
		    </c:when>
		    <c:otherwise>
		       <input type="hidden" id="ftrIdn" name="ftrIdn" onchange="getNumber(this);" onkeyup="getNumber(this);" maxlength="10" value="${ftrIdn }" />
		    </c:otherwise>
		</c:choose>




  <!-- container -->
  <div id="container"  class="content"  > 
    
    <!-- list -->
    <div class="regist-box ">
      <h2 ><span>일반정보</span>
        <div class="btn_group m-b-5 right "  >
         
          <button id="btnDel" type="button" class="btn btn-default " > <img src="/fms/images/icon_btn_delete.png" width="16" height="16" alt=""/> 삭제 </button>
          <button id="btnSave" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_save.png" width="16" height="16" alt=""/> 저장 </button>
        </div>
      </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table table-view-regist " >
          <colgroup>
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
      
          </colgroup>
          <tbody>
			<tr>
				<th>지형지물</th>
				<td>
					<select id="selectFtrCde" name="selectFtrCde" readonly class="disable not-used" >
						<c:forEach var="cmtFtrcMaList" items="${cmtFtrcMaList}" varStatus="status">
							<option value="${cmtFtrcMaList.ftrCde }" ${wttWutlHt.ftrCde == cmtFtrcMaList.ftrCde ? "selected" : "" } >${cmtFtrcMaList.ftrNam}</option>
						</c:forEach>
					</select>
				</td>
				<th>관리번호</th>
				<td>
				    <c:set var="idnchk" value="${ftrIdn}" />
					<c:choose>
					    <c:when test="${idnchk eq ''}">
					       <input type="text" id="searchCondition11" name="searchCondition11" onchange="getNumber(this);" onkeyup="getNumber(this);" maxlength="10" value="${wttWutlHt.ftrIdn }" readonly class="disable"/>
					    </c:when>
					    <c:when test="${empty idnchk}">
					       <input type="text" id="searchCondition11" name="searchCondition11" onchange="getNumber(this);" onkeyup="getNumber(this);" maxlength="10" value="${wttWutlHt.ftrIdn  }" readonly class="disable"/>
					    </c:when>
					    <c:otherwise>
					       <input type="text" id="searchCondition11" name="searchCondition11" onchange="getNumber(this);" onkeyup="getNumber(this);" maxlength="10" value="${ftrIdn }" readonly class="disable"/>
					    </c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr>
				<th>시공자</th>
				<td>
					<input type="text" name="searchCondition3" id="searchCondition3" value="${wttWutlHt.oprNam }" />
				</td>
				<th>유지보수일</th>
				<td>
					<input type="text" class="datepicker" id="searchCondition2" name="searchCondition2" value="${wttWutlHt.repYmd }" />
				</td>
			</tr>
			<tr>
				<th>유지보수 사유</th>
				<td>
					<select name="searchCondition5" id="searchCondition5">
						<c:forEach var="sbjCdeList" items="${sbjCdeList}" varStatus="status">
							<option value="${sbjCdeList.codeCode }" ${wttWutlHt.sbjCde == sbjCdeList.codeCode ? "selected" : "" } >${sbjCdeList.codeAlias}</option>
						</c:forEach>
					</select>
				</td>
				<th>유지보수 구분</th>
				<td>
					<select name="searchCondition4" id="searchCondition4">
						<c:forEach var="repCdeList" items="${repCdeList}" varStatus="status">
							<option value="${repCdeList.codeCode }" ${wttWutlHt.repCde == repCdeList.codeCode ? "selected" : "" } >${repCdeList.codeAlias}</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<th>유지보수 내용</th>
				<td colspan="3">
					<input type="text" name="searchCondition6" id="searchCondition6" value="${wttWutlHt.repDes }" />
				</td>
			</tr>
          </tbody>
        </table>
      </div>
    </div>
    <!-- //list --> 

    
    <!-- list -->
    <div class="regist-box m-t-10">
      <h2 class="m-b-5">소모품/예비품 정보</h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table table-view-regist " >
          <colgroup>
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">

          </colgroup>
          <tbody>
						<tr>
							<th>소모품</th>
							<td>
								<select name="searchCondition7" id="searchCondition7">
									<option value="">선택안함</option>
									<c:forEach var="list" items="${pdjtMaList}" varStatus="status">
										<option value="${list.pdtNum }" pddNum="${list.pddNum }">${list.pdtNam}</option>
									</c:forEach>
								</select>
							</td>
							<th>사용일자</th>
							<td>
								<input type="text" class="datepicker" id="searchCondition8" name="searchCondition8" />
							</td>
						</tr>
						<tr>
							<th>현재 재고</th>
							<td>
								<input type="text" class="td_right" id="searchCondition9" name="searchCondition9" value="" readonly="readonly" />
							</td>
							<th>사용개수</th>
							<td>
								<input type="text" class="td_right" id="searchCondition10" name="searchCondition10" onkeyup="onlyNumber(this);" maxlength="6"/>
							</td>
						</tr>
          </tbody>
        </table>
      </div>
    </div>
    <!-- //list --> 
    
    <!-- tab -->
    <div class="regist-box ">
      <div class="btn_group m-b-5 m-r-5 m-t-5 right " >
          <button id="btnGrdAdd" type="button" class="btn btn-default " > <img src="/fms/images/icon_btn_add.png" width="16" height="16" alt=""/> 추가</button>
          <button id="btnGrdDel" type="button" class="btn btn-default " > <img src="/fms/images/icon_btn_delete.png" width="16" height="16" alt=""/> 삭제 </button>
       </div>
      <!--tab list-->

    <div class="list-regist">
     
        <table border="0" cellpadding="0" cellspacing="0"  class="table table-list-regist  " style="white-space: nowrap; ">
          <colgroup>
          <col width="60px">
           <col width="*">
          <col width="120px">
           <col width="120px">
          
          </colgroup>
          <thead>
            <tr>
              <th>선택</th>
              <th>소모품</th>
              <th>사용일자</th>
              <th>사용개수(량)</th>
            </tr>
          </thead>
					<tbody id="sub_table" class="td_add_pdjt">
						<c:if test="${fn:length(pdjtList) == 0}">
							<tr class="no-rows"><td colspan="4"> 조회내역이 없습니다. </td></tr>
						</c:if>
						<c:forEach var="list" items="${pdjtList}" varStatus="listIndex">
							<tr class="viewPopup">
								<td>
									<input type="checkbox" class="pdjt_ht_check" name="pdjt_ht_check" pdhNum="${list.pdhNum }" value="${list.pdhNum }"/>
								</td>
								<td class="left">${list.pdtNam}</td>
								<td>${list.iotYmd}</td>
								<td class="right">${list.pdhCnt} 
									<input type="hidden" name="statVal" id="statVal_${listIndex.index}" value="">
									<input type="hidden" name="pdtNum" id="pdtNum_${listIndex.index}" value="${list.pdtNum}">									
									<input type="hidden" name="iotYmd" id="iotYmd_${listIndex.index}" value="${list.iotYmd}">									
									<input type="hidden" name="pdhCnt" id="pdhCnt_${listIndex.index}" value="${list.pdhCnt}">									
									<input type="hidden" name="pddNum" id="pddNum_${listIndex.index}" value="${list.pddNum}">									
								</td>
							</tr>
						</c:forEach>
					</tbody>

        </table>
   
     </div>
    </div>
    
    <!-- //tab --> 
    
  </div>
  
  <!-- //container --> 
</form>

  
</div>
<!-- //UI Object -->

</body>
</html>

