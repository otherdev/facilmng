<%--
  Class Name : usrUpdt.jsp
  Description : 사용자조회, 수정 JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>사용자 상세 및 수정</title><meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<!-- <meta http-equiv="Content-Language" content="ko" > -->
<meta charset="utf-8">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
 
<!-- jstl태그-->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<!-- 스타일-->
<link href="<c:url value='/css/style_popup.css' />" rel="stylesheet" type="text/css">
<link href="<c:url value='/fonts/font-awesome/css/font-awesome.min.css' />" rel="stylesheet" />
<link href="<c:url value='/css/date-style.css' />" rel="stylesheet" type="text/css">
<link href="<c:url value='/css/jquery-ui.css' />" rel="stylesheet" type="text/css">
<link href="<c:url value='/css/AXJ.min.css' />" rel="stylesheet" type="text/css">


<!-- 스크립트-->
<script type="text/javascript" src="<c:url value='/js/lib/jquery-1.12.4.js' />" ></script>
<script type="text/javascript" src="<c:url value='/js/lib/jquery-ui.js' />" ></script>
<script type="text/javascript" src="<c:url value='/js/lib/jquery.number.js' />" ></script>
<script type="text/javascript" src="<c:url value='/js/lib/AXJ.js' />" ></script>
<script type="text/javascript" src="<c:url value='/js/lib/AXJ.min.js' />"></script>
<script type="text/javascript" src="<c:url value='/js/lib/AXGrid.js' />" ></script>
<script type="text/javascript" src="<c:url value='/js/lib/AXUtil.js' />" ></script> 

<script type="text/javascript" src="<c:url value="/validator.do"/>"></script>
<script type="text/javascript" src="<c:url value='/js/pms/cmm.js' />" ></script> 
<script type="text/javascript" src="<c:url value='/js/pms/pms.js' />" ></script> 
<script type="text/javaScript" language="javascript" defer="defer">
<!--
var chk = false; //중복확인


$(document).ready(function() {
	
	gfn_init();
	
	// 버튼권한처리
	setBtnAuth();
	
});


//저장
function fnUpdate(){
    if(!gfn_formValid("frm"))	return;
	var regEng = /^[A-Za-z0-9_]*$/;//영문숫자 정규식
	var regNum = /^[0-9]*$/;//숫자 정규식

	if(!regEng.test($("#userId").val())){
		gfn_alert("ID는 영문,숫자로 입력하세요.");
		$("#userId").focus();
		return;
	}
	
	if(!regNum.test($("#mbtlnum").val())){
		gfn_alert("전화번호는 숫자만 입력가능합니다.");
		$("#mbtlnum").focus();
		return;
	}
	if( $("#CHG_PWD").val() == 'Y' ) {
		if(gfn_isNull($("#password").val() ) ){
			gfn_alert("비밀번호을 입력하세요."); 
			$("#password").focus();
			return;
		} else if(gfn_isNull($("#password2").val() ) ){
			gfn_alert("비밀번호2을  입력하세요."); 
			$("#password2").focus();
			return;
		} else if($("#password").val() != $("#password2").val()){
			gfn_alert("비밀번호 확인값이 다릅니다."); 
			$("#password2").focus();
			return;
		}
	}
	if(!chk && "${mode}" == "ADD"){
		gfn_alert("중복확인을 확인하세요.");
		return;
	}

	gfn_confirm("저장하시겠습니까?",function(result){
		if(result){
			var sChgPwd = $("#CHG_PWD").val();
			var sUserId = $("#userId").val();
			var sUserNm = $("#userNm").val();
			
			var sTel = $("#mbtlnum").val();
			var sPassword = $("#password").val();
			var sMode = $("#mode").val();
				
			var objData = new Object();
			objData.CHG_PWD 	= sChgPwd;
			objData.userId 		= sUserId;
			objData.userNm 		= sUserNm;
			objData.userTel 	= sTel;
			objData.userPwd 	= sPassword;
			objData.mode 		= sMode;
			
			//저장처리
			gfn_saveList("/fms/pms/user/usrInfoChange.do" ,{ data : objData }, function(data){
				if (data.result) {
					self.close();
				} else{
					gfn_alert("저장에 실패하였습니다.","E");
					return;
				}
			});
		}
	});
}


/// 중복체크
function doCheck() {
	
	if(gfn_isNull($("#userId").val())){
		gfn_alert("먼저 ID를 입력하세요.");
		$("#userId").focus();
		return;
	}
	
	var regEng = /^[A-Za-z0-9_]*$/;//영문숫자 정규식
	
	if(!regEng.test($("#userId").val())){
		gfn_alert("ID는 영문,숫자로 입력하세요.");
		$("#userId").focus();
		return;
	}
	
	gfn_selectList({
		sqlId : "selectUserDtl",
		userId:$("#userId").val()}
	
	, function(data){
		if(data.data.length > 0){
			gfn_alert("중복된 ID입니다.");
			chk = false;
			$("#btnChk").attr("disabled",false);
			$("#userId").removeAttr("readonly");				
			return;
		}else{
			gfn_alert("사용가능한 ID입니다.");
			chk = true;
			$("#btnChk").attr("disabled",true);
			$("#userId").attr("readonly","readonly");				
		}
	} );		
	
}


/// 버튼및권한처리
var setBtnAuth = function(){
	
	switch("${mode}"){
	case "ADD" :
		$("#userId").attr("readonly",false);				
		$("#btnChk").attr("disabled",false);
		$("#password").attr("disabled",false);
		$("#password2").attr("disabled",false);
		$("#btnPwd").hide();			
		break;
		
	case "MOD" :		
		$("#userId").attr("readonly",true);				
		$("#btnChk").attr("disabled",true);
		$("#password").attr("disabled",true);				
		$("#password2").attr("disabled",true);
		if("${isAdmYn}" == "Y" || "${sessionScope.id}" == "${usrDtl.userId}"){
			$("#btnPwd").show();
		}
		break;
		
	default :	
		break;
	};
	
	
};


function fnRefresh(){
    document.frm.action = "<c:url value='/pms/adm/usrInfo.do'/>";
    document.frm.submit();
}


//비밀번호변경모드
var doPwd = function(){
	$("#btnPwd").hide();
	$("#CHG_PWD").val("Y");
	$("#password").attr("disabled",false); 
	$("#password2").attr("disabled",false);
};
// 강제 Submit 막기 
var fnNoSubmit = function(e){
	e.preventDefault();
	e.stopPropagation();
	/* do something with Error */
};
$("form[name='frm']").bind("submit",fnNoSubmit);
//-->
</script>

</head>
<body>
<noscript>자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>    

<!-- UI Object -->
<div id="wrap"> 
  
 
  
<h1><i class="fa fa-chevron-right  "></i> 사용자정보</h1>
<div class="conts"  >
          
          
 	<form id="frm" name="frm" onkeydown="if(event.keyCode==13) return false;" >
		<input type="hidden" id="mode" name="mode" value="${mode}"/>
		<input type="hidden" id="CHG_PWD" name="CHG_PWD" value="N"/>
           
    	<!-- write -->
          
          <div class="table-responsive">
            <table class="table table-write">
              <caption>사용자정보
              </caption>
              <colgroup>
              <col width="30%">
              <col width="70%">
              </colgroup>
              <tbody>
                <tr>
                  <th class="req">아이디</th>
                  <td><input type="text" class="form-control reqVal" style="width:67%"  name="userId"  id="userId" value="${usrDtl.userId}">
                  		<button type="button" id="btnChk" class="AXButton" style="width:20%;" onclick="doCheck();"  ><i class="fa fa-lg"></i> 중복확인</button>
                  </td>
                </tr>
                <tr>
                  <th class="req">이름</th>
                  <td><input type="text" class="form-control reqVal" style="width:98.5%"  id="userNm" name="userNm" value="${usrDtl.userNm}" ></td>
                </tr>
                <tr>
                  <th>연락처</th>
                  <td><input type="text" class="form-control" style="width:98.5%"  name="mbtlnum" id="mbtlnum" value="${usrDtl.userTel}" ></td>
                </tr>
                <tr>
                  <th class="req">비밀번호</th>
                  <td><input type="password" class="form-control" name="password" id="password" value="${usrDtl.password}" style="width:98.5%"  ></td>
                </tr>
                <tr>
                  <th>비밀번호 확인</th>
                  <td><input type="password" class="form-control" id="password2" value="${usrDtl.password}" style="width:98.5%"  ></td>
                </tr>
              </tbody>
            </table>
          </div>
    	<div class="btn_group center m-t-15 "  >
	      <button id="btnSave" class="btn btn-default"  onclick="JavaScript:fnUpdate(); return false;"> <i class="fa fa-check" aria-hidden="true"></i> 저장</button>
	      <button id="btnPwd"  class="btn btn-default"  onclick="JavaScript:doPwd(); return false;"> <i class="fa fa-check" aria-hidden="true"></i> 비밀번호변경</button>
	      <button id="button"  class="btn btn-default " onclick="JavaScript:self.close();"> <i class="fa fa-close" aria-hidden="true"></i> 닫기</button>
        </div>          
          <!-- //write --> 
     </form>
  </div>
  <!--conts--> 
   </div>
</body>
</html>

