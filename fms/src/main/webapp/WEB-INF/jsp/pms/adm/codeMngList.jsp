<%--
  Class Name : codeMngList.jsp
  Description : 코드관리(조회,삭제) JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀 
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>코드관리</title>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>


<script type="text/javascript" src="<c:url value='/js/pms/adm/codeMngList.js' />" ></script>
<script type="text/javaScript" language="javascript" defer="defer">
<!--
$(document).ready(function() {

	fnSearch();	

});




//코드마스터 조회
function fnSearch(){
	/* 	
	    document.listForm.pageIndex.value = 1;
	    document.listForm.action = "<c:url value='/pms/adm/codeMngList.do'/>";
	    document.listForm.submit();
	 */

	 
	 // 코드마스터 리스트
	gfn_selectList({sqlId: 'selectMstCdList', }, function(ret){
		
		lst = ret.data;
		try{
			_mstCd = lst[0].mstCd;
		}catch(e){}
		// 그리드 초기화
		fnObj.pageStart();
	});
	 
	// 상세코드리스트
	gfn_selectList({sqlId: 'selectDtlCdList', mstCd:_mstCd}, function(ret){
		lst2 = ret.data;
		
		// 그리드 초기화
		fnObj2.pageStart();	
	});	
	
 }


//코드상세 조회
function fnSearch2(mstCd){
	// 상세코드리스트
	gfn_selectList({sqlId: 'selectDtlCdList', mstCd:mstCd}, function(ret){
		lst2 = ret.data;
		
		// 그리드 초기화
		fnObj2.pageStart();	
	});	
}

//코드마스터 저장 
function fnSave(){
	myGrid.editCellClear(0,0,0);
	myGrid.selectClear();
	
	if(!myGrid.validateCheck('C') || !myGrid.validateCheck('U') ){
		return false;
	}	 
	
	// ajax 저장 - 공정, 팀원
	var _lst = fnObj.grid.getList();
	var ll = fnObj.grid.getSelectedItem();
	
	gfn_confirm("저장하시겠습니까?",function(ret){
		if(!ret)	return
		
		//저장처리
		gfn_saveList(
				"/fms/pms/adm/saveMstCdList.do"
				,{lst: _lst, sqlId: 'saveMstCd',}
				, function(data){
					if (data.result) {
						gfn_alert("성공적으로 저장되었습니다.");
						fnSearch();
						return;
					}
					else{
						gfn_alert("저장에 실패하였습니다 : " + data.error, "E");
						return;
					}
		});	
	});


}

//코드t상세 저장 
function fnSave2(mstCd){
	myGrid2.editCellClear(0,0,0);
	myGrid2.selectClear();

	if(!myGrid2.validateCheck('U') || !myGrid2.validateCheck('C')){
		return false;
	}
	
	// ajax 저장 
	var _lst = fnObj2.grid.getList();
	var ll = fnObj2.grid.getSelectedItem();	
	
	gfn_confirm("저장하시겠습니까?", function(ret){
		if(!ret) return;
		
		//저장처리
		gfn_saveList(
				"/fms/pms/adm/saveDtlCdList.do"
				,{lst: _lst, sqlId: 'saveDtlCd',}
				, function(data){
					if (data.result) {
						gfn_alert("성공적으로 저장되었습니다.");
						fnSearch2(_mstCd);
						return;
					}
					else{
						gfn_alert("저장에 실패하였습니다.","E");
						return;
					}
		});	
	});


}

'<c:if test="${!empty resultMsg}">alert("<spring:message code="${resultMsg}" />");</c:if>'
//-->
</script>

</head>
<body>
	<noscript class="noScriptTitle">자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>
	<form name="listForm" method="post" onkeydown="return gfn_fireKey(event);"  >

    <div class="contents-box"  > 

	<!-- 현재위치 네비게이션 시작 -->
	<div class="conts"  >
          <h1><i class="fa fa-chevron-circle-right  "></i> 코드관리</h1>
         
         
          
        <!-- list -->
       
        <div style="width:40%;  margin-right:3% ; display:inline-block; ">
         <h2>상위코드 <span class="f_r"></span></h2>
         
			<div style="padding:10px;" >
	   			<input type="button" class="btn btn-default " onclick="fnObj.grid.append();" value="추가">
	   			<input type="button" class="btn btn-default " onclick="fnObj.grid.remove();" value="삭제">
	   			<input type="button" class="btn btn-default " onclick="fnSave();" style="float:right;"  value="저장"/>
			</div>					  	
			<div id="grdMstCd" style="border-top:2px solid #005ba6; height:300px;"></div>
        
        </div>
        
            
          
          
    
      <!-- //list --> 
        <!-- list -->
    
      
       
        <div style="width:57%;  display:inline-block; float:right">
         <h2>하위코드 <span class="f_r"> </span></h2>
            <div style="padding:10px;" >
	   			<input type="button" class="btn btn-default " onclick="fnObj2.grid.append();" value="추가">
	   			<input type="button" class="btn btn-default " onclick="fnObj2.grid.remove();" value="삭제">
	   			<input type="button" class="btn btn-default " onclick="fnSave2();" style="float:right;"  value="저장"/>
 			</div>					  	
		  	<div id="grdDtlCd" style="border-top:2px solid #005ba6; height:300px;"></div>
         
        </div>
        
            
          
          
      <!-- //list --> 
      
      
      
          
    </div>
	<!-- //content 끝 -->
    </div>
    
	</form>
	        
	<!-- footer 시작 
	<div id="footer"><c:import url="/EgovPageLink.do?link=main/inc/EgovIncFooter" /></div>
	-->
</body>
</html>