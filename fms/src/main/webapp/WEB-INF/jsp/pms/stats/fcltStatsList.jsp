<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<script type="text/javascript">
$(document).ready(function() {
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();
	
	
	 /*******************************************
	 * 이벤트 설정
	 *******************************************/			
	var now = new Date();
	var year = now.getFullYear();
	var month = now.getMonth() + 1;
	month = month < 10 ? '0' + month : month;
	var date = now.getDate() < 10 ? '0' + now.getDate() : now.getDate();
	
	if("${param.day}" != "") {
		$("#date").val("${param.day}");
	}else {
		$("#date").val(year+"-"+month+"-"+date);
	}
	$("#date").datepicker( "option", "maxDate", "today" );
	setDatepicker();
	
	$("#btnClose").click(function () {
		window.close();
	});
});

function getInputDayLabel(day) { 
	var week = new Array('일', '월', '화', '수', '목', '금', '토'); 
	var today = new Date(day).getDay(); 
	var todayLabel = week[today]; 
	return todayLabel; 
}

function changeDay() {
	location.href='/fms/pms/stats/fcltStatsList.do?day='+$('#date').val();
}
</script>


<body>	

<div id="wrap" class="wrap" > 
  <div id="header" class="header"  >
    <h1 >시설물 통계</h1>
  </div>
	
	
  <!-- container -->
  <div id="container"  class="content"  > 


    <div class="list_box" style="padding:10px 10px 10px 10px; width:1080px;">
      <h2 ><span style="float:left">시설물 통계</span>
        <div class="btn_group m-b-5 right "  >
          <button id="btnClose" type="button" class="btn btn-default " > <img src="/fms/images/icon_btn_delete.png" width="16" height="16" alt=""/>닫기</button>
        </div>
      </h2>
						
	  <div class="list " style="width:1080px; height:440px; overflow-x:hidden">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-list center " style="white-space: nowrap; ">

								<colgroup>
									<col width="20%"></col>
									<col width="20%"></col>
									<col width="12%"></col>
									<col width="12%"></col>
									<col width="12%"></col>
									<col width="12%"></col>
									<col width="12%"></col>
								</colgroup>
								<thead>
									<tr>
										<th >시설명</th>
										<th>
											<input type="text" class="datepicker" name="date" id="date"  style="background:url(/fms/images/icon_calenda.png) no-repeat 30px 50%; width:160px; padding-left:30px" readonly onchange="changeDay();">
										</th> 									
										<th>2017년</th> 									
										<th>2016년</th> 									
										<th>2015년</th> 									
										<th>2014년</th> 									
										<th>2013년</th> 									
									</tr>
								</thead>
								
								<tbody>
									<tr>
										<td class="item">상수관로</td>
										<td><c:if test="${facList0[0].rowCnt == null}">0</c:if><c:if test="${facList0[0].rowCnt != ''}">${facList0[0].rowCnt}</c:if></td>
										<td><c:if test="${facList1[0].rowCnt == null}">0</c:if><c:if test="${facList1[0].rowCnt != ''}">${facList1[0].rowCnt}</c:if></td>
										<td><c:if test="${facList2[0].rowCnt == null}">0</c:if><c:if test="${facList2[0].rowCnt != ''}">${facList2[0].rowCnt}</c:if></td>
										<td><c:if test="${facList3[0].rowCnt == null}">0</c:if><c:if test="${facList3[0].rowCnt != ''}">${facList3[0].rowCnt}</c:if></td>
										<td><c:if test="${facList4[0].rowCnt == null}">0</c:if><c:if test="${facList4[0].rowCnt != ''}">${facList4[0].rowCnt}</c:if></td>
										<td><c:if test="${facList5[0].rowCnt == null}">0</c:if><c:if test="${facList5[0].rowCnt != ''}">${facList5[0].rowCnt}</c:if></td>
									</tr>
									<tr>
										<td  class="item">변류시설</td>
										<td><c:if test="${facList0[1].rowCnt == null}">0</c:if><c:if test="${facList0[1].rowCnt != ''}">${facList0[1].rowCnt}</c:if></td>
										<td><c:if test="${facList1[1].rowCnt == null}">0</c:if><c:if test="${facList1[1].rowCnt != ''}">${facList1[1].rowCnt}</c:if></td>
										<td><c:if test="${facList2[1].rowCnt == null}">0</c:if><c:if test="${facList2[1].rowCnt != ''}">${facList2[1].rowCnt}</c:if></td>
										<td><c:if test="${facList3[1].rowCnt == null}">0</c:if><c:if test="${facList3[1].rowCnt != ''}">${facList3[1].rowCnt}</c:if></td>
										<td><c:if test="${facList4[1].rowCnt == null}">0</c:if><c:if test="${facList4[1].rowCnt != ''}">${facList4[1].rowCnt}</c:if></td>
										<td><c:if test="${facList5[1].rowCnt == null}">0</c:if><c:if test="${facList5[1].rowCnt != ''}">${facList5[1].rowCnt}</c:if></td>
									</tr>
									<tr>
										<td  class="item">소방시설</td>
										<td><c:if test="${facList0[2].rowCnt == null}">0</c:if><c:if test="${facList0[2].rowCnt != ''}">${facList0[2].rowCnt}</c:if></td>
										<td><c:if test="${facList1[2].rowCnt == null}">0</c:if><c:if test="${facList1[2].rowCnt != ''}">${facList1[2].rowCnt}</c:if></td>
										<td><c:if test="${facList2[2].rowCnt == null}">0</c:if><c:if test="${facList2[2].rowCnt != ''}">${facList2[2].rowCnt}</c:if></td>
										<td><c:if test="${facList3[2].rowCnt == null}">0</c:if><c:if test="${facList3[2].rowCnt != ''}">${facList3[2].rowCnt}</c:if></td>
										<td><c:if test="${facList4[2].rowCnt == null}">0</c:if><c:if test="${facList4[2].rowCnt != ''}">${facList4[2].rowCnt}</c:if></td>
										<td><c:if test="${facList5[2].rowCnt == null}">0</c:if><c:if test="${facList5[2].rowCnt != ''}">${facList5[2].rowCnt}</c:if></td>
									</tr>
									<tr>
										<td  class="item">상수맨홀</td>
										<td><c:if test="${facList0[3].rowCnt == null}">0</c:if><c:if test="${facList0[3].rowCnt != ''}">${facList0[3].rowCnt}</c:if></td>
										<td><c:if test="${facList1[3].rowCnt == null}">0</c:if><c:if test="${facList1[3].rowCnt != ''}">${facList1[3].rowCnt}</c:if></td>
										<td><c:if test="${facList2[3].rowCnt == null}">0</c:if><c:if test="${facList2[3].rowCnt != ''}">${facList2[3].rowCnt}</c:if></td>
										<td><c:if test="${facList3[3].rowCnt == null}">0</c:if><c:if test="${facList3[3].rowCnt != ''}">${facList3[3].rowCnt}</c:if></td>
										<td><c:if test="${facList4[3].rowCnt == null}">0</c:if><c:if test="${facList4[3].rowCnt != ''}">${facList4[3].rowCnt}</c:if></td>
										<td><c:if test="${facList5[3].rowCnt == null}">0</c:if><c:if test="${facList5[3].rowCnt != ''}">${facList5[3].rowCnt}</c:if></td>
									</tr>
									<tr>
										<td  class="item">유량계</td>
										<td><c:if test="${facList0[4].rowCnt == null}">0</c:if><c:if test="${facList0[4].rowCnt != ''}">${facList0[4].rowCnt}</c:if></td>
										<td><c:if test="${facList1[4].rowCnt == null}">0</c:if><c:if test="${facList1[4].rowCnt != ''}">${facList1[4].rowCnt}</c:if></td>
										<td><c:if test="${facList2[4].rowCnt == null}">0</c:if><c:if test="${facList2[4].rowCnt != ''}">${facList2[4].rowCnt}</c:if></td>
										<td><c:if test="${facList3[4].rowCnt == null}">0</c:if><c:if test="${facList3[4].rowCnt != ''}">${facList3[4].rowCnt}</c:if></td>
										<td><c:if test="${facList4[4].rowCnt == null}">0</c:if><c:if test="${facList4[4].rowCnt != ''}">${facList4[4].rowCnt}</c:if></td>
										<td><c:if test="${facList5[4].rowCnt == null}">0</c:if><c:if test="${facList5[4].rowCnt != ''}">${facList5[4].rowCnt}</c:if></td>
									</tr>
									<tr>
										<td  class="item">스탠드파이프</td>
										<td><c:if test="${facList0[5].rowCnt == null}">0</c:if><c:if test="${facList0[5].rowCnt != ''}">${facList0[5].rowCnt}</c:if></td>
										<td><c:if test="${facList1[5].rowCnt == null}">0</c:if><c:if test="${facList1[5].rowCnt != ''}">${facList1[5].rowCnt}</c:if></td>
										<td><c:if test="${facList2[5].rowCnt == null}">0</c:if><c:if test="${facList2[5].rowCnt != ''}">${facList2[5].rowCnt}</c:if></td>
										<td><c:if test="${facList3[5].rowCnt == null}">0</c:if><c:if test="${facList3[5].rowCnt != ''}">${facList3[5].rowCnt}</c:if></td>
										<td><c:if test="${facList4[5].rowCnt == null}">0</c:if><c:if test="${facList4[5].rowCnt != ''}">${facList4[5].rowCnt}</c:if></td>
										<td><c:if test="${facList5[5].rowCnt == null}">0</c:if><c:if test="${facList5[5].rowCnt != ''}">${facList5[5].rowCnt}</c:if></td>
									</tr>
									<tr>
										<td  class="item">수압계</td>
										<td><c:if test="${facList0[6].rowCnt == null}">0</c:if><c:if test="${facList0[6].rowCnt != ''}">${facList0[6].rowCnt}</c:if></td>
										<td><c:if test="${facList1[6].rowCnt == null}">0</c:if><c:if test="${facList1[6].rowCnt != ''}">${facList1[6].rowCnt}</c:if></td>
										<td><c:if test="${facList2[6].rowCnt == null}">0</c:if><c:if test="${facList2[6].rowCnt != ''}">${facList2[6].rowCnt}</c:if></td>
										<td><c:if test="${facList3[6].rowCnt == null}">0</c:if><c:if test="${facList3[6].rowCnt != ''}">${facList3[6].rowCnt}</c:if></td>
										<td><c:if test="${facList4[6].rowCnt == null}">0</c:if><c:if test="${facList4[6].rowCnt != ''}">${facList4[6].rowCnt}</c:if></td>
										<td><c:if test="${facList5[6].rowCnt == null}">0</c:if><c:if test="${facList5[6].rowCnt != ''}">${facList5[6].rowCnt}</c:if></td>
									</tr>
									<tr>
										<td  class="item">수원지</td>
										<td><c:if test="${facList0[7].rowCnt == null}">0</c:if><c:if test="${facList0[7].rowCnt != ''}">${facList0[7].rowCnt}</c:if></td>
										<td><c:if test="${facList1[7].rowCnt == null}">0</c:if><c:if test="${facList1[7].rowCnt != ''}">${facList1[7].rowCnt}</c:if></td>
										<td><c:if test="${facList2[7].rowCnt == null}">0</c:if><c:if test="${facList2[7].rowCnt != ''}">${facList2[7].rowCnt}</c:if></td>
										<td><c:if test="${facList3[7].rowCnt == null}">0</c:if><c:if test="${facList3[7].rowCnt != ''}">${facList3[7].rowCnt}</c:if></td>
										<td><c:if test="${facList4[7].rowCnt == null}">0</c:if><c:if test="${facList4[7].rowCnt != ''}">${facList4[7].rowCnt}</c:if></td>
										<td><c:if test="${facList5[7].rowCnt == null}">0</c:if><c:if test="${facList5[7].rowCnt != ''}">${facList5[7].rowCnt}</c:if></td>
									</tr>
									<tr>
										<td  class="item">취수장</td>
										<td><c:if test="${facList0[8].rowCnt == null}">0</c:if><c:if test="${facList0[8].rowCnt != ''}">${facList0[8].rowCnt}</c:if></td>
										<td><c:if test="${facList1[8].rowCnt == null}">0</c:if><c:if test="${facList1[8].rowCnt != ''}">${facList1[8].rowCnt}</c:if></td>
										<td><c:if test="${facList2[8].rowCnt == null}">0</c:if><c:if test="${facList2[8].rowCnt != ''}">${facList2[8].rowCnt}</c:if></td>
										<td><c:if test="${facList3[8].rowCnt == null}">0</c:if><c:if test="${facList3[8].rowCnt != ''}">${facList3[8].rowCnt}</c:if></td>
										<td><c:if test="${facList4[8].rowCnt == null}">0</c:if><c:if test="${facList4[8].rowCnt != ''}">${facList4[8].rowCnt}</c:if></td>
										<td><c:if test="${facList5[8].rowCnt == null}">0</c:if><c:if test="${facList5[8].rowCnt != ''}">${facList5[8].rowCnt}</c:if></td>
									</tr>
									<tr>
										<td  class="item">배수지</td>
										<td><c:if test="${facList0[9].rowCnt == null}">0</c:if><c:if test="${facList0[9].rowCnt != ''}">${facList0[9].rowCnt}</c:if></td>
										<td><c:if test="${facList1[9].rowCnt == null}">0</c:if><c:if test="${facList1[9].rowCnt != ''}">${facList1[9].rowCnt}</c:if></td>
										<td><c:if test="${facList2[9].rowCnt == null}">0</c:if><c:if test="${facList2[9].rowCnt != ''}">${facList2[9].rowCnt}</c:if></td>
										<td><c:if test="${facList3[9].rowCnt == null}">0</c:if><c:if test="${facList3[9].rowCnt != ''}">${facList3[9].rowCnt}</c:if></td>
										<td><c:if test="${facList4[9].rowCnt == null}">0</c:if><c:if test="${facList4[9].rowCnt != ''}">${facList4[9].rowCnt}</c:if></td>
										<td><c:if test="${facList5[9].rowCnt == null}">0</c:if><c:if test="${facList5[9].rowCnt != ''}">${facList5[9].rowCnt}</c:if></td>
									</tr>
									<tr>
										<td  class="item">정수장</td>
										<td><c:if test="${facList0[10].rowCnt == null}">0</c:if><c:if test="${facList0[10].rowCnt != ''}">${facList0[10].rowCnt}</c:if></td>
										<td><c:if test="${facList1[10].rowCnt == null}">0</c:if><c:if test="${facList1[10].rowCnt != ''}">${facList1[10].rowCnt}</c:if></td>
										<td><c:if test="${facList2[10].rowCnt == null}">0</c:if><c:if test="${facList2[10].rowCnt != ''}">${facList2[10].rowCnt}</c:if></td>
										<td><c:if test="${facList3[10].rowCnt == null}">0</c:if><c:if test="${facList3[10].rowCnt != ''}">${facList3[10].rowCnt}</c:if></td>
										<td><c:if test="${facList4[10].rowCnt == null}">0</c:if><c:if test="${facList4[10].rowCnt != ''}">${facList4[10].rowCnt}</c:if></td>
										<td><c:if test="${facList5[10].rowCnt == null}">0</c:if><c:if test="${facList5[10].rowCnt != ''}">${facList5[10].rowCnt}</c:if></td>
									</tr>
									<tr>
										<td  class="item">가압펌프장</td>
										<td><c:if test="${facList0[11].rowCnt == null}">0</c:if><c:if test="${facList0[11].rowCnt != ''}">${facList0[11].rowCnt}</c:if></td>
										<td><c:if test="${facList1[11].rowCnt == null}">0</c:if><c:if test="${facList1[11].rowCnt != ''}">${facList1[11].rowCnt}</c:if></td>
										<td><c:if test="${facList2[11].rowCnt == null}">0</c:if><c:if test="${facList2[11].rowCnt != ''}">${facList2[11].rowCnt}</c:if></td>
										<td><c:if test="${facList3[11].rowCnt == null}">0</c:if><c:if test="${facList3[11].rowCnt != ''}">${facList3[11].rowCnt}</c:if></td>
										<td><c:if test="${facList4[11].rowCnt == null}">0</c:if><c:if test="${facList4[11].rowCnt != ''}">${facList4[11].rowCnt}</c:if></td>
										<td><c:if test="${facList5[11].rowCnt == null}">0</c:if><c:if test="${facList5[11].rowCnt != ''}">${facList5[11].rowCnt}</c:if></td>
									</tr>
									<tr>
										<td  class="item">급수관로</td>
										<td><c:if test="${facList0[12].rowCnt == null}">0</c:if><c:if test="${facList0[12].rowCnt != ''}">${facList0[12].rowCnt}</c:if></td>
										<td><c:if test="${facList1[12].rowCnt == null}">0</c:if><c:if test="${facList1[12].rowCnt != ''}">${facList1[12].rowCnt}</c:if></td>
										<td><c:if test="${facList2[12].rowCnt == null}">0</c:if><c:if test="${facList2[12].rowCnt != ''}">${facList2[12].rowCnt}</c:if></td>
										<td><c:if test="${facList3[12].rowCnt == null}">0</c:if><c:if test="${facList3[12].rowCnt != ''}">${facList3[12].rowCnt}</c:if></td>
										<td><c:if test="${facList4[12].rowCnt == null}">0</c:if><c:if test="${facList4[12].rowCnt != ''}">${facList4[12].rowCnt}</c:if></td>
										<td><c:if test="${facList5[12].rowCnt == null}">0</c:if><c:if test="${facList5[12].rowCnt != ''}">${facList5[12].rowCnt}</c:if></td>
									</tr>
									<tr>
										<td  class="item">급수전계량기</td>
										<td><c:if test="${facList0[13].rowCnt == null}">0</c:if><c:if test="${facList0[13].rowCnt != ''}">${facList0[13].rowCnt}</c:if></td>
										<td><c:if test="${facList1[13].rowCnt == null}">0</c:if><c:if test="${facList1[13].rowCnt != ''}">${facList1[13].rowCnt}</c:if></td>
										<td><c:if test="${facList2[13].rowCnt == null}">0</c:if><c:if test="${facList2[13].rowCnt != ''}">${facList2[13].rowCnt}</c:if></td>
										<td><c:if test="${facList3[13].rowCnt == null}">0</c:if><c:if test="${facList3[13].rowCnt != ''}">${facList3[13].rowCnt}</c:if></td>
										<td><c:if test="${facList4[13].rowCnt == null}">0</c:if><c:if test="${facList4[13].rowCnt != ''}">${facList4[13].rowCnt}</c:if></td>
										<td><c:if test="${facList5[13].rowCnt == null}">0</c:if><c:if test="${facList5[13].rowCnt != ''}">${facList5[13].rowCnt}</c:if></td>
									</tr>
									<tr>
										<td  class="item">저수조</td>
										<td><c:if test="${facList0[14].rowCnt == null}">0</c:if><c:if test="${facList0[14].rowCnt != ''}">${facList0[14].rowCnt}</c:if></td>
										<td><c:if test="${facList1[14].rowCnt == null}">0</c:if><c:if test="${facList1[14].rowCnt != ''}">${facList1[14].rowCnt}</c:if></td>
										<td><c:if test="${facList2[14].rowCnt == null}">0</c:if><c:if test="${facList2[14].rowCnt != ''}">${facList2[14].rowCnt}</c:if></td>
										<td><c:if test="${facList3[14].rowCnt == null}">0</c:if><c:if test="${facList3[14].rowCnt != ''}">${facList3[14].rowCnt}</c:if></td>
										<td><c:if test="${facList4[14].rowCnt == null}">0</c:if><c:if test="${facList4[14].rowCnt != ''}">${facList4[14].rowCnt}</c:if></td>
										<td><c:if test="${facList5[14].rowCnt == null}">0</c:if><c:if test="${facList5[14].rowCnt != ''}">${facList5[14].rowCnt}</c:if></td>
									</tr>
								</tbody>
			</table>
		</div>

	</div>
	<!-- /box_right -->

</div>
<!-- /popup_layer01 -->
