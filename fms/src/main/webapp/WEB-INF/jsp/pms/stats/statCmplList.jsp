<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>


<script type="text/javascript">
$(document).ready(function() {
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();
	
	
	 /*******************************************
	 * 이벤트 설정
	 *******************************************/
	$("#btnClose").click(function () {
		window.close();
	});
});
</script>

<div id="wrap" class="wrap" > 
  <div id="header" class="header"  >
    <h1 >민원 통계</h1>
  </div>
	
	
  <!-- container -->
  <div id="container"  class="content"  > 


    <div class="list_box" style="padding:10px 0px 10px 10px;width:1080px;">
      <h2 ><span style="float:left">민원통계 (${fn:length(repList)}건)</span>
        <div class="btn_group m-b-5 right "  >
          <button id="btnClose" type="button" class="btn btn-default " > <img src="/fms/images/icon_btn_delete.png" width="16" height="16" alt=""/>닫기</button>
        </div>
      </h2>
						
	  <div class="list " style="width:1080px;  height:440px;  overflow-x:hidden">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-list center " style="white-space: nowrap;">

				<thead>
					<tr>
						<th>구분</th>
						<th>1월</th> 									
						<th>2월</th> 									
						<th>3월</th> 									
						<th>4월</th> 									
						<th>5월</th> 									
						<th>6월</th> 									
						<th>7월</th> 									
						<th>8월</th> 									
						<th>9월</th> 									
						<th>10월</th> 									
						<th>11월</th> 									
						<th>12월</th> 	
					</tr>																	
				</thead>
				
				<tbody>
				<c:forEach var="list" items="${repList}" varStatus="listIndex">	
					<tr>
						<td  class="item">${list.nam}</td>
						<td><c:if test="${repMap01[list.cde] == null}">0</c:if><c:if test="${repMap01[list.cde] != null}">${repMap01[list.cde]}</c:if></td>
						<td><c:if test="${repMap02[list.cde] == null}">0</c:if><c:if test="${repMap02[list.cde] != null}">${repMap02[list.cde]}</c:if></td>
						<td><c:if test="${repMap03[list.cde] == null}">0</c:if><c:if test="${repMap03[list.cde] != null}">${repMap03[list.cde]}</c:if></td>
						<td><c:if test="${repMap04[list.cde] == null}">0</c:if><c:if test="${repMap04[list.cde] != null}">${repMap04[list.cde]}</c:if></td>
						<td><c:if test="${repMap05[list.cde] == null}">0</c:if><c:if test="${repMap05[list.cde] != null}">${repMap05[list.cde]}</c:if></td>
						<td><c:if test="${repMap06[list.cde] == null}">0</c:if><c:if test="${repMap06[list.cde] != null}">${repMap06[list.cde]}</c:if></td>
						<td><c:if test="${repMap07[list.cde] == null}">0</c:if><c:if test="${repMap07[list.cde] != null}">${repMap07[list.cde]}</c:if></td>
						<td><c:if test="${repMap08[list.cde] == null}">0</c:if><c:if test="${repMap08[list.cde] != null}">${repMap08[list.cde]}</c:if></td>
						<td><c:if test="${repMap09[list.cde] == null}">0</c:if><c:if test="${repMap09[list.cde] != null}">${repMap09[list.cde]}</c:if></td>
						<td><c:if test="${repMap10[list.cde] == null}">0</c:if><c:if test="${repMap10[list.cde] != null}">${repMap10[list.cde]}</c:if></td>
						<td><c:if test="${repMap11[list.cde] == null}">0</c:if><c:if test="${repMap11[list.cde] != null}">${repMap11[list.cde]}</c:if></td>
						<td><c:if test="${repMap12[list.cde] == null}">0</c:if><c:if test="${repMap12[list.cde] != null}">${repMap12[list.cde]}</c:if></td>
					</tr>
				</c:forEach>
				
				
				</tbody>
			</table>
		</div>

	</div>
	<!-- /box_right -->

</div>
<!-- /popup_layer01 -->



</div>

</body>


</html>