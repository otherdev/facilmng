<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>
<head>
<title>상수관망관리</title>
<link href="css/common_popup.css" rel="stylesheet" />
</head>
<script type="text/javascript">
$(document).ready(function() {
	$('#btnWorkGroupSave').click(function () { 
		
		var deCkmCdes = "";
		var chmCdes = ""; 

		$('input[name=ckmCdes]').each(function(v, k){
			deCkmCdes += $(k).val() + ",";	
			if($(k).is(":checked") == true) { 
				chmCdes += $(k).val() + ",";	
			}
		});
		var objData = new Object();
		objData = { ftrCde    : "${ftrCde}"
			       ,ftrIdn    : "${ftrIdn}"
			       ,ckmCdes   : chmCdes
				   ,deCkmCdes : deCkmCdes
				};

	    gfn_confirm("저장하시겠습니까?", function(ret){
			if(!ret) return;
			//저장처리
			gfn_saveList(
					"/fms/pms/popup/workGroupSave.do"
					,objData
					, function(data){
						console.log("data.result=> " + data.result) ; 
						if (data.result ) {	
							gfn_alert("저장되었습니다.",'',function(){
								self.close();
							});					
							return;
						}
						else{
							gfn_alert("데이터 저장에 실패하였습니다. : " + data.error,"E");
							return;
						}
			});
	    });
	});

});	

var currentInitialize = function() {

};

</script>
<body  >
<div id="wrap" class="wrap" > 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1 >작업그룹 목록</h1>
  </div>
  <!-- // header --> 
  
  <form name="workGroupFrm" id="workGroupFrm" >
  <!-- container -->
  <div id="container"  class="content"  > 
    
    <!-- list -->
    <div class="confirm-box">
      
        <div class="btn_group m-b-5 right "  >
          <button id="btnWorkGroupSave" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_save.png" width="16" height="16" alt=""/> 저장 </button>
        </div>
     
      <div class="table-list-regist m-b-5 ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table table-list-regist  " style="white-space: nowrap; ">
          <colgroup>
          <col width="*">
          </colgroup>
          <thead>
            <tr>
              <th><input type="checkbox" name="checkbox6" id="checkbox6"></th>
              <th>작업그룹명</th>
            </tr>
          </thead>
          <tbody>
            <c:forEach var="list" items="${cmtCkmcMaList}" varStatus="listIndex">																	
				<tr class="viewPopup ${listIndex.index % 2 == 0 ? '' : 'tr_back01' }" >				
					<td><a href="javascript:;"><input type="checkbox" name="ckmCdes" value="${list.ckmCde}" <c:if test="${list.chk == '1'}">checked</c:if>/></a></td>
					<td><a href="javascript:;">${list.ckmNam}</a></td>												
				</tr>
			</c:forEach>

			<c:if test="${empty cmtCkmcMaList}">
				<tr>
					<td colspan="2" style="color:black;text-align:center;">검색 결과가 없습니다.</td>
				</tr>
			</c:if>
          </tbody>
        </table>
      </div>
    </div>
    <!-- //list -->     
  </div>
  
  <!-- //container --> 
</form>
  
</div>
<!-- //UI Object -->

</body>
</html>
