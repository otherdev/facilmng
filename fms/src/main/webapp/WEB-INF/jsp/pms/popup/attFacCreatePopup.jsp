<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>
<head>
<title>상수관망관리</title>
<link href="css/common_popup.css" rel="stylesheet" />
</head>
<script type="text/javascript">

$(document).ready(function() { 
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();
		
	 /*******************************************
	 * 이벤트 설정
	 *******************************************/
	
	$('#btnAttFacSave').click(function () {	 
		
		if(!gfn_formValid("attFacFrm")) return;
		
		gfn_confirm("저장하시겠습니까?",function(ret){
			if(!ret) return;
	    	
			gfn_saveFormCmm("attFacFrm", "insertPopupWttAttaDt", function(data){
				if(data.result){
					gfn_alert("데이터가 저장되었습니다.","",function(){
						opener.fn_reload();
						window.close();
					});
				}else{
					gfn_alert("데이터 저장에 실패하였습니다. : " + data.error,"E");
				}
			});
		});
	});
});	

</script>
<body  >
<div id="wrap" class="wrap" > 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1 >부속시설 세부현황 등록</h1>
  </div>
  <!-- // header --> 
    
  <form name="attFacFrm" id="attFacFrm" method="post">
  <input type="hidden" id="ftrCde" name="ftrCde" value="${ftrCde}">
  <input type="hidden" id="attaSeq" name="attaSeq" value="${attaSeq}">
  <!-- container -->
  <div id="container"  class="content"  > 
    
    <!-- list -->
    <div class="regist-box ">
      <h2 ><span>일반정보</span>
        <div class="btn_group m-b-5 right "  >         
          <button id="btnAttFacSave" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_save.png" width="16" height="16" alt=""/> 저장 </button>
        </div>
      </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table table-view-regist " >
          <colgroup>
          <col width="114px">
          <col width="166px">
          <col width="114px">
          <col width="166px">
      
          </colgroup>
          <tbody>
			<tr>
				<th class="req">관리번호</th>
				<td>
					<input type="text" id="ftrIdn" name="ftrIdn" class="reqVal" value="${ftrIdn }" readonly class="disable" />
				</td>
				<th class="req">지형지물</th>
				<td>
					<input type="text" id="attaFtrCde" name="attaFtrCde" class="reqVal" value="${mapFtrcMa[ftrCde] }" readonly class="disable" />
				</td>
			</tr>
			<tr>
				<th >세부시설번호</th>
				<td>
					<input type="text" id="attIdn" name="attIdn" maxlength="6" onkeydown="onlyNumber(this)"/>
				</td>
				<th >설치연도</th>
				<td>
					<select id="creYy" name="creYy">
						<option value="">선택</option>
				          <c:set var="today" value="<%=new java.util.Date()%>" />					
				          <fmt:formatDate value="${today}" pattern="yyyy" var="start"/>								          
				          <c:forEach var="i" begin="0" end="${(start)-2000}">
						    <c:set var="yearOption" value="${(start)-i}" />
						    <c:choose>
						    	<c:when test="${start eq yearOption}">
							        <option value="${yearOption}" selected>${yearOption}</option>
							    </c:when>
							    <c:otherwise>
							        <option value="${yearOption}">${yearOption}</option>
							    </c:otherwise>
							</c:choose>						    
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<th >세부시설명</th>
				<td colspan="3">
					<input type="text" id="attNam" name="attNam" style="width:412px;"/>
				</td>
			</tr>
			<tr>
				<th >시설개요</th>
				<td colspan="3">
					<input type="text" id="attDes" name="attDes" style="width:412px;"/>
				</td>
			</tr>
          </tbody>
        </table>
      </div>
    </div>
    <!-- //list --> 
    
  </div>
  
  <!-- //container --> 
</form>
  
</div>
<!-- //UI Object -->

</body>
</html>
