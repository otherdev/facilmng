<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<body>	
<script type="text/javascript">

var popupX = (screen.availWidth-660)/2;
var popupY = (screen.availHeight-1130)/3;

$(document).ready(function() {
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();
		
	 /*******************************************
	 * 이벤트 설정
	 *******************************************/
	 
	setDatepicker();
	$(document).on('focus',".datepicker", setDatepicker);
	
	$ ('#searchCondition7').datepicker();
    $ ('#searchCondition7').datepicker("option", "maxDate", $ ("#searchCondition8").val());
    $ ('#searchCondition7').datepicker("option", "onClose", function ( selectedDate ) {
        $ ("#searchCondition8").datepicker( "option", "minDate", selectedDate );
    });
 
    $ ('#searchCondition8').datepicker();
    $ ('#searchCondition8').datepicker("option", "minDate", $ ("#searchCondition7").val());
    $ ('#searchCondition8').datepicker("option", "onClose", function ( selectedDate ) {
        $ ("#searchCondition7").datepicker( "option", "maxDate", selectedDate );
    });
    
    $ ('#searchCondition9').datepicker();
    $ ('#searchCondition9').datepicker("option", "maxDate", $ ("#searchCondition10").val());
    $ ('#searchCondition9').datepicker("option", "onClose", function ( selectedDate ) {
        $ ("#searchCondition10").datepicker( "option", "minDate", selectedDate );
    });
 
    $ ('#searchCondition10').datepicker();
    $ ('#searchCondition10').datepicker("option", "minDate", $ ("#searchCondition9").val());
    $ ('#searchCondition10').datepicker("option", "onClose", function ( selectedDate ) {
        $ ("#searchCondition9").datepicker( "option", "maxDate", selectedDate );
    });
		
	$('#popup_layer01_close').click(function () {
		window.close();
		
		return false;
	});
	$('#searchCondition5').val(setComma('${param.searchCondition5 }'));
	$('#searchCondition6').val(setComma('${param.searchCondition6 }'));
	
	/* 상세 클릭 Event */
	$('.viewPopup').click(function () {
		opener.setChildValue($(this).find('.cntNumPopup').attr('data-cntNum'));
		window.close();
	});

	$('#btnReset').click(function () {
		$('form[name=splyMaPopupForm]')[0].reset();
		
		return false;
	});
	
	$('#btnSearch').click(function () {
		suplMaList(1);
	});
});	

function suplMaList (pageNo) {
	$('form[name=splyMaListForm] input[name=pageIndex]').val(pageNo);
	$('form[name=splyMaListForm]').submit();
}

/* 엔터키 적용 */
function onKeyDown () {
	
	if (event.keyCode == 13) {
		splyMaList(1);
	}
}
	
function getNumber(obj){
    
    var num01;
    var num02;
    num01 = obj.value;
    num02 = num01.replace(/\D/g,""); //숫자가 아닌것을 제거, 
                                     //즉 [0-9]를 제외한 문자 제거; /[^0-9]/g 와 같은 표현
    num01 = setComma(num02); //콤마 찍기
    obj.value =  num01;
}
function setComma(n) {
    var reg = /(^[+-]?\d+)(\d{3})/;   // 정규식
    n += '';                          // 숫자를 문자열로 변환         
    while (reg.test(n)) {
       n = n.replace(reg, '$1' + ',' + '$2');
    }         
    return n;
}
function removeComma() {
	var obj = $('.num_style');
	if (obj.length > 0) {
		for (var i in obj) {
			if (obj[i].value != undefined) {
				obj[i].value = obj[i].value.replace(/,/g,"");
			}
		}
	}
}
	</script>
</head>

<body>

<div id="wrap" class="wrap" > 
  <div id="header" class="header"  >
    <h1 >급수전대장 검색 목록</h1>
  </div>
		
		
<form name="splyMaPopupForm" id="splyMaPopupForm" method="POST" action="${ctxRoot }/pms/popup/facetMngPopup.do">
	<input type="hidden" name="pageIndex" value="1"/>
	<input type="hidden" name="cntNum" id="cntNum" value=""/>
	
	
<!-- container -->
<div id="container" class="content"  > 
	
<div class="seach_box" >
      <h2 >검색항목 </h2>
        <table border="0" cellpadding="0" cellspacing="0" class="table table-search">
          <colgroup>
          <col width="114px">
          <col width="*">
          </colgroup>
          
			<tbody>
						<tr>
							<th>공사구분</th>
							<td>
								<select name="searchCondition1" id="searchCondition1">
									<option value="">전체</option>
									<c:forEach var="cntList" items="${cntCdeList }">
										<option value="${cntList.codeCode }" ${param.searchCondition1 == cntList.codeCode ? "selected" : "" }>${cntList.codeAlias}</option>
									</c:forEach>
								</select>
							</td>
						</tr>
						<tr>
							<th>공사번호</th>
							<td>
								<input type="text" id="searchCondition2" name="searchCondition2" onKeyDown="onKeyDown();" maxlength="10" value="${param.searchCondition2 }"/>
							</td>
						</tr>
						<tr>
							<th>공사명</th>
							<td>
								<input type="text" id="searchCondition3" name="searchCondition3" onKeyDown="onKeyDown();" maxlength="50" value="${param.searchCondition3 }"/>
							</td>
						</tr>
						<tr>
							<th>행정동</th>
							<td>
								<select name="searchCondition4" id="searchCondition4">
									<option value="">전체</option>
									<c:forEach var="cmtList" items="${cmtAdarMaList}" varStatus="status">
										<option value="${cmtList.hjdCde }" ${param.searchCondition4 == cmtList.hjdCde ? "selected" : "" }>${cmtList.hjdNam}</option>
									</c:forEach>
								</select>
							</td>
						</tr>
						<tr>
							<th>계약금액 [이상]</th>
							<td>
								<input type="text" class="num_style" id="searchCondition5" style="text-align: right;" maxlength="10" name="searchCondition5" onchange="getNumber(this);" onkeyup="getNumber(this);" value="${param.searchCondition5 }"/>
							</td>
						</tr>
						<tr>
							<th>계약금액 [이하]</th>
							<td>
								<input type="text" class="num_style" id="searchCondition6" style="text-align: right;" maxlength="10" name="searchCondition6" onchange="getNumber(this);" onkeyup="getNumber(this);" value="${param.searchCondition6 }"/>
							</td>
						</tr>
						<tr>
							<th>착수일자 [이상]</th>
							<td>
								<input type="text" class="datepicker" name="searchCondition7" id="searchCondition7" value="${param.searchCondition7 }"/>
							</td>
						</tr>
						<tr>
							<th>착수일자 [이하]</th>
							<td>
								<input type="text" class="datepicker"  name="searchCondition8" id="searchCondition8" value="${param.searchCondition8 }"/>
							</td>
						</tr>
						<tr>
							<th>준공일자 [이상]</th>
							<td>
								<input type="text" class="datepicker" name="searchCondition9" id="searchCondition9" value="${param.searchCondition9 }"/>
							</td>
						</tr>
						<tr>
							<th>준공일자 [이하]</th>
							<td>
								<input type="text" class="datepicker" name="searchCondition10" id="searchCondition10" value="${param.searchCondition10 }"/>
							</td>
						</tr>
						<tr>
							<th>설치주소</th>
							<td>
								<input type="text" id="searchCondition11" name="searchCondition11" onKeyDown="onKeyDown();" maxlength="50" value="${param.searchCondition11 }"/>
							</td>
						</tr>
						<tr>
							<th >청구자주소</th>
							<td>
								<input type="text" id="searchCondition12" name="searchCondition12" onKeyDown="onKeyDown();" maxlength="50" value="${param.searchCondition12 }">
							</td>
						</tr>
						<tr>
							<th >시공자명</th>
							<td>
								<input type="text" id="searchCondition13" name="searchCondition13" onKeyDown="onKeyDown();" maxlength="20" value="${param.searchCondition13 }"/>
							</td>
						</tr>
					</tbody>
	</table>
	
	
      <div class="btn_group m-t-10 "  >
        <button id="btnReset" class="btn btn-search "   style="width:114px"><img src="/fms/images/icon_btn_reset.png" width="16" height="16" alt=""/> 초기화 </button>
        <button id="btnSearch" class="btn btn-search "  style="width:132px"><img src="/fms/images/icon_btn_search.png" width="16" height="16" alt=""/> 검색 </button>
      </div>

	</div>
	
    
	<!-- /box_left -->
	
	
    <div class="list_box">
      <h2 ><span style="float:left">목록 | 급수전대장(${paginationInfo.totalRecordCount}건)</span>
        <div class="btn_group m-b-5 right "  style="height:22px;" >
        </div>
      </h2>
						
	  <div class="list ">

        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-striped table-list center " style="white-space: nowrap;">

				<thead>
					<tr>
						<th>공사번호 </th>
						<th>공사명 </th>
						<th>행정동 </th>
						<th>공사구분 </th>
						<th>시공자명 </th>
						<th>설계총액 </th>
						<th>설계일자 </th>
						<th>착수일자 </th>
						<th>준공일자 </th>
						<th>시공자명 </th>
						<th>설치주소 </th>
						<th>청구지주소 </th>
						
					</tr>
				</thead>
				
				<tbody>
				<c:forEach var="result" items="${resultList }" varStatus="listIndex">
					<tr class="viewPopup ${listIndex.index % 2 == 0 ? '' : 'tr_back01' }" cntNum="${result.cntNum }" g2Id="${result.g2Id }"  >
						<!--td><a href="javascript:;">${listIndex.index + 1}</a></td>-->
						<td><a href="javascript:;" class="cntNumPopup" data-cntNum="${result.cntNum}">${result.cntNum }</a> </td>
						<td class= "td_left"><a href="javascript:;" >${result.wttSujnMa.cntNam }</a> </td>
						<td><a href="javascript:;">${result.hjdCde }</a> </td>
						<td><a href="javascript:;">${mapCntCde[result.wttSujnMa.conCde]}</a> </td>
						<td><a href="javascript:;">${result.oprNam }</a> </td>
						<td class="td_right"><a href="javascript:;"><fmt:formatNumber value="${result.totAmt }" pattern="#,###" /></a> </td>
						<td class= "td_right"><a href="javascript:;">${result.wttSujnMa.dsnYmd }</a> </td>
						<td class= "td_right"><a href="javascript:;">${result.begYmd }</a> </td>
						<td class= "td_center"><a href="javascript:;">${result.fnsYmd }</a> </td>
						<td class= "td_left"><a href="javascript:;">${result.oprNam }</a> </td>
						<td class= "td_left"><a href="javascript:;">${result.wttSujnMa.istAdr }</a> </td>
						<td class= "td_left"><a href="javascript:;">${result.wttSujnMa.rcvAdr }</a> </td>
					</tr>
				</c:forEach>
				
				<c:if test="${empty resultList}">
					<tr>
						<td colspan="13" style="color:black;text-align:center;">검색 결과가 없습니다.</td>
					</tr>
				</c:if>
				</tbody>
			</table>
		</div>


      <div style="width:100%; text-align:center">
        <ul class="pagination pagination-sm  " >
			<ui:pagination paginationInfo = "${paginationInfo}"  type="image" jsFunction="fn_egov_link_page" />
        </ul>
      </div>


	</div>
	<!-- /box_right -->

</div>	
</form>
	
</div>

</body>

</html>