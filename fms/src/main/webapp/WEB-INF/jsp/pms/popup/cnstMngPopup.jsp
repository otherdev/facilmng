<!-- 
* 상수공사대장 검색목록
	
	* 검색조건 빠진항목 
	  ㄴ 법적읍면동
	  ㄴ 설치주소
	  ㄴ 청구자주소
	  ㄴ 시공자명

-->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<script type="text/javascript">
var popupX = (screen.availWidth-660)/2;
var popupY= (screen.availHeight-1130)/3;

$(document).ready(function() {
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();
	
	// 달력 이상-이하
	setDatepicker();
	$(document).on('focus',".datepicker", setDatepicker);
	
	$ ('#searchCondition6').datepicker();
    $ ('#searchCondition6').datepicker("option", "maxDate", $("#searchCondition7").val());
    $ ('#searchCondition6').datepicker("option", "onClose", function ( selectedDate ) {
        $ ("#searchCondition7").datepicker( "option", "minDate", selectedDate );
    });
 
    $ ('#searchCondition7').datepicker();
    $ ('#searchCondition7').datepicker("option", "minDate", $("#searchCondition6").val());
    $ ('#searchCondition7').datepicker("option", "onClose", function ( selectedDate ) {
        $ ("#searchCondition6").datepicker( "option", "maxDate", selectedDate );
    });
    
    $ ('#searchCondition8').datepicker();
    $ ('#searchCondition8').datepicker("option", "maxDate", $("#searchCondition9").val());
    $ ('#searchCondition8').datepicker("option", "onClose", function ( selectedDate ) {
        $ ("#searchCondition9").datepicker( "option", "minDate", selectedDate );
    });
 
    $ ('#searchCondition9').datepicker();
    $ ('#searchCondition9').datepicker("option", "minDate", $("#searchCondition8").val());
    $ ('#searchCondition9').datepicker("option", "onClose", function ( selectedDate ) {
        $ ("#searchCondition8").datepicker( "option", "maxDate", selectedDate );
    });
	
	$('#popup_layer01_close').click(function () {
		window.close();
	});

	
	$('#searchCondition4').val(setComma('${param.searchCondition4 }'));
	$('#searchCondition5').val(setComma('${param.searchCondition5 }'));
	 /*******************************************
	 * 이벤트 설정
	 *******************************************/
	
	 
	/* 상세 클릭 Event */
	$('.viewPopup').click(function () {
		opener.setChildValue($(this).find('.cntNumPopup').attr('data-cntNum'));
		window.close();
	});
	$('#btnReset').click(function () {
		$('form[name=consMaPopupForm]')[0].reset();
		
		return false;
	});
	
	$('#btnSearch').click(function () {
		consMaList(1);
	});
});	

// 페이징 함수
function consMaList (pageNo) {
	$('form[name=consMaPopupForm] input[name=pageIndex]').val(pageNo);
	$('form[name=consMaPopupForm]').submit();
}
/** 공통처리 필요 */
//금액입력시 숫자 외 문자 제거 
function onlyNumber (obj) {
	$(obj).keyup(function () {
		$(this).val($(this).val().replace(/[^0-9]/g,''));
	});
}

// 금액 처리 함수
function getNumber(obj){
    
    var num01;
    var num02;
    num01 = obj.value;
    num02 = num01.replace(/\D/g,""); //숫자가 아닌것을 제거, 
                                     //즉 [0-9]를 제외한 문자 제거; /[^0-9]/g 와 같은 표현
    num01 = setComma(num02); //콤마 찍기
    obj.value =  num01;
}

// 콤마 셋팅 함수
function setComma(n) {
    var reg = /(^[+-]?\d+)(\d{3})/;   // 정규식
    n += '';                          // 숫자를 문자열로 변환         
    while (reg.test(n)) {
       n = n.replace(reg, '$1' + ',' + '$2');
    }         
    return n;
}

// 콤마 제거 함수
function removeComma() {
	var obj = $('.num_style');
	if (obj.length > 0) {
		for (var i in obj) {
			if (obj[i].value != undefined) {
				obj[i].value = obj[i].value.replace(/,/g,"");
			}
		}
	}
}
</script>
</head>

<body>

<div id="wrap" class="wrap" > 
  <div id="header" class="header"  >
    <h1 >상수공사 대장 검색목록</h1>
  </div>
			
			
			
  <form name="consMaPopupForm" id="consMaPopupForm"  method="POST" action="${ctxRoot }/pms/popup/cnstMngPopup.do">
	<input type="hidden" name="pageIndex" value="1"/>
	<input type="hidden" id="cntNum" name="cntNum" value=""/>
				
  <!-- container -->
  <div id="container"  class="content"  > 
				
				
    <div class="seach_box" >
      <h2 >검색항목 </h2>
      <div>
        <table border="0" cellpadding="0" cellspacing="0" class="table table-search">
          <colgroup>
          <col width="114px">
          <col width="*">
          </colgroup>
          
			<tbody>
				<tr>
					<th>공사구분</th>
					<td>
						<select name="searchCondition1" id="searchCondition1">
							<option value="">전체</option>
							<c:forEach var="cntList" items="${cntCdeList }">
								<option value="${cntList.codeCode}" ${param.searchCondition1 == cntList.codeCode ? "selected" : "" }>${cntList.codeAlias}</option>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<th>공사번호</th>
					<td>
						<input type="text" name="searchCondition2" id="searchCondition2" maxlength="10" value="${param.searchCondition2 }" />
					</td>
				</tr>
				<tr>
					<th>공사명</th>
					<td>
						<input type="text" name="searchCondition3" id="searchCondition3" value="${param.searchCondition3 }" />
					</td>
				</tr>
				<tr>
					<th>계약금액 [이상]</th>
					<td>
						<input type="text" class="num_style" style="text-align:right;" id="searchCondition4" name="searchCondition4" onchange="getNumber(this);" onkeyup="getNumber(this);" maxlength="16" value="${param.searchCondition4 }" />
					</td>
				</tr>
				<tr>
					<th>계약금액 [이하]</th>
					<td>
						<input type="text" class="num_style" style="text-align:right;" id="searchCondition5" name="searchCondition5" onchange="getNumber(this);" onkeyup="getNumber(this);" maxlength="16" value="${param.searchCondition5 }" />
					</td>
				</tr>
				<tr>
					<th>착공일자 [이상] </th>
					<td>
						<input type="text" class="datepicker" name="searchCondition6" id="searchCondition6" value="${param.searchCondition6 }" />
					</td>
				</tr>
				<tr>
					<th>착공일자 [이하]</th>
					<td>
						<input type="text" class="datepicker" name="searchCondition7" id="searchCondition7" value="${param.searchCondition7 }" />
					</td>
				</tr>
				<tr>
					<th>준공일자 [이상] </th>
					<td>
						<input type="text" class="datepicker" name="searchCondition8" id="searchCondition8" value="${param.searchCondition8 }" />
					</td>
				</tr>
				<tr>
					<th>준공일자 [이하]</th>
					<td>
						<input type="text" class="datepicker" name="searchCondition9" id="searchCondition9" value="${param.searchCondition9 }" />
					</td>
				</tr>
				<tr>
					<th>계약방법</th>
					<td>
						<select name="searchCondition10" id="searchCondition10">
							<option value="">전체</option>
							<c:forEach var="cttList" items="${cttCodeList }">
								<option value="${cttList.codeCode }"${param.searchCondition10 == cttList.codeCode ? "selected" : "" }>${cttList.codeAlias}</option>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<th>공사위치</th>
					<td>
						<input type="text" id="searchCondition11" name="searchCondition11" value="${param.searchCondition11 }" />
					</td>
				</tr>
		</tbody>
	</table>
	
	
      <div class="btn_group m-t-10 "  >
        <button id="btnReset" class="btn btn-search "   style="width:114px"><img src="/fms/images/icon_btn_reset.png" width="16" height="16" alt=""/> 초기화 </button>
        <button id="btnSearch" class="btn btn-search "  style="width:132px"><img src="/fms/images/icon_btn_search.png" width="16" height="16" alt=""/> 검색 </button>
      </div>
      
	</div>
	</div>
	<!-- /box_left -->


    <div class="list_box">
      <h2 ><span style="float:left">목록 | 상수공사대장(${paginationInfo.totalRecordCount}건)</span>
        <div class="btn_group m-b-5 right "  style="height:22px;" >
        </div>
      </h2>
						
	  <div class="list ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-striped table-list center " style="white-space: nowrap;">
          <colgroup>
          <col width="">
          </colgroup>
			<thead>
				<tr>
					<th>공사번호</th>
					<th>공사명</th>
					<th>공사위치</th>
					<th>공사구분</th>
					<th>설계자명</th>
					<th>설계총액</th>
					<th>계약방법</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="list" items="${resultList}" varStatus="listIndex">																		
					<tr class="viewPopup ${listIndex.index % 2 == 0 ? '' : 'tr_back01' }">
						<!--td><a href="javascript:;">${listIndex.index + 1}</a></td>-->								
						<td ><a href="javascript:;" class="cntNumPopup" data-cntNum="${list.cntNum}" data-g2Id="${list.g2Id }">${list.cntNum}</a></td>
						<td class="left"><a href="javascript:;">${list.cntNam}</a></td>
						<td class="left"><a href="javascript:;">${list.cntLoc}</a></td>
						<td ><a href="javascript:;">${mapCntCde[list.cntCde]}</a></td>
						<td><a href="javascript:;">${list.dsnNam}</a></td>
						<td ><a href="javascript:;"><fmt:formatNumber value="${list.dsnAmt}" pattern="#,###" /></a></td>
						<td>
							<a href="javascript:;">
								<c:forEach var="cttList" items="${cttCodeList }">
									<c:if test="${list.cttCde == cttList.codeCode }">
										${cttList.codeAlias}
									</c:if>
								</c:forEach>
							</a>
						</td>
						<td></td>
					</tr>
				</c:forEach>					
							
				<c:if test="${empty resultList}">
					<tr>
						<td colspan="7" style="color:black;text-align:center;">검색 결과가 없습니다.</td>
					</tr>
				</c:if>
			</tbody>
		</table>
	</div>
		
		
      <div style="width:100%; text-align:center">
        <ul class="pagination pagination-sm  " >
			<ui:pagination paginationInfo = "${paginationInfo}"  type="image" jsFunction="consMaList" />
        </ul>
      </div>
		
	</div>
	<!-- /box_right -->
					
					
  </div>	

  </form>

</div>
<!-- /popup_layer01 -->

<div id="popup_layer01"></div>
</body>

</html>