<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>
<head>
<title>상수관망관리</title>
<link href="css/common_popup.css" rel="stylesheet" />
</head>
<script type="text/javascript">
var saveLink = '${ctxRoot}/pms/popup/wtrTrk/attFacPopupUpdate.do?ftrCde=${resultVO.ftrCde}&ftrIdn=${resultVO.ftrIdn}&attNum=${resultVO.attNum}&attNum=${resultVO.attNum}&g2Id=${resultVO.g2Id}';
var deleteLink = '${ctxRoot}/pms/popup/wtrTrk/attFacPopupDelete.do?ftrCde=${resultVO.ftrCde}&ftrIdn=${resultVO.ftrIdn}&attNum=${resultVO.attNum}&attNum=${resultVO.attNum}&g2Id=${resultVO.g2Id}';
$(document).ready(function() { 
	$('#btnSave').click(function () { 
	    gfn_confirm("저장하시겠습니까?", function(ret){
			if(!ret) return;
			$('#attFacFrm').attr('action', saveLink);
			$('#attFacFrm').submit();
	    });
	});
	$('#btnDelete').click(function () { 
	    gfn_confirm("삭제하시겠습니까?", function(ret){
			if(!ret) return;
			$('#attFacFrm').attr('action', deleteLink);
			$('#attFacFrm').submit();
		});
	});

});	

var currentInitialize = function() {

};

</script>
<body  >
<div id="wrap" class="wrap" > 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1 >청소이력 상세정보</h1>
  </div>
  <!-- // header --> 
  
  <form name="attFacFrm" id="attFacFrm" method="post" action="${ctxRoot}/pms/popup/wtrTrk/attFacViewPopup.do?ftrCde=${resultVO.ftrCde}&ftrIdn=${resultVO.ftrIdn}&g2Id=${resultVO.g2Id}">
  <!-- container -->
  <div id="container"  class="content"  > 
    
    <!-- list -->
    <div class="regist-box ">
      <h2 ><span>일반정보</span>
        <div class="btn_group m-b-5 right "  >    
          <button id="btnDelete" type="button" class="btn btn-default " > <img src="/fms/images/icon_btn_delete.png" width="16" height="16" alt=""/> 삭제 </button>     
          <button id="btnSave" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_save.png" width="16" height="16" alt=""/> 저장 </button>
        </div>
      </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table table-view-regist " >
          <colgroup>
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
      
          </colgroup>
          <tbody>
			<tr>
				<th >세부시설번호</th>
				<td>
					<input type="text" name="attNum" id="attNum" readonly class="disable"  value="${resultVO.attNum }"/>
				</td>
				<th >지상지하구분</th>
				<td>
					<input type="text" name="ougCde" id="ougCde" value="${resultVO.ougCde }" />
				</td>
			</tr>
			<tr>
				<th >관재질</th>
				<td>
					<select name="mopCde" id="mopCde">
						<option value="">선택안함</option>
						<c:forEach var="mopCdeList" items="${mopCdeList}" varStatus="status">
							<option value="${mopCdeList.codeCode }" ${mopCdeList.codeCode == resultVO.mopCde ? "selected" : "" }>${mopCdeList.codeAlias}</option>
						</c:forEach>
					</select>
				</td>
				<th >저수조 개소수</th>
				<td>
					<input type="text" name="rsrCnt" id="rsrCnt" value="${resultVO.rsrCnt }" />
				</td>
			</tr>
			<tr>
				<th >저수조규격</th>
				<td>
					<input type="text" name="rsrStd" id="rsrStd" value="${resultVO.rsrStd }" />
				</td>
				<th >저수조용량</th>
				<td>
					<select name="rsrVol" id="rsrVol">
						<option value="">전체</option>
						<option value="1" <c:if test="${resultVO.rsrVol == '1'}">selected</c:if>>용량</option>
					</select>
				</td>
			</tr>
			<tr>
				<th >저수조용도</th>
				<td>
					<input type="text" name="sacCde" id="sacCde" value="${resultVO.sacCde }" />
				</td>
				<th >맨홀개소수</th>
				<td>
					<input type="text" name="manCnt" id="manCnt" value="${resultVO.manCnt }" />
				</td>
			</tr>
			<tr>
				<th >맨홀규격</th>
				<td>
					<input type="text" name="manStd" id="manStd" value="${resultVO.manStd }" />
				</td>
				<th >맨홀 설치위치</th>
				<td>
					<input type="text" name="manLoc" id="manLoc" value="${resultVO.manLoc }" />
				</td>
			</tr>
			<tr>
				<th >침전물배출구여부</th>
				<td>
					<input type="text" name="dcwCde" id="dcwCde" value="${resultVO.dcwCde }" />
				</td>
				<th >침전물배출구위치</th>
				<td>
					<select name="dcwLoc" id="dcwLoc">
						<option value="">전체</option>
						<option value="1" <c:if test="${resultVO.dcwLoc == '1'}">selected</c:if>>침전물</option>
					</select>
				</td>
			</tr>
			<tr>
				<th >유입배출구시설현황</th>
				<td>
					<input type="text" name="iohExp" id="iohExp" value="${resultVO.iohExp }" />
				</td>
				<th >월류관규격</th>
				<td>
					<input type="text" name="wolStd" id="wolStd" value="${resultVO.wolStd }"/>
				</td>
			</tr>
			<tr>
				<th >월류관위치</th>
				<td>
					<input type="text" name="wolLoc" id="wolLoc" value="${resultVO.wolLoc }" />
				</td>
				<th >통기관규격</th>
				<td>
					<input type="text" name="togStd" id="togStd" value="${resultVO.togStd }" />
				</td>
			</tr>
			<tr>
				<th >통기관위치</th>
				<td>
					<input type="text" name="togLoc" id="togLoc" value="${resultVO.togLoc }" />
				</td>
				<th >만수 감수경보장치여부</th>
				<td>
					<input type="text" name="lhwCde" id="lhwCde" value="${resultVO.lhwCde }" />
				</td>
			</tr>
			<tr>
				<th >만수 감수경보장치위치</th>
				<td>
					<input type="text" name="lhwLoc" id="lhwLoc" value="${resultVO.lhwLoc }" />
				</td>
				<th >수조구획여부</th>
				<td>
					<input type="text" name="tnkCde" id="tnkCde" value="${resultVO.tnkCde }" />
				</td>
			</tr>
			<tr>
				<th >기타설명</th>
				<td>
					<input type="text" name="etcExp" id="etcExp" value="${resultVO.etcExp }" />
				</td>
				<th >속성최종수정자명</th>
				<td>
					<input type="text" name="attUsr" id="attUsr" value="${resultVO.attTim }" />
				</td>
			</tr>
			<tr>
				<th >속성최종수정일자</th>
				<td>
					<input type="text" name="attTim" id="attTim" value="${resultVO.attTim }" />
				</td>
				<th >-</th>
				<td>
					<input type="text" id="" name="" readonly class="disable" />
				</td>
			</tr>
          </tbody>
        </table>
      </div>
    </div>
    <!-- //list --> 
    
  </div>
  
  <!-- //container --> 
</form>
  
</div>
<!-- //UI Object -->

</body>
</html>
