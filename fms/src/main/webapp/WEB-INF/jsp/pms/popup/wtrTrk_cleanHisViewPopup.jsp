<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>
<head>
<title>상수관망관리</title>
<link href="css/common_popup.css" rel="stylesheet" />
</head>

<script type="text/javascript">

$(document).ready(function() { 
		/*******************************************
		 * 화면초기화
		 *******************************************/
		gfn_init();
		
		var msg = '${msg}';
		if(msg) {
			alert(msg);			
			window.close();
		}
		
		
		
		 /*******************************************
		 * 이벤트 설정
		 *******************************************/
		 //저장
		$('#btnSave').click(function () { 
		    
			if(!gfn_formValid("attFacFrm")) return;
			
			gfn_confirm("저장하시겠습니까?",function(ret){
				if(!ret) return;
		    	
				gfn_saveFormCmm("attFacFrm", "updateWttRsrvHt", function(data){
					if(data.result){
						gfn_alert("데이터가 저장되었습니다.","",function(){
							opener.fn_reload();
							window.close();
						});
					}else{
						gfn_alert("데이터 저장에 실패하였습니다. : " + data.error,"E");
					}
				});
			});
		});
		 
		 
		 //삭제
		$('#btnDelete').click(function () { 
			gfn_confirm("삭제하시겠습니까?",function(ret){
				if(!ret) return;
				
				gfn_saveCmm({sqlId:"deleteWttRsrvHt", data:{ftrCde: "${ftrCde}", ftrIdn: "${ftrIdn}", seq: "${seq}"}  }
				,function(data){
					if (data.result) {
						gfn_alert("삭제 되었습니다.","",function(){
							opener.fn_reload();
							window.close();
							return;
						});
					}		
					else{
						gfn_alert("저장에 실패하였습니다.","E");
						return;
					}
				});
			});		
		});
});	


</script>
<body  >
<div id="wrap" class="wrap" > 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1 >청소이력 상세정보</h1>
  </div>
  <!-- // header --> 
  
  <form name="attFacFrm" id="attFacFrm" method="post" action="">
  <input type="hidden" name="seq" id="seq" value="${resultVO.seq }" />
  <!-- container -->
  <div id="container"  class="content"  > 
    
    <!-- list -->
    <div class="regist-box ">
      <h2 ><span>일반정보</span>
        <div class="btn_group m-b-5 right "  >    
          <button id="btnDelete" type="button" class="btn btn-default " > <img src="/fms/images/icon_btn_delete.png" width="16" height="16" alt=""/> 삭제 </button>     
          <button id="btnSave" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_save.png" width="16" height="16" alt=""/> 저장 </button>
        </div>
      </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table table-view-regist " >
          <colgroup>
          <col width="130px">
          <col width="*">
          <col width="130px">
          <col width="*">
      
          </colgroup>
          <tbody>
			<tr>
				<th >청소일렬번호</th>
				<td>
					<input type="text" name="seq" id="seq" value="${resultVO.seq }" readonly class="disable" />
				</td>
				<th >청소일자</th>
				<td>
					<input type="text" name="clnYmd" id="clnYmd" class="datepicker" value="${resultVO.clnYmd }" />
				</td>
			</tr>
			<tr>
				<th >청소업체명</th>
				<td colspan="3">
					<input type="text" name="clnExp" id="clnExp" value="${resultVO.clnExp }" style="width:408px;" />
				</td>
			</tr>
			<tr>
				<th >청소내용</th>
				<td colspan="3">
					<input type="text" name="clnNam" id="clnNam" value="${resultVO.clnNam }" style="width:408px;"/>
				</td>
			</tr>
          </tbody>
        </table>
      </div>
    </div>
    <!-- //list --> 
    
  </div>
  
  <!-- //container --> 
</form>
  
</div>
<!-- //UI Object -->

</body>
</html>
