<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>
<head>
<title>상수관망관리</title>
<link href="css/common_popup.css" rel="stylesheet" />
</head>
<script type="text/javascript">


$(document).ready(function() { 
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();
	
	var msg = '${msg}';
	if(msg) {
		alert(msg);			
		window.close();
	}
	
	
	
	 /*******************************************
	 * 이벤트 설정
	 *******************************************/
	$('#btnSave').click(function () { 
		if(!gfn_formValid("hydtMetrChgFrm")) return;
		
		gfn_confirm("저장하시겠습니까?",function(ret){
			if(!ret) return;
	    	
			gfn_saveFormCmm("hydtMetrChgFrm", "updateWttMetaHt", function(data){
				if(data.result){
					gfn_alert("데이터가 저장되었습니다.","",function(){
						opener.fn_reload();
						window.close();
					});
				}else{
					gfn_alert("데이터 저장에 실패하였습니다. : " + data.error,"E");
				}
			});
		});
	});

	 
	$('#btnDelete').click(function () {
		gfn_confirm("삭제하시겠습니까?",function(ret){
			if(!ret) return;
			
			gfn_saveCmm({sqlId:"deleteWttMetaHt", data:{ftrCde: "${ftrCde}", ftrIdn: "${ftrIdn}", metaSeq: "${metaSeq}"}  }
			,function(data){
				if (data.result) {
					gfn_alert("삭제 되었습니다.","",function(){
						opener.fn_reload();
						window.close();
						return;
					});
				}		
				else{
					gfn_alert("저장에 실패하였습니다.","E");
					return;
				}
			});
		});		
		
	});
	

});	

</script>
<body  >
<div id="wrap" class="wrap" > 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1 >계량기교체이력 상세정보</h1>
  </div>
  <!-- // header --> 
  
  <form name="hydtMetrChgFrm" id="hydtMetrChgFrm" method="post" action="${ctxRoot}/pms/popup/hydtMetrChgUpdate.do?ftrCde=${ftrCde}&ftrIdn=${ftrIdn}">
  	<input type="hidden" name="ftrCde" value="${ftrCde}" />
  	<input type="hidden" name="ftrIdn" value="${ftrIdn}" />

  <!-- container -->
  <div id="container"  class="content"  > 
    
    <!-- list -->
    <div class="regist-box ">
      <h2 ><span>일반정보</span>
        <div class="btn_group m-b-5 right "  >   
          <button id="btnDelete" type="button" class="btn btn-default " > <img src="/fms/images/icon_btn_delete.png" width="16" height="16" alt=""/> 삭제 </button>
          <button id="btnSave" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_save.png" width="16" height="16" alt=""/> 저장 </button>
        </div>
      </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table table-view-regist " >
          <colgroup>
          <col width="130px">
          <col width="*">
          <col width="130px">
          <col width="*">      
          </colgroup>
          <tbody>
			<tr>
				<th >변경일련번호</th>
				<td>
					<input type="text" name=metaSeq id="metaSeq" value="${resultVO.metaSeq}" class="disable" readonly>
				</td>
				<th >교체구분</th>
				<td>
					<select id="gcwCde" name="gcwCde">
						<option value="">교체구분</option>
						<c:forEach var="gcwCdeList" items="${gcwCdeList}" varStatus="status">
							<option value="${gcwCdeList.codeCode }" ${resultVO.gcwCde == gcwCdeList.codeCode? "selected" : "" } >${gcwCdeList.codeAlias}</option>
						</c:forEach>	
					</select>
				</td>
			</tr>
			<tr>
				<th >교체일자</th>
				<td>
					<input type="text" class="datepicker" name="chgYmd" id="chgYmd" value="${resultVO.chgYmd }">
				</td>
				<th >철거계량기기물번호</th>
				<td>
					<input type="text" name="omeNum" id="omeNum" value="${resultVO.omeNum }">
				</td>
			</tr>
			<tr>
				<th class="num" >철거계량기구경</th>
				<td>
					<input type="text" name="omeDip" id="omeDip" value="${resultVO.omeDip }" class="numVal" >
				</td>
				<th >철거계량기형식</th>
				<td>
					<select id="omeMof" name="omeMof">
						<option value="">철거계량기형식</option>
						<c:forEach var="mofCdeList" items="${mofCdeList}" varStatus="status">
							<option value="${mofCdeList.codeCode }" ${resultVO.omeMof == mofCdeList.codeCode? "selected" : "" } >${mofCdeList.codeAlias}</option>
						</c:forEach>	
					</select>
				</td>
			</tr>
			<tr>
				<th class="num" >철거계량기지침수</th>
				<td>
					<input type="text" name="omeCnt" id="omeCnt" value="${resultVO.omeCnt }" class="numVal" >
				</td>
				<th >철거계량기제작회사</th>
				<td>
					<input type="text" name="omeNam" id="omeNam" value="${resultVO.omeNam }">
				</td>
			</tr>
          </tbody>
        </table>
      </div>
    </div>
    <!-- //list --> 
    
  </div>
  
  <!-- //container --> 
</form>
  
</div>
<!-- //UI Object -->

</body>
</html>
