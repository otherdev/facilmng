<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<script type="text/javascript">	

var popupX = (screen.availWidth-660)/2;
var popupY = (screen.availHeight-1130)/3;

$(document).ready(function() {
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();
	
	
	 /*******************************************
	 * 이벤트 설정
	 *******************************************/
	 	 
	$ ('#searchCondition4').datepicker();
    $ ('#searchCondition4').datepicker("option", "maxDate", $ ("#searchCondition5").val());
    $ ('#searchCondition4').datepicker("option", "onClose", function ( selectedDate ) {
        $ ("#searchCondition5").datepicker( "option", "minDate", selectedDate );
    });
 
    $ ('#searchCondition5').datepicker();
    $ ('#searchCondition5').datepicker("option", "minDate", $ ("#searchCondition4").val());
    $ ('#searchCondition5').datepicker("option", "onClose", function ( selectedDate ) {
        $ ("#searchCondition4").datepicker( "option", "maxDate", selectedDate );
    });
    
	$ ('#searchCondition6').datepicker();
    $ ('#searchCondition6').datepicker("option", "maxDate", $ ("#searchCondition7").val());
    $ ('#searchCondition6').datepicker("option", "onClose", function ( selectedDate ) {
        $ ("#searchCondition7").datepicker( "option", "minDate", selectedDate );
    });
 
    $ ('#searchCondition7').datepicker();
    $ ('#searchCondition7').datepicker("option", "minDate", $ ("#searchCondition6").val());
    $ ('#searchCondition7').datepicker("option", "onClose", function ( selectedDate ) {
        $ ("#searchCondition6").datepicker( "option", "maxDate", selectedDate );
    }); 			
		    
	$('#btnReset').click(function () {
           var frm = $('#consMaListFrm');
           frm.find('input[type=text]').val('');
           frm.find('select').val('');
        });
	
	$('#btnSearch').click(function () {
		fn_search(1);
	});	
			
	//상세팝업
	$(".dtlPopup").click(function(event){
		opener.setRcvNumChildValue($(this).attr('rcvNum'));
		window.close();		
	});

});	

/* pagination 페이지 링크 function */
function fn_search(pageNo){
	if(gfn_isNull(pageNo)){
		pageNo = 1;
	}	
	document.getElementById("consMaListFrm").pageIndex.value = pageNo;
   	document.getElementById("consMaListFrm").submit();
}

</script>


<body>	

<div id="wrap" class="wrap" > 
  <div id="header" class="header"  >
    <h1 >상수공사 민원관리</h1>
  </div>
	
	<form:form  commandName="searchVO" id="consMaListFrm" name="consMaListFrm" method="post" action="${ctxRoot}/pms/cmpl/cnstCmplList.do">
		<input type="hidden" name="pageIndex" value="1"/>
	
  <!-- container -->
  <div id="container"  class="content"  > 
				
				
    <div class="seach_box" >
      <h2 >검색항목 </h2>
      <div>
        <table border="0" cellpadding="0" cellspacing="0" class="table table-search">
          <colgroup>
          <col width="114px">
          <col width="*">
          </colgroup>
          
			<tbody>
				<tr>
					<th>민원번호</th>
					<td>
						<input type="text" id="searchCondition1" name="searchCondition1" maxlength="10" value="${param.searchCondition1}"/>
					</td>
				</tr>
				<tr>
					<th>민원지행정동</th>
					<td>
						<select name="searchCondition2" id="searchCondition2">
							<option value="">전체</option>
							<c:forEach var="cmtList" items="${cmtAdarMaList}" varStatus="status">
								<option value="${cmtList.hjdCde }" ${param.searchCondition2 == cmtList.hjdCde ? "selected" : "" } >${cmtList.hjdNam}</option>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<th>민원구분</th>
					<td>
						<select name="searchCondition3" id="searchCondition3">
							<option value="">전체</option>
							<c:forEach var="aplCdeList" items="${aplCdeList}" varStatus="status">
								<option value="${aplCdeList.codeCode }" ${param.searchCondition3 == aplCdeList.codeCode ? "selected" : "" } >${aplCdeList.codeAlias}</option>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<th>접수일자[이상]</th>
					<td>
						<!-- css쪽으로 설정 이동 할것 -->
						<input type="text" class="datepicker"  name="searchCondition4" id="searchCondition4" value="${param.searchCondition4}"/>
					</td>
				</tr>
				<tr>
					<th>접수일자[이하]</th>
					<td>
						<input type="text" class="datepicker"  name="searchCondition5" id="searchCondition5" value="${param.searchCondition5}"/>
					</td>
				</tr>
				<tr>
					<th>처리일자[이상]</th>
					<td>
						<!-- css쪽으로 설정 이동 할것 -->
						<input type="text" class="datepicker"  name="searchCondition6" id="searchCondition6" value="${param.searchCondition6}"/>
					</td>
				</tr>
				<tr>
					<th>처리일자[이하]</th>
					<td>
						<input type="text" class="datepicker"  name="searchCondition7" id="searchCondition7" value="${param.searchCondition7}"/>
					</td>
				</tr>
				<tr>
					<th>처리상태</th>
					<td>
						<select name="searchCondition8" id="searchCondition8">
							<option value="">전체</option>
							<option value="1" ${param.searchCondition8 == 1 ? "selected" : "" }>처리중</option>
							<option value="2" ${param.searchCondition8 == 2 ? "selected" : "" }>처리완료</option>
						</select>
					</td>
				</tr>
				<tr>
					<th>-</th>
					<td>
						<input type="text" name="" id="" readonly class="disable" />
					</td>
				</tr>
				<tr>
					<th>-</th>
					<td>
						<input type="text" name="" id="" readonly class="disable" />
					</td>
				</tr>
				<tr>
					<th>-</th>
					<td>
						<input type="text" name="" id="" readonly class="disable" />
					</td>
				</tr>
			</tbody>
		</table>

      <div class="btn_group m-t-10 "  >
        <button type="button" id="btnReset" class="btn btn-search "   style="width:114px"><img src="/fms/images/icon_btn_reset.png" width="16" height="16" alt=""/> 초기화 </button>
        <button type="button" id="btnSearch" class="btn btn-search "  style="width:132px"><img src="/fms/images/icon_btn_search.png" width="16" height="16" alt=""/> 검색 </button>
      </div>
      
	</div>
	</div>
	<!-- /box_left -->


						
    <div class="list_box">
      <h2 ><span style="float:left">목록 | 상수공사 민원관리(<fmt:formatNumber value="${totCnt}" pattern="#,###" />건)</span>
        <div class="btn_group m-b-5 right "  >          
        </div>
      </h2>
						
	  <div class="list ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-striped table-list center " style="white-space: nowrap;">
						
			<thead>
				<tr>
					<th>민원번호</th>
					<th>민원지행정동</th>
					<th>민원구분</th>
					<th>접수일자</th>
					<th>처리일자</th>
					<th>처리상태</th>
					<th>민원인</th>
					<th>민원인 연락처</th>
				</tr>
			</thead>
			
			<tbody>
				<c:forEach var="list" items="${resultList}" varStatus="listIndex">																	
					<tr>	
						<td class="dtlPopup" style="cursor:pointer" rcvNum='${list.rcvNum}' wserSeq='${list.wserSeq}' >${list.rcvNum}</td>
						<td class="dtlPopup" style="cursor:pointer" rcvNum='${list.rcvNum}' wserSeq='${list.wserSeq}' >${mapAdarMa[list.aplHjd]}</td>
						<td class="dtlPopup" style="cursor:pointer" rcvNum='${list.rcvNum}' wserSeq='${list.wserSeq}' >${mapAplCde[list.aplCde]}</td>
						<td class="dtlPopup" style="cursor:pointer" rcvNum='${list.rcvNum}' wserSeq='${list.wserSeq}' >${list.rcvYmd}</td>
						<td class="dtlPopup" style="cursor:pointer" rcvNum='${list.rcvNum}' wserSeq='${list.wserSeq}' >${list.proYmd}</td>
						<td>
							<a href="javascript:;">
								<c:if test="${list.proCde == 1}">처리중</c:if>
								<c:if test="${list.proCde == 2}">처리완료</c:if>
							
						</td>
						<td class="dtlPopup" style="cursor:pointer" rcvNum='${list.rcvNum}' wserSeq='${list.wserSeq}' >${list.apmNam}</td>
						<td class="dtlPopup" style="cursor:pointer" rcvNum='${list.rcvNum}' wserSeq='${list.wserSeq}' >${list.apmTel}</td>
					</tr>
				</c:forEach>

				<c:if test="${empty resultList}">
					<tr>
						<td colspan="7" style="color:black;text-align:center;">검색 결과가 없습니다.</td>
					</tr>
				</c:if>
			</tbody>
		</table>
	</div>

      <div style="width:100%; text-align:center">
        <ul class="pagination pagination-sm  " >
			<ui:pagination paginationInfo = "${paginationInfo}"  type="image" jsFunction="fn_search" />
        </ul>
      </div>
		
	</div>
	<!-- /box_right -->


	</div>
</form:form>
</div>


</body>

</html>
