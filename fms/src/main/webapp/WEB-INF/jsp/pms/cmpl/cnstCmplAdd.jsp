<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<script type="text/javascript">
	var saveLink = '${ctxRoot}/pms/cmpl/cnstCmplSave.do';
	
	$(document).ready(function() {
		
		/*******************************************
		 * 화면초기화
		 *******************************************/
		gfn_init();
			
		 /*******************************************
		 * 이벤트 설정
		 *******************************************/
		var msg = '${msg}';

		if(msg) {
			gfn_alert(msg,"",function(){
				window.close();
			});
		}
		
		$ ('#durYmd').datepicker();
	    $ ('#durYmd').datepicker("option", "maxDate", $ ("#proYmd").val());
	    $ ('#durYmd').datepicker("option", "onClose", function ( selectedDate ) {
	        $ ("#proYmd").datepicker( "option", "minDate", selectedDate );
	    });
	 
	    $ ('#proYmd').datepicker();
	    $ ('#proYmd').datepicker("option", "minDate", $ ("#durYmd").val());
	    $ ('#proYmd').datepicker("option", "onClose", function ( selectedDate ) {
	        $ ("#durYmd").datepicker( "option", "maxDate", selectedDate );
	    });
		
		$('#cntNum').click(function () {
			window.open('${ctxRoot}/pms/popup/cnstMngPopup.do', "_blank", 'status=no, width=1100, height=540');
		});
		
		$('#btnSave').click(function() {
			
			if(!gfn_formValid("cnstCmplFrm")) return;
				
			gfn_saveFormCmm("cnstCmplFrm", "insertCmplWttWserMa", function(data){
				if(data.result){					
					gfn_alert("데이터가 저장되었습니다.","",function(){
						opener.fn_search();
						window.close();
					});
				}else{
					gfn_alert("데이터 저장에 실패하였습니다. : " + data.error,"E");
				}
			});
		});
	});	

	function setChildValue(name) {
		document.getElementById("cntNum").value = name;
	}
</script>
</head>

<body>	
	
	
<div id="wrap" class="wrap" > 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1>상수공사 민원관리</h1>
  </div>
  <!-- // header --> 
  
<form name="cnstCmplFrm" id="cnstCmplFrm" method="post">	
  <input type="hidden" id="wserSeq" name="wserSeq" value="${wserSeq}"/>
  	
  <!-- container -->
  <div id="container"  class="content"  > 
    
    <!-- list -->
    <div class="view_box">
      <h2 ><span style="float:left">민원 등록정보</span>
	      <div class="btn_group m-b-5 right "  >
             <button id="btnSave" class="btn btn-default " type="button" > 
             	<img src="/fms/images/icon_btn_save.png" width="16" height="16" alt=""/>저장
             </button>
          </div>
      </h2>
      <div class="view ">
		<table border="0" cellpadding="0" cellspacing="0"  class="table  table-view ">
			<colgroup>
	          <col width="114px">
	          <col width="*">
	          <col width="114px">
	          <col width="*">
	          <col width="114px">
	          <col width="*">
	          <col width="114px">
	          <col width="*">
			</colgroup>
			<tbody>
			<tr>
				<th class="req">민원접수번호</th>
				<td>
					<input type="text" id="rcvNum" name="rcvNum" value="${rcvNum}" class="reqVal"/>
				</td>
				<th class="req">민원인 성명</th>
				<td>
					<input type="text" id="apmNam" name="apmNam" class="reqVal"/>
				</td>
				<th class="req">민원인 전화번호</th>
				<td>
					<input type="text" id="apmTel" name="apmTel" class="reqVal"/>
				</td>
				<th class="req">공사번호</th>
				<td>
					<input type="text" id="cntNum" name="cntNum" class="reqVal"/>
				</td>
			</tr>
			<tr>				
				<th>민원구분</th>
				<td>
					<select id="aplCde" name="aplCde" style="width:100%;">
						<option value="">선택안함</option>
						<c:forEach var="aplCdeList" items="${aplCdeList}" varStatus="status">
							<option value="${aplCdeList.codeCode }" >${aplCdeList.codeAlias}</option>
						</c:forEach>
					</select>
				</td>
				<th>접수일자</th>
				<td>
					<input type="text" id="rcvYmd" name="rcvYmd" class="datepicker" />
				</td>
				<th>접수자명</th>
				<td>
					<input type="text" id="rcvNam" name="rcvNam" />
				</td>
				<th>처리기한일</th>
				<td>
					<input type="text" id="durYmd" name="durYmd" class="datepicker" />
				</td>
			</tr>
			<tr>
				<th>민원지행정동</th>
				<td>
					<select id="aplHjd" name="aplHjd" style="width:100%;">
						<option value="">전체</option>
						<c:forEach var="cmtList" items="${cmtAdarMaList}" varStatus="status">
						<option value="${cmtList.hjdCde }">${cmtList.hjdNam}</option>
					</c:forEach>
					</select>
				</td>
				<th>민원인주소</th>
				<td colspan="5">
					<input type="text" id="apmAdr" name="apmAdr" style="width:100%;"/>
				</td>							
			</tr>
			<tr>
				<th>민원지 위치 설명</th>
				<td colspan="7">
					<input type="text" id="aplAdr" name="aplAdr" style="width:100%;"/>
				</td>
			</tr>
			<tr>
				<th>민원내용</th>
				<td colspan="7">
					<input type="text" id="aplExp" name="aplExp" style="width:100%;"/>
				</td>
			</tr>	
			<tr>
				<th>처리상태</th>
				<td>
					<select id="proCde" name="proCde" style="width:100%;">
						<option value="">전체</option>
						<option value="1">처리중</option>
						<option value="2">처리완료</option>
					</select>
				</td>
				<th>처리완료일</th>
				<td>
					<input type="text" id="proYmd" name="proYmd" class="datepicker" />
				</td>
				<th>처리자명</th>
				<td>
					<input type="text" id="proNam" name="proNam" />
				</td>
				<th>-</th>
				<td>
					<input type="text" id="" name="" readonly class="disable"/>
				</td>
			</tr>
			<tr>
				<th>처리내용</th>
				<td colspan="7">
					<input type="text" id="proExp" name="proExp" style="width:100%;"/>
				</td>
			</tr>			
			</tbody>
		</table>
      </div>
    </div>
    <!-- //list -->
    
  </div>
  <!-- //container --> 

</form>

</div>
<!-- //UI Object -->

</body>
</html>
