<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>


<script type="text/javascript">	

var addLink = '${ctxRoot}/pms/cmpl/spwCnstAdd.do';
var popupX = (screen.availWidth-660)/2;
var popupY = (screen.availHeight-1130)/3;

$(document).ready(function() {
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();
	
	
	 /*******************************************
	 * 이벤트 설정
	 *******************************************/	
	
	$('#btnCrtPop').click(function () {
		window.open(addLink, "_blank", 'status=no, width=1100, height=340, left='+ popupX + ', top='+ popupY + ', resizable=no');
	});
	 
	$ ('#searchCondition4').datepicker();
    $ ('#searchCondition4').datepicker("option", "maxDate", $ ("#searchCondition5").val());
    $ ('#searchCondition4').datepicker("option", "onClose", function ( selectedDate ) {
        $ ("#searchCondition5").datepicker( "option", "minDate", selectedDate );
    });
 
    $ ('#searchCondition5').datepicker();
    $ ('#searchCondition5').datepicker("option", "minDate", $ ("#searchCondition4").val());
    $ ('#searchCondition5').datepicker("option", "onClose", function ( selectedDate ) {
        $ ("#searchCondition4").datepicker( "option", "maxDate", selectedDate );
    });
    
	$ ('#searchCondition6').datepicker();
    $ ('#searchCondition6').datepicker("option", "maxDate", $ ("#searchCondition7").val());
    $ ('#searchCondition6').datepicker("option", "onClose", function ( selectedDate ) {
        $ ("#searchCondition7").datepicker( "option", "minDate", selectedDate );
    });
 
    $ ('#searchCondition7').datepicker();
    $ ('#searchCondition7').datepicker("option", "minDate", $ ("#searchCondition6").val());
    $ ('#searchCondition7').datepicker("option", "onClose", function ( selectedDate ) {
        $ ("#searchCondition6").datepicker( "option", "maxDate", selectedDate );
    }); 		
		    
	$('#btnReset').click(function () {
           var frm = $('#suppMaListFrm');
           frm.find('input[type=text]').val('');
           frm.find('select').val('');
        });
	
	$('#btnSearch').click(function () {
		fn_search(1);
	});		
	
	$('#btnExcelDown').click(function() {
		var html = '';
		html += '<form id="excelForm" method="post">';
		html += '	<input type="hidden" name="searchCondition1" value= "' + $('#searchCondition1').val() + '" />';
		html += '	<input type="hidden" name="searchCondition2" value= "' + $('#searchCondition2').val() + '" />';
		html += '	<input type="hidden" name="searchCondition3" value= "' + $('#searchCondition3').val() + '" />';
		html += '	<input type="hidden" name="searchCondition4" value= "' + $('#searchCondition4').val() + '" />';
		html += '	<input type="hidden" name="searchCondition5" value= "' + $('#searchCondition5').val() + '" />';
		html += '	<input type="hidden" name="searchCondition6" value= "' + $('#searchCondition6').val() + '" />';
		html += '	<input type="hidden" name="searchCondition7" value= "' + $('#searchCondition7').val() + '" />';
		html += '	<input type="hidden" name="searchCondition8" value= "' + $('#searchCondition8').val() + '" />';
		html += '</form>';
		$('#wrap').append(html);			
		$('#excelForm').attr('action', '${ctxRoot}/pms/cmpl/supp/excel/download.do');
		$('#excelForm').submit();
	});
	
	//상세팝업
	$(".dtlPopup").click(function(event){

		var dtlLink = '${ctxRoot}/pms/cmpl/spwCnstDtl.do?rcvNum='+$(this).attr('rcvNum')+'&wserSeq='+$(this).attr('wserSeq');
		var centerPos = parseInt( $(screen).get(0).width ) / 2;
		var windowCenterPos = 858 / 2;
		var leftPos = centerPos - windowCenterPos;

		var _win = "";
		try{ opener.closeChild(); }catch(e){}
		_win = window.open(dtlLink, "child", "left="+leftPos+",top=100, width=1100, height=340, menubar=no, location=no, resizable=no, toolbar=no, status=no");
		_win.focus();
		try{ opener._winChild = _win; }catch(e){}
		
	});
});	

/* pagination 페이지 링크 function */
function fn_search(pageNo){
	if(gfn_isNull(pageNo)){
		pageNo = 1;
	}		
	document.getElementById("suppMaListFrm").pageIndex.value = pageNo;
   	document.getElementById("suppMaListFrm").submit();
}

function captureReturnKey(e) {
    if(e.keyCode==13 && e.srcElement.type != 'textarea'){
       	fn_search();
    }
}
</script>
	
	
<body>	

<div id="wrap" class="wrap" > 
  <div id="header" class="header"  >
    <h1 >급수공사 민원관리</h1>
  </div>
	
<form:form  commandName="searchVO" id="suppMaListFrm" name="suppMaListFrm" method="post" action="${ctxRoot}/pms/cmpl/spwCnstList.do">
	<input type="hidden" name="pageIndex" value="1"/>
	
  <!-- container -->
  <div id="container"  class="content"  > 
				
				
    <div class="seach_box" >
      <h2 >검색항목 </h2>
      <div>
        <table border="0" cellpadding="0" cellspacing="0" class="table table-search">
          <colgroup>
          <col width="114px">
          <col width="*">
          </colgroup>
          
			<tbody>
						<tr>
							<th>민원번호</th>
							<td>
								<input type="text" id="searchCondition1" name="searchCondition1" maxlength="10" value="${param.searchCondition1}" onkeypress="captureReturnKey(event)"/>
							</td>
						</tr>
						<tr>
							<th>민원지행정동</th>
							<td>
								<select name="searchCondition2" id="searchCondition2">
									<option value="">전체</option>
									<c:forEach var="cmtList" items="${cmtAdarMaList}" varStatus="status">
										<option value="${cmtList.hjdCde }" ${param.searchCondition2 == cmtList.hjdCde ? "selected" : "" } >${cmtList.hjdNam}</option>
									</c:forEach>
								</select>
							</td>
						</tr>
						<tr>
							<th>민원구분</th>
							<td>
								<select name="searchCondition3" id="searchCondition3">
									<option value="">전체</option>
									<c:forEach var="aplCdeList" items="${aplCdeList}" varStatus="status">
										<option value="${aplCdeList.codeCode }" ${param.searchCondition3 == aplCdeList.codeCode ? "selected" : "" } >${aplCdeList.codeAlias}</option>
									</c:forEach>
								</select>
							</td>
						</tr>
						<tr>
							<th>접수일자[이상]</th>
							<td>
								<!-- css쪽으로 설정 이동 할것 -->
								<input type="text" class="datepicker"  name="searchCondition4" id="searchCondition4" value="${param.searchCondition4}" onkeypress="captureReturnKey(event)"/>
							</td>
						</tr>
						<tr>
							<th>접수일자[이하]</th>
							<td>
								<input type="text" class="datepicker"  name="searchCondition5" id="searchCondition5" value="${param.searchCondition5}" onkeypress="captureReturnKey(event)"/>
							</td>
						</tr>
						<tr>
							<th>처리일자[이상]</th>
							<td>
								<!-- css쪽으로 설정 이동 할것 -->
								<input type="text" class="datepicker"  name="searchCondition6" id="searchCondition6" value="${param.searchCondition6}" onkeypress="captureReturnKey(event)"/>
							</td>
						</tr>
						<tr>
							<th>처리일자[이하]</th>
							<td>
								<input type="text" class="datepicker"  name="searchCondition7" id="searchCondition7" value="${param.searchCondition7}" onkeypress="captureReturnKey(event)"/>
							</td>
						</tr>
						<tr>
							<th>처리상태</th>
							<td>
								<select name="searchCondition8" id="searchCondition8">
									<option value="">전체</option>
									<option value="1" ${param.searchCondition8 == 1 ? "selected" : "" }>처리중</option>
									<option value="2" ${param.searchCondition8 == 2 ? "selected" : "" }>처리완료</option>
								</select>
							</td>
						</tr>
						<tr>
							<th>-</th>
							<td>
								<input type="text" name="" id="" readonly class="disable" />
							</td>
						</tr>
						<tr>
							<th>-</th>
							<td>
								<input type="text" name="" id="" readonly class="disable" />
							</td>
						</tr>
						<tr>
							<th>-</th>
							<td>
								<input type="text" name="" id="" readonly class="disable" />
							</td>
						</tr>
					</tbody>
				</table>

      <div class="btn_group m-t-10 "  >
        <button id="btnReset" class="btn btn-search "  type="button" style="width:114px"><img src="/fms/images/icon_btn_reset.png" width="16" height="16" alt=""/> 초기화 </button>
        <button id="btnSearch" class="btn btn-search " type="button" style="width:132px"><img src="/fms/images/icon_btn_search.png" width="16" height="16" alt=""/> 검색 </button>
      </div>
      
	</div>
	</div>
	<!-- /box_left -->




    <div class="list_box">
      <h2 ><span style="float:left">목록 | 급수공사 민원관리(<fmt:formatNumber value="${totCnt}" pattern="#,###" />건)</span>
        <div class="btn_group m-b-5 right "  >
          <button id="btnCrtPop" type="button" class="btn btn-default " > <img src="/fms/images/icon_btn_regist.png" width="16" height="16" alt=""/>등록 </button>
          <button id="btnExcelDown" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_download.png" width="16" height="16" alt=""/> 다운로드 </button>
        </div>
      </h2>
						
	  <div class="list ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-striped table-list center " style="white-space: nowrap;">

				<thead>
					<tr>
						<th>민원번호</th>
						<th>민원지행정동</th>
						<th>민원구분</th>
						<th>접수일자</th>
						<th>처리일자</th>
						<th>처리상태</th>
						<th>민원인</th>
						<th>민원인 연락처</th>
					</tr>
				</thead>
				
				<tbody>
					<c:forEach var="list" items="${resultList}" varStatus="listIndex">																	
						<tr>	
							<td class="dtlPopup" style="cursor:pointer" rcvNum='${list.rcvNum}' wserSeq='${list.wserSeq}' >${list.rcvNum}</td>
							<td class="dtlPopup" style="cursor:pointer" rcvNum='${list.rcvNum}' wserSeq='${list.wserSeq}' >${mapAdarMa[list.aplHjd]}</td>
							<td class="dtlPopup" style="cursor:pointer" rcvNum='${list.rcvNum}' wserSeq='${list.wserSeq}' >${mapAplCde[list.aplCde]}</td>
							<td class="dtlPopup" style="cursor:pointer" rcvNum='${list.rcvNum}' wserSeq='${list.wserSeq}' >${list.rcvYmd}</td>
							<td class="dtlPopup" style="cursor:pointer" rcvNum='${list.rcvNum}' wserSeq='${list.wserSeq}' >${list.proYmd}</td>
							<td class="dtlPopup" style="cursor:pointer" rcvNum='${list.rcvNum}' wserSeq='${list.wserSeq}' >
								<c:if test="${list.proCde == 1}">처리중</c:if>
								<c:if test="${list.proCde == 2}">처리완료</c:if>
							</td>
							<td class="dtlPopup" style="cursor:pointer" rcvNum='${list.rcvNum}' wserSeq='${list.wserSeq}' >${list.apmNam}</td>
							<td class="dtlPopup" style="cursor:pointer" rcvNum='${list.rcvNum}' wserSeq='${list.wserSeq}' >${list.apmTel}</td>
						</tr>
					</c:forEach>

					<c:if test="${empty resultList}">
						<tr>
							<td colspan="8" style="color:black;text-align:center;">검색 결과가 없습니다.</td>
						</tr>
					</c:if>
				</tbody>
			</table>
		</div>

      <div style="width:100%; text-align:center">
        <ul class="pagination pagination-sm  " >
			<ui:pagination paginationInfo = "${paginationInfo}"  type="image" jsFunction="fn_search" />
        </ul>
      </div>
		
	</div>
	<!-- /box_right -->

</div>
<!-- /layer01_box -->
</form:form>
	
	
</div>
<!-- /popup_layer01 -->

</body>

</html>
