<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>


<script type="text/javascript">

var addLink = '${ctxRoot}/pms/cmpl/lekSiteAdd.do';
var popupX = (screen.availWidth-660)/2;
var popupY = (screen.availHeight-1130)/3;

$(document).ready(function() {
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();
	
	
	 /*******************************************
	 * 이벤트 설정
	 *******************************************/
	 
	$('#btnCrtPop').click(function () {
		window.open(addLink, "_blank", 'status=no, width=1100, height=400, left='+ popupX + ', top='+ popupY + ', resizable=no');
	});
	 
	$ ('#searchCondition5').datepicker();
    $ ('#searchCondition5').datepicker("option", "maxDate", $ ("#searchCondition6").val());
    $ ('#searchCondition5').datepicker("option", "onClose", function ( selectedDate ) {
        $ ("#searchCondition6").datepicker( "option", "minDate", selectedDate );
    });
 
    $ ('#searchCondition6').datepicker();
    $ ('#searchCondition6').datepicker("option", "minDate", $ ("#searchCondition5").val());
    $ ('#searchCondition6').datepicker("option", "onClose", function ( selectedDate ) {
        $ ("#searchCondition5").datepicker( "option", "maxDate", selectedDate );
    });
    
    $ ('#searchCondition7').datepicker();
    $ ('#searchCondition7').datepicker("option", "maxDate", $ ("#searchCondition8").val());
    $ ('#searchCondition7').datepicker("option", "onClose", function ( selectedDate ) {
        $ ("#searchCondition8").datepicker( "option", "minDate", selectedDate );
    });
 
    $ ('#searchCondition8').datepicker();
    $ ('#searchCondition8').datepicker("option", "minDate", $ ("#searchCondition7").val());
    $ ('#searchCondition8').datepicker("option", "onClose", function ( selectedDate ) {
        $ ("#searchCondition7").datepicker( "option", "maxDate", selectedDate );
    });
    
	$('#btnReset').click(function () {
		var frm = $('#leakPsListfrm');
		frm.find('input[type=text]').val('');
		frm.find('select').val('');
	});
	
	$('#btnSearch').click(function () {
		fn_search(1);
	});
	
	$('#btnExcelDown').click(function() {
		var title = '누수지점 목록'; 
		var sqlId = 'selectWtlLeakPsList';
		gfn_ExcelDown(title, sqlId);
	});	
	
	//상세팝업
	$(".dtlPopup").click(function(event){

		var dtlLink = '${ctxRoot}/pms/cmpl/lekSiteDtl.do?ftrCde='+$(this).attr('ftrCde')+'&ftrIdn='+$(this).attr('ftrIdn');
		var centerPos = parseInt( $(screen).get(0).width ) / 2;
		var windowCenterPos = 858 / 2;
		var leftPos = centerPos - windowCenterPos;

		var _win = "";
		try{ opener.closeChild(); }catch(e){}
		_win = window.open(dtlLink, "child", "left="+leftPos+",top=100, width=1100, height=600, menubar=no, location=no, resizable=no, toolbar=no, status=no");
		_win.focus();
		try{ opener._winChild = _win; }catch(e){}
		
	});
	
	
	//위치이동
	$(".dtlMap").click(function(event){
		
		event.cancelBubble=true;
				
		var _content = opener.parent.document.getElementById('content').contentWindow;
		// 수정함
		var msg = _content.moveMap('WTL_CLPI_LM',$(this).attr('ogrFid'));

		if(!gfn_isNull(msg)){
			gfn_alert(msg);
		};
	});
	
	
});	

/* pagination 페이지 링크 function */
function fn_search(pageNo){
	if(gfn_isNull(pageNo)){
		pageNo = 1;
	}	
	document.getElementById("leakPsListfrm").pageIndex.value = pageNo;
   	document.getElementById("leakPsListfrm").submit();
}

function captureReturnKey(e) {
    if(e.keyCode==13 && e.srcElement.type != 'textarea'){
       	fn_search();
    }
}

</script>
	
	
<body>
	
<div id="wrap" class="wrap" > 
  <div id="header" class="header"  >
    <h1 >누수지점 관리</h1>
  </div>
	
<form:form  commandName="searchVO" id="leakPsListfrm" name="leakPsListfrm" method="post" action="${ctxRoot}/pms/cmpl/lekSiteList.do">
	<input type="hidden" name="pageIndex" value="1"/>

	<!-- 편집 호출 유무 : yhhwang -->
	<c:if test="${fn:length(gisLayerNm) > 1}">
	<input type="hidden" name="gisLayerNm" value="WTL_LEAK_PS"/>
	</c:if>
	
  <!-- container -->
  <div id="container"  class="content"  > 
				
				
    <div class="seach_box" >
      <h2 >검색항목 </h2>
      <div>
        <table border="0" cellpadding="0" cellspacing="0" class="table table-search">
          <colgroup>
          <col width="114px">
          <col width="*">
          </colgroup>
          
			<tbody>

						<tr>
							<th>관리번호</th>
							<td>
								<input type="text" id="searchCondition1" name="searchCondition1" maxlength="10" value="${param.searchCondition1}" onchange="onlyNumber(this);" onkeyup="onlyNumber(this);" onkeypress="captureReturnKey(event)"/>
							</td>
						</tr>
						<tr>
							<th>도엽번호</th>
							<td>
								<input type="text" id="searchCondition2" name="searchCondition2" maxlength="10" value="${param.searchCondition2}" onkeypress="captureReturnKey(event)"/>
							</td>
						</tr>
						<tr>
							<th>행정동</th>
							<td>
								<select name="searchCondition3" id="searchCondition3">
									<option value="">전체</option>
									<c:forEach var="cmtList" items="${cmtAdarMaList}" varStatus="status">
										<option value="${cmtList.hjdCde }" ${param.searchCondition3 == cmtList.hjdCde ? "selected" : "" } >${cmtList.hjdNam}</option>
									</c:forEach>
								</select>
							</td>
						</tr>
						<tr>
							<th>관로관리번호</th>
							<td>
								<input type="text" id="searchCondition4" name="searchCondition4" maxlength="50"  value="${param.searchCondition4}" onchange="onlyNumber(this);" onkeyup="onlyNumber(this);" onkeypress="captureReturnKey(event)"/>
							</td>
						</tr>
						<tr>
							<th>누수일자[이상]</th>
							<td>
								<!-- css쪽으로 설정 이동 할것 -->
								<input type="text" class="datepicker"  name="searchCondition5" id="searchCondition5" value="${param.searchCondition5}" onkeypress="captureReturnKey(event)"/>
							</td>
						</tr>
						<tr>
							<th>누수일자[이하]</th>
							<td>
								<input type="text" class="datepicker"  name="searchCondition6" id="searchCondition6" value="${param.searchCondition6}" onkeypress="captureReturnKey(event)"/>
							</td>
						</tr>
						<tr>
							<th>복구일자[이상]</th>
							<td>
								<!-- css쪽으로 설정 이동 할것 -->
								<input type="text" class="datepicker"  name="searchCondition7" id="searchCondition7" value="${param.searchCondition7}" onkeypress="captureReturnKey(event)"/>
							</td>
						</tr>
						<tr>
							<th>복구일자[이하]</th>
							<td>
								<input type="text" class="datepicker"  name="searchCondition8" id="searchCondition8" value="${param.searchCondition8}" onkeypress="captureReturnKey(event)"/>
							</td>
						</tr>
						<tr>
							<th>누수부위</th>
							<td>
								<select name="searchCondition9" id="searchCondition9">
									<option value="">전체</option>
									<c:forEach var="lepCodeList" items="${lepCodeList}" varStatus="status">
										<option value="${lepCodeList.codeCode }" ${param.searchCondition9 == lepCodeList.codeCode ? "selected" : "" }>${lepCodeList.codeAlias}</option>
									</c:forEach>	
								</select>
							</td>
						</tr>
						<tr>
							<th>누수원인</th>
							<td>
								<select name="searchCondition10" id="searchCondition10">
									<option value="">전체</option>
									<c:forEach var="lrsCodeList" items="${lrsCodeList}" varStatus="status">
										<option value="${lrsCodeList.codeCode }" ${param.searchCondition10 == lrsCodeList.codeCode ? "selected" : "" } >${lrsCodeList.codeAlias}</option>
									</c:forEach>
								</select>
							</td>
						</tr>
						<tr>
							<th>민원접수번호</th>
							<td>	
								<input type="text" id="searchCondition11" name="searchCondition11" value="${param.searchCondition11}" onkeypress="captureReturnKey(event)"/>																					
							</td>
						</tr>
					</tbody>
				</table>

      <div class="btn_group m-t-10 "  >
        <button id="btnReset" class="btn btn-search "  type="button" style="width:114px"><img src="/fms/images/icon_btn_reset.png" width="16" height="16" alt=""/> 초기화 </button>
        <button id="btnSearch" class="btn btn-search " type="button" style="width:132px"><img src="/fms/images/icon_btn_search.png" width="16" height="16" alt=""/> 검색 </button>
      </div>
      
	</div>
	</div>
	<!-- /box_left -->



				
    <div class="list_box">
      <h2 ><span style="float:left">목록 | 누수지점(${totCnt}건)</span>
        <div class="btn_group m-b-5 right "  >
          <button id="btnCrtPop" type="button" class="btn btn-default " > <img src="/fms/images/icon_btn_regist.png" width="16" height="16" alt=""/>등록 </button>
          <button id="btnExcelDown" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_download.png" width="16" height="16" alt=""/> 다운로드 </button>
        </div>
      </h2>
						
	  <div class="list ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-striped table-list center " style="white-space: nowrap;">
				<thead>
					<tr id="excel_head">
						<th tit="">위치</th>
						<th tit="ftrNam">지형지물 </th>
						<th tit="ftrIdn">관리번호 </th>
						<th tit="shtNum">도엽번호</th>
						<th tit="hjdNam">행정동</th>
						<th tit="pipIdn"> 관로관리번호</th>
						<th tit="lekYmd"> 누수일자</th>
						<th tit="repYmd"> 복구일자</th>
						<th tit="lepCdeNam"> 누수부위</th>
						<th tit="lrsCdeNam"> 누수원인</th>
						<th tit="rcvNum"> 민원접수번호 </th>
					</tr>
				</thead>
				
				<tbody>
					<c:forEach var="list" items="${resultList}" varStatus="listIndex">						
						<!-- tr class="viewPopup ${listIndex.index % 2 == 0 ? '' : 'tr_back01' } popup-link" href="${ctxRoot}/pms/cmpl/lekSiteDtl.do?ftrCde=${list.ftrCde}&ftrIdn=${list.ftrIdn}" popup-data="width=1100, height=600"  -->
						<tr>
							<c:choose>
								<c:when test="${list.isGeometry eq 'Y'}">
									<td class="dtlMap"   style="cursor:pointer" ogrFid='${list.ogrFid}'><img src="${rscRoot}/dist/images/search.png" alt="위치이동" /></td>
								</c:when>
								<c:otherwise>
									<td>&nbsp; &nbsp;</td>
								</c:otherwise>
							</c:choose>
							<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' >${mapFtrcMa[list.ftrCde]}</td>
							<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' >${list.ftrIdn}</td>
							<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' >${list.shtNum}</td>
							<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' >${mapAdarMa[list.hjdCde]}</td>
							<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' >${list.pipIdn}</td>
							<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' >${list.lekYmd}</td>
							<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' >${list.repYmd}</td>
							<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' >${mapLepCde[list.lepCde]}</td>
							<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' >${mapLrsCde[list.lrsCde]}</td>
							<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' >${list.rcvNum} </td>
						</tr>
					</c:forEach>

					<c:if test="${empty resultList}">
						<tr>
							<td colspan="11" style="color:black;text-align:center;">검색 결과가 없습니다.</td>
						</tr>
					</c:if>
				</tbody>
			</table>
		</div>

      <div style="width:100%; text-align:center">
        <ul class="pagination pagination-sm  " >
			<ui:pagination paginationInfo = "${paginationInfo}"  type="image" jsFunction="fn_search" />
        </ul>
      </div>
		
	</div>
	<!-- /box_right -->

	</div>
	<!-- /layer01_box -->
</form:form>

</div>
<!-- /popup_layer01 -->


</body>

</html>
