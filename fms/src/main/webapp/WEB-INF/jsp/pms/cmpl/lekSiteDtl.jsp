<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<style>
	.item{
		width:250px;
		height:180px;
	}
</style>

<script type="text/javascript">
var popupX = (screen.availWidth-660)/2;
var popupY = (screen.availHeight-1130)/3;

$(document).ready(function() {
		
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();
		
	
	//사진정보 가져오기
	fn_getPhoto('${filSeq}');
	
	var msg = '${msg}';
		
	if(msg) {
		gfn_alert(msg,"",function(){				
			window.close();
		});		
	}
	
	 /*******************************************
	 * 이벤트 설정
	 *******************************************/
	// 저장
	$('#btnSave').click(function() {
		
		if(!gfn_formValid("lekSiteFrm")) return;
		
		gfn_confirm('저장하시겠습니까?',function(ret){			
			if(!ret) return;
			
			gfn_saveFormCmm("lekSiteFrm", "updateWtlLeakPs", function(data){
				if(data.result){
					gfn_alert("데이터가 저장되었습니다.","",function(){
						opener.fn_search();
						fn_reload();
					});
				}else{
					gfn_alert("데이터 저장에 실패하였습니다. : " + data.error,"E");
				}
			});
			
		});
		
	});

	 
	// 삭제
	$('#btnDel').click(function() {
		// 자식데이터 체크
		gfn_selectList({sqlId: 'selectFileMapList',
						ftrCde:'${resultVO.ftrCde}', ftrIdn:'${resultVO.ftrIdn}', bizId:'${resultVO.ftrCde}${resultVO.ftrIdn}'} , function(ret){
							
			if(ret.data != null && ret.data.length > 0){
				gfn_alert("첨부내용이 있습니다.","W");
				return false;
			}
			
			//삭제처리			
			gfn_confirm("삭제하시겠습니까?",function(ret){
				if(!ret) return;
				gfn_saveFormCmm("lekSiteFrm", "deleteWtlLeakPs", function(data){
					if(data.result){
						gfn_alert("데이터가 삭제되었습니다.","",function(){						
							opener.fn_search();
							window.close();
						});
					}else{
						gfn_alert("데이터 삭제가 실패하였습니다. : " + data.error,"E");
					}
				});
			});	
		});
	});
	
	
	//대상시설물 선택팝업
	$("#pipIdn").click(function(){
		
		fn_SelFtrPop();
	});		
	
	
	// 사진첨부등록
	$('#btnAddFile').click(function () {
		var drawingFilSeq = '${filSeq}';
		window.open('${ctxRoot}/pms/file/lnkFileDtl.do?filSeq=' + drawingFilSeq + '&bizId=${resultVO.ftrCde}${resultVO.ftrIdn}&grpTyp=${F_DWG}', '누수사진', 'width=570, height=450, left='+ popupX + ', top='+ popupY + ', status=no, resizable=no');
	});
		
	
	$('#rcvNum').click(function () {
		if ($('#rcvNum').val() != '') {

			gfn_confirm("상수공사 민원접수번호를 변경하시겠습니까?", function(ret){
				if(!ret) return false;
				
				window.open('${ctxRoot}/pms/popup/cnstCmplPopup.do', "_blank"
						, 'status=no, width=1100, height=540, left='+ popupX + ', top='+ popupY +', resizable=no');
			});
		}else{
			window.open('${ctxRoot}/pms/popup/cnstCmplPopup.do', "_blank"
					, 'status=no, width=1100, height=540, left='+ popupX + ', top='+ popupY +', resizable=no');	
		}		
	});
		
});	//ondocument ready
	
	
	
//시설물코드 선택팝업
var fn_SelFtrPop = function(){
	
	window.open('${ctxRoot}/pms/link/lnkFtrList.do', "ftrSelList", 'status=no, width=1100, height=540 , left='+ popupX + ', top='+ popupY + ', screenX='+ popupX + ', screenY= '+ popupY);
}
//시설물팝업 선택 콜백
var callback_selFtr = function(_ftrCde, _ftrIdn, _ftrNam, _hjdCde, _hjdNam){
	$("#pipIdn").val(_ftrIdn);
	$("#pipCde").val(_ftrCde);
	$("#pipNam").val(_ftrNam);
}


//화면갱신
var fn_reload = function(){
	location.reload();
}



//점검결과 선택 후 사진목록 갱신	
var fn_getPhoto = function(_filSeq){

	// 1.사진정보세팅
	try{
		//점검결과 선택row 사진리스트
		gfn_selectList({sqlId: 'selectFileDtlList', filSeq: _filSeq}, function(ret){
			file_list = ret.data;

			var html2 = "";
			$("#owl-chkPhoto > div").remove();
			
			$.each(file_list, function(idx, map){
				
				//파일갤러리 만들기
				html2 += '<div class="item"><img class="galery" src="${ctxRoot}/pms/file/downloadImg.do?seq='+map.seq+'&filSeq='+map.filSeq+'"/></div>';
			})
			$("#owl-chkPhoto").append(html2);
			
			//갤러리초기화
			 $("#owl-chkPhoto").owlCarousel('destroy'); 
 			 $("#owl-chkPhoto").owlCarousel({
 			      items : 4, //10 items above 1000px browser width
 			      itemsDesktop : [1000,5], //5 items between 1000px and 901px
 			      itemsDesktopSmall : [900,3], // betweem 900px and 601px
 			      itemsTablet: [600,2], //2 items between 600 and 0
 			      itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
				  autoplay:true,
			  });  
			
 			 
 			// 갤러리 no image 세팅
 		    $(".galery").on("error", function(){
 		        $(this).attr('src', '${ctxRoot}/images/img_noimage.png');
 		    });	
 			 
		});
	}catch(e){
		console.log("사진없음");
	}
	
}		

function setRcvNumChildValue(name) {
	document.getElementById("rcvNum").value = name;			
}

</script>

</head>

<body>

<div id="wrap" class="wrap"> 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1>누수지점 상세정보</h1>
  </div>
  <!-- // header --> 
  
<form name="lekSiteFrm" id="lekSiteFrm" method="post">
		<input type="hidden" id="ogrFid" name="ogrFid" value="${resultVO.ogrFid }"/>

  <!-- container -->
  <div id="container"  class="content"  > 
    
    <!-- list -->
    <div class="view_box">
      <h2 ><span style="float:left">일반정보</span>
        <div class="btn_group m-b-5 right">
          <button id="btnDel" class="btn btn-default " type="button" > <img src="/fms/images/icon_btn_delete.png" width="16" height="16" alt="삭제"/>삭제</button>
          <button id="btnSave" class="btn btn-default " type="button" > <img src="/fms/images/icon_btn_save.png" width="16" height="16" alt="저장"/>저장</button>
        </div>
      </h2>
      <div class="view">
		<table border="0" cellpadding="0" cellspacing="0"  class="table  table-view ">
			<colgroup>
	          <col width="114px">
	          <col width="*">
	          <col width="114px">
	          <col width="*">
	          <col width="114px">
	          <col width="*">
			</colgroup>
			<tbody>
				<tr>
					<th class="req">관리번호</th>
					<td>
						<input type="text" id="ftrIdn" name="ftrIdn" value="${resultVO.ftrIdn }" readonly class="disable reqVal"/>
					</td>
					<th  class="req">지형지물</th>
					<td>
						<select id="ftrCde" name="ftrCde" class="disable reqVal">
							<option value="">선택안함</option>
							<c:forEach var="cmtFtrcMaList" items="${cmtFtrcMaList}" varStatus="status">
								<option value="${cmtFtrcMaList.ftrCde }" ${resultVO.ftrCde == cmtFtrcMaList.ftrCde ? "selected" : "" }>${cmtFtrcMaList.ftrNam}</option>
							</c:forEach>
						</select>
					</td>
					<th class="req">행정동</th>
					<td>
						<select name="hjdCde" id="hjdCde"  class="reqVal">
							<option value="">선택안함</option>
							<c:forEach var="cmtList" items="${cmtAdarMaList}" varStatus="status">
								<option value="${cmtList.hjdCde }" ${resultVO.hjdCde == cmtList.hjdCde ? "selected" : "" }>${cmtList.hjdNam}</option>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<th class="req">민원접수번호</th>
					<td>
						<input type="text" id="rcvNum" name="rcvNum" value="${resultVO.rcvNum }" class="reqVal" readOnly/>
					</td>
					<th class="req">관로 관리번호</th>
					<td>
						<input type="text" id="pipIdn" name="pipIdn" value="${resultVO.pipIdn }"  class="reqVal"/>
					</td>
					<th>관로 지형지물</th>
					<td>
						<input type="hidden" id="pipCde" name="pipCde" value="${resultVO.pipCde }" />
						<input type="text" id="pipNam"  readonly class="disable" value="${mapFtrcMa[resultVO.pipCde]}" />
					</td>
				</tr>
				<tr>
					<th>도엽번호</th>
					<td>
						<input type="text" id="shtNum" name="shtNum" value="${resultVO.shtNum }" />
					</td>
					<th>관재질</th>
					<td>
						<select id="mopCde" name="mopCde">
							<option value="">선택안함</option>
							<c:forEach var="mopCdeList" items="${mopCdeList}" varStatus="status">
								<option value="${mopCdeList.codeCode }" ${resultVO.mopCde == mopCdeList.codeCode ? "selected" : "" }>${mopCdeList.codeAlias}</option>
							</c:forEach>
						</select>
					</td>
					<th class="num2">관경</th>
					<td>
						<input type="text" id="pipDip" name="pipDip" value="${resultVO.pipDip }" class="numVal2"/>
					</td>
				</tr>
				<tr>
					<th>누수부위</th>
					<td>
						<select name="lepCde" id="lepCde">
							<option value="">선택안함</option>
							<c:forEach var="lepCodeList" items="${lepCodeList}" varStatus="status">
								<option value="${lepCodeList.codeCode }" ${resultVO.lepCde == lepCodeList.codeCode ? "selected" : "" }>${lepCodeList.codeAlias}</option>
							</c:forEach>	
						</select>
					</td>
					<th>누수원인</th>
					<td>
						<select name="lrsCde" id="lrsCde">
							<option value="">선택안함</option>
							<c:forEach var="lrsCodeList" items="${lrsCodeList}" varStatus="status">
								<option value="${lrsCodeList.codeCode }" ${resultVO.lrsCde == lrsCodeList.codeCode ? "selected" : "" }>${lrsCodeList.codeAlias}</option>
							</c:forEach>
						</select>
					</td>
					<th>누수일자</th>
					<td>
						<input type="text" id="lekYmd" name="lekYmd" class="datepicker" value="${resultVO.lekYmd }" />
					</td>
				</tr>
				<tr>
					<th>소요자재내역</th>
					<td>
						<input type="text" id="matDes" name="matDes" value="${resultVO.matDes} " />
					</td>
					<th>누수복구자명</th>
					<td>
						<input type="text" id="repNam" name="repNam" value="${resultVO.repNam }" />
					</td>
					<th>복구일자</th>
					<td>
						<input type="text" id="repYmd" name="repYmd" class="datepicker" value="${resultVO.repYmd }"/>
					</td>
				</tr>
				<tr>
					<th>누수현황</th>
					<td>
						<input type="text" id="lekExp" name="lekExp" value="${resultVO.lekExp }" />
					</td>
					<th>누수위치설명</th>
					<td>
						<input type="text" id="lekLoc" name="lekLoc" value="${resultVO.lekLoc }" />
					</td>
					<th>대장초기화여부</th>
					<td>
						<input type="text" id="sysChk" name="sysChk" value="${resultVO.sysChk }"/>
					</td>
				</tr>
				<tr>
					<th>누수복구내용</th>
					<td colspan="5">
						<input type="text" id="repExp" name="repExp" value="${resultVO.repExp }" style="width: 866px;"/>
					</td>
				</tr>
			</tbody>
		</table>
      </div>
    
    </div>
    <!-- //list --> 

	<br>
    <div class="view_box">
      <h2 ><span style="float:left">누수점검사진</span>
        <div class="btn_group m-b-5 right">
          <button id="btnAddFile" class="btn btn-default " type="button" > <img src="/fms/images/icon_btn_add.png" width="16" height="16" alt="사진"/>사진첨부</button>
        </div>
      </h2>
      
	  <div class="view" style="height:170px; border: 1px solid #96beac;">

			<!-- 갤러리영역 -->
			<div id="owl-chkPhoto" class="owl-carousel owl-theme">
			</div>    
	  
	  </div>      
    </div>




  </div>
  <!-- //container --> 
  
</form>
</div>
<!-- //UI Object -->

</body>
</html>

