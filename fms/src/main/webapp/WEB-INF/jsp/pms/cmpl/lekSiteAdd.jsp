<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<script type="text/javascript">
var popupX = (screen.availWidth-660)/2;
var popupY= (screen.availHeight-1130)/3;

$(document).ready(function() {
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();
		
	var msg = '${msg}';
		
	if(msg) {
		gfn_alert(msg,"",function(){				
			window.close();
		});		
	}	
	
	 /*******************************************
	 * 이벤트 설정
	 *******************************************/
	$('#btnSave').click(function() {
		if(!gfn_formValid("lekSiteFrm")) return;
		
		gfn_confirm('저장하시겠습니까?',function(ret){			
			if(!ret) return;
			
			gfn_saveFormCmm("lekSiteFrm", "insertWtlLeakPs", function(data){
				if(data.result){
					gfn_alert("데이터가 저장되었습니다.","",function(){
						opener.fn_search();
						window.close();
					});
				}else{
					gfn_alert("데이터 저장에 실패하였습니다. : " + data.error,"E");
				}
			});
			
		});		
	});
	 
	 
	//대상시설물 선택팝업
	$("#pipIdn").click(function(){		
		fn_SelFtrPop();
	});		
	
	$('#rcvNum').click(function () {
		if ($('#rcvNum').val() != '') {

			gfn_confirm("상수공사 민원접수번호를 변경하시겠습니까?", function(ret){
				if(!ret) return false;
				
				window.open('${ctxRoot}/pms/popup/cnstCmplPopup.do', "_blank"
						, 'status=no, width=1100, height=540, left='+ popupX + ', top='+ popupY +', resizable=no');
			});
		}else{
			window.open('${ctxRoot}/pms/popup/cnstCmplPopup.do', "_blank"
					, 'status=no, width=1100, height=540, left='+ popupX + ', top='+ popupY +', resizable=no');	
		}		
	});
	 
});	//ondocument ready


//시설물코드 선택팝업
var fn_SelFtrPop = function(){
	
	window.open('${ctxRoot}/pms/link/lnkFtrList.do', "ftrSelList", 'status=no, width=1100, height=540 , left='+ popupX + ', top='+ popupY + ', screenX='+ popupX + ', screenY= '+ popupY);
}
//시설물팝업 선택 콜백
var callback_selFtr = function(_ftrCde, _ftrIdn, _ftrNam, _hjdCde, _hjdNam){
	$("#pipIdn").val(_ftrIdn);
	$("#pipCde").val(_ftrCde);
	$("#pipNam").val(_ftrNam);
}
	
function setRcvNumChildValue(name) {
	document.getElementById("rcvNum").value = name;			
}
</script>

</head>

<body>

<div id="wrap" class="wrap"> 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1>누수지점 등록정보</h1>
  </div>
  <!-- // header --> 
  
<form name="lekSiteFrm" id="lekSiteFrm" method="post">

  <!-- container -->
  <div id="container"  class="content"  > 
    
    <!-- list -->
    <div class="view_box">
      <h2 ><span style="float:left">일반정보</span>
        <div class="btn_group m-b-5 right">
          <button id="btnSave" class="btn btn-default " type="button" > <img src="/fms/images/icon_btn_save.png" width="16" height="16" alt="저장"/>저장</button>
        </div>
      </h2>
      <div class="view">
		<table border="0" cellpadding="0" cellspacing="0"  class="table  table-view ">
			<colgroup>
	          <col width="114px">
	          <col width="*">
	          <col width="114px">
	          <col width="*">
	          <col width="114px">
	          <col width="*">
			</colgroup>
			<tbody>
				<tr>
					<th class="req">관리번호</th>
					<td>
						<input type="text" id="ftrIdn" name="ftrIdn" value="${ftrIdnVal}"  class="disable reqVal"/>
					</td>
					<th class="req">지형지물</th>
					<td>
						<select id="ftrCde" name="ftrCde"  class="reqVal">
							<option value="">선택안함</option>
							<c:forEach var="cmtFtrcMaList" items="${cmtFtrcMaList}" varStatus="status">
								<option value="${cmtFtrcMaList.ftrCde }">${cmtFtrcMaList.ftrNam}</option>
							</c:forEach>
						</select>
					</td>
					<th class="req">행정동</th>
					<td>
						<select name="hjdCde" id="hjdCde" class="reqVal">
							<option value="">선택안함</option>
							<c:forEach var="cmtList" items="${cmtAdarMaList}" varStatus="status">
								<option value="${cmtList.hjdCde }" >${cmtList.hjdNam}</option>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<th class="req">민원접수번호</th>
					<td>
						<input type="text" id="rcvNum" name="rcvNum" class="reqVal" readOnly />
						<input type="hidden" id="wserSeq" name="wserSeq" />
					</td>
					<th class="req">관로 관리번호</th>
					<td>
						<input type="text" id="pipIdn" name="pipIdn" class="reqVal"/>
					</td>
					<th>관로 지형지물</th>
					<td>
						<input type="hidden" id="pipCde" name="pipCde" />
						<input type="text" id="pipNam"  readonly class="disable" />
					</td>
				</tr>
				<tr>
					<th>도엽번호</th>
					<td>
						<input type="text" id="shtNum" name="shtNum" />
					</td>
					<th>관재질</th>
					<td>
						<select id="mopCde" name="mopCde">
							<option value="">선택안함</option>
							<c:forEach var="mopCdeList" items="${mopCdeList}" varStatus="status">
								<option value="${mopCdeList.codeCode }" >${mopCdeList.codeAlias}</option>
							</c:forEach>
						</select>
					</td>
					<th class="num2">관경</th>
					<td>
						<input type="text" id="pipDip" name="pipDip"  class="numVal2"/>
					</td>
				</tr>
				<tr>
					<th>누수부위</th>
					<td>
						<select name="lepCde" id="lepCde">
							<option value="">선택안함</option>
							<c:forEach var="lepCodeList" items="${lepCodeList}" varStatus="status">
								<option value="${lepCodeList.codeCode }" >${lepCodeList.codeAlias}</option>
							</c:forEach>	
						</select>
					</td>
					<th>누수원인</th>
					<td>
						<select name="lrsCde" id="lrsCde">
							<option value="">선택안함</option>
							<c:forEach var="lrsCodeList" items="${lrsCodeList}" varStatus="status">
								<option value="${lrsCodeList.codeCode }" >${lrsCodeList.codeAlias}</option>
							</c:forEach>
						</select>
					</td>
					<th>누수일자</th>
					<td>
						<input type="text" id="lekYmd" name="lekYmd" class="datepicker" />
					</td>
				</tr>
				<tr>
					<th>소요자재내역</th>
					<td>
						<input type="text" id="metDes" name="metDes" />
					</td>
					<th>누수복구자명</th>
					<td>
						<input type="text" id="repNam" name="repNam" />
					</td>
					<th>복구일자</th>
					<td>
						<input type="text" id="repYmd" name="repYmd" class="datepicker" />
					</td>
				</tr>
				<tr>
					<th>누수현황</th>
					<td>
						<input type="text" id="lekExp" name="lekExp" />
					</td>
					<th>누수위치설명</th>
					<td>
						<input type="text" id="lekLoc" name="lekLoc"  />
					</td>
					<th>대장초기화여부</th>
					<td>
						<input type="text" id="sysChk" name="sysChk" />
					</td>
				</tr>
				<tr>
					<th>누수복구내용</th>
					<td colspan="5">
						<input type="text" id="repExp" name="repExp" style="width:866px;"/>
					</td>
				</tr>
			</tbody>
		</table>
      </div>
    </div>
    <!-- //list --> 


  </div>
  <!-- //container --> 
  
</form>
</div>
<!-- //UI Object -->

</body>
</html>

