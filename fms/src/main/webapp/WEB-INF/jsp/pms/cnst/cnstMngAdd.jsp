<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<script type="text/javascript">


$(document).ready(function () {
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();
	
	setDatepicker();
	$(document).on('focus',".datepicker", setDatepicker);
	
	
	 /*******************************************
	 * 이벤트 설정
	 *******************************************/
	
	/// 인포탭선택이벤트
	$(".tab-info>li>a").click(function(){
		//탭타이틀 처리
		$(".tab-info>li>a").not(this).removeClass("active");
		if($(this).not(":visible")){
			$(this).addClass("active");	
		}
		
		//탭본문처리
		$(".tab-view").removeClass("active");
		$("#"+$(this).attr("abbr")).addClass("active");
	});
	
	
	// 신규저장
	$('#btnSave').click(function () {
		
	    if(!gfn_formValid("frm"))	return;

	    if ($('#cntNum').val().length < 6) {
			gfn_alert("공사번호는 최소 6자 이상 10자 이하로 입력하세요.","",function(){
				$('#cntNum').focus();	
			});	
			return false;
		}
		
		
		//공사번호 중복체크
		gfn_selectList({sqlId: 'selectWttConsMa', cntNum: $("#cntNum").val()}, function(ret){
			if(ret.data != null && ret.data.length > 0){
				gfn_alert("공사번호가 중복되었습니다.","W",function(){
					$("#cntNum").select();
				});
				return false;
			}
			
			//저장처리			
		    gfn_confirm("저장하시겠습니까?", function(ret){
				if(!ret) return;
				
				gfn_saveFormCmm("frm", "insertWttConsMa", function(data){
					if(data.result){
						gfn_alert("데이터가 저장되었습니다.","",function(){
							self.close();
							opener.fn_search();
						});
					}else{
						gfn_alert("데이터 저장에 실패하였습니다. : " + data.error,"E");
					}
				});	
		    });
		});	
	
		
	});//click
	
	
	
});








</script>
</head>
<style>
.num_style {text-align: right;}
</style>

<body>


<div id="wrap" class="wrap" > 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1 >상수공사 대장 등록정보</h1>
  </div>

	<form  name="frm" id="frm" method="post">
		<%-- <input type="hidden" name="cntNum" id="cntNum" value="${cntNum }"/> --%>
		
		
  <!-- container -->
  <div id="container"  class="content"  > 
    
    <!-- list -->
    <div class="view_box">
      <h2 ><span style="float:left">일반정보</span>
        <div class="btn_group m-b-5 right "  >
          <button id="btnSave" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_save.png" width="16" height="16" alt=""/> 저장 </button>
        </div>
      </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
          <colgroup>
          <col width="114px">
          <col width="154px">
          <col width="114px">
          <col width="154px">
          <col width="114px">
          <col width="153px">
          <col width="114px">
          <col width="153px">
          </colgroup>
					<tbody>
						<tr>
							<th class="req">공사번호</th>
							<td>
								<%-- ${cntNum } --%>
								<input type="text" id="cntNum" name="cntNum" value="" onkeydown="noHanPress(this);" minlength="6" maxlength="10"  class="reqVal"/>
							</td>
							<th class="req">공사구분</th>
							<td>
								<select name="cntCde" id="cntCde" class="reqVal">
									<c:forEach var="cntList" items="${cntCdeList }">
										<option value="${cntList.codeCode }">${cntList.codeAlias}</option>
									</c:forEach>
								</select>
							</td>
							<th class="req">공사명</th>
							<td colspan="3">
								<input type="text" id="cntNam" name="cntNam" value="" maxlength="100" style="width: 403px"  class="reqVal"/>
							</td>
						</tr>
						<tr>
							<th>설계자명</th>
							<td>
								<input type="text" id="dsnNam" name="dsnNam" value="" maxlength="20"/>
							</td>
							<th class="req"> 공사위치</th>
							<td colspan = "5">
								<input type="text" id="cntLoc" name="cntLoc" value="" maxlength="50" style="width: 671px"  class="reqVal"/>
							</td>
						</tr>
						<tr>
							<th>설계총액</th>
							<td>
								<input type="text" class="num_style" id="dsnAmt" name="dsnAmt" value="" maxlength="14" onchange="getNumber(this);" onkeyup="getNumber(this);"/>
							</td>
							<th>순공사비</th>
							<td>
								<input type="text" class="num_style" id="dpcAmt" name="dpcAmt" value="" maxlength="14" onchange="getNumber(this);" onkeyup="getNumber(this);"/>
							</td>
							<th>환급금액</th>
							<td>
								<input type="text" class="num_style" id="dgcAmt" name="dgcAmt" value="" maxlength="14" onchange="getNumber(this);" onkeyup="getNumber(this);"/>
							</td>
							<th>기타잡비</th>
							<td>
								<input type="text" class="num_style" id="detAmt" name="detAmt" value="" maxlength="14" onchange="getNumber(this);" onkeyup="getNumber(this);"/>
							</td>
						</tr>
          </tbody>
        </table>
      </div>
    </div>
    <!-- //list --> 



    <!-- tabinfo -->
    <div class=" tabinfo-box">
      
      <div class="tab-info">
        <ul class="tab-info">
          <li ><a href="#" abbr="tb01" class="active">공사정보</a></li>
          <li><a href="#" abbr="tb02">계약정보</a></li>
          <li><a href="#" abbr="tb03">예산정보</a></li>
          <li><a href="#" abbr="tb04">도급자정보</a></li>
          <li><a href="#" abbr="tb05">지출과목정보</a></li>
         
          
        </ul>
      </div> 
    
    
      <!--공사정보-->
      <div class="tab-view active" id="tb01"  >
     	<div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
          <colgroup>
          <col width="114px">
          <col width="154px">
          <col width="114px">
          <col width="154px">
          <col width="114px">
          <col width="153px">
          <col width="114px">
          <col width="153px">
          </colgroup>
          <tbody>
						<tr>
							<th>감독자명</th>
							<td>
								<input type="text" id="svsNsm" name="svsNsm" value="" maxlength="40"/>
							</td>
							<th>착공일자</th>
							<td>
								<input type="text" class="datepicker" id="begYmd" name="begYmd" value=""/>
							</td>
							<th>준공검사일자</th>
							<td>
								<input type="text" class="datepicker" id="fchYmd" name="fchYmd" value=""/>
							</td>
							<th>준공검사자명</th>
							<td>
								<input type="text" id="fchNam" name="fchNam" value="" maxlength="40"/>
							</td>
						</tr>
						<tr>
							<th>준공예정일자</th>
							<td>
								<input type="text" class="datepicker" id="fnsYmd" name="fnsYmd" value=""/>
							</td>
							<th>실준공일자</th>
							<td>
								<input type="text" class="datepicker" id="rfnYmd" name="rfnYmd" value=""/>
							</td>
							<th>관급물량</th>
							<td>
								<input type="text" id="gvrDes" name="gvrDes" value="" maxlength="500"/>
							</td>
							<th></th>
							<td></td>
						</tr>
						<tr>
							<th>공사개요</th>
							<td colspan="7">
								<input type="text" id="cntDes" name="cntDes" value="" maxlength="500"/>
							</td>
						</tr>
          </tbody>
        </table>
      </div>
    </div>


      <!--계약정보-->
      <div class="tab-view" id="tb02"   >
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
          <colgroup>
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          </colgroup>
          <tbody>
						<tr>
							<th>입찰일자</th>
							<td>
								<input type="text" class="datepicker" id="bidYmd" name="bidYmd" value=""/>
							</td>
							<th>계약일자</th>
							<td>
								<input type="text" class="datepicker" id="cttYmd" name="cttYmd" value=""/>
								<!-- 
								<select class="cal_sel cal_sel_b">
									<option>전체</option>
								</select> -->
							</td>
							<th>예정금액</th>
							<td>
								<input type="text" class="num_style" id="estAmt" name="estAmt" value="" maxlength="14" onchange="getNumber(this);" onkeyup="getNumber(this);"/>
							</td>
							<th>계약방법</th>
							<td>
								<select id="cttCde" name="cttCde">
									<c:forEach var="cttList" items="${cttCodeList }">
										<option value="${cttList.codeCode }">${cttList.codeAlias}</option>
									</c:forEach>
								</select>
							</td>
						</tr>
						<tr>
							<th>계약총액</th>
							<td>
								<input type="text" class="num_style" id="tctAmt" name="tctAmt" value="" maxlength="14" onchange="getNumber(this);" onkeyup="getNumber(this);"/>
							</td>
							<th>기타잡비</th>
							<td>
								<input type="text" class="num_style" id="cetAmt" name="cetAmt" value="" maxlength="14" onchange="getNumber(this);" onkeyup="getNumber(this);"/>
							</td>
							<th>순공사비</th>
							<td>
								<input type="text" class="num_style" id="cpcAmt" name="cpcAmt" value="" maxlength="14" onchange="getNumber(this);" onkeyup="getNumber(this);"/>
							</td>
							<th>관급금액</th>
							<td>
								<input type="text" class="num_style" id="cgvAmt" name="cgvAmt" value="" maxlength="14" onchange="getNumber(this);" onkeyup="getNumber(this);"/>
							</td>
						</tr>
          </tbody>
        </table>
      </div>
    </div>


						
       <!--tabinfo 예산정보-->
      <div class="tab-view" id="tb03"   >
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
          <colgroup>
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          </colgroup>
          <tbody>
						<tr>
							<th>국비</th>
							<td>
								<input type="text" class="num_style" id="natAmt" name="natAmt" value="" maxlength="14" onchange="getNumber(this);" onkeyup="getNumber(this);"/>
							</td>
							<th>도비</th>
							<td>
								<input type="text" class="num_style" id="couAmt" name="couAmt" value="" maxlength="14" onchange="getNumber(this);" onkeyup="getNumber(this);"/>
							</td>
							<th>시군비</th>
							<td>
								<input type="text" class="num_style" id="citAmt" name="citAmt" value="" maxlength="14" onchange="getNumber(this);" onkeyup="getNumber(this);"/>
							</td>
							<th>기채</th>
							<td>
								<input type="text" class="num_style" id="bndAmt" name="bndAmt" value="" maxlength="14" onchange="getNumber(this);" onkeyup="getNumber(this);"/>
							</td>
						</tr>
						
						<tr>
							<th>잉여금</th>
							<td>
								<input type="text" class="num_style" id="cssAmt" name="cssAmt" value="" maxlength="14" onchange="getNumber(this);" onkeyup="getNumber(this);"/>
							</td>
							<th>-</th>
							<td>
							</td>
							<th>-</th>
							<td>
							</td>
							<th>-</th>
							<td>
							</td>
						</tr>
          </tbody>
        </table>
      </div>
    </div>


						
      <!--도급자정보-->
      <div class="tab-view" id="tb04"   >
     
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
           <colgroup>
          <col width="114px">
          <col width="154px">
          <col width="114px">
          <col width="154px">
          <col width="114px">
          <col width="153px">
          <col width="114px">
          <col width="153px">
          </colgroup>
          <tbody>
						<tr>
							<th>도급자명</th>
							<td>
								<input type="text" id="gcnNam" name="gcnNam" value="" maxlength="20"/>
							</td>
							<th>대표자명</th>
							<td>
								<input type="text" id="pocNam" name="pocNam" value="" maxlength="20"/>
							</td>
							<th>주소</th>
							<td>
								<input type="text" id="gcnAdr" name="gcnAdr" value="" maxlength="50"/>
							</td>						
							<th>전화번호</th>
							<td>
								<input type="text" id="gcnTel" name="gcnTel" value="" maxlength="30"/>
							</td>
						</tr>
          </tbody>
        </table>
      </div>
    </div>
						
						
						
   	<!--지출과목정보-->
      <div class="tab-view" id="tb05"   >
  
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
          <colgroup>
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          </colgroup>
          <tbody>
						<tr>
							<th>관</th>
							<td>
								<input type="text" id="kwnExp" name="kwnExp" value="" maxlength="30"/>
							</td>
							<th>항</th>
							<td>
								<input type="text" id="hngExp" name="hngExp" value="" maxlength="30"/>
							</td>
							<th>세항</th>
							<td>
								<input type="text" id="shnExp" name="shnExp" value="" maxlength="30"/>
							</td>
							<th>세목</th>
							<td>
								<input type="text" id="mokExp" name="mokExp" value="" maxlength="30"/>
							</td>
						</tr>

					</tbody>
				</table>
			</div>
		</div>
			
			
	</div>
	<!-- tabinfo -->
	
  </div>
  <!-- //container --> 
			
</form>

</div>
<!-- //UI Object -->

</body>
</html>