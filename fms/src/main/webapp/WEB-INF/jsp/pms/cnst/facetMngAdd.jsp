<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<script type="text/javascript">
var popupX = (screen.availWidth-660)/2;
var popupY= (screen.availHeight-1130)/3;

var deleteLinkFlgp = '${ctxRoot}/cons/flgpMaDelete.do';

var cntNum = '${wttSplyMa.cntNum}';
var menuTab = '${param.tab}';	


$(document).ready(function () {

	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();
	

	var msg = '${msg}';
	
	if(msg) {
		alert(msg);	
		try { opener.selfRefresh(); }catch(e){}
		location.href="${ctxRoot}/splyMa/splyMaAdd.do";
	}
	
	setDatepicker();
	$(document).on('focus',".datepicker", setDatepicker);
	
	var now = new Date;
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
    
    //$('#rcpYmd, #begYmd, #fnsYmd').val(today);

    
    
    
    
    
    
    
	 /*******************************************
	 * 이벤트 설정
	 *******************************************/

	
	// 저장
	$('#btnSave').click(function () {
		if(!gfn_formValid("splyMaSaveForm"))	return;

		if ($('#cntNum').val().length < 6) {
			alert('공사번호는 최소 6자 이상 10자 이하로 입력해 주세요.');
			$('#cntNum').focus();
			return false;
		} 		
		
		
		//공사번호 중복체크
		gfn_selectList({sqlId: 'selectWttSplyMa', cntNum: $("#cntNum").val()}, function(ret){
			if(ret.data != null && ret.data.length > 0){
				gfn_alert("공사번호가 중복되었습니다.","W",function(){
					$("#cntNum").select();
				});
				return false;
			}
			
		    gfn_confirm("저장하시겠습니까?", function(ret){
				if(!ret) return;
				
				// 컨트롤러 지정방식
				gfn_saveForm("splyMaSaveForm", "/fms/pms/cnst/insertWttSply.do" , function(data){
					
					if(data.result){
						gfn_alert("데이터가 저장되었습니다.","",function(){
							opener.fn_search();
							window.close();
						});
					}else{
						gfn_alert("데이터 저장에 실패하였습니다. : " + data.error,"E");
					}
				});
				
			});
			
		});
		
		
	});//click

	

});
	


</script>
</head>



<body>

<div id="wrap" class="wrap" > 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1 >급수전대장 상세정보</h1>
  </div>
  <!-- // header --> 

	<form name="splyMaSaveForm" id="splyMaSaveForm" method="POST">

		


<!-- container -->
  <div id="container"  class="content"  > 
    
    <!-- list -->
    <div class="view_box">
      <h2 ><span style="float:left">일반정보</span>
        <div class="btn_group m-b-5 right "  >
          <button id="btnSave" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_save.png" width="16" height="16" alt=""/> 저장 </button>
      </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
          <colgroup>
	          <col width="114px">
	          <col width="154px">
	          <col width="114px">
	          <col width="154px">
	          <col width="114px">
	          <col width="153px">
	          <col width="114px">
	          <col width="153px">
          </colgroup>
          <tbody>

			<tr>
				<th class="req">공사번호</th>
				<td>
					<input type="text" id="cntNum" name="cntNum" value="${wttSplyMa.cntNum }" class="reqVal"/>
				</td>
				<th>시공자명</th>
				<td>
					<input type="text" id="oprNam" name="oprNam" maxlength="40" value="${wttSplyMa.oprNam }"/>
				</td>
				<th>준공검사자명</th>
				<td>
					<input type="text" id="fnsNam" name="fnsNam" maxlength="40" value="${wttSplyMa.fnsNam }"/>
				</td>
				<th>감독자성명</th>
				<td>
					<input type="text" id="svsNam" name="svsNam" maxlength="40" value="${wttSplyMa.svsNam }"/>
				</td>
			</tr>
			<tr>	
				<th>착수일자</th>
				<td>
					<input type="text" class="datepicker" style="height:25px;" id="begYmd" name="begYmd" value="${wttSplyMa.begYmd }"/>
				</td>
				<th>준공일자</th>
				<td>
					<input type="text" class="datepicker" style="height:25px;" id="fnsYmd" name="fnsYmd" value="${wttSplyMa.fnsYmd }"/>
				</td>
				<th>수납일자</th>
				<td>
					<input type="text" class="datepicker" style="height:25px;" id="rcpYmd" name="rcpYmd" value="${wttSplyMa.rcpYmd }"/>
				</td>
				<th > 행정동</th>
				<td>
					<select name="hjdCde" id="hjdCde"  >
						<option value="">선택안함</option>
						<c:forEach var="cmtList" items="${cmtAdarMaList}" varStatus="status">
							<option value="${cmtList.hjdCde }" ${wttSplyMa.hjdCde == cmtList.hjdCde ? "selected" : "" }>${cmtList.hjdNam}</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<th>민원접수번호</th>
				<td>
					<input type="text" id="rcvNum" name="rcvNum" style="text-align:right;" maxlength="9" value="${wttSplyMa.rcvNum }" />
				</td>
				<th>설계수수료</th>
				<td>
					<input type="text" class="num_style" id="dfeAmt" name="dfeAmt" style="text-align:right;" onchange="getNumber(this);" onkeyup="getNumber(this);" maxlength="10" value="${wttSplyMa.dfeAmt }"/>
				</td>
				<th></th><td></td>
				<th></th><td></td>
			</tr>
          </tbody>
        </table>
      </div>
    </div>
    <!-- //list --> 
    
    
  

  </div>
  <!-- //container --> 
</form>
  
</div>
<!-- //UI Object -->

</body>
</html>

