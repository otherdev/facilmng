<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<script type="text/javascript">
var popupX = (screen.availWidth-660)/2;
var popupY= (screen.availHeight-1130)/3;

var deleteLinkFlgp = '${ctxRoot}/cons/flgpMaDelete.do';

var cntNum = '${wttSplyMa.cntNum}';
var menuTab = '${param.tab}';	

$(document).ready(function () {
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();

	var msg = '${msg}';
		
	if(msg) {
		alert(msg);
		try { opener.selfRefresh(); }catch(e){}
		location.href="${ctxRoot}/wttSplyMa/selectWttSplyMaView.do?cntNum=${param.cntNum}";
	}
	
	setDatepicker();
	$(document).on('focus',".datepicker", setDatepicker);
	

	
	// 콤마 처리
	$('#taxAmt').val(setComma('${wttSplyMa.taxAmt }'));
	$('#totAmt').val(setComma('${wttSplyMa.totAmt }'));
	$('#rorAmt').val(setComma('${wttSplyMa.rorAmt }'));
	$('#prvAmt').val(setComma('${wttSplyMa.prvAmt }'));
	$('#gvrAmt').val(setComma('${wttSplyMa.gvrAmt }'));
	$('#gfeAmt').val(setComma('${wttSplyMa.gfeAmt }'));
	$('#ffeAmt').val(setComma('${wttSplyMa.ffeAmt }'));
	$('#etcAmt').val(setComma('${wttSplyMa.etcAmt }'));
	$('#divAmt').val(setComma('${wttSplyMa.divAmt }'));
	$('#tctAmt').val(setComma('${wttSplyMa.tctAmt}'));
	$('#dfeAmt').val(setComma('${wttSplyMa.dfeAmt}'));

	
	
	
	
	 /*******************************************
	 * 이벤트 설정
	 *******************************************/
	
		
	/// 그리드탭선택이벤트
	$(".tab-menu>li>a").click(function(){
	    
		//탭타이틀 처리
		$(".tab-menu>li>a").not(this).removeClass("active");
		if($(this).not(":visible")){
			$(this).addClass("active");	
		}
		
		//탭 본문처리
		$(".tab-list").removeClass("active");
		$("#"+$(this).attr("abbr")).addClass("active");
		
		//그리드 초기화
	    var target_id = $(this).attr("abbr");
		if ((target_id == 'tab01')) {
			fnObj.pageStart();	
	    } 
	    else if ((target_id == 'tab02')) {
			fnObj2.pageStart();	
    	} 
	    else if ((target_id == 'tab03')) {
			fnObj3.pageStart();	
    	} 
	    else if ((target_id == 'tab04')) {
			fnObj4.pageStart();	
    	} 
	     
		
	});
	
	
	
	
	
	// 수정
	$('#btnSave').click(function () {
		
		if(!gfn_formValid("splyMaUpdateForm"))	return;

		if ($('#cntNum').val().length < 6) {
			gfn_alert('공사번호는 최소 6자 이상 10자 이하로 입력해 주세요.',function(){
				$('#cntNum').focus();
				return false;
			});
		}
		
		gfn_confirm("저장하시겠습니까?", function(ret){
			if(!ret) return;
			
			// 컨트롤러 지정방식
			gfn_saveForm("splyMaUpdateForm", "/fms/pms/cnst/updateWttSply.do" , function(data){
				
				if(data.result){
					gfn_alert("데이터가 저장되었습니다.","",function(){
						opener.fn_search();
						fn_reload();
					});
				}else{
					gfn_alert("데이터 저장에 실패하였습니다. : " + data.error,"E");
				}
			});
			
		});
		
	});
	

	// 삭제
	$('#btnDel').click(function () {
		
		gfn_selectList({sqlId:  'selectFileMapList',		//사진첨부 / 참조자료
					    bizId : '${wttSplyMa.cntNum}'
					    }, function(ret){
				
			
			if(ret.data != null && ret.data.length > 0){
				gfn_alert("사진첨부/참조자료 내역이 있습니다.","W");
				return false;
			}
					
			gfn_confirm("삭제하시겠습니까?", function(ret){
			    if(!ret) return;
				    //마스터 삭제
					gfn_saveCmm({sqlId:"deleteWttSplyMa", data:{cntNum: '${wttSplyMa.cntNum}' }  } ,function(data){
						if (data.result) {
							gfn_alert("삭제 되었습니다.","",function(){
								opener.fn_search();
								window.close();
							});
						}		
						else{
							gfn_alert("저장에 실패하였습니다.","E");
							return;
						}
					});					
			});
		});
	});
	

	
	

});
	

// 본페이지 재조회
var fn_reload = function(){
	location.reload();
}

</script>
</head>



<body>

<div id="wrap" class="wrap" > 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1 >급수전대장 상세정보</h1>
  </div>
  <!-- // header --> 

	<form action="${ctxRoot}/splyMaUpdate.do" name="splyMaUpdateForm" id="splyMaUpdateForm" method="POST">
	


  <!-- container -->
  <div id="container"  class="content"  > 
    
    <!-- list -->
    <div class="view_box">
      <h2 ><span style="float:left">일반정보</span>
        <div class="btn_group m-b-5 right "  >
          <!--button id="btnPrint" type="button" class="btn btn-default " onclick="gfn_print();"> <img src="/fms/images/icon_btn_print.png" width="16" height="16" alt=""/> 인쇄 </button-->
          <button id="btnDel" type="button" class="btn btn-default " > <img src="/fms/images/icon_btn_delete.png" width="16" height="16" alt=""/> 삭제 </button>
          <button id="btnSave" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_save.png" width="16" height="16" alt=""/> 저장 </button>
      </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
          <colgroup>
          <col width="114px">
          <col width="154px">
          <col width="114px">
          <col width="154px">
          <col width="114px">
          <col width="153px">
          <col width="114px">
          <col width="153px">
          </colgroup>
          <tbody>
			<tr>
				<th class="req">공사번호</th>
				<td>
					<input type="text" id="cntNum" name="cntNum" value="${wttSplyMa.cntNum }" readonly class="disable reqVal"/>
				</td>
				<th>시공자명</th>
				<td>
					<input type="text" id="oprNam" name="oprNam" maxlength="40" value="${wttSplyMa.oprNam }"/>
				</td>
				<th>준공검사자명</th>
				<td>
					<input type="text" id="fnsNam" name="fnsNam" maxlength="40" value="${wttSplyMa.fnsNam }"/>
				</td>
				<th>감독자성명</th>
				<td>
					<input type="text" id="svsNam" name="svsNam" maxlength="40" value="${wttSplyMa.svsNam }"/>
				</td>
			</tr>
			<tr>	
				<th>착수일자</th>
				<td>
					<input type="text" class="datepicker" style="height:25px;" id="begYmd" name="begYmd" value="${wttSplyMa.begYmd }"/>
				</td>
				<th>준공일자</th>
				<td>
					<input type="text" class="datepicker" style="height:25px;" id="fnsYmd" name="fnsYmd" value="${wttSplyMa.fnsYmd }"/>
				</td>
				<th>수납일자</th>
				<td>
					<input type="text" class="datepicker" style="height:25px;" id="rcpYmd" name="rcpYmd" value="${wttSplyMa.rcpYmd }"/>
				</td>
				<th > 행정동</th>
				<td>
					<select name="hjdCde" id="hjdCde"  >
						<option value="">선택안함</option>
						<c:forEach var="cmtList" items="${cmtAdarMaList}" varStatus="status">
							<option value="${cmtList.hjdCde }" ${wttSplyMa.hjdCde == cmtList.hjdCde ? "selected" : "" }>${cmtList.hjdNam}</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<th>민원접수번호</th>
				<td>
					<input type="text" id="rcvNum" name="rcvNum" style="text-align:right;" maxlength="9" value="${wttSplyMa.rcvNum }" readonly class="disable"  />
				</td>
				<th>설계수수료</th>
				<td>
					<input type="text" class="num_style" id="dfeAmt" name="dfeAmt" style="text-align:right;" onchange="getNumber(this);" onkeyup="getNumber(this);" maxlength="10" value="${wttSplyMa.dfeAmt }"/>
				</td>
				<th></th><td></td>
				<th></th><td></td>
			</tr>
			
          </tbody>
        </table>
      </div>
    </div>
    <!-- //list --> 
    
    

    
   
    
    
    
    <!-- tab -->
    <div class=" tab-box">
      <div class="tab-menu">
        <ul class="tab-menu">
          <li><a href="#" abbr="tab05" class="active">사진첨부</a></li>
          <li><a href="#" abbr="tab06">참조자료</a></li>
          
        </ul>
      </div>


      	<!--tab list-->
		<div  id="tab05" class="tab-list active"  >				
				<c:import url="/pms/link/lnkDwgPhotoList.do?bizId=${wttSplyMa.cntNum}"/>
		</div>
		
		
		<div  id="tab06" class="tab-list"  >
				<c:import url="/pms/link/lnkCnstRefList.do?bizId=${wttSplyMa.cntNum}"/>
		</div>
      	<!--// --> 
    
    </div>
    
    <!-- //tab --> 
    

  </div>
  <!-- //container --> 
</form>
  
</div>
<!-- //UI Object -->

</body>
</html>

