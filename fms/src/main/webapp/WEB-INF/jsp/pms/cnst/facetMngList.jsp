<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>


<script type="text/javascript">
var viewLink = '${ctxRoot}/wttSplyMa/selectWttSplyMaView.do';
var popupX = (screen.availWidth-660)/2;
var popupY = (screen.availHeight-1130)/3;

$(document).ready(function() {
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();
		
	
	setDatepicker();
	$(document).on('focus',".datepicker", setDatepicker);
	
	$ ('#searchCondition7').datepicker();
    $ ('#searchCondition7').datepicker("option", "maxDate", $ ("#searchCondition8").val());
    $ ('#searchCondition7').datepicker("option", "onClose", function ( selectedDate ) {
        $ ("#searchCondition8").datepicker( "option", "minDate", selectedDate );
    });
 
    $ ('#searchCondition8').datepicker();
    $ ('#searchCondition8').datepicker("option", "minDate", $ ("#searchCondition7").val());
    $ ('#searchCondition8').datepicker("option", "onClose", function ( selectedDate ) {
        $ ("#searchCondition7").datepicker( "option", "maxDate", selectedDate );
    });
    
    $ ('#searchCondition9').datepicker();
    $ ('#searchCondition9').datepicker("option", "maxDate", $ ("#searchCondition10").val());
    $ ('#searchCondition9').datepicker("option", "onClose", function ( selectedDate ) {
        $ ("#searchCondition10").datepicker( "option", "minDate", selectedDate );
    });
 
    $ ('#searchCondition10').datepicker();
    $ ('#searchCondition10').datepicker("option", "minDate", $ ("#searchCondition9").val());
    $ ('#searchCondition10').datepicker("option", "onClose", function ( selectedDate ) {
        $ ("#searchCondition9").datepicker( "option", "maxDate", selectedDate );
    });
		
	
	/*******************************************
	 * 이벤트 설정
	 *******************************************/
	 
	/* 상세 클릭 Event */
	$('.viewPopup').click(function (e) {
		e.preventDefault();
		var centerPos = parseInt( $(screen).get(0).width ) / 2;
		var windowCenterPos = 858 / 2;
		var leftPos = centerPos - windowCenterPos;
		var _win = window.open('${ctxRoot}/pms/cnst/facetMngDtl.do?cntNum=' + $(this).attr('cntNum') 
				, this.title, "left="+leftPos+",top=100, width=1100, height=500, menubar=no, location=no, resizable=no, toolbar=no, status=no");
		_win.focus();			

	});
	
	/* 등록하기 팝업 open */
	$('#btnAdd').click(function () {
		window.open('${ctxRoot}/pms/cnst/facetMngAdd.do', '_blank', 'status=no, width=1100, height=340, left='+ popupX + ', top='+ popupY + ', resizable=no');
		return false;
	});
	
	
	$('#btnReset').click(function () {
        var frm = $('#splyMaListForm');
        frm.find('input[type=text]').val('');
        frm.find('select').val('');
     });
	
	$('#searchCondition5').val(setComma('${param.searchCondition5 }'));
	$('#searchCondition6').val(setComma('${param.searchCondition6 }'));
	
	$('#btnSearch').click(function () {			
		fn_search(1);
	});
	
			
	$('#btnExcel').click(function() {
		var html = '';
		html += '<form id="excelForm" method="post">';		
		html += '	<input type="hidden" name="searchCondition2" value= "' + $('#searchCondition2').val() + '" />';
		html += '	<input type="hidden" name="searchCondition4" value= "' + $('#searchCondition4').val() + '" />';
		html += '	<input type="hidden" name="searchCondition7" value= "' + $('#searchCondition7').val() + '" />';
		html += '	<input type="hidden" name="searchCondition8" value= "' + $('#searchCondition8').val() + '" />';
		html += '	<input type="hidden" name="searchCondition9" value= "' + $('#searchCondition9').val() + '" />';
		html += '	<input type="hidden" name="searchCondition10" value= "' + $('#searchCondition10').val() + '" />';		
		html += '</form>';
		$('#wrap').append(html);
		var url = '${ctxRoot}/pms/cnst/facetMngList/excel/download.do';
		$('#excelForm').attr('action', url);
		//$('#excelForm').submit();
		
		var param = $("#excelForm").serializeArray();
		 
		 //로딩 이미지
		 gfn_startLoading();
		 $.fileDownload(url, {
			 httpMethod: 'POST',
	         dataType:"json", // data type of response
	         contentType:"application/json",
	         data: param,
	         successCallback: function (url) {
	        	 //로딩 이미지 종료
	        	 gfn_endLoading();
	         },
	         failCallback: function (responseHtml, url) {
	        	//로딩 이미지 종료
	        	 gfn_endLoading();          
	         }
	     });
	});	 
	 

});
	
function fn_search(pageNo){
	if(gfn_isNull(pageNo)){
		pageNo = 1;
	}	
	$('form[name=splyMaListForm] input[name=pageIndex]').val(pageNo);
	$('form[name=splyMaListForm]').submit();
}


/* 엔터키 적용 */
function onKeyDown () {
	
	if (event.keyCode == 13) {
		fn_search(1);
	}
}

function captureReturnKey(e) {
    if(e.keyCode==13 && e.srcElement.type != 'textarea'){
       fn_search();
    }
 }
	
</script>
</head>




<body>

<div id="wrap" class="wrap" > 
  <div id="header" class="header"  >
    <h1 >급수전대장 검색 목록</h1>
  </div>
		
		
<form name="splyMaListForm" id="splyMaListForm" method="POST" action="${ctxRoot }/pms/cnst/facetMngList.do">
	<input type="hidden" name="pageIndex" value="1"/>
	<input type="hidden" name="cntNum" id="cntNum" value=""/>
	
<!-- container -->
<div id="container" class="content"  > 
	
<div class="seach_box" >
      <h2 >검색항목 </h2>
        <table border="0" cellpadding="0" cellspacing="0" class="table table-search">
          <colgroup>
          <col width="114px">
          <col width="*">
          </colgroup>
          
			<tbody>						
				<tr>
					<th>공사번호</th>
					<td>
						<input type="text" id="searchCondition2" name="searchCondition2" onKeyDown="onKeyDown();" maxlength="10" value="${param.searchCondition2 }" onkeypress="captureReturnKey(event)"/>
					</td>
				</tr>					
				<tr>
					<th>행정동</th>
					<td>
						<select name="searchCondition4" id="searchCondition4">
							<option value="">전체</option>
							<c:forEach var="cmtList" items="${cmtAdarMaList}" varStatus="status">
								<option value="${cmtList.hjdCde }" ${param.searchCondition4 == cmtList.hjdCde ? "selected" : "" }>${cmtList.hjdNam}</option>
							</c:forEach>
						</select>
					</td>
				</tr>						
				<tr>
					<th>착수일자 [이상]</th>
					<td>
						<input type="text" class="datepicker" name="searchCondition7" id="searchCondition7" value="${param.searchCondition7 }" onkeypress="captureReturnKey(event)"/>
					</td>
				</tr>
				<tr>
					<th>착수일자 [이하]</th>
					<td>
						<input type="text" class="datepicker"  name="searchCondition8" id="searchCondition8" value="${param.searchCondition8 }" onkeypress="captureReturnKey(event)"/>
					</td>
				</tr>
				<tr>
					<th>준공일자 [이상]</th>
					<td>
						<input type="text" class="datepicker" name="searchCondition9" id="searchCondition9" value="${param.searchCondition9 }" onkeypress="captureReturnKey(event)"/>
					</td>
				</tr>
				<tr>
					<th>준공일자 [이하]</th>
					<td>
						<input type="text" class="datepicker" name="searchCondition10" id="searchCondition10" value="${param.searchCondition10 }" onkeypress="captureReturnKey(event)"/>
					</td>
				</tr>
			</tbody>
	</table>
	
	
      <div class="btn_group m-t-10 "  >
        <button id="btnReset" class="btn btn-search "  type="button" style="width:114px"><img src="/fms/images/icon_btn_reset.png" width="16" height="16" alt=""/> 초기화 </button>
        <button id="btnSearch" class="btn btn-search " type="button" style="width:132px"><img src="/fms/images/icon_btn_search.png" width="16" height="16" alt=""/> 검색 </button>
      </div>

	</div>
	
    
	<!-- /box_left -->
	
	
    <div class="list_box">
      <h2 ><span style="float:left">목록 | 급수전대장(${paginationInfo.totalRecordCount}건)</span>
        <div class="btn_group m-b-5 right "  >
          <button id="btnAdd" type="button" class="btn btn-default " > <img src="/fms/images/icon_btn_regist.png" width="16" height="16" alt=""/>등록 </button>
          <button id="btnExcel" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_print.png" width="16" height="16" alt=""/> 다운로드 </button>
        </div>
      </h2>
						
	  <div class="list ">

        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-striped table-list center " style="white-space: nowrap;">

				<thead>
					<tr>
						<th>공사번호 </th>
						<th>행정동 </th>						
						<th>시공자명 </th>
						<th>설계총액 </th>
						<th>착수일자 </th>
						<th>준공일자 </th>				
					</tr>
				</thead>
				
				<tbody>
				<c:forEach var="result" items="${resultList }" varStatus="listIndex">
					<tr class="viewPopup ${listIndex.index % 2 == 0 ? '' : 'tr_back01' }" cntNum="${result.cntNum }"   >
						<!--td><a href="javascript:;">${listIndex.index + 1}</a></td>-->
						<td><a href="javascript:;">${result.cntNum }</a> </td>
						<td><a href="javascript:;">${result.hjdCde }</a> </td>
						<td><a href="javascript:;">${result.oprNam }</a> </td>
						<td class="right"><a href="javascript:;"><fmt:formatNumber value="${result.totAmt }" pattern="#,###" /></a> </td>
						<td class= ""><a href="javascript:;">${result.begYmd }</a> </td>
						<td class= ""><a href="javascript:;">${result.fnsYmd }</a> </td>						
					</tr>
				</c:forEach>
				
				<c:if test="${empty resultList}">
					<tr>
						<td colspan="7" style="color:black;text-align:center;">검색 결과가 없습니다.</td>
					</tr>
				</c:if>
				</tbody>
			</table>
		</div>


      <div style="width:100%; text-align:center">
        <ul class="pagination pagination-sm  " >
			<ui:pagination paginationInfo = "${paginationInfo}"  type="image" jsFunction="fn_search" />
        </ul>
      </div>


	</div>	<!-- /box_right -->

</div>	
</form>
	
</div>

</body>

</html>