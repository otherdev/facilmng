<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<script type="text/javascript">
// 상수공사대장 등록처리 호출경로
var addLink = '${ctxRoot}/pms/cnst/cnstMngAdd.do';
var popupX = (screen.availWidth-660)/2;
var popupY= (screen.availHeight-1130)/3;

$(document).ready(function() {
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();
	

	// 달력 이상-이하
	setDatepicker();
	$(document).on('focus',".datepicker", setDatepicker);
	
	$ ('#searchCondition6').datepicker();
    $ ('#searchCondition6').datepicker("option", "maxDate", $("#searchCondition7").val());
    $ ('#searchCondition6').datepicker("option", "onClose", function ( selectedDate ) {
        $ ("#searchCondition7").datepicker( "option", "minDate", selectedDate );
    });
 
    $ ('#searchCondition7').datepicker();
    $ ('#searchCondition7').datepicker("option", "minDate", $("#searchCondition6").val());
    $ ('#searchCondition7').datepicker("option", "onClose", function ( selectedDate ) {
        $ ("#searchCondition6").datepicker( "option", "maxDate", selectedDate );
    });
    
    $ ('#searchCondition8').datepicker();
    $ ('#searchCondition8').datepicker("option", "maxDate", $("#searchCondition9").val());
    $ ('#searchCondition8').datepicker("option", "onClose", function ( selectedDate ) {
        $ ("#searchCondition9").datepicker( "option", "minDate", selectedDate );
    });
 
    $ ('#searchCondition9').datepicker();
    $ ('#searchCondition9').datepicker("option", "minDate", $("#searchCondition8").val());
    $ ('#searchCondition9').datepicker("option", "onClose", function ( selectedDate ) {
        $ ("#searchCondition8").datepicker( "option", "maxDate", selectedDate );
    });
	
	
	$('#searchCondition4').val(setComma('${param.searchCondition4 }'));
	$('#searchCondition5').val(setComma('${param.searchCondition5 }'));
	
	
	 /*******************************************
	 * 이벤트 설정
	 *******************************************/
	$('#btnAdd').click(function () {
		window.open(addLink, "_blank", 'status=no, width=1100, height=355, left='+ popupX + ', top='+ popupY + ', resizable=no');
	});
	 
	 
	// 상세화면 호출 함수
	$('.viewPopup').click(function (e) {
		e.preventDefault();
		var centerPos = parseInt( $(screen).get(0).width ) / 2;
		var windowCenterPos = 858 / 2;
		var leftPos = centerPos - windowCenterPos;
		var _win = window.open('${ctxRoot}/pms/cnst/cnstMngDtl.do?cntNum=' + $(this).find('.cntNumPopup').attr('data-cntNum') , "viewPopup", "left="+leftPos+",top=100,"+$(this).attr("popup-data") + ", menubar=no, location=no, resizable=no, toolbar=no, status=no");
		_win.focus();
		try{ opener._winEdit = _win; }catch(e){}
	});
	
	
	// 초기화 함수
	$('#btnReset').click(function () {
        var frm = $('#consMaListForm');
        frm.find('input[type=text]').val('');
        frm.find('select').val('');
	});
	
	// 상수공사대장 조회 함수
	$('#btnSearch').click(function () {
		removeComma();
		fn_search(1);
		return false;
	});
	
	// 엑셀다운로드 함수
	$('#btnExcel').click(function() {
				
		var title = '상수공사대장 목록'; 
		var sqlId = 'selectWttCnstMaExcelList';
		gfn_ExcelDown(title, sqlId);
	});
	 
});	
			



// 페이징 함수
function fn_search (pageNo) {
	if(gfn_isNull(pageNo)){
		pageNo = 1;
	}		
	$('form[name=consMaListForm] input[name=pageIndex]').val(pageNo);
	$('form[name=consMaListForm]').submit();
}

function captureReturnKey(e) {
    if(e.keyCode==13 && e.srcElement.type != 'textarea'){
    	removeComma();
       	fn_search();
    }
 }
	
	
</script>
</head>


<body>

<div id="wrap" class="wrap" > 
  <div id="header" class="header"  >
    <h1 >상수공사 대장 검색목록</h1>
  </div>
			
			
			
  <form name="consMaListForm" id="consMaListForm"  method="POST">
	<input type="hidden" name="pageIndex" value="1"/>
	<input type="hidden" id="cntNum" name="cntNum" value=""/>
					
  <!-- container -->
  <div id="container"  class="content"  > 
				
				
    <div class="seach_box" >
      <h2 >검색항목 </h2>
      <div>
        <table border="0" cellpadding="0" cellspacing="0" class="table table-search">
          <colgroup>
          <col width="114px">
          <col width="*">
          </colgroup>
			<tbody>
				<tr>
					<th>공사구분</th>
					<td>
						<select name="searchCondition1" id="searchCondition1">
							<option value="">전체</option>
							<c:forEach var="cntList" items="${cntCdeList}">
								<option value="${cntList.codeCode}" ${param.searchCondition1 == cntList.codeCode ? "selected" : "" }>${cntList.codeAlias}</option>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<th>공사번호</th>
					<td>
						<input type="text" name="searchCondition2" id="searchCondition2" maxlength="10" value="${param.searchCondition2 }" onkeypress="captureReturnKey(event)"/>
					</td>
				</tr>
				<tr>
					<th>공사명</th>
					<td>
						<input type="text" name="searchCondition3" id="searchCondition3" value="${param.searchCondition3 }" onkeypress="captureReturnKey(event)"/>
					</td>
				</tr>
				<tr>
					<th>계약금액 [이상]</th>
					<td>
						<input type="text" class="num_style" style="text-align:right;" id="searchCondition4" name="searchCondition4" onchange="getNumber(this);" onkeyup="getNumber(this);" maxlength="16" value="${param.searchCondition4 }" onkeypress="captureReturnKey(event)"/>
					</td>
				</tr>
				<tr>
					<th>계약금액 [이하]</th>
					<td>
						<input type="text" class="num_style" style="text-align:right;" id="searchCondition5" name="searchCondition5" onchange="getNumber(this);" onkeyup="getNumber(this);" maxlength="16" value="${param.searchCondition5 }" onkeypress="captureReturnKey(event)"/>
					</td>
				</tr>
				<tr>
					<th>착공일자 [이상] </th>
					<td>
						<input type="text" class="datepicker" name="searchCondition6" id="searchCondition6" value="${param.searchCondition6 }" onkeypress="captureReturnKey(event)"/>
					</td>
				</tr>
				<tr>
					<th>착공일자 [이하]</th>
					<td>
						<input type="text" class="datepicker" name="searchCondition7" id="searchCondition7" value="${param.searchCondition7 }" onkeypress="captureReturnKey(event)"/>
					</td>
				</tr>
				<tr>
					<th>준공일자 [이상] </th>
					<td>
						<input type="text" class="datepicker" name="searchCondition8" id="searchCondition8" value="${param.searchCondition8 }" onkeypress="captureReturnKey(event)"/>
					</td>
				</tr>
				<tr>
					<th>준공일자 [이하]</th>
					<td>
						<input type="text" class="datepicker" name="searchCondition9" id="searchCondition9" value="${param.searchCondition9 }" onkeypress="captureReturnKey(event)"/>
					</td>
				</tr>
				<tr>
					<th>계약방법</th>
					<td>
						<select name="searchCondition10" id="searchCondition10">
							<option value="">전체</option>
							<c:forEach var="cttList" items="${cttCodeList }">
								<option value="${cttList.codeCode }"${param.searchCondition10 == cttList.codeCode ? "selected" : "" }>${cttList.codeAlias}</option>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<th>공사위치</th>
					<td>
						<input type="text" id="searchCondition11" name="searchCondition11" value="${param.searchCondition11 }" />
					</td>
				</tr>
		</tbody>
	</table>
	
	
      <div class="btn_group m-t-10 "  >
        <button id="btnReset" class="btn btn-search "  type="button" style="width:114px"><img src="/fms/images/icon_btn_reset.png" width="16" height="16" alt=""/> 초기화 </button>
        <button id="btnSearch" class="btn btn-search " type="button" style="width:132px"><img src="/fms/images/icon_btn_search.png" width="16" height="16" alt=""/> 검색 </button>
      </div>
      
	</div>
	</div>
	<!-- /box_left -->


    <div class="list_box">
      <h2 ><span style="float:left">목록 | 상수공사대장(${paginationInfo.totalRecordCount}건)</span>
        <div class="btn_group m-b-5 right "  >
          <button id="btnAdd"  type="button" class="btn btn-default " > <img src="/fms/images/icon_btn_regist.png" width="16" height="16" alt=""/>등록 </button>
          <button id="btnExcel" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_download.png" width="16" height="16" alt=""/> 다운로드 </button>
        </div>
      </h2>
						
	  <div class="list ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-striped table-list center " style="white-space: nowrap;">
          <colgroup>
	          <col width="100">
	          <col width="*">
	          <col width="200">
	          <col width="80">
	          <col width="80">
	          <col width="120">
	          <col width="80">
          </colgroup>
			<thead>
				<tr id="excel_head">
					<th tit="cntNum">공사번호</th>
					<th tit="cntNam" typ="text">공사명</th>
					<th tit="cntLoc" typ="text">공사위치</th>
					<th tit="cntCdeNam">공사구분</th>
					<th tit="dsnNam">설계자명</th>
					<th tit="dsnAmt" typ="number">설계총액</th>
					<th tit="cttNam">계약방법</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="list" items="${resultList}" varStatus="listIndex">						
					<tr class="viewPopup" popup-data="width=1100, height=650" >
						<!--td><a href="javascript:;">${listIndex.index + 1}</a></td>-->								
						<td ><a href="javascript:;" class="cntNumPopup" data-cntNum="${list.cntNum}" >${list.cntNum}</a></td>
						<td class="left"><a href="javascript:;">${list.cntNam}</a></td>
						<td class="left"><a href="javascript:;">${list.cntLoc}</a></td>
						<td ><a href="javascript:;">${mapCntCde[list.cntCde]}</a></td>
						<td><a href="javascript:;">${list.dsnNam}</a></td>
						<td class="right"><a href="javascript:;"><fmt:formatNumber value="${list.dsnAmt}" pattern="#,###" /></a></td>
						<td >
							<a href="javascript:;">
								<c:forEach var="cttList" items="${cttCodeList }">
									<c:if test="${list.cttCde == cttList.codeCode }">
										${cttList.codeAlias}
									</c:if>
								</c:forEach>
							</a>
						</td>
					</tr>
				</c:forEach>					
							
				<c:if test="${empty resultList}">
					<tr>
						<td colspan="7" style="color:black;text-align:center;">검색 결과가 없습니다.</td>
					</tr>
				</c:if>
			</tbody>
		</table>
	</div>
		
		
      <div style="width:100%; text-align:center">
        <ul class="pagination pagination-sm  " >
			<ui:pagination paginationInfo = "${paginationInfo}"  type="image" jsFunction="fn_search" />
        </ul>
      </div>
		
	</div>
	<!-- /box_right -->
					
					
  </div>	

  </form>

</div>
<!-- /popup_layer01 -->

<div id="popup_layer01"></div>
</body>

</html>