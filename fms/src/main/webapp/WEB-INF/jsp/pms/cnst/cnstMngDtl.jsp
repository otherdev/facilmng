<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>


<script type="text/javascript" src="<c:url value='/js/pms/cnst/cnstMngDtl.js' />" ></script>

<script type="text/javascript">
var popupX = (screen.availWidth-660)/2;
var popupY = (screen.availHeight-1130)/3;

var fn_reload = function(){
	location.reload();
}

$(document).ready(function(){
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();

	
	// 1.공사비지급 그리드
	// 공통코드 가져오기
	gfn_selectList({sqlId: 'selectCodeList', typeCode:'PTY_CDE'}, function(ret){
		
		PTY_CDE_LIST = ret.data;
		// 공정리스트
		gfn_selectList({sqlId: 'selectWttCostDtAddList', cntNum:'${wttConsMa.cntNum}'}, function(ret){
			lst = ret.data;
			
			// 그리드 초기화
			fnObj.pageStart();	
		});	
	});	

	
	// 2.설계변경그리드
	gfn_selectList({sqlId: 'selectWttChngDtAddList', cntNum:'${wttConsMa.cntNum}'}, function(ret){
		lst2 = ret.data;
		
		// 그리드 초기화
		fnObj2.pageStart();	
	});	
	
	// 3.하도급그리드
	gfn_selectList({sqlId: 'selectWttSubcDtAddList', cntNum:'${wttConsMa.cntNum}'}, function(ret){
		lst3 = ret.data;
		
		// 그리드 초기화
		fnObj3.pageStart();	
	});	

	// 4.하자보수그리드
	gfn_selectList({sqlId: 'selectWttFlawDtAddList', cntNum:'${wttConsMa.cntNum}'}, function(ret){
		lst4 = ret.data;
		
		// 그리드 초기화
		fnObj4.pageStart();	
	});	
	
	
	
	
	
	
	
	
	 /*******************************************
	 * 이벤트 설정
	 *******************************************/
	
	/// 인포탭선택이벤트  
	$(".tab-info>li>a").click(function(){
		//탭타이틀 처리
		$(".tab-info>li>a").not(this).removeClass("active");
		if($(this).not(":visible")){
			$(this).addClass("active");	
		}
		
		//탭본문처리
		$(".tab-view").removeClass("active");
		$("#"+$(this).attr("abbr")).addClass("active");
	});

	
	
	/// 그리드탭선택이벤트
	$(".tab-menu>li>a").click(function(){
	    
		//탭타이틀 처리
		$(".tab-menu>li>a").not(this).removeClass("active");
		if($(this).not(":visible")){
			$(this).addClass("active");	
		}
		
		//탭 본문처리
		$(".tab-list").removeClass("active");
		$("#"+$(this).attr("abbr")).addClass("active");
		
		//그리드 초기화
	    var target_id = $(this).attr("abbr");
		if ((target_id == 'tab01')) {
			fnObj.pageStart();	
	    } 
	    else if ((target_id == 'tab02')) {
			fnObj2.pageStart();	
    	} 
	    else if ((target_id == 'tab03')) {
			fnObj3.pageStart();	
    	} 
	    else if ((target_id == 'tab04')) {
			fnObj4.pageStart();	
    	} 
	});
	

	
	
	
	
	// 상수공사대장 마스터 저장하기 
	$('#btnSave').click(function () {
		
		if ($('#cntNum').val().length < 6) {
			gfn_alert("공사번호는 최소 6자 이상 10자 이하로 입력하세요.","",function(){
				$('#cntNum').focus();	
			});
			return false;
		}
			    
		if(!gfn_formValid("consMaViewFrm"))	return;
	    

	    gfn_confirm("저장하시겠습니까?", function(ret){
	    	
			if(!ret) return;
			
			gfn_saveFormCmm("consMaViewFrm", "updateWttConsMa", function(data){
				if(data.result){
					gfn_alert("데이터가 저장되었습니다.","",function(){
						opener.fn_search();
						fn_reload();
					});
				}else{
					gfn_alert("데이터 저장에 실패하였습니다. : " + data.error,"E");
				}
			});	
		});
		
	});	

	
	// 상수공사대장 삭제하기 Event
	$('#btnDel').click(function () {
		
		// 1.공사비지급 체크 
		gfn_selectList({sqlId: 'selectWttCostDtAddList',
						sqlId2: 'selectWttChngDtAddList',
						sqlId3: 'selectWttSubcDtAddList',
						sqlId4: 'selectWttFlawDtAddList',
						sqlId5: 'selectFileMapList',		//사진첨부 / 참조자료
					    cntNum:'${wttConsMa.cntNum}',
					    bizId:'${wttConsMa.cntNum}'}, function(ret){
			if(ret.data != null && ret.data.length > 0){
				gfn_alert("공사비지급 내역이 있습니다.","W");
				return false;
			}

			if(ret.data2 != null && ret.data2.length > 0){
				gfn_alert("설계변경 내역이 있습니다.","W");
				return false;
			}
			
			if(ret.data3 != null && ret.data3.length > 0){
				gfn_alert("하도급 내역이 있습니다.","W");
				return false;
			}
			
			if(ret.data4 != null && ret.data4.length > 0){
				gfn_alert("하자보수 내역이 있습니다.","W");
				return false;
			}
			
			if(ret.data5 != null && ret.data5.length > 0){
				gfn_alert("사진첨부/참조자료 내역이 있습니다.","W");
				return false;
			}
						
			// 삭제처리
			gfn_confirm("삭제하시겠습니까?", function(ret){
			    if(!ret)	return;
				
				gfn_saveCmm({sqlId:"deleteWttConsMa", data:{cntNum: '${wttConsMa.cntNum}' }  }
				,function(data){
					if (data.result) {
						gfn_alert("삭제 되었습니다.","",function(){
							opener.fn_search();
							window.close();
						});
					}		
					else{
						gfn_alert("삭제에 실패하였습니다.","E");
						return;
					}
				});
			});			
		});	
		
		
	});
	
	
	
	
	
	
	
	/*		공사비지급내역  탭		*/
	/* ======================================================================================== */

	//그리드저장
	$("#saveCostDtBtn").click(function(){
		myGrid.editCellClear(0,0,0);
		myGrid.selectClear();

		// ajax 저장 
		var _lst = fnObj.grid.getList();
		
	    //그리드필수체크
		myGrid.editCellClear(0,0,0);
		myGrid.selectClear();
		if(!myGrid.validateCheck('C') || !myGrid.validateCheck('U') ){
			return false;
		}	 


		// 저장
		gfn_confirm("저장하시겠습니까?", function(ret){
			if(!ret) return;
			//저장처리
			gfn_saveCmmList(
				{lst: _lst, sqlId: 'saveWttCostDt', cntNum: '${wttConsMa.cntNum}'}
				, function(data){
					if (data.result) {
						gfn_alert("저장되었습니다.");
						gfn_selectList({sqlId: 'selectWttCostDtAddList', cntNum:'${wttConsMa.cntNum}'}, function(ret){
							lst = ret.data;
							// 그리드 초기화
							fnObj.pageStart();	
						});	
					}
					else{
						var err_msg = "저장에 실패하였습니다.";
						try{
							err_msg = data.error;
						}catch(e){}
						gfn_alert(err_msg,"E");
						gfn_selectList({sqlId: 'selectWttCostDtAddList', cntNum:'${wttConsMa.cntNum}'}, function(ret){
							lst = ret.data;
							// 그리드 초기화
							fnObj.pageStart();	
						});	
						return;
					}
			});
		});
		
	});

		
		
	/* 설계변경내역 */
	/* ======================================================================================== */

	//그리드저장
	$("#chngDtSavBtn").click(function(){
		myGrid2.editCellClear(0,0,0);
		myGrid2.selectClear();

		// ajax 저장 
		var _lst2 = fnObj2.grid.getList();
		
	    //그리드필수체크
		myGrid2.editCellClear(0,0,0);
		myGrid2.selectClear();
		if(!myGrid2.validateCheck('C') || !myGrid2.validateCheck('U') ){
			return false;
		}	 


		// 저장
		gfn_confirm("저장하시겠습니까?", function(ret){
			if(!ret) return;
			//저장처리
			gfn_saveCmmList(
				{lst: _lst2, sqlId: 'updateWttChngDt', cntNum: '${wttConsMa.cntNum}'}
				, function(data){
					if (data.result) {
						gfn_alert("저장되었습니다.");
						gfn_selectList({sqlId: 'selectWttChngDtAddList', cntNum:'${wttConsMa.cntNum}'}, function(ret){
							lst2 = ret.data;
							// 그리드 초기화
							fnObj2.pageStart();	
						});	
					}
					else{
						var err_msg = "저장에 실패하였습니다.";
						try{
							err_msg = data.error;
						}catch(e){}
						gfn_alert(err_msg,"E");
						gfn_selectList({sqlId: 'selectWttChngDtAddList', cntNum:'${wttConsMa.cntNum}'}, function(ret){
							lst2 = ret.data;
							// 그리드 초기화
							fnObj2.pageStart();	
						});	
						return;
					}
			});
		});
		
	});



	/* 하도급내역 */
	/* ======================================================================================== */

	//그리드저장
	$("#subDtSavBtn").click(function(){
		myGrid3.editCellClear(0,0,0);
		myGrid3.selectClear();

		// ajax 저장 
		var _lst3 = fnObj3.grid.getList();
		
	    //그리드필수체크
		myGrid3.editCellClear(0,0,0);
		myGrid3.selectClear();
		if(!myGrid3.validateCheck('C') || !myGrid3.validateCheck('U') ){
			return false;
		}	 


		// 저장
		gfn_confirm("저장하시겠습니까?", function(ret){
			if(!ret) return;
			//저장처리
			gfn_saveCmmList(
				{lst: _lst3, sqlId: 'updateWttSubcDt', cntNum: '${wttConsMa.cntNum}'}
				, function(data){
					if (data.result) {
						gfn_alert("저장되었습니다.");
						gfn_selectList({sqlId: 'selectWttSubcDtAddList', cntNum:'${wttConsMa.cntNum}'}, function(ret){
							lst3 = ret.data;
							// 그리드 초기화
							fnObj3.pageStart();	
						});	
					}
					else{
						var err_msg = "저장에 실패하였습니다.";
						try{
							err_msg = data.error;
						}catch(e){}
						gfn_alert(err_msg,"E");
						gfn_selectList({sqlId: 'selectWttSubcDtAddList', cntNum:'${wttConsMa.cntNum}'}, function(ret){
							lst3 = ret.data;
							// 그리드 초기화
							fnObj3.pageStart();	
						});	
						return;
					}
			});
		});
		
	});






	/* 하자보수내역 */
	/* ======================================================================================== */
	
	//그리드저장
	$("#btnFlawDtSav").click(function(){
		myGrid4.editCellClear(0,0,0);
		myGrid4.selectClear();

		// ajax 저장 
		var _lst4 = fnObj4.grid.getList();
		
	    //그리드필수체크
		myGrid4.editCellClear(0,0,0);
		myGrid4.selectClear();
		if(!myGrid4.validateCheck('C') || !myGrid4.validateCheck('U') ){
			return false;
		}	 


		// 저장
		gfn_confirm("저장하시겠습니까?", function(ret){
			if(!ret) return;
			//저장처리
			gfn_saveCmmList(
				{lst: _lst4, sqlId: 'updateWttFlawDt', cntNum: '${wttConsMa.cntNum}'}
				, function(data){
					if (data.result) {
						gfn_alert("저장되었습니다.");
						gfn_selectList({sqlId: 'selectWttFlawDtAddList', cntNum:'${wttConsMa.cntNum}'}, function(ret){
							lst4 = ret.data;
							// 그리드 초기화
							fnObj4.pageStart();	
						});	
					}
					else{
						var err_msg = "저장에 실패하였습니다.";
						try{
							err_msg = data.error;
						}catch(e){}
						gfn_alert(err_msg,"E");
						gfn_selectList({sqlId: 'selectWttFlawDtAddList', cntNum:'${wttConsMa.cntNum}'}, function(ret){
							lst4 = ret.data;
							// 그리드 초기화
							fnObj4.pageStart();	
						});	
						return;
					}
			});
		});		
		
	});

	$("#btnPrint").click(function(){     
		$('<form>', {
            "id": "cnstMngDtl",
            "html": '<input type="text" id="cntNum" name="cntNum" value="${cntNum}" />',
            "action": '/fms/pms/pdf/cnstMngDtl.do'
        }).appendTo(document.body).submit();		
	});	



	// 콤마 처리
	$('#dsnAmt').val(comma('${wttConsMa.dsnAmt}'));
	$('#dgvAmt').val(comma('${wttConsMa.dgvAmt}'));
	$('#detAmt').val(comma('${wttConsMa.detAmt}'));
	$('#dpcAmt').val(comma('${wttConsMa.dpcAmt}'));
	
	$('#estAmt').val(comma('${wttConsMa.estAmt}'));
	$('#tctAmt').val(comma('${wttConsMa.tctAmt}'));
	$('#cetAmt').val(comma('${wttConsMa.cetAmt}'));
	$('#cpcAmt').val(comma('${wttConsMa.cpcAmt}'));
	$('#cgvAmt').val(comma('${wttConsMa.cgvAmt}'));
	
	$('#natAmt').val(comma('${wttConsMa.natAmt}'));
	$('#couAmt').val(comma('${wttConsMa.couAmt}'));
	$('#citAmt').val(comma('${wttConsMa.citAmt}'));
	$('#bndAmt').val(comma('${wttConsMa.bndAmt}'));
	$('#cssAmt').val(comma('${wttConsMa.cssAmt}'));
	
	
	
	
});



</script>
</head>



<body>

	
					
<div id="wrap" class="wrap" > 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1 >상수공사 대장 상세정보</h1>
  </div>
  <!-- // header --> 

	<form name="consMaDeleteForm" id="consMaDeleteForm" method="POST" action="${ctxRoot}/cons/consMaDelete.do">
		<input type="hidden" id="cntNum" name="cntNum" value="${wttConsMa.cntNum}"/>
	</form>
	
	<form name="consMaViewFrm" id="consMaViewFrm" method="post">
 
  <!-- container -->
  <div id="container"  class="content"  > 
    
    <!-- list -->
    <div class="view_box">
      <h2 ><span style="float:left">일반정보</span>
        <div class="btn_group m-b-5 right "  >
          <button id="btnPrint" type="button" class="btn btn-default "> <img src="/fms/images/icon_btn_print.png" width="16" height="16" alt=""/> 인쇄 </button>
          <button id="btnDel" type="button" class="btn btn-default " > <img src="/fms/images/icon_btn_delete.png" width="16" height="16" alt=""/> 삭제 </button>
          <button id="btnSave" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_save.png" width="16" height="16" alt=""/> 저장 </button>
        </div>
      </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
          <colgroup>
          <col width="114px">
          <col width="154px">
          <col width="114px">
          <col width="154px">
          <col width="114px">
          <col width="153px">
          <col width="114px">
          <col width="153px">
          </colgroup>
					
					<tbody>
						
						<tr>
							<th class="req">공사번호</th>
							<td>
								<input type="text" id="cntNum" name="cntNum" value="${wttConsMa.cntNum}" readonly class="disable reqVal"/>
							</td>
							<th class="req">공사구분</th>
							<td>
								<select id="cntCde" name="cntCde"  class="reqVal">
									<option value="">전체</option>
									<c:forEach var="cntList" items="${cntCdeList }">
										<option value="${cntList.codeCode }"${wttConsMa.cntCde == cntList.codeCode ? "selected" : "" }>${cntList.codeAlias}</option>
									</c:forEach>
								</select>
							</td>
							<th class="req">공사명</th>
							<td colspan="3">
								<input type="text" id="cntNam" name="cntNam" value="${wttConsMa.cntNam}" maxlength="100" style="width: 403px" class="reqVal"/>
							</td>
						</tr>
						<tr>
							<th>설계자명</th>
							<td>
								<input type="text" id="dsnNam" name="dsnNam" value="${wttConsMa.dsnNam}" maxlength="20"/>
							</td>
							<th class="req">공사위치</th>
							<td colspan="5">
								<input type="text" id="cntLoc" name="cntLoc" value="${wttConsMa.cntLoc}" maxlength="50" style="width:672px"  class="reqVal"/>
							</td>

						</tr>
						<tr>
							<th>설계총액</th>
							<td>
								<input type="text" class="num_style" id="dsnAmt" name="dsnAmt" value="" maxlength="14" style="text-align:right;" onchange="getNumber(this);" onkeyup="getNumber(this);"/>
							</td>
							<th>순공사비</th>
							<td>
								<input type="text" class="num_style" id="dpcAmt" name="dpcAmt" value="${wttConsMa.dpcAmt}" maxlength="14" style="text-align:right;" onchange="getNumber(this);" onkeyup="getNumber(this);"/>
							</td>
							<th>설계금액_관급금액</th>
							<td>
								<input type="text" class="num_style" id="dgvAmt" name="dgvAmt" value="${wttConsMa.dgvAmt}" maxlength="14" style="text-align:right;" onchange="getNumber(this);" onkeyup="getNumber(this);"/>
							</td>
							<th>기타잡비</th>
							<td>
								<input type="text" class="num_style" id="detAmt" name="detAmt" value="${wttConsMa.detAmt}" maxlength="14" style="text-align:right;" onchange="getNumber(this);" onkeyup="getNumber(this);"/>
							</td>
						</tr>
						
          </tbody>
        </table>
      </div>
    </div>
    <!-- //list --> 




    <!-- tabinfo -->
    <div class=" tabinfo-box">
      
      <div class="tab-info">
        <ul class="tab-info">
          <li ><a href="#" abbr="tb01" class="active">공사정보</a></li>
          <li><a href="#" abbr="tb02">계약정보</a></li>
          <li><a href="#" abbr="tb03">예산정보</a></li>
          <li><a href="#" abbr="tb04">도급자정보</a></li>
          <li><a href="#" abbr="tb05">지출과목정보</a></li>
         
          
        </ul>
      </div> 
    
    
      <!--공사정보-->
      <div class="tab-view active" id="tb01"  >
     	<div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
          <colgroup>
          <col width="114px">
          <col width="154px">
          <col width="114px">
          <col width="154px">
          <col width="114px">
          <col width="153px">
          <col width="114px">
          <col width="153px">
          </colgroup>
          <tbody>
						<tr>
							<th>감독자명</th>
							<td>
								<input type="text" id="svsNsm" name="svsNsm" value="${wttConsMa.svsNsm}" maxlength="40"/>
							</td>
							<th>착공일자</th>
							<td>
								<input type="text" id="begYmd" name="begYmd" class="datepicker" value="${wttConsMa.begYmd}">
							</td>
							<th>준공검사일자</th>
							<td>
								<input type="text" id="fchYmd" name="fchYmd" class="datepicker" value="${wttConsMa.fchYmd}"/>
							</td>
							<th>준공검사자명</th>
							<td>
								<input type="text" id="fchNam" name="fchNam" value="${wttConsMa.fchNam}" maxlength="40"/>
							</td>
						</tr>
						<tr>
							<th>준공일자</th>
							<td>
								<input type="text" class="datepicker" id="fnsYmd" name="fnsYmd" value="${wttConsMa.fnsYmd}"/>
							</td>
							<th>실준공일자</th>
							<td>
								<input type="text" class="datepicker" id="rfnYmd" name="rfnYmd" value="${wttConsMa.rfnYmd}"/>
							</td>
							<th>관급물량</th>
							<td>
								<input type="text" id="gvrDes" name="gvrDes" value="${wttConsMa.gvrDes}" maxlength="500"/>
							</td>
							<th></th>
							<td></td>
						</tr>
						<tr>
							<th>공사개요</th>
							<td colspan="7">
								<input type="text" id="cntDes" name="cntDes" value="${wttConsMa.cntDes}" maxlength="500"  style="width:940px"/>
							</td>
						</tr>

          </tbody>
        </table>
      </div>
   	</div>


      <!--계약정보-->
      <div class="tab-view" id="tb02"   >
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
          <colgroup>
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          </colgroup>
          <tbody>
						<tr>
							<th>입찰일자</th>
							<td>
								<input type="text" class="datepicker" id="bidYmd" name="bidYmd" value="${wttConsMa.bidYmd}"/>
							</td>
							<th>계약일자</th>
							<td>
								<input type="text" class="datepicker" id="cttYmd" name="cttYmd" value="${wttConsMa.cttYmd}"/>
							</td>
							<th>예정금액</th>
							<td>
								<input type="text" class="num_style" id="estAmt" name="estAmt" value="${wttConsMa.estAmt}" maxlength="14" style="text-align:right;" onchange="getNumber(this);" onkeyup="getNumber(this);"/>
							</td>
							<th>계약방법</th>
							<td>
								<select id="cttCde" name="cttCde">
									<c:forEach var="cttList" items="${cttCodeList }">
										<option value="${cttList.codeCode }"${wttConsMa.cttCde == cttList.codeCode ? "selected" : "" }>${cttList.codeAlias}</option>
									</c:forEach>
								</select>
							</td>
						</tr>
						<tr>
							<th>계약총액</th>
							<td>
								<input type="text" class="num_style" id="tctAmt" name="tctAmt" value="${wttConsMa.tctAmt}" maxlength="14" style="text-align:right;" onchange="getNumber(this);" onkeyup="getNumber(this);"/>
							</td>
							<th>기타잡비</th>
							<td>
								<input type="text" class="num_style" id="cetAmt" name="cetAmt" value="${wttConsMa.cetAmt}" maxlength="14" style="text-align:right;" onchange="getNumber(this);" onkeyup="getNumber(this);"/>
							</td>
							<th>순공사비</th>
							<td>
								<input type="text" class="num_style" id="cpcAmt" name="cpcAmt" value="${wttConsMa.cpcAmt}" maxlength="14" style="text-align:right;" onchange="getNumber(this);" onkeyup="getNumber(this);"/>
							</td>
							<th>관급금액</th>
							<td>
								<input type="text" class="num_style" id="cgvAmt" name="cgvAmt" value="${wttConsMa.cgvAmt}" maxlength="14" style="text-align:right;" onchange="getNumber(this);" onkeyup="getNumber(this);"/>
							</td>
						</tr>
          </tbody>
        </table>
      </div>
    </div>
						
						
						
       <!--tabinfo 예산정보-->
      <div class="tab-view" id="tb03"   >
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
          <colgroup>
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          </colgroup>
          <tbody>
						<tr>
							<th>국비</th>
							<td>
								<input type="text" class="num_style" id="natAmt" name="natAmt" value="${wttConsMa.natAmt}" maxlength="14" style="text-align:right;" onchange="getNumber(this);" onkeyup="getNumber(this);"/>
							</td>
							<th>도비</th>
							<td>
								<input type="text" class="num_style" id="couAmt" name="couAmt" value="${wttConsMa.couAmt}" maxlength="14" style="text-align:right;" onchange="getNumber(this);" onkeyup="getNumber(this);"/>
							</td>
							<th>시군비</th>
							<td>
								<input type="text" class="num_style" id="citAmt" name="citAmt" value="${wttConsMa.citAmt}" maxlength="14" style="text-align:right;" onchange="getNumber(this);" onkeyup="getNumber(this);"/>
							</td>
							<th>기채</th>
							<td>
								<input type="text" class="num_style" id="bndAmt" name="bndAmt" value="${wttConsMa.bndAmt}" maxlength="14" style="text-align:right;" onchange="getNumber(this);" onkeyup="getNumber(this);"/>
							</td>
						</tr>
						
						<tr>
							<th>잉여금</th>
							<td>
								<input type="text" class="num_style" id="cssAmt" name="cssAmt" value="${wttConsMa.cssAmt}" maxlength="14" style="text-align:right;" onchange="getNumber(this);" onkeyup="getNumber(this);"/>
							</td>
							<th>-</th>
							<td>
							</td>
							<th>-</th>
							<td>
							</td>
							<th>-</th>
							<td>
							</td>
						</tr>
						
          </tbody>
        </table>
      </div>
    </div>
						
						
						
						
      <!--도급자정보-->
      <div class="tab-view" id="tb04"   >
     
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
           <colgroup>
          <col width="114px">
          <col width="154px">
          <col width="114px">
          <col width="154px">
          <col width="114px">
          <col width="153px">
          <col width="114px">
          <col width="153px">
          </colgroup>
          <tbody>
						<tr>
							<th>도급자명</th>
							<td>
								<input type="text" id="gcnNam" name="gcnNam" value="${wttConsMa.gcnNam}" maxlength="20"/>
							</td>
							<th>대표자명</th>
							<td>
								<input type="text" id="pocNam" name="pocNam" value="${wttConsMa.pocNam}" maxlength="20"/>
							</td>
							<th>주소</th>
							<td>
								<input type="text" id="gcnAdr" name="gcnAdr" value="${wttConsMa.gcnAdr}" maxlength="50"/>
							</td>
							<th>전화번호</th>
							<td>
								<input type="text" id="gcnTel" name="gcnTel" value="${wttConsMa.gcnTel}" maxlength="10" onkeyup="onlyNumber(this);" />
							</td>
						</tr>
          </tbody>
        </table>
      </div>
    </div>
						
						
						
   	<!--지출과목정보-->
      <div class="tab-view" id="tb05"   >
  
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
          <colgroup>
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          </colgroup>
          <tbody>
						<tr>
							<th>관</th>
							<td>
								<input type="text" id="kwnExp" name="kwnExp" value="${wttConsMa.kwnExp}" maxlength="30"/>
							</td>
							<th>항</th>
							<td>
								<input type="text" id="hngExp" name="hngExp" value="${wttConsMa.hngExp}" maxlength="30"/>
							</td>
							<th>세항</th>
							<td>
								<input type="text" id="shnExp" name="shnExp" value="${wttConsMa.shnExp}" maxlength="30"/>
							</td>
							<th>세목</th>
							<td>
								<input type="text" id="mokExp" name="mokExp" value="${wttConsMa.mokExp}" maxlength="30"/>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
			
			
	</div>
	<!-- tabinfo -->
			
			
			
			
			
    <!-- 그리드tab -->
    <div class=" tab-box">
      
        <div class="tab-menu">
        <ul class="tab-menu">
          <li><a href="#" abbr="tab01" class="active">공사비지급내역</a></li>
          <li><a href="#" abbr="tab02">설계변경내역</a></li>
          <li><a href="#" abbr="tab03">하도급내역</a></li>
          <li><a href="#" abbr="tab04">하자보수내역</a></li>
          <li><a href="#" abbr="tab05">사진첨부</a></li>
          <li><a href="#" abbr="tab06">참조자료</a></li>
          
        </ul>
      </div> 

					<div id="tab01" class="tab-list active"  >

		                <div class="btn_group" style="padding:10px;">
		                    <input type="button" value="추가" class="AXButton Green" onclick="fnObj.grid.append();"/>
		                    <input type="button" value="삭제" class="AXButton Green" onclick="fnObj.grid.remove();"/>
		                    <input type="button" id="saveCostDtBtn" value="저장" class="AXButton Blue" style="float:right;"/>
		                </div>					  	
					  	<div id="grdPay" style="height:300px;"></div>

					</div>
					
					
					<div  id="tab02" class="tab-list"  >

		                <div class="btn_group" style="padding:10px;">
		                    <input type="button" value="추가" class="AXButton Green" onclick="fnObj2.grid.append();"/>
		                    <input type="button" value="삭제" class="AXButton Green" onclick="fnObj2.grid.remove();"/>
		                    <input type="button" id="chngDtSavBtn" value="저장" class="AXButton Blue" style="float:right;"/>
		                </div>					  	
					  	<div id="grdChg" style="height:300px;"></div>
					</div>					
					
					
					
					<div  id="tab03" class="tab-list"  >

		                <div class="btn_group" style="padding:10px;">
		                    <input type="button" value="추가" class="AXButton Green" onclick="fnObj3.grid.append();"/>
		                    <input type="button" value="삭제" class="AXButton Green" onclick="fnObj3.grid.remove();"/>
		                    <input type="button" id="subDtSavBtn" value="저장" class="AXButton Blue" style="float:right;"/>
		                </div>					  	
					  	<div id="grdSub" style="height:300px;"></div>
					
					</div>
					
					<div  id="tab04" class="tab-list"  >
							
		                <div class="btn_group" style="padding:10px;">
		                    <input type="button" value="추가" class="AXButton Green" onclick="fnObj4.grid.append();"/>
		                    <input type="button" value="삭제" class="AXButton Green" onclick="fnObj4.grid.remove();"/>
		                    <input type="button" id="btnFlawDtSav" value="저장" class="AXButton Blue" style="float:right;"/>
		                </div>					  	
					  	<div id="grdRpr" style="height:300px;"></div>
					
					</div>
					
					
					<div  id="tab05" class="tab-list"  >				
						<!-- bizId 공사 : 공사번호 -->
						<!-- bizId 시설물 : ftrCde + ftrIdn -->
						<c:import url="/pms/link/lnkDwgPhotoList.do?bizId=${wttConsMa.cntNum}"/>
					</div>
					
					
					<div  id="tab06" class="tab-list"  >
						<c:import url="/pms/link/lnkCnstRefList.do?bizId=${wttConsMa.cntNum}"/>
					</div>
			
			</div>			
			
    </div>
    <!-- //tab --> 
    
  </div>
  <!-- //container --> 
</form>  

</div>
<!-- //UI Object -->

</body>
</html>