<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<body>
<title>상수관망관리</title>

<script type="text/javascript">

var popupX = (screen.availWidth-660)/2;
var popupY = (screen.availHeight-1130)/3;

var createLink = '${ctxRoot}/pms/pipen/stndPi/stndPiCreate.do';
		 
$(document).ready(function() {
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();
	
	/*******************************************
	 * 이벤트 설정
	 *******************************************/
	$ ('#searchCondition5').datepicker();
    $ ('#searchCondition5').datepicker("option", "maxDate", $ ("#searchCondition6").val());
    $ ('#searchCondition5').datepicker("option", "onClose", function ( selectedDate ) {
        $ ("#searchCondition6").datepicker( "option", "minDate", selectedDate );
    });
 
    $ ('#searchCondition6').datepicker();
    $ ('#searchCondition6').datepicker("option", "minDate", $ ("#searchCondition5").val());
    $ ('#searchCondition6').datepicker("option", "onClose", function ( selectedDate ) {
        $ ("#searchCondition5").datepicker( "option", "maxDate", selectedDate );
    });
		
	$('#btnReset').click(function () {
        var frm = $('#stndPiListFrm');
        frm.find('input[type=text]').val('');
        frm.find('select').val('');
    });

	$('#btnSearch').click(function () {
		
		removeComma();
		var data = $("#searchCondition9").val();
		if(!gfn_isNum2(data)){  
						
			gfn_alert("구경은 숫자 항목입니다.", "W", function(){
				$("#searchCondition9").focus();
			});
			return false;
		}
		
		data = $("#searchCondition10").val();
		if(!gfn_isNum2(data)){  
						
			gfn_alert("표고는 숫자 항목입니다.", "W", function(){
				$("#searchCondition10").focus();
			});
			return false;
		}
		fn_search(1);
	});	

	
	$('#btnCreate').click(function () {
		window.open(createLink, "_blank", 'status=no, width=1100, height=270, left='+ popupX + ', top='+ popupY + ', resizable=no');
	});
	
	$('#btnExcelDown').click(function() {
		var title = '스탠드파이프목록'; 
		var sqlId = 'selectStpiPsExcelList';
		gfn_ExcelDown(title, sqlId);
	});
	
	//상세팝업
	$(".dtlPopup").click(function(event){
 
		var dtlLink = '${ctxRoot}/pms/pipen/stndPi/stndPiView.do?ftrCde='+$(this).attr('ftrCde')+'&ftrIdn='+$(this).attr('ftrIdn');
		var centerPos = parseInt( $(screen).get(0).width ) / 2;
		var windowCenterPos = 858 / 2;
		var leftPos = centerPos - windowCenterPos;

		var _win = "";
		try{ opener.closeChild(); }catch(e){}
		_win = window.open(dtlLink, "child", "left="+leftPos+",top=100, width=1100, height=560, menubar=no, location=no, resizable=no, toolbar=no, status=no");
		_win.focus();
		try{ opener._winChild = _win; }catch(e){}
		
	});
});

/* pagination 페이지 링크 function */
function fn_search(pageNo){   		
	if(gfn_isNull(pageNo)){
		pageNo = 1;
	}
	
   	$('form[name=stndPiListFrm] input[name=pageIndex]').val(pageNo);
	$('form[name=stndPiListFrm]').submit();
}

function captureReturnKey(e) {
	
	removeComma();
	var data = $("#searchCondition9").val();
	if(!gfn_isNum2(data)){  
					
		gfn_alert("구경은 숫자 항목입니다.", "W", function(){
			$("#searchCondition9").val("");
			$("#searchCondition9").focus();
		});
		return false;
	}
	
	data = $("#searchCondition10").val();
	if(!gfn_isNum2(data)){  
					
		gfn_alert("표고는 숫자 항목입니다.", "W", function(){
			$("#searchCondition10").val("");
			$("#searchCondition10").focus();
		});
		return false;
	}
	
    if(e.keyCode==13 && e.srcElement.type != 'textarea'){    	    	    	
		fn_search();	   	
    }
 }
</script>

<style>
.popup_layer {margin: 0px auto 0 auto;}
#popup_layer01{width:100%; left:0; top:0; margin-top:0; margin-left:0;}
</style>

<body>



<div id="wrap" class="wrap" > 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1 >스탠드파이프 검색목록</h1>
  </div>
  <!-- // header --> 
  
  
<form name="stndPiListFrm" id="stndPiListFrm" method="post">
  <input type="hidden" name="pageIndex" value="1"/>
  <input type="hidden" id="cntNum" name="cntNum" value=""/>
  <input type="submit" style="display: none;" />
  
  <!-- container -->
  <div id="container"  class="content"  > 
    
    <!-- seach -->
    <div class="seach_box" >
      <h2 >검색항목 </h2>
      <div>
        <table border="0" cellpadding="0" cellspacing="0" class="table table-search">
          <caption>
          조회양식
          </caption>
          <colgroup>
          <col width="114px">
          <col width="*">
          </colgroup>
          <tbody>
            <tr>
              <th>관리기관</th>
              <td><select     name="searchCondition1"  id="searchCondition1">
                  <option value="">전체</option>
					<c:forEach var="cdeList" items="${dataCodeList}" varStatus="status">
						<option value="${cdeList.codeCode }" ${param.searchCondition1 == cdeList.codeCode ? "selected" : "" } >${cdeList.codeAlias}</option>
					</c:forEach>	
                </select>
                </td>
            </tr>
            <tr>
              <th>관리번호</th>
              <td>
				<input type="text" name="searchCondition2" id="searchCondition2" maxlength="10" value="${param.searchCondition2 }" onchange="onlyNumber(this);" onkeyup="onlyNumber(this);" onkeypress="captureReturnKey(event)"/>
             	</td>
            </tr>
            <tr>
              <th>행정동</th>
              <td>
				<select name="searchCondition3" id="searchCondition3"    >
					<option value="">전체</option>
					<c:forEach var="cmtList" items="${cmtAdarMaList}" varStatus="status">
						<option value="${cmtList.hjdCde }" ${param.searchCondition3 == cmtList.hjdCde ? "selected" : "" } >${cmtList.hjdNam}</option>
					</c:forEach>
				</select>
               </td>
            </tr>
            <tr>
              <th>공사번호</th>
              <td>
				<input type="text" name="searchCondition11" id="searchCondition11" maxlength="50" value="${param.searchCondition11 }" onkeypress="captureReturnKey(event)" />
              </td>
            </tr>
            <tr>
              <th>도엽번호</th>
              <td>
				<input type="text" name="searchCondition4" id="searchCondition4" maxlength="50" value="${param.searchCondition4 }" onkeypress="captureReturnKey(event)" />
              </td>
            </tr>
            <tr>
              <th>설치일자 [이상]</th>
              <td><input type="text" class="datepicker"  name="searchCondition5" id="searchCondition5" value="${param.searchCondition5 }" onkeypress="captureReturnKey(event)" /></td>
            </tr>
            <tr>
              <th>설치일자 [이하]</th>
              <td><input type="text" class="datepicker" name="searchCondition6" id="searchCondition6" value="${param.searchCondition6 }" onkeypress="captureReturnKey(event)" /></td>
            </tr>
            
            <tr>
              <th>형식</th>
              <td>
				<select name="searchCondition7" id="searchCondition7"  >
					<option value="">전체</option>
					<c:forEach var="mofCdeList" items="${mofCdeList}" varStatus="status">
						<option value="${mofCdeList.codeCode }" ${param.searchCondition7 == mofCdeList.codeCode ? "selected" : "" } >${mofCdeList.codeAlias}</option>
					</c:forEach>
				</select>
               </td>
            </tr>
            <tr>
              <th>관재질</th>
              <td>
				<select name="searchCondition8" id="searchCondition8"  >
					<option value="">전체</option>
					<c:forEach var="mopCdeList" items="${mopCdeList}" varStatus="status">
						<option value="${mopCdeList.codeCode }" ${param.searchCondition8 == mopCdeList.codeCode ? "selected" : "" } >${mopCdeList.codeAlias}</option>
					</c:forEach>
				</select>
               </td>
            </tr>
            <tr>
              <th>구경</th>
              <td>
				<input type="text" id="searchCondition9" name="searchCondition9" class="num_style" maxlength="50" value="${param.searchCondition9}" onkeypress="captureReturnKey(event)"/>
              </td>
            </tr>
            <tr>
              <th>표고</th>
              <td>
				<input type="text" id="searchCondition10" name="searchCondition10" class="num_style" maxlength="50" value="${param.searchCondition10}" onkeypress="captureReturnKey(event)"/>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="btn_group m-t-10 "  >
        <button id="btnReset" type="button" class="btn btn-search " style="width:114px"><img src="/fms/images/icon_btn_reset.png" width="16" height="16" alt=""/> 초기화 </button>
        <button id="btnSearch" type="button" class="btn btn-search " style="width:132px"><img src="/fms/images/icon_btn_search.png" width="16" height="16" alt=""/> 검색 </button>
      </div>
    </div>
    <!-- //seach --> 
    
    <!-- list -->
    <div class="list_box">
      <h2 ><span style="float:left">목록 | 스탠드파이프(<fmt:formatNumber value="${totCnt}" pattern="#,###" />건)</span>
        <div class="btn_group m-b-5 right " >
          <button  id="btnCreate" name="btnCreate" type="button" class="btn btn-default " > <img src="/fms/images/icon_btn_w_regist.png" width="16" height="16" alt=""/>등록 </button>
          <button id="btnExcelDown" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_download.png" width="16" height="16" alt=""/> 다운로드 </button>
        </div>
      </h2>
      <div class="list " >
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-striped table-list center " style="white-space: nowrap;">
          <colgroup>
          <col width="">
          </colgroup>
          <thead>
            <tr id="excel_head">
				<th tit="">위치</th>
				<th tit="ftrCdeNam">지형지물</th>
				<th tit="mngCdeNam">관리기관</th>
				<th tit="ftrIdn">관리번호</th>
				<th tit="hjdCdeNam">행정동</th>
				<th tit="cntNum">공사번호</th>
				<th tit="shtNum">도엽번호</th>
				<th tit="istYmdFmt">설치일자</th>
				<th tit="mofCdeNam">형식</th>
				<th tit="mopCdeNam">관재질</th>
				<th tit="stdDip" typ="number">구경</th>
				<th tit="stpAlt" typ="number">표고</th>
            </tr>
          </thead>
          <tbody>
          
			<c:forEach var="list" items="${resultList}" varStatus="listIndex">									
				<tr>
					<c:if test="${fn:length(gisLayerNm) > 1}">
						<td class="dtlMap"   style="cursor:pointer" ogrFid='${list.ogrFid}'><img src="${rscRoot}/dist/images/search.png" alt="위치이동" /></td>
						<td><a href="javascript:editWaterPipe('${gisLayerNm}','${list.ftrCde}','${list.ftrIdn}')">${mapFtrcMa[list.ftrCde]}</a>&nbsp; &nbsp;</td>
						<td><a href="javascript:editWaterPipe('${gisLayerNm}','${list.ftrCde}','${list.ftrIdn}')">${mapMngCde[list.mngCde]}</a>&nbsp; &nbsp;</td>
						<td><a href="javascript:editWaterPipe('${gisLayerNm}','${list.ftrCde}','${list.ftrIdn}')">${list.ftrIdn}</a>&nbsp; &nbsp;</td>
						<td><a href="javascript:editWaterPipe('${gisLayerNm}','${list.ftrCde}','${list.ftrIdn}')">${mapAdarMa[list.hjdCde]}</a>&nbsp; &nbsp;</td>
						<td><a href="javascript:editWaterPipe('${gisLayerNm}','${list.ftrCde}','${list.ftrIdn}')">${list.cntNum}</a>&nbsp; &nbsp;</td>
						<td><a href="javascript:editWaterPipe('${gisLayerNm}','${list.ftrCde}','${list.ftrIdn}')">${list.shtNum}</a>&nbsp; &nbsp;</td>
						<td>
							<a href="javascript:editWaterPipe('${gisLayerNm}','${list.ftrCde}','${list.ftrIdn}')">
								<c:if test="${fn:length(list.istYmd) < 8}">
									${list.istYmd}
								</c:if>
								<c:if test="${fn:length(list.istYmd) >= 8}">														
									<fmt:parseDate value="${list.istYmd}" var="dateFmt" pattern="yyyyMMdd"/>
   										<fmt:formatDate value="${dateFmt}"  pattern="yyyy-MM-dd"/>	
								</c:if>												
							</a>&nbsp; &nbsp;
						</td>
						<td><a href="javascript:editWaterPipe('${gisLayerNm}','${list.ftrCde}','${list.ftrIdn}')">${mapMofCde[list.mofCde]}</td>
						<td><a href="javascript:editWaterPipe('${gisLayerNm}','${list.ftrCde}','${list.ftrIdn}')">${mapMopCde[list.mopCde]}</td>
						<td><a href="javascript:editWaterPipe('${gisLayerNm}','${list.ftrCde}','${list.ftrIdn}')">${list.stdDip}</td>
						<td><a href="javascript:editWaterPipe('${gisLayerNm}','${list.ftrCde}','${list.ftrIdn}')">${list.stpAlt}</td>
					</c:if>
					<c:if test="${fn:length(gisLayerNm) < 1}">
						<c:choose>
							<c:when test="${list.isGeometry eq 'Y'}">
								<td class="dtlMap"   style="cursor:pointer" ogrFid='${list.ogrFid}'><img src="${rscRoot}/dist/images/search.png" alt="위치이동" /></td>
							</c:when>
							<c:otherwise>
								<td>&nbsp; &nbsp;</td>
							</c:otherwise>
						</c:choose>
						<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' >${mapFtrcMa[list.ftrCde]}</td>
						<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' >${mapMngCde[list.mngCde]}</td>
						<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' >${list.ftrIdn}</td>
						<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' >${mapAdarMa[list.hjdCde]}</td>
						<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' >${list.cntNum}</td>
						<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' >${list.shtNum}</td>
						<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' >		
											
							<c:if test="${fn:length(list.istYmd) < 8}">
								${list.istYmd}
							</c:if>
							<c:if test="${fn:length(list.istYmd) >= 8}">														
								<fmt:parseDate value="${list.istYmd}" var="dateFmt" pattern="yyyyMMdd"/>
  										<fmt:formatDate value="${dateFmt}"  pattern="yyyy-MM-dd"/>	
							</c:if>
						</td>
						<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' >${mapMofCde[list.mofCde]}</td>
						<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' >${mapMopCde[list.mopCde]}</td>
						<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' >${list.stdDip}</td>
						<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' >${list.stpAlt}</td>
					</c:if>
				</tr>
			</c:forEach>
			<c:if test="${empty resultList}">
				<tr>
					<td colspan="12" style="color:black;text-align:center;">검색 결과가 없습니다.</td>
				</tr>
			</c:if>			          


          
          </tbody>
        </table>
      </div>
		<!-- Paging -->
      <div style="width:100%; text-align:center">
        <ul class="pagination pagination-sm  " >
			<ui:pagination paginationInfo = "${paginationInfo}"  type="image" jsFunction="fn_search" />
        </ul>
    
      </div>

      
      
    </div>
    <!-- //list --> 
    
  </div>
  
  <!-- //container --> 
  
</form>
  
</div>


</body>
</html>