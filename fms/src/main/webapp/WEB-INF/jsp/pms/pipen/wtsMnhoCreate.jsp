<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<script type="text/javascript">

var popupX = (screen.availWidth-660)/2;
var popupY = (screen.availHeight-1130)/3;

var saveLink = "${ctxRoot}/pms/pipen/wtsmnho/manh/addManhSave.do"; 

$(document).ready(function(){
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();
	
	var msg = '${msg}';
	
	if(msg) {
		gfn_alert(msg,"",function(){
			window.close();	
		});		
	}		
		
	$("#btnAddLocation").hide();	//위치등록 기능없음.
	 /*******************************************
	 * 이벤트 설정
	 *******************************************/	
	
	$('#btnSave').click(function() {
				
		if(!gfn_formValid("wtsMnhoFrm")) return;
		
	    gfn_confirm("저장하시겠습니까?", function(ret){
			if(!ret) return;
			
			// 컨트롤러 지정방식
			gfn_saveForm("wtsMnhoFrm", "/fms/pms/pipen/wtsmnho/insertWtsMnho.do" , function(data){
				
				if(data.result){
					if('${type}' == 'edit'){
						gfn_alert("데이터가 저장되었습니다. 위치등록을 해주세요.","",function(){
							opener.document.getElementById('ftrIdn').value = data.ftrIdn; 
							window.close();
						});
					}
					else{
						gfn_alert("데이터가 저장되었습니다.","",function(){
							opener.fn_search();
							window.close();
						});
					}
				}else{
					gfn_alert("데이터 저장에 실패하였습니다. : " + data.error,"E");
				}
			});
		});		
	});
		
	$('#cntNum').click(function () {
		if ($('#cntNum').val() != '') {
			gfn_confirm("상수공사대장을 변경하시겠습니까?", function(ret){	    	
				if(!ret) return;
				
				window.open('${ctxRoot}/pms/popup/cnstMngPopup.do', "_blank"
						, 'status=no, width=1100, height=540, left='+ popupX + ', top='+ popupY +', resizable=no');
			});	
		}else{
			window.open('${ctxRoot}/pms/popup/cnstMngPopup.do', "_blank"
					, 'status=no, width=1100, height=540, left='+ popupX + ', top='+ popupY +', resizable=no');	
		}		
	});
});	

function setChildValue(name) {
	document.getElementById("cntNum").value = name;
}

</script>

</head>

<body>


<div id="wrap" class="wrap" > 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1 >상수맨홀 대장 등록정보</h1>
  </div>
  <!-- // header --> 
  
<form name="wtsMnhoFrm" id="wtsMnhoFrm" method="post">
	<input type="hidden" id="ftrCde" name="ftrCde" value="SA100"/>
	<input type="hidden" id="type" name="type" value="${type}"/>
  
  <!-- container -->
  <div id="container"  class="content"  > 
    
    <!-- list -->
    <div class="view_box">
      <h2 ><span style="float:left">일반정보 </span>
	      <div class="btn_group m-b-5 right "  >
             <button id="btnSave" class="btn btn-default " type="button" > 
             	<img src="/fms/images/icon_btn_save.png" width="16" height="16" alt=""/>저장
             </button>
             <button id="btnAddLocation" class="btn btn-default " type="button" onclick="addLocation();" > 
             	<img src="/fms/images/icon_btn_save.png" width="16" height="16" alt=""/>위치등록
             </button>
             
          </div>
      </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
          <colgroup>
          <col width="115px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          </colgroup>

          <tbody>  
			<tr>
				<th class="req"> 관리번호</th>
				<td>
					<input type="text" id="ftrIdn" name="ftrIdn" value="${ftrIdnVal.ftrIdn }" onkeydown="noHanPress(this);" readonly class="disable reqVal" />
				</td>
				<th class="req"> 행정동</th>
				<td>
					<select name="hjdCde" id="hjdCde" class="reqVal" style="width:100%">
						<option value="">전체</option>
						<c:forEach var="cmtList" items="${cmtAdarMaList}" varStatus="status">
							<option value="${cmtList.hjdCde }" >${cmtList.hjdNam}</option>
						</c:forEach>
					</select>
				</td>
				<th> 공사번호</th>
				<td>
					<input type="text" id="cntNum" name="cntNum" />
				</td>
				<th> 관리기관</th>
				<td>
					<select id="mngCde" name="mngCde" style="width:100%">
						<option value="">전체</option>
						<c:forEach var="cdeList" items="${dataCodeList}" varStatus="status">
							<option value="${cdeList.codeCode }" >${cdeList.codeAlias}</option>
						</c:forEach>	
					</select>
				</td>
			</tr>
			<tr>				
				<th> 도엽번호</th>
				<td>
					<input type="text" id="shtNum" name="shtNum" />
				</td>
				<th> 설치일자</th>
				<td>
					<input type="text" class="datepicker" id="istYmd" name="istYmd"/>
				</td>
				<th >-</th>
				<td><input type="text" id="" name="" value="" readonly class="disable" /></td>
				<th >-</th>
				<td><input type="text" id="" name="" value="" readonly class="disable" /></td>
			</tr>
          </tbody>
        </table>
      </div>
    </div>
    <!-- //list --> 

    
    <!-- list -->
    
    <div class="view_box">
      <h2 class="m-b-5">시설현황 정보 </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
          <colgroup>
          <col width="115px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          </colgroup>
          <tbody>
			<tr>
				<th >규격</th>
				<td>									
					<input type="text" id="manStd" name="manStd" />
				</td>
				<th >맨홀종류</th>
				<td>
					<select id="somCde" name="somCde" style="width:100%">
						<option value="">선택안함</option>
						<c:forEach var="somCdeList" items="${somCdeList}" varStatus="status">
							<option value="${somCdeList.codeCode }" >${somCdeList.codeAlias}</option>
						</c:forEach>	
					</select>
				</td>
				<th >맨홀형태</th>
				<td>								
					<select id="mhsCde" name="mhsCde" style="width:100%">
						<option value="">선택안함</option>
						<c:forEach var="mhsCdeList" items="${mhsCdeList}" varStatus="status">
							<option value="${mhsCdeList.codeCode }" >${mhsCdeList.codeAlias}</option>
						</c:forEach>	
					</select>
				</td>
				<th >대장초기화여부</th>
				<td>
					<select id="sysChk" name="sysChk" style="width:100%">
						<option value="0">무</option>
						<option value="1">유</option>
					</select>
				</td>
			</tr>
			<tr>
				<th class="num">방향각</th>
				<td>
					<input type="text" class="numVal" id="angDir" name="angDir"/>
				</td>
				<th >-</th>
				<td><input type="text" id="" name="" value="" readonly class="disable" /></td>
				<th >-</th>
				<td><input type="text" id="" name="" value="" readonly class="disable" /></td>
				<th >-</th>
				<td><input type="text" id="" name="" value="" readonly class="disable" /></td>
			</tr>	
		</tbody>
        </table>
      </div>
    </div>
    <!-- //list -->     
  </div>
  <!-- //container --> 
  
	</form>
</div>
<!-- //UI Object -->

</body>
</html>