<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<script type="text/javascript">

var popupX = (screen.availWidth-660)/2;
var popupY = (screen.availHeight-1130)/3;

$(document).ready(function() {
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
 	gfn_init();
 
 
	 /*******************************************
	 * 이벤트 설정
	 *******************************************/
	var msg = '${msg}';
		
	if(msg) {
		gfn_alert(msg,"",function(){
			window.close();
		});		
	}		
		
	$('#btnSave').click(function() {
		
	    if(!gfn_formValid("pipeLmFrm"))	return;	    

	    gfn_confirm("저장하시겠습니까?", function(ret){	    	
			if(!ret) return;
			
			// 컨트롤러 지정방식
			gfn_saveForm("pipeLmFrm", "/fms/pms/pipen/insertWtpipMng.do" , function(data){
				
				if(data.result){
					if('${type}' == 'edit'){
						gfn_alert("데이터가 저장되었습니다. 위치등록을 해주세요.","",function(){
							opener.document.getElementById('ftrIdn').value = data.ftrIdn;
							window.close();
						});
					}
					else{
						gfn_alert("데이터가 저장되었습니다.","",function(){
							opener.fn_search();
							window.close();
						});
					}
				}else{
					gfn_alert("데이터 저장에 실패하였습니다. : " + data.error,"E");
				}
			});
		});
	});	
	 
	 $('#cntNum').click(function () {
		if ($('#cntNum').val() != '') {
			
			gfn_confirm("상수공사대장을 변경하시겠습니까?", function(ret){	    	
				if(!ret) return;
				
				window.open('${ctxRoot}/pms/popup/cnstMngPopup.do', "_blank"
						, 'status=no, width=1100, height=540, left='+ popupX + ', top='+ popupY +', resizable=no');
			});
		}else{
			window.open('${ctxRoot}/pms/popup/cnstMngPopup.do', "_blank"
					, 'status=no, width=1100, height=540, left='+ popupX + ', top='+ popupY +', resizable=no');	
		}				
	});
	
	
});	


/*공사번호 선택 콜백*/
function setChildValue(name) {
	document.getElementById("cntNum").value = name;
}

/* GIS등록 */
function addLocation () {
	opener.allBtnInactive();
	opener.drawFeature();
	$("#edit_save").hide();
}
</script>

</head>


<body>

<div id="wrap" class="wrap" > 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1 >상수관로 대장 등록정보</h1>
  </div>
  <!-- // header --> 
  
<form name="pipeLmFrm" id="pipeLmFrm" method="post">
  <input type="hidden" id="ftrCde" name="ftrCde" value="SA001"/>
  <input type="hidden" id="ftrIdn" name="ftrIdn" value="${wtlPipeLm.ftrIdn}" />
  <input type="hidden" id="type" name="type" value="${type}"/>
  
  <!-- container -->
  <div id="container"  class="content"  > 
    
    <!-- list -->
    <div class="view_box">
      <h2 ><span style="float:left">일반정보 </span>
	      <div class="btn_group m-b-5 right "  >
             <button id="btnSave" class="btn btn-default " type="button" > 
             	<img src="/fms/images/icon_btn_save.png" width="16" height="16" alt=""/>저장
             </button>
          </div>
      </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
          <colgroup>
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          </colgroup>

          <tbody>
			<tr>
				<th class="req">관리번호</th>
				<td>
					<input type="text" id="ftrIdn" name="ftrIdn" value="${wtlPipeLm.ftrIdn}" onkeydown="noHanPress(this);" maxlength="8" readonly class="disable reqVal"  />
				</td>
				<th class="req">행정동</th>
				<td>
					<select id="hjdCde" name="hjdCde" class="reqVal" style="width:100%;">
						<option value="">전체</option>
						<c:forEach var="cmtList" items="${cmtAdarMaList}" varStatus="status">
							<option value="${cmtList.hjdCde }">${cmtList.hjdNam}</option>
						</c:forEach>
					</select>
				</td>
				<th >공사번호</th>
				<td>
					<input type="text" id="cntNum" name="cntNum" maxlength="10" />
				</td>
				<th >관리기관</th>
				<td>
					<select id="mngCde" name="mngCde" style="width:100%;">
						<option value="">전체</option>
						<c:forEach var="cdeList" items="${dataCodeList}" varStatus="status">
							<option value="${cdeList.codeCode }">${cdeList.codeAlias}</option>
						</c:forEach>	
					</select>
				</td>
			</tr>
			<tr>				
				<th >도엽번호</th>
				<td>
					<input type="text" id="shtNum" name="shtNum" maxlength="10" />
				</td>
				<th >설치일자</th>
				<td>
					<input type="text" class="datepicker" id="istYmd" name="istYmd" />
				</td>
				<th > -</th>
				<td>
					<input type="text" id="" name="" readonly class="disable" />
				</td>
				<th > -</th>
				<td>
					<input type="text" id="" name="" readonly class="disable" />
				</td>
			</tr>
          </tbody>
        </table>
      </div>
    </div>
    <!-- //list --> 

    
    <!-- list -->
    
    <div class="view_box">
      <h2 class="m-b-5">시설현황 정보 </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
          <colgroup>
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          </colgroup>
          <tbody>
			<tr>
				<th >관용도</th>
				<td>
					<select id="saaCde" name="saaCde" style="width:100%;">
						<option value="">선택안함</option>
						<c:forEach var="saaCdeList" items="${saaCdeList}" varStatus="status">
							<option value="${saaCdeList.codeCode }">${saaCdeList.codeAlias}</option>
						</c:forEach>	
					</select>
				</td>
				<th >접합종류</th>
				<td>
					<select id="jhtCde" name="jhtCde" style="width:100%;">
						<option value="">선택안함</option>
						<c:forEach var="jhtCdeList" items="${jhtCdeList}" varStatus="status">
							<option value="${jhtCdeList.codeCode }">${jhtCdeList.codeAlias}</option>
						</c:forEach>						
					</select>
				</td>
				<th >관재질</th>
				<td>
					<select id="mopCde" name="mopCde" style="width:100%;">
						<option value="">선택안함</option>
						<c:forEach var="mopCdeList" items="${mopCdeList}" varStatus="status">
							<option value="${mopCdeList.codeCode }">${mopCdeList.codeAlias}</option>
						</c:forEach>
					</select>
				</td>
				<th class="num2">최저깊이(m)</th>
				<td>
					<input type="text" class="numVal2" id="lowDep" name="lowDep" maxlength="6" />
				</td>				
			</tr>
			<tr>				
				<th class="num2">구경</th>
				<td>
					<input type="text" class="numVal2" id="pipDip" name="pipDip" maxlength="100"  />
				</td>				
				<th class="num2">최고깊이(m)</th>
				<td>
					<input type="text" class="numVal2" id="hghDep" name="hghDep" maxlength="6" />
				</td>
				<th class="num2">연장(m)</th>
				<td>
					<input type="text" class="numVal2" id="pipLen" name="pipLen" maxlength="14" />
				</td>
				<th >관라벨</th>
				<td>
					<input type="text" id="pipLbl" name="pipLbl" maxlength="50" />
				</td>
			</tr>
			<tr>
				<th >대장초기화여부</th>
				<td>
					<select id="sysChk" name="sysChk" style="width:100%;">
						<option value="0" ${wtlPipeLm.sysChk == "0" ? "selected" : "" } >무</option>
						<option value="1" ${wtlPipeLm.sysChk == "1" ? "selected" : "" } >유</option>
					</select>
				</td>
				<th> -</th>
				<td>
					<input type="text" id="" name="" readonly class="disable" />
				</td>
				<th> -</th>
				<td>
					<input type="text" id="" name="" readonly class="disable" />
				</td>
				<th> -</th>
				<td>
					<input type="text" id="" name="" readonly class="disable" />
				</td>				
			</tr>
			
		</tbody>
        </table>
      </div>
    </div>
    <!-- //list -->     
  </div>
  <!-- //container --> 
  
	</form>
</div>
<!-- //UI Object -->

</body>
</html>