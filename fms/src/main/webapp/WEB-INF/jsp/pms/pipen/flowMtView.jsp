<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<script type="text/javascript">

var popupX = (screen.availWidth-660)/2;
var popupY = (screen.availHeight-1130)/3;

$(document).ready(function() {
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();		
	
	/*******************************************
	 * 이벤트 설정
	 *******************************************/
	 
	var msg = '${msg}';
		
	if(msg) {
		gfn_alert(msg,"",function(){
			window.close();	
		});		
	}
				
	// 탭선택이벤트
	$(".tab-menu>li>a").click(function(){
		//탭타이틀 처리
		$(".tab-menu>li>a").not(this).removeClass("active");
		if($(this).not(":visible")){
			$(this).addClass("active");	
		}
		
		//탭본문처리
		$(".tab-list").removeClass("active");
		$("#"+$(this).attr("abbr")).addClass("active");
	});

	
	$('#btnSave').click(function() {
		
		if(!gfn_formValid("flowMtFrm")) return;
						
		gfn_confirm('저장하시겠습니까?',function(ret){			
			if(!ret) return;
			
			gfn_saveFormCmm("flowMtFrm", "updateWtlFlowPs", function(data){
				if(data.result){
					gfn_alert("데이터가 저장되었습니다.","",function(){
						opener.fn_search();
						fn_reload();
					});
				}else{
					gfn_alert("데이터 저장에 실패하였습니다. : " + data.error,"E");
				}
			});			
		});
		
	});
	
	$('#btnDel').click(function() {
		
		gfn_selectList({sqlId : 'selectCmmChscResSubList',	//유지보수(점검시설 결과)
						sqlId2: 'selectFileMapList',		//사진첨부 / 참조자료
					    ftrCde: '${ftrCde}',
					    ftrIdn: '${ftrIdn}',
					    bizId : '${ftrCde}${ftrIdn}'
					    }, function(ret){
			
			if(ret.data != null && ret.data.length > 0){
				gfn_alert("유지보수 내역이 있습니다.","W");
				return false;
			}
			
			if(ret.data2 != null && ret.data2.length > 0){
				gfn_alert("사진첨부/참조자료 내역이 있습니다.","W");
				return false;
			}
							
			gfn_confirm("삭제하시겠습니까?", function(ret){
				if(!ret) return;
					
				gfn_saveFormCmm("flowMtFrm", "deleteWtlFlowPs", function(data){
					if(data.result){
						gfn_alert("데이터가 삭제되었습니다.","",function(){
							opener.fn_search();
							window.close();
						});
					}else{
						gfn_alert("데이터 삭제가 실패하였습니다. : " + data.error,"E");
					}
				});
			});
		});
		
	});

	$('#cntNum').click(function () {
		if ($('#cntNum').val() != '') {			
			gfn_confirm("상수공사대장을 변경하시겠습니까?", function(ret){
				if(!ret) return false;
				
				window.open('${ctxRoot}/pms/popup/cnstMngPopup.do', "_blank"
						, 'status=no, width=1100, height=540, left='+ popupX + ', top='+ popupY +', resizable=no');
			});	
		}else{
			window.open('${ctxRoot}/pms/popup/cnstMngPopup.do', "_blank"
					, 'status=no, width=1100, height=540, left='+ popupX + ', top='+ popupY +', resizable=no');
		}
		
	});
	
	$("#btnPrint").click(function(){		
		
        $('<form>', {
            "id": "flowMtList",
            "html": '<input type="text" id="ftrCde" name="ftrCde" value="${ftrCde}" />'
            		+'<input type="text" id="ftrIdn" name="ftrIdn" value="${ftrIdn}" />'
            		+'<input type="text" id="barCde" name="barCde" value="${ftrCde}${ftrIdn}" />',
            "action": '/fms/pms/pdf/flowMtList.do'
        }).appendTo(document.body).submit();		
	});	
});

var fn_reload = function(){
	location.reload();
}

function setChildValue(name) {
	document.getElementById("cntNum").value = name;
}

</script>

</head>


<body>


<div id="wrap" class="wrap" > 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1 >유량계 대장 상세정보</h1>
  </div>
  <!-- // header --> 
  
<form name="flowMtFrm" id="flowMtFrm" method="post">
  <input type="hidden" id="ftrCde" name="ftrCde" value="${resultVO.ftrCde }" />
  <!-- container -->
  <div id="container"  class="content"  > 
    
    <!-- list -->
    <div class="view_box">
      <h2 ><span style="float:left">일반정보</span>
        <div class="btn_group m-b-5 right "  >
          <button id="btnPrint" class="btn btn-default " type="button"> <img src="/fms/images/icon_btn_print.png" width="16" height="16" alt=""/>인쇄 </button>
          <button id="btnDel" class="btn btn-default " type="button" > <img src="/fms/images/icon_btn_delete.png" width="16" height="16" alt=""/>삭제</button>
          <button id="btnSave" class="btn btn-default " type="button" > <img src="/fms/images/icon_btn_save.png" width="16" height="16" alt=""/>저장</button>
        </div>
      </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
          <colgroup>
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          </colgroup>

          <tbody>
			<tr>
				<th class="req"> 관리번호</th>
				<td>
					<input type="text" id="ftrIdn" name="ftrIdn" value="${resultVO.ftrIdn }" readonly class="disable reqVal" />
				</td>
				<th class="req"> 행정동</th>
				<td>
					<select id="hjdCde" name="hjdCde" class="reqVal" style="width:100%">
						<option value="">전체</option>
						<c:forEach var="cmtList" items="${cmtAdarMaList}" varStatus="status">
							<option value="${cmtList.hjdCde }" ${resultVO.hjdCde == cmtList.hjdCde ? "selected" : "" } >${cmtList.hjdNam}</option>
						</c:forEach>
					</select>
				</td>
				<th >공사번호</th>
				<td>
					<input type="text" id="cntNum" name="cntNum" value="${resultVO.cntNum }"/>
				</td>
				<th >관리기관</th>
				<td>
					<select id="mngCde" name="mngCde" style="width:100%">
						<option value="">전체</option>
						<c:forEach var="cdeList" items="${dataCodeList}" varStatus="status">
							<option value="${cdeList.codeCode }" ${resultVO.mngCde == cdeList.codeCode ? "selected" : "" } >${cdeList.codeAlias}</option>
						</c:forEach>	
					</select>
				</td>
				
			</tr>
			<tr>
				<th >도엽번호</th>
				<td>
					<input type="text" id="shtNum" name="shtNum" value="${resultVO.shtNum }"/>
				</td>
				<th >설치일자</th>
				<td>
					<input type="text" class="datepicker" id="istYmd" name="istYmd" value="${resultVO.istYmd}"/>
				</td>
				<th > -</th>
				<td><input type="text" id="" name="" value="" readonly class="disable" /></td>				
				<th > -</th>
				<td><input type="text" id="" name="" value="" readonly class="disable" /></td>
			</tr>
          </tbody>
        </table>
      </div>
    </div>
    <!-- //list --> 
    <!-- list -->
    <div class="view_box">
      <h2 class="m-b-5">시설현황 정보 </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
          <colgroup>
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          </colgroup>
          <tbody>
			<tr>
				<th >유량계 종류</th>
				<td>
					<select id="gagCde" name="gagCde" style="width:100%">
						<option value="">선택안함</option>
						<c:forEach var="gagCdeList" items="${gagCdeList}" varStatus="status">
							<option value="${gagCdeList.codeCode }" ${resultVO.gagCde == gagCdeList.codeCode ? "selected" : "" }>${gagCdeList.codeAlias}</option>
						</c:forEach>	
					</select>
				</td>
				<th >형식</th>
				<td>		
					<select id="mofCde" name="mofCde" style="width:100%">
						<option value="">선택안함</option>
						<c:forEach var="mofCdeList" items="${mofCdeList}" varStatus="status">
							<option value="${mofCdeList.codeCode }" ${resultVO.mofCde == mofCdeList.codeCode ? "selected" : "" }>${mofCdeList.codeAlias}</option>
						</c:forEach>	
					</select>
				</td>
				<th class="num2">구경</th>
				<td>		
					<input type="text" class="numVal2" id="floDip" name="floDip"  value="${resultVO.floDip }"/>	
				</td>			
				<th >제작회사명</th>
				<td> 
					<input type="text" id="prdNam" name="prdNam" value="${resultVO.prdNam }"/>
				</td>
				
			</tr>
			<tr>				
				<th >관로지형지물부호</th>
				<td>
					<input type="text" id="pipCde" name="pipCde" value="${resultVO.pipCde }"/>
				</td>
				<th >관로관리번호</th>
				<td>
					<input type="text" id="pipIdn" name="pipIdn" value="${resultVO.pipIdn }"/>
				</td>
				<th >대장초기화여부</th>
				<td>
					<select id="sysChk" name="sysChk" style="width:100%">
						<option value="0" ${resultVO.sysChk == "0" ? "selected" : "" } >무</option>
						<option value="1" ${resultVO.sysChk == "1" ? "selected" : "" } >유</option>
					</select>
				</td>
				<th class="num">방향각</th>
				<td>
					<input type="text" class="numVal" id="angDir" name="angDir" value="${resultVO.angDir }" />
				</td>							
			</tr>
			
		</tbody>
        </table>
      </div>
    </div>
     
    <!-- //list --> 
    
    <!-- tab -->
    <div class=" tab-box">
	
      <div class="tab-menu">
      
        <ul class="tab-menu">
			<li><a href="#" abbr="tab01" class="active">유지보수</a></li>
			<li><a href="#" abbr="tab02">사진첨부</a></li>
			<li><a href="#" abbr="tab03">참조자료</a></li>
		</ul>
							
	  </div>
    
    
      <!--tab list-->
      
		<!-- 유지보수     	 -->
		<div  id="tab01" class="tab-list active" >
			<c:import url="/pms/link/lnkChscList.do"/>
		</div>
      				
		<!-- 사진첨부  -->
		<div  id="tab02" class="tab-list" >
			<c:import url="/pms/link/lnkDwgPhotoList.do?bizId=${ftrCde}${ftrIdn}"/>
		</div>
		
		<div  id="tab03" class="tab-list"  >
			<c:import url="/pms/link/lnkCnstRefList.do?bizId=${ftrCde}${ftrIdn}"/>
		</div>

      <!--/tab list-->

    </div>
    <!-- //tab-box -->  
    
  </div>
  <!-- //container --> 
  
	</form>
</div>
<!-- //UI Object -->

</body>
</html>