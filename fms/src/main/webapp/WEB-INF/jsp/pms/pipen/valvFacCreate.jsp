<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<script type="text/javascript">

var popupX = (screen.availWidth-660)/2;
var popupY = (screen.availHeight-1130)/3;

$(document).ready(function() {
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();
		
	 /*******************************************
	 * 이벤트 설정
	 *******************************************/
		
	$('#btnSave').click(function() {
		
		if(!gfn_formValid("valvFacFrm"))	return;		    

		//저장여부 메시지
	    gfn_confirm("저장하시겠습니까?", function(ret){			
	    	if(!ret) return;
	    	
			gfn_saveFormCmm("valvFacFrm", "insertWtlValvPs", function(data){
				if(data.result){
					if('${type}' == 'edit'){
						gfn_alert("데이터가 저장되었습니다. 위치등록을 해주세요.","",function(){
							opener.document.getElementById('ftrIdn').value = $('#ftrIdn').val(); 
							window.close();
						});
					}
					else{
						gfn_alert("데이터가 저장되었습니다.","",function(){
							opener.fn_search();
							window.close();
						});
					}
				}else{
					gfn_alert("데이터 저장에 실패하였습니다. : " + data.error,"E");
				}
			});	    
		});		
	});

	$('#cntNum').click(function () {
		if ($('#cntNum').val() != '') {
			//저장여부 메시지
		    gfn_confirm("상수공사대장을 변경하시겠습니까?", function(ret){	    	
				if(!ret) return;			
				
				window.open('${ctxRoot}/pms/popup/cnstMngPopup.do', "_blank"
						, 'status=no, width=1100, height=540, left='+ popupX + ', top='+ popupY +', resizable=no');
			});		
		}else{
			window.open('${ctxRoot}/pms/popup/cnstMngPopup.do', "_blank"
					, 'status=no, width=1100, height=540, left='+ popupX + ', top='+ popupY +', resizable=no');
		}
		
	});
});	

function setChildValue(name) {
	document.getElementById("cntNum").value = name;
}

</script>

</head>
<body>

<div id="wrap" class="wrap" > 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1 >변류시설 대장 등록정보</h1>
  </div>
  <!-- // header --> 
  
<form name="valvFacFrm" id="valvFacFrm" method="post">
  
  <!-- container -->
  <div id="container"  class="content"  > 
    
    <!-- list -->
    <div class="view_box">
      <h2 ><span style="float:left">일반정보 </span>
	      <div class="btn_group m-b-5 right "  >
             <button id="btnSave" class="btn btn-default " type="button" > 
             	<img src="/fms/images/icon_btn_save.png" width="16" height="16" alt=""/>저장
             </button>
          </div>
      </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
          <colgroup>
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          </colgroup>

          <tbody>
			<tr>
				<th class="req"> 지형지물</th>
				<td>
					<select id="ftrCde" name="ftrCde" class="reqVal" style="width:100%;">
						<c:forEach var="cmtFtrcMaList" items="${cmtFtrcMaList}" varStatus="status">
							<option value="${cmtFtrcMaList.ftrCde }" ${"SA200" == cmtFtrcMaList.ftrCde ? "selected" : "" } >${cmtFtrcMaList.ftrNam}</option>
						</c:forEach>
					</select>
				</td>
				<th class="req"> 관리번호</th>
				<td>
					<input type="text" id="ftrIdn" name="ftrIdn" value="${ftrIdnVal.ftrIdn }" onkeydown="noHanPress(this);" readonly class="disable reqVal" />
				</td>
				<th class="req"> 행정동</th>
				<td>
					<select name="hjdCde" id="hjdCde" class="reqVal" style="width:100%;">
						<option value="">선택안함</option>
						<c:forEach var="cmtList" items="${cmtAdarMaList}" varStatus="status">
							<option value="${cmtList.hjdCde }" >${cmtList.hjdNam}</option>
						</c:forEach>
					</select>
				</td>				
				<th> 관리기관</th>
				<td>
					<select id="mngCde" name="mngCde" style="width:100%;">
						<option value="">선택안함</option>
						<c:forEach var="cdeList" items="${dataCodeList}" varStatus="status">
							<option value="${cdeList.codeCode }" >${cdeList.codeAlias}</option>
						</c:forEach>	
					</select>
				</td>
			</tr>
			<tr>				
				<th> 도엽번호</th>
				<td>
					<input type="text" id="shtNum" name="shtNum" />
				</td>
				<th >공사번호</th>
				<td>
					<input type="text" id="cntNum" name="cntNum" />
				</td>
				<th> 설치일자</th>
				<td>
					<input type="text" class="datepicker" id="istYmd" name="istYmd"/>
				</td>
				<th >-</th>
				<td><input type="text" id="" name="" readonly class="disable" /></td>
			</tr>
          </tbody>
        </table>
      </div>
    </div>
    <!-- //list --> 

    
    <!-- list -->
    
    <div class="view_box">
      <h2 class="m-b-5">시설현황 정보 </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
          <colgroup>
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          </colgroup>
          <tbody>
			<tr>				
				<th >형식</th>
				<td>									
					<select id="mofCde" name="mofCde" style="width:100%;">
						<option value="">선택안함</option>
						<c:forEach var="mofCdeList" items="${mofCdeList}" varStatus="status">
							<option value="${mofCdeList.codeCode }" >${mofCdeList.codeAlias}</option>
						</c:forEach>	
					</select>
				</td>
				<th >관재질</th>
				<td>
					<select id="mopCde" name="mopCde" style="width:100%;">
						<option value="">선택안함</option>
						<c:forEach var="mopCdeList" items="${mopCdeList}" varStatus="status">
							<option value="${mopCdeList.codeCode }">${mopCdeList.codeAlias}</option>
						</c:forEach>
					</select>
				</td>
				<th class="num2">구경</th>
				<td>	
					<input type="text" id="stdDip" name="stdDip" class="numVal2" />	
				</td>
				<th >제수변회전방향</th>
				<td>
					<select id="saeCde" name="saeCde" style="width:100%;">
						<option value="">선택안함</option>
						<c:forEach var="saeCdeList" items="${saeCdeList}" varStatus="status">
							<option value="${saeCdeList.codeCode }" >${saeCdeList.codeAlias}</option>
						</c:forEach>	
					</select>
				</td>
			</tr>
			<tr>				
				<th class="num2">제수변총회전수</th>
				<td>
					<input type="text" id="troCnt" name="troCnt" class="numVal2" />
				</td>
				<th class="num2">제수변회전수</th>
				<td>
					<input type="text" id="croCnt" name="croCnt" class="numVal2"/>
				</td>
				<th >제수변구동방법</th>
				<td>
					<select id="mthCde" name="mthCde" style="width:100%;">
						<option value="">선택안함</option>
						<c:forEach var="mthCdeList" items="${mthCdeList}" varStatus="status">
							<option value="${mthCdeList.codeCode }" >${mthCdeList.codeAlias}</option>
						</c:forEach>	
					</select>
				</td>
				<th >시설물형태</th>
				<td>
					<select id="forCde" name="forCde" style="width:100%;">
						<option value="">선택안함</option>
						<c:forEach var="forCdeList" items="${forCdeList}" varStatus="status">
							<option value="${forCdeList.codeCode }" >${forCdeList.codeAlias}</option>
						</c:forEach>	
					</select>
				</td>
			</tr>
			<tr>
				<th >변실규격</th>
				<td>
					<input type="text" id="valStd" name="valStd" />
				</td>
				<th class="num2">설정압력</th>
				<td>
					<input type="text" id="valSaf" name="valSaf" class="numVal2" />
				</td>
				<th >제작회사명</th>
				<td>
					<input type="text" id="prcNam" name="prcNam" />
				</td>
				<th >관로지형지물부호</th>
				<td>
					<input type="text" id="pipCde" name="pipCde" placeholder="수정안됨" />
				</td>
			</tr>
			<tr>				
				<th >관로관리번호</th>
				<td>
					<input type="text" id="pipIdn" name="pipIdn" placeholder="수정안됨" />
				</td>
				<th >이상상태</th>
				<td>
					<select id="cstCde" name="cstCde" style="width:100%;">
						<option value="">선택안함</option>
						<c:forEach var="cstCdeList" items="${cstCdeList}" varStatus="status">
							<option value="${cstCdeList.codeCode }" >${cstCdeList.codeAlias}</option>
						</c:forEach>	
					</select>
				</td>
				<th >개폐여부</th>
				<td>
					<select id="offCde" name="offCde" style="width:100%;">
						<option value="">선택안함</option>
						<c:forEach var="offCdeList" items="${offCdeList}" varStatus="status">
							<option value="${offCdeList.codeCode }" >${offCdeList.codeAlias}</option>
						</c:forEach>	
					</select>
				</td>
				<th class="num">방향각</th>
				<td>
					<input type="text" id="angDir" name="angDir" class="numVal"/>
				</td>
			</tr>
			<tr>				
				<th >대장초기화여부</th>
				<td>
					<select id="sysChk" name="sysChk" style="width:100%;">
						<option value="0">무</option>
						<option value="1">유</option>
					</select>
				</td>
				<th >-</th>
				<td><input type="text" id="" name="" readonly class="disable" /></td>
				<th >-</th>
				<td><input type="text" id="" name="" readonly class="disable" /></td>
				<th >-</th>
				<td><input type="text" id="" name="" readonly class="disable" /></td>
			</tr>			
		</tbody>
        </table>
      </div>
    </div>
    <!-- //list -->     
  </div>
  <!-- //container --> 
  
	</form>
</div>
<!-- //UI Object -->

</body>
</html>