<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>


<script type="text/javascript">
var saveLink = '${ctxRoot}/wutl/wutlHtSave.do';
var deleteLink = '${ctxRoot}/wutl/deleteWutlHt.do?ftrCde=${wttWutlHt.ftrCde}&ftrIdn=${wttWutlHt.ftrIdn}&repNum=${wttWutlHt.repNum}&g2Id=${wttWutlHt.g2Id}';

$(document).ready(function () {
		
    var now = new Date;
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
    if('${mode}'=='add') {
        $('#searchCondition2, #searchCondition8').val(today);
    }

	
    var msg = '${msg}';
	
	if(msg) {
		alert(msg);
		try { opener.selfRefresh(); }catch(e){}
		location.href="${ctxRoot}/wutl/view/popup.do?repNum=${param.reqNum}&g2Id=${param.g2Id}&mode=edit";
	}

	$("#saveBtn").click(function(e) {
		if ($('#searchCondition11').val() == '') {
			alert('관리번호를 입력하세요.');
			return false;
		}
		if ($('#searchCondition2').val() == '') {
			alert('유지보수일을 등록하세요.');
			return false;
		}
		if ($('#searchCondition3').val() == '') {
			alert('시공자를 입력하세요.');
			return false;
		}
		if ($('#searchCondition6').val() == '') {
			alert('유지보수 내용을 입력하세요.');
			return false;
		}
		
		e.preventDefault();
		
		var html = '';
		//html += '<form id="saveWutlHtFrm" method="post">';
		$('#sub_table .td_add_pdjt').each(function (i) {
			html += '<input type="hidden" name="wttPdjtHtList[' + i + '].pdtNum" value= "' + $('#pdtNum_'+i).val() + '" />';
			html += '<input type="hidden" name="wttPdjtHtList[' + i + '].iotYmd" value= "' + $('#iotYmd_'+i).val() + '" />';
			html += '<input type="hidden" name="wttPdjtHtList[' + i + '].pdhCnt" value= "' + $('#pdhCnt_'+i).val() + '" />';
			html += '<input type="hidden" name="wttPdjtHtList[' + i + '].pddNum" value= "' + $('#pddNum_'+i).val() + '" />';
		});
		//html += '</form>';
		$('#addPdjtDiv').append(html);
		
		$('#wutlHtSaveFrm').attr('action', saveLink);
		$('#wutlHtSaveFrm').submit();
	});
	
	$('#searchCondition7').change(function() {
		$('#searchCondition7 option:selected').each(function () {
			$.ajax({
				url : '${ctxRoot}/pdjt/pdjtCnt.do',
				type : 'get',
			 	contentType: "application/json",
				dataType : 'json',
				data : {pdtNum:$(this).val(), pddNum:$("#searchCondition7 option:selected").attr("pddNum")},
				success : function(res) {
					$('#searchCondition9').val(res.data.pddCnt);
				},
				error: function(request, status, error) {
					console.log("호출에 실패하였습니다.");
				}
			});
		});
	});
	
	/* 추가하기 */
	$('#addPdjt').click(function() {
		if($.trim( $("select[name=searchCondition7]").val() ) == "") {
			alert("소모품을 선택하세요.");
			$("select[name=searchCondition7]").focus();
			return;
		}
		if($.trim( $("input[name=searchCondition8]").val() ) == "") {
			alert("사용일자를 입력하세요.");
			$("input[name=searchCondition8]").focus();
			return;
		}
		if($.trim( $("input[name=searchCondition10]").val() ) == "") {
			alert("사용수량을 입력하세요.");
			$("input[name=searchCondition10]").focus();
			return;
		}
		var storeCnt = parseInt( $.trim( $("input[name=searchCondition9]").val()) );
		var useCnt = parseInt( $.trim( $("input[name=searchCondition10]").val()) );
		if(storeCnt < useCnt) {
			if(storeCnt == 0) {
				alert("현재 재고가 없습니다.");
			}else {
				alert("사용수량이 현재 재고를 초과합니다.\n\n" + storeCnt + " 이하로 입력하세요.");
			}
			$("input[name=searchCondition10]").val("");
			$("input[name=searchCondition10]").focus();
			return;
		}
		
		$('.no-rows').hide();
		var rowI = $('input[name=rowChk]').size();
		var html = '';
		html += '<tr class="td_add_pdjt">';
		html += '<td><input type="checkbox" name="rowChk" id="rowChk_'+rowI+'" /></td>';
		html += '<td class="td_left"><input type="hidden" name="pdtNum" id="pdtNum_'+rowI+'" value="'+$('#searchCondition7').val()+'" />' + $('#searchCondition7 option:selected').text() + '</td>';
		html += '<td><input type="hidden" name="iotYmd" id="iotYmd_'+rowI+'" value="'+$('#searchCondition8').val()+'" /><span class="iotYmd">' + $('#searchCondition8').val() + '</span></td>';
		html += '<td class="td_right"><input type="hidden" name="pdhCnt" id="pdhCnt_'+rowI+'" value="'+$('#searchCondition10').val()+'" /><span class="pdhCnt">' + $('#searchCondition10').val() + '</span></td></tr>';
		html += '<input type="hidden" name="pddNum" id="pddNum_'+rowI+'" value="'+$("#searchCondition7 option:selected").attr("pddNum")+'" />';
		
	    $('#sub_table > tbody:last').append(html);

	    /* 재고 차감 */
		var newCnt = storeCnt - useCnt;
		$("input[name=searchCondition9]").val(newCnt);
		/* 날짜 초기화 */
		$("input[name=searchCondition8]").val("");
		/* 수량 초기화 */
		$("input[name=searchCondition10]").val("");
	});
	
	$('#removePdjt').click(function() {
		rowCheDel();
	});
	
	function rowCheDel(){
		var $obj = $("input[name='pdjt_ht_check']");
 		var checkCount = $obj.size();
 		
 		if (checkCount == 0) { 			
			alert('삭제할 정보가 없습니다.');
 			return false;
 		}
 		
		var checkedCnt = 0;
		checkedCnt = checkedCnt + $('#sub_table .pdjt_ht_check:checked').length;
		
		if (checkedCnt>0) {
				if (confirm('선택된 항목들을 삭제하시겠습니까?')) {
					
					var html = '<form id="deletePdjtHtFrm" method="post"><input type="hidden" id="repNum" name="repNum" value="${param.repNum}"/><input type="hidden" id="g2Id" name="g2Id" value="${param.g2Id}"/>';
						$('#sub_table .pdjt_ht_check:checked').each(function (i) {
							html += '<input type="hidden" name="wttPdjtHtVOList[' + i + '].repNum" value="${param.reqNum}"/>';
							html += '<input type="hidden" name="wttPdjtHtVOList[' + i + '].g2Id" value="${param.g2Id}"/>';
							html += '<input type="hidden" name="wttPdjtHtVOList[' + i + '].pdhNum" value="' + $(this).attr("pdhNum") + '"/>';
						});
						html += '</form>';
						$('#popup_layer11').append(html);
					var pdjtHtdel = "${ctxRoot}/wutl/deletePdjtHt.do";
					/* 데이터 삭제시 */
					if( $('#sub_table .pdjt_ht_check:checked').length > 0) {
						$('#deletePdjtHtFrm').attr('action', pdjtHtdel);					
						$('#deletePdjtHtFrm').submit();
					}
					
				}
			}
		
 		for (var i=0; i<checkCount; i++){
	  		if($obj.eq(i).is(":checked")){
	  			$obj.eq(i).parent().parent().remove();
	  		}
 		} 		
	}
});

function removeComma() {
	var obj = $('.num_style');
	if (obj.length > 0) {
		for (var i in obj) {
			if (obj[i].value != undefined) {
				obj[i].value = obj[i].value.replace(/,/g,"");
			}
		}
	}
}
/* 금액입력시 숫자 외 문자 제거 */
function onlyNumber (obj) {
    
    var num01;
    var num02;
    num01 = obj.value;
    num02 = num01.replace(/\D/g,""); //숫자가 아닌것을 제거, 
                                     //즉 [0-9]를 제외한 문자 제거; /[^0-9]/g 와 같은 표현
    num01 = setComma(num02); //콤마 찍기
    obj.value =  num01;
}
function getNumber(obj){
    
    var num01;
    var num02;
    num01 = obj.value;
    num02 = num01.replace(/\D/g,""); //숫자가 아닌것을 제거, 
                                     //즉 [0-9]를 제외한 문자 제거; /[^0-9]/g 와 같은 표현
    // num01 = setComma(num02); //콤마 찍기
    // obj.value =  num01;
    obj.value =  num02;
}
function setComma(n) {
    var reg = /(^[+-]?\d+)(\d{3})/;   // 정규식
    n += '';                          // 숫자를 문자열로 변환         
    while (reg.test(n)) {
       n = n.replace(reg, '$1' + ',' + '$2');
    }         
    return n;
}
</script>




<body>

<div id="wrap" class="wrap" > 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1 >유지보수대장 상세정보</h1>
  </div>
  <!-- // header --> 

	<form name="wutlHtSaveFrm" id="wutlHtSaveFrm" method="post">
		<div class="addPdjtDiv" id="addPdjtDiv"></div>
		<input type="hidden" id="repNum" name="repNum" value="${wttWutlHt.repNum}" />
		<input type="hidden" id="g2Id" name="g2Id" value="${wttWutlHt.g2Id}" />
		<input type="hidden" id="searchCondition1" name="searchCondition1" value="${wttWutlHt.ftrCde}" />
	    <c:set var="idnchk" value="${ftrIdn}" />
		<c:choose>
		    <c:when test="${idnchk eq ''}">
		       <input type="hidden" id="ftrIdn" name="ftrIdn" onchange="getNumber(this);" onkeyup="getNumber(this);" maxlength="10" value="${wttWutlHt.ftrIdn }" />
		    </c:when>
		    <c:when test="${empty idnchk}">
		       <input type="hidden" id="ftrIdn" name="ftrIdn" onchange="getNumber(this);" onkeyup="getNumber(this);" maxlength="10" value="${wttWutlHt.ftrIdn  }" />
		    </c:when>
		    <c:otherwise>
		       <input type="hidden" id="ftrIdn" name="ftrIdn" onchange="getNumber(this);" onkeyup="getNumber(this);" maxlength="10" value="${ftrIdn }" />
		    </c:otherwise>
		</c:choose>




  <!-- container -->
  <div id="container"  class="content"  > 
    
    <!-- list -->
    <div class="regist-box ">
      <h2 ><span>일반정보</span>
        <div class="btn_group m-b-5 right "  >
         
<%-- 					<a href="${ctxRoot}/wutl/deleteWutlHt.do?ftrCde=${wttWutlHt.ftrCde}&ftrIdn=${wttWutlHt.ftrIdn}&repNum=${wttWutlHt.repNum}&g2Id=${wttWutlHt.g2Id}" id="removeBtn" name="removeBtn"><img src="${imgUrl }/popup/table_delete.png" onmouseover="this.src='${imgUrl}/popup/table_delete_over.png'" onmouseout="this.src='${imgUrl}/popup/table_delete.png'" alt="삭제" /></a> --%>
<%-- 					<a href="javascript:;" id="saveBtn" popup-frm="wutlHtSaveFrm"><img src="${imgUrl }/popup/table_save.png" onmouseover="this.src='${imgUrl}/popup/table_save_over.png'" onmouseout="this.src='${imgUrl}/popup/table_save.png'" alt="저장" /></a> --%>
          <button id="deleteBtn" type="button" class="btn btn-default " > <img src="/fms/images/icon_btn_delete.png" width="16" height="16" alt=""/> 삭제 </button>
          <button id="saveBtn" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_save.png" width="16" height="16" alt=""/> 저장 </button>
        </div>
      </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table table-view-regist " >
          <colgroup>
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
      
          </colgroup>
          <tbody>
						<tr>
							<th>지형지물</th>
							<td>
								<select id="selectFtrCde" name="selectFtrCde" readonly class="disable not-used" >
									<c:forEach var="cmtFtrcMaList" items="${cmtFtrcMaList}" varStatus="status">
										<option value="${cmtFtrcMaList.ftrCde }" ${wttWutlHt.ftrCde == cmtFtrcMaList.ftrCde ? "selected" : "" } >${cmtFtrcMaList.ftrNam}</option>
									</c:forEach>
								</select>
							</td>
							<th>관리번호</th>
							<td>
							    <c:set var="idnchk" value="${ftrIdn}" />
								<c:choose>
								    <c:when test="${idnchk eq ''}">
								       <input type="text" id="searchCondition11" name="searchCondition11" onchange="getNumber(this);" onkeyup="getNumber(this);" maxlength="10" value="${wttWutlHt.ftrIdn }" readonly class="disable"/>
								    </c:when>
								    <c:when test="${empty idnchk}">
								       <input type="text" id="searchCondition11" name="searchCondition11" onchange="getNumber(this);" onkeyup="getNumber(this);" maxlength="10" value="${wttWutlHt.ftrIdn  }" readonly class="disable"/>
								    </c:when>
								    <c:otherwise>
								       <input type="text" id="searchCondition11" name="searchCondition11" onchange="getNumber(this);" onkeyup="getNumber(this);" maxlength="10" value="${ftrIdn }" readonly class="disable"/>
								    </c:otherwise>
								</c:choose>
							</td>
						</tr>
						<tr>
							<th>시공자</th>
							<td>
								<input type="text" name="searchCondition3" id="searchCondition3" value="${wttWutlHt.oprNam }" />
							</td>
							<th>유지보수일</th>
							<td>
								<input type="text" class="datepicker" id="searchCondition2" name="searchCondition2" value="${wttWutlHt.repYmd }" />
							</td>
						</tr>
						<tr>
							<th>유지보수 사유</th>
							<td>
								<select name="searchCondition5" id="searchCondition5">
									<c:forEach var="sbjCdeList" items="${sbjCdeList}" varStatus="status">
										<option value="${sbjCdeList.codeCode }" ${wttWutlHt.sbjCde == sbjCdeList.codeCode ? "selected" : "" } >${sbjCdeList.codeAlias}</option>
									</c:forEach>
								</select>
							</td>
							<th>유지보수 구분</th>
							<td>
								<select name="searchCondition4" id="searchCondition4">
									<c:forEach var="repCdeList" items="${repCdeList}" varStatus="status">
										<option value="${repCdeList.codeCode }" ${wttWutlHt.repCde == repCdeList.codeCode ? "selected" : "" } >${repCdeList.codeAlias}</option>
									</c:forEach>
								</select>
							</td>
						</tr>
						<tr>
							<th>유지보수 내용</th>
							<td colspan="3">
								<input type="text" name="searchCondition6" id="searchCondition6" value="${wttWutlHt.repDes }" />
							</td>
						</tr>
          </tbody>
        </table>
      </div>
    </div>
    <!-- //list --> 

    
    <!-- list -->
    <div class="regist-box m-t-10">
      <h2 class="m-b-5">소모품/예비품 정보</h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table table-view-regist " >
          <colgroup>
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">

          </colgroup>
          <tbody>
						<tr>
							<th>소모품</th>
							<td>
								<select name="searchCondition7" id="searchCondition7">
									<option value="">선택안함</option>
									<c:forEach var="list" items="${pdjtMaList}" varStatus="status">
										<option value="${list.pdtNum }" pddNum="${list.pddNum }">${list.pdtNam}</option>
									</c:forEach>
								</select>
							</td>
							<th>사용일자</th>
							<td>
								<input type="text" class="datepicker" id="searchCondition8" name="searchCondition8" />
							</td>
						</tr>
						<tr>
							<th>현재 재고</th>
							<td>
								<input type="text" class="td_right" id="searchCondition9" name="searchCondition9" value="" readonly="readonly" />
							</td>
							<th>사용개수</th>
							<td>
								<input type="text" class="td_right" id="searchCondition10" name="searchCondition10" onkeyup="onlyNumber(this);" maxlength="6"/>
							</td>
						</tr>
          </tbody>
        </table>
      </div>
    </div>
    <!-- //list --> 
    
    <!-- tab -->
    <div class="regist-box ">
      <div class="btn_group m-b-5 m-r-5 m-t-5 right " >
          <button id="addPdjt" type="button" class="btn btn-default " > <img src="/fms/images/icon_btn_add.png" width="16" height="16" alt=""/> 추가</button>
          <button id="removePdjt" type="button" class="btn btn-default " > <img src="/fms/images/icon_btn_delete.png" width="16" height="16" alt=""/> 삭제 </button>
       </div>
      <!--tab list-->




    <div class="list-regist">
     
        <table border="0" cellpadding="0" cellspacing="0"  class="table table-list-regist  " style="white-space: nowrap; ">
          <colgroup>
          <col width="60px">
           <col width="*">
          <col width="120px">
           <col width="120px">
          
          </colgroup>
          <thead>
            <tr>
              <th>선택</th>
              <th>소모품</th>
              <th>사용일자</th>
              <th>사용개수(량)</th>
            </tr>
          </thead>
					<tbody>
						<c:if test="${fn:length(pdjtList) == 0}">
							<tr class="no-rows"><td colspan="4"> 조회내역이 없습니다. </td></tr>
						</c:if>
						<c:forEach var="list" items="${pdjtList}" varStatus="listIndex">
							<c:if test="${listIndex.index%2==0 }">
								<tr class="viewPopup">
							</c:if>
							<c:if test="${listIndex.index%2!=0 }">
								<tr class="viewPopup tr_back01">
							</c:if>
								<td>
									<input type="checkbox" class="pdjt_ht_check" name="pdjt_ht_check" pdhNum="${list.pdhNum }" value="${list.pdhNum }"/>
								</td>
								<td class="td_left">${list.pdtNam}</td>
								<td>${list.iotYmd}</td>
								<td class="td_right">${list.pdhCnt} </td>
							</tr>
						</c:forEach>
					</tbody>

        </table>
   
     </div>
    </div>
    
    <!-- //tab --> 
    
  </div>
  
  <!-- //container --> 
</form>
  
</div>
<!-- //UI Object -->

</body>
</html>

