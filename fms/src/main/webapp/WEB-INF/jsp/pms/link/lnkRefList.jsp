<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>


<script type="text/javascript">
var deleteLinkFlgp = '${ctxRoot}/cons/flgpMaDelete.do';


$(document).ready(function() {
	
	$("#referenceAddBtn").click(function(e) {
		e.preventDefault();
		var centerPos = parseInt( $(screen).get(0).width ) / 2;
		var windowCenterPos = 858 / 2;
		var leftPos = centerPos - windowCenterPos;
		var _win = "";
		if($(this).attr("tab-flag")=="tab") {
			var abbr="";
			if(this.abbr==""||this.abbr==null) {
				abbr=$(this).attr("abbr");
			} else {
				abbr=this.abbr;
			}
			_win = window.open(abbr, this.title, "left="+leftPos+",top=100,"+$(this).attr("popup-data") + ", menubar=no, location=no, resizable=no, toolbar=no, status=no");
			_win.focus();
		} else {
			try{ opener.closeChild(); }catch(e){}
			_win = window.open($(this).attr("abbr"), "child", "left="+leftPos+",top=100,"+$(this).attr("popup-data") + ", menubar=no, location=no, resizable=no, toolbar=no, status=no");
			_win.focus();
			try{ opener._winChild = _win; }catch(e){}
		}
	});

	
	// 전체 체크
	$('#referenceAllCheck').click(function () {
		if ($(this).is(':checked')) {
			$('#referenceTable tbody .reference_check').each(function () {
				$(this).attr('checked', 'checked').prop('checked', true);
			});
		} else {
			$('#referenceTable tbody .reference_check').each(function () {
				$(this).attr('checked', '').prop('checked', false);
			});
		}
	});
	
	// 참조자료 삭제
	$('#referenceDelBtn').click(function () {
		
		if ($('#referenceTable .reference_check:checked').length == 0 && $('#referenceTable .reference_check').length > 0) {
			
			alert('삭제할 항목이 없습니다.');
			
		} else if ($('#referenceTable .reference_check:checked').length > 0 && $('#referenceTable .reference_check').length > 0) {
		
			if (confirm('선택된 항목들을 삭제하시겠습니까?')) {
				
				///ajax 변경
				var lst = [];
				$('#referenceTable .reference_check:checked').each(function (i) {
					var file = {};
					file.figNum = $(this).val();
					
					lst.push(file);
				});
				gfn_saveCmmList(
						{lst: lst, sqlId: 'deleteWttFlgpMa', }
						, function(data){
							if (data.result) {
								alert("저장되었습니다.");
								fn_reload();
							}
							else{
								var err_msg = "저장에 실패하였습니다.";
								try{
									err_msg = data.error;
								}catch(e){}
								gfn_alert(err_msg,"E");
								return;
							}
					});
				
				
				
				
				// 				var html = '<form id="deleteFlgpMaFrm" method="post">';
				// 					html += '<input type="hidden" name="location" value="' + location.href + '"/>';
				// 					html += '<input type="hidden" name="cntNum" value="${resultVO.cntNum}"/>';
				// 					$('#referenceTable .reference_check:checked').each(function (i) {
				// 						html += '<input type="hidden" name="figNums" value="' + $(this).val() + '"/>';
				// 					});
				// 					html += '</form>';
									
				// 				$('#wrap').append(html);
								
				// 				$('#deleteFlgpMaFrm').attr('action', deleteLinkFlgp);
				// 				$('#deleteFlgpMaFrm').submit();
				
				
			}
		}
	});
	
	//참조자료 상세
	$('.referenceRow').click(function (e) {
		if (!$(e.target).is('.reference_check')) {
			window.open('${ctxRoot}/pms/link/lnkDwgPhotoDtl.do?figNum=' + $(this).attr('figNum') + '', '참조자료 확인', 'width=570, height=520, left='+ popupX + ', top='+ popupY + ', status=no, resizable=no');
		}
	});
	
});	
</script>



<div class="btn_group m-b-5 right "  >
  <button id="referenceAddBtn" type="button" class="btn btn-default " abbr="${ctxRoot}/pms/link/lnkDwgPhotoReg.do?type=reference&cntNum=${param.ftrCde}${param.ftrIdn}&ptype=ftr" tab-flag="tab" popup-data="width=570, height=440">
  	<img src="/fms/images/icon_btn_add.png" width="16" height="16" alt=""/> 추가</button>
  	
  <button id="referenceDelBtn" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_delete.png" width="16" height="16" alt=""/> 삭제</button>
</div>

<table border="0" cellpadding="0" cellspacing="0"  class="table table-tab  " style="white-space: nowrap;" id="referenceTable">
	<colgroup>
		<col width="22"></col>
		<col width="100"></col>
		<col width="100"></col>
		<col width="150"></col>
		<col width="*"></col>
	</colgroup>
	<thead>
		<tr>
			<th><input type="checkbox" id="referenceAllCheck" name="" /></th>
			<th>제목</th>
			<th>저장일자</th>
			<th>등록자</th>
			<th>설명</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="list" items="${referenceList }" varStatus="status">
			<tr class="referenceRow ${status.index%2 == 0 ? '' : 'tr_back01'}" figNum="${list.figNum }">
				<td class="line_left">
					<input type="checkbox" class="reference_check" value="${list.figNum }" />
				</td>
				<td>${list.titNam }</td>
				<td>${list.creYmd }</td>
				<td>${list.creUsr }</td>
				<td class="line_right">${list.grpExp }</td>
			</tr>
		</c:forEach>
		
		<c:if test="${empty referenceList }">
			<tr class="none_tr">
				<td colspan="5" class="line_center">검색 결과가 없습니다.</td>
			</tr>
		</c:if>
	</tbody>
</table>