<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>

<script type="text/javascript">
$(document).ready(function() {
	$('#btnAttFac').click(function () { 
		var addLink2 = '${pageContext.request.contextPath }/pms/popup/attFacCreatePopup.do?ftrCde=${resultVO.ftrCde}&ftrIdn=${resultVO.ftrIdn}&attSeq=${resultVO.attSeq}';
		window.open(addLink2, "_blank", 'status=no, width=552, height=240, left='+ popupX + ', top='+ popupY + ', resizable=no');
	});
});	

var popupX = (screen.availWidth-660)/2;
var popupY = (screen.availHeight-1130)/3;

function fn_DtlPopup(ftrCde,ftrIdn,attaSeq){	
	var addLink3 = '${pageContext.request.contextPath }/pms/popup/attFacViewPopup.do?ftrCde=' + ftrCde + '&ftrIdn=' + ftrIdn + '&attaSeq='+ attaSeq ;
	window.open(addLink3, "_blank", 'status=no, width=552, height=240, left='+ popupX + ', top='+ popupY + ', resizable=no');
} 

function fn_reload(){
	location.reload();
}
</script>

<div class="btn_group m-b-5 right "  > 
	<button id="btnAttFac"   type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_w_regist.png" width="16" height="16" alt=""/> 등록</button>		
</div>
<table border="0" cellpadding="0" cellspacing="0"  class="table table-tab  " style="white-space: nowrap;">
     	<colgroup>
		<col width="120"></col>
		<col width="80"></col>
		<col width=""></col>									
		<col width="80"></col>
		<col width="150"></col>
	</colgroup>
	<thead>
		<tr>
			<th>세부시설번호</th>
			<th>지형지물</th>
			<th>관리번호</th>
			<th>세부시설명</th>
			<th>시설개요</th>
		</tr>
	</thead>			
	<tbody>
		<c:forEach var="list" items="${dtResultList}" varStatus="listIndex"> 
			<tr onclick="javascript:fn_DtlPopup('${list.ftrCde}','${list.ftrIdn}','${list.attaSeq}');return false;" >
				<td><a href="javascript:;">${list.attIdn}</a></td>
				<td><a href="javascript:;">${mapFtrcMa[list.ftrCde]}</a></td>
				<td><a href="javascript:;">${list.ftrIdn}</a></td>
				<td><a href="javascript:;">${list.attNam}</a></td>
				<td><a href="javascript:;">${list.attDes}</a></td>
			</tr>
		</c:forEach>												
		<c:if test="${empty dtResultList }">
			<tr class="none_tr">
				<td colspan="5" class="line_center">검색 결과가 없습니다.</td>
			</tr>
		</c:if>			
	</tbody>
</table>


