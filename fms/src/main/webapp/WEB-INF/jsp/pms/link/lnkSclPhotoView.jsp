<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<script type="text/javascript">
var popupX = (screen.availWidth-660)/2;
var popupY = (screen.availHeight-1130)/3;

$(document).ready(function(){
	
		
	// 이미지 미리보기
	$('.fileHtImg').click(function () {
		window.open('${ctxRoot}/pms/file/downloadImg.do?seq=' + $(this).attr('_seq')+'&filSeq='+ $(this).attr('fil_seq'), '미리보기', 'width=775, height=508, left='+ popupX + ', top='+ popupY + ', status=no, resizable=no');
		
		return false;
	});
		
	
}); //ondocument ready

</script>


<body>

<div id="wrap" class="wrap" > 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1 >점검사진</h1>
  </div>
  <!-- // header --> 

		<form name="saveForm" id="saveForm" method="post" action="${ctxRoot}/pms/file/sclFileSave.do" enctype="multipart/form-data">
			<input type="hidden" name="filSeq" value="${filSeq }"/>
		
		
  <!-- container -->
  <div id="container"  class="content"  > 
    
    <!-- list -->
    <div class="regist-box2 ">
      <h2 ><span >파일정보</span>
        <div class="btn_group m-b-5 right "  >
          &nbsp;
        </div>
      </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table table-view-regist " >
          <colgroup>
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="114px">
      
          </colgroup>
          <tbody>		
				<tr>
					<th>점검사진제목<img src="/fms/images/icon_table_required_w.png" width="12" height="12" alt=""/></th>
					<td>${filNm}</td>
					<th>점검번호<img src="/fms/images/icon_table_required_w.png" width="12" height="12" alt=""/></th>
					<td>${sclNum}</td>
				</tr>							
          </tbody>
        </table>
      </div>
    </div>
    <!-- //list --> 


    <!-- list -->
    <div class="regist-box2 m-t-10">
      <h2 ><span >파일목록</span>
        <div class="btn_group m-b-5 right "  >
        &nbsp;
        </div>
      </h2>
    <div class="list-regist">
     
        <table border="0" cellpadding="0" cellspacing="0"  class="table table-list-regist  " style="white-space: nowrap; ">
          <colgroup>
			<col width="50px">
			<col width="*">
			<col width="120px">
			<col width="80px">
			<col width="120px">
			<col width="120px">
          </colgroup>
          <thead>
            <tr>
              <th>No.</th>
              <th>파일명</th>
              <th>미리보기</th>
              <th>다운로드</th>
              <th>사이즈</th>
              <th>용량</th>
            </tr>
          </thead>
				<tbody id="fileList">
				<c:forEach var="f" items="${fList}">
					<c:set var="fsiz" value="${f.filSiz/1000}"></c:set>
					<tr class="left"> 
						<td>
							${f.seq }
						</td>
						<td class="left">  ${f.dwnNam}  </td>
						<td>
							<a href="javascript:;" class="fileHtImg" fil_seq="${f.filSeq}" _seq="${f.seq }"><img src="${ctxRoot}/pms/file/downloadImg.do?seq=${f.seq}&filSeq=${f.filSeq}" alt="" width="20px" height="20px"/></a>
						</td>
						<td>
							<a href="${ctxRoot}/pms/file/downloadFile.do?seq=${f.seq}&filSeq=${f.filSeq}"><img src="${ctxRoot}/images/icon_file_down.png" /></a>
						</td>
						<td> ${f.filRst} </td>
						<td>  ${fsiz}  KB</td> 
					</tr>
				</c:forEach>
					
				</tbody>
        </table>
   
     </div>
    </div>
    <!-- //list --> 
    

    
  </div>
  
  <!-- //container --> 
  
</form>
  
</div>
<!-- //UI Object -->

</body>
</html>