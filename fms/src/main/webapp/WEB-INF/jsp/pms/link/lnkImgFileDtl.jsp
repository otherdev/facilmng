<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>



<script type="text/javascript">
var popupX = (screen.availWidth-660)/2;
var popupY = (screen.availHeight-1130)/3;


$(document).ready(function(){

	if ("${smode}" == "add") {
		gfn_alert('저장했습니다.',function(){
			opener.fn_reload();
			//opener.parent.fn_reload();
			window.close();
		});
	}
	else if ("${smode}" == "dtl") {
		gfn_alert('저장했습니다.',function(){
			opener.fn_reload();
		});
		
	}

	if (location.search.indexOf('error=1') > -1) {
		alert('저장을 실패했습니다. 다시 시도해주세요.')
		window.close();
	}
	

	//파일저장
	$('#btnSave').click(function () {
		
		if (!$('input[name=titNam]').val()) {
			gfn_alert('파일명을 입력해주세요.','W',function(){
				$('input[name=titNam]').focus();
			});
			return false;
		}
		
		if ($('input[name=files]')[0].files.length == 0) {
			gfn_alert('파일을 선택해주세요.','W');
			return false;
		}
		
		$('#saveForm').submit();
	});
	
	
	//파일선택리스트
	$('input[name=files]').change(function () {
		$('#fileList>tr.nf').empty();
		
		for (var i = 0; i < this.files.length; i++) {
			$('#fileList').append(
					'<tr class="nf left">' +
						'<td class="line_left">' +
							'<input type="checkbox" id="" name="" class="file_ht_check" _seq="-1" />' +
						'</td>' +
						'<td class="left">' + this.files[i].name + '</td>' +
						'<td></td>' +
						'<td></td>' +
						'<td></td>' +
						'<td>' + ((this.files[i].size || this.files[i].fileSize) / 1000) + 'KB</td>' +
					'</tr>'
			);
		}
	});
	
	
	
	// 이미지 미리보기
	$('.fileHtImg').click(function () {
		window.open('${ctxRoot}/pms/file/downloadImg.do?seq=' + $(this).attr('_seq')+'&filSeq='+ $(this).attr('fil_seq'), '미리보기', 'width=775, height=508, left='+ popupX + ', top='+ popupY + ', status=no, resizable=no');
		
		return false;
	});

	
	
	// 파일 삭제
	$('#btnDel').click(function () {
		
		if ($('.file_ht_check:checked').length < 1 && $('.file_ht_check').length > 0) {
			gfn_alert('삭제할 항목이 없습니다.','W');
			return;
		}
		else if($('.file_ht_check:checked').length > 1 ){
			gfn_alert('삭제할 항목을 하나만 선택하세요.','W');
			return;
		}
		
		var _seq = $('.file_ht_check:checked').first().attr('_seq');		
		// 파일삭제처리  - 신규파일은 모두클리어, 기존파일은 하나씩 삭제
		if(_seq < 0){
			$('#fileList>tr.nf').empty();
			$('input[name=files]').empty();
		}
		else{
			gfn_confirm("삭제하시겠습니까?", function(ret){
				if(!ret)	return;
				
				gfn_saveCmm({sqlId:"deleteFileDtl", data: {filSeq: '${filSeq}', seq: _seq}}, function(data){
					if(data.result){
						gfn_alert("삭제되었습니다.","",function(){
							var url = (location.href).replace(/smode=dtl/g,"");
							location.replace(url);
						});
					}else{
						gfn_alert("데이터 저장에 실패하였습니다. : " + data.error,"E");
					}
				});	
			});
		}
		
		
		
	});
	
	
}); //ondocument ready






</script>


<body>

<div id="wrap" class="wrap" > 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1 >첨부파일 등록</h1>
  </div>
  <!-- // header --> 

		<form name="saveForm" id="saveForm" method="post" action="${ctxRoot}/pms/file/fileSave.do" enctype="multipart/form-data">
			<input type="hidden" name="filSeq" value="${filSeq }"/>
			<input type="hidden" name="grpTyp" value="${grpTyp}"/>
		
		
  <!-- container -->
  <div id="container"  class="content"  > 
    
    <!-- list -->
    <div class="regist-box2 ">
      <h2 ><span >자료 정보</span>
        <div class="btn_group m-b-5 right "  >
         
        
          <button id="btnSave" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_save.png" width="16" height="16" alt=""/> 저장 </button>
        </div>
      </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table table-view-regist " >
          <colgroup>
          <col width="120px">
          <col width="*">
          </colgroup>
          <tbody>
		
				<tr>
					<th>파일제목<img src="/fms/images/icon_table_required_w.png" width="12" height="12" alt=""/></th>
					<td colspan="3">
						<input type="text" id="" name="titNam" value="${titNam}" style="width:100%"/>
					</td>
					<th>업무ID<img src="/fms/images/icon_table_required_w.png" width="12" height="12" alt=""/></th>
					<td colspan="3">
						<input type="text" id="" name="bizId" value="${bizId}" style="width:100%" class="disable" readonly />
					</td>
				</tr>
				<tr>
					<th>자료설명</th>
					<td colspan="7">
						<input type="text" id="" name="ctnt" value="${ctnt}" style="width:100%;" />
					</td>
				</tr>
				<tr>
					<th>파일업로드<img src="/fms/images/icon_table_required_w.png" width="12" height="12" alt=""/></th>
					<td colspan="7" class="td_file">
						<input type="file" id="" name="files" multiple="multiple" style="width:100%;" />
					</td>
				</tr>
							
          </tbody>
        </table>
      </div>
    </div>
    <!-- //list --> 


    <!-- list -->
    <div class="regist-box2 m-t-10">
      <h2 ><span >파일목록</span>
        <div class="btn_group m-b-5 right "  >
         
        <button id="btnDel" type="button" class="btn btn-default " > <img src="/fms/images/icon_btn_delete.png" width="16" height="16" alt=""/> 삭제 </button>
        </div>
      </h2>
           
    <div class="list-regist">
     
        <table border="0" cellpadding="0" cellspacing="0"  class="table table-list-regist  " style="white-space: nowrap; ">
          <colgroup>
			<col width="50px">
			<col width="*">
			<col width="120px">
			<col width="80px">
			<col width="120px">
			<col width="120px">

          </colgroup>
          <thead>
            <tr>
              <th>선택</th>
              <th>파일명</th>
              <th>미리보기</th>
              <th>다운로드</th>
              <th>사이즈</th>
              <th>용량</th>
            </tr>
          </thead>
				<tbody id="fileList">
				<c:forEach var="f" items="${fList}">
					<c:set var="fsiz" value="${f.filSiz/1000}"></c:set>
					<tr class="left"> 
						<td class="line_left">
							<input type="checkbox" id="" name="" class="file_ht_check" _seq="${f.seq }" />
						</td>
						<td class="left">  ${f.dwnNam}  </td>
						<td>
							<a href="javascript:;" class="fileHtImg" fil_seq="${f.filSeq}" _seq="${f.seq }"><img src="${ctxRoot}/pms/file/downloadImg.do?seq=${f.seq}&filSeq=${f.filSeq}" alt="" width="20px" height="20px"/></a>
						</td>
						<td>
							<a href="${ctxRoot}/pms/file/downloadFile.do?seq=${f.seq}&filSeq=${f.filSeq}"><img src="${ctxRoot}/images/icon_file_down.png" /></a>
						</td>
						<td> ${f.filRst} </td>
						<td>  ${fsiz}  KB</td> 
					</tr>
				</c:forEach>
					
				</tbody>
        </table>
   
     </div>
    </div>
    <!-- //list --> 
    

    
  </div>
  
  <!-- //container --> 
  
</form>
  
</div>
<!-- //UI Object -->

</body>
</html>