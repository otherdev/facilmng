<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<script type="text/javascript">
var popupX = (screen.availWidth-660)/2;
var popupY = (screen.availHeight-1130)/3;

$(document).ready(function() {
	// 상세화면 호출 함수
	$('.viewPopup').click(function (e) {
		window.open('${ctxRoot}/pms/file/lnkSclPhotoView.do?sclNum=' + $(this).find('.lnkFileSeqPop').attr('lnkData-sclNum')  
				+ '&filSeq='+$(this).find('.lnkFileSeqPop').attr('lnkData-fileSeq'), '점검사진', 'width=680, height=350, left='+ popupX + ', top='+ popupY + ', status=no, resizable=no');
	});
});	

</script>

<div class="btn_group m-b-5 right "  >
  &nbsp;
</div>

<table border="0" cellpadding="0" cellspacing="0"  class="table table-tab  " style="white-space: nowrap;">
<colgroup>
	<col width="50"></col>
	<col width="150"></col>
	<col width="150"></col>									
	<col width="150"></col>
	<col width="150"></col>
	<col width="150"></col>
	<col width="*"></col>
</colgroup>
<thead>
	<tr>
		<th >번호</th>
		<th >지형지물</th>
		<th >보수일자</th>
		<th >보수구분</th>
		<th >보수사유</th>
		<th >시공자</th>
		<th >보수내용</th>
	</tr>
</thead>
<tbody>
	<c:forEach var="list" items="${fmsChscFtrResList}" varStatus="listIndex">
		<c:if test="${empty list.fileSeq}">
		<tr>
			<td>${listIndex.index + 1}</td>
			<td>${mapFtrcMa[list.ftrCde]}</td>
			<td>${list.rprYmdFmt}</td>
			<td>${list.rprCatCdeNam}</td>
			<td>${list.rprCuzCdeNam}</td>
			<td>${list.rprUsrNm}</td>
			<td class="left"><a href="javascript:;">${list.rprCtnt}</a></td>
		</tr>
		</c:if>
		<c:if test="${!empty list.fileSeq}">
		<tr class="viewPopup">
			<td><a href="javascript:;" class="lnkFileSeqPop" lnkData-fileSeq="${list.fileSeq}" lnkData-sclNum="${list.sclNum}" >${listIndex.index + 1}</a></td>
			<td><a href="javascript:;">${mapFtrcMa[list.ftrCde]}</a></td>
			<td><a href="javascript:;">${list.rprYmdFmt}</a></td>
			<td><a href="javascript:;">${list.rprCatCdeNam}</a></td>
			<td><a href="javascript:;">${list.rprCuzCdeNam}</a></td>
			<td><a href="javascript:;">${list.rprUsrNm}</a></td>
			<td class="left"><a href="javascript:;">${list.rprCtnt}</a></td>
		</tr>
		</c:if>
	</c:forEach>
	<c:if test="${empty fmsChscFtrResList}">
		<tr class="none_tr">
			<td colspan="7" class="line_center">검색 결과가 없습니다.</td>
		</tr>
	</c:if>
	</tbody>
</table>


