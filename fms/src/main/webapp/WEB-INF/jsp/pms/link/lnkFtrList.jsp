<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>
    
<title>지형지물선택</title>
<script type="text/javascript">	
		 
$(document).ready(function() {
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();
	

	
	/*******************************************
	 * 이벤트 설정
	 *******************************************/
	
	
	$('#btnReset').click(function () {
        var frm = $('#ftrFrm');
        frm.find('input[type=text]').val('');
        frm.find('select').val('');
    });

	$('#btnSearch').click(function () {
		fn_search(1);
	});	

	//시설물선택
	$('#btnSel').click(function () {
		
		if ($('#drawingTable .drawing_check:checked').length == 0 && $('#drawingTable .drawing_check').length > 0) {
			alert('시설물 항목을 선택하세요.');
			return false;
			
		} else if ($('#drawingTable .drawing_check:checked').length > 1 ) {
			alert('시설물 항목을 하나만 선택하세요.');
			return false;
		}

		var _ftrCde = "";
		var _ftrIdn = "";
		var _ftrNam = "";
		var _hjdCde = "";
		var _hjdNam = "";
		
		
		$('#drawingTable .drawing_check:checked').each(function (i) {
			_ftrCde = $(this).attr('ftrCde');
			_ftrIdn = $(this).attr('ftrIdn');
			_ftrNam = $(this).attr('ftrNam');
			_hjdCde = $(this).attr('hjdCde');  
			_hjdNam = $(this).attr('hjdNam');  
		});
		
		opener.parent.callback_selFtr(_ftrCde, _ftrIdn, _ftrNam, _hjdCde, _hjdNam);
		window.close();
		
	});
	
	
	//시설물서택(클릭)
	$(".dtlPopup").click(function(){
		var _ftrCde = "";
		var _ftrIdn = "";
		var _ftrNam = "";
		var _hjdCde = "";
		var _hjdNam = "";
		
		_ftrCde = $(this).attr('ftrCde');
		_ftrIdn = $(this).attr('ftrIdn');
		_ftrNam = $(this).attr('ftrNam');
		_hjdCde = $(this).attr('hjdCde');  
		_hjdNam = $(this).attr('hjdNam');  
		
		opener.parent.callback_selFtr(_ftrCde, _ftrIdn, _ftrNam, _hjdCde, _hjdNam);
		window.close();
	});
	
	
});

/* pagination 페이지 링크 function */
function fn_search(pageNo){   		
	if(gfn_isNull(pageNo)){
		pageNo = 1;
	}
   	$('form[name=ftrFrm] input[name=pageIndex]').val(pageNo);
	$('form[name=ftrFrm]').submit();
}




</script>


<body>



<div id="wrap" class="wrap" > 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1 >시설물 검색목록</h1>
  </div>
  <!-- // header --> 
  
  
<form name="ftrFrm" id="ftrFrm" method="post" >
 	<input type="hidden" name="pageIndex" value="1"/>
	<input type="submit" style="display: none;" />

  <!-- container -->
  <div id="container"  class="content"  > 
    
    <!-- seach -->
    <div class="seach_box" >
      <h2 >검색항목 </h2>
      <div>
        <table border="0" cellpadding="0" cellspacing="0" class="table table-search" >
          <colgroup>
          <col width="114px">
          <col width="*">
          </colgroup>
          <tbody>
            <tr>
              <th>시설물구분</th>
              <td>
				<select name="searchCondition1" id="searchCondition1">
					<option value="">전체</option>
					<c:forEach var="cmtFtrcMaList" items="${cmtFtrcMaList}" varStatus="status">
						<option value="${cmtFtrcMaList.ftrCde }" ${param.searchCondition1 == cmtFtrcMaList.ftrCde ? "selected" : "" } >${cmtFtrcMaList.ftrNam}</option>
					</c:forEach>	
				</select>
             </td>
            </tr>
            <tr>
              <th>시설물명</th>
              <td>
				<input type="text" name="searchCondition2" id="searchCondition2" maxlength="50" value="${param.searchCondition2 }" />
             	</td>
            </tr>
            <tr>
              <th>관리번호</th>
              <td>
				<input type="text" name="searchCondition0" id="searchCondition0" maxlength="10" value="${param.searchCondition0 }" />
             	</td>
            </tr>
            <tr>
              <th>행정동</th>
              <td>
				<select name="searchCondition5" id="searchCondition5"    >
					<option value="">전체</option>
					<c:forEach var="cmtList" items="${cmtAdarMaList}" varStatus="status">
						<option value="${cmtList.hjdCde }" ${param.searchCondition5 == cmtList.hjdCde ? "selected" : "" } >${cmtList.hjdNam}</option>
					</c:forEach>
				</select>
               </td>
            </tr>
            <tr>
              <th>공사번호</th>
              <td>
				<input type="text" name="searchCondition3" id="searchCondition3" maxlength="50" value="${param.searchCondition3 }"  />
              </td>
            </tr>
            <tr>
              <th>공사명</th>
              <td>
				<input type="text" name="searchCondition4" id="searchCondition0" maxlength="50" value="${param.searchCondition4 }" />
             	</td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="btn_group m-t-10 "  >
        <button id="btnReset" class="btn btn-search " style="width:114px"><img src="/fms/images/icon_btn_reset.png" width="16" height="16" alt=""/> 초기화 </button>
        <button id="btnSearch" class="btn btn-search " style="width:132px"><img src="/fms/images/icon_btn_search.png" width="16" height="16" alt=""/> 검색 </button>
      </div>
    </div>
    <!-- //seach --> 
    
    
    
    
    <!-- list -->
    <div class="list_box">
      <h2 ><span style="float:left">목록 | 시설물(${paginationInfo.totalRecordCount}건)</span>
        <div class="btn_group m-b-5 right " >
          <button  id="btnSel" name="btnSel" type="button" class="btn btn-default " > <img src="/fms/images/icon_btn_save.png" width="16" height="16" alt=""/>선택</button>
        </div>
      </h2>
      <div class="list " >
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-striped table-list center " style="white-space: nowrap;" id="drawingTable">
          <colgroup>
          <col width="40px">
          <col width="150px">
          <col width="70px">
          <col width="150px">
          <col width="130px">
          <col width="*">
          </colgroup>
          <thead>
            <tr>
				<th>선택</th>
				<th>지형지물&nbsp; &nbsp;</th>
				<th>관리번호&nbsp; &nbsp;</th>
				<th>행정동&nbsp; &nbsp;</th>
				<th>공사번호&nbsp; &nbsp;</th>
				<th>공사명&nbsp; &nbsp;</th>
            </tr>
          </thead>
          <tbody>
          
			<c:forEach var="list" items="${resultList}" varStatus="listIndex">									
				<tr>
						<td class="line_left"><input type="checkbox" class="drawing_check" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' ftrNam='${list.ftrNam}' hjdCde='${list.hjdCde}' hjdNam='${list.hjdNam}' /></td>
						<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' ftrIdn='${list.ftrIdn}' ftrNam='${list.ftrNam}' hjdCde='${list.hjdCde}' hjdNam='${list.hjdNam}' >${list.ftrNam}</td>
						<td class="dtlPopup right" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' ftrIdn='${list.ftrIdn}' ftrNam='${list.ftrNam}' hjdCde='${list.hjdCde}' hjdNam='${list.hjdNam}' >${list.ftrIdn}</td>
						<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' ftrIdn='${list.ftrIdn}' ftrNam='${list.ftrNam}' hjdCde='${list.hjdCde}' hjdNam='${list.hjdNam}' >${list.hjdCde}</td>
						<td class="dtlPopup" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' ftrIdn='${list.ftrIdn}' ftrNam='${list.ftrNam}' hjdCde='${list.hjdCde}' hjdNam='${list.hjdNam}' >${list.cntNum}</td>
						<td class="dtlPopup left" style="cursor:pointer" ftrCde='${list.ftrCde}' ftrIdn='${list.ftrIdn}' ftrIdn='${list.ftrIdn}' ftrNam='${list.ftrNam}' hjdCde='${list.hjdCde}' hjdNam='${list.hjdNam}' >${list.cntNam}</td>
				</tr>
			</c:forEach>
			<c:if test="${empty resultList}">
				<tr>
					<td colspan="6" style="color:black;text-align:center;">검색 결과가 없습니다.</td>
				</tr>
			</c:if>			          


          
          </tbody>
        </table>
      </div>
		<!-- Paging -->
      <div style="width:100%; text-align:center">
        <ul class="pagination pagination-sm  " >
			<ui:pagination paginationInfo = "${paginationInfo}"  type="image" jsFunction="fn_search" />
        </ul>
    
      </div>      
    </div>
    <!-- //list --> 
    
  </div>
  
  <!-- //container --> 
  
</form>
  
</div>


</body>
</html>