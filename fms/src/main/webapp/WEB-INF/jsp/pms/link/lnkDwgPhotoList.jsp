<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>

<script type="text/javascript">
$(document).ready(function() {
	
	// 전체 체크
	$('#drawingAllCheck').click(function () {
		if ($(this).is(':checked')) {
			$('#drawingTable tbody .drawing_check').each(function () {
				$(this).attr('checked', 'checked').prop('checked', true);
			});
		} else {
			$('#drawingTable tbody .drawing_check').each(function () {
				$(this).attr('checked', '').prop('checked', false);
			});
		}
	});
	
	// 사진 추가
	$('#btnAddFile').click(function () {
		window.open('${ctxRoot}/pms/file/lnkImgFileDtl.do?bizId=${bizId}&grpTyp=${F_DWG}', '사진 추가', 'width=680, height=450, left='+ popupX + ', top='+ popupY + ', status=no, resizable=no');
		
		return false;
	});
		
	// 수정
	$(".drawingRow").click(function(e){
		var drawingFilSeq = $(this).attr('filSeq');
		  if (!$(e.target).is('.drawing_check')) {
			window.open('${ctxRoot}/pms/file/lnkImgFileDtl.do?filSeq=' + drawingFilSeq + '&bizId=${bizId}&grpTyp=${F_DWG}', '사진 확인', 'width=680, height=450, left='+ popupX + ', top='+ popupY + ', status=no, resizable=no');
		  }
	});
	
	// 사진 삭제
	$('#btnDelFile').click(function () {
		if ($('#drawingTable .drawing_check:checked').length == 0 && $('#drawingTable .drawing_check').length > 0) {
			
			gfn_alert('삭제할 항목이 없습니다.','W');
			
		} else if ($('#drawingTable .drawing_check:checked').length > 0 && $('#drawingTable .drawing_check').length > 0) {

			gfn_confirm('선택된 항목들을 삭제하시겠습니까?',function(ret){
				if(!ret)	return;
				
				///ajax 변경
				var lst = [];
				$('#drawingTable .drawing_check:checked').each(function (i) {
					var file = {};
					file.filSeq = $(this).val();
					file.bizId = '${bizId}';
					
					lst.push(file);
				});
				gfn_saveCmmList(
						{lst: lst, sqlId: 'deleteFileMap', }
						, function(data){
							if (data.result) {
								gfn_alert("삭제되었습니다.",function(){
									fn_reload();
								});
							}
							else{
								var err_msg = "저장에 실패하였습니다.";
								try{
									err_msg = data.error;
								}catch(e){}
								gfn_alert(err_msg,"E");
								return;
							}
				});
			});
			
		}
	});	
	
});

</script>


<div class="btn_group m-b-5 right "  >
  <button id="btnAddFile" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_add.png" width="16" height="16" alt=""/> 추가</button>
  <button id="btnDelFile" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_delete.png" width="16" height="16" alt=""/> 삭제</button>
</div>


<table border="0" cellpadding="0" cellspacing="0"  class="table table-tab  " style="white-space: nowrap;" id="drawingTable">
	<colgroup>
		<col width="25"></col>
		<col width="150"></col>
		<col width="150"></col>
		<col width="150"></col>
		<col width="*"></col>
	</colgroup>
	<thead>
		<tr>
			<th>
				<input type="checkbox" id="drawingAllCheck" name="" />
			</th>
			<th>제 목</th>
			<th>저장일자</th>
			<th>등록자</th>
			<th>설명</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="list" items="${drawingList }" varStatus="status">
			<tr class="drawingRow ${status.index%2 == 0 ? '' : 'tr_back01'}" filSeq="${list.filSeq }" style="cursor:pointer">
				<td class="line_left">
					<input type="checkbox" class="drawing_check" value="${list.filSeq }" />
				</td>
				<td class="tit_nam" style="text-align: left; padding-left: 5px;">${list.titNam }</td>
				<td class="cre_ymd">${list.creYmd }</td>
				<td class="cre_usr">${list.creUsr }</td>
				<td class="line_right grp_exp" style="text-align: left; padding-left: 5px;">${list.ctnt }</td>
			</tr>
		</c:forEach>
		
		<c:if test="${empty drawingList }">
			<tr class="none_tr">
				<td colspan="5" class="line_center">검색 결과가 없습니다.</td>
			</tr>
		</c:if>
	</tbody>
</table>