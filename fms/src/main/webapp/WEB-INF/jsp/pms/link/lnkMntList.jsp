<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<script type="text/javascript">
$(document).ready(function() {
	
	$('#addBtn').click(function () {
		window.open(addLink, "_blank", 'status=no, width=570, height=590, left='+ popupX + ', top='+ popupY + ', resizable=no');
	});
	
});	

var addLink = '${ctxRoot}/pms/link/lnkMntReg.do?ftrCde=${ftrCde}&ftrIdn=${ftrIdn}';
var popupX = (screen.availWidth-660)/2;
var popupY= (screen.availHeight-1130)/3;

</script>

<div class="btn_group m-b-5 right "  >
  <button id="addBtn" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_regist.png" width="16" height="16" alt=""/> 등록</button>
</div>

<table border="0" cellpadding="0" cellspacing="0"  class="table table-tab  " style="white-space: nowrap;">
<colgroup>
	<col width="50"></col>
	<col width="150"></col>
	<col width="150"></col>									
	<col width="150"></col>
	<col width="150"></col>
	<col width="150"></col>
	<col width="*"></col>
</colgroup>
<thead>
	<tr>
		<th >번호</th>
		<th >지형지물</th>
		<th >유지보수일자</th>
		<th >구분</th>
		<th >유지보수 사유</th>
		<th >시공자</th>
		<th >유지보수내용</th>
	</tr>
</thead>
<tbody>
	<c:forEach var="list" items="${wttWutlHtList}" varStatus="listIndex">
		<tr class="viewPopup">
			<td>${listIndex.index + 1}</td>
			<td>${mapFtrcMa[list.ftrCde]}</td>
			<td>${list.repYmd}</td>
			<td>${list.repCde}</td>
			<td>${list.sbjCde}</td>
			<td>${list.oprNam}</td>
			<td>${list.repDes}</td>
		</tr>
	</c:forEach>
	<c:if test="${empty wttWutlHtList}">
		<tr class="none_tr">
			<td colspan="7" class="line_center">검색 결과가 없습니다.</td>
		</tr>
	</c:if>
	</tbody>
</table>


