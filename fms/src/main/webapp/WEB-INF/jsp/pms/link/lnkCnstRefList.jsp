<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>

<script type="text/javascript">
$(document).ready(function() {
	
	// 전체 체크
	$('#refAllCheck').click(function () {
		if ($(this).is(':checked')) {
			$('#refTable tbody .ref_check').each(function () {
				$(this).attr('checked', 'checked').prop('checked', true);
			});
		} else {
			$('#refTable tbody .ref_check').each(function () {
				$(this).attr('checked', '').prop('checked', false);
			});
		}
	});
	
	// 참조자료 추가
	$('#btnAddFile2').click(function () {
		window.open('${ctxRoot}/pms/file/lnkFileDtl.do?bizId=${bizId}&grpTyp=${F_REF}', '참조자료 추가', 'width=580, height=450, left='+ popupX + ', top='+ popupY + ', status=no, resizable=no');
		
		return false;
	});
		


	// 수정
	$(".refRow").click(function(e){
		var refFilSeq = $(this).attr('filSeq');
		  if (!$(e.target).is('.ref_check')) {
			window.open('${ctxRoot}/pms/file/lnkFileDtl.do?filSeq=' + refFilSeq + '&bizId=${bizId}&grpTyp=${F_REF}', '참조자료 확인', 'width=570, height=450, left='+ popupX + ', top='+ popupY + ', status=no, resizable=no');
		  }
	});
	
	// 참조자료 삭제
	$('#btnDelFile2').click(function () {
		if ($('#refTable .ref_check:checked').length == 0 && $('#refTable .ref_check').length > 0) {
			
			gfn_alert('삭제할 항목이 없습니다.','W');
			
		} else if ($('#refTable .ref_check:checked').length > 0 && $('#refTable .ref_check').length > 0) {
		
			gfn_confirm('선택된 항목들을 삭제하시겠습니까?', function(ret){
				if(!ret)	reteurn;
				
				///ajax 변경
				var lst = [];
				$('#refTable .ref_check:checked').each(function (i) {
					var file = {};
					file.filSeq = $(this).val();
					file.bizId = '${bizId}';
					
					lst.push(file);
				});
				gfn_saveCmmList(
						{lst: lst, sqlId: 'deleteFileMap', }
						, function(data){
							if (data.result) {
								gfn_alert("삭제되었습니다.",function(){
									fn_reload();
								});
							}
							else{
								var err_msg = "저장에 실패하였습니다.";
								try{
									err_msg = data.error;
								}catch(e){}
								gfn_alert(err_msg,"E");
								return;
							}
				});
				
			});		
			
			
		}
	});	
	
});

</script>


<div class="btn_group m-b-5 right "  >
  <button id="btnAddFile2" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_add.png" width="16" height="16" alt=""/> 추가</button>
  <button id="btnDelFile2" type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_delete.png" width="16" height="16" alt=""/> 삭제</button>
</div>


<table border="0" cellpadding="0" cellspacing="0"  class="table table-tab  " style="white-space: nowrap;" id="refTable">
	<colgroup>
		<col width="25"></col>
		<col width="150"></col>
		<col width="150"></col>
		<col width="150"></col>
		<col width="*"></col>
	</colgroup>
	<thead>
		<tr>
			<th>
				<input type="checkbox" id="refAllCheck" name="" />
			</th>
			<th>제 목</th>
			<th>저장일자</th>
			<th>등록자</th>
			<th>설명</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="list" items="${refList }" varStatus="status">
			<tr class="refRow ${status.index%2 == 0 ? '' : 'tr_back01'}" filSeq="${list.filSeq }" style="cursor:pointer" >
				<td class="line_left">
					<input type="checkbox" class="ref_check" value="${list.filSeq }" />
				</td>
				<td class="tit_nam" style="text-align: left; padding-left: 5px;">${list.titNam }</td>
				<td class="cre_ymd">${list.creYmd }</td>
				<td class="cre_usr">${list.creUsr }</td>
				<td class="line_right grp_exp" style="text-align: left; padding-left: 5px;">${list.ctnt }</td>
			</tr>
		</c:forEach>
		
		<c:if test="${empty refList }">
			<tr class="none_tr">
				<td colspan="5" class="line_center">검색 결과가 없습니다.</td>
			</tr>
		</c:if>
	</tbody>
</table>