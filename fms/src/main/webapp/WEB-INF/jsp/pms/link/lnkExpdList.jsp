<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>


<table border="0" cellpadding="0" cellspacing="0"  class="table table-tab  " style="white-space: nowrap;">
	<colgroup>
		<col width="100"></col>
		<col width="*"></col>
		<col width="200"></col>
		<col width="200"></col>
	</colgroup>
	<thead>
		<tr>
			<th >순번</th>
			<th>소모품</th>
			<th >사용일자</th>
			<th >사용개수(량)</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="list" items="${wttPdjtHtList}" varStatus="listIndex">
			<tr class="viewPopup">
				<td>${listIndex.index + 1}</td>
				<td class="td_left">${list.pdtNam}</td>
				<td>${list.iotYmd }</td>
				<td class="td_right" style="padding-right:5px">${list.pdhCnt }</td>
			</tr>
		</c:forEach>
		
		<c:if test="${empty wttPdjtHtList}">
			<tr class="none_tr">
				<td colspan="4" class="line_center">검색 결과가 없습니다.</td>
			</tr>
		</c:if>
	</tbody>
</table>