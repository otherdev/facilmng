<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>


<style>
#qrImage { display:none; }
</style>
<div id="qrImage" style="text-align:center;"><img src="<c:url value="/common/genQr.do?data="/>" width="150"/> </div>
<tiles:insertAttribute name="content"/>
