<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<script type="text/javascript">

var popupX = (screen.availWidth-660)/2;
var popupY = (screen.availHeight-1130)/3;

$(document).ready(function() {
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();
	
	var msg = '${msg}';
	
	if(msg) {
		gfn_alert(msg,"",function(){
			window.close();	
		});		
	}	
	
	/*******************************************
	 * 이벤트 설정
	 *******************************************/	
	 $('#btnSave').click(function() {
				
		if(!gfn_formValid("wtrSupFrm")) return;

		gfn_confirm("저장하시겠습니까?",function(ret){
			if(!ret) return;
	    	
			gfn_saveFormCmm("wtrSupFrm", "insertWtlServPs", function(data){
				if(data.result){
					if('${type}' == 'edit'){
						gfn_alert("데이터가 저장되었습니다. 위치등록을 해주세요.","",function(){
							opener.document.getElementById('ftrIdn').value = $('#ftrIdn').val(); 
							window.close();
						});
					}
					else{
						gfn_alert("데이터가 저장되었습니다.","",function(){
							opener.fn_search();
							window.close();
						});
					}
				}else{
					gfn_alert("데이터 저장에 실패하였습니다. : " + data.error,"E");
				}
			});
		});
	});
	
	$('#cntNum').click(function () {
		if ($('#cntNum').val() != '') {			
			 gfn_confirm("상수공사대장을 변경하시겠습니까?", function(ret){	    	
				if(!ret) return;			
				
				window.open('${ctxRoot}/pms/popup/cnstMngPopup.do', "_blank"
						, 'status=no, width=1100, height=540, left='+ popupX + ', top='+ popupY +', resizable=no');
			});	
		}else{
			window.open('${ctxRoot}/pms/popup/cnstMngPopup.do', "_blank"
					, 'status=no, width=1100, height=540, left='+ popupX + ', top='+ popupY +', resizable=no');
		}	
	});
	
});	

function setChildValue(name) {
	document.getElementById("cntNum").value = name;
}
</script>

</head>


<body>


<div id="wrap" class="wrap" > 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1 >배수지 대장 등록정보</h1>
  </div>
  <!-- // header --> 
  
<form name="wtrSupFrm" id="wtrSupFrm" method="post">
  <input type="hidden" id="ftrCde" name="ftrCde"  value="SA114"/>
  
  <!-- container -->
  <div id="container"  class="content"  > 
    
    <!-- list -->
    <div class="view_box">
      <h2 ><span style="float:left">일반정보 </span>
	      <div class="btn_group m-b-5 right "  >
             <button id="btnSave" class="btn btn-default " type="button" > 
             	<img src="/fms/images/icon_btn_save.png" width="16" height="16" alt=""/>저장
             </button>
          </div>
      </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
          <colgroup>
          <col width="115px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          </colgroup>

          <tbody>
			<tr>
				<th class="req">관리번호</th>
				<td>
					<input type="text" id="ftrIdn" name="ftrIdn" value="${ftrIdnVal.ftrIdn }" onkeydown="noHanPress(this);" readonly class="disable reqVal" />
				</td>
				<th class="req">행정동</th>
				<td>
					<select name="hjdCde" id="hjdCde" class="reqVal" style="width:100%">
						<option value="">선택안함</option>
						<c:forEach var="cmtList" items="${cmtAdarMaList}" varStatus="status">
							<option value="${cmtList.hjdCde }" >${cmtList.hjdNam}</option>
						</c:forEach>
					</select>
				</td>
				<th >공사번호</th>
				<td>
					<input type="text" id="cntNum" name="cntNum" />
				</td>
				<th >관리기관</th>
				<td>
					<select id="mngCde" name="mngCde" style="width:100%">
						<option value="">선택안함</option>
						<c:forEach var="cdeList" items="${dataCodeList}" varStatus="status">
							<option value="${cdeList.codeCode }" >${cdeList.codeAlias}</option>
						</c:forEach>	
					</select>
				</td>
			</tr>
			<tr>				
				
				<th >도엽번호</th>
				<td>
					<input type="text" id="shtNum" name="shtNum" />
				</td>
				<th >준공일자</th>
				<td>
					<input type="text" class="datepicker" name="fnsYmd" id="fnsYmd">
				</td>
				<th >-</th>
				<td>
					<input type="text" id="" name="" readonly class="disable" />
				</td>
				<th >-</th>
				<td>
					<input type="text" id="" name="" readonly class="disable" />
				</td>
			</tr>
          </tbody>
        </table>
      </div>
    </div>
    <!-- //list --> 

    
    <!-- list -->
    
    <div class="view_box">
      <h2 class="m-b-5">시설현황 정보 </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
          <colgroup>
          <col width="115px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          </colgroup>
          <tbody>
			<tr>
				<th >배수지명</th>
				<td>
					<input type="text" id="srvNam" name="srvNam" />
				</td>
				<th >정수장명</th>
				<td>
					<input type="text" id="purNam" name="purNam" />
				</td>
				<th class="num2">부지면적(㎡)</th>
				<td>
					<input type="text" id="srvAra" name="srvAra" class="numVal2"/>
				</td>
				<th >관리방법</th>
				<td>
					<select id="sagCde" name="sagCde" style="width:100%">
						<option value="">선택안함</option>
						<c:forEach var="sagCdeList" items="${sagCdeList}" varStatus="status">
							<option value="${sagCdeList.codeCode }" >${sagCdeList.codeAlias}</option>
						</c:forEach>	
					</select>
				</td>
			</tr>
			<tr>
				<th class="num">시설용량(t)</th>
				<td>
					<input type="text" id="srvVol" name="srvVol" class="numVal"/>
				</td>
				<th class="num2">최고수위</th>
				<td>
					<input type="text" id="hghWal" name="hghWal" class="numVal2"/>
				</td>
				<th class="num2">최저수위</th>
				<td>
					<input type="text" id="lowWal" name="lowWal" class="numVal2"/>
				</td>
				<th class="num">배수지유입량(t)</th>
				<td>
					<input type="text" id="isrVol" name="isrVol" class="numVal"/>
				</td>
			</tr>
			<tr>				
				<th >급수지역</th>
				<td>
					<input type="text" id="supAre" name="supAre" />
				</td>
				<th class="num">급수인구</th>
				<td>
					<input type="text" id="supPop" name="supPop" class="numVal"/>
				</td>
				<th >배수지제어방법</th>
				<td>
					<select id="scwCde" name="scwCde" style="width:100%">
						<option value="">선택안함</option>
						<c:forEach var="scwCdeList" items="${scwCdeList}" varStatus="status">
							<option value="${scwCdeList.codeCode }" >${scwCdeList.codeAlias}</option>
						</c:forEach>	
					</select>
				</td>
				<th >대장초기화여부</th>
				<td>
					<select id="sysChk" name="sysChk" style="width:100%">
						<option value="0">무</option>
						<option value="1">유</option>
					</select>
				</td>
			</tr>	
		</tbody>
        </table>
      </div>
    </div>
    <!-- //list -->     
  </div>
  <!-- //container --> 
  
	</form>
</div>
<!-- //UI Object -->

</body>
</html>