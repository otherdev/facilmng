<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<script type="text/javascript">
var popupX = (screen.availWidth-660)/2;
var popupY = (screen.availHeight-1130)/3;

$(document).ready(function() {
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();
		
	var msg = '${msg}';

	if(msg) {
		gfn_alert(msg,"",function(){
			window.close();	
		});	
	}
	 /*******************************************
	 * 이벤트 설정
	 *******************************************/
	 $('#btnSave').click(function() {
		
		if(!gfn_formValid("wtrTrkFrm")) return;
		
		gfn_confirm("저장하시겠습니까?",function(ret){
			if(!ret) return;
	    	
			gfn_saveFormCmm("wtrTrkFrm", "insertWtlRsrvPs", function(data){
				if(data.result){
					if('${type}' == 'edit'){
						gfn_alert("데이터가 저장되었습니다. 위치등록을 해주세요.","",function(){
							opener.document.getElementById('ftrIdn').value = $("#ftrIdn").val(); 
							window.close();
						});
					}
					else{
						gfn_alert("데이터가 저장되었습니다.","",function(){
							opener.fn_search();
							window.close();
						});
					}
				}else{
					gfn_alert("데이터 저장에 실패하였습니다. : " + data.error,"E");
				}
			});
		});
	});
	 
	//공사번호 변경
	$('#cntNum').click(function () {
		if ($('#cntNum').val() != '') {
			gfn_confirm("급수공사대장을 변경하시겠습니까?", function(ret){
				if(!ret) return false;
				
				window.open('${ctxRoot}/pms/popup/facetMngPopup.do', "_blank"
						, 'status=no, width=1100, height=618, left='+ popupX + ', top='+ popupY +', resizable=no');				 
			});
		}else{
			window.open('${ctxRoot}/pms/popup/facetMngPopup.do', "_blank"
					, 'status=no, width=1100, height=618, left='+ popupX + ', top='+ popupY +', resizable=no');	
		}				
	});
});	

function setChildValue(name) {
	document.getElementById("cntNum").value = name;
}
</script>

</head>
<body>
<div id="wrap" class="wrap" > 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1 >저수조 대장 등록정보</h1>
  </div>
  <!-- // header --> 
  
<form name="wtrTrkFrm" id="wtrTrkFrm" method="post">
  
  <!-- container -->
  <div id="container"  class="content"  > 
    
    <!-- list -->
    <div class="view_box">
      <h2 ><span style="float:left">일반정보 </span>
	      <div class="btn_group m-b-5 right "  >
             <button id="btnSave" class="btn btn-default " type="button" > 
             	<img src="/fms/images/icon_btn_save.png" width="16" height="16" alt=""/>저장
             </button>
          </div>
      </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
          <colgroup>
          <col width="115px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          </colgroup>

          <tbody>
			<tr>
				<th class="req">지형지물</th>
				<td>
					<select id="ftrCde" name="ftrCde" readonly class="disable reqVal">
						<c:forEach var="cmtFtrcMaList" items="${cmtFtrcMaList}" varStatus="status">
							<option value="${cmtFtrcMaList.ftrCde }" ${"SA120" == cmtFtrcMaList.ftrCde ? "selected" : "" } >${cmtFtrcMaList.ftrNam}</option>
						</c:forEach>
					</select>
				</td>
				<th class="req">관리번호</th>
				<td>
					<input type="text" id="ftrIdn" name="ftrIdn" class="reqVal disable" value="${ftrIdnVal}"  readonly />
				</td>
				<th class="req">행정동</th>
				<td>
					<select name="hjdCde" id="hjdCde" class="reqVal">
						<option value="">선택안함</option>
						<c:forEach var="cmtList" items="${cmtAdarMaList}" varStatus="status">
							<option value="${cmtList.hjdCde }" >${cmtList.hjdNam}</option>
						</c:forEach>
					</select>
				</td>
				<th >준공일자</th>
				<td>
					<input type="text" class="datepicker" name="fnsYmd" id="fnsYmd">
				</td>
			</tr>
			<tr>				
				<th >도엽번호</th>
				<td>
					<input type="text" name="shtNum" id="shtNum" />
				</td>
				<th >관리기관</th>
				<td>
					<select id="mngCde" name="mngCde">
						<option value="">선택안함</option>
						<c:forEach var="cdeList" items="${dataCodeList}" varStatus="status">
							<option value="${cdeList.codeCode }" >${cdeList.codeAlias}</option>
						</c:forEach>	
					</select>
				</td>
				<th >허가일자</th>
				<td>
					<input type="text" class="datepicker" name="pmsYmd" id="pmsYmd"/>
				</td>
			</tr>
          </tbody>
        </table>
      </div>
    </div>
    <!-- //list --> 

    
    <!-- list -->
    
    <div class="view_box">
      <h2 class="m-b-5">시설현황 정보 </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
          <colgroup>
          <col width="115px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          </colgroup>
          <tbody>
			<tr>
				<th >저수조명</th>
				<td>
					<input type="text" name="rsrNam" id="rsrNam" />
				</td>
				<th >건물유형</th>
				<td>									
					<select id="blsCde" name="blsCde">
						<option value="">선택안함</option>
						<c:forEach var="blsCdeList" items="${blsCdeList}" varStatus="status">
							<option value="${blsCdeList.codeCode }" >${blsCdeList.codeAlias}</option>
						</c:forEach>	
					</select>
				</td>
				<th >소유자성명</th>
				<td>
					<input type="text" name="ownNam" id="ownNam" />
				</td>
				<th >소유자주소</th>
				<td>
					<input type="text" name="ownAdr" id="ownAdr" />
				</td>
			</tr>
			<tr>
				<th >소유자전화번호</th>
				<td>
					<input type="text" name="ownTel" id="ownTel" />
				</td>
				<th >관리자</th>
				<td>
					<input type="text" name="mngNam" id="mngNam" />
				</td>
				<th >관리자주소</th>
				<td>
					<input type="text" name="mngAdr" id="mngAdr" />
				</td>
				<th >관리자전화번호</th>
				<td>
					<input type="text" name="mngTel" id="mngTel" />
				</td>
			</tr>
			<tr>
				<th class="num" >건축면적</th>
				<td>
					<input type="text" name="bldAra" id="bldAra" class="numVal" />
				</td>
				<th class="num">건축연면적</th>
				<td>
					<input type="text" name="tblAra" id="tblAra" class="numVal"/>
				</td>
				<th class="num">세대수</th>
				<td>
					<input type="text" name="famCnt" id="famCnt" class="numVal"/>
				</td>
				<th >건물주소</th>
				<td>
					<input type="text" name="bldAdr" id="bldAdr" />
				</td>
			</tr>
			<tr>
				<th >대장초기화여부</th>
				<td>
					<select name="sysChk" id="sysChk">
						<option value="0">무</option>
						<option value="1">유</option>
					</select>
				</td>
			</tr>
		</tbody>
        </table>
      </div>
    </div>
    <!-- //list -->     
  </div>
  <!-- //container --> 
  
	</form>
</div>
<!-- //UI Object -->

</body>
</html>