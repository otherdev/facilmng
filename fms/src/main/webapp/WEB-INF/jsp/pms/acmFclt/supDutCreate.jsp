<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<script type="text/javascript">
var popupX = (screen.availWidth-660)/2;
var popupY = (screen.availHeight-1130)/3;

$(document).ready(function() {
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();
	
	var msg = '${msg}';
	
	if(msg) {
		gfn_alert(msg,"",function(){
			window.close();	
		});	
	}
	
	 /*******************************************
	 * 이벤트 설정
	 *******************************************/
	$('#btnSave').click(function() {
		
		if(!gfn_formValid("supDutFrm")) return;
		
		gfn_confirm("저장하시겠습니까?",function(ret){
			if(!ret) return;
	    	
			gfn_saveFormCmm("supDutFrm", "insertWtlSplyLs", function(data){
				if(data.result){
					if('${type}' == 'edit'){
						gfn_alert("데이터가 저장되었습니다. 위치등록을 해주세요.","",function(){
							opener.document.getElementById('ftrIdn').value = $("#ftrIdn").val(); 
							window.close();
						});
					}
					else{
						gfn_alert("데이터가 저장되었습니다.","",function(){
							opener.fn_search();
							window.close();
						});
					}
				}else{
					gfn_alert("데이터 저장에 실패하였습니다. : " + data.error,"E");
				}
			});
		});
	});
	
	//공사번호 변경
	$('#cntNum').click(function () {
		if ($('#cntNum').val() != '') {
			gfn_confirm("급수공사대장을 변경하시겠습니까?", function(ret){
				if(!ret) return false;
				
				window.open('${ctxRoot}/pms/popup/facetMngPopup.do', "_blank"
						, 'status=no, width=1100, height=618, left='+ popupX + ', top='+ popupY +', resizable=no');				 
			});
		}else{
			window.open('${ctxRoot}/pms/popup/facetMngPopup.do', "_blank"
					, 'status=no, width=1100, height=618, left='+ popupX + ', top='+ popupY +', resizable=no');	
		}				
	});
});	

//공사번호 선택
function setChildValue(name) {
	document.getElementById("cntNum").value = name;
}

</script>

</head>

<body>
<div id="wrap" class="wrap" > 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1 >급수관로 대장 등록정보</h1>
  </div>
  <!-- // header --> 
  
<form name="supDutFrm" id="supDutFrm" method="post">
  <!-- container -->
  <div id="container"  class="content"  > 
    
    <!-- list -->
    <div class="view_box">
      <h2 ><span style="float:left">일반정보 </span>
	      <div class="btn_group m-b-5 right "  >
             <button id="btnSave" class="btn btn-default " type="button" > 
             	<img src="/fms/images/icon_btn_save.png" width="16" height="16" alt=""/>저장
             </button>
          </div>
      </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
          <colgroup>
          <col width="115px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          </colgroup>

          <tbody>
			<tr>
				<th class="req">지형지물</th>
				<td>
					<select id="ftrCde" name="ftrCde" readonly class="disable reqVal" >
						<option value="SA001">급수관로</option>					
					</select>
				</td>
				<th class="req">관리번호</th>
				<td>
					<input type="text" id="ftrIdn" name="ftrIdn" value="${ftrIdnVal}" readonly class="disable reqVal" style="width:130px;"  />
				</td>
				<th class="req">행정동</th>
				<td>
					<select name="hjdCde" id="hjdCde" class="reqVal">
						<option value="">선택안함</option>
						<c:forEach var="cmtList" items="${cmtAdarMaList}" varStatus="status">
							<option value="${cmtList.hjdCde }" >${cmtList.hjdNam}</option>
						</c:forEach>
					</select>
				</td>
				<th >공사번호</th>
				<td>
					<input type="text" id="cntNum" name="cntNum" />
				</td>
			</tr>
			<tr>				
				<th >설치일자</th>
				<td>
					<input type="text" class="datepicker" id="istYmd" name="istYmd" style="width:130px;"/>
				</td>
				<th >관리기관</th>
				<td>
					<select id="mngCde" name="mngCde">
						<option value="">선택안함</option>
						<c:forEach var="cdeList" items="${dataCodeList}" varStatus="status">
							<option value="${cdeList.codeCode }" >${cdeList.codeAlias}</option>
						</c:forEach>	
					</select>
				</td>
				<th >도엽번호</th>
				<td>
					<input type="text" id="shtNum" name="shtNum" />
				</td>
				<th></th>
				<td></td>
			</tr>
          </tbody>
        </table>
      </div>
    </div>
    <!-- //list --> 

    
    <!-- list -->
    
    <div class="view_box">
      <h2 class="m-b-5">시설현황</h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
          <colgroup>
          <col width="115px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          </colgroup>
          <tbody>
			<tr>
				<th >관용도</th>
				<td>
					<select id="saaCde" name="saaCde">
						<option value="">선택안함</option>
						<c:forEach var="saaCdeList" items="${saaCdeList}" varStatus="status">
							<option value="${saaCdeList.codeCode }">${saaCdeList.codeAlias}</option>
						</c:forEach>	
					</select>
				</td>
				<th >접합종류</th>
				<td>
					<select id="jhtCde" name="jhtCde">
						<option value="">선택안함</option>
						<c:forEach var="jhtCdeList" items="${jhtCdeList}" varStatus="status">
							<option value="${jhtCdeList.codeCode }" >${jhtCdeList.codeAlias}</option>
						</c:forEach>
					</select>
				</td>
				<th >관라벨</th>
				<td>
					<input type="text" id="pipLbl" name="pipLbl" />
				</td>
				<th >구경</th>
				<td>
					<input type="text" id="pipDip" name="pipDip"  onkeydown="onlyNumber(this);"/>
				</td>
			</tr>
		</tbody>
        </table>
      </div>
    </div>
    <!-- //list -->
  </div>
  <!-- //container --> 
  
	</form>
</div>
<!-- //UI Object -->

</body>
</html>