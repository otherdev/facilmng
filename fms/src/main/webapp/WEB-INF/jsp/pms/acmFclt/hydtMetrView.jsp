<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<script type="text/javascript">
var popupX = (screen.availWidth-660)/2;
var popupY = (screen.availHeight-1130)/3;

var addLink2 = '${ctxRoot}/pms/acmFclt/hydtMetr/hydtMetrChgCreatePopup.do?ftrCde=${resultVO.ftrCde}&ftrIdn=${resultVO.ftrIdn}';


$(document).ready(function() { 
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();
		
	var msg = '${msg}';
		
	if(msg) {
		gfn_alert(msg,"",function(){
			window.close();	
		});	
	}
	
	
	 /*******************************************
	 * 이벤트 설정
	 *******************************************/
	
	//교체이력 신규등록
	$('#btnHtAdd').click(function () { 
		window.open(addLink2, "_blank", 'status=no, width=560, height=300, left='+ popupX + ', top='+ popupY + ', resizable=no');
	});
	
	/// 탭선택이벤트
	$(".tab-menu>li>a").click(function(){
		//탭타이틀 처리
		$(".tab-menu>li>a").not(this).removeClass("active");
		if($(this).not(":visible")){
			$(this).addClass("active");	
		}
		
		//탭본문처리
		$(".tab-list").removeClass("active");
		$("#"+$(this).attr("abbr")).addClass("active");
	});

	
	
	$('#btnSave').click(function() {
		
		if(!gfn_formValid("wydtMetrFrm")) return;
		
		gfn_confirm('저장하시겠습니까?',function(ret){			
			if(!ret) return;
			
			gfn_saveFormCmm("wydtMetrFrm", "updateWtlMetaPs", function(data){
				if(data.result){
					gfn_alert("데이터가 저장되었습니다.","",function(){
						opener.fn_search();
						fn_reload();
					});
				}else{
					gfn_alert("데이터 저장에 실패하였습니다. : " + data.error,"E");
				}
			});
			
		});
	});
	
	$('#btnDel').click(function() {
		
		// 자식데이터 체크
		gfn_selectList({sqlId: 'selectCmmChscResSubList',
						sqlId2: 'selectFileMapList',
						sqlId3: 'selectWttMetaHtList',
						ftrCde:'${ftrCde}', ftrIdn:'${ftrIdn}', bizId:'${ftrCde}${ftrIdn}'} , function(ret){
							
			if(ret.data != null && ret.data.length > 0){
				gfn_alert("유지보수 내역이 있습니다.","W");
				return false;
			}
			if(ret.data2 != null && ret.data2.length > 0){
				gfn_alert("첨부내용이 있습니다.","W");
				return false;
			}
			if(ret.data3 != null && ret.data3.length > 0){
				gfn_alert("계량기 교체이력이 있습니다.","W");
				return false;
			}
			
			//삭제처리			
			gfn_confirm("삭제하시겠습니까?",function(ret){
				if(!ret) return;
				gfn_saveFormCmm("wydtMetrFrm", "deleteWtlMetaPs", function(data){
					if(data.result){
						gfn_alert("데이터가 삭제되었습니다.","",function(){						
							opener.fn_search();
							window.close();
						});
					}else{
						gfn_alert("데이터 삭제가 실패하였습니다. : " + data.error,"E");
					}
				});
			});	
		});
		
	});

	$('#cntNum').click(function () {
		if ($('#cntNum').val() != '') {
			gfn_confirm("급수공사대장을 변경하시겠습니까?", function(ret){
				if(!ret) return false;
				
				window.open('${ctxRoot}/pms/popup/facetMngPopup.do', "_blank"
						, 'status=no, width=1100, height=618, left='+ popupX + ', top='+ popupY +', resizable=no');				 
			});
		}else{
			window.open('${ctxRoot}/pms/popup/facetMngPopup.do', "_blank"
					, 'status=no, width=1100, height=618, left='+ popupX + ', top='+ popupY +', resizable=no');	
		}				
	});
	
	
	$("#btnPrint").click(function(){		
		
        $('<form>', {
            "id": "hydtMetrView",
            "html": '<input type="text" id="ftrCde" name="ftrCde" value="${ftrCde}" />'
            		+'<input type="text" id="ftrIdn" name="ftrIdn" value="${ftrIdn}" />'
            		+'<input type="text" id="barCde" name="barCde" value="${ftrCde}${ftrIdn}" />',
            "action": '/fms/pms/pdf/hydtMetrView.do'
        }).appendTo(document.body).submit();		
	});

});	

var fn_reload = function(){
	location.reload();
}
	
function setChildValue(name) {
	document.getElementById("cntNum").value = name;
}

</script>

</head>


<body>


<div id="wrap" class="wrap" > 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1 >급수전계량기 대장 상세정보</h1>
  </div>
  <!-- // header --> 
  
<form name="wydtMetrFrm" id="wydtMetrFrm" method="post">  
  <input type="hidden" id="ogrFid" name="ogrFid" value="${resultVO.ogrFid}"/>
  <input type="hidden" id="ftrCde" name="ftrCde" value="${ftrCde}"/>
   
  <!-- container -->
  <div id="container"  class="content"  > 
    
    <!-- list -->
    <div class="view_box">
      <h2 ><span style="float:left">일반정보</span>
        <div class="btn_group m-b-5 right "  >
          <button id="btnPrint" class="btn btn-default " type="button" > <img src="/fms/images/icon_btn_print.png" width="16" height="16" alt=""/>인쇄 </button>
          <button id="btnDel" class="btn btn-default " type="button" > <img src="/fms/images/icon_btn_delete.png" width="16" height="16" alt=""/>삭제</button>
          <button id="btnSave" class="btn btn-default " type="button" > <img src="/fms/images/icon_btn_save.png" width="16" height="16" alt=""/>저장</button>
        </div>
      </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
          <colgroup>
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          </colgroup>

          <tbody>
			<tr>
				<th class="req">지형지물</th>
				<td>
					<select id="ftrCde" name="ftrCde" disabled class="disable reqVal">
						<c:forEach var="cmtFtrcMaList" items="${cmtFtrcMaList}" varStatus="status">
							<option value="${cmtFtrcMaList.ftrCde }" ${resultVO.ftrCde == cmtFtrcMaList.ftrCde ? "selected" : "" } >${cmtFtrcMaList.ftrNam}</option>
						</c:forEach>
					</select>
				</td>
				<th class="req">관리번호</th>
				<td>
					<input type="text" id="ftrIdn" name="ftrIdn" value="${resultVO.ftrIdn }" readonly class="disable reqVal" />
				</td>
				<th class="req">행정동</th>
				<td>
					<select id="hjdCde" name="hjdCde" class="reqVal">
						<option value="">선택안함</option>
						<c:forEach var="cmtList" items="${cmtAdarMaList}" varStatus="status">
							<option value="${cmtList.hjdCde }" ${resultVO.hjdCde == cmtList.hjdCde ? "selected" : "" } >${cmtList.hjdNam}</option>
						</c:forEach>
					</select>
				</td>
				<th >공사번호</th>
				<td>
					<input type="text" id="cntNum" name="cntNum" value="${resultVO.cntNum }" />
				</td>
			</tr>
			<tr>				
				<th >설치일자</th>
				<td>
					<input type="text" class="datepicker" name="istYmd" id="istYmd" value="${resultVO.istYmd }" style="width:130px;" />
				</td>
				<th >도엽번호</th>
				<td>
					<input type="text" id="shtNum" name="shtNum" value="${resultVO.shtNum }"/>
				</td>
				<th ></th>
				<td></td>
				<th ></th>
				<td></td>
			</tr>
          </tbody>
        </table>
      </div>
    </div>
    <!-- //list --> 
    <!-- list -->
    <div class="view_box">
      <h2 class="m-b-5">시설현황 정보 </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
          <colgroup>
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          </colgroup>
          <tbody>
			<tr>
				<th >수용가번호</th>
				<td>
					<input type="text" id="homNum" name="homNum" value="${resultVO.homNum }"/>
				</td>
				<th >수용가성명</th>
				<td>
					<input type="text" id="homNam" name="homNam" value="${resultVO.homNam }" />
				</td>
				<th >수용가행정동</th>
				<td>
					<select id="homCde" name="homCde" >
						<option value="">선택안함</option>
						<c:forEach var="cmtList" items="${cmtAdarMaList}" varStatus="status">
							<option value="${cmtList.hjdCde }" ${resultVO.homCde == cmtList.hjdCde ? "selected" : "" } >${cmtList.hjdNam}</option>
						</c:forEach>
					</select>
					
				
				</td>
				<th >수용가주소</th>
				<td>
					<input type="text" id="homAdr" name="homAdr" value="${resultVO.homAdr }"  />
				</td>
			</tr>
			<tr>
				<th class="num">가구수</th>
				<td>
					<input type="text" id="homCnt" name="homCnt" value="${resultVO.homCnt }" class="numVal" />
				</td>
				<th >업종</th>
				<td>
					<select name="sbiCde" id="sbiCde">
						<option value="">선택안함</option>
						<c:forEach var="sbiCdeList" items="${sbiCdeList}" varStatus="status">
							<option value="${sbiCdeList.codeCode }" ${resultVO.sbiCde == sbiCdeList.codeCode ? "selected" : "" }>${sbiCdeList.codeAlias}</option>
						</c:forEach>	
					</select>
				</td>
				<th >계량기물번호</th>
				<td>
					<input type="text" id="metNum" name="metNum" value="${resultVO.metNum }" />
				</td>
				<th class="num">계량기구경</th>
				<td>
					<input type="text" id="metDip" name="metDip" value="${resultVO.metDip }" class="numVal" />
				</td>
			</tr>
			<tr>
				<th >형식</th>
				<td>
					<select name="mofCde" id="mofCde">
						<option value="">선택안함</option>
						<c:forEach var="mofCdeList" items="${mofCdeList}" varStatus="status">
							<option value="${mofCdeList.codeCode }" ${resultVO.mofCde == mofCdeList.codeCode ? "selected" : "" }>${mofCdeList.codeAlias}</option>
						</c:forEach>	
					</select>
				</td>
				<th >계량기제작회사명</th>
				<td>
					<input type="text" id="prdNum" name="prdNum" value="${resultVO.prdNum }" />
				</td>
			</tr>
		</tbody>
        </table>
      </div>
    </div>
     
    <!-- //list --> 
    
    <!-- tab -->
    <div class=" tab-box">
	
      <div class="tab-menu">
      
        <ul class="tab-menu">
			<li><a href="#" abbr="tab01" class="active">유지보수</a></li>
			<li><a href="#" abbr="tab02">사진첨부</a></li>
			<li><a href="#" abbr="tab03">참조자료</a></li>
			<li><a href="#" abbr="tab05">계량기교체이력</a></li>
		</ul>
							
	  </div>  

	<!--tab list-->
		<div  id="tab01" class="tab-list active" >
			<c:import url="/pms/link/lnkChscList.do"/>
		</div>
		
		<div  id="tab02" class="tab-list" >
			<c:import url="/pms/link/lnkDwgPhotoList.do?bizId=${ftrCde}${ftrIdn}"/>
		</div>
		<div  id="tab03" class="tab-list" >
			<c:import url="/pms/link/lnkCnstRefList.do?bizId=${ftrCde}${ftrIdn}"/>
		</div>
		
		<div  id="tab05" class="tab-list" >			
			<div class="btn_group m-b-5 right "  >
				<button id="btnHtAdd"   type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_w_regist.png" width="16" height="16" alt=""/> 등록</button>		
			</div>
	
			<table border="0" cellpadding="0" cellspacing="0"  class="table table-tab  " style="white-space: nowrap;">
				<colgroup>
					<col width="25"></col>
					<col width="80"></col>
					<col width="70"></col>
					<col width="95"></col>
					<col width="115"></col>
					<col width="115"></col>	
					<col width="115"></col>
				</colgroup>
				<thead>
					<tr>
						<th>순번</th>
						<th>변경일련번호</th>
						<th>교체일자</th>
						<th>교체구분</th>
						<th>철거계량기기물번호</th>
						<th>철거계량기구경</th>
						<th>철거계량기형식</th>
					</tr>
				</thead>
				<tbody>					
					<c:forEach var="list" items="${htResultList}" varStatus="listIndex">
						<tr class="popup-link" href="${ctxRoot}/pms/acmFclt/hydtMetr/hydtMetrChgViewPopup.do?metaSeq=${list.metaSeq}&ftrCde=${resultVO.ftrCde}&ftrIdn=${resultVO.ftrIdn}" title="세부시설현황등록" tab-flag="tab" popup-data="width=560, height=300" >
							<td><a href="javascript:;">${listIndex.index + 1}</a></td> 
							<td><a href="javascript:;">${list.metaSeq}</a></td>
							<td><a href="javascript:;">${list.chgYmd}</a> </td>
							<td><a href="javascript:;">${mapGcwCde[list.gcwCde]}</a> </td>
							<td><a href="javascript:;">${list.omeNum}</a></td>
							<td><a href="javascript:;">${list.omeDip}</a></td>
							<td><a href="javascript:;">${mapMofCde[list.omeMof]} </a></td>
						</tr>
					</c:forEach>													
					<c:if test="${empty htResultList }">
						<tr class="none_tr">
							<td colspan="8" style="color:black; text-align:center;">검색 결과가 없습니다.</td>
						</tr>
					</c:if>		
				</tbody>
			</table>
		</div>

      <!--/tab list-->

    </div>
    <!-- //tab-box --> 
    
  </div>
  <!-- //container --> 
  
	</form>
</div>
<!-- //UI Object -->

</body>
</html>