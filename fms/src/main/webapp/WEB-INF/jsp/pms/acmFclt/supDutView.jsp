<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<style>
	td>input{
		width:140px;
	}
</style>

<script type="text/javascript">
var popupX = (screen.availWidth-660)/2;
var popupY = (screen.availHeight-1130)/3;

$(document).ready(function() {
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();
		
	
	 /*******************************************
	 * 이벤트 설정
	 *******************************************/
	var msg = '${msg}';
		
	if(msg) {
		gfn_alert(msg,"",function(){
			window.close();	
		});		
	}
	

	
	
	/// 탭선택이벤트
	$(".tab-menu>li>a").click(function(){
		//탭타이틀 처리
		$(".tab-menu>li>a").not(this).removeClass("active");
		if($(this).not(":visible")){
			$(this).addClass("active");	
		}
		
		//탭본문처리
		$(".tab-list").removeClass("active");
		$("#"+$(this).attr("abbr")).addClass("active");
	});

	
	$('#btnSave').click(function() {
		
		if(!gfn_formValid("supDutFrm")) return;
		
		gfn_confirm('저장하시겠습니까?',function(ret){			
			if(!ret) return;
			
			gfn_saveFormCmm("supDutFrm", "updateWtlSplyLs", function(data){
				if(data.result){
					gfn_alert("데이터가 저장되었습니다.","",function(){
						opener.fn_search();
						fn_reload();
					});
				}else{
					gfn_alert("데이터 저장에 실패하였습니다. : " + data.error,"E");
				}
			});
			
		});
	});

	
	// 삭제처리
	$('#btnDel').click(function() {
		
		// 자식데이터 체크
		gfn_selectList({sqlId: 'selectCmmChscResSubList',
						sqlId2: 'selectFileMapList',
						sqlId3: 'selectSubList',
						ftrCde:'${ftrCde}', ftrIdn:'${ftrIdn}', bizId:'${ftrCde}${ftrIdn}'} , function(ret){
							
			if(ret.data != null && ret.data.length > 0){
				gfn_alert("유지보수 내역이 있습니다.","W");
				return false;
			}
			if(ret.data2 != null && ret.data2.length > 0){
				gfn_alert("첨부내용이 있습니다.","W");
				return false;
			}
			if(ret.data3 != null && ret.data3.length > 0){
				gfn_alert("급수전계량기 정보가 있습니다.","W");
				return false;
			}

			
			// 삭제처리
			gfn_confirm("삭제하시겠습니까?",function(ret){
				if(!ret) return;
				gfn_saveFormCmm("supDutFrm", "deleteWtlSplyLs", function(data){
					if(data.result){
						gfn_alert("데이터가 삭제되었습니다.","",function(){						
							opener.fn_search();
							window.close();
						});
					}else{
						gfn_alert("데이터 삭제가 실패하였습니다. : " + data.error,"E");
					}
				});
			});	
	    });
	
	});
	
	// 공사번호 변경
	$('#cntNum').click(function () {
		if ($('#cntNum').val() != '') {
			gfn_confirm("급수공사대장을 변경하시겠습니까?", function(ret){
				if(!ret) return false;
				
				window.open('${ctxRoot}/pms/popup/facetMngPopup.do', "_blank"
						, 'status=no, width=1100, height=618, left='+ popupX + ', top='+ popupY +', resizable=no');				 
			});
		}else{
			window.open('${ctxRoot}/pms/popup/facetMngPopup.do', "_blank"
					, 'status=no, width=1100, height=618, left='+ popupX + ', top='+ popupY +', resizable=no');	
		}		
	});
	
	$("#btnPrint").click(function(){		
		
        $('<form>', {
            "id": "supDutView",
            "html": '<input type="text" id="ftrCde" name="ftrCde" value="${ftrCde}" />'
            		+'<input type="text" id="ftrIdn" name="ftrIdn" value="${ftrIdn}" />'
            		+'<input type="text" id="barCde" name="barCde" value="${ftrCde}${ftrIdn}" />',
            "action": '/fms/pms/pdf/supDutView.do'
        }).appendTo(document.body).submit();		
	});
}); //document ready	



var fn_reload = function(){
	location.reload();
}

function setChildValue(name) {
	document.getElementById("cntNum").value = name;
}

</script>

</head>


<body>


<div id="wrap" class="wrap" > 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1 >급수관로 대장 상세정보</h1>
  </div>
  <!-- // header --> 
  
<form name="supDutFrm" id="supDutFrm" method="post">
  
  <!-- container -->
  <div id="container"  class="content"  > 
    
    <!-- list -->
    <div class="view_box">
      <h2 ><span style="float:left">일반정보</span>
        <div class="btn_group m-b-5 right "  >
          <button id="btnPrint" class="btn btn-default " type="button" > <img src="/fms/images/icon_btn_print.png" width="16" height="16" alt=""/>인쇄 </button>
          <button id="btnDel" class="btn btn-default " type="button" > <img src="/fms/images/icon_btn_delete.png" width="16" height="16" alt=""/>삭제</button>
          <button id="btnSave" class="btn btn-default " type="button" > <img src="/fms/images/icon_btn_save.png" width="16" height="16" alt=""/>저장</button>
        </div>
      </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
          <colgroup>
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          </colgroup>

          <tbody>
			<tr>
				<th class="req">지형지물</th>
				<td>
					<select id="ftrCde" name="ftrCde" readonly class="disable reqVal" >
						<option value="SA001">급수관로</option>						
					</select>
				</td>
				<th class="req">관리번호</th>
				<td>
					<input type="text" id="ftrIdn" name="ftrIdn" value="${resultVO.ftrIdn }" readonly class="disable reqVal" style="width:130px;"/>
				</td>
				<th class="req">행정동</th>
				<td>
					<select id="hjdCde" name="hjdCde" class="reqVal">
						<option value="">선택안함</option>
						<c:forEach var="cmtList" items="${cmtAdarMaList}" varStatus="status">
							<option value="${cmtList.hjdCde }" ${resultVO.hjdCde == cmtList.hjdCde ? "selected" : "" } >${cmtList.hjdNam}</option>
						</c:forEach>
					</select>
				</td>
				<th >공사번호</th>
				<td>
					<input type="text" id="cntNum" name="cntNum" value="${resultVO.cntNum}" style="width:130px;"/>
				</td>
			</tr>
			<tr>				
				<th >설치일자</th>
				<td>
					<input type="text" class="datepicker" id="istYmd" name="istYmd" value="${resultVO.istYmd}" style="width:130px;"/>
				</td>
				<th >관리기관</th>
				<td>
					<select id="mngCde" name="mngCde">
						<option value="">선택안함</option>
						<c:forEach var="cdeList" items="${dataCodeList}" varStatus="status">
							<option value="${cdeList.codeCode }" ${resultVO.mngCde == cdeList.codeCode ? "selected" : "" } >${cdeList.codeAlias}</option>
						</c:forEach>	
					</select>
				</td>
				<th >도엽번호</th>
				<td>
					<input type="text" id="shtNum" name="shtNum" value="${resultVO.shtNum }"/>
				</td>
				<th></th>
				<td></td>
			</tr>
          </tbody>
        </table>
      </div>
    </div>
    <!-- //list --> 
    <!-- list -->
    <div class="view_box">
      <h2 class="m-b-5">시설현황 </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
          <colgroup>
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          </colgroup>
          <tbody>
			<tr>
				<th >관용도</th>
				<td>
					<select id="saaCde" name="saaCde">
						<option value="">선택안함</option>
						<c:forEach var="saaCdeList" items="${saaCdeList}" varStatus="status">
							<option value="${saaCdeList.codeCode }" ${resultVO.saaCde == saaCdeList.codeCode ? "selected" : "" } >${saaCdeList.codeAlias}</option>
						</c:forEach>	
					</select>
				</td>
				<th >접합종류</th>
				<td>
					<select id="jhtCde" name="jhtCde">
						<option value="">선택안함</option>
						<c:forEach var="jhtCdeList" items="${jhtCdeList}" varStatus="status">
							<option value="${jhtCdeList.codeCode }" ${resultVO.jhtCde == jhtCdeList.codeCode ? "selected" : "" } >${jhtCdeList.codeAlias}</option>
						</c:forEach>
					</select>
				</td>
				<th >관라벨</th>
				<td>
					<input type="text" id="pipLbl" name="pipLbl" style="width:130px;" value="${resultVO.pipLbl}"/>
				</td>
				<th class="num">구경</th>
				<td>
					<input type="text" id="pipDip" name="pipDip"  class="numVal" value="${resultVO.pipDip }"/>
				</td>
			</tr>
		</tbody>
        </table>
      </div>
    </div>
     
    <!-- //list --> 
    
    <br>
    <!-- tab -->
    <div class=" tab-box">
	
      <div class="tab-menu">
      
        <ul class="tab-menu">
			<li><a href="#" abbr="tab01" class="active">유지보수</a></li>
			<li><a href="#" abbr="tab02">사진첨부</a></li>
			<li><a href="#" abbr="tab03">참조자료</a></li>
			<li><a href="#" abbr="tab05">급수전계량기</a></li>
		</ul>
							
	  </div>  
	<!--tab list-->
		<div  id="tab01" class="tab-list active" >
			<c:import url="/pms/link/lnkChscList.do"/>
		</div>
		
		<div  id="tab02" class="tab-list" >
			<c:import url="/pms/link/lnkDwgPhotoList.do?bizId=${ftrCde}${ftrIdn}"/>
		</div>
		<div  id="tab03" class="tab-list" >
			<c:import url="/pms/link/lnkCnstRefList.do?bizId=${ftrCde}${ftrIdn}"/>
		</div>
		
		<div  id="tab05" class="tab-list" >
			<table border="0" cellpadding="0" cellspacing="0"  class="table table-tab  " style="white-space: nowrap;">
				<colgroup>
					<col width="80"></col>
					<col width="120"></col>
					<col width="120"></col>									
					<col width="100"></col>
					<col width="130"></col>
					<col width="120"></col>
					<col width="160"></col>
					<col width="160"></col>
				</colgroup>
				<thead>
					<tr>
						<th>순번</th>
						<th>관리번호</th>
						<th>행정동</th>
						<th>설치일자</th>
						<th>수용가번호</th>
						<th>수용가성명</th>
						<th>수용가행정동</th>
					</tr>
				</thead>
				<tbody>					
					<c:forEach var="list" items="${wtlMetaList}" varStatus="listIndex">
						<c:if test="${listIndex.index%2==0 }">
							<tr class="viewPopup">
						</c:if>
						<c:if test="${listIndex.index%2!=0 }">
							<tr class="viewPopup tr_back01">
						</c:if>
						<td>${listIndex.index + 1}</td>
						<td>${list.ftrIdn }</td>
						<td>${mapAdarMa[list.hjdCde]}</td>
						<td>
							<fmt:parseDate value="${list.istYmd}" var="dateFmt" pattern="yyyyMMdd"/>
												 <fmt:formatDate value="${dateFmt}"  pattern="yyyy-MM-dd"/>													
						</td>
						<td>${list.homNum }</td>
						<td>${list.homNam }</td>
						<td>${mapAdarMa[list.homCde]}</td>
					</tr>
					</c:forEach>												
					<c:if test="${empty wtlMetaList }">
						<tr class="none_tr">
							<td colspan="7" style="color:black; text-align:center;">검색 결과가 없습니다.</td>
						</tr>
					</c:if>
				</tbody>
			</table>
		</div>

      <!--/tab list-->

    </div>
    <!-- //tab-box --> 
    
  </div>
  <!-- //container --> 
  
	</form>
</div>
<!-- //UI Object -->

</body>
</html>