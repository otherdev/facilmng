<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<script type="text/javascript">
var popupX = (screen.availWidth-660)/2;
var popupY = (screen.availHeight-1130)/3;

var addLink3 = '${ctxRoot}/pms/acmFclt/wtrTrk/cleanHisCreatePopup.do?ftrCde=${resultVO.ftrCde}&ftrIdn=${resultVO.ftrIdn}';


$(document).ready(function() {
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();
		
	var msg = '${msg}';

	if(msg) {
		gfn_alert(msg,"",function(){
			window.close();	
		});	
	}

	
	 /*******************************************
	 * 이벤트 설정
	 *******************************************/

	/// 탭선택이벤트
	$(".tab-menu>li>a").click(function(){
		//탭타이틀 처리
		$(".tab-menu>li>a").not(this).removeClass("active");
		if($(this).not(":visible")){
			$(this).addClass("active");	
		}
		
		//탭본문처리
		$(".tab-list").removeClass("active");
		$("#"+$(this).attr("abbr")).addClass("active");
	});

	 
	// 청소이력 신규등록
	$('#btnList6Create').click(function () { 
		window.open(addLink3, "_blank", 'status=no, width=560, height=300, left='+ popupX + ', top='+ popupY + ', resizable=no');
	});
	
	
	// 저장
	$('#btnSave').click(function() {
		
		if(!gfn_formValid("wtrTrkFrm")) return;
		
		gfn_confirm('저장하시겠습니까?',function(ret){			
			if(!ret) return;
			
			gfn_saveFormCmm("wtrTrkFrm", "updateWtlRsrvPs", function(data){
				if(data.result){
					gfn_alert("데이터가 저장되었습니다.","",function(){
						opener.fn_search();
						fn_reload();
					});
				}else{
					gfn_alert("데이터 저장에 실패하였습니다. : " + data.error,"E");
				}
			});
			
		});
	});
	
	// 삭제
	$('#btnDel').click(function() {
		
		// 자식데이터 체크
		gfn_selectList({sqlId: 'selectCmmChscResSubList',
						sqlId2: 'selectFileMapList',
						sqlId3: 'selectWttRsrvHtList',
						ftrCde:'${resultVO.ftrCde}', ftrIdn:'${resultVO.ftrIdn}', bizId:'${resultVO.ftrCde}${resultVO.ftrIdn}'} , function(ret){
							
			if(ret.data != null && ret.data.length > 0){
				gfn_alert("유지보수 내역이 있습니다.","W");
				return false;
			}
			if(ret.data2 != null && ret.data2.length > 0){
				gfn_alert("첨부내용이 있습니다.","W");
				return false;
			}
			if(ret.data3 != null && ret.data3.length > 0){
				gfn_alert("청소이력이 있습니다.","W");
				return false;
			}
			
			//삭제처리			
			gfn_confirm("삭제하시겠습니까?",function(ret){
				if(!ret) return;
				gfn_saveFormCmm("wtrTrkFrm", "deleteWtlRsrvPs", function(data){
					if(data.result){
						gfn_alert("데이터가 삭제되었습니다.","",function(){						
							opener.fn_search();
							window.close();
						});
					}else{
						gfn_alert("데이터 삭제가 실패하였습니다. : " + data.error,"E");
					}
				});
			});	
		});		
		
	});

	
	// 공사번호변경
	$('#cntNum').click(function () {
		if ($('#cntNum').val() != '') {
			gfn_confirm("급수공사대장을 변경하시겠습니까?", function(ret){
				if(!ret) return false;
				
				window.open('${ctxRoot}/pms/popup/facetMngPopup.do', "_blank"
						, 'status=no, width=1100, height=618, left='+ popupX + ', top='+ popupY +', resizable=no');				 
			});
		}else{
			window.open('${ctxRoot}/pms/popup/facetMngPopup.do', "_blank"
					, 'status=no, width=1100, height=618, left='+ popupX + ', top='+ popupY +', resizable=no');	
		}	
	});
	
	$("#btnPrint").click(function(){		
		
        $('<form>', {
            "id": "wtrTrView",
            "html": '<input type="text" id="ftrCde" name="ftrCde" value="${ftrCde}" />'
            		+'<input type="text" id="ftrIdn" name="ftrIdn" value="${ftrIdn}" />'
            		+'<input type="text" id="barCde" name="barCde" value="${ftrCde}${ftrIdn}" />',
            "action": '/fms/pms/pdf/wtrTrView.do'
        }).appendTo(document.body).submit();		
	});

});	

var fn_reload = function(){
	location.reload();
}

function setChildValue(name) {
	document.getElementById("cntNum").value = name;
}

</script>

</head>


<body>


<div id="wrap" class="wrap" > 
  <!-- header -->
  <div id="header" class="header"  >
    <h1 >저수조 대장 상세정보</h1>
  </div>
  <!-- // header --> 
  
<form name="wtrTrkFrm" id="wtrTrkFrm" method="post">
  <input type="hidden" name="ftrCde" value="${resultVO.ftrCde}"/>
  
  <!-- container -->
  <div id="container"  class="content"  > 
    
    <!-- list -->
    <div class="view_box">
      <h2 ><span style="float:left">일반정보</span>
        <div class="btn_group m-b-5 right "  >
          <button id="btnPrint" class="btn btn-default " type="button"> <img src="/fms/images/icon_btn_print.png" width="16" height="16" alt=""/>인쇄 </button>
          <button id="btnDel" class="btn btn-default " type="button" > <img src="/fms/images/icon_btn_delete.png" width="16" height="16" alt=""/>삭제</button>
          <button id="btnSave" class="btn btn-default " type="button" > <img src="/fms/images/icon_btn_save.png" width="16" height="16" alt=""/>저장</button>
        </div>
      </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
          <colgroup>
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          </colgroup>

          <tbody>
			<tr>
				<th class="req">지형지물</th>
				<td>
					<select id="ftrCde" name="ftrCde" disabled class="disable reqVal" >
						<c:forEach var="cmtFtrcMaList" items="${cmtFtrcMaList}" varStatus="status">
							<option value="${cmtFtrcMaList.ftrCde }" ${resultVO.ftrCde == cmtFtrcMaList.ftrCde ? "selected" : "" } >${cmtFtrcMaList.ftrNam}</option>
						</c:forEach>
					</select>
				</td>
				<th class="req">관리번호</th>
				<td>
					<input type="text" id="ftrIdn" name="ftrIdn" value="${resultVO.ftrIdn }" readonly class="disable reqVal" />
				</td>
				<th class="req">행정동</th>
				<td>
					<select name="hjdCde" id="hjdCde" class="reqVal">
						<option value="">전체</option>
						<c:forEach var="cmtList" items="${cmtAdarMaList}" varStatus="status">
							<option value="${cmtList.hjdCde }"  ${cmtList.hjdCde == resultVO.hjdCde ? "selected" : "" } >${cmtList.hjdNam}</option>
						</c:forEach>
					</select>
				</td>
				<th >준공일자</th>
				<td>
					<input type="text" class="datepicker" name="fnsYmd" id="fnsYmd" value="${resultVO.fnsYmd}"/>
				</td>
			</tr>
			<tr>				
				<th >도엽번호</th>
				<td>
					<input type="text" name="shtNum" id="shtNum" value="${resultVO.shtNum }"/>
				</td>
				<th >관리기관</th>
				<td>
					<select id="mngCde" name="mngCde">
						<option value="">전체</option>
						<c:forEach var="cdeList" items="${dataCodeList}" varStatus="status">
							<option value="${cdeList.codeCode }"  ${cdeList.codeCode == resultVO.mngCde ? "selected" : "" } >${cdeList.codeAlias}</option>
						</c:forEach>	
					</select>
				</td>
				<th >허가일자</th>
				<td>
					<input type="text" class="datepicker" name="pmsYmd" id="pmsYmd" value="${resultVO.pmsYmd }"/>
				</td>
			</tr>
          </tbody>
        </table>
      </div>
    </div>
    <!-- //list --> 
    <!-- list -->
    <div class="view_box">
      <h2 class="m-b-5">시설현황 정보 </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
          <colgroup>
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          </colgroup>
          <tbody>
			<tr>
				<th >저수조명</th>
				<td>
					<input type="text" name="rsrNam" id="rsrNam" value="${resultVO.rsrNam }"/>
				</td>
				<th >건물유형</th>
				<td>					
					<select id="blsCde" name="blsCde">
						<option value="">선택안함</option>
						<c:forEach var="blsCdeList" items="${blsCdeList}" varStatus="status">
							<option value="${blsCdeList.codeCode }"  ${blsCdeList.codeCode == resultVO.blsCde ? "selected" : "" }>${blsCdeList.codeAlias}</option>
						</c:forEach>	
					</select>
				</td>
				<th >소유자성명</th>
				<td>
					<input type="text" name="ownNam" id="ownNam" value="${resultVO.ownNam }" />
				</td>
				<th >소유자주소</th>
				<td>
					<input type="text" name="ownAdr" id="ownAdr" value="${resultVO.ownAdr }" />
				</td>
			</tr>
			<tr>
				<th >소유자전화번호</th>
				<td>
					<input type="text" name="ownTel" id="ownTel" value="${resultVO.ownTel }" />
				</td>
				<th >관리자</th>
				<td>
					<input type="text" name="mngNam" id="mngNam" value="${resultVO.mngNam }" />
				</td>
				<th >관리자주소</th>
				<td>
					<input type="text" name="mngAdr" id="mngAdr" value="${resultVO.mngAdr }"/>
				</td>
				<th >관리자전화번호</th>
				<td>
					<input type="text" name="mngTel" id="mngTel" value="${resultVO.mngTel }" />
				</td>
			</tr>
			<tr>
				<th class="num2">건축면적</th>
				<td>
					<input type="text" name="bldAra" id="bldAra" value="${resultVO.bldAra }" class="numVal2"/>
				</td>
				<th class="num2">건축연면적</th>
				<td>
					<input type="text" name="tblAra" id="tblAra" value="${resultVO.tblAra }" class="numVal2" />
				</td>
				<th class="num">세대수</th>
				<td>
					<input type="text" name="famCnt" id="famCnt" value="${resultVO.famCnt }" class="numVal"/>
				</td>
				<th >건물주소</th>
				<td>
					<input type="text" name="bldAdr" id="bldAdr" value="${resultVO.bldAdr }" />
				</td>
			</tr>
			<tr>
				<th >대장초기화여부</th>
				<td>
					<select id="sysChk" name="sysChk">
						<option value="0" ${resultVO.sysChk == "0" ? "selected" : "" } >무</option>
						<option value="1" ${resultVO.sysChk == "1" ? "selected" : "" } >유</option>
					</select>
				</td>
			</tr>
		</tbody>
        </table>
      </div>
    </div>
     
    <!-- //list --> 
    
    <!-- tab -->
    <div class=" tab-box">
	
      <div class="tab-menu">
      
        <ul class="tab-menu">
			<li><a href="#" abbr="tab01" class="active">유지보수</a></li>
			<li><a href="#" abbr="tab02">사진첨부</a></li> 	
			<li><a href="#" abbr="tab03">참조자료</a></li> 	
			<li><a href="#" abbr="tab06">청소이력</a></li>
		</ul>
							
	  </div>  
	<!--tab list-->
		<div  id="tab01" class="tab-list active" >
			<c:import url="/pms/link/lnkChscList.do"/>
		</div>
		
		<div  id="tab02" class="tab-list" >
			<c:import url="/pms/link/lnkDwgPhotoList.do?bizId=${resultVO.ftrCde}${resultVO.ftrIdn}"/>
		</div>
		<div  id="tab03" class="tab-list" >
			<c:import url="/pms/link/lnkCnstRefList.do?bizId=${resultVO.ftrCde}${resultVO.ftrIdn}"/>
		</div>

		
		<div  id="tab06" class="tab-list" >
			<div class="btn_group m-b-5 right "  >
				<button id="btnList6Create"   type="button" class="btn btn-default " ><img src="/fms/images/icon_btn_w_regist.png" width="16" height="16" alt=""/> 등록</button>		
			</div>
	
			<table border="0" cellpadding="0" cellspacing="0"  class="table table-tab  " style="white-space: nowrap;">
				<colgroup>
					<col width="5%"/>
					<col width="10%"/>
					<col width="10%"/>									
					<col width="*"/>
					<col width="15%"/>	
				</colgroup>
				<thead>
					<tr>
						<th >순번</th>
						<th >청소일련번호</th>
						<th >청소일자</th>
						<th >청소내용</th>
						<th >청소업체명</th>
					</tr> 
				</thead>
				<tbody>
					<c:forEach var="list" items="${htResultList}" varStatus="listIndex">														
						<tr class="popup-link" href="${ctxRoot}/pms/acmFclt/wtrTrk/cleanHisViewPopup.do?seq=${list.seq}&ftrCde=${list.ftrCde}&ftrIdn=${list.ftrIdn}" title="세부시설현황등록" tab-flag="tab" popup-data="width=560, height=300" >
							<td><a href="javascript:;">${listIndex.index+1}</a></td>
							<td class="line_left"><a href="javascript:;">${list.seq}</a></td>
							<td><a href="javascript:;">${list.clnYmd}</a></td>
							<td class="line_left"><a href="javascript:;">${list.clnNam}</a></td>
							<td><a href="javascript:;">${list.clnExp}</a></td>
						</tr>	
					</c:forEach>
					
					<c:if test="${empty htResultList }">
						<tr class="none_tr">
							<td colspan="5" style="color:black; text-align:center;">검색 결과가 없습니다.</td>
						</tr>
					</c:if>	
				</tbody>
			</table>
		</div>

      <!--/tab list-->

    </div>
    <!-- //tab-box --> 
    
  </div>
  <!-- //container --> 
  
	</form>
</div>
<!-- //UI Object -->

</body>
</html>