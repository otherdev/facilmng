<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<script type="text/javascript">
var popupX = (screen.availWidth-660)/2;
var popupY= (screen.availHeight-1130)/3;


$(document).ready(function() {
	
	/*******************************************
	 * 화면초기화
	 *******************************************/
	gfn_init();
		
	var msg = '${msg}';
	
	if(msg) {
		gfn_alert(msg,"",function(){
			window.close();	
		});	
	}
	
	
	
	
	 /*******************************************
	 * 이벤트 설정
	 *******************************************/
	 $('#btnSave').click(function() {
		
		if(!gfn_formValid("wydtMetrFrm")) return;
		
		gfn_confirm("저장하시겠습니까?",function(ret){
			if(!ret) return;
	    	
			gfn_saveFormCmm("wydtMetrFrm", "insertWtlMetaPs", function(data){
				if(data.result){
					if('${type}' == 'edit'){
						gfn_alert("데이터가 저장되었습니다. 위치등록을 해주세요.","",function(){
							opener.document.getElementById('ftrIdn').value = $("#ftrIdn").val(); 
							window.close();
						});
					}
					else{
						gfn_alert("데이터가 저장되었습니다.","",function(){
							opener.fn_search();
							window.close();
						});
					}
				}else{
					gfn_alert("데이터 저장에 실패하였습니다. : " + data.error,"E");
				}
			});
		
		});
	});
	
	//공사번호 선택
	 $('#cntNum').click(function () {
		if ($('#cntNum').val() != '') {
			gfn_confirm("급수공사대장을 변경하시겠습니까?", function(ret){
				if(!ret) return false;
				
				window.open('${ctxRoot}/pms/popup/facetMngPopup.do', "_blank"
						, 'status=no, width=1100, height=618, left='+ popupX + ', top='+ popupY +', resizable=no');				 
			});
		}else{
			window.open('${ctxRoot}/pms/popup/facetMngPopup.do', "_blank"
					, 'status=no, width=1100, height=618, left='+ popupX + ', top='+ popupY +', resizable=no');	
		}				
	});
});	

function setChildValue(name) {
	document.getElementById("cntNum").value = name;
}
</script>

</head>


<body>


<div id="wrap" class="wrap" > 
  
  <!-- header -->
  <div id="header" class="header"  >
    <h1 >급수전계량기 대장 등록정보</h1>
  </div>
  <!-- // header --> 
  
<form name="wydtMetrFrm" id="wydtMetrFrm" method="post">
  
  <!-- container -->
  <div id="container"  class="content"  > 
    
    <!-- list -->
    <div class="view_box">
      <h2 ><span style="float:left">일반정보 </span>
	      <div class="btn_group m-b-5 right "  >
             <button id="btnSave" class="btn btn-default " type="button" > 
             	<img src="/fms/images/icon_btn_save.png" width="16" height="16" alt=""/>저장
             </button>
          </div>
      </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
          <colgroup>
          <col width="115px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          </colgroup>

          <tbody>
			<tr>
				<th class="req">지형지물</th>
				<td>
					<select id="ftrCde" name="ftrCde" class="disable reqVal">
						<c:forEach var="cmtFtrcMaList" items="${cmtFtrcMaList}" varStatus="status">
							<option value="${cmtFtrcMaList.ftrCde }" ${"SA122" == cmtFtrcMaList.ftrCde ? "selected" : "" } >${cmtFtrcMaList.ftrNam}</option>
						</c:forEach>
					</select>
				</td>
				<th class="req">관리번호</th>
				<td>
					<input type="text" id="ftrIdn" name="ftrIdn" readonly class="disable reqVal" value="${ftrIdnVal}" />
				</td>
				<th class="req">행정동</th>
				<td>
					<select name="hjdCde" id="hjdCde" class="reqVal">
						<option value="">선택안함</option>
						<c:forEach var="cmtList" items="${cmtAdarMaList}" varStatus="status">
							<option value="${cmtList.hjdCde }" >${cmtList.hjdNam}</option>
						</c:forEach>
					</select>
				</td>
				<th >공사번호</th>
				<td>
					<input type="text" id="cntNum" name="cntNum" />
				</td>
			</tr>
			<tr>				
				<th >설치일자</th>
				<td>
					<input type="text" class="datepicker" name="istYmd" id="istYmd" style="width:130px;" />
				</td>
				<th >도엽번호</th>
				<td>
					<input type="text" id="shtNum" name="shtNum" />
				</td>
			</tr>
          </tbody>
        </table>
      </div>
    </div>
    <!-- //list --> 

    
    <!-- list -->
    
    <div class="view_box">
      <h2 class="m-b-5">시설현황 정보 </h2>
      <div class="view ">
        <table border="0" cellpadding="0" cellspacing="0"  class="table  table-view " >
          <colgroup>
          <col width="115px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          <col width="114px">
          <col width="*">
          </colgroup>
          <tbody>
			<tr>
				<th >수용가번호</th>
				<td>
					<input type="text" id="homNum" name="homNum" />
				</td>
				<th >수용가성명</th>
				<td>
					<input type="text" id="homNam" name="homNam" />
				</td>
				<th >수용가행정동</th>
				<td>						
					<select id="homCde" name="homCde">
						<option value="">선택안함</option>
						<c:forEach var="cmtList" items="${cmtAdarMaList}" varStatus="status">
							<option value="${cmtList.hjdCde }" >${cmtList.hjdNam}</option>
						</c:forEach>
					</select>
				</td>
				<th >수용가주소</th>
				<td>
					<input type="text" id="homAdr" name="homAdr" />
				</td>
			</tr>
			<tr>
				<th class="num" >가구수</th>
				<td>
					<input type="text" id="homCnt" name="homCnt" class="numVal" />
				</td>
				<th >업종</th>
				<td>
					<select name="sbiCde" id="sbiCde">
						<option value="">선택안함</option>
						<c:forEach var="sbiCdeList" items="${sbiCdeList}" varStatus="status">
							<option value="${sbiCdeList.codeCode }" >${sbiCdeList.codeAlias}</option>
						</c:forEach>	
					</select>
				</td>
				<th >계량기물번호</th>
				<td>
					<input type="text" id="metNum" name="metNum" />
				</td>
				<th class="num">계량기구경</th>
				<td>
					<input type="text" id="metDip" name="metDip" class="numVal" />
				</td>
			</tr>
			<tr>
				<th >형식</th>
				<td>
					<select name="mofCde" id="mofCde">
						<option value="">선택안함</option>
						<c:forEach var="mofCdeList" items="${mofCdeList}" varStatus="status">
							<option value="${mofCdeList.codeCode }" >${mofCdeList.codeAlias}</option>
						</c:forEach>	
					</select>
				</td>
				<th >계량기제작회사명</th>
				<td>
					<input type="text" id="prdNum" name="prdNum" />
				</td>
			</tr>
		</tbody>
        </table>
      </div>
    </div>
    <!-- //list -->     
  </div>
  <!-- //container --> 
  
	</form>
</div>
<!-- //UI Object -->

</body>
</html>