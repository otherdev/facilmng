<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms.jspf"%>
<%@ include file="/WEB-INF/jsp/pms/include-fms-css.jspf"%>

<!-- Custom CSS -->
<link href="/fms/static/dist/css/style.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="<c:url value='/js/pms/dash/dashChart1.js' />" ></script><!-- 1.민원현황 -->
<script type="text/javascript" src="<c:url value='/js/pms/dash/dashChart2.js' />" ></script><!-- 2.소모품 재고현황 -->
<script type="text/javascript" src="<c:url value='/js/pms/dash/dashChart3.js' />" ></script><!-- 3.점검현황 -->
<script type="text/javascript" src="<c:url value='/js/pms/dash/dashChart4.js' />" ></script><!-- 4.시설물 현황 -->
<script type="text/javascript" src="<c:url value='/js/pms/dash/dashChart5.js' />" ></script><!-- 5.보수사유현황 -->
<script type="text/javascript" src="<c:url value='/js/pms/dash/dashChart6.js' />" ></script><!-- 6.공사현황 -->
<script type="text/javascript" src="<c:url value='/js/pms/dash/dashChart7.js' />" ></script><!-- 7.소모품 사용현황 -->

<body>	
<script type="text/javascript">

var aPrtChartList = ["a_chartdiv","b_chartdiv","c_chartdiv","d_chartdiv"];
var aPrtChartType = ["A","B"];	//A:막대, B:선차트
var aChartList = [];
var aChartCnt = 0;
var chartType = "A";

var sFullDate = new Date();
var sYear  = sFullDate.getFullYear();
var sMonth = (sFullDate.getMonth() + 1);
var sYm    = sFullDate.format("yyyyMM");

$(document).ready(function() {
	
	var selOptList = ""; 	
	
	for(var i=sYear;i>1900;i--){
		if(sYear == i){
			selOptList += "<option value='" + i + "' selected='selected'>" + i + "년</option>";	
		}else{
			selOptList += "<option value='" + i + "'>" + i + "년</option>";	
		}		
	}
	$("#selYearList").html(selOptList);
	
	selOptList = ""; 
	var sMon = "";
	for(var i=12;i>0;i--){
		if(i < 10) sMon = '0' + i;
		else sMon = i;
		
		if(sMonth == i){
			selOptList += "<option value='" + sMon + "' selected='selected'>" + i + "월</option>";	
		}else{
			selOptList += "<option value='" + sMon + "'>" + i + "월</option>";	
		}		
	}
	$("#selMonthList").html(selOptList);
				
	gfn_selectList({sqlId:'selectDashMenuList',	//유지보수(점검시설 결과)
					id: "${sessionScope.id}"
	   				}, function(ret){
		
	   		var list = ret.data;
	   			   		
	   		$.each(list, function(i, data){
	   				   			
	   			var objData = {};	
	   			if(data.mnuUseYn == "Y"){
	   				objData.mnuCd = data.mnuCd;
	   				objData.mnuNm = data.mnuNm; 
	   				objData.mnuFleNm = data.mnuFleNm;
	   				objData.mnuChartDivNm = aPrtChartList[aChartCnt];
	   				objData.mnuChartType  = aPrtChartType[0];
	   				aChartList[aChartCnt] = objData;
	   				aChartCnt++;
	   			}
	   		});
	   		
	   		fn_search(chartType, sYm);
	});		
	
	$( "#a_chartGb" ).change(function() {
		
		var ym = $("#selYearList").val() + $("#selMonthList").val();
		chartType = this.value;				
		fn_search(chartType,ym);
	});
		
	$( "#btnSave" ).click(function() {
		fn_save();
	});
		
	$("#btnTog").click(function() {	
		favorToggle();
	});
	
	$("#btnSearch").click(function() {		
		var ym = $("#selYearList").val() + $("#selMonthList").val();		
		fn_search(chartType,ym);
	});
	
	$(document).keydown(function(e) {
		//e.ctrlKey
		//e.altKey
		//e.shiftKey
		//13 keyCode => Enter
		if(e.ctrlKey && e.altKey && e.shiftKey && e.keyCode == 13){
			if( $("#selWhere").css("display") != "none" ) {
				// 
				$("#selWhere").hide();
			}else{
				$("#selWhere").show();
			}
			
		}
	});

	
	
	
});


function fn_search(type,ym){
	
	var data;	
	
	if(ym == null){
		ym = sYm;
	}else{
		if(ym.length != 6){
			ym = sYm;
		}
	}
	
	for(var i=0;i<aChartCnt;i++){
		
		data = aChartList[i];
		
		if(type != "A" && type != "B"){
			type = data.mnuChartType;
		}
		
		$("#" + data.mnuChartDivNm + "View").css("display","block");	
		
		if(data.mnuFleNm == "dashChart1"){
			dash_search1(data.mnuChartDivNm,type,ym)
		}else if(data.mnuFleNm == "dashChart2"){
			dash_search2(data.mnuChartDivNm,type,ym);
		}else if(data.mnuFleNm == "dashChart3"){
			dash_search3(data.mnuChartDivNm,type,ym);
		}else if(data.mnuFleNm == "dashChart4"){
			dash_search4(data.mnuChartDivNm,type,ym);
		}else if(data.mnuFleNm == "dashChart5"){
			dash_search5(data.mnuChartDivNm,type,ym);
		}else if(data.mnuFleNm == "dashChart6"){
			dash_search6(data.mnuChartDivNm,type,ym);
		}else if(data.mnuFleNm == "dashChart7"){
			dash_search7(data.mnuChartDivNm,type,ym);
		}
	}
	
	if(aChartCnt == 1){
		
	}
}


function fn_save(){
			
	if ($('#drawingTable .drawing_check:checked').length == 0 && $('#drawingTable .drawing_check').length > 0) {
		
		gfn_alert('저장할 항목이 없습니다.','W');
		
	} else if ($('#drawingTable .drawing_check:checked').length > 0 && $('#drawingTable .drawing_check').length > 0) {

		gfn_confirm('선택된 항목들을 저장하시겠습니까?',function(ret){
			if(!ret)	return;
			
			//ajax 변경
			var lst = [];
			$('#drawingTable .drawing_check:checked').each(function (i) {
				var data = {};
				data.mnuUseYn = $(this).attr("mnuUseYn");
				data.mnuCd = $(this).attr("mnuCd");
				console.log($(this).attr("mnuCd"));
				lst.push(data);
			});
			
			//로그인 사용자 정보 삭제
			gfn_saveCmmList(
				{lst:[{}], sqlId: 'deleteUserDashMnu', }
				, function(data){
					if (data.result) {
						//삭제 후 사용자별 메뉴 등록
						gfn_saveCmmList( 
							{lst: lst, sqlId: 'insertUserDashMnu', }
							, function(data){
								if (data.result) {
									gfn_alert("저장되었습니다.",function(){
										fn_reload();
									});
								}
								else{
									var err_msg = "저장에 실패하였습니다.";
									try{
										err_msg = data.error;
									}catch(e){}
									gfn_alert(err_msg,"E");
									return;
								}
							}
						);
					}
					else{
						var err_msg = "저장에 실패하였습니다.";
						try{
							err_msg = data.error;
						}catch(e){}
						gfn_alert(err_msg,"E");
						return;
					}
				}
			);	
			
		});		
	}	
}

//토글바 접힘
var favorToggle = function() {
	if ($("#itemDiv").is(':visible')) {
		
		$("#popMainDiv").css('left', '0');
		$("#popMainDiv").css('width', '100%');
		
		$("#btnTog").css('left', '0');
		$("#btnTog").html("▶");
		$("#itemDiv").toggle("slide", {
			direction : 'left'
		}, function() {			
			$("#bar").css('width', '0');		
		});		
	} else {
		$("#itemDiv").toggle("slide", {
			direction : 'left'
		}, function() {
			$("#btnTog").css('left', '100%');
			$("#btnTog").html("◀");
			$("#bar").css('width', '20%');
			
			$("#popMainDiv").css('left', '20%');
			$("#popMainDiv").css('width', '80%');
		});		
	}
};

function fn_chkMnuUseYn(obj){
	
	if ($('#drawingTable .drawing_check:checked').length > 4 && $('#drawingTable .drawing_check').length > 0) {
		
		obj.checked = false;
		gfn_alert('4개까지만 선택이 가능합니다.','W');
		return false;
	}
		
	if(obj.checked){
		$(".drawing_check").prop("mnuUseYn","Y");	
	}else{
		
		if ($('#drawingTable .drawing_check:checked').length == 0 && $('#drawingTable .drawing_check').length > 0) {
			
			obj.checked = true;
			gfn_alert('1개이상 선택하셔야 합니다.(1~4)','W');
			return false;
		}
		
		$(".drawing_check").prop("mnuUseYn","N");
	}	
}

var fn_reload = function(){
	location.reload();
}

</script>
<style>
#bar {
	width: 0; /*20%;*/
	height: 100%;
	background: #dce9e3;
	z-index: 1999;
	position: absolute;
	top: 0;
	right: 0;
	left:0;
}

#btnTog {
	width: 15px;
	height: 60px;
	background: #35495f;
	position: absolute;
	top: 40%;
	border-radius: 0px 5px 5px 0px;
	color: #76818e;
	padding: 20px 0px 0px 0px;
	border: 1px solid #13202e;	
	left : 0;/* 100%; */
}

#popMainDiv {
	width: 100%;/* 80%; */
	height: 100%;
	background: #ccc;
	position: absolute;
	top: 0;
	right: 0;
	left : 0;/* 20%; */
}

#itemDiv {
	width: 100%; 
	height: 100%;
	background: #dce9e3;
	position: absolute;
	top: 0;
	right: 0;
	left : 0;
}

#b_chartdiv, #a_chartdiv, #d_chartdiv, #c_chartdiv {
	width		: 100%;
	height		: 40%;
	font-size	: 10px;
	background-color : #f6f6f7;
}
.page-wrapper_z {
    margin-left: 0px;
    padding-top: 0px;
}

</style>

<div id="bar" style="background-color : #fff;">
	<div id="btnTog" style="cursor:pointer" >▶</div>
		<div id="itemDiv" style="border-left-color: rgb(19, 32, 46); border-left-width: 1px; border-left-style: solid; display:none">	        		          
	
		    <div class="btn_group" style="padding: 10px 10px 10px 10px;">
		    	 <span style="font-size:14px;float:left;padding-top:6px;"><img src="/fms/images/bullet_popup_con_title.png" width="14" height="14" alt=""/>&nbsp;항목</span>                 
                 <input class="AXButton Blue" id="btnSave" style="float: right;" type="button" value="저장">
            </div>	        
			<table border="0" cellpadding="0" cellspacing="0"  class="table table-tab  " style="margin: 30px 5px 5px 5px; width: 95%; white-space: nowrap;" id="drawingTable">
				<colgroup>
					<col width="10"></col>
					<col width="*"></col>
				</colgroup>
				<thead>
					<tr>
						<th style="text-align:center">선택</th>
						<th style="text-align:center">항목</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="list" items="${mnuList}" varStatus="status">
						<tr>
							<td class="line_left">
								<c:if test="${list.mnuUseYn eq 'Y'}">
									<input type="checkbox" class="drawing_check" onclick="fn_chkMnuUseYn(this);" mnuUseYn="${list.mnuUseYn}" mnuCd="${list.mnuCd}" checked />
								</c:if>
								<c:if test="${list.mnuUseYn ne 'Y'}">
									<input type="checkbox" class="drawing_check" onclick="fn_chkMnuUseYn(this);" mnuUseYn="${list.mnuUseYn}" mnuCd="${list.mnuCd}" />
								</c:if>
							</td>
							<td style="text-align: left; padding-left: 5px;">${list.mnuNm }</td>
						</tr>
					</c:forEach>
					
					<c:if test="${empty mnuList }">
						<tr class="none_tr">
							<td colspan="2" class="line_center">검색 결과가 없습니다.</td>
						</tr>
					</c:if>					
				</tbody>
			</table>
			
			<table id="selWhere" border="0" cellpadding="0" cellspacing="0"  style="margin: 30px 5px 5px 5px; width: 95%;display:none">
				<colgroup>
					<col width="80"></col>
					<col width="80"></col>
					<col width="*"></col>
				</colgroup>				
				<tbody>
					<tr>
					<td>
					<select id="selYearList" style="width:100%">
					 <option value="">선택</option>
					</select>
					</td>
					<td>
					<select id="selMonthList">
					<option value="">선택</option>
					</select>
					</td>
					<td>
					<input type="button" id="btnSearch" value="조회"/>
					</td>
					
					</tr>			
				</tbody>
			</table>
			
			
			
		</div>
	</div>
</div>
<!-- Main Content -->
<div id="popMainDiv" class="page-wrapper page-wrapper_z" style="position:relative;background:#fff;">
    <div class="container-fluid container-fluid_z">
		<div class="row">
			<div class="col-md-12">
				<!-- Row -->
				<div class="row">

					<div class="col-md-6_z" id="a_chartdivView" style="display:none">
						<div class="panel panel-default card-view" style="background-color:#dce9e3">
							<div class="panel-heading" style="background-color:#35495f">
								<div class="pull-left"> 
									<p class="panel-title"><span id="a_chartdivTitle"></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<select id="a_chartGb" style="height:20px;font-size:9px;display:none;">
									<option value="A">막대차트</option>
									<option value="B">선차트</option>
									</select>
									</p>								
								</div>
								<div class="clearfix"></div>
							</div>

							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div id="a_chartdiv"></div>
								</div>
							</div>

						</div>
					</div>

					<div class="col-md-6_z" id="b_chartdivView" style="display:none"> 
						<div class="panel panel-default card-view"  style="background-color:#dce9e3">
							<div class="panel-heading" style="background-color:#35495f">
								<div class="pull-left">
									<p class="panel-title"><span id="b_chartdivTitle"></span></p>									
								</div>
								<div class="clearfix"></div>
							</div>

							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div id="b_chartdiv"></div>
								</div>
							</div>

						</div>
					</div>
					
				</div>
				<!-- /Row -->

				<!-- Row -->
				<div class="row">

					<div class="col-md-6_z" id="c_chartdivView" style="display:none" >
						<div class="panel panel-default card-view"  style="background-color:#dce9e3">
							<div class="panel-heading" style="background-color:#35495f">
								<div class="pull-left" >
									<p class="panel-title"><span id="c_chartdivTitle"></span></p>
								</div>
								<div class="clearfix"></div>
							</div>

							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div id="c_chartdiv"></div>
								</div>
							</div>

						</div>
					</div>

						<div class="col-md-6_z" id="d_chartdivView" style="display: none">
							<div class="panel panel-default card-view"
								style="background-color: #dce9e3">
								<div class="panel-heading" style="background-color: #35495f">
									<div class="pull-left">
										<p class="panel-title">
											<span id="d_chartdivTitle"></span>
										</p>
									</div>
									<div class="clearfix"></div>
								</div>

								<div class="panel-wrapper collapse in">
									<div class="panel-body">
										<div id="d_chartdiv"></div>
									</div>
								</div>

							</div>
						</div>

					</div>
				<!-- /Row -->

			</div>
			<!-- /col-md-12 -->
		</div>
		<!-- /row -->
	</div>
	<!-- /container-fluid -->
</div>
<!-- /page-wrapper -->

</body>
</html>
<!-- HTML -->