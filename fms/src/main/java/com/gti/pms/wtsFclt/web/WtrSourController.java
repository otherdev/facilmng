package com.gti.pms.wtsFclt.web;

import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.gti.pms.cmm.service.CmmService;
import com.gti.pms.cmm.web.CodeController;
import com.gti.pms.cmm.web.CommandMap;
import com.gti.pms.wtsFclt.vo.WtrSourVO;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

/**
 * @Class Name : WtrSourController.java
 * @Description : WtrSour Controller class
 * @Modification Information
 *
 * @author drct
 * @since 2018-06-02
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Controller
@RequestMapping("/pms/wtsFclt/wtrSour")
public class WtrSourController extends CodeController {
	
	private static final Logger logger = LoggerFactory.getLogger(WtrSourController.class);

	@Resource(name = "cmmService")
	private CmmService cmmService;

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;	
	
	/**
	 * WTL_HEAD_PS 목록을 조회한다. (pageing)
	 * @param searchVO - 조회할 정보가 담긴 WtlHeadPsDefaultVO
	 * @return "/head/headPsList"
	 * @exception Exception
	 */
	@RequestMapping(value="/wtrSourList.do")
	public String wtrSourList(
			HttpServletRequest request,
			HttpServletResponse response,
			@ModelAttribute("searchVO") WtrSourVO searchVO,
			@RequestParam(value = "gisLayerNm", required = false) String gisLayerNm,
			Model model) throws Exception {
		
		/** EgovPropertyService.sample */
		searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
		searchVO.setPageSize(propertiesService.getInt("pageSize"));
		
		/** pageing */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());
		
		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		/* 편집 호출 유무 : yhhwang*/
		if (gisLayerNm != null) {
			searchVO.setGeomYN("Y");
		}
				
		List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectWtlWtrSourList", searchVO);
		model.addAttribute("resultList", list);
		
		List<Map<String, Object>> listCnt = (List<Map<String, Object>>) cmmService.selectList("selectWtlWtrSourListTotCnt", searchVO);
		int totCnt = 0;
		try{
			totCnt = ((BigDecimal) listCnt.get(0).get("totCnt")).intValue();
		}catch(Exception e){
			logger.debug("..." + e.getMessage());
		};
		
		model.addAttribute("totCnt", totCnt);
		
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);

		/* 편집 호출 유무 : yhhwang*/
		model.addAttribute("gisLayerNm", gisLayerNm);
		
		/*지형지물*/
		getFtrcMaListModel(model);		
		/*관리기관*/
		getMngCdeListModel(model);		
		/*행정동*/
		getAdarMaListModel(model);		
		/*수원구분*/
		getWsrCdeListModel(model);
		
		return "pms/wtsFclt/wtrSourList";
	}  

	@RequestMapping("/wtrSourCreate.do")
	public String wtrSourCreate(HttpServletRequest request,
			HttpServletResponse response, Model model,
			@RequestParam(value = "type", required = false) String type)
			throws Exception {
		
		Map<String, Object> map = new HashMap<String, Object>();		
		Map<String, Object> smap = (Map<String, Object>) cmmService.select("selectWtrSourTopFtrIdn", null);
		
		int ftrIdn = 0;
		try{
			ftrIdn = ((BigDecimal) smap.get("ftrIdn")).intValue();
		}catch(Exception e){
			logger.debug("..." + e.getMessage());
		};
		
		map.put("ftrIdn", (ftrIdn + 1));
		model.addAttribute("ftrIdnVal", map);
				
		// GIS에서 대장등록하기 위한 구분변수
		model.addAttribute("type", type);

		/*지형지물*/
		getFtrcMaListModel(model);		
		/*관리기관*/
		getMngCdeListModel(model);		
		/*행정동*/
		getAdarMaListModel(model);		
		/*수원구분*/
		getWsrCdeListModel(model);
		
		return "pms/wtsFclt/wtrSourCreate";
	} 
	
	@RequestMapping("/wtrSourView.do")
	public String wtrSourView(HttpServletRequest request,
			HttpServletResponse response, WtrSourVO paramVO, Model model)
			throws Exception {
		try {

			// 수원지 상세내역			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("ftrCde", paramVO.getFtrCde());
			map.put("ftrIdn", paramVO.getFtrIdn());
	
			Map<String, Object> rmap = (Map<String, Object>) cmmService.select("selectWtrSourView", map);			
			model.addAttribute("resultVO", rmap);
			
			model.addAttribute("ftrCde", paramVO.getFtrCde());
			model.addAttribute("ftrIdn", paramVO.getFtrIdn());
						
			/*지형지물*/
			getFtrcMaListModel(model);		
			/*관리기관*/
			getMngCdeListModel(model);		
			/*행정동*/
			getAdarMaListModel(model);		
			/*수원구분*/
			getWsrCdeListModel(model);  
			
		}catch (Exception e) {
			// TODO Auto-generated catch block
			model.addAttribute("res", "FAIL");
			model.addAttribute("err", "ERR005");
			model.addAttribute("msg", "조회할 내역이 없습니다.");
		}	 	
		
		return "pms/wtsFclt/wtrSourView";
	}
	@RequestMapping(value="/exceldownload.do")
	public void exceldownload(HttpServletRequest request, @ModelAttribute WtrSourVO vo, HttpServletResponse response , Model model) throws Exception {
		HSSFWorkbook objWorkBook = new HSSFWorkbook();
		HSSFSheet objSheet;
		HSSFRow objRow;
		HSSFCell objCell;
		OutputStream fileOut = null;
		
		Map<String, Object> map = new HashMap<String, Object>();
		BigDecimal dData = new BigDecimal(0);

		/*제목폰트*/
		HSSFFont font = objWorkBook.createFont();
		font.setFontHeightInPoints((short)20);
		font.setBoldweight((short)font.BOLDWEIGHT_BOLD);
		font.setFontName("맑은고딕");

		HSSFCellStyle titleStyle = objWorkBook.createCellStyle();
		titleStyle.setFont(font);
		titleStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		titleStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

		/*테이블 헤더 폰트*/
		HSSFFont headerFont = objWorkBook.createFont();
		headerFont.setFontHeightInPoints((short)10);
		headerFont.setBoldweight((short)font.BOLDWEIGHT_BOLD);
		headerFont.setFontName("맑은고딕");

		HSSFCellStyle tableHeaderStyle = objWorkBook.createCellStyle();
		tableHeaderStyle.setFont(headerFont);
		tableHeaderStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		tableHeaderStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		tableHeaderStyle.setBorderTop((short)1);
		tableHeaderStyle.setBorderBottom((short)1);
		
		/*테이블 내용 폰트*/
		HSSFFont contentFont = objWorkBook.createFont();
		contentFont.setFontHeightInPoints((short)9);
		contentFont.setBoldweight((short)font.BOLDWEIGHT_NORMAL);
		contentFont.setFontName("맑은고딕");

		HSSFCellStyle contentStyle = objWorkBook.createCellStyle();
		contentStyle.setFont(contentFont);
		contentStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		contentStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		
		/*왼쪽 정렬*/
		HSSFCellStyle contentLeftStyle = objWorkBook.createCellStyle();
		contentLeftStyle.setFont(contentFont);
		contentLeftStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
		contentLeftStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		
		/*오른쪽 정렬*/
		HSSFCellStyle contentRightStyle = objWorkBook.createCellStyle();
		contentRightStyle.setFont(contentFont);
		contentRightStyle.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
		contentRightStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		
		objSheet = objWorkBook.createSheet("수원지");

		Date today = new Date();
		SimpleDateFormat format1 = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");

		 // 제목 행
		objRow = objSheet.createRow(0);
		objRow.setHeight ((short) 0x250);
		objCell = objRow.createCell(0);
		objCell.setCellValue("수원지 목록");
		objCell.setCellStyle(titleStyle);
		objSheet.addMergedRegion(new CellRangeAddress(0,0,0,8));
 
		// 시간 행
		objRow = objSheet.createRow(2);
		objRow.setHeight ((short) 0x150);
		objCell = objRow.createCell(0);
		objCell.setCellValue(format2.format(today));		
		objCell.setCellStyle(tableHeaderStyle);

		// 헤더 행
		objRow = objSheet.createRow(3);
		objRow.setHeight ((short) 0x150);
		
		objCell = objRow.createCell(0);
		objCell.setCellValue("순번");
		objCell.setCellStyle(tableHeaderStyle);

		objCell = objRow.createCell(1);
		objCell.setCellValue("지형지물");
		objCell.setCellStyle(tableHeaderStyle);

		objCell = objRow.createCell(2);
		objCell.setCellValue("관리번호");
		objCell.setCellStyle(tableHeaderStyle);

		objCell = objRow.createCell(3);
		objCell.setCellValue("행정동");
		objCell.setCellStyle(tableHeaderStyle);
		
		objCell = objRow.createCell(4);
		objCell.setCellValue("도엽번호");
		objCell.setCellStyle(tableHeaderStyle);
		
		objCell = objRow.createCell(5);
		objCell.setCellValue("관리기관");
		objCell.setCellStyle(tableHeaderStyle);
		
		objCell = objRow.createCell(6);
		objCell.setCellValue("준공일자");
		objCell.setCellStyle(tableHeaderStyle);

		List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectWtrSourExcelList", vo);
   		
   		int listSize = 0;
		if( list.size()>10000) {
			listSize = 10000;
		} else {
			listSize = list.size();
		}

		
		int i = 0; 
		for( i=0; i<listSize; i++ ) { 			
			map = list.get(i);

			objRow = objSheet.createRow(i+4);
			objRow.setHeight ((short) 0x150);
			
			objCell = objRow.createCell(0);
			objCell.setCellValue(i+1);		
			objCell.setCellStyle(contentStyle);
			
			objCell = objRow.createCell(1);
			objCell.setCellValue((String) map.get("ftrCdeNam"));	
			objCell.setCellStyle(contentStyle);
			
			dData = (BigDecimal) map.get("ftrIdn");
			objCell = objRow.createCell(2);						
   			if(dData == null){
   				objCell.setCellValue("");	
   			}else{
   				objCell.setCellValue(dData.intValue());		
   			}	
			objCell.setCellStyle(contentStyle);
			
			objCell = objRow.createCell(3);
			objCell.setCellValue((String) map.get("hjdCdeNam"));
			objCell.setCellStyle(contentStyle);
			
			objCell = objRow.createCell(4);
			objCell.setCellValue((String) map.get("shtNum"));
			objCell.setCellStyle(contentStyle);
			
			objCell = objRow.createCell(5);
			objCell.setCellValue((String) map.get("mngCdeNam"));
			objCell.setCellStyle(contentStyle);
			
			objCell = objRow.createCell(6);	
			objCell.setCellValue((String) map.get("fnsYmd"));
			objCell.setCellStyle(contentStyle);			
			
		}
		
		for (i=0; i < 7; i++) {
			objSheet.autoSizeColumn((short)i);
		}
		
		response.setContentType("Application/Msexcel");
		response.setHeader("Content-Disposition", "ATTachment; Filename="+URLEncoder.encode("수원지","UTF-8") + "_" + format1.format(today) +".xls");
		fileOut  = response.getOutputStream(); 
		objWorkBook.write(fileOut);
		fileOut.close();
		response.getOutputStream().flush();
		response.getOutputStream().close();
	}
		
	
	/**
	 * 수원지 신규저장
	 * @param commandMap
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/insertWtlHead.do")
	public ModelAndView saveChgMst(CommandMap commandMap, HttpServletRequest request) throws Exception {
		ModelAndView mv = new ModelAndView("/proc/proc");

		
		// 1.공정변경 마스터 저장
		commandMap.getMap().put("sqlId","insertWtlHeadPs");
		String result = cmmService.saveFormData(commandMap.getMap());
		
		JSONObject ret = new JSONObject();
		ret.put("result", true);
		ret.put("ftrIdn", commandMap.getMap().get("ftrIdn"));
		
		mv.addObject("result", ret.toJSONString());

		return mv;
	}	
	
}