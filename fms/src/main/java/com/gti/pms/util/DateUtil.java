package com.gti.pms.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

	/**
	 * 날짜형식 변경
	 * ex) 2017-01-01 -> 20170101
	 * @param str
	 * @return
	 */
	public static String dateToVarchar (String str) {
		String[] replaceStr = {"-", ".", "/"};
		
		for (String s : replaceStr) {
			str = str.replaceAll(s, "");
		}
		return str;
	}
	
	/**
	 * 날짜형식 변경
	 * ex) 20170101 -> 2017-01-01
	 * @param str
	 * @return
	 */
	public static String dateAddHyphen (String str) {
		String year = str.substring(0, 3);
		String month = str.substring(4, 5);
		String day = str.substring(6, 7);
		
		return year + "-" + month + "-" + day;
	}
	
	/**
	 * 날짜 계산 - 현재일 기준 
	 * @param begin
	 * @param end
	 * @return
	 * @throws Exception
	 */
	public static long diffOfDate(String begin) throws Exception {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
	 
	    Date beginDate = formatter.parse(begin);
	    Date endDate = formatter.parse(formatter.format(new Date()));
	 
	    long diff = endDate.getTime() - beginDate.getTime();
	    long diffDays = diff / (24 * 60 * 60 * 1000);
	    return diffDays;
	}
	
	/**
	 * 날짜 계산 
	 * @param begin
	 * @return
	 * @throws Exception
	 */
	public static long diffOfDate2(String begin, String end) throws Exception {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
	 
	    Date beginDate = formatter.parse(begin);
	    Date endDate = formatter.parse(end);
	 
	    long diff = endDate.getTime() - beginDate.getTime();
	    long diffDays = diff / (24 * 60 * 60 * 1000);
	 
	    return diffDays;
	}
}
