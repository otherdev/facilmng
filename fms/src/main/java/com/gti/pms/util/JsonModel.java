package com.gti.pms.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

/**
 * JSON 응답 클래스
 * @Class JsonModel
 * @author 김성천
 * @since 2016. 6. 28.
 * @see
 *
 * <pre>
 *<<개정이력(Modification Information)>>
 * 수정일              수정자      수정내용
 * 2016. 6. 28.  김성천
 *</pre>
 */
public class JsonModel {
	Map data=new HashMap();
	PaginationInfo paginationInfo=new PaginationInfo();
	/** 애플리케이션에서 JsonObject 형태로 주고 받을 상태코드 */
	String resultMessage   = "empty message";
	/** 애플리케이션에서 JsonObject 형태로 주고 받을 성공 메세지 명 */
	String resultFlag = "success";
	Integer resultStatusCode = 200;
	
	
	public JsonModel() {
		this.paginationInfo.setCurrentPageNo(1);
		this.paginationInfo.setPageSize(1);
		this.paginationInfo.setTotalRecordCount(1);
		this.paginationInfo.setRecordCountPerPage(1);
	}
	
	public Map getData() {
		return data;
	}
	public void setData(Map data) {
		this.data = data;
	}
	public void putData(String key, Object data){
		this.data.put(key, data);
	}
	public void putData(String key, Collection<Object> data){
		this.data.put(key, data);
	}

	public PaginationInfo getPaginationInfo() {
		return paginationInfo;
	}
	public void setPaginationInfo(PaginationInfo paginationInfo) {
		this.paginationInfo = paginationInfo;
	}
	public String getResultMessage() {
		return resultMessage;
	}
	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}
	public String getResultFlag() {
		return resultFlag;
	}
	public void setResultFlag(String resultFlag) {
		this.resultFlag = resultFlag;
	}
	public Integer getResultStatusCode() {
		return resultStatusCode;
	}
	public void setResultStatusCode(Integer resultStatusCode) {
		this.resultStatusCode = resultStatusCode;
	}
	public void initFailField(){
		this.setResultFlag("fail");
		this.setResultStatusCode(500);
	}
}