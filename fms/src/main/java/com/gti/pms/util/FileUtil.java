package com.gti.pms.util;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.multipart.MultipartFile;

import egovframework.rte.fdl.property.EgovPropertyService;

public class FileUtil {
	/**
	 * Upload to Server or Local
	 * 
	 * @param file
	 * @param type ("drawing", "reference", ...)
	 * @return
	 * @throws IOException
	 */
	public static String upload(HttpServletRequest request, MultipartFile file) throws IOException {
		EgovPropertyService service = (EgovPropertyService)WebApplicationContextUtils
				.getWebApplicationContext(request.getSession().getServletContext())
				.getBean("propertiesService");
		
		return upload(
    			request,
    			file,
    			service.getString("fms.file.server.host"),
    			service.getInt("fms.file.server.port"),
    			service.getString("fms.file.server.username"),
    			service.getString("fms.file.server.password"),
    			service.getString("fms.file.path"),
    			service.getString("fms.file.path.local")
    			);
	}
	
	/**
	 * Upload to Server or Local
	 * 
	 * @param file
	 * @param host
	 * @param port
	 * @param username
	 * @param password
	 * @param path
	 * @return
	 * @throws IOException
	 */
	public static String upload(HttpServletRequest request, MultipartFile file,
			String host, int port, String username, String password,
			String remotePath, String localPath) throws IOException {
		if (remotePath == null || remotePath.equals("")) {
			//Local
			return uploadToLocal(file,
//					request.getSession().getServletContext().getRealPath(localPath));
					localPath);
    	} else {
    		//Server
    		return uploadToServer(file, host, port, username, password, remotePath);
    	}
	}
	
	/**
	 * Upload to Server
	 * 
	 * @param file
	 * @param host(ip)
	 * @param port
	 * @param username
	 * @param password
	 * @param path
	 * @return uploaded file name (null:upload fail)
	 * @throws IOException
	 */
	public static String uploadToServer(MultipartFile file, String host, int port,
			String username, String password, String path) throws IOException {
		FTPClient client = null;
		InputStream in = null;
		
		try {
			client = new FTPClient();
			client.connect(host, port);
			
			int reply = client.getReplyCode();
			
			if (!FTPReply.isPositiveCompletion(reply)) {
				client.logout();
				client.disconnect();
				
				throw new IOException("FTP server refused connection.");
			}
			
			client.login(username, password);
			
			in = file.getInputStream();
			
			String name = createUploadFileName(file.getOriginalFilename());
			
			boolean result = client.storeFile(path + "/" + name, in);
			
			if (result) {
				return name;
			}
			
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			
			return null;
		} finally {
			if (in != null) in.close();
			if (client != null) client.logout();
			if (client != null) client.disconnect();
		}
	}
	
	/**
	 * Upload to Local
	 * 
	 * @param file
	 * @param path
	 * @return uploaded file name (null:upload fail)
	 * @throws IOException
	 */
	public static String uploadToLocal(MultipartFile file, String path) {
		try {
			String fileName = createUploadFileName(file.getOriginalFilename());
			
			file.transferTo(new File(path + "/" + fileName));
			
			return fileName;
		} catch (Exception e) {
			e.printStackTrace();
			
			return null;
		}
	}
	
	/**
	 * Create Unique File name
	 * 
	 * @param originalName
	 * @return unique file name
	 */
	public static String createUploadFileName(String originalName) {
		String ext = originalName.substring(originalName.lastIndexOf("."));
		String name = originalName.substring(0, originalName.lastIndexOf("."));
		
		return name + "_" + new Date().getTime() + ext;
	}
	
	/**
	 * return ".jpg"
	 * 
	 * @param fileName
	 * @return file ext
	 */
	public static String getExt(String fileName) {
		return fileName.substring(fileName.lastIndexOf(".") + 1);
	}
	
	public static Map<String, Integer> getWidthHeight(MultipartFile file) {
		int width = 0;
		int height = 0;
		
		try {
			BufferedImage bi = ImageIO.read(file.getInputStream());
			
			width = bi.getWidth();
			height = bi.getHeight();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Map<String, Integer> map = new HashMap<>();
		map.put("width", width);
		map.put("height", height);
		
		return map;
	}
	
	/**
	 * 브라우저 구분 얻기.
	 *
	 * @param request
	 * @return
	 */
	public static String getBrowser(HttpServletRequest request) {
		String header = request.getHeader("User-Agent");
		if (header.indexOf("MSIE") > -1) {
			return "MSIE";
		} else if (header.indexOf("Trident") > -1) { // IE11 문자열 깨짐 방지
			return "Trident";
		} else if (header.indexOf("Chrome") > -1) {
			return "Chrome";
		} else if (header.indexOf("Opera") > -1) {
			return "Opera";
		}
		return "Firefox";
	}

	/**
	 * Disposition 지정하기.
	 *
	 * @param filename
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public static void setDisposition(String filename, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String browser = getBrowser(request);

		String dispositionPrefix = "attachment; filename=";
		String encodedFilename = null;

		if (browser.equals("MSIE")) {
			encodedFilename = URLEncoder.encode(filename, "UTF-8").replaceAll("\\+", "%20");
		} else if (browser.equals("Trident")) { // IE11 문자열 깨짐 방지
			encodedFilename = URLEncoder.encode(filename, "UTF-8").replaceAll("\\+", "%20");
		} else if (browser.equals("Firefox")) {
			encodedFilename = "\"" + new String(filename.getBytes("UTF-8"), "8859_1") + "\"";
		} else if (browser.equals("Opera")) {
			encodedFilename = "\"" + new String(filename.getBytes("UTF-8"), "8859_1") + "\"";
		} else if (browser.equals("Chrome")) {
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < filename.length(); i++) {
				char c = filename.charAt(i);
				if (c > '~') {
					sb.append(URLEncoder.encode("" + c, "UTF-8"));
				} else {
					sb.append(c);
				}
			}
			encodedFilename = sb.toString();
		} else {
			//throw new RuntimeException("Not supported browser");
			throw new IOException("Not supported browser");
		}

		response.setHeader("Content-Disposition", dispositionPrefix + encodedFilename);

		if ("Opera".equals(browser)) {
			response.setContentType("application/octet-stream;charset=UTF-8");
		}
	}
}
