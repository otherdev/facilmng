package com.gti.pms.util;

import java.text.DecimalFormat;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

public class NumberUtil {
	
	/**
	 * 랜덤 키 난수 생성 
	 * 범위 0 ~ 999999999 (9자리)
	 * param index - 자릿수
	 * @return
	 */
	public static String randomKey (int index) {
		
		//System.out.println(randomNumber);
		
		String randomKey = "";
		
		for (int i = 0; i < index; i++) {
			Integer number = (int) (Math.random() * 10);
			randomKey += number.toString();
		}
		return randomKey;
	}
	
	public static String toDecimal(Number number) {
        return toDecimal(number, "#.###");
    }

    public static String toDecimal(String number) {
        return toDecimal(number, "#.###");
    }
    
    public static String toDecimal(String number, String format) {
        if (StringUtils.isEmpty(number)) {
            return "0";
        }

        number = number.replaceAll(",", "");

        if (!NumberUtils.isNumber(number)) {
            return "0";
        }

        return new DecimalFormat(format).format(Double.parseDouble(number));
    }
    
    public static String toDecimal(Number number, String format) {
        if (number == null) {
            return "0";
        }

        return new DecimalFormat(format).format(number);
    }
}
