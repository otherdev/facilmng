package com.gti.pms.addr.web;

import java.util.List;

import javax.annotation.Resource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gti.pms.addr.service.AddrService;
import com.gti.pms.addr.vo.AddrVo;
import com.gti.pms.util.JsonModel;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

@Controller
//@SessionAttributes(types=AddrVo.class)
public class AddrController {

	@Resource(name = "addrService")
	private AddrService addrService;

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;
	
	 
	@RequestMapping(value="/addr/addrList.do")
	public String addrList(@ModelAttribute("searchVO") AddrVo searchVO, ModelMap model) throws Exception {
		
		/** EgovPropertyService.sample */
		searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
		searchVO.setPageSize(propertiesService.getInt("pageSize"));
		
		/** pageing */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());
		
		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		
		List<?> addrList = addrService.selectAddrJb(searchVO);
		AddrVo advo = (AddrVo) addrList.get(0);
		
		/*STRUCT st = (oracle.sql.STRUCT) advo.getGeom();
		JGeometry j_geom = JGeometry.load(st);*/
	
		model.addAttribute("resultList", addrList);
		
		
		return "/waterpipe/rsrv/psList";
	} 
	
	@ResponseBody
	@RequestMapping(value="/addr/selectAddrHBd.do")
	public JsonModel selectAddrBj(@ModelAttribute("searchVO") AddrVo searchVO, JsonModel model) throws Exception {
		
		List<?> AddrList = null;
		String bjdChk = searchVO.getBjdChk();
		
		if(bjdChk.equals("0")){
			AddrList = addrService.selectAddrHj(searchVO);
		} 
		else {
			AddrList = addrService.selectAddrBj(searchVO);
		}
		model.putData("AddrList", AddrList);
		
		return model;
	}
	
	@ResponseBody
	@RequestMapping(value="/addr/searchAddr.do")
	public JSONObject searchAddr(@ModelAttribute("searchVO") AddrVo searchVO, JsonModel model) throws Exception {
		
		AddrVo searchAddr = (AddrVo) addrService.searchAddr(searchVO);
		
		return resultJson(searchAddr);
	}
	
	public JSONObject resultJson(AddrVo addrVo) {
		JSONObject featureCollection = new JSONObject();
		// geom만 보내줘도 그려진다.
		// key 대소문자 구분함 .
		// id가 중복되면 하나의 feature만 보여준다.
		JSONObject geomObj = new JSONObject();
		try {
			featureCollection.put("type", "FeatureCollection");
			featureCollection.put("totalFeatures", 2);

			// CRS 
			JSONObject crsObj = new JSONObject();
			JSONObject proObjs = new JSONObject();
			JSONObject proObj = new JSONObject();
			proObj.put("name","urn:ogc:def:crs:EPSG::5187");
			proObjs.put("properties", proObj);
			crsObj.put("properties", proObj);
			crsObj.put("type", "name");
			featureCollection.put("crs", crsObj);
			
			// features
			JSONArray featureList = new JSONArray();
			// System.out.println(addrVo.getGeom());
			// POLYGON ((232744.73 324698.63, 232744.72 324698.63, 232743.98 324698.76, 232743.97 324698.76, 232729.99 324701.33, 232705.39 324708.52, 232704.68 324708.77, 232702.72 324709.45, 232702.71 324709.44, 232701.22 324709.24, 232701.22 324709.24, 232701.21 324709.24, 232701.22 324708.77, 232701.52 324696.5, 232695.99 324697.52, 232695.98 324697.52, 232692.99 324695.86, 232692.99 324695.85, 232697.21 324693.84, 232699.79 324692.62, 232719.51 324682.21, 232723.82 324678.98, 232723.83 324678.97, 232725.5 324683.47, 232731.2 324687.8, 232737.47 324690.7, 232744.73 324698.63))
			String geomStr = addrVo.getGeom();
			geomStr = geomStr.replace("POLYGON ((", " ");
			geomStr = geomStr.replace("))", "");
			String[] geom = geomStr.split(",");
					
			JSONObject ftsObj = new JSONObject();

			geomObj.put("type","MultiPolygon");

			JSONArray geomList = new JSONArray();
			JSONArray JSONArrayCoord1 = new JSONArray();
			JSONArray JSONArrayCoord2 = new JSONArray();
			JSONArray JSONArrayCoord3 = new JSONArray();
			
			for (int a =0; a < geom.length ; a++) {
				String[] xy = geom[a].split(" ");
				
				JSONArrayCoord3 = new JSONArray();
				JSONArrayCoord3.add(0, Double.parseDouble(xy[1]));
				JSONArrayCoord3.add(1, Double.parseDouble(xy[2]));
				
				JSONArrayCoord2.add(JSONArrayCoord3);
				
			}
			
			JSONArrayCoord1.add(JSONArrayCoord2);
			geomList.add(JSONArrayCoord1);
			geomObj.put("coordinates", geomList);
			
			ftsObj.put("geometry", geomObj);
			/*JSONObject prosObj = new JSONObject();
			prosObj.put("FTR_CDE","SA403");
			prosObj.put("FTR_IDN",100001);
			prosObj.put("SYS_CHK","Y");
			prosObj.put("CNT_NUM",null);
			ftsObj.put("properties", prosObj);*/
			ftsObj.put("geometry_name", "GEOM");
			ftsObj.put("type", "Feature");
			ftsObj.put("id", "WTL_BDST_AS.177");
			
			featureList.add(ftsObj);
			
			featureCollection.put("features", featureList);

		} catch (Exception e) {
			e.printStackTrace();
		}
		// output the result
		// System.out.println(featureCollection.toString());
		return featureCollection;
	}
}