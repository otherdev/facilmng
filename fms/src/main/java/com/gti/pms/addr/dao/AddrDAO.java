package com.gti.pms.addr.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import egovframework.rte.psl.dataaccess.EgovAbstractDAO;
import com.gti.pms.addr.vo.AddrVo;

/**
 * @Class Name : AddrDAO.java
 * @Description : Addr DAO Class
 * @Modification Information
 *
 * @author DRCTS
 * @since 2018-07-01
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Repository("AddrDAO")
public class AddrDAO extends EgovAbstractDAO {


    public List<?> selectAddrBj(AddrVo searchVO) throws Exception {
        return list("AddrDAO.selectAddrBj", searchVO);
    }
    
    public List<?> selectAddrHj(AddrVo searchVO) throws Exception {
        return list("AddrDAO.selectAddrHj", searchVO);
    }
    
    public List<?> selectAddrJb(AddrVo searchVO) throws Exception {
        return list("AddrDAO.selectAddrJb", searchVO);
    }

	public Object searchAddr(AddrVo searchVO) {
		return select("AddrDAO.searchAddr", searchVO);
	}

}
