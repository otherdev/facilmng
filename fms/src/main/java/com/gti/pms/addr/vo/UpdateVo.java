package com.gti.pms.addr.vo;

import java.io.Serializable;

public class UpdateVo implements Serializable{

	private static final long serialVersionUID = 1L;

	private String ftrCde;
	
	private Integer ftrIdn;
	
	private String layerNm;
	
	private String coordinates;
	
	private String insertYn;
	
	public String getInsertYn() {
		return insertYn;
	}
	public void setInsertYn(String insertYn) {
		this.insertYn = insertYn;
	}
	public String getFtrCde() {
		return ftrCde;
	}
	public void setFtrCde(String ftrCde) {
		this.ftrCde = ftrCde;
	}
	public Integer getFtrIdn() {
		return ftrIdn;
	}
	public void setFtrIdn(Integer ftrIdn) {
		this.ftrIdn = ftrIdn;
	}
	public String getLayerNm() {
		return layerNm;
	}
	public void setLayerNm(String layerNm) {
		this.layerNm = layerNm;
	}
	public String getCoordinates() {
		return coordinates;
	}
	public void setCoordinates(String coordinates) {
		this.coordinates = coordinates;
	}
	
}
