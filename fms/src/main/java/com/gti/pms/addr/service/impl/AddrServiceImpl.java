package com.gti.pms.addr.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import com.gti.pms.addr.dao.AddrDAO;
import com.gti.pms.addr.service.AddrService;
import com.gti.pms.addr.vo.AddrVo;

@Service("addrService")
public class AddrServiceImpl extends EgovAbstractServiceImpl implements AddrService {
        
    private static final Logger LOGGER = LoggerFactory.getLogger(AddrServiceImpl.class);

    @Resource(name="AddrDAO")
    private AddrDAO AddrDAO;
    
    /** ID Generation */
    //@Resource(name="{egovAddrIdGnrService}")    
    //private EgovIdGnrService egovIdGnrService;

	

    public List<?> selectAddrBj(AddrVo searchVO) throws Exception {
        return AddrDAO.selectAddrBj(searchVO);
    }
    
    public List<?> selectAddrHj(AddrVo searchVO) throws Exception {
        return AddrDAO.selectAddrHj(searchVO);
    }
    
    public List<?> selectAddrJb(AddrVo searchVO) throws Exception {
        return AddrDAO.selectAddrJb(searchVO);
    }

	public Object searchAddr(AddrVo searchVO) throws Exception {
		return AddrDAO.searchAddr(searchVO);
	}

}
