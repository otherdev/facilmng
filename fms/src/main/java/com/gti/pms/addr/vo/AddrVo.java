package com.gti.pms.addr.vo;

import java.io.Serializable;

public class AddrVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6677392357195025226L;

	/** 검색조건 */
	private String searchCondition = "";

	/** 검색Keyword */
	private String searchKeyword = "";

	/** 검색사용여부 */
	private String searchUseYn = "";

	/** 현재페이지 */
	private int pageIndex = 1;

	/** 페이지갯수 */
	private int pageUnit = 10;

	/** 페이지사이즈 */
	private int pageSize = 10;

	/** firstIndex */
	private int firstIndex = 1;

	/** lastIndex */
	private int lastIndex = 1;

	/** recordCountPerPage */
	private int recordCountPerPage = 10;
	
	private String fid;
	
	private String ftrCde;
	private String hjdCde;
	private String hjdNm;
	
	private String bjdCde;
	private String bjdNm;
	
	// 법정동/행정동 체크
	private String bjdChk;
	
	private String sanChk;
	private String facNum;
	private String fadNum;
	private String nrdNm;
	private String nfcNum;
	private String nfdNum;
	private String parLbl;
	private String faeCde;
	
	private String geom;

	public String getGeom() {
		return geom;
	}

	public void setGeom(String geom) {
		this.geom = geom;
	}

	public String getFid() {
		return fid;
	}

	public void setFid(String fid) {
		this.fid = fid;
	}



	public String getFtrCde() {
		return ftrCde;
	}

	public void setFtrCde(String ftrCde) {
		this.ftrCde = ftrCde;
	}

	public String getHjdCde() {
		return hjdCde;
	}

	public void setHjdCde(String hjdCde) {
		this.hjdCde = hjdCde;
	}

	public String getHjdNm() {
		return hjdNm;
	}

	public void setHjdNm(String hjdNm) {
		this.hjdNm = hjdNm;
	}

	public String getBjdCde() {
		return bjdCde;
	}

	public void setBjdCde(String bjdCde) {
		this.bjdCde = bjdCde;
	}

	public String getBjdNm() {
		return bjdNm;
	}

	public void setBjdNm(String bjdNm) {
		this.bjdNm = bjdNm;
	}

	public String getSanChk() {
		return sanChk;
	}

	public void setSanChk(String sanChk) {
		this.sanChk = sanChk;
	}

	public String getBjdChk() {
		return bjdChk;
	}

	public void setBjdChk(String bjdChk) {
		this.bjdChk = bjdChk;
	}

	public String getFacNum() {
		return facNum;
	}

	public void setFacNum(String facNum) {
		this.facNum = facNum;
	}

	public String getFadNum() {
		return fadNum;
	}

	public void setFadNum(String fadNum) {
		this.fadNum = fadNum;
	}

	public String getNrdNm() {
		return nrdNm;
	}

	public void setNrdNm(String nrdNm) {
		this.nrdNm = nrdNm;
	}

	public String getNfcNum() {
		return nfcNum;
	}

	public void setNfcNum(String nfcNum) {
		this.nfcNum = nfcNum;
	}

	public String getNfdNum() {
		return nfdNum;
	}

	public void setNfdNum(String nfdNum) {
		this.nfdNum = nfdNum;
	}

	public String getParLbl() {
		return parLbl;
	}

	public void setParLbl(String parLbl) {
		this.parLbl = parLbl;
	}

	public String getFaeCde() {
		return faeCde;
	}

	public void setFaeCde(String faeCde) {
		this.faeCde = faeCde;
	}

	public String getSearchCondition() {
		return searchCondition;
	}

	public void setSearchCondition(String searchCondition) {
		this.searchCondition = searchCondition;
	}

	public String getSearchKeyword() {
		return searchKeyword;
	}

	public void setSearchKeyword(String searchKeyword) {
		this.searchKeyword = searchKeyword;
	}

	public String getSearchUseYn() {
		return searchUseYn;
	}

	public void setSearchUseYn(String searchUseYn) {
		this.searchUseYn = searchUseYn;
	}

	public int getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}

	public int getPageUnit() {
		return pageUnit;
	}

	public void setPageUnit(int pageUnit) {
		this.pageUnit = pageUnit;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getFirstIndex() {
		return firstIndex;
	}

	public void setFirstIndex(int firstIndex) {
		this.firstIndex = firstIndex;
	}

	public int getLastIndex() {
		return lastIndex;
	}

	public void setLastIndex(int lastIndex) {
		this.lastIndex = lastIndex;
	}

	public int getRecordCountPerPage() {
		return recordCountPerPage;
	}

	public void setRecordCountPerPage(int recordCountPerPage) {
		this.recordCountPerPage = recordCountPerPage;
	}

}
