package com.gti.pms.addr.service;

import java.util.List;
import com.gti.pms.addr.vo.AddrVo;

/**
 * @Class Name : AddrService.java
 * @Description : Addr Business class
 * @Modification Information
 *
 * @author DRCTS
 * @since 2018-07-01
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */
public interface AddrService {
	
    List selectAddrBj(AddrVo searchVO) throws Exception;
    
    List selectAddrHj(AddrVo searchVO) throws Exception;
    
    List selectAddrJb(AddrVo searchVO) throws Exception;
    
	Object searchAddr(AddrVo searchVO) throws Exception;
    
}
