package com.gti.pms.acmFclt.web;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.gti.pms.cmm.service.CmmService;
import com.gti.pms.cmm.service.LinkVO;
import com.gti.pms.cmm.web.CodeController;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

/**
 * @Class Name : WttMetaHtController.java
 * @Description : WttMetaHt Controller class
 * @Modification Information
 *
 * @author DRCTS
 * @since 2018-07-01
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Controller
@RequestMapping("/pms/acmFclt/hydtMetr")
public class HydtMetrController extends CodeController{


	@Resource(name = "cmmService")
	private CmmService cmmService;

	

	
	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	
	
	
	/**
	 * 급수전계량기 WTL_RSRV_PS 목록을 조회한다. (pageing)
	 * @param searchVO - 조회할 정보가 담긴 WtlMetaPsDefaultVO
	 * @return 
	 * @exception Exception
	 */
	@RequestMapping(value="/hydtMetrList.do")
	public String hydtMetrList(
			HttpServletRequest request,
			HttpServletResponse response,
			@ModelAttribute("searchVO") LinkVO searchVO,
			@RequestParam(value="gisLayerNm", required=false) String gisLayerNm, Model model) throws Exception {
		
		/** EgovPropertyService.sample */
		searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
		searchVO.setPageSize(propertiesService.getInt("pageSize"));
		
		/** pageing */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());
		
		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());		
	
		if (gisLayerNm != null) {
			searchVO.setGeomYN("Y");
		}
		
		//조회
		List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectWtlMetaPsList", searchVO);
		model.addAttribute("resultList", lst);

		
		//페이징처리
		int totCnt = 0;
		List<Map<String, Object>> listCnt = (List<Map<String, Object>>) cmmService.selectList("selectWtlMetaPsListTotCnt", searchVO);
		try{
			totCnt = ((BigDecimal) listCnt.get(0).get("totCnt")).intValue();
		}catch(Exception e){
		};
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);
		
		
		//코드내려주기
		getAdarMaListModel(model); //행정동
		getMofCdeListModel(model); //형식
		getFtrcMaListModel(model); //지형지물코드
		
		model.addAttribute("gisLayerNm", gisLayerNm);
		return "pms/acmFclt/hydtMetrList"; 
	} 

	
	
	
	

	
	
	
	/**
	 * 급수전계량기 신규등록
	 * @param request
	 * @param response
	 * @param paramVO
	 * @param model
	 * @param type
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/hydtMetrCreate.do")
	public String hydtMetrCreate(
			HttpServletRequest request,
			HttpServletResponse response, 
			Model model,
			@RequestParam(value="type", required=false) String type) throws Exception {
		
	
		//관리번호 채번
		int ftrIdn = 0;
		Map<String, Object> map = (Map<String, Object>) cmmService.select("selectMaxMetaFtrIdn", new HashMap<String, Object>());
		if(map != null && !map.isEmpty()){
			ftrIdn = Integer.parseInt(map.get("ftrIdn").toString());
		}
		
		model.addAttribute("ftrIdnVal", ftrIdn);
		
		// GIS에서 대장등록하기 위한 구분변수
		model.addAttribute("type", type);
	
		//코드내려주기
		getAdarMaListModel(model); //행정동
		getMofCdeListModel(model); //형식
		getFtrcMaListModel(model); //지형지물코드
		getSbiCdeListModel(model); //업종
	
		return "pms/acmFclt/hydtMetrCreate"; 
	}
	
	
	
	/**
	 * 급수전계량기 상세
	 * @param request
	 * @param response
	 * @param ftrCde
	 * @param ftrIdn
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/hydtMetrView.do")
	public String hydtMetrView(
			HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam("ftrCde") String ftrCde, 
			@RequestParam("ftrIdn") String ftrIdn, 
			@RequestParam("ogrFid") String ogrFid, 
			Model model) throws Exception {

		
		/*상세정보*/
		Map<String, Object> param = new HashMap<String, Object> ();
		param.put("ftrCde", ftrCde);
		param.put("ftrIdn", ftrIdn);
		param.put("ogrFid", ogrFid);
		Map<String, Object> map = (Map<String, Object>) cmmService.select("selectWtlMetaPs", param);
		model.addAttribute("resultVO", map);   	
	
		/*계량기교체이력*/
		List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectWttMetaHtList", param);
		model.addAttribute("htResultList", lst);
		
		
		//코드내려주기
		getAdarMaListModel(model); //행정동
		getMofCdeListModel(model); //형식
		getFtrcMaListModel(model); //지형지물코드
		getSbiCdeListModel(model); //업종
		getGcwCdeListModel(model); //교체구분
		
		model.addAttribute("ftrCde", ftrCde);
		model.addAttribute("ftrIdn", ftrIdn);
		model.addAttribute("ogrFid", ogrFid);
		
		return "pms/acmFclt/hydtMetrView"; 
	}

	
	
	
	
    /**
     * 계량기 교체신규등록 팝업
     * @param request
     * @param response
     * @param paramVO
     * @param model
     * @return
     * @throws Exception
     */
	@RequestMapping("/hydtMetrChgCreatePopup.do")
	public String hydtMetrCreatePopup(
		HttpServletRequest request,
		HttpServletResponse response,
		@RequestParam("ftrCde") String ftrCde, 
		@RequestParam("ftrIdn") String ftrIdn, 
		Model model) throws Exception {
		
		//0.교체순번 채번
		int metaSeq = 0;
		Map<String, Object> map = (Map<String, Object>) cmmService.select("selectWttMaxMetaHt", new HashMap<String, Object>());
		model.addAttribute("metaSeqVal", map.get("metaSeq"));
		
		//코드내려주기
		getGcwCdeListModel(model);
		getMofCdeListModel(model);

		model.addAttribute("ftrCde", ftrCde);
		model.addAttribute("ftrIdn", ftrIdn);
		return "pms/popup/hydtMetrChgCreatePopup"; 
	}
	
	
	
	/**
	 * 계량기 교체 상세 팝업
	 * @param request
	 * @param response
	 * @param ftrCde
	 * @param ftrIdn
	 * @param metaSeq
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/hydtMetrChgViewPopup.do")
	public String hydtMetrViewPopup(
			HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam("ftrCde") String ftrCde, 
			@RequestParam("ftrIdn") String ftrIdn, 
			@RequestParam("metaSeq") String metaSeq, 
			Model model) throws Exception {
		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("ftrCde", ftrCde);
		param.put("ftrIdn", ftrIdn);
		param.put("metaSeq", metaSeq);
		Map<String, Object> map = (Map<String, Object>) cmmService.select("selectWttMetaHt", param);
		model.addAttribute("resultVO", map);

		
		//코드내려주기
		getGcwCdeListModel(model);
		getMofCdeListModel(model);
		
		model.addAttribute("ftrCde", ftrCde);
		model.addAttribute("ftrIdn", ftrIdn);
		model.addAttribute("metaSeq", metaSeq);
	 
		return "pms/popup/hydtMetrChgViewPopup"; 
	}	   

	
		
}
