package com.gti.pms.acmFclt.web;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.gti.pms.cmm.service.CmmService;
import com.gti.pms.cmm.service.LinkVO;
import com.gti.pms.cmm.web.CodeController;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

/**
 * @Class Name : WtlRsrvPsController.java
 * @Description : WtlRsrvPs Controller class
 * @Modification Information
 *
 * @author DRCTS
 * @since 2018-07-01
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Controller
@RequestMapping("/pms/acmFclt/wtrTrk")
public class WtrTrkController extends CodeController{

	@Resource(name = "cmmService")
	private CmmService cmmService;
	

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;
	
	
	
	
	
	
	
	/**
	 * 저수조 WTL_RSRV_PS 목록을 조회한다. (pageing)
	 * @param request
	 * @param response
	 * @param searchVO
	 * @param gisLayerNm
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/wtrTrkList.do")
	public String wtrTrkList(
		HttpServletRequest request,
		HttpServletResponse response,		
		@ModelAttribute("searchVO") LinkVO searchVO, 
		@RequestParam(value="gisLayerNm", required=false) String gisLayerNm, Model model) throws Exception {
	
		/** EgovPropertyService.sample */
		searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
		searchVO.setPageSize(propertiesService.getInt("pageSize"));
		
		/** pageing */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());
		
		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());		
		if (gisLayerNm != null) {
			searchVO.setGeomYN("Y");
		}
		
		//조회
		List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectWtlRsrvPsList", searchVO);
		model.addAttribute("resultList", lst);

		
		//페이징처리
		List<Map<String, Object>> listCnt = (List<Map<String, Object>>) cmmService.selectList("selectWtlRsrvPsListTotCnt", searchVO);
		int totCnt = 0;
		try{
			totCnt = ((BigDecimal) listCnt.get(0).get("totCnt")).intValue();
		}catch(Exception e){
		};
		paginationInfo.setTotalRecordCount(totCnt);
		
				

		
		//코드내려주기
		getMngCdeListModel(model); /*관리기관*/
		getAdarMaListModel(model); /*행정동*/
		getFtrcMaListModel(model); //지형지물코드

		model.addAttribute("paginationInfo", paginationInfo);
		model.addAttribute("gisLayerNm", gisLayerNm); /* 편집 호출 유무 */
		
		return "pms/acmFclt/wtrTrkList"; 
	} 

	
	
	/**
	 * 저수조 신규등록
	 * @param request
	 * @param response
	 * @param model
	 * @param type
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/wtrTrkCreate.do")
	public String wtrTrkCreate(
			HttpServletRequest request,
			HttpServletResponse response, 
			Model model,
			@RequestParam(value="type", required=false) String type) throws Exception {
	 	
		//관리번호 채번
		int ftrIdn = 0;
		Map<String, Object> map = (Map<String, Object>) cmmService.select("selectMaxRsrvFtrIdn", new HashMap<String, Object>());
		if(map != null && !map.isEmpty()){
			ftrIdn = Integer.parseInt(map.get("ftrIdn").toString());
		}
		
		model.addAttribute("ftrIdnVal", ftrIdn);
		
		// GIS에서 대장등록하기 위한 구분변수
		model.addAttribute("type", type);
	
		//코드내려주기
		getAdarMaListModel(model); //행정동
		getFtrcMaListModel(model); //지형지물코드
		getBlsCdeListModel(model); //건물유형
		getMngCdeListModel(model); //관리기관
	
		return "pms/acmFclt/wtrTrkCreate"; 
	}

	
	

	/**
	 * 저수조 상세
	 * @param request
	 * @param response
	 * @param ftrCde
	 * @param ftrIdn
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/wtrTrkView.do")
	public String wtrTrkView(
			HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam("ftrCde") String ftrCde, 
			@RequestParam("ftrIdn") String ftrIdn, 
			Model model) throws Exception {
		try {

			// 1.상세정보 조회
			Map<String, Object> param = new HashMap<String, Object> ();
			param.put("ftrCde", ftrCde);
			param.put("ftrIdn", ftrIdn);
			Map<String, Object> map = (Map<String, Object>) cmmService.select("selectWtlRsrvPs", param);
			model.addAttribute("resultVO", map);
			
			
			// 2.청소이력
			List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectWttRsrvHtList", param);
			model.addAttribute("htResultList", lst);
			
			//코드내려주기
			getAdarMaListModel(model); //행정동
			getFtrcMaListModel(model); //지형지물코드
			getBlsCdeListModel(model); //건물유형
			getMngCdeListModel(model); //관리기관
				
		}catch (Exception e) {
			// TODO Auto-generated catch block
			model.addAttribute("res", "FAIL");
			model.addAttribute("err", "ERR005");
			model.addAttribute("msg", "조회할 내역이 없습니다.");
		} 
		
		return "pms/acmFclt/wtrTrkView"; 
	}
	


	
	
	

	
		
		
		
		
		/**
		 * 청소이력 신규등록
		 * @param request
		 * @param response
		 * @param ftrCde
		 * @param ftrIdn
		 * @param model
		 * @return
		 * @throws Exception
		 */
		@RequestMapping("/cleanHisCreatePopup.do")
		public String wtrTrk_attFacCreatePopup(
			HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam("ftrCde") String ftrCde, 
			@RequestParam("ftrIdn") String ftrIdn, 
			Model model) throws Exception {
			
			//0.청소순번 채번
			int seq = 0;
			Map<String, Object> map = (Map<String, Object>) cmmService.select("selectWttMaxRsrvHt", new HashMap<String, Object>());
			model.addAttribute("seqVal", map.get("seq"));
			

			model.addAttribute("ftrCde", ftrCde);
			model.addAttribute("ftrIdn", ftrIdn);
			return "pms/popup/wtrTrk_cleanHisCreatePopup"; 
		}
		
				
		
		
		
		/**
		 * 계량기 교체 상세 팝업
		 * @param request
		 * @param response
		 * @param ftrCde
		 * @param ftrIdn
		 * @param seq
		 * @param model
		 * @return
		 * @throws Exception
		 */
		@RequestMapping("/cleanHisViewPopup.do")
		public String hydtMetrViewPopup(
				HttpServletRequest request,
				HttpServletResponse response,
				@RequestParam("ftrCde") String ftrCde, 
				@RequestParam("ftrIdn") String ftrIdn, 
				@RequestParam("seq") String seq, 
				Model model) throws Exception {
			
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("ftrCde", ftrCde);
			param.put("ftrIdn", ftrIdn);
			param.put("seq", seq);
			Map<String, Object> map = (Map<String, Object>) cmmService.select("selectWttRsrvHt", param);
			model.addAttribute("resultVO", map);

			
			
			model.addAttribute("ftrCde", ftrCde);
			model.addAttribute("ftrIdn", ftrIdn);
			model.addAttribute("seq", seq);
		 
			return "pms/popup/wtrTrk_cleanHisViewPopup";
		}	   

				
		



		
		

		
}
