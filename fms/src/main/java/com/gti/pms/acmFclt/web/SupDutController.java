package com.gti.pms.acmFclt.web;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.gti.pms.acmFclt.service.WtlSplyLsDefaultVO;
import com.gti.pms.cmm.service.CmmService;
import com.gti.pms.cmm.web.CodeController;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

/**
 * @Class Name : WtlSplyLsController.java
 * @Description : WtlSplyLs Controller class
 * @Modification Information
 *
 * @author DRCTS
 * @since 2018-07-01
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Controller
@RequestMapping("/pms/acmFclt/supDut")
public class SupDutController extends CodeController {


	@Resource(name = "cmmService")
	private CmmService cmmService;

	
	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;
	
	
	
	
	/**
	 * 급수관로 WTL_SPLY_LS 목록을 조회한다. (pageing)
	 * @param searchVO - 조회할 정보가 담긴 WtlSplyLsDefaultVO
	 * @return jspRoot + "/WtlSplyLsList"
	 * @exception Exception
	 */
	@RequestMapping(value="/supDutList.do")
	public String supDutList(
			HttpServletRequest request,
			HttpServletResponse response, 
			@ModelAttribute("searchVO") WtlSplyLsDefaultVO searchVO, 
			@RequestParam(value="gisLayerNm", required=false) String gisLayerNm,
			Model model)throws Exception {
		
		/** EgovPropertyService.sample */
		searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
		searchVO.setPageSize(propertiesService.getInt("pageSize"));
		
		/** pageing */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());
		
		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		
		/* 편집 호출 유무 */
		if (gisLayerNm != null) {
			searchVO.setGeomYN("Y");
		}
		

		//조회
		List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectWtlSplyLsList", searchVO);
		model.addAttribute("resultList", lst);

		
		//페이징처리
		List<Map<String, Object>> listCnt = (List<Map<String, Object>>) cmmService.selectList("selectWtlSplyLsListTotCnt", searchVO);
		int totCnt = 0;
		try{
			totCnt = ((BigDecimal) listCnt.get(0).get("totCnt")).intValue();
		}catch(Exception e){
		};
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);
		
		
		//코드내려주기
		/*관리기관*/
		getMngCdeListModel(model);
		/*행정동*/
		getAdarMaListModel(model);
		/*관용도*/
		getSaaCdeListModel(model);
		/*접합종류*/
		getJhtCdeListModel(model);	
		

		
		return "pms/acmFclt/supDutList"; 
	}

	
	
	
	/**
	 * 급수관로 신규등록
	 * @param request
	 * @param response
	 * @param paramVO
	 * @param model
	 * @param type
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/supDutCreate.do")
	public String supDutCreate(
			HttpServletRequest request,
			HttpServletResponse response, 
			Model model,
			@RequestParam(value="type", required=false) String type) throws Exception {
		
		
		//관리번호 채번
		int ftrIdn = 0;
		Map<String, Object> map = (Map<String, Object>) cmmService.select("selectMaxSplyFtrIdn", new HashMap<String, Object>());
		if(map != null && !map.isEmpty()){
			ftrIdn = Integer.parseInt(map.get("ftrIdn").toString());
		}
		
		model.addAttribute("ftrIdnVal", ftrIdn);

		// GIS에서 대장등록하기 위한 구분변수
		model.addAttribute("type", type);
		
		//코드내려주기
		/*관리기관*/
		getMngCdeListModel(model);
		/*행정동*/
		getAdarMaListModel(model);
		/*관용도*/
		getSaaCdeListModel(model);
		/*접합종류*/
		getJhtCdeListModel(model);	

		return "pms/acmFclt/supDutCreate"; 
	} 

	
	
	/**
	 * 급수관로 상세
	 * @param request
	 * @param response
	 * @param paramVO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/supDutView.do")
	public String supDutView(
			HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam("ftrCde") String ftrCde, 
			@RequestParam("ftrIdn") String ftrIdn, 
			Model model) throws Exception {

		
		try {
			
			Map<String, Object> param = new HashMap<String, Object> ();
			param.put("ftrCde", ftrCde);
			param.put("ftrIdn", ftrIdn);
			Map<String, Object> map = (Map<String, Object>) cmmService.select("selectWtlSplyLs", param);
			model.addAttribute("resultVO", map);	
			
		
			/*급수전계량기 목록  */
			List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectSubList", param);
			model.addAttribute("wtlMetaList", lst);
			
			
			
			/*관리기관*/
			getMngCdeListModel(model);
			/*행정동*/
			getAdarMaListModel(model);
			/*관용도*/
			getSaaCdeListModel(model);
			/*접합종류*/
			getJhtCdeListModel(model);	

			
			model.addAttribute("ftrCde", ftrCde);
			model.addAttribute("ftrIdn", ftrIdn);

		}catch (Exception e) {
			model.addAttribute("res", "FAIL");
			model.addAttribute("err", "ERR005");
			model.addAttribute("msg", "조회가능한 내역이 없습니다.");
		}

		return "pms/acmFclt/supDutView"; 
	} 
	

}
