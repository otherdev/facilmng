package com.gti.pms.fcltMnt.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springmodules.validation.commons.DefaultBeanValidator;

import com.gti.pms.cmm.service.CmmService;
import com.gti.pms.cmm.web.CodeController;
import com.gti.pms.fcltMnt.service.EduDataModVO;
import com.gti.pms.fcltMnt.service.EduDataService;
import com.gti.pms.fcltMnt.service.EduVO;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.SessionUtil;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.fdl.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

/**
 * 교육자료관리 
 * @author 공통서비스 개발팀 
 * @see
 *
 */
@Controller
public class EduDataController extends CodeController {

	
	/** eduDataService */
	@Resource(name = "eduDataService")
	private EduDataService eduDataService;

	
	/** cmmService */
	@Resource(name = "cmmService")
	private CmmService cmmService;

	/** EgovMessageSource */
	@Resource(name = "egovMessageSource")
	EgovMessageSource egovMessageSource;

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/** DefaultBeanValidator beanValidator */
	@Autowired
	private DefaultBeanValidator beanValidator;


	/**
	 * FAQ 목록을 조회한다. (pageing)
	 * @param eduVO 검색조건정보
	 * @throws Exception
	 */
	@RequestMapping(value = "/pms/fcltMnt/eduFaqList.do")
	public String eduFaqList(@ModelAttribute("eduVO") EduVO eduVO, Model model ,HttpSession session
			) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

		String isEduMngYn = SessionUtil.getIsAdmYn(); //관리자 여부
		model.addAttribute("userId", SessionUtil.getId());
		model.addAttribute("eduMngYn", isEduMngYn);
		
		eduVO.setSeq(-1);
		eduVO.setMode("VIEW");
    	
		/** EgovPropertyService */
    	eduVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	eduVO.setPageSize(propertiesService.getInt("pageSize"));

		/** pageing */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(eduVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(eduVO.getPageUnit());
		paginationInfo.setPageSize(eduVO.getPageSize());

		eduVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		eduVO.setLastIndex(paginationInfo.getLastRecordIndex());
		eduVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		model.addAttribute("searchCondition", eduVO.getSearchCondition());
		model.addAttribute("searchKeyword", eduVO.getSearchKeyword());
		model.addAttribute("resultList", eduDataService.selectEduFaqList(eduVO));
 
		int totCnt = eduDataService.selectEduFaqListTotCnt(eduVO);
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);
		model.addAttribute("eduVO", eduVO);


        /*지형지물*/
		getFtrcMaListModel(model);
		
		return "pms/fcltMnt/eduFaqList";
	}
	

	/**
	 * 교육자료정보를 상세조회한다.
	 * @param eduVO 검색조건
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping("/pms/fcltMnt/eduFaqInfo.do")
	public String eduFaqInfo(
				@RequestParam("mode") String mode,  
				@ModelAttribute("eduVO") EduVO eduVO, ModelMap model) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

		String isEduMngYn = SessionUtil.getIsAdmYn(); //관리자 여부
		model.addAttribute("userId", SessionUtil.getId());
		model.addAttribute("eduMngYn", isEduMngYn);
		
    	model.addAttribute("mode", mode); 

		// 조회수 증가 처리 
    	EduDataModVO eduDataModVO = new EduDataModVO();
    	eduDataModVO.setSeq(eduVO.getSeq());
		eduDataService.updateEduFaqReadCnt(eduDataModVO); 
		// 조회 
		eduDataModVO = eduDataService.selectEduFaqInfo(eduVO); 
		
		eduDataModVO.setPageIndex(eduVO.getPageIndex());
		eduDataModVO.setPageSize(eduVO.getPageSize());
		eduDataModVO.setPageUnit(eduVO.getPageUnit());
		eduDataModVO.setSearchCondition(eduVO.getSearchCondition());
		eduDataModVO.setSearchKeyword(eduVO.getSearchKeyword());
		eduDataModVO.setMode(mode);
		model.addAttribute("eduDataModVO", eduDataModVO);

		return  "pms/fcltMnt/eduFaqInfo";
	}


	/**
	 * 수정을 위해 교육자료정보를 상세조회한다.
	 * @param eduVO 검색조건
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping("/pms/fcltMnt/eduFaqUp.do")
	public String eduFaqUp(
				@RequestParam("mode") String mode,  
				@ModelAttribute("eduVO") EduVO eduVO, Model model) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}
		String isEduMngYn = SessionUtil.getIsAdmYn(); //관리자 여부
		model.addAttribute("userId", SessionUtil.getId());
		model.addAttribute("eduMngYn", isEduMngYn);
		
		
		// 상세조회
		//eduVo.setSeq(eduModVO.getSeq());
		//eduVo.setFaqCatCd(eduModVO.getFaqCatCd());

    	EduDataModVO eduDataModVO = eduDataService.selectEduFaqInfo(eduVO); 
    	if( eduDataModVO == null ){
    		eduDataModVO = new EduDataModVO();
    		eduDataModVO.setSeq(eduVO.getSeq());
    	}
		
		eduDataModVO.setPageIndex(eduVO.getPageIndex());
		eduDataModVO.setPageSize(eduVO.getPageSize());
		eduDataModVO.setPageUnit(eduVO.getPageUnit());
		eduDataModVO.setSearchCondition(eduVO.getSearchCondition());
		eduDataModVO.setSearchKeyword(eduVO.getSearchKeyword());
		eduDataModVO.setMode(mode);


    	model.addAttribute("mode", mode); 
		model.addAttribute("eduDataModVO", eduDataModVO);
		

		/*지형지물*/
		getFtrcMaListModel(model);
		

		return  "pms/fcltMnt/eduFaqUp";
	}	

	/**
	 * 교육자료정보를 저장후 상세조회한다.
	 * @param eduVO 검색조건
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping("/pms/fcltMnt/eduFaqUpdate.do")
	public String eduFaqUpdate(
				@RequestParam("mode") String mode,  
				@ModelAttribute("eduDataModVO") EduDataModVO eduDataModVO, ModelMap model) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

		String isEduMngYn = SessionUtil.getIsAdmYn(); //관리자 여부
		model.addAttribute("userId", SessionUtil.getId());
		model.addAttribute("eduMngYn", isEduMngYn);
		
        if(mode.equals("ADD") || mode.equals("UPDATE") ){
    		eduDataModVO.setRegId(SessionUtil.getId());
	    	// 저장 처리 
	    	eduDataService.updateEduFaq(eduDataModVO); 
        }
    	    	
		EduVO eduVo = new EduVO();
		eduVo.setSeq(eduDataModVO.getSeq());
		eduVo.setPageIndex(eduDataModVO.getPageIndex());
		eduVo.setPageSize(eduDataModVO.getPageSize());
		eduVo.setPageUnit(eduDataModVO.getPageUnit());
		eduVo.setSearchCondition(eduDataModVO.getSearchCondition());
		eduVo.setSearchKeyword(eduDataModVO.getSearchKeyword());
		
		// eduModVO = eduService.selectEduDataList(eduVo); 

		/** pageing */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(eduVo.getPageIndex());
		paginationInfo.setRecordCountPerPage(eduVo.getPageUnit());
		paginationInfo.setPageSize(eduVo.getPageSize());

		model.addAttribute("resultList", eduDataService.selectEduFaqList(eduVo));

		int totCnt = eduDataService.selectEduFaqListTotCnt(eduVo);
		eduVo.setSeq(-1); // 목록으로 초기화 시키기 
		
		model.addAttribute("searchCondition", eduDataModVO.getSearchCondition());
		model.addAttribute("searchKeyword", eduDataModVO.getSearchKeyword());
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);
		model.addAttribute("eduVO", eduVo);

		//Exception 없이 진행시 수정성공메시지
        if(mode.equals("ADD")){ //  || mode.equals("UPDATE") ){
        	model.addAttribute("resultMsg", "success.common.insert");
        } else {
        	model.addAttribute("resultMsg", "success.common.update");
        }
		return  "pms/fcltMnt/eduFaqList";
	}	

	/**
	 * 교육자료정보를 삭제 후 상세조회한다.
	 * @param eduVO 검색조건
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping("/pms/fcltMnt/eduFaqDelete.do")
	public String eduFaqDelete(
				@RequestParam("mode") String mode,  
				@ModelAttribute("eduDataModVO") EduDataModVO eduDataModVO, ModelMap model,HttpSession session) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}
    	
    	// 삭제 처리 
    	eduDataService.deleteEduFaq(eduDataModVO); 

		String isEduMngYn = SessionUtil.getIsAdmYn(); //관리자 여부
		model.addAttribute("userId", SessionUtil.getId());
		model.addAttribute("eduMngYn", isEduMngYn);
		
    	Map<String, Object> map = new HashMap<String, Object>();
    	
		EduVO eduVO = new EduVO();
		eduVO.setSeq(eduDataModVO.getSeq());
		eduVO.setPageIndex(eduDataModVO.getPageIndex());
		eduVO.setPageSize(eduDataModVO.getPageSize());
		eduVO.setPageUnit(eduDataModVO.getPageUnit());
		eduVO.setSearchCondition(eduDataModVO.getSearchCondition());
		eduVO.setSearchKeyword(eduDataModVO.getSearchKeyword());
		
		// eduModVO = eduService.selectEduDataList(eduVo); 

		/** pageing */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(eduVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(eduVO.getPageUnit());
		paginationInfo.setPageSize(eduVO.getPageSize());

		model.addAttribute("resultList", eduDataService.selectEduFaqList(eduVO));

		int totCnt = eduDataService.selectEduFaqListTotCnt(eduVO);
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("searchCondition", eduDataModVO.getSearchCondition());
		model.addAttribute("searchKeyword", eduDataModVO.getSearchKeyword());
		model.addAttribute("paginationInfo", paginationInfo);

		eduVO.setSeq(-1); // 목록으로 초기화 시키기 
    	model.addAttribute("mode", "VIEW"); 
    	model.addAttribute("pageIndex", eduVO.getPageIndex()); 
		// model.addAttribute("eduModVO", eduModVO);
    	
    	model.addAttribute("resultMsg", "success.common.delete");

		return  "pms/fcltMnt/eduFaqList";
	}
	
	
	
	
}
