package com.gti.pms.fcltMnt.service;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 교육관련 VO클래스로서 조회 키값 및 화면 처리시 기타조건성 항을 구성한다.
 * @author 공통서비스 개발팀 조재영
 * @since 2009.04.10
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2009.04.10  조재영          최초 생성
 *   2011.08.31  JJY            경량환경 템플릿 커스터마이징버전 생성 
 *
 * </pre>
 */
public class EduVO implements Serializable {
	
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	
	/** 검색조건 */
    private String searchCondition = "";
    private String searchCondition1 = "";
    
    /** 검색Keyword */
    private String searchKeyword = "";
    
    
    /** 사업관리 관련키     */
	private long 	bizSeq	 = 0L;
	private long 	schedSeq	 = 0L;
	private long 	workSeq	 = 0L;

	private String 	id	 = "";
    
    /** 현재페이지 */
    private int pageIndex = 1;
    
    /** 페이지갯수 */
    private int pageUnit = 10;
    
    /** 페이지사이즈 */
    private int pageSize = 10;

    /** firstIndex */
    private int firstIndex = 1;

    /** lastIndex */
    private int lastIndex = 1;

    /** recordCountPerPage */
    private int recordCountPerPage = 10;

	private long 	seq	 = 0L;

	
	private String 	mode	 = "";
	
	private long 	eduSeq	 = 0L;
	private long 	taskSeq	 = 0L;
	private long 	submitSeq	 = 0L;
	
	private String 	regId	 = "";
	private String 	mnuCd	 = "";

	private String 	faqCatCde	 = "";
	private String 	faqCuzCde	 = "";
	private String 	ttl	 = "";

	public String getMnuCd() {
		return mnuCd;
	}
	public void setMnuCd(String mnuCd) {
		this.mnuCd = mnuCd;
	}
	public String getRegId() {
		return regId;
	}
	public void setRegId(String regId) {
		this.regId = regId;
	}
	
	public long getSeq() {
		return seq;
	}
	public void setSeq(long seq) {
		this.seq = seq;
	}
	



	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}

	public long getEduSeq() {
		return eduSeq;
	}
	public void setEduSeq(long eduSeq) {
		this.eduSeq = eduSeq;
	}

	public long getTaskSeq() {
		return taskSeq;
	}
	public void setTaskSeq(long taskSeq) {
		this.taskSeq = taskSeq;
	}

	public long getSubmitSeq() {
		return submitSeq;
	}
	public void setSubmitSeq(long submitSeq) {
		this.submitSeq = submitSeq;
	}

	/**
	 * searchCondition attribute 값을  리턴한다.
	 * @return String
	 */
	public String getSearchCondition() {
		return searchCondition;
	}

	/**
	 * searchCondition attribute 값을 설정한다.
	 * @param searchCondition String
	 */
	public void setSearchCondition(String searchCondition) {
		this.searchCondition = searchCondition;
	}

	/**
	 * searchKeyword attribute 값을  리턴한다.
	 * @return String
	 */
	public String getSearchKeyword() {
		return searchKeyword;
	}

	/**
	 * searchKeyword attribute 값을 설정한다.
	 * @param searchKeyword String
	 */
	public void setSearchKeyword(String searchKeyword) {
		this.searchKeyword = searchKeyword;
	}

	

	/**
	 * pageIndex attribute 값을  리턴한다.
	 * @return int
	 */
	public int getPageIndex() {
		return pageIndex;
	}

	/**
	 * pageIndex attribute 값을 설정한다.
	 * @param pageIndex int
	 */
	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}

	/**
	 * pageUnit attribute 값을  리턴한다.
	 * @return int
	 */
	public int getPageUnit() {
		return pageUnit;
	}

	/**
	 * pageUnit attribute 값을 설정한다.
	 * @param pageUnit int
	 */
	public void setPageUnit(int pageUnit) {
		this.pageUnit = pageUnit;
	}

	/**
	 * pageSize attribute 값을  리턴한다.
	 * @return int
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * pageSize attribute 값을 설정한다.
	 * @param pageSize int
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * firstIndex attribute 값을  리턴한다.
	 * @return int
	 */
	public int getFirstIndex() {
		return firstIndex;
	}

	/**
	 * firstIndex attribute 값을 설정한다.
	 * @param firstIndex int
	 */
	public void setFirstIndex(int firstIndex) {
		this.firstIndex = firstIndex;
	}

	/**
	 * lastIndex attribute 값을  리턴한다.
	 * @return int
	 */
	public int getLastIndex() {
		return lastIndex;
	}

	/**
	 * lastIndex attribute 값을 설정한다.
	 * @param lastIndex int
	 */
	public void setLastIndex(int lastIndex) {
		this.lastIndex = lastIndex;
	}

	/**
	 * recordCountPerPage attribute 값을  리턴한다.
	 * @return int
	 */
	public int getRecordCountPerPage() {
		return recordCountPerPage;
	}

	/**
	 * recordCountPerPage attribute 값을 설정한다.
	 * @param recordCountPerPage int
	 */
	public void setRecordCountPerPage(int recordCountPerPage) {
		this.recordCountPerPage = recordCountPerPage;
	}
    
	/**
     * toString 메소드를 대치한다.
     */
    public String toString() {
    	return ToStringBuilder.reflectionToString(this);
    }


	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	

	public long getBizSeq() {
		return bizSeq;
	}

	public void setBizSeq(long bizSeq) {
		this.bizSeq = bizSeq;
	}

	public long getSchedSeq() {
		return schedSeq;
	}

	public void setSchedSeq(long schedSeq) {
		this.schedSeq = schedSeq;
	}

	public long getWorkSeq() {
		return workSeq;
	}

	public void setWorkSeq(long workSeq) {
		this.workSeq = workSeq;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	public String getFaqCatCde() {
		return faqCatCde;
	}
	public void setFaqCatCde(String faqCatCde) {
		this.faqCatCde = faqCatCde;
	}
	public String getFaqCuzCde() {
		return faqCuzCde;
	}
	public void setFaqCuzCde(String faqCuzCde) {
		this.faqCuzCde = faqCuzCde;
	}
	public String getTtl() {
		return ttl;
	}
	public void setTtl(String ttl) {
		this.ttl = ttl;
	}
	public String getSearchCondition1() {
		return searchCondition1;
	}
	public void setSearchCondition1(String searchCondition1) {
		this.searchCondition1 = searchCondition1;
	}
    
    
	

}
