package com.gti.pms.fcltMnt.web;

import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.gti.pms.cmm.service.CmmService;
import com.gti.pms.cmm.web.CodeController;
import com.gti.pms.fcltMnt.service.FcltMntVO;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
/**
 * @Class Name : WtlServPsController.java
 * @Description : WtlServPs Controller class
 * @Modification Information
 *
 * @author DRCTS
 * @since 2018-07-01
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Controller
@RequestMapping("/pms/fcltMnt")
public class oldMngController extends CodeController{
		
	private static final Logger logger = LoggerFactory.getLogger(oldMngController.class);
	
	@Resource(name = "cmmService")
    private CmmService cmmService;
	
	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/**
	 * 시설물 가동시간 목록 조회 (paging)
	 * @param searchVO - 조회할 정보가 담긴 FcltMntVO
	 * @return "pms/fcltMnt/oldMngList.do"
	 * @exception Exception
	 */
	@RequestMapping("/oldMngList.do")
	public String oldMngList(HttpServletRequest request, 
			@ModelAttribute("searchVO") FcltMntVO searchVO, Model model) throws Exception {
		
		searchVO.setPageUnit(propertiesService.getInt("pageUnit")); 
		searchVO.setPageSize(propertiesService.getInt("pageSize"));
		
		//paging
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());
		
		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		
		//조회
		List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectOldMngList", searchVO);
		model.addAttribute("resultList", list);
		
		//페이징처리
		List<Map<String, Object>> listCnt = (List<Map<String, Object>>) cmmService.selectList("selectOldMngTotCnt", searchVO);
		int totCnt = 0;
		try{
			totCnt = ((BigDecimal) listCnt.get(0).get("totCnt")).intValue();
		}catch(Exception e){
			logger.debug("..." + e.getMessage());
		};
		
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);
				
		return "pms/fcltMnt/oldMngList";
	}
	
	/**
	 * 시설물 가동시간 목록 조회 엑셀 다운로드
	 * @param searchVO - 조회할 정보가 담긴 FcltMntVO
	 * @return "pms/fcltMnt/getExelOldMngList.do"
	 * @exception Exception
	 */
	@RequestMapping(value="/oldMngList/excel/download.do")
   	public void getExelOldMngList(HttpServletRequest request, @ModelAttribute FcltMntVO vo, HttpServletResponse response , Model model) throws Exception {
   		HSSFWorkbook objWorkBook = new HSSFWorkbook();
   		HSSFSheet objSheet;
   		HSSFRow objRow;
   		HSSFCell objCell;
   		OutputStream fileOut = null;
   		
   		Map<String, Object> map = new HashMap<String, Object>();
   		BigDecimal dData = new BigDecimal(0);

   		//제목폰트
   		HSSFFont font = objWorkBook.createFont();
   		font.setFontHeightInPoints((short)20);
   		font.setBoldweight((short)font.BOLDWEIGHT_BOLD);
   		font.setFontName("맑은고딕");

   		HSSFCellStyle titleStyle = objWorkBook.createCellStyle();
   		titleStyle.setFont(font);
   		titleStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
   		titleStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

   		//테이블 헤더 폰트
   		HSSFFont headerFont = objWorkBook.createFont();
   		headerFont.setFontHeightInPoints((short)10);
   		headerFont.setBoldweight((short)font.BOLDWEIGHT_BOLD);
   		headerFont.setFontName("맑은고딕");

   		HSSFCellStyle tableHeaderStyle = objWorkBook.createCellStyle();
   		tableHeaderStyle.setFont(headerFont);
   		tableHeaderStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
   		tableHeaderStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
   		tableHeaderStyle.setBorderTop((short)1);
   		tableHeaderStyle.setBorderBottom((short)1);
   		
   		//테이블 내용 폰트
   		HSSFFont contentFont = objWorkBook.createFont();
   		contentFont.setFontHeightInPoints((short)9);
   		contentFont.setBoldweight((short)font.BOLDWEIGHT_NORMAL);
   		contentFont.setFontName("맑은고딕");

   		HSSFCellStyle contentStyle = objWorkBook.createCellStyle();
   		contentStyle.setFont(contentFont);
   		contentStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
   		contentStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
   		
   		objSheet = objWorkBook.createSheet("시설물 가동시간 목록");

   		Date today = new Date();
   		SimpleDateFormat format1 = new SimpleDateFormat("yyyyMMddHHmmssSSS");
   		SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");

   		 // 제목 행
   		objRow = objSheet.createRow(0);
   		objRow.setHeight ((short) 0x250);
   		objCell = objRow.createCell(0);
   		objCell.setCellValue("시설물 가동시간 목록");
   		objCell.setCellStyle(titleStyle);
   		objSheet.addMergedRegion(new CellRangeAddress(0,0,0,8));
    
   		// 시간 행
   		objRow = objSheet.createRow(2);
   		objRow.setHeight ((short) 0x150);
   		objCell = objRow.createCell(0);
   		objCell.setCellValue(format2.format(today));		
   		objCell.setCellStyle(tableHeaderStyle);

   		// 헤더 행
   		objRow = objSheet.createRow(3);
   		objRow.setHeight ((short) 0x150);
   		
   		objCell = objRow.createCell(0);
   		objCell.setCellValue("순번");
   		objCell.setCellStyle(tableHeaderStyle);

   		objCell = objRow.createCell(1);
   		objCell.setCellValue("일련번호");
   		objCell.setCellStyle(tableHeaderStyle);

   		objCell = objRow.createCell(2);
   		objCell.setCellValue("지형지물명");
   		objCell.setCellStyle(tableHeaderStyle);

   		objCell = objRow.createCell(3);
   		objCell.setCellValue("관리번호");
   		objCell.setCellStyle(tableHeaderStyle);
   		
   		objCell = objRow.createCell(4);
   		objCell.setCellValue("주요도");
   		objCell.setCellStyle(tableHeaderStyle);
   		
   		objCell = objRow.createCell(5);
   		objCell.setCellValue("내구연한");
   		objCell.setCellStyle(tableHeaderStyle);
   		
   		objCell = objRow.createCell(6);
   		objCell.setCellValue("설치년도");
   		objCell.setCellStyle(tableHeaderStyle);
   		
   		objCell = objRow.createCell(7);
   		objCell.setCellValue("노호도");
   		objCell.setCellStyle(tableHeaderStyle);
   		
   		objCell = objRow.createCell(8);
   		objCell.setCellValue("노후등급");
   		objCell.setCellStyle(tableHeaderStyle);
   		
   		objCell = objRow.createCell(9);
   		objCell.setCellValue("점검주기");
   		objCell.setCellStyle(tableHeaderStyle);
   		
   		objCell = objRow.createCell(10);
   		objCell.setCellValue("최근점검일자");
   		objCell.setCellStyle(tableHeaderStyle);
   		
   		List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectOldMngExcelList", vo);
   		
   		int listSize = 0;
		if( list.size()>10000) {
			listSize = 10000;
		} else {
			listSize = list.size();
		}
				
		int i = 0;				
		for( i=0; i<listSize; i++ ) {
			map = list.get(i);
			
			objRow = objSheet.createRow(i+4);
   			objRow.setHeight ((short) 0x150);
   			
   			//1.순번
   			objCell = objRow.createCell(0);
   			objCell.setCellValue(i + 1);		
   			objCell.setCellStyle(contentStyle);
   			
   			//2.일련번호   			   			
   			dData = (BigDecimal) map.get("g2Id");
   			objCell = objRow.createCell(1);
   			if(dData == null){
   				objCell.setCellValue("");	
   			}else{
   				objCell.setCellValue(dData.intValue());		
   			}		
   			objCell.setCellStyle(contentStyle);
   			
   			//3.지형지물명
   			objCell = objRow.createCell(2);
   			objCell.setCellValue((String) map.get("ftrNam"));		
   			objCell.setCellStyle(contentStyle);
   			
   			//4.관리번호
   			dData = (BigDecimal) map.get("ftrIdn");
   			objCell = objRow.createCell(3);
   			objCell.setCellValue(dData.intValue()); 
   			objCell.setCellStyle(contentStyle);
   			
   			//5.주요도   	 			
   			objCell = objRow.createCell(4);
   			objCell.setCellValue((String) map.get("impNam"));		
   			objCell.setCellStyle(contentStyle);
   			
   		    //6.내구연한
   			dData = (BigDecimal) map.get("lmtYear");
   			objCell = objRow.createCell(5);
   			if(dData == null){
   				objCell.setCellValue("");	
   			}else{
   				objCell.setCellValue(dData.intValue());		
   			}		
   			objCell.setCellStyle(contentStyle);
   			
   		    //7.설치년도
   			objCell = objRow.createCell(6);
   			objCell.setCellValue((String) map.get("creYy"));	
   			objCell.setCellStyle(contentStyle);
   			
   		    //8.노후도
   			dData = (BigDecimal) map.get("oldRate");
   			objCell = objRow.createCell(7);
   			if(dData == null){
   				objCell.setCellValue("");	
   			}else{
   				objCell.setCellValue(dData.intValue());		
   			}		
   			objCell.setCellStyle(contentStyle);
   			   			
   		    //9.노후등급
   			objCell = objRow.createCell(8);
   			objCell.setCellValue((String) map.get("oldLvl"));	
   			objCell.setCellStyle(contentStyle);
   			
   			//10.점검주기
   			objCell = objRow.createCell(9);
   			objCell.setCellValue((String) map.get("chkPrd"));
   			objCell.setCellStyle(contentStyle);
   			
   			//11.최근점검일자
   			objCell = objRow.createCell(10);
   			objCell.setCellValue((String) map.get("chkResultYmd"));		
   			objCell.setCellStyle(contentStyle);
		}		
   		
   		for (i=0; i < 11; i++) {
   			objSheet.autoSizeColumn((short)i);
   		}
   		
   		response.setContentType("Application/Msexcel");
   		response.setHeader("Content-Disposition", "ATTachment; Filename="+URLEncoder.encode("시설물노후도","UTF-8") + "_" + format1.format(today) +".xls");
   		fileOut  = response.getOutputStream(); 
   		objWorkBook.write(fileOut);
   		fileOut.close();
   		response.getOutputStream().flush();
   		response.getOutputStream().close();
   	}
	
	/**
	 * 시설물 가동시간 및 점검이력
	 * @param searchVO - 조회할 정보가 담긴 FcltMntVO
	 * @return "pms/fcltMnt/oldMngDtl.do"
	 * @exception Exception
	 */
	@RequestMapping(value = "/oldMngDtl.do")
	public String oldMngDtl(HttpServletRequest request
			,@RequestParam(value="g2Id", required=false) BigDecimal g2Id
			,@RequestParam(value="tagId", required=false) String tagId
			,@RequestParam(value="ftrCde", required=false) String ftrCde
			,@RequestParam(value="ftrIdn", required=false) BigDecimal ftrIdn
			,HttpServletResponse response, Model model) throws Exception {
		
		Map<String, Object> map = new HashMap<String, Object>();
			
		map.put("g2Id"	, g2Id);
		map.put("tagId"	, tagId);
		map.put("ftrCde", ftrCde);
		map.put("ftrIdn", ftrIdn);
		
		//조회
		Map<String, Object> info = (Map<String, Object>) cmmService.select("selectOldMngDtlInfo", map);		
		model.addAttribute("oldMngInfo", info);
		
		//조회
		List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectOldMngChkHtList",map);
		model.addAttribute("oldMngList", list);
		
		return "pms/fcltMnt/oldMngDtl";
	}	

}
