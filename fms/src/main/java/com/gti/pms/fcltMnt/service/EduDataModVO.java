package com.gti.pms.fcltMnt.service;

/**
 * 교육자료VO클래스로서 사업관리관리 비지니스로직 처리용 항목을 구성한다.
 * @author 공통서비스 개발팀 
 * @since 2017.04.10
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2017.04.10           최초 생성
 *
 * </pre>
 */
public class EduDataModVO extends EduVO{

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	/** */
	private String 	ttl	 = "";
	private String 	contents	 = "";
	private String 	delYn	 = "";
	private String 	regId	 = "";
	private String 	regDt	 = "";
	private String 	edtId	 = "";
	private String 	edtDt	 = "";

	private long 	seq	 = 0L;
	private String 	faqCatCde	 = "";
	private String 	faqCuzCde	 = "";
	private String 	ftrCde	 = "";
	private String 	ftrNam	 = "";
	private String 	sysCd	 = "";
	private String 	sysNm	 = "";
	private long 	readCnt	 = 0L;
	private String 	regNm	 = "";
	private String 	ttl2	 = "";
	private String 	fileSeq	 = "";
	private String 	question = "";
	private String 	repl	 = "";
	
	private String 	mode	 = "";

    /** 현재페이지 */
    private int pageIndex = 1;
    
    /** 페이지갯수 */
    private int pageUnit = 10;
    
    /** 페이지사이즈 */
    private int pageSize = 10;

    /** firstIndex */
    private int firstIndex = 1;

    /** lastIndex */
    private int lastIndex = 1;

    /** recordCountPerPage */
    private int recordCountPerPage = 10;
  
	/** 검색조건 */
    private String searchCondition = "";
    
    /** 검색Keyword */
    private String searchKeyword = "";
    
	private String 	questionSeq	 = "";
	private String 	lvl	 = "";
	private long 	childCnt	 = 0L;

	public long getChildCnt() {
		return childCnt;
	}
	public void setChildCnt(long childCnt) {
		this.childCnt = childCnt;
	}
	

	public String getLvl() {
		return lvl;
	}
	public void setLvl(String lvl) {
		this.lvl = lvl;
	}
	
	public String getQuestionSeq() {
		return questionSeq;
	}
	public void setQuestionSeq(String questionSeq) {
		this.questionSeq = questionSeq;
	}
	public long getSeq() {
		return seq;
	}
	public void setSeq(long seq) {
		this.seq = seq;
	}

	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getRepl() {
		return repl;
	}
	public void setRepl(String repl) {
		this.repl = repl;
	}


	public long getReadCnt() {
		return readCnt;
	}
	public void setReadCnt(long readCnt) {
		this.readCnt = readCnt;
	}	

	public String getRegNm() {
		return regNm;
	}
	public void setRegNm(String regNm) {
		this.regNm = regNm;
	}

	public String getFileSeq() {
		return fileSeq;
	}
	public void setFileSeq(String fileSeq) {
		this.fileSeq = fileSeq;
	}

	public String getTtl2() {
		return ttl2;
	}
	public void setTtl2(String ttl2) {
		this.ttl2 = ttl2;
	}

	public String getSysCd() {
		return sysCd;
	}
	public void setSysCd(String sysCd) {
		this.sysCd = sysCd;
	}
	public String getSysNm() {
		return sysNm;
	}
	public void setSysNm(String sysNm) {
		this.sysNm = sysNm;
	}
	
	/**
	 * searchCondition attribute 값을  리턴한다.
	 * @return String
	 */
	public String getSearchCondition() {
		return searchCondition;
	}

	/**
	 * searchCondition attribute 값을 설정한다.
	 * @param searchCondition String
	 */
	public void setSearchCondition(String searchCondition) {
		this.searchCondition = searchCondition;
	}

	/**
	 * searchKeyword attribute 값을  리턴한다.
	 * @return String
	 */
	public String getSearchKeyword() {
		return searchKeyword;
	}

	/**
	 * searchKeyword attribute 값을 설정한다.
	 * @param searchKeyword String
	 */
	public void setSearchKeyword(String searchKeyword) {
		this.searchKeyword = searchKeyword;
	}

	

	/**
	 * pageIndex attribute 값을  리턴한다.
	 * @return int
	 */
	public int getPageIndex() {
		return pageIndex;
	}

	/**
	 * pageIndex attribute 값을 설정한다.
	 * @param pageIndex int
	 */
	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}

	/**
	 * pageUnit attribute 값을  리턴한다.
	 * @return int
	 */
	public int getPageUnit() {
		return pageUnit;
	}

	/**
	 * pageUnit attribute 값을 설정한다.
	 * @param pageUnit int
	 */
	public void setPageUnit(int pageUnit) {
		this.pageUnit = pageUnit;
	}

	/**
	 * pageSize attribute 값을  리턴한다.
	 * @return int
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * pageSize attribute 값을 설정한다.
	 * @param pageSize int
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * firstIndex attribute 값을  리턴한다.
	 * @return int
	 */
	public int getFirstIndex() {
		return firstIndex;
	}

	/**
	 * firstIndex attribute 값을 설정한다.
	 * @param firstIndex int
	 */
	public void setFirstIndex(int firstIndex) {
		this.firstIndex = firstIndex;
	}

	/**
	 * lastIndex attribute 값을  리턴한다.
	 * @return int
	 */
	public int getLastIndex() {
		return lastIndex;
	}

	/**
	 * lastIndex attribute 값을 설정한다.
	 * @param lastIndex int
	 */
	public void setLastIndex(int lastIndex) {
		this.lastIndex = lastIndex;
	}

	/**
	 * recordCountPerPage attribute 값을  리턴한다.
	 * @return int
	 */
	public int getRecordCountPerPage() {
		return recordCountPerPage;
	}

	/**
	 * recordCountPerPage attribute 값을 설정한다.
	 * @param recordCountPerPage int
	 */
	public void setRecordCountPerPage(int recordCountPerPage) {
		this.recordCountPerPage = recordCountPerPage;
	}
	
	public String getTtl() {
		return ttl;
	}
	public void setTtl(String ttl) {
		this.ttl = ttl;
	}
	public String getContents() {
		return contents;
	}
	public void setContents(String contents) {
		this.contents = contents;
	}
	public String getDelYn() {
		return delYn;
	}
	public void setDelYn(String delYn) {
		this.delYn = delYn;
	}
	public String getRegId() {
		return regId;
	}
	public void setRegId(String regId) {
		this.regId = regId;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getEdtId() {
		return edtId;
	}
	public void setEdtId(String edtId) {
		this.edtId = edtId;
	}
	public String getEdtDt() {
		return edtDt;
	}
	public void setEdtDt(String edtDt) {
		this.edtDt = edtDt;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	public String getFaqCatCde() {
		return faqCatCde;
	}
	public void setFaqCatCde(String faqCatCde) {
		this.faqCatCde = faqCatCde;
	}
	public String getFaqCuzCde() {
		return faqCuzCde;
	}
	public void setFaqCuzCde(String faqCuzCde) {
		this.faqCuzCde = faqCuzCde;
	}
	public String getFtrCde() {
		return ftrCde;
	}
	public void setFtrCde(String ftrCde) {
		this.ftrCde = ftrCde;
	}
	public String getFtrNam() {
		return ftrNam;
	}
	public void setFtrNam(String ftrNam) {
		this.ftrNam = ftrNam;
	}
	
	
}

