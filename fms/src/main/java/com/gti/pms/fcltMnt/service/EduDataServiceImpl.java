package com.gti.pms.fcltMnt.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.gti.pms.fcltMnt.dao.EduDataDAO;

import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;

/**
 * 교육자료에 관한 비지니스 클래스를 정의한다.
 * @author 공통서비스 개발팀 
 * @version 1.0
 * @see
 *
 */
@Service("eduDataService")
public class EduDataServiceImpl extends EgovAbstractServiceImpl implements EduDataService {

	/** eduDataDAO */
	@Resource(name="eduDataDAO")
	private EduDataDAO eduDataDAO;
	
	/**
	 * 교육자료의 정보를 데이터베이스에서 읽어와 화면에 출력
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public List<?> selectEduDataList(EduVO eduVO) {
		List<?> result = eduDataDAO.selectEduDataList(eduVO);
		return result; 
	}

	/**
	 * 교육자료목록의 전체수를 확인
	 * @param eduVO 검색조건
	 * @return 총사용자갯수(int)
	 * @throws Exception
	 */
	@Override
	public int selectEduListTotCnt(EduVO eduVO) {
		return eduDataDAO.selectEduListTotCnt(eduVO);
	}	

	/**
	 * 사업관리의 정보를 데이터베이스에서 읽어와 화면에 출력
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public EduDataModVO selectEduDataInfo(EduVO eduVO) {
		EduDataModVO eduDataModVo = eduDataDAO.selectEduDataInfo(eduVO);
		return eduDataModVo;
	}

	/**
	 * 저장처리 
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public void updateEdu(EduDataModVO eduModVO) {
		eduDataDAO.updateEdu(eduModVO);
	}

	/**
	 * 조회수 업데이트
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public void updateEduReadCnt(EduDataModVO eduModVO) {
		eduDataDAO.updateEduReadCnt(eduModVO);
	}

	/**
	 * 삭제처리
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public void deleteEdu(EduDataModVO eduModVO) {
		eduDataDAO.deleteEdu(eduModVO);
	}
	/**
	 * 교육자료의 정보를 데이터베이스에서 읽어와 화면에 출력
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public List<?> selectEduFaqList(EduVO eduVO) {
		List<?> result = eduDataDAO.selectEduFaqList(eduVO);
		return result; 
	}

	/**
	 * 교육자료목록의 전체수를 확인
	 * @param eduVO 검색조건
	 * @return 총사용자갯수(int)
	 * @throws Exception
	 */
	@Override
	public int selectEduFaqListTotCnt(EduVO eduVO) {
		return eduDataDAO.selectEduFaqListTotCnt(eduVO);
	}	

	/**
	 * 사업관리의 정보를 데이터베이스에서 읽어와 화면에 출력
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public EduDataModVO selectEduFaqInfo(EduVO eduVO) {
		EduDataModVO eduDataModVo = eduDataDAO.selectEduFaqInfo(eduVO);
		return eduDataModVo;
	}

	/**
	 * 저장처리 
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public void updateEduFaq(EduDataModVO eduModVO) { 
		eduDataDAO.updateEduFaq(eduModVO);
	}

	/**
	 * 삭제처리
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public void deleteEduFaq(EduDataModVO eduModVO) {
		eduDataDAO.deleteEduFaq(eduModVO);
	}

	/**
	 * 조회수 업데이트
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public void updateEduFaqReadCnt(EduDataModVO eduModVO) {
		eduDataDAO.updateEduFaqReadCnt(eduModVO);
	}

	/**
	 * 교육자료의 정보를 데이터베이스에서 읽어와 화면에 출력
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public List<?> selectEduQnaList(EduVO eduVO) {
		List<?> result = eduDataDAO.selectEduQnaList(eduVO);
		return result; 
	}

	/**
	 * 교육자료목록의 전체수를 확인
	 * @param eduVO 검색조건
	 * @return 총사용자갯수(int)
	 * @throws Exception
	 */
	@Override
	public int selectEduQnaListTotCnt(EduVO eduVO) {
		return eduDataDAO.selectEduQnaListTotCnt(eduVO);
	}	

	/**
	 * 사업관리의 정보를 데이터베이스에서 읽어와 화면에 출력
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public EduDataModVO selectEduQnaInfo(EduVO eduVO) {
		EduDataModVO eduDataModVo = eduDataDAO.selectEduQnaInfo(eduVO);
		return eduDataModVo;
	}

	/**
	 * 사업관리의 정보를 데이터베이스에서 읽어와 화면에 출력
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public EduDataModVO selectEduQnaReplyInfo(EduVO eduVO) {
		EduDataModVO eduDataModVo = eduDataDAO.selectEduQnaReplyInfo(eduVO);
		return eduDataModVo;
	}

	/**
	 * 저장처리 
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public void updateEduQna(EduDataModVO eduModVO) { 
		eduDataDAO.updateEduQna(eduModVO);
	}

	/**
	 * 삭제처리
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public void deleteEduQna(EduDataModVO eduModVO) {
		eduDataDAO.deleteEduQna(eduModVO);
	}

	/**
	 * 조회수 업데이트
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public void updateEduQnaReadCnt(EduDataModVO eduModVO) {
		eduDataDAO.updateEduQnaReadCnt(eduModVO);
	}
}