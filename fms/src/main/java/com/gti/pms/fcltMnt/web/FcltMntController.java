package com.gti.pms.fcltMnt.web;

import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.gti.pms.cmm.service.CmmService;
import com.gti.pms.cmm.web.CodeController;
import com.gti.pms.fcltMnt.service.PdjtMaVO;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

/**
 * @Class Name : WttWutlHtController.java
 * @Description : WttWutlHt Controller class
 * @Modification Information
 *
 * @author DRCTS
 * @since 2018-07-01
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Controller
@RequestMapping("/pms/fcltMnt")
public class FcltMntController extends CodeController {
	
	private static final Logger logger = LoggerFactory.getLogger(FcltMntController.class);
	

	@Resource(name = "cmmService")
    private CmmService cmmService;
	
	
    
    /** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;

    
	
    
    
    
    /**
     * 점검일정목록
     * @param request
     * @param searchVO
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/chkSchList.do")
    public String chscMaListIndex (HttpServletRequest request, Model model) throws Exception {

        
    	//관리기관콤보
    	getMngCdeListModel(model);        
		
    	return "pms/fcltMnt/chkSchList";
    }
    
    
    
    

    
    
    /**
     * 점검일정등록
     * @param request
     * @param sclNum
     * @param inhNum
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/chkSchCreate.do")
    public String chkSchCreate (HttpServletRequest request, Model model) throws Exception {    	
    	
		/*관리기관 코드*/
		getMngCdeListModel(model);
		
    	return "pms/fcltMnt/chkSchCreate";
    }
        
    
    
    
    /**
     * 점검일정 상세정보
     * @param request
     * @param sclNum
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/chkSchDtl.do")
    public String chkSchDtl (HttpServletRequest request,
    		@RequestParam(value="sclNum", required=false) String sclNum, Model model) throws Exception {
    	
    	// 점검상세조회
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put("sclNum", sclNum);
    	List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectChscMaDtl", param);
    	
    	if(lst != null && !lst.isEmpty()){
    		model.addAttribute("chscMaDtl", lst.get(0));
    	}
    	else{
    		model.addAttribute("msg", "점검정보가 존재하지 않습니다.");
    	}
    	
    	/*관리기관 코드*/
    	getMngCdeListModel(model);
    	
    	model.addAttribute("sclNum", sclNum);
    	return "pms/fcltMnt/chkSchDtl";
    }
    
    
    
    
    
    
    
    
    
    
    
    
	/**
	 * 소모품 목록 조회 화면 호출
	 * 
	 * @return "/fcltMnt/pdjtMaMngList"
	 * @exception Exception
	 */
	@RequestMapping(value = "/pdjtMaMngList.do")
	public String pdjtMaMngList(HttpServletRequest request,HttpServletResponse response, Model model) throws Exception {
		return "pms/fcltMnt/pdjtMaMngList";
	}
    
	
	
	/**
	 * 소모품 추가,수정,삭제
	 * 
	 * @return "/fcltMnt/pdjtMaMngList"
	 * @exception Exception
	 */
	@RequestMapping(value = "/savePdjtMaMngList.do")
	public ModelAndView savePdjtMaMngList(@RequestBody Map<String,Object> param) throws Exception {
		ModelAndView mv = new ModelAndView("/proc/proc");
		
		String status  = "";
		String sPdhNum = "";
		String result = "";
		
		// 1.공정스케줄  저장
		
		List<Map<String, Object>> lst = (List<Map<String, Object>>) param.get("lst");
		
		int saveCnt = 0;
		for (Map<String, Object> map : lst) {			
						
			map.put("sqlId","");
			if("D".equals(map.get("_CUD"))){
				//소모품 입고현황 삭제
				map.put("sqlId","deletePdjtInHtPop");
				result = cmmService.saveFormData(map);
				//소모품마스터 삭제 
				map.put("sqlId","deletePdjtMaMng");
				result = cmmService.saveFormData(map);				
				saveCnt++;
			}else if("U".equals(map.get("_CUD")) || "C".equals(map.get("_CUD"))){
				map.put("sqlId","updatePdjtMaMng");
				result = cmmService.saveFormData(map);
				saveCnt++;
			}
		}
		
		JSONObject ret = new JSONObject();
		ret.put("resultCnt", saveCnt);
		ret.put("result", true);
		
		mv.addObject("result", ret.toJSONString());
		
		return mv;
	}
	
	/**
	 * 소모품 입고등록 화면 호출
	 * 
	 * @return "/fcltMnt/pdjtMaMng"
	 * @exception Exception
	 */
	@RequestMapping(value = "/pdjtMaMngInCrt.do")
	public String pdjtMaMngInCrt(HttpServletRequest request,HttpServletResponse response, Model model) throws Exception {
		return "pms/fcltMnt/pdjtMaMngInCrt";
	}
	
	/**
	 * WTT_CONS_MA 목록 조회 (paging)
	 * @param searchVO - 조회할 정보가 담긴 PdjtMaVo
	 * @return "pms/fcltMnt/pdjtMaUseHtList"
	 * @exception Exception
	 */
	@RequestMapping("/pdjtMaUseHtList.do")
	public String pdjtMaUseHitList(HttpServletRequest request, 
			@ModelAttribute("searchVO") PdjtMaVO searchVO, Model model) throws Exception {
		
		searchVO.setPageUnit(propertiesService.getInt("pageUnit")); 
		searchVO.setPageSize(propertiesService.getInt("pageSize"));
		
		//paging
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());
		
		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		
		//조회
		List<Map<String, Object>> pdjtMaUseHtList = (List<Map<String, Object>>) cmmService.selectList("selectPdjtMaUseHtList", searchVO);
		model.addAttribute("resultList", pdjtMaUseHtList);
		
		//공통코드내려주기
		//setCodeModels(request, model);
		
		//페이징처리
		List<Map<String, Object>> listCnt = (List<Map<String, Object>>) cmmService.selectList("selectPdjtMaUseHtTotCnt", searchVO);
		int totCnt = 0;
		try{
			totCnt = ((BigDecimal) listCnt.get(0).get("totCnt")).intValue();
		}catch(Exception e){
			logger.debug("..." + e.getMessage());
		};
		
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);
				
		return "pms/fcltMnt/pdjtMaUseHtList";
	}
	
	/**
	 * 소모품 입고등록 화면 호출
	 * 
	 * @return "/fcltMnt/pdjtMaMng"
	 * @exception Exception
	 */
	@RequestMapping(value = "/pdjtMaUseHtDtl.do")
	public String pdjtMaUseHtDtl(HttpServletRequest request
			,@RequestParam(value="pdhNum", required=false) BigDecimal pdhNum
			,HttpServletResponse response, Model model) throws Exception {
		
		Map<String, Object> map = new HashMap<String, Object>();			
		map.put("pdhNum", pdhNum);
		
		//조회
		Map<String, Object> pdjtMaHtInfo = (Map<String, Object>) cmmService.select("selectPdjtMaUseHtDtlInfo", map);
		
		model.addAttribute("pdjtMaHtInfo", pdjtMaHtInfo);
		
		//조회
		List<Map<String, Object>> pdjtMaHtList = (List<Map<String, Object>>) cmmService.selectList("selectPdjtMaUseHtDtlList", map);
		model.addAttribute("pdjtMaHtList", pdjtMaHtList);
		
		return "pms/fcltMnt/pdjtMaUseHtDtl";
	}
	
	@RequestMapping(value="/pdjtMaUseHt/excel/download.do")
   	public void getExelPdjtMaUseHt(HttpServletRequest request, @ModelAttribute PdjtMaVO vo, HttpServletResponse response , Model model) throws Exception {
   		HSSFWorkbook objWorkBook = new HSSFWorkbook();
   		HSSFSheet objSheet;
   		HSSFRow objRow;
   		HSSFCell objCell;
   		OutputStream fileOut = null;
   		
   		Map<String, Object> map = new HashMap<String, Object>();
   		BigDecimal dData = new BigDecimal(0);
   		
   		//System.out.println("get idx : " + vo.getSearchCondition());

   		//제목폰트
   		HSSFFont font = objWorkBook.createFont();
   		font.setFontHeightInPoints((short)20);
   		font.setBoldweight((short)font.BOLDWEIGHT_BOLD);
   		font.setFontName("맑은고딕");

   		HSSFCellStyle titleStyle = objWorkBook.createCellStyle();
   		titleStyle.setFont(font);
   		titleStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
   		titleStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

   		//테이블 헤더 폰트
   		HSSFFont headerFont = objWorkBook.createFont();
   		headerFont.setFontHeightInPoints((short)10);
   		headerFont.setBoldweight((short)font.BOLDWEIGHT_BOLD);
   		headerFont.setFontName("맑은고딕");

   		HSSFCellStyle tableHeaderStyle = objWorkBook.createCellStyle();
   		tableHeaderStyle.setFont(headerFont);
   		tableHeaderStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
   		tableHeaderStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
   		tableHeaderStyle.setBorderTop((short)1);
   		tableHeaderStyle.setBorderBottom((short)1);
   		
   		//테이블 내용 폰트
   		HSSFFont contentFont = objWorkBook.createFont();
   		contentFont.setFontHeightInPoints((short)9);
   		contentFont.setBoldweight((short)font.BOLDWEIGHT_NORMAL);
   		contentFont.setFontName("맑은고딕");

   		HSSFCellStyle contentStyle = objWorkBook.createCellStyle();
   		contentStyle.setFont(contentFont);
   		contentStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
   		contentStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
   		
   		objSheet = objWorkBook.createSheet("소모품 예비품 현황");

   		Date today = new Date();
   		SimpleDateFormat format1 = new SimpleDateFormat("yyyyMMddHHmmssSSS");
   		SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");

   		 // 제목 행
   		objRow = objSheet.createRow(0);
   		objRow.setHeight ((short) 0x250);
   		objCell = objRow.createCell(0);
   		objCell.setCellValue("소모품 예비품 현황 목록");
   		objCell.setCellStyle(titleStyle);
   		objSheet.addMergedRegion(new CellRangeAddress(0,0,0,8));
    
   		// 시간 행
   		objRow = objSheet.createRow(2);
   		objRow.setHeight ((short) 0x150);
   		objCell = objRow.createCell(0);
   		objCell.setCellValue(format2.format(today));		
   		objCell.setCellStyle(tableHeaderStyle);

   		// 헤더 행
   		objRow = objSheet.createRow(3);
   		objRow.setHeight ((short) 0x150);
   		
   		objCell = objRow.createCell(0);
   		objCell.setCellValue("순번");
   		objCell.setCellStyle(tableHeaderStyle);

   		objCell = objRow.createCell(1);
   		objCell.setCellValue("구분");
   		objCell.setCellStyle(tableHeaderStyle);

   		objCell = objRow.createCell(2);
   		objCell.setCellValue("품명");
   		objCell.setCellStyle(tableHeaderStyle);

   		objCell = objRow.createCell(3);
   		objCell.setCellValue("모델 및 규격");
   		objCell.setCellStyle(tableHeaderStyle);
   		
   		objCell = objRow.createCell(4);
   		objCell.setCellValue("총입고량");
   		objCell.setCellStyle(tableHeaderStyle);
   		
   		objCell = objRow.createCell(5);
   		objCell.setCellValue("총사용량");
   		objCell.setCellStyle(tableHeaderStyle);
   		
   		objCell = objRow.createCell(6);
   		objCell.setCellValue("재고수량");
   		objCell.setCellStyle(tableHeaderStyle);
   		
   		List<Map<String, Object>> pdjtMaUseHtList = (List<Map<String, Object>>) cmmService.selectList("selectPdjtMaUseHtExcelList", vo);
   		
   		int listSize = 0;
		if( pdjtMaUseHtList.size()>10000) {
			listSize = 10000;
		} else {
			listSize = pdjtMaUseHtList.size();
		}
				
		int i = 0;				
		for( i=0; i<listSize; i++ ) {
			map = pdjtMaUseHtList.get(i);
			
			objRow = objSheet.createRow(i+4);
   			objRow.setHeight ((short) 0x150);
   			
   			//1.순번
   			objCell = objRow.createCell(0);
   			objCell.setCellValue(i + 1);		
   			objCell.setCellStyle(contentStyle);
   			
   			//2.구분명
   			objCell = objRow.createCell(1);
   			objCell.setCellValue((String) map.get("pdtCatCdeNm"));		
   			objCell.setCellStyle(contentStyle);
   			
   			//3.품명
   			objCell = objRow.createCell(2);
   			objCell.setCellValue((String) map.get("pdtNam"));		
   			objCell.setCellStyle(contentStyle);
   			
   			//4.모델 및 규격
   			objCell = objRow.createCell(3);
   			objCell.setCellValue((String) map.get("pdtMdlStd"));		
   			objCell.setCellStyle(contentStyle);
   			
   			//5.총입고량
   			dData = (BigDecimal) map.get("totInAmt");
   			objCell = objRow.createCell(4);
   			objCell.setCellValue(dData.intValue());		
   			objCell.setCellStyle(contentStyle);
   			
   		    //6.총사용량
   			dData = (BigDecimal) map.get("totUseAmt");
   			objCell = objRow.createCell(5);
   			objCell.setCellValue(dData.intValue());		
   			objCell.setCellStyle(contentStyle);
   			
   		    //7.총재고수량
   			dData = (BigDecimal) map.get("curStckCnt");
   			objCell = objRow.createCell(6);
   			objCell.setCellValue(dData.intValue());		
   			objCell.setCellStyle(contentStyle);
		}		
   		
   		for (i=0; i < 9; i++) {
   			objSheet.autoSizeColumn((short)i);
   		}
   		
   		response.setContentType("Application/Msexcel");
   		response.setHeader("Content-Disposition", "ATTachment; Filename="+URLEncoder.encode("소모품 예비품 현황","UTF-8") + "_" + format1.format(today) +".xls");
   		fileOut  = response.getOutputStream(); 
   		objWorkBook.write(fileOut);
   		fileOut.close();
   		response.getOutputStream().flush();
   		response.getOutputStream().close();
   	}
	
	
	
	
}

