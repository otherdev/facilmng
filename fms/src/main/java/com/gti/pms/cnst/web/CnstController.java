package com.gti.pms.cnst.web;

import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.gti.pms.cmm.service.CmmService;
import com.gti.pms.cmm.web.CodeController;
import com.gti.pms.cnst.vo.CnstVO;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

/**
 * @Class Name : WttConsMaController.java
 * @description : WtlConsMa Controller class
 * @Modification Information
 * 
 * @author DRCTS
 * @since 2017-08-10
 * @see
 * 
 *  Copyright (C)  All right reserved.
 */

@Controller
@RequestMapping("/pms/cnst")
public class CnstController extends CodeController{
	
	private static final Logger logger = LoggerFactory.getLogger(CnstController.class);
	
	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/** cmmService */
	@Resource(name = "cmmService")
	private CmmService cmmService;
	

	
	

	

	
        
	/**
	 * WTT_CONS_MA 목록 조회 (paging)
	 * @param searchVO - 조회할 정보가 담긴 CnstVO
	 * @return "/waterpipe/cons/consList"
	 * @exception Exception
	 */
	@RequestMapping("/cnstMngList.do")
	public String cnstMngListPopup (HttpServletRequest request, 
			@ModelAttribute("searchVO") CnstVO searchVO, Model model) throws Exception {
		
		searchVO.setPageUnit(propertiesService.getInt("pageUnit")); 
		searchVO.setPageSize(propertiesService.getInt("pageSize"));
		
		//paging
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());
		
		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		
		//조회
		List<Map<String, Object>> wttConsMaList = (List<Map<String, Object>>) cmmService.selectList("selectWttConsMaList", searchVO);
		model.addAttribute("resultList", wttConsMaList);
		
		//공통코드내려주기
        /*공사구분*/
    	getCntCdeListModel(model);
		/*관리기관*/
		getMngCdeListModel(model);
		/*행정동*/
		getAdarMaListModel(model);
		/*계약방법*/
		getCttCdeListModel(model);
		/*지급구분*/
		getPtyCdeListModel(model);
		/*법정동*/
		getLgarMaListModel(model);

		
		
		//페이징처리
		List<Map<String, Object>> listCnt = (List<Map<String, Object>>) cmmService.selectList("selectWttConsMaListTotCnt", searchVO);
		int totCnt = 0;
		try{
			totCnt = ((BigDecimal) listCnt.get(0).get("totCnt")).intValue();
		}catch(Exception e){
			logger.debug("..." + e.getMessage());
		};
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);
		
		
		return "pms/cnst/cnstMngList";
	}
	
	

	
	/**
	 * WTT_CONS_MA 상세 조회
	 * @param paramVO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/cnstMngDtl.do")
	public String cnstMngDtl (HttpServletRequest request, 
			@RequestParam(value = "cntNum") String cntNum, 
			Model model) throws Exception {

		// 상수공사대장 상세정보
		Map<String, Object> param = new HashMap<String, Object> ();
		param.put("cntNum", cntNum);
		Map<String, Object> map = (Map<String, Object>) cmmService.select("selectWttConsMa", param);
		model.addAttribute("wttConsMa", map);
		
		//공통코드내려주기
        /*공사구분*/
    	getCntCdeListModel(model);
    	/*계약방법*/
    	getCttCdeListModel(model);

    	
		model.addAttribute("cntNum", cntNum);
		return "pms/cnst/cnstMngDtl";
	}
	

	
	
	/**
	 * WTT_CONS_MA 신규등록
	 * @param searchVO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/cnstMngAdd.do")
	public String cnstMngAdd (HttpServletRequest request, Model model) throws Exception {
		
		//공통코드내려주기
        /*공사구분*/
    	getCntCdeListModel(model);
		/*관리기관*/
		getMngCdeListModel(model);
		/*행정동*/
		getAdarMaListModel(model);
		/*계약방법*/
		getCttCdeListModel(model);
		/*지급구분*/
		getPtyCdeListModel(model);
		/*법정동*/
		getLgarMaListModel(model);
		
		return "pms/cnst/cnstMngAdd";
	}
	
	
	
	
	

}
