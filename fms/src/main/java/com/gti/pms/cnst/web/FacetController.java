package com.gti.pms.cnst.web;

import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.gti.pms.cmm.service.CmmService;
import com.gti.pms.cmm.web.CodeController;
import com.gti.pms.cmm.web.CommandMap;
import com.gti.pms.cnst.vo.CnstVO;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

/**
 * @Class Name : WttConsMaController.java
 * @description : WtlConsMa Controller class
 * @Modification Information
 * 
 * @author DRCTS
 * @since 2017-08-10
 * @see
 * 
 *  Copyright (C)  All right reserved.
 */

@Controller
@RequestMapping("/pms/cnst")
public class FacetController extends CodeController{
	
	private static final Logger logger = LoggerFactory.getLogger(FacetController.class);
	
	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/** cmmService */
	@Resource(name = "cmmService")
	private CmmService cmmService;
		
	

	       
	
    /**
     * 급수전대장 목록
     * @param searchVO
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/facetMngList.do")
	public String facetMngListPopup(HttpServletRequest request, @ModelAttribute("searchVO") CnstVO searchVO, Model model) throws Exception {
		/** EgovPropertyService.sample */
    	searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	searchVO.setPageSize(propertiesService.getInt("pageSize"));
    	
    	/** pageing */
    	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());
		
		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		// 공통코드내려주기
		getAdarMaListModel(model); //행정동

        //List<?> wttSplyMaList = wttSplyMaService.selectWttSplyMaList(searchVO);
        List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectWttSplyMaList", searchVO);
        model.addAttribute("resultList", list);
        
        //int totCnt = wttSplyMaService.selectWttSplyMaListTotCnt(searchVO);
        List<Map<String, Object>> listCnt = (List<Map<String, Object>>) cmmService.selectList("selectWttSplyMaListTotCnt", searchVO);
		int totCnt = 0;
		try{
			totCnt = ((BigDecimal) listCnt.get(0).get("totCnt")).intValue();
		}catch(Exception e){
			logger.debug("..." + e.getMessage());
		};
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);
		
		
		return "pms/cnst/facetMngList";
	}
    

    /**
     * 급수전대장 상세
     * @param wttSplyMaVO
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/facetMngDtl.do")
    public String facetMngDtl(HttpServletRequest request, @RequestParam(value = "cntNum") String cntNum, Model model) throws Exception {
    	

		// 급수전대장 상세정보
    	Map<String,Object> param = new HashMap<String,Object>();
    	param.put("cntNum", cntNum);
		Map<String,Object> map = (Map<String, Object>) cmmService.select("selectWttSplyMa", param);
		
		if(map != null && !map.isEmpty()){
			model.addAttribute("wttSplyMa", map);
		}
		
		
		// 공통코드내려주기
		getAdarMaListModel(model); //행정동
		
    	
    	
		model.addAttribute("cntNum", cntNum);
		return "pms/cnst/facetMngDtl";
    }
    	
    /**
     * 급수전대장 등록화면
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/facetMngAdd.do")
    public String facetMngAdd (HttpServletRequest request, Model model) throws Exception {
    	
		// 공통코드내려주기
		getAdarMaListModel(model); //행정동
		
        return "pms/cnst/facetMngAdd";
    }
	
    
	/**
	 * 급수전대장 신규저장
	 * @param commandMap
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/insertWttSply.do")
	public ModelAndView insertWttSply(CommandMap commandMap, HttpServletRequest request) throws Exception {
		ModelAndView mv = new ModelAndView("/proc/proc");

		
		// 1.급수전 마스터 저장
		commandMap.getMap().put("sqlId","insertWttSplyMa");
		String result = cmmService.saveFormData(commandMap.getMap());
		
		
		JSONObject ret = new JSONObject();
		ret.put("result", true);
		
		mv.addObject("result", ret.toJSONString());

		return mv;
	}	
        
    
	/**
	 * 급수전대장 수정
	 * @param commandMap
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateWttSply.do")
	public ModelAndView updateWttSply(CommandMap commandMap, HttpServletRequest request) throws Exception {
		ModelAndView mv = new ModelAndView("/proc/proc");
				
		// 1.급수전 마스터 저장
		commandMap.getMap().put("sqlId","updateWttSplyMa");
		String result = cmmService.saveFormData(commandMap.getMap());
		
		
		JSONObject ret = new JSONObject();
		ret.put("result", true);
		
		mv.addObject("result", ret.toJSONString());
		
		return mv;
	}	
	
	
    
    
    
    
    
    
    
    
    /** 엑셀다운로드
     * @param request
     * @param vo
     * @param response
     * @param model
     * @throws Exception
     */
    @RequestMapping(value="/facetMngList/excel/download.do")
	public void getExelFacetMngList(HttpServletRequest request, @ModelAttribute CnstVO vo, HttpServletResponse response , Model model) throws Exception {
		HSSFWorkbook objWorkBook = new HSSFWorkbook();
		HSSFSheet objSheet;
		HSSFRow objRow;
		HSSFCell objCell;
		OutputStream fileOut = null;
		
		Map<String, Object> map = new HashMap<String, Object>();
		BigDecimal dData = new BigDecimal(0);

		/*제목폰트*/
		HSSFFont font = objWorkBook.createFont();
		font.setFontHeightInPoints((short)20);
		font.setBoldweight((short)font.BOLDWEIGHT_BOLD);
		font.setFontName("맑은고딕");

		HSSFCellStyle titleStyle = objWorkBook.createCellStyle();
		titleStyle.setFont(font);
		titleStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		titleStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

		/*테이블 헤더 폰트*/
		HSSFFont headerFont = objWorkBook.createFont();
		headerFont.setFontHeightInPoints((short)10);
		headerFont.setBoldweight((short)font.BOLDWEIGHT_BOLD);
		headerFont.setFontName("맑은고딕");

		HSSFCellStyle tableHeaderStyle = objWorkBook.createCellStyle();
		tableHeaderStyle.setFont(headerFont);
		tableHeaderStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		tableHeaderStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		tableHeaderStyle.setBorderTop((short)1);
		tableHeaderStyle.setBorderBottom((short)1);
		
		/*테이블 내용 폰트*/
		HSSFFont contentFont = objWorkBook.createFont();
		contentFont.setFontHeightInPoints((short)9);
		contentFont.setBoldweight((short)font.BOLDWEIGHT_NORMAL);
		contentFont.setFontName("맑은고딕");

		HSSFCellStyle contentStyle = objWorkBook.createCellStyle();
		contentStyle.setFont(contentFont);
		contentStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		contentStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		
		/*왼쪽 정렬*/
		HSSFCellStyle contentLeftStyle = objWorkBook.createCellStyle();
		contentLeftStyle.setFont(contentFont);
		contentLeftStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
		contentLeftStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		
		/*오른쪽 정렬*/
		HSSFCellStyle contentRightStyle = objWorkBook.createCellStyle();
		contentRightStyle.setFont(contentFont);
		contentRightStyle.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
		contentRightStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		
		try {
			objSheet = objWorkBook.createSheet("급수전대장");
	
			Date today = new Date();
			SimpleDateFormat format1 = new SimpleDateFormat("yyyyMMddHHmmssSSS");
			SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
	
			 // 제목 행
			objRow = objSheet.createRow(0);
			objRow.setHeight ((short) 0x250);
			objCell = objRow.createCell(0);
			objCell.setCellValue("급수전대장 목록");
			objCell.setCellStyle(titleStyle);
			objSheet.addMergedRegion(new CellRangeAddress(0,0,0,6));
	 
			// 시간 행
			objRow = objSheet.createRow(2);
			objRow.setHeight ((short) 0x150);
			objCell = objRow.createCell(0);
			objCell.setCellValue(format2.format(today));		
			objCell.setCellStyle(tableHeaderStyle);
	
			// 헤더 행
			objRow = objSheet.createRow(3);
			objRow.setHeight ((short) 0x150);
			
			objCell = objRow.createCell(0);
			objCell.setCellValue("순번");
			objCell.setCellStyle(tableHeaderStyle);
	
			objCell = objRow.createCell(1);
			objCell.setCellValue("공사번호");
			objCell.setCellStyle(tableHeaderStyle);
	
			objCell = objRow.createCell(2);
			objCell.setCellValue("행정동");
			objCell.setCellStyle(tableHeaderStyle);
					
			objCell = objRow.createCell(3);
			objCell.setCellValue("시공자명");
			objCell.setCellStyle(tableHeaderStyle);
			
			objCell = objRow.createCell(4);
			objCell.setCellValue("설계총액");
			objCell.setCellStyle(tableHeaderStyle);
					
			objCell = objRow.createCell(5);
			objCell.setCellValue("착수일자");
			objCell.setCellStyle(tableHeaderStyle);
			
			objCell = objRow.createCell(6);
			objCell.setCellValue("준공일자");
			objCell.setCellStyle(tableHeaderStyle);
			
			
			List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectWttSplyMaExcelList", vo);
			int listSize = 0;
			if( list.size()>10000) {
				listSize = 10000;
			} else {
				listSize = list.size();
			}
			int i = 0; 
			for( i=0; i<listSize; i++ ) { 			
				map = list.get(i);
				
				objRow = objSheet.createRow(i+4);
				objRow.setHeight ((short) 0x150);
				
				objCell = objRow.createCell(0);
				objCell.setCellValue(i+1);		
				objCell.setCellStyle(contentStyle);
				
				objCell = objRow.createCell(1);
				objCell.setCellValue((String) map.get("cntNum"));
				objCell.setCellStyle(contentStyle);
							
				objCell = objRow.createCell(2);
				objCell.setCellValue((String) map.get("hjdCde"));
				objCell.setCellStyle(contentStyle);
							
				objCell = objRow.createCell(3);	
				objCell.setCellValue((String) map.get("oprNam"));
				objCell.setCellStyle(contentStyle);
							
				objCell = objRow.createCell(4);
				dData = (BigDecimal) map.get("totAmt");
				if(dData == null){
	   				objCell.setCellValue("");	
	   			}else{
	   				objCell.setCellValue(dData.intValue());		
	   			}	
				objCell.setCellStyle(contentRightStyle);
				
				objCell = objRow.createCell(5);
				objCell.setCellValue((String) map.get("begYmd"));
				objCell.setCellStyle(contentStyle);
				
				objCell = objRow.createCell(6);	
				objCell.setCellValue((String) map.get("fnsYmd"));
				objCell.setCellStyle(contentStyle);
				
			}
			
			for (i=0; i < 7; i++) {
				objSheet.autoSizeColumn((short)i);
			}
			
			response.setContentType("Application/Msexcel");
			response.setHeader("Set-Cookie", "fileDownload=true; path=/");
			response.setHeader("Content-Disposition", "ATTachment; Filename="+URLEncoder.encode("급수전대장","UTF-8") + "_" + format1.format(today) +".xls");
			fileOut  = response.getOutputStream(); 
			objWorkBook.write(fileOut);
		} catch(Exception e) {
			response.setHeader("Set-Cookie", "fileDownload=false; path=/");
			response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
			response.setHeader("Content-Type","text/html; charset=utf-8");
			
			OutputStream out = null;
	        try {
	            out = response.getOutputStream();
	            byte[] data = new String("fail..").getBytes();
	            out.write(data, 0, data.length);
	        } catch(Exception ignore) {
	            ignore.printStackTrace();
	        } finally {
	            if(out != null) try { out.close(); } catch(Exception ignore) {}
	        }
			
		} finally {
			fileOut.close();
			response.getOutputStream().flush();
			response.getOutputStream().close();
		}
	}
    	
    

}
