package com.gti.pms.cmpl.web;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.gti.pms.cmm.service.CmmService;
import com.gti.pms.cmm.service.LinkVO;
import com.gti.pms.cmm.web.CodeController;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
/**
 * @Class Name : WttWserMaController.java
 * @Description : WttWserMa Controller class
 * @Modification Information
 *
 * @author DRCTS
 * @since 2018-07-01
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Controller
@RequestMapping("/pms/cmpl")
public class LekSiteController extends CodeController{

	@Resource(name = "cmmService")
	private CmmService cmmService;
	

    /** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;
   	
    
    
    
	
	/**
	 * 누수지점 WTL_LEAK_PS 목록을 조회한다. (pageing)
	 * @param request
	 * @param searchVO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/lekSiteList.do")
	public String selectWtlLeakPsList(HttpServletRequest request, @ModelAttribute("searchVO") LinkVO searchVO, 
			Model model) throws Exception {
		
		/** EgovPropertyService.sample */
		searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
		searchVO.setPageSize(propertiesService.getInt("pageSize"));
		
		/** pageing */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());
		
		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
			
		
		//조회
		List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectWtlLeakPsList", searchVO);
		model.addAttribute("resultList", lst);

		
		//페이징처리
		int totCnt = 0;
		List<Map<String, Object>> listCnt = (List<Map<String, Object>>) cmmService.selectList("selectWtlLeakPsListTotCnt", searchVO);
		try{
			totCnt = ((BigDecimal) listCnt.get(0).get("totCnt")).intValue();
		}catch(Exception e){
		};
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);
		
		
		//코드내려주기
		getAdarMaListModel(model); //행정동
		getLepCdeListModel(model); //누수부위
		getLrsCdeListModel(model); //누수원인
		getFtrcMaListModel(model); //지형지물코드
		
		
		return "pms/cmpl/lekSiteList";
	} 
    
	
	
	/**
	 * 누수지점 상세
	 * @param request
	 * @param response
	 * @param ftrCde
	 * @param ftrIdn
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/lekSiteDtl.do")
	public String leakPsView(
			HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam("ftrCde") String ftrCde, 
			@RequestParam("ftrIdn") String ftrIdn, 
			Model model) throws Exception {
		try {
			/*상세정보*/
			Map<String, Object> param = new HashMap<String, Object> ();
			param.put("ftrCde", ftrCde);
			param.put("ftrIdn", ftrIdn);
			Map<String, Object> map = (Map<String, Object>) cmmService.select("selectWtlLeakPs", param);
			model.addAttribute("resultVO", map);   	

			/*첨부파일ID*/
			map = (Map<String, Object>) cmmService.select("selectLekFileSeq", param);
			if(map != null && !map.isEmpty()){
				model.addAttribute("filSeq", map.get("filSeq"));   	
			}
			
			
			
			//코드내려주기
			getAdarMaListModel(model); //행정동
			getLepCdeListModel(model); //누수부위
			getLrsCdeListModel(model); //누수원인
			getFtrcMaListModel(model); //지형지물코드
			getMopCdeListModel(model); //관재질코드
			
		}catch (Exception e) {
			// TODO Auto-generated catch block
			model.addAttribute("res", "FAIL");
			model.addAttribute("err", "ERR005");
			model.addAttribute("msg", "조회할 내역이 없습니다.");
		} 
		return "pms/cmpl/lekSiteDtl";
	}

	
	/**
	 * 누수지점 신규등록
	 * @param request
	 * @param response
	 * @param wserSeq
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/lekSiteAdd.do")
	public String leakPsAdd(
			HttpServletRequest request,
			HttpServletResponse response,
			Model model) throws Exception {
		
		
		//관리번호 채번
		int ftrIdn = 0;
		Map<String, Object> map = (Map<String, Object>) cmmService.select("selectMaxLekFtrIdn", new HashMap<String, Object>());
		if(map != null && !map.isEmpty()){
			ftrIdn = Integer.parseInt(map.get("ftrIdn").toString());
		}
		model.addAttribute("ftrIdnVal", ftrIdn);
		
	
		//코드내려주기
		getAdarMaListModel(model); //행정동
		getLepCdeListModel(model); //누수부위
		getLrsCdeListModel(model); //누수원인
		getFtrcMaListModel(model); //지형지물코드
		getMopCdeListModel(model); //관재질코드

		// GIS에서 대장등록하기 위한 구분변수
		//model.addAttribute("type", type);
		
		return "pms/cmpl/lekSiteAdd";
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	@RequestMapping(value="/leak/excel/download.do")
		public void getExel(HttpServletRequest request, @ModelAttribute LinkVO vo, HttpServletResponse response , Model model) throws Exception {
		/*
		HSSFWorkbook objWorkBook = new HSSFWorkbook();
		HSSFSheet objSheet;
		HSSFRow objRow;
		HSSFCell objCell;
		OutputStream fileOut = null;
		
		System.out.println("get idx : " + vo.getSearchCondition1());

		제목폰트
		HSSFFont font = objWorkBook.createFont();
		font.setFontHeightInPoints((short)20);
		font.setBoldweight((short)font.BOLDWEIGHT_BOLD);
		font.setFontName("맑은고딕");

		HSSFCellStyle titleStyle = objWorkBook.createCellStyle();
		titleStyle.setFont(font);
		titleStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		titleStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

		테이블 헤더 폰트
		HSSFFont headerFont = objWorkBook.createFont();
		headerFont.setFontHeightInPoints((short)10);
		headerFont.setBoldweight((short)font.BOLDWEIGHT_BOLD);
		headerFont.setFontName("맑은고딕");

		HSSFCellStyle tableHeaderStyle = objWorkBook.createCellStyle();
		tableHeaderStyle.setFont(headerFont);
		tableHeaderStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		tableHeaderStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		tableHeaderStyle.setBorderTop((short)1);
		tableHeaderStyle.setBorderBottom((short)1);
		
		테이블 내용 폰트
		HSSFFont contentFont = objWorkBook.createFont();
		contentFont.setFontHeightInPoints((short)9);
		contentFont.setBoldweight((short)font.BOLDWEIGHT_NORMAL);
		contentFont.setFontName("맑은고딕");

		HSSFCellStyle contentStyle = objWorkBook.createCellStyle();
		contentStyle.setFont(contentFont);
		contentStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		contentStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		
		objSheet = objWorkBook.createSheet("누수지점");

		Date today = new Date();
		SimpleDateFormat format1 = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
		
		 // 제목 행
		objRow = objSheet.createRow(0);
		objRow.setHeight ((short) 0x250);
		objCell = objRow.createCell(0);
		objCell.setCellValue("누수지점 목록");
		objCell.setCellStyle(titleStyle);
		objSheet.addMergedRegion(new CellRangeAddress(0,0,0,8));
 
		// 시간 행
		objRow = objSheet.createRow(2);
		objRow.setHeight ((short) 0x150);
		objCell = objRow.createCell(0);
		objCell.setCellValue(format2.format(today));		
		objCell.setCellStyle(tableHeaderStyle);

		// 헤더 행
		objRow = objSheet.createRow(3);
		objRow.setHeight ((short) 0x150);
		
		objCell = objRow.createCell(0);
		objCell.setCellValue("순번");
		objCell.setCellStyle(tableHeaderStyle);

		objCell = objRow.createCell(1);
		objCell.setCellValue("관리번호");
		objCell.setCellStyle(tableHeaderStyle);

		objCell = objRow.createCell(2);
		objCell.setCellValue("지형지물");
		objCell.setCellStyle(tableHeaderStyle);

		objCell = objRow.createCell(3);
		objCell.setCellValue("행정동");
		objCell.setCellStyle(tableHeaderStyle);
		
		objCell = objRow.createCell(4);
		objCell.setCellValue("관로지형지물");
		objCell.setCellStyle(tableHeaderStyle);
		
		objCell = objRow.createCell(5);
		objCell.setCellValue("누수부위");
		objCell.setCellStyle(tableHeaderStyle);
		
		objCell = objRow.createCell(6);
		objCell.setCellValue("누수원인");
		objCell.setCellStyle(tableHeaderStyle);
		
		objCell = objRow.createCell(7);
		objCell.setCellValue("누수위치설명");
		objCell.setCellStyle(tableHeaderStyle);
		
		objCell = objRow.createCell(8);
		objCell.setCellValue("누수일자");
		objCell.setCellStyle(tableHeaderStyle);
		
		objCell = objRow.createCell(9);
		objCell.setCellValue("누수복구내용");
		objCell.setCellStyle(tableHeaderStyle);
		
		objCell = objRow.createCell(10);
		objCell.setCellValue("복구일자");
		objCell.setCellStyle(tableHeaderStyle);
		
		setCodeModels(request, model);
		
		List<WtlLeakPsVO> leakList = wtlLeakPsService.selectExcelList(vo);
		int listSize = 0;
		if( leakList.size()>10000) {
			listSize = 10000;
		} else {
			listSize = leakList.size();
		}
		int i = 0; 
		for( i=0; i<listSize; i++ ) { 			
			WtlLeakPsVO leak = leakList.get(i);
			
			objRow = objSheet.createRow(i+4);
			objRow.setHeight ((short) 0x150);
			
			objCell = objRow.createCell(0);
			objCell.setCellValue(i+1);		
			objCell.setCellStyle(contentStyle);
			
			objCell = objRow.createCell(1);
			objCell.setCellValue(leak.getFtrIdn().intValue());			
			objCell.setCellStyle(contentStyle);
			
			objCell = objRow.createCell(2);
			objCell.setCellValue(mapFtrcMa.get(leak.getFtrCde()));		
			objCell.setCellStyle(contentStyle);
			
			objCell = objRow.createCell(3);
			objCell.setCellValue(mapAdarMa.get(leak.getHjdCde()));		
			objCell.setCellStyle(contentStyle);
			
			objCell = objRow.createCell(4);
			objCell.setCellValue(mapFtrcMa.get(leak.getPipCde()));		
			objCell.setCellStyle(contentStyle);
			
			objCell = objRow.createCell(5);
			objCell.setCellValue(mapLepCde.get(leak.getLepCde()));		
			objCell.setCellStyle(contentStyle);
			
			objCell = objRow.createCell(6);
			objCell.setCellValue(mapLrsCde.get(leak.getLrsCde()));		
			objCell.setCellStyle(contentStyle);
			
			objCell = objRow.createCell(7);
			objCell.setCellValue(leak.getLekLoc());		
			objCell.setCellStyle(contentStyle);
			
			objCell = objRow.createCell(8);
			objCell.setCellValue(leak.getLekYmd());		
			objCell.setCellStyle(contentStyle);
			
			objCell = objRow.createCell(9);
			objCell.setCellValue(leak.getRepExp());		
			objCell.setCellStyle(contentStyle);
			
			objCell = objRow.createCell(10);
			objCell.setCellValue(leak.getRepYmd());		
			objCell.setCellStyle(contentStyle);
			
			
		}
		
		for (i=0; i < 9; i++) {
			objSheet.autoSizeColumn((short)i);
		}
		
		response.setContentType("Application/Msexcel");
		response.setHeader("Content-Disposition", "ATTachment; Filename="+URLEncoder.encode("누수지점","UTF-8") + "_" + format1.format(today) +".xls");
		fileOut  = response.getOutputStream(); 
		objWorkBook.write(fileOut);
		fileOut.close();
		response.getOutputStream().flush();
		response.getOutputStream().close();
 */
	}
}
