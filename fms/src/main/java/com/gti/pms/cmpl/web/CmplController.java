package com.gti.pms.cmpl.web;

import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gti.pms.cmm.service.CmmService;
import com.gti.pms.cmm.web.CodeController;
import com.gti.pms.cmpl.vo.CmplVO;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
/**
 * @Class Name : WttWserMaController.java
 * @Description : WttWserMa Controller class
 * @Modification Information
 *
 * @author DRCT
 * @since 2018-06-02
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Controller
@RequestMapping("/pms/cmpl")
public class CmplController extends CodeController{

	private static final Logger logger = LoggerFactory.getLogger(CmplController.class);
	
	@Resource(name = "cmmService")
	private CmmService cmmService;
	    
    /** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;
            
    /**
	 * WTT_WSER_MA 목록을 조회한다. (pageing)
	 * @param searchVO - 조회할 정보가 담긴 WttWserMaDefaultVO
	 * @return jspRoot + "/WttWserMaList"
	 * @exception Exception
	 */
    @RequestMapping(value="/cnstCmplList.do")
    public String wserConsMaList(HttpServletRequest request, @ModelAttribute("searchVO") CmplVO searchVO, 
    		Model model)
            throws Exception {
    	
    	/** EgovPropertyService.sample */
    	searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	searchVO.setPageSize(propertiesService.getInt("pageSize"));
    	
    	/** pageing */
    	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());
		
		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		
		List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectCnstCmplList", searchVO);
		model.addAttribute("resultList", list);
		
		List<Map<String, Object>> listCnt = (List<Map<String, Object>>) cmmService.selectList("selectCnstCmplListTotCnt", searchVO);
		int totCnt = 0;
		try{
			totCnt = ((BigDecimal) listCnt.get(0).get("totCnt")).intValue();
		}catch(Exception e){
			logger.debug("..." + e.getMessage());
		};

		model.addAttribute("totCnt", totCnt);
        
		paginationInfo.setTotalRecordCount(totCnt);
        model.addAttribute("paginationInfo", paginationInfo);
        
        /*지형지물*/
    	getFtrcMaListModel(model);    	
    	/*관리기관*/
    	getMngCdeListModel(model);    	
		/*행정동*/
    	getAdarMaListModel(model);    	
    	/*민원구분*/
    	getAplCdeListModel(model);
    	
        return "pms/cmpl/cnstCmplList";
    } 
        
    /**
     * 급수공사민원관리
     * */
    @RequestMapping(value="/spwCnstList.do")
    public String selectWttWserMaList(HttpServletRequest request, @ModelAttribute("searchVO") CmplVO searchVO, 
    		Model model)
            throws Exception {
    	
    	/** EgovPropertyService.sample */
    	searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	searchVO.setPageSize(propertiesService.getInt("pageSize"));
    	
    	/** pageing */
    	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());
		
		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		
		List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectSpwCnstList", searchVO);
		model.addAttribute("resultList", list);
		
		List<Map<String, Object>> listCnt = (List<Map<String, Object>>) cmmService.selectList("selectSpwCnstListTotCnt", searchVO);
		int totCnt = 0;
		try{
			totCnt = ((BigDecimal) listCnt.get(0).get("totCnt")).intValue();
		}catch(Exception e){
			logger.debug("..." + e.getMessage());
		};

        model.addAttribute("totCnt", totCnt);
        
		paginationInfo.setTotalRecordCount(totCnt);
        model.addAttribute("paginationInfo", paginationInfo);
        
        /*지형지물*/
    	getFtrcMaListModel(model);    	
    	/*관리기관*/
    	getMngCdeListModel(model);    	
		/*행정동*/
    	getAdarMaListModel(model);    	
    	/*민원구분*/
    	getAplCdeListModel(model);
        
        return "pms/cmpl/spwCnstList";
    }     
    
    @RequestMapping("/cnstCmplAdd.do")
    public String wserConsAdd(
    		HttpServletRequest request,
    		HttpServletResponse response,
    		CmplVO paramVO, Model model) throws Exception {        
		
		Map<String, Object> smap = (Map<String, Object>) cmmService.select("selectCmplTopRcvNum", null);
		String rcvNum = "";
		int wserSeq = 0;
		try{
			rcvNum  = (String) smap.get("rcvNum");
			wserSeq = ((BigDecimal) smap.get("wserSeq")).intValue();
		}catch(Exception e){
			logger.debug("..." + e.getMessage());
		};
		
		model.addAttribute("wserSeq", wserSeq);
		model.addAttribute("rcvNum" , rcvNum);
        
		 /*지형지물*/
    	getFtrcMaListModel(model);    	
    	/*관리기관*/
    	getMngCdeListModel(model);    	
		/*행정동*/
    	getAdarMaListModel(model);    	
    	/*민원구분*/
    	getAplCdeListModel(model);
    	
    	return "pms/cmpl/cnstCmplAdd";
    }

    @RequestMapping("/cnstCmplDtl.do")
    public String wserConsView(
    		HttpServletRequest request,
    		HttpServletResponse response,
    		CmplVO paramVO, Model model) throws Exception {
     
    	Map<String, Object> map = new HashMap<String, Object>();
		map.put("wserSeq", paramVO.getWserSeq());
		
		Map<String, Object> rmap = (Map<String, Object>) cmmService.select("selectWttWserMa", map);			
		model.addAttribute("resultVO", rmap);
				
		//누수지점목록
		map.put("rcvNum", rmap.get("rcvNum"));   
		
		List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectCmplSubList", map);
		model.addAttribute("resultList", list);
		
		model.addAttribute("wserSeq", paramVO.getWserSeq());
		model.addAttribute("rcvNum" , rmap.get("rcvNum"));
        
		/*지형지물*/
    	getFtrcMaListModel(model);    	
    	/*관리기관*/
    	getMngCdeListModel(model);    	
		/*행정동*/
    	getAdarMaListModel(model);    	
    	/*민원구분*/
    	getAplCdeListModel(model);
        
        return "pms/cmpl/cnstCmplDtl";
    }

	@RequestMapping("/spwCnstAdd.do")
	public String wserSuppAdd(
			HttpServletRequest request,
			HttpServletResponse response,
			CmplVO paramVO, Model model) throws Exception {     
		
		Map<String, Object> smap = (Map<String, Object>) cmmService.select("selectCmplTopRcvNum", null);
		String rcvNum = "";
		int wserSeq = 0;
		try{
			rcvNum  = (String) smap.get("rcvNum");
			wserSeq = ((BigDecimal) smap.get("wserSeq")).intValue();
		}catch(Exception e){
			logger.debug("..." + e.getMessage());
		};
		
		model.addAttribute("wserSeq", wserSeq);
		model.addAttribute("rcvNum" , rcvNum);
		
		 /*지형지물*/
    	getFtrcMaListModel(model);    	
    	/*관리기관*/
    	getMngCdeListModel(model);    	
		/*행정동*/
    	getAdarMaListModel(model);    	
    	/*민원구분*/
    	getAplCdeListModel(model);
        
		return "pms/cmpl/spwCnstAdd";
	}
      
	@RequestMapping("/spwCnstDtl.do")
	public String wserSuppView(
			HttpServletRequest request,
			HttpServletResponse response,
			CmplVO paramVO, Model model) throws Exception {
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("wserSeq", paramVO.getWserSeq());
		
		Map<String, Object> rmap = (Map<String, Object>) cmmService.select("selectWttWserMa", map);			
		model.addAttribute("resultVO", rmap);			
        
		 /*지형지물*/
    	getFtrcMaListModel(model);    	
    	/*관리기관*/
    	getMngCdeListModel(model);    	
		/*행정동*/
    	getAdarMaListModel(model);    	
    	/*민원구분*/
    	getAplCdeListModel(model);
		
		return "pms/cmpl/spwCnstDtl";
	}  

	@RequestMapping(value="/cons/excel/download.do")
	public void getExelCons(HttpServletRequest request, @ModelAttribute CmplVO vo, HttpServletResponse response , Model model) throws Exception {
		HSSFWorkbook objWorkBook = new HSSFWorkbook();
		HSSFSheet objSheet;
		HSSFRow objRow;
		HSSFCell objCell;
		OutputStream fileOut = null;
		
		Map<String, Object> map = new HashMap<String, Object>();
		BigDecimal dData = new BigDecimal(0);

		//제목폰트
		HSSFFont font = objWorkBook.createFont();
		font.setFontHeightInPoints((short)20);
		font.setBoldweight((short)font.BOLDWEIGHT_BOLD);
		font.setFontName("맑은고딕");

		HSSFCellStyle titleStyle = objWorkBook.createCellStyle();
		titleStyle.setFont(font);
		titleStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		titleStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

		//테이블 헤더 폰트
		HSSFFont headerFont = objWorkBook.createFont();
		headerFont.setFontHeightInPoints((short)10);
		headerFont.setBoldweight((short)font.BOLDWEIGHT_BOLD);
		headerFont.setFontName("맑은고딕");

		HSSFCellStyle tableHeaderStyle = objWorkBook.createCellStyle();
		tableHeaderStyle.setFont(headerFont);
		tableHeaderStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		tableHeaderStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		tableHeaderStyle.setBorderTop((short)1);
		tableHeaderStyle.setBorderBottom((short)1);
		
		//테이블 내용 폰트
		HSSFFont contentFont = objWorkBook.createFont();
		contentFont.setFontHeightInPoints((short)9);
		contentFont.setBoldweight((short)font.BOLDWEIGHT_NORMAL);
		contentFont.setFontName("맑은고딕");

		HSSFCellStyle contentStyle = objWorkBook.createCellStyle();
		contentStyle.setFont(contentFont);
		contentStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		contentStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		
		/*왼쪽 정렬*/
		HSSFCellStyle contentLeftStyle = objWorkBook.createCellStyle();
		contentLeftStyle.setFont(contentFont);
		contentLeftStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
		contentLeftStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		
		/*오른쪽 정렬*/
		HSSFCellStyle contentRightStyle = objWorkBook.createCellStyle();
		contentRightStyle.setFont(contentFont);
		contentRightStyle.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
		contentRightStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

		
		objSheet = objWorkBook.createSheet("상수공사 민원관리");

		Date today = new Date();
		SimpleDateFormat format1 = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
		
		 // 제목 행
		objRow = objSheet.createRow(0);
		objRow.setHeight ((short) 0x250);
		objCell = objRow.createCell(0);
		objCell.setCellValue("상수공사 민원관리 목록");
		objCell.setCellStyle(titleStyle);
		objSheet.addMergedRegion(new CellRangeAddress(0,0,0,8));
 
		// 시간 행
		objRow = objSheet.createRow(2);
		objRow.setHeight ((short) 0x150);
		objCell = objRow.createCell(0);
		objCell.setCellValue(format2.format(today));		
		objCell.setCellStyle(tableHeaderStyle);

		// 헤더 행
		objRow = objSheet.createRow(3);
		objRow.setHeight ((short) 0x150);
		
		objCell = objRow.createCell(0);
		objCell.setCellValue("순번");
		objCell.setCellStyle(tableHeaderStyle);

		objCell = objRow.createCell(1);
		objCell.setCellValue("민원번호");
		objCell.setCellStyle(tableHeaderStyle);

		objCell = objRow.createCell(2);
		objCell.setCellValue("민원지행정동");
		objCell.setCellStyle(tableHeaderStyle);

		objCell = objRow.createCell(3);
		objCell.setCellValue("민원구분");
		objCell.setCellStyle(tableHeaderStyle);
		
		objCell = objRow.createCell(4);
		objCell.setCellValue("민원인");
		objCell.setCellStyle(tableHeaderStyle);
		
		objCell = objRow.createCell(5);
		objCell.setCellValue("민원인 연락처");
		objCell.setCellStyle(tableHeaderStyle);
		
		objCell = objRow.createCell(6);
		objCell.setCellValue("접수일자");
		objCell.setCellStyle(tableHeaderStyle);
		
		objCell = objRow.createCell(7);
		objCell.setCellValue("처리일자");
		objCell.setCellStyle(tableHeaderStyle);
		
		objCell = objRow.createCell(8);
		objCell.setCellValue("처리상태");
		objCell.setCellStyle(tableHeaderStyle);
	    	
    	List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectCnstCmplExcelList", vo);
   		
   		int listSize = 0;
		if( list.size()>10000) {
			listSize = 10000;
		} else {
			listSize = list.size();
		}
				
		int i = 0; 
		for( i=0; i<listSize; i++ ) { 			
			map = list.get(i);
			
			objRow = objSheet.createRow(i+4);
			objRow.setHeight ((short) 0x150);
			
			objCell = objRow.createCell(0);
			objCell.setCellValue(i+1);		
			objCell.setCellStyle(contentStyle);
			
			objCell = objRow.createCell(1);
			objCell.setCellValue((String) map.get("rcvNum"));		
			objCell.setCellStyle(contentStyle);
			
			objCell = objRow.createCell(2);
			objCell.setCellValue((String) map.get("aplHjdNam"));		
			objCell.setCellStyle(contentStyle);
			
			objCell = objRow.createCell(3);
			objCell.setCellValue((String) map.get("aplCdeNam"));		
			objCell.setCellStyle(contentStyle);
			
			objCell = objRow.createCell(4);
			objCell.setCellValue((String) map.get("apmNam"));		
			objCell.setCellStyle(contentStyle);
			
			objCell = objRow.createCell(5);
			objCell.setCellValue((String) map.get("apmTel"));		
			objCell.setCellStyle(contentStyle);
			
			objCell = objRow.createCell(6);
			objCell.setCellValue((String) map.get("rcvYmd"));		
			objCell.setCellStyle(contentStyle);
			
			objCell = objRow.createCell(7);
			objCell.setCellValue((String) map.get("proYmd"));		
			objCell.setCellStyle(contentStyle);
			
			objCell = objRow.createCell(8);
    		objCell.setCellValue((String) map.get("proCdeNam"));
			objCell.setCellStyle(contentStyle);				
			
		}
		
		for (i=0; i < 9; i++) {
			objSheet.autoSizeColumn((short)i);
		}
		
		response.setContentType("Application/Msexcel");
		response.setHeader("Content-Disposition", "ATTachment; Filename="+URLEncoder.encode("상수공사 민원관리","UTF-8") + "_" + format1.format(today) +".xls");
		fileOut  = response.getOutputStream(); 
		objWorkBook.write(fileOut);
		fileOut.close();
		response.getOutputStream().flush();
		response.getOutputStream().close();
	}
	
	@RequestMapping(value="/supp/excel/download.do")
	public void getExelSupp(HttpServletRequest request,  @ModelAttribute CmplVO vo, HttpServletResponse response , Model model) throws Exception {
		HSSFWorkbook objWorkBook = new HSSFWorkbook();
		HSSFSheet objSheet;
		HSSFRow objRow;
		HSSFCell objCell;
		OutputStream fileOut = null;
		
		Map<String, Object> map = new HashMap<String, Object>();
		BigDecimal dData = new BigDecimal(0);
		
		//제목폰트
		HSSFFont font = objWorkBook.createFont();
		font.setFontHeightInPoints((short)20);
		font.setBoldweight((short)font.BOLDWEIGHT_BOLD);
		font.setFontName("맑은고딕");
		
		HSSFCellStyle titleStyle = objWorkBook.createCellStyle();
		titleStyle.setFont(font);
		titleStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		titleStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		
		//테이블 헤더 폰트
		HSSFFont headerFont = objWorkBook.createFont();
		headerFont.setFontHeightInPoints((short)10);
		headerFont.setBoldweight((short)font.BOLDWEIGHT_BOLD);
		headerFont.setFontName("맑은고딕");
		
		HSSFCellStyle tableHeaderStyle = objWorkBook.createCellStyle();
		tableHeaderStyle.setFont(headerFont);
		tableHeaderStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		tableHeaderStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		tableHeaderStyle.setBorderTop((short)1);
		tableHeaderStyle.setBorderBottom((short)1);
		
		//테이블 내용 폰트
		HSSFFont contentFont = objWorkBook.createFont();
		contentFont.setFontHeightInPoints((short)9);
		contentFont.setBoldweight((short)font.BOLDWEIGHT_NORMAL);
		contentFont.setFontName("맑은고딕");
		
		HSSFCellStyle contentStyle = objWorkBook.createCellStyle();
		contentStyle.setFont(contentFont);
		contentStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		contentStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		
		/*왼쪽 정렬*/
		HSSFCellStyle contentLeftStyle = objWorkBook.createCellStyle();
		contentLeftStyle.setFont(contentFont);
		contentLeftStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
		contentLeftStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		
		/*오른쪽 정렬*/
		HSSFCellStyle contentRightStyle = objWorkBook.createCellStyle();
		contentRightStyle.setFont(contentFont);
		contentRightStyle.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
		contentRightStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		
		objSheet = objWorkBook.createSheet("급수공사 민원관리");
		
		Date today = new Date();
		SimpleDateFormat format1 = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
		
		// 제목 행
		objRow = objSheet.createRow(0);
		objRow.setHeight ((short) 0x250);
		objCell = objRow.createCell(0);
		objCell.setCellValue("급수공사 민원관리 목록");
		objCell.setCellStyle(titleStyle);
		objSheet.addMergedRegion(new CellRangeAddress(0,0,0,8));
		
		// 시간 행
		objRow = objSheet.createRow(2);
		objRow.setHeight ((short) 0x150);
		objCell = objRow.createCell(0);
		objCell.setCellValue(format2.format(today));		
		objCell.setCellStyle(tableHeaderStyle);
		
		// 헤더 행
		objRow = objSheet.createRow(3);
		objRow.setHeight ((short) 0x150);
		
		objCell = objRow.createCell(0);
		objCell.setCellValue("순번");
		objCell.setCellStyle(tableHeaderStyle);
		
		objCell = objRow.createCell(1);
		objCell.setCellValue("민원번호");
		objCell.setCellStyle(tableHeaderStyle);
		
		objCell = objRow.createCell(2);
		objCell.setCellValue("민원지행정동");
		objCell.setCellStyle(tableHeaderStyle);
		
		objCell = objRow.createCell(3);
		objCell.setCellValue("민원구분");
		objCell.setCellStyle(tableHeaderStyle);
		
		objCell = objRow.createCell(4);
		objCell.setCellValue("민원인");
		objCell.setCellStyle(tableHeaderStyle);
		
		objCell = objRow.createCell(5);
		objCell.setCellValue("민원인 연락처");
		objCell.setCellStyle(tableHeaderStyle);
		
		objCell = objRow.createCell(6);
		objCell.setCellValue("접수일자");
		objCell.setCellStyle(tableHeaderStyle);
		
		objCell = objRow.createCell(7);
		objCell.setCellValue("처리일자");
		objCell.setCellStyle(tableHeaderStyle);
		
		objCell = objRow.createCell(8);
		objCell.setCellValue("처리상태");
		objCell.setCellStyle(tableHeaderStyle);
						
		List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectSpwCnstExcelList", vo);
   		
   		int listSize = 0;
		if( list.size()>10000) {
			listSize = 10000;
		} else {
			listSize = list.size();
		}
				
		int i = 0; 
		for( i=0; i<listSize; i++ ) { 			
			map = list.get(i);
			
			objRow = objSheet.createRow(i+4);
			objRow.setHeight ((short) 0x150);
			
			objCell = objRow.createCell(0);
			objCell.setCellValue(i+1);		
			objCell.setCellStyle(contentStyle);
			
			objCell = objRow.createCell(1);
			objCell.setCellValue((String) map.get("rcvNum"));		
			objCell.setCellStyle(contentStyle);
			
			objCell = objRow.createCell(2);
			objCell.setCellValue((String) map.get("aplHjdNam"));		
			objCell.setCellStyle(contentStyle);
			
			objCell = objRow.createCell(3);
			objCell.setCellValue((String) map.get("aplCdeNam"));		
			objCell.setCellStyle(contentStyle);
			
			objCell = objRow.createCell(4);
			objCell.setCellValue((String) map.get("apmNam"));		
			objCell.setCellStyle(contentStyle);
			
			objCell = objRow.createCell(5);
			objCell.setCellValue((String) map.get("apmTel"));		
			objCell.setCellStyle(contentStyle);
			
			objCell = objRow.createCell(6);
			objCell.setCellValue((String) map.get("rcvYmd"));		
			objCell.setCellStyle(contentStyle);
			
			objCell = objRow.createCell(7);
			objCell.setCellValue((String) map.get("proYmd"));		
			objCell.setCellStyle(contentStyle);
			
			objCell = objRow.createCell(8);
    		objCell.setCellValue((String) map.get("proCdeNam"));
			objCell.setCellStyle(contentStyle);				
			
		}
		
		for (i=0; i < 9; i++) {
			objSheet.autoSizeColumn((short)i);
		}
		
		response.setContentType("Application/Msexcel");
		response.setHeader("Content-Disposition", "ATTachment; Filename="+URLEncoder.encode("급수공사 민원관리","UTF-8") + "_" + format1.format(today) +".xls");
		fileOut  = response.getOutputStream(); 
		objWorkBook.write(fileOut);
		fileOut.close();
		response.getOutputStream().flush();
		response.getOutputStream().close();			
	}

}
