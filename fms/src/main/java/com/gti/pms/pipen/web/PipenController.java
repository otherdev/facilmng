package com.gti.pms.pipen.web;

import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.gti.pms.cmm.service.CmmService;
import com.gti.pms.cmm.web.CodeController;
import com.gti.pms.cmm.web.CommandMap;
import com.gti.pms.pipen.vo.PipenVO;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

/**
 * @Class Name : pipenController.java
 * @Description : pipenController class
 * @Modification Information
 *  
 * @author drct 
 * @since 2018-06-02
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Controller
@RequestMapping("/pms/pipen")
public class PipenController extends CodeController{
	
	private static final Logger logger = LoggerFactory.getLogger(PipenController.class);	

    @Resource(name = "cmmService")
    private CmmService cmmService;
    
    /** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;	
	
    	
	/**
	 * 급수관로 검색 팝업
	 * @param request
	 * @param response
	 * @param searchVO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/wtpipMngList.do")
	public String pipeLmListPopup (
			HttpServletRequest request,
    		HttpServletResponse response, 
			@ModelAttribute("searchVO") PipenVO searchVO, 
			@RequestParam(value="gisLayerNm", required=false) String gisLayerNm,
			Model model) throws Exception {
		searchVO.setPageUnit(propertiesService.getInt("pageUnit")); 
		searchVO.setPageSize(propertiesService.getInt("pageSize"));
		
		//paging
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());
		
		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		/* 편집 호출 유무 : yhhwang*/
		if (gisLayerNm != null) {
			searchVO.setGeomYN("Y");
		}
		
		List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectWtPipeMngList", searchVO);
		model.addAttribute("resultList", list);
		
		List<Map<String, Object>> listCnt = (List<Map<String, Object>>) cmmService.selectList("selectWtPipeMngListTotCnt", searchVO);
		
		int totCnt = 0;
		try{
			totCnt = ((BigDecimal) listCnt.get(0).get("totCnt")).intValue();
		}catch(Exception e){
			logger.debug("..." + e.getMessage());
		};
		
		model.addAttribute("totCnt", totCnt);
		
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);
		
		/* 편집 호출 유무 : yhhwang*/
		model.addAttribute("gisLayerNm", gisLayerNm);
		
		/*지형지물*/
		getFtrcMaListModel(model);		
		/*관리기관*/
		getMngCdeListModel(model);		
		/*행정동*/
		getAdarMaListModel(model);		
		/*관재질*/
		getMopCdeListModel(model);		
		/*접합종류*/
		getJhtCdeListModel(model);	
		
		return "pms/pipen/wtpipMngList";
	}
    
	
	
	/**
	 * 상수관로 상세조회
	 * @param paramVO
	 * @param model
	 * @return
	 * @throws Exception
	 */
    @RequestMapping("/wtpipMngDtl.do")
    public String pipeLmView(HttpServletRequest request,    		
			PipenVO paramVO,
    		Model model) throws Exception {
        
    	//상수관료 상제정보
    	Map<String, Object> map = new HashMap<String, Object>();
		map.put("ftrCde", paramVO.getFtrCde());
		map.put("ftrIdn", paramVO.getFtrIdn());		

		map.put("grpTyp", PipenVO.GRP_TYPE_CONS_DRAWING);
		
		Map<String, Object> rmap = (Map<String, Object>) cmmService.select("selectWtPipenDtl", map);			
		model.addAttribute("resultVO", rmap);

		/*지형지물*/
		getFtrcMaListModel(model);		
		/*관리기관*/
		getMngCdeListModel(model);		
		/*관용도*/
		getSaaCdeListModel(model);
		/*행정동*/
		getAdarMaListModel(model);		
		/*관재질*/
		getMopCdeListModel(model);		
		/*접합종류*/
		getJhtCdeListModel(model);	
		
		// TODO : 하단 텝 유지보수 및 리스트 가져와야함		
		//============= 누수지점 목록 =============//
		List<Map<String, Object>> wlist = (List<Map<String, Object>>) cmmService.selectList("selectWtlLeakPsSubList", map);
		model.addAttribute("wtlLeakList", wlist);
	
		model.addAttribute("ftrCde", paramVO.getFtrCde());
		model.addAttribute("ftrIdn", paramVO.getFtrIdn());
				
        return "pms/pipen/wtpipMngDtl";
    }
    	

	/**
	 * 상수관로 상세조회
	 * @param paramVO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/pipeLmCreate.do")
	public String pipeLmCreate(HttpServletRequest request,			 
			Model model, PipenVO paramVO,
			@RequestParam(value="type", required=false) String type) throws Exception {
						
		Map<String, Object> map = new HashMap<String, Object>();
		
		Map<String, Object> smap = (Map<String, Object>) cmmService.select("selectWtPipenTopFtrIdn", null);			
		int ftrIdn = 0;		
		try{
			ftrIdn = ((BigDecimal) smap.get("ftrIdn")).intValue();
		}catch(Exception e){
			logger.debug("..." + e.getMessage());
		};
		
		map.put("ftrIdn",(ftrIdn + 1));
		model.addAttribute("wtlPipeLm", map);
				
		// GIS에서 대장등록하기 위한 구분변수
		model.addAttribute("type", type);
		
		/*지형지물*/
		getFtrcMaListModel(model);		
		/*관리기관*/
		getMngCdeListModel(model);		
		/*관용도*/
		getSaaCdeListModel(model);
		/*행정동*/
		getAdarMaListModel(model);		
		/*관재질*/
		getMopCdeListModel(model);		
		/*접합종류*/
		getJhtCdeListModel(model);	
			
		return "pms/pipen/wtpipMngCreate";
	}
		
	/**
	 * 상수관로 신규저장
	 * @param commandMap
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/insertWtpipMng.do")
	public ModelAndView insertWtpipMng(CommandMap commandMap, HttpServletRequest request) throws Exception {
		ModelAndView mv = new ModelAndView("/proc/proc");
		
		// 1.상수관로 마스터 저장
		commandMap.getMap().put("sqlId","insertWtlPipeLm");
		String result = cmmService.saveFormData(commandMap.getMap());
		
		JSONObject ret = new JSONObject();
		ret.put("result", true);
		ret.put("ftrIdn", commandMap.getMap().get("ftrIdn"));
		
		mv.addObject("result", ret.toJSONString());

		return mv;
	}	
	
	@RequestMapping(value="/pipenMngList/excel/download.do")
	public void getPipenMngListExel(@ModelAttribute PipenVO vo, HttpServletResponse response , Model model) throws Exception {
		HSSFWorkbook objWorkBook = new HSSFWorkbook();
		HSSFSheet objSheet;
		HSSFRow objRow;
		HSSFCell objCell;
		OutputStream fileOut = null;
		
		Map<String, Object> map = new HashMap<String, Object>();
		BigDecimal dData = new BigDecimal(0);
		
		/*제목폰트*/
		HSSFFont font = objWorkBook.createFont();
		font.setFontHeightInPoints((short)20);
		font.setBoldweight((short)font.BOLDWEIGHT_BOLD);
		font.setFontName("맑은고딕");

		HSSFCellStyle titleStyle = objWorkBook.createCellStyle();
		titleStyle.setFont(font);
		titleStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		titleStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		
		/*왼쪽 정렬*/
		HSSFCellStyle contentLeftStyle = objWorkBook.createCellStyle();
		contentLeftStyle.setFont(font);
		contentLeftStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
		contentLeftStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		
		/*오른쪽 정렬*/
		HSSFCellStyle contentRightStyle = objWorkBook.createCellStyle();
		contentRightStyle.setFont(font);
		contentRightStyle.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
		contentRightStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

		/*테이블 헤더 폰트*/
		HSSFFont headerFont = objWorkBook.createFont();
		headerFont.setFontHeightInPoints((short)10);
		headerFont.setBoldweight((short)font.BOLDWEIGHT_BOLD);
		headerFont.setFontName("맑은고딕");

		HSSFCellStyle tableHeaderStyle = objWorkBook.createCellStyle();
		tableHeaderStyle.setFont(headerFont);
		tableHeaderStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		tableHeaderStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		tableHeaderStyle.setBorderTop((short)1);
		tableHeaderStyle.setBorderBottom((short)1);
		
		/*테이블 내용 폰트*/
		HSSFFont contentFont = objWorkBook.createFont();
		contentFont.setFontHeightInPoints((short)9);
		contentFont.setBoldweight((short)font.BOLDWEIGHT_NORMAL);
		contentFont.setFontName("맑은고딕");

		HSSFCellStyle contentStyle = objWorkBook.createCellStyle();
		contentStyle.setFont(contentFont);
		contentStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		contentStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		
		objSheet = objWorkBook.createSheet("상수관로");

		Date today = new Date();
		SimpleDateFormat format1 = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");

		 // 제목 행
		objRow = objSheet.createRow(0);
		objRow.setHeight ((short) 0x250);
		objCell = objRow.createCell(0);
		objCell.setCellValue("상수관로 목록");
		objCell.setCellStyle(titleStyle);
		objSheet.addMergedRegion(new CellRangeAddress(0,0,0,8));
 
		// 시간 행
		objRow = objSheet.createRow(2);
		objRow.setHeight ((short) 0x150);
		objCell = objRow.createCell(0);
		objCell.setCellValue(format2.format(today));		
		objCell.setCellStyle(tableHeaderStyle);

		// 헤더 행
		objRow = objSheet.createRow(3);
		objRow.setHeight ((short) 0x150);
		
		objCell = objRow.createCell(0);
		objCell.setCellValue("순번");
		objCell.setCellStyle(tableHeaderStyle);

		objCell = objRow.createCell(1);
		objCell.setCellValue("지형지물");
		objCell.setCellStyle(tableHeaderStyle);

		objCell = objRow.createCell(2);
		objCell.setCellValue("관리번호");
		objCell.setCellStyle(tableHeaderStyle);

		objCell = objRow.createCell(3);
		objCell.setCellValue("행정동");
		objCell.setCellStyle(tableHeaderStyle);
		
		objCell = objRow.createCell(4);
		objCell.setCellValue("도엽번호");
		objCell.setCellStyle(tableHeaderStyle);
		
		objCell = objRow.createCell(5);
		objCell.setCellValue("관리기관");
		objCell.setCellStyle(tableHeaderStyle);
		
		objCell = objRow.createCell(6);
		objCell.setCellValue("설치일자");
		objCell.setCellStyle(tableHeaderStyle);
		
		objCell = objRow.createCell(7);
		objCell.setCellValue("관용도");
		objCell.setCellStyle(tableHeaderStyle);
		
		objCell = objRow.createCell(8);
		objCell.setCellValue("관재질");
		objCell.setCellStyle(tableHeaderStyle);

		List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectWtpipMngExcelList", vo);
   		
   		int listSize = 0;
		if( list.size()>10000) {
			listSize = 10000;
		} else {
			listSize = list.size();
		}
		int i = 0; 
		for( i=0; i<listSize; i++ ) { 			
			map = list.get(i);
			
			objRow = objSheet.createRow(i+4);
			objRow.setHeight ((short) 0x150);
			
			objCell = objRow.createCell(0);
			objCell.setCellValue(i+1);		
			objCell.setCellStyle(contentStyle);
			
			objCell = objRow.createCell(1);

			objCell.setCellValue((String) map.get("ftrCde"));
			objCell.setCellStyle(contentStyle);
			
			
			dData = (BigDecimal) map.get("ftrIdn");
			objCell = objRow.createCell(2);
			if(dData == null){
   				objCell.setCellValue(0);	
   			}else{
   				objCell.setCellValue(dData.intValue());		
   			}	
			objCell.setCellStyle(contentStyle);
			
			objCell = objRow.createCell(3);
			objCell.setCellValue((String) map.get("hjdCdeNam"));
			objCell.setCellStyle(contentStyle);
			
			objCell = objRow.createCell(4);
			objCell.setCellValue((String) map.get("shtNum"));
			objCell.setCellStyle(contentStyle);
			
			objCell = objRow.createCell(5);
			objCell.setCellValue((String) map.get("mngCdeNam"));
			objCell.setCellStyle(contentStyle);
			
			objCell = objRow.createCell(6);	
			objCell.setCellValue((String) map.get("istYmdFmt"));
			objCell.setCellStyle(contentStyle);
			
			objCell = objRow.createCell(7);
			objCell.setCellValue((String) map.get("saaCdeNam"));
			objCell.setCellStyle(contentStyle);
			
			objCell = objRow.createCell(8);
			objCell.setCellValue((String) map.get("mopCdeNam"));
			objCell.setCellStyle(contentStyle);						
		}
		
		for (i=0; i < 9; i++) {
			objSheet.autoSizeColumn((short)i);
		}
		
		response.setContentType("Application/Msexcel");
		response.setHeader("Content-Disposition", "ATTachment; Filename="+URLEncoder.encode("상수관로","UTF-8") + "_" + format1.format(today) +".xls");
		fileOut  = response.getOutputStream(); 
		objWorkBook.write(fileOut);
		fileOut.close();
		response.getOutputStream().flush();
		response.getOutputStream().close();
	}
}
