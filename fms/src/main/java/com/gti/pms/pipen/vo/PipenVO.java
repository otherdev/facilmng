package com.gti.pms.pipen.vo;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * @Class Name : WtlPipeLmDefaultVO.java
 * @Description : WtlPipeLm Default VO class
 * @Modification Information
 *
 * @author DRCTS
 * @since 2018-07-01
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */
public class PipenVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public static final String GRP_TYPE_CONS_DRAWING = "111";
    public static final String GRP_TYPE_CONS_REFERENCE = "112";
    public static final String GRP_TYPE_CONS_PICTURE = "121";
	
	/** 검색조건 */
    private String searchCondition1 = "";
    private String searchCondition2 = "";
    private String searchCondition3 = "";
    private String searchCondition4 = "";
    private String searchCondition5 = "";
    private String searchCondition6 = "";
    private String searchCondition7 = "";
    private String searchCondition8 = "";
    private String searchCondition9 = "";
    private String searchCondition10 = "";
    private String searchCondition11 = "";
            
    /** 각 테이블명 변수*/
    private String tblNm = "";    
    private String geomYN = "";
    
    private String ftrCde;
    private Integer ftrIdn;
    
    public String getGeomYN() {
		return geomYN;
	}

	public void setGeomYN(String geomYN) {
		this.geomYN = geomYN;
	}
	
    public String getSearchCondition1() {
		return searchCondition1;
	}

	public void setSearchCondition1(String searchCondition1) {
		this.searchCondition1 = searchCondition1;
	}

	public String getSearchCondition2() {
		return searchCondition2;
	}

	public void setSearchCondition2(String searchCondition2) {
		this.searchCondition2 = searchCondition2;
	}

	public String getSearchCondition3() {
		return searchCondition3;
	}

	public void setSearchCondition3(String searchCondition3) {
		this.searchCondition3 = searchCondition3;
	}

	public String getSearchCondition4() {
		return searchCondition4;
	}

	public void setSearchCondition4(String searchCondition4) {
		this.searchCondition4 = searchCondition4;
	}

	public String getSearchCondition5() {
		return searchCondition5;
	}

	public void setSearchCondition5(String searchCondition5) {
		this.searchCondition5 = searchCondition5;
	}

	public String getSearchCondition6() {
		return searchCondition6;
	}

	public void setSearchCondition6(String searchCondition6) {
		this.searchCondition6 = searchCondition6;
	}

	public String getSearchCondition7() {
		return searchCondition7;
	}

	public void setSearchCondition7(String searchCondition7) {
		this.searchCondition7 = searchCondition7;
	}

	public String getSearchCondition8() {
		return searchCondition8;
	}

	public void setSearchCondition8(String searchCondition8) {
		this.searchCondition8 = searchCondition8;
	}

	public String getSearchCondition9() {
		return searchCondition9;
	}

	public void setSearchCondition9(String searchCondition9) {
		this.searchCondition9 = searchCondition9;
	}

	public String getSearchCondition10() {
		return searchCondition10;
	}

	public void setSearchCondition10(String searchCondition10) {
		this.searchCondition10 = searchCondition10;
	}

	public String getSearchCondition11() {
		return searchCondition11;
	}

	public void setSearchCondition11(String searchCondition11) {
		this.searchCondition11 = searchCondition11;
	}

	/** 검색Keyword */
    private String searchKeyword = "";
    
    /** 검색사용여부 */
    private String searchUseYn = "";
    
    /** 현재페이지 */
    private int pageIndex = 1;
    
    /** 페이지갯수 */
    private int pageUnit = 10;
    
    /** 페이지사이즈 */
    private int pageSize = 10;

    /** firstIndex */
    private int firstIndex = 1;

    /** lastIndex */
    private int lastIndex = 1;

    /** recordCountPerPage */
    private int recordCountPerPage = 10;
    
    /**
     * 관리기관 분류 코드
     */
    private String typeCode;
    
        
	public int getFirstIndex() {
		return firstIndex;
	}

	public void setFirstIndex(int firstIndex) {
		this.firstIndex = firstIndex;
	}

	public int getLastIndex() {
		return lastIndex;
	}

	public void setLastIndex(int lastIndex) {
		this.lastIndex = lastIndex;
	}

	public int getRecordCountPerPage() {
		return recordCountPerPage;
	}

	public void setRecordCountPerPage(int recordCountPerPage) {
		this.recordCountPerPage = recordCountPerPage;
	}

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    public String getSearchUseYn() {
        return searchUseYn;
    }

    public void setSearchUseYn(String searchUseYn) {
        this.searchUseYn = searchUseYn;
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public int getPageUnit() {
        return pageUnit;
    }

    public void setPageUnit(int pageUnit) {
        this.pageUnit = pageUnit;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

	public String getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}

	public String getTblNm() {
		return tblNm;
	}

	public void setTblNm(String tblNm) {
		this.tblNm = tblNm;
	}

	public String getFtrCde() {
		return ftrCde;
	}

	public void setFtrCde(String ftrCde) {
		this.ftrCde = ftrCde;
	}

	public Integer getFtrIdn() {
		return ftrIdn;
	}

	public void setFtrIdn(Integer ftrIdn) {
		this.ftrIdn = ftrIdn;
	}
}
