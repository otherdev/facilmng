package com.gti.pms.stats.web;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gti.pms.cmm.service.CmmService;
import com.gti.pms.cmm.web.CodeController;

import egovframework.let.utl.fcc.service.EgovStringUtil;
import egovframework.rte.fdl.property.EgovPropertyService;

@Controller
@RequestMapping("/pms/stats")
public class StatsController extends CodeController{
    
	private static final Logger logger = LoggerFactory.getLogger(StatsController.class);

    @Resource(name = "cmmService")
    private CmmService cmmService;
	
    /** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;
	 		
    @RequestMapping(value="/statCmplList.do")
    public String statWserList(
    		HttpServletRequest request,
    		HttpServletResponse response, 
    		Model model) throws Exception {

		SimpleDateFormat sdfY = new SimpleDateFormat("yyyy");
    	String sYear = sdfY.format(new Date());
    	
    	Map<String, Object> pMap = new HashMap<String, Object>();
    	Map<String, Object> map = new HashMap<String, Object>();
		    	
		List<Map<String, Object>> repList = (List<Map<String, Object>>) cmmService.selectList("selectStatsByMonth", "");
		
		HashMap<String, String> repMap01 = new HashMap<String, String>();
		HashMap<String, String> repMap02 = new HashMap<String, String>();
		HashMap<String, String> repMap03 = new HashMap<String, String>();
		HashMap<String, String> repMap04 = new HashMap<String, String>();
		HashMap<String, String> repMap05 = new HashMap<String, String>();
		HashMap<String, String> repMap06 = new HashMap<String, String>();
		HashMap<String, String> repMap07 = new HashMap<String, String>();
		HashMap<String, String> repMap08 = new HashMap<String, String>();
		HashMap<String, String> repMap09 = new HashMap<String, String>();
		HashMap<String, String> repMap10 = new HashMap<String, String>();
		HashMap<String, String> repMap11 = new HashMap<String, String>();
		HashMap<String, String> repMap12 = new HashMap<String, String>();
		
		pMap = new HashMap<String, Object>();
		pMap.put("searchKeyword", sYear + "01");		
		List<Map<String, Object>> repList1 = (List<Map<String, Object>>) cmmService.selectList("selectStatsByMonth", pMap);
		for (int i=0;i<repList1.size();i++) {
			map = repList1.get(i);
			repMap01.put("cde",EgovStringUtil.isNullToString(map.get("rowCnt")));
		}
		
		pMap = new HashMap<String, Object>();
		pMap.put("searchKeyword", sYear + "02");		
		List<Map<String, Object>> repList2 = (List<Map<String, Object>>) cmmService.selectList("selectStatsByMonth", pMap);
		for (int i=0;i<repList2.size();i++) {
			map = repList2.get(i);
			repMap02.put("cde",EgovStringUtil.isNullToString(map.get("rowCnt")));
		}
		
		pMap = new HashMap<String, Object>();
		pMap.put("searchKeyword", sYear + "03");		
		List<Map<String, Object>> repList3 = (List<Map<String, Object>>) cmmService.selectList("selectStatsByMonth", pMap);
		for (int i=0;i<repList3.size();i++) {
			map = repList3.get(i);
			repMap03.put("cde",EgovStringUtil.isNullToString(map.get("rowCnt")));
		}
		
		pMap = new HashMap<String, Object>();
		pMap.put("searchKeyword", sYear + "04");		
		List<Map<String, Object>> repList4 = (List<Map<String, Object>>) cmmService.selectList("selectStatsByMonth", pMap);
		for (int i=0;i<repList4.size();i++) {
			map = repList4.get(i);
			repMap04.put("cde",EgovStringUtil.isNullToString(map.get("rowCnt")));
		}
		
		pMap = new HashMap<String, Object>();
		pMap.put("searchKeyword", sYear + "05");		
		List<Map<String, Object>> repList5 = (List<Map<String, Object>>) cmmService.selectList("selectStatsByMonth", pMap);
		for (int i=0;i<repList5.size();i++) {
			map = repList5.get(i);
			repMap05.put("cde",EgovStringUtil.isNullToString(map.get("rowCnt")));
		}
		
		pMap = new HashMap<String, Object>();
		pMap.put("searchKeyword", sYear + "06");		
		List<Map<String, Object>> repList6 = (List<Map<String, Object>>) cmmService.selectList("selectStatsByMonth", pMap);
		for (int i=0;i<repList6.size();i++) {
			map = repList6.get(i);
			repMap06.put("cde",EgovStringUtil.isNullToString(map.get("rowCnt")));
		}
		
		pMap = new HashMap<String, Object>();
		pMap.put("searchKeyword", sYear + "07");		
		List<Map<String, Object>> repList7 = (List<Map<String, Object>>) cmmService.selectList("selectStatsByMonth", pMap);
		for (int i=0;i<repList7.size();i++) {
			map = repList7.get(i);
			repMap07.put("cde",EgovStringUtil.isNullToString(map.get("rowCnt")));
		}
		
		pMap = new HashMap<String, Object>();
		pMap.put("searchKeyword", sYear + "08");		
		List<Map<String, Object>> repList8 = (List<Map<String, Object>>) cmmService.selectList("selectStatsByMonth", pMap);
		for (int i=0;i<repList8.size();i++) {
			map = repList8.get(i);
			repMap08.put("cde",EgovStringUtil.isNullToString(map.get("rowCnt")));
		}
		
		pMap = new HashMap<String, Object>();
		pMap.put("searchKeyword", sYear + "09");		
		List<Map<String, Object>> repList9 = (List<Map<String, Object>>) cmmService.selectList("selectStatsByMonth", pMap);
		for (int i=0;i<repList9.size();i++) {
			map = repList9.get(i);
			repMap09.put("cde",EgovStringUtil.isNullToString(map.get("rowCnt")));
		}
		
		pMap = new HashMap<String, Object>();
		pMap.put("searchKeyword", sYear + "10");		
		List<Map<String, Object>> repList10 = (List<Map<String, Object>>) cmmService.selectList("selectStatsByMonth", pMap);
		for (int i=0;i<repList10.size();i++) {
			map = repList10.get(i);
			repMap10.put("cde",EgovStringUtil.isNullToString(map.get("rowCnt")));
		}
		
		pMap = new HashMap<String, Object>();
		pMap.put("searchKeyword", sYear + "11");		
		List<Map<String, Object>> repList11 = (List<Map<String, Object>>) cmmService.selectList("selectStatsByMonth", pMap);
		for (int i=0;i<repList11.size();i++) {
			map = repList11.get(i);
			repMap11.put("cde",EgovStringUtil.isNullToString(map.get("rowCnt")));
		}
		
		pMap = new HashMap<String, Object>();
		pMap.put("searchKeyword", sYear + "12");		
		List<Map<String, Object>> repList12 = (List<Map<String, Object>>) cmmService.selectList("selectStatsByMonth", pMap);
		for (int i=0;i<repList12.size();i++) {
			map = repList12.get(i);
			repMap12.put("cde",EgovStringUtil.isNullToString(map.get("rowCnt")));
		}
		
		
		model.addAttribute("repList", repList);
		model.addAttribute("repMap01", repMap01);
		model.addAttribute("repMap01", repMap01);
		model.addAttribute("repMap02", repMap02);
		model.addAttribute("repMap03", repMap03);
		model.addAttribute("repMap04", repMap04);
		model.addAttribute("repMap05", repMap05);
		model.addAttribute("repMap06", repMap06);
		model.addAttribute("repMap07", repMap07);
		model.addAttribute("repMap08", repMap08);
		model.addAttribute("repMap09", repMap09);
		model.addAttribute("repMap10", repMap10);
		model.addAttribute("repMap11", repMap11);
		model.addAttribute("repMap12", repMap12);
		
        return "pms/stats/statCmplList";
    } 
        
    @RequestMapping(value="/fcltStatsList.do")
    public String statFaciList(
    		HttpServletRequest request,
    		HttpServletResponse response, 
    		Model model) throws Exception {
    	
    	String day = EgovStringUtil.isNullToString( request.getParameter("day") );
    	String sDay = "";
    	if(!day.equals("")) {
    		sDay = day.replaceAll("-", "");
    	}else {
    		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    		sDay = sdf.format(new Date());
    	}
    	
    	SimpleDateFormat sdfY = new SimpleDateFormat("yyyy");
    	String sYear = sdfY.format(new Date());
    	Integer iYear = Integer.valueOf(sYear);
    	
    	Map<String, Object> pMap = new HashMap<String, Object>();
    					
		pMap.put("searchKeyword", sDay);
		List<Map<String, Object>> facList0 = (List<Map<String, Object>>) cmmService.selectList("selectStatsByMonth", pMap);
		
		pMap.put("searchKeyword", iYear.toString() );
		List<Map<String, Object>> facList1 = (List<Map<String, Object>>) cmmService.selectList("selectStatsByMonth", pMap);
		
		pMap.put("searchKeyword", iYear.intValue()-1 );
		List<Map<String, Object>> facList2 = (List<Map<String, Object>>) cmmService.selectList("selectStatsByMonth", pMap);
		
		pMap.put("searchKeyword", iYear.intValue()-2 );
		List<Map<String, Object>> facList3 = (List<Map<String, Object>>) cmmService.selectList("selectStatsByMonth", pMap);
		
		pMap.put("searchKeyword", iYear.intValue()-3 );
		List<Map<String, Object>> facList4 = (List<Map<String, Object>>) cmmService.selectList("selectStatsByMonth", pMap);
		
		pMap.put("searchKeyword", iYear.intValue()-4 );
		List<Map<String, Object>> facList5 = (List<Map<String, Object>>) cmmService.selectList("selectStatsByMonth", pMap);
				
		model.addAttribute("facList0", facList0);
		model.addAttribute("facList1", facList1);
		model.addAttribute("facList2", facList2);
		model.addAttribute("facList3", facList3);
		model.addAttribute("facList4", facList4);
		model.addAttribute("facList5", facList5);
    	
    	return "pms/stats/fcltStatsList";
    } 
    
}