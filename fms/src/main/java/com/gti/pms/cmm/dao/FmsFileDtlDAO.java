package com.gti.pms.cmm.dao;

import org.springframework.stereotype.Repository;

import com.gti.pms.cmm.service.FmsFileDtlVO;

import egovframework.rte.psl.dataaccess.EgovAbstractDAO;

/**
 * @Class Name : WttFileHtDAO.java
 * @Description : WttFileHt DAO Class
 * @Modification Information
 *
 * @author DRCTS
 * @since 2018-07-01
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Repository("fmsFileDtlDAO")
public class FmsFileDtlDAO extends EgovAbstractDAO {

	
    
    
        
    public Integer insertFileDtl(FmsFileDtlVO vo) throws Exception {
        return (Integer)insert("insertFileDtl", vo);
    }
    
}
