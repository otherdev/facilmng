package com.gti.pms.cmm.web;

import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gti.pms.cmm.service.CmmService;
import com.gti.pms.cmm.service.LinkVO;

/**
 * 사업관리 
 * @author 공통서비스 개발팀 
 * @see
 *
 */
@Controller
public class ExcelController {


	/** cmmService */
	@Resource(name = "cmmService")
	private CmmService cmmService;



	
	/**
	 * 엑셀다운로드 공통
	 * @param vo
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping(value="/pms/excel/download.do")
	public void getPipenMngListExel(@ModelAttribute LinkVO vo, HttpServletResponse response , Model model) throws Exception {

		HSSFWorkbook objWorkBook = new HSSFWorkbook();
		HSSFSheet objSheet;
		HSSFRow objRow;
		HSSFCell objCell;
		OutputStream fileOut = null;
		
		Map<String, Object> map = new HashMap<String, Object>();
		BigDecimal dData = new BigDecimal(0);
		
		/**
		 * 서식정보 관련설정
		 */
		/*제목폰트*/
		HSSFFont font = objWorkBook.createFont();
		font.setFontHeightInPoints((short)20);
		font.setBoldweight((short)font.BOLDWEIGHT_BOLD);
		font.setUnderline(HSSFFont.U_SINGLE_ACCOUNTING);
		font.setFontName("고딕");
		/*테이블 헤더 폰트*/
		HSSFFont headerFont = objWorkBook.createFont();
		headerFont.setFontHeightInPoints((short)10);
		headerFont.setBoldweight((short)font.BOLDWEIGHT_BOLD);
		headerFont.setFontName("고딕");
		/*테이블 내용 폰트*/
		HSSFFont contentFont = objWorkBook.createFont();
		contentFont.setFontHeightInPoints((short)9);
		contentFont.setBoldweight((short)font.BOLDWEIGHT_NORMAL);
		contentFont.setFontName("고딕");

		
		
		
		/*제목스타일*/
		HSSFCellStyle titleStyle = objWorkBook.createCellStyle();
		titleStyle.setFont(font);
		titleStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		titleStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		
		/*타이틀스타일*/
		HSSFCellStyle tableHeaderStyle = objWorkBook.createCellStyle();
		tableHeaderStyle.setFont(headerFont);
		tableHeaderStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		tableHeaderStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		tableHeaderStyle.setBorderTop((short)1);
		tableHeaderStyle.setBorderBottom((short)1);
		tableHeaderStyle.setFillForegroundColor(HSSFColor.LIME.index);
		tableHeaderStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

		/*가운데스타일*/
		HSSFCellStyle contentStyle = objWorkBook.createCellStyle();
		contentStyle.setFont(contentFont);
		contentStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		contentStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

		/*왼쪽 정렬스타일*/
		HSSFCellStyle contentLeftStyle = objWorkBook.createCellStyle();
		contentLeftStyle.setFont(contentFont);
		contentLeftStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
		contentLeftStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		
		/*오른쪽 정렬스타일*/
		HSSFCellStyle contentRightStyle = objWorkBook.createCellStyle();
		contentRightStyle.setFont(contentFont);
		contentRightStyle.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
		contentRightStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);


		
		try {
		
			/*날짜서식*/
			Date today = new Date();
			SimpleDateFormat format1 = new SimpleDateFormat("yyyyMMddHHmmssSSS");
			SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
	
			
			
			
			
			/**
			 * 엑셀작성
			 */
			
			// 0.시트명
			objSheet = objWorkBook.createSheet(vo.getTitle());
			
			// 0.제목 행
			objRow = objSheet.createRow(0);
			objRow.setHeight ((short) 0x250);
			objCell = objRow.createCell(0);
			objCell.setCellValue(vo.getTitle());
			objCell.setCellStyle(titleStyle);
			int size = 0;
			for(String s : vo.getAryTit()){
				if(s ==  null || "".equals(s))	continue;
				size++;
			}
			objSheet.addMergedRegion(new CellRangeAddress(0,0,0,size));  
	 
			// 0.날짜 행
			objRow = objSheet.createRow(2);
			objRow.setHeight ((short) 0x150);
			objCell = objRow.createCell(0);
			objCell.setCellValue(format2.format(today));		
			objCell.setCellStyle(contentStyle);
	
			
			
			// 1.헤더  작성
			objRow = objSheet.createRow(3);
			objRow.setHeight ((short) 0x150);
			
			String[] aryTitle = vo.getAryTitle();//제목배열
			String[] aryTit = vo.getAryTit();//컬럼배열
			String[] aryTyp = vo.getAryTyp();//타입배열
			
			
			//no 표시
			objCell = objRow.createCell(0);
			objCell.setCellValue("순번");		
			objCell.setCellStyle(tableHeaderStyle);
			int colIdx = 1;
			for(int i=0; i<aryTit.length; i++ ){
				if(aryTit[i] == null || "".equals(aryTit[i])) continue;
				
				objCell = objRow.createCell(colIdx);
				objCell.setCellValue(aryTitle[i]);
				objCell.setCellStyle(tableHeaderStyle);
				colIdx++;
			}
			
			
			
			// 2.데이터행  작성
			vo.setFirstIndex(0);
			vo.setLastIndex(1000000);
			List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList(vo.getSqlId(), vo);
	   		
	   		int listSize = 0;
			if( list.size()>10000) {
				listSize = 10000;
			} else {
				listSize = list.size();
			}
			
			
			int i = 0; 
			
			for( i=0; i<listSize; i++ ) { 			
				map = list.get(i);
				
				objRow = objSheet.createRow(4+i);
				objRow.setHeight ((short) 0x150);
				
				//no 표시
				objCell = objRow.createCell(0);
				objCell.setCellValue(i+1);		
				objCell.setCellStyle(contentStyle);
				
				//컬럼별 데이터표시
				colIdx = 1;
				for(int j=0; j<aryTit.length; j++){
					if(aryTit[j] == null || "".equals(aryTit[j])) continue;
	
					objCell = objRow.createCell(colIdx);//셀만들고
					
					//숫자형이면 오른정렬
					if("number".equals(aryTyp[j])){
						objCell.setCellStyle(contentRightStyle);
						try{
							dData = (BigDecimal) map.get(aryTit[j]);
							if(dData == null){
								objCell.setCellValue(0);	
							}else{
								objCell.setCellValue(dData.doubleValue());		
							}	
						}catch(Exception e){					
							objCell.setCellValue(0);	
						}
					}
					//문자형이면 왼쪽정렬
					else if("text".equals(aryTyp[j])){
						objCell.setCellStyle(contentLeftStyle);
						objCell.setCellValue((String)map.get(aryTit[j]));	
					}
					//기본 중앙정렬
					else{
						objCell.setCellStyle(contentStyle);
						try{
							dData = (BigDecimal) map.get(aryTit[j]);
							objCell.setCellValue(dData.intValue());		
						}catch(Exception e){					
							objCell.setCellValue((String)map.get(aryTit[j]));	
						}
					}
					
					colIdx++;
				}
				
			}
			
			
			
			for (i=0; i < aryTit.length; i++) {
				objSheet.autoSizeColumn((short)i);
			}
			
			response.setContentType("Application/Msexcel");
			response.setHeader("Set-Cookie", "fileDownload=true; path=/");
			response.setHeader("Content-Disposition", "ATTachment; Filename="+URLEncoder.encode(vo.getTitle(),"UTF-8") + "_" + format1.format(today) +".xls");
			fileOut  = response.getOutputStream(); 
			objWorkBook.write(fileOut);
		} catch(Exception e) {
			
			response.setHeader("Set-Cookie", "fileDownload=false; path=/");
			response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
			response.setHeader("Content-Type","text/html; charset=utf-8");
			
			OutputStream out = null;
	        try {
	            out = response.getOutputStream();
	            byte[] data = new String("fail..").getBytes();
	            out.write(data, 0, data.length);
	        } catch(Exception ignore) {
	            ignore.printStackTrace();
	        } finally {
	            if(out != null) try { out.close(); } catch(Exception ignore) {}
	        }
			
		} finally {
      
			fileOut.close();
			response.getOutputStream().flush();
			response.getOutputStream().close();
		}
	}
	
	
	
}
