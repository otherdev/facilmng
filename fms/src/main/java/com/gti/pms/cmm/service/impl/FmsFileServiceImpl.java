package com.gti.pms.cmm.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.gti.pms.cmm.dao.FmsFileDAO;
import com.gti.pms.cmm.dao.FmsFileDtlDAO;
import com.gti.pms.cmm.service.CmmService;
import com.gti.pms.cmm.service.FmsFileDtlVO;
import com.gti.pms.cmm.service.FmsFileMstVO;
import com.gti.pms.cmm.service.FmsFileService;

import egovframework.com.cmm.SessionUtil;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;

/**
 * @Class Name : WttFlgpMaServiceImpl.java
 * @Description : WttFlgpMa Business Implement class
 * @Modification Information
 *
 * @author DRCTS
 * @since 2018-07-01
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Service("fmsFileService")
public class FmsFileServiceImpl extends EgovAbstractServiceImpl implements FmsFileService {
        
    private static final Logger LOGGER = LoggerFactory.getLogger(FmsFileServiceImpl.class);


    
    @Resource(name="fmsFileDAO")
    private FmsFileDAO fmsFileDAO;
    
    @Resource(name="fmsFileDtlDAO")
    private FmsFileDtlDAO fmsFileDtlDAO;

    @Resource(name="cmmService")
    private CmmService cmmService;

    
    /** ID Generation */
    //@Resource(name="{egovWttFlgpMaIdGnrService}")    
    //private EgovIdGnrService egovIdGnrService;

    
    
    
    /**
	 * 파이버전(파일마스터), 파일상세 등록
	 * @param vo - 등록할 정보가 담긴 WttFlgpMaVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    public String saveFmsFile(FmsFileMstVO fmst, List<FmsFileDtlVO> fdtlList) throws Exception {
    	
    	int filSeq = 0;
    	
    	//버전(파일마스터)저장
		try{
			fmst.setCreUsr(SessionUtil.getId());
		}catch(Exception e){
			fmst.setCreUsr("nosession");
		}


		
		//1.파일마스터 추가
		if(fmst.getFilSeq()== null){
			
			//파일마스터 추가
			filSeq = fmsFileDAO.insertFileMst(fmst);
		}
		else{
			//파일마스터 수정
			filSeq = fmst.getFilSeq(); 
			fmsFileDAO.updateFileMst(fmst);
		}
		
    	
    	
		
		//1.파일 상세 추가 - 무조건추가(삭제는 화면에서 개별로..)
    	for (FmsFileDtlVO fdtl : fdtlList) {
    		fdtl.setFilSeq(filSeq);
    		try{
    			fdtl.setCreUsr(SessionUtil.getId());
    		}catch(Exception e){
    			fdtl.setCreUsr("nosession");
    		}
        	
    		fmsFileDtlDAO.insertFileDtl(fdtl);
    	}
    	
    	
    	return String.valueOf(filSeq);
    }    
    
    
}
