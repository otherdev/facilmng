package com.gti.pms.cmm.web;


import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.gti.pms.cmm.service.CmmService;
import com.gti.pms.cmm.service.FmsFileDtlVO;
import com.gti.pms.cmm.service.FmsFileMstVO;
import com.gti.pms.cmm.service.FmsFileService;
import com.gti.pms.util.FileUtil;

import egovframework.rte.fdl.property.EgovPropertyService;


@Controller
@RequestMapping("/pms/file")
public class FmsFileController {

	@Resource(name = "fmsFileService")
	private FmsFileService fmsFileService;

	@Resource(name = "propertiesService")
	private EgovPropertyService propertiesService;
	

	@Resource(name = "cmmService")
	private CmmService cmmService;
	
	
		

	
	
	


	

	
	
	
	
	
	/**
	 * 첨부파일 화면
	 * @param bizId
	 * @param filSeq
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/lnkFileDtl.do")
	public String lnkFileDtl(
			@RequestParam(value="filSeq", required=false) String filSeq,
			@RequestParam(value="bizId", required=false) String bizId,
			@RequestParam(value="grpTyp", required=false) String grpTyp,
			@RequestParam(value="smode", required=false) String smode,
			Model model) throws Exception {
		
		
		//0.업무파일정보
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("filSeq", filSeq);
		param.put("bizId", bizId);
		param.put("grpTyp", grpTyp);
		List<Map<String,Object>> lst0 = (List<Map<String, Object>>) cmmService.selectList("selectFileMap", param);
		if(lst0 != null && !lst0.isEmpty()){
			model.addAttribute("titNam", lst0.get(0).get("titNam"));
			model.addAttribute("ctnt", lst0.get(0).get("ctnt"));
		}
		
		
		//1.파일마스터 정보
		List<Map<String,Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectFileMst", param);
		if(lst != null && !lst.isEmpty()){
			model.addAttribute("filNm", lst.get(0).get("filNm"));
		}
		
		
		//2.파일상세리스트
		List<Map<String,Object>> fList = (List<Map<String, Object>>) cmmService.selectList("selectFileDtlList", param);
		model.addAttribute("fList", fList);
		
		
		//파라미터 화면으로전달
		model.addAttribute("bizId", bizId);
		model.addAttribute("filSeq", filSeq);
		model.addAttribute("grpTyp", grpTyp);
		model.addAttribute("smode", smode);
		
		return "pms/link/lnkFileDtl";
	}
	
	
	/**
	 * 첨부파일 화면
	 * @param bizId
	 * @param filSeq
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/lnkImgFileDtl.do")
	public String lnkImgFileDtl(
			@RequestParam(value="filSeq", required=false) String filSeq,
			@RequestParam(value="bizId", required=false) String bizId,
			@RequestParam(value="grpTyp", required=false) String grpTyp,
			@RequestParam(value="smode", required=false) String smode,
			Model model) throws Exception {
		
		
		//0.업무파일정보
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("filSeq", filSeq);
		param.put("bizId", bizId);
		param.put("grpTyp", grpTyp);
		List<Map<String,Object>> lst0 = (List<Map<String, Object>>) cmmService.selectList("selectFileMap", param);
		if(lst0 != null && !lst0.isEmpty()){
			model.addAttribute("titNam", lst0.get(0).get("titNam"));
			model.addAttribute("ctnt", lst0.get(0).get("ctnt"));
		}
		
		
		//1.파일마스터 정보
		List<Map<String,Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectFileMst", param);
		if(lst != null && !lst.isEmpty()){
			model.addAttribute("filNm", lst.get(0).get("filNm"));
		}
		
		
		//2.파일상세리스트
		List<Map<String,Object>> fList = (List<Map<String, Object>>) cmmService.selectList("selectFileDtlList", param);
		model.addAttribute("fList", fList);
		
		
		//파라미터 화면으로전달
		model.addAttribute("bizId", bizId);
		model.addAttribute("filSeq", filSeq);
		model.addAttribute("grpTyp", grpTyp);
		model.addAttribute("smode", smode);
		
		return "pms/link/lnkImgFileDtl";
	}
	
	
		
	
	
	/**
	 * 파일 다운로드
	 * 
	 * @param wttFileHt
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/downloadFile.do")
	public void download(HttpServletRequest request, HttpServletResponse response, String filSeq, String seq) throws Exception {
		
		
		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("filSeq", filSeq);
		param.put("seq", seq);
		Map<String, Object> fMap = (Map<String, Object>) cmmService.select("selectFileDtl", param);
		
		File uFile = null;
		String url = propertiesService.getString("fms.file.url");
    	
    	if (url != null && !url.equals("")) {
    		//Server
    		uFile = new File(new URI(url));
    	} else {
    		//Local
    		String path = propertiesService.getString("fms.file.path.local");
    		
    		uFile = new File(path, (String)fMap.get("upfNam"));
    	}

		long fSize = uFile.length();

		if (fSize > 0) {
			String mimetype = "application/x-msdownload";

			//response.setBufferSize(fSize);	// OutOfMemeory 발생
			response.setContentType(mimetype);
			//response.setHeader("Content-Disposition", "attachment; filename=\"" + URLEncoder.encode(fvo.getOrignlFileNm(), "utf-8") + "\"");
			FileUtil.setDisposition(fMap.get("dwnNam").toString(), request, response);
			//response.setContentLength(fSize);

			BufferedInputStream in = null;
			BufferedOutputStream out = null;

			try {
				in = new BufferedInputStream(new FileInputStream(uFile));
				out = new BufferedOutputStream(response.getOutputStream());

				FileCopyUtils.copy(in, out);
				out.flush();
			} catch (Exception ex) {
				ex.printStackTrace();
			} finally {
				if (in != null) {
					try {
						in.close();
					} catch (Exception ignore) {
					}
				}
				if (out != null) {
					try {
						out.close();
					} catch (Exception ignore) {
					}
				}
			}

		} else {
			
		}
	}
	
	/**
	 * 이미지 다운로드 URL
	 * 
	 * @param response
	 * @param wttFileHt
	 * @throws Exception
	 */
	@RequestMapping("/downloadImg.do")
	public void downloadImg(HttpServletResponse response, String filSeq, String seq) 
			throws Exception {
		
		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("filSeq", filSeq);
		param.put("seq", seq);
		Map<String, Object> fMap = (Map<String, Object>) cmmService.select("selectFileDtl", param);
		
		
		File file = null;
		String url = propertiesService.getString("fms.file.url");
    	
    	if (url != null && !url.equals("")) {
    		//Server
    		file = new File(new URI(url));
    	} else {
    		//Local
    		String path = propertiesService.getString("fms.file.path.local");
    		
    		file = new File(path, fMap.get("upfNam").toString());
    	}
    	
		FileInputStream fis = null;

		BufferedInputStream in = null;
		ByteArrayOutputStream bStream = null;
		try{
			fis = new FileInputStream(file);
			in = new BufferedInputStream(fis);
			bStream = new ByteArrayOutputStream();
			int imgByte;
			while ((imgByte = in.read()) != -1) {
			    bStream.write(imgByte);
			}

			String contentType = "";

		    if ("jpg".equals(FileUtil.getExt(fMap.get("upfNam").toString()))) {
		    	contentType = "image/jpeg";
		    } else {
		    	contentType = "image/" + FileUtil.getExt(fMap.get("upfNam").toString());
		    }

			response.setHeader("Content-Type", contentType);
			response.setContentLength(bStream.size());

			bStream.writeTo(response.getOutputStream());

			response.getOutputStream().flush();
			response.getOutputStream().close();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if (bStream != null) {
				try {
					bStream.close();
				} catch (Exception est) {
					est.printStackTrace();
				}
			}
			if (in != null) {
				try {
					in.close();
				} catch (Exception ei) {
					ei.printStackTrace();
				}
			}
			if (fis != null) {
				try {
					fis.close();
				} catch (Exception efis) {
					efis.printStackTrace();
				}
			}
		}
	}


	/**
	 * 첨부파일 등록
	 * 
	 * @param request
	 * @param wttFlgpMa
	 * @param wttFverMa
	 * @param type
	 * @param files
	 * @param model
	 * @return
	 */
	@RequestMapping("/fileSave.do")
	public String fmsFileSave(HttpServletRequest request,
			@RequestParam(value="filSeq", required=false) String filSeq,
			@RequestParam(value="filNm", required=false) String filNm,
			@RequestParam(value="titNam", required=false) String titNam,
			@RequestParam(value="ctnt", required=false) String ctnt,
			@RequestParam(value="bizId", required=false) String bizId,
			@RequestParam(value="grpTyp", required=false) String grpTyp,
			@RequestPart("files") List<MultipartFile> files,
			Model model) {
		
		
		// 화면모드
		String smode = "dtl"; //저장모드
		if(filSeq == null || "".equals(filSeq)){
			smode = "add";
		}
		
		// 전달받은 파일아이디
		FmsFileMstVO fmst = new FmsFileMstVO();
		try{
			fmst.setFilSeq(Integer.valueOf(filSeq));
		}catch(Exception e){}
		fmst.setFilNm(filNm);;
		
		
		try {
			
			
			// 0.멀티파트파일 서버에 업로드
			List<FmsFileDtlVO> fdtlList = new ArrayList<>();
			for (MultipartFile file : files) {
				//파일 저장
		    	String uploadName = FileUtil.upload(request, file);
		    	
		    	if (uploadName == null) {
		    		//저장 실패
		    		throw new IOException("파일 저장 실패");
		    	}
		    	
		    	Map<String, Integer> map = FileUtil.getWidthHeight(file);
		    	
		    	FmsFileDtlVO fdtl = new FmsFileDtlVO();
		    	fdtl.setDwnNam(file.getOriginalFilename());
		    	fdtl.setUpfNam(uploadName);
		    	fdtl.setFilTyp(FileUtil.getExt(uploadName));
		    	fdtl.setFilSiz((file.getSize() / 1000) + "");
		    	fdtl.setFilRst(map.get("width") + " X " + map.get("height"));
		    	
		    	fdtlList.add(fdtl);
			}
			
			
			
			// 1.첨부파일저장 서비스
			filSeq = fmsFileService.saveFmsFile(fmst, fdtlList);
			
			
			
			// 2.업무파일 매핑처리
			Map<String, Object> param = new HashMap<String, Object>();
	    	param.put("sqlId", "saveFileMap");
	    	Map<String,Object> data = new HashMap<String, Object>();
	    	data.put("bizId", bizId);
	    	data.put("grpTyp", grpTyp);
	    	data.put("filSeq", filSeq);
	    	data.put("titNam", titNam);
	    	data.put("ctnt", ctnt);
	    	param.put("data", data);
			cmmService.saveData(param);
			
			
			
			
			
			//파일등록팝업 리다이렉트
			return "redirect:/pms/file/lnkFileDtl.do?filSeq="+filSeq+"&bizId="+bizId+"&grpTyp="+grpTyp+"&smode="+smode;
			
		} catch (Exception e) {
			
			String redirect = "redirect:/pms/file/lnkFileDtl.do?error=1";
			return redirect;
		}
	}
	
	
			
	
	
	
		
	
	
	
	
	
	
	
	
	
	
	
	
	/******************************************************************************************************
	 * 점검 첨부파일 화면
	 * @param type
	 * @param sclNum
	 * @param filSeq
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/lnkSclPhotoDtl.do")
	public String lnkSclPhotoDtl(
			@RequestParam(value="sclNum", required=false) String sclNum,
			@RequestParam(value="filSeq", required=false) String filSeq,
			@RequestParam(value="smode", required=false) String smode,
			Model model) throws Exception {
		

		Map<String,Object> param = new HashMap<String,Object>();
		param.put("filSeq", filSeq);
		
		//1.파일마스터 정보
		List<Map<String,Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectFileMst", param);
		if(lst != null && !lst.isEmpty()){
			model.addAttribute("filNm", lst.get(0).get("filNm"));
		}
		
		
		//2.파일상세리스트
		List<Map<String,Object>> fList = (List<Map<String, Object>>) cmmService.selectList("selectFileDtlList", param);
		model.addAttribute("fList", fList);
		
		
		
		//파라미터 화면으로전달
		model.addAttribute("sclNum", sclNum);
		model.addAttribute("filSeq", filSeq);
		model.addAttribute("smode", smode);
		
		return "pms/link/lnkSclPhotoDtl";
	}
	
	/******************************************************************************************************
	 * 점검 첨부파일 화면
	 * @param type
	 * @param sclNum
	 * @param filSeq
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/lnkSclPhotoView.do")
	public String lnkSclPhotoView(
			@RequestParam(value="sclNum", required=false) String sclNum,
			@RequestParam(value="filSeq", required=false) String filSeq,
			Model model) throws Exception {
		

		Map<String,Object> param = new HashMap<String,Object>();
		param.put("filSeq", filSeq);
		
		//1.파일마스터 정보
		List<Map<String,Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectFileMst", param);
		if(lst != null && !lst.isEmpty()){
			model.addAttribute("filNm", lst.get(0).get("filNm"));
		}
				
		//2.파일상세리스트
		List<Map<String,Object>> fList = (List<Map<String, Object>>) cmmService.selectList("selectFileDtlList", param);
		model.addAttribute("fList", fList);
		
		//파라미터 화면으로전달
		model.addAttribute("sclNum", sclNum);
		model.addAttribute("filSeq", filSeq);
	
		return "pms/link/lnkSclPhotoView";
	}
	
	
	
	
	/**
	 * 점검일정 파일그룹 등록
	 * 
	 * @param request
	 * @param wttFlgpMa
	 * @param wttFverMa
	 * @param type
	 * @param files
	 * @param model
	 * @return
	 */
	@RequestMapping("/sclFileSave.do")
	public String sclFileSave(HttpServletRequest request,
			@RequestParam("filSeq") String filSeq,
			@RequestParam("filNm") String filNm,
			@RequestPart("files") List<MultipartFile> files,
			@RequestParam(value="sclNum", required=false) String sclNum,
			Model model) {
		
		
		// 화면모드
		String smode = "dtl"; //저장모드
		if(filSeq == null || "".equals(filSeq)){
			smode = "add";
		}
		
		// 전달받은 파일아이디
		FmsFileMstVO fmst = new FmsFileMstVO();
		try{
			fmst.setFilSeq(Integer.valueOf(filSeq));
		}catch(Exception e){}
		fmst.setFilNm(filNm);;
				
		
		
		try {
			// 0.멀티파트파일 서버에 업로드
			List<FmsFileDtlVO> fdtlList = new ArrayList<>();
			for (MultipartFile file : files) {
				//파일 저장
		    	String uploadName = FileUtil.upload(request, file);
		    	
		    	if (uploadName == null) {
		    		//저장 실패
		    		throw new IOException("파일 저장 실패");
		    	}
		    	
		    	Map<String, Integer> map = FileUtil.getWidthHeight(file);
		    	
		    	FmsFileDtlVO fdtl = new FmsFileDtlVO();
		    	fdtl.setDwnNam(file.getOriginalFilename());
		    	fdtl.setUpfNam(uploadName);
		    	fdtl.setFilTyp(FileUtil.getExt(uploadName));
		    	fdtl.setFilSiz((file.getSize() / 1000) + "");
		    	fdtl.setFilRst(map.get("width") + " X " + map.get("height"));
		    	
		    	fdtlList.add(fdtl);
			}
			
			
			
			// 1.첨부파일저장 서비스
			filSeq = fmsFileService.saveFmsFile(fmst, fdtlList);
			
			
			
			//파일등록팝업 리다이렉트
			return "redirect:/pms/file/lnkSclPhotoDtl.do?smode="+smode+"&filSeq="+filSeq+"&sclNum="+sclNum;
			
		} catch (Exception e) {
			
			String redirect = "redirect:/pms/file/lnkSclPhotoDtl.do?error=1";
			return redirect;
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/********************************************************************************************
	 * 닷넷 복수파일 업로드
	 * @param files
	 * @param request
	 * @param sclNum
	 * @param ftrCde
	 * @param ftrIdn
	 * @param seq
	 * @param filSeq
	 * @return
	 */
	@RequestMapping("/fmsFileSave.do")
	@ResponseBody
	public String fmsFileSaves(HttpServletRequest request
			, @RequestPart("mfiles") List<MultipartFile> files
			, @RequestParam(value="sclNum", required=false) String sclNum 
			, @RequestParam(value="ftrCde", required=false) String ftrCde 
			, @RequestParam(value="ftrIdn", required=false) String ftrIdn 
			, @RequestParam(value="seq", required=false) String seq 
			, @RequestParam(value="filSeq", required=false) String filSeq  
			) {
		
		

		
		try {
			List<FmsFileDtlVO> fdtlList = new ArrayList<>();
			for (MultipartFile file : files) {
				//파일 저장
		    	String uploadName = FileUtil.upload(request, file);
		    	
		    	if (uploadName == null) {
		    		//저장 실패
		    		throw new IOException("파일 저장 실패");
		    	}
		    	
		    	Map<String, Integer> map = FileUtil.getWidthHeight(file);
		    	
		    	FmsFileDtlVO fdtl = new FmsFileDtlVO();
		    	fdtl.setDwnNam(file.getOriginalFilename());
		    	fdtl.setUpfNam(uploadName);
		    	fdtl.setFilTyp(FileUtil.getExt(uploadName));
		    	fdtl.setFilSiz((file.getSize() / 1000) + "");
		    	fdtl.setFilRst(map.get("width") + " X " + map.get("height"));
		    	
		    	fdtlList.add(fdtl);
			}
			
	    	
	    	// 전달받은 파일아이디
	    	FmsFileMstVO fmst = new FmsFileMstVO();
	    	try{
	    		fmst.setFilSeq(Integer.valueOf(filSeq));
	    	}catch(Exception e){}

	    	//0.사진파일저장
	    	String _filSeq = fmsFileService.saveFmsFile(fmst, fdtlList);
			
			
			//0.점검시설에 파일매핑
			Map<String, Object> data = new HashMap<String, Object>();
			data.put("sclNum", sclNum);
			data.put("ftrCde", ftrCde);
			data.put("ftrIdn", ftrIdn);
			data.put("seq", seq);
			data.put("filSeq", _filSeq);
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("sqlId", "updateFileResult");
			param.put("data", data);
			cmmService.saveData(param);
			
			
			//파일등록 성공
			return "filSeq="+_filSeq;
			
		} catch (Exception e) {
			
			return "FMS_FAIL";
		}
	}
	
 	
}