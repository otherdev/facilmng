package com.gti.pms.cmm.web;


import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.gti.pms.cmm.service.CmmService;
import com.gti.pms.cmm.service.impl.CmmDAO;
import com.gti.pms.util.QrUtil;

import egovframework.let.utl.fcc.service.EgovStringUtil;
import egovframework.rte.fdl.property.EgovPropertyService;


@Controller
@RequestMapping("/pms/report")
public class ReportController extends CodeController{

    
	@Resource(name = "cmmService")
	private CmmService cmmService;

	@Resource(name = "cmmDAO")
	private CmmDAO cmmDAO;
	
    
    /** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;
    
	
	
	
	/**
     * 
     */
    @RequestMapping(value="/pdf.do")
    public ModelAndView pdf(
    		@RequestParam(value="param1", required=false) String param1,
    		@RequestParam(value="param2", required=false) String param2,
			ModelAndView mav) throws Exception {	
    	
    			
		Map<String, Object> parameterMap = new HashMap<String, Object>();
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>(); 
		list.add(new HashMap<String, Object>(){{put("id","alsokyun");put("userNm","K.Hur");}} );
		list.add(new HashMap<String, Object>(){{put("id","jiaxu");put("userNm","JiaXu");}} );
		
		
		JRDataSource JRdataSource = new JRBeanCollectionDataSource(list);
        parameterMap.put( "datasource", JRdataSource);		
		
		
		
        //pdfReport bean has ben declared in the jasper-views.xml file
        mav = new ModelAndView("pdfReport" , parameterMap);
 		return mav;
    }
	
    
 
    

    
    
    @RequestMapping(value="/pdf2.do")
    public ModelAndView pdf2(
    		@RequestParam(value="param1", required=false) String param1,
    		@RequestParam(value="param2", required=false) String param2,
    		ModelAndView mav) throws Exception {	
    	
    	
    	Map<String, Object> parameterMap = new HashMap<String, Object>();
    	List<Map<String, Object>> list = new ArrayList<Map<String, Object>>(); 
    	list.add(new HashMap<String, Object>(){{put("id","alsokyun");put("userNm","K.Hur");}} );
    	list.add(new HashMap<String, Object>(){{put("id","jiaxu");put("userNm","JiaXu");}} );
    	
    	
    	JRDataSource JRdataSource = new JRBeanCollectionDataSource(list);
    	parameterMap.put( "datasource", JRdataSource);		
    	try {
    		Connection conn = cmmDAO.getDataSource().getConnection();
            parameterMap.put( "REPORT_CONNECTION" , conn);
	    } catch (Exception e) {
            e.printStackTrace();
	    }
    	
    	
    	
    	//pdfReport bean has ben declared in the jasper-views.xml file
    	mav = new ModelAndView("pdfReport2" , parameterMap);
    	return mav;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    /**
     * QR생성
     * genQr
     * @param data
     * @throws Exception
     * @return QR Image (PNG)
     */
    @RequestMapping("/common/genQr.do")
    public void genQr (HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
    	String data = EgovStringUtil.nullConvert(request.getParameter("data"));
    	if("" == data) {
    		data = "none";
    	}
    	QrUtil.genQr(response, data);
    }
    
 
  
}