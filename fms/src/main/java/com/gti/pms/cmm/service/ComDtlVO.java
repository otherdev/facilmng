package com.gti.pms.cmm.service;

import java.io.Serializable;

/** 
 * 공통상세코드 모델 클래스
 * @author 공통서비스 개발팀 
 */
public class ComDtlVO implements Serializable {

	private static final long serialVersionUID = -6508801327314181679L;

	/*
	 * 코드ID
	 */
	private String 	mstCd	= "";
	private String 	dtlCd	= "";
	private String 	cdNm	= "";
	private String 	etc	= "";
	private String 	ord	= "";
	private String 	delYn	= "";
	private String 	edtId	= "";
	private String 	edtDt	= "";
	
	
	
	public String getMstCd() {
		return mstCd;
	}
	public void setMstCd(String mstCd) {
		this.mstCd = mstCd;
	}
	public String getDtlCd() {
		return dtlCd;
	}
	public void setDtlCd(String dtlCd) {
		this.dtlCd = dtlCd;
	}
	public String getCdNm() {
		return cdNm;
	}
	public void setCdNm(String cdNm) {
		this.cdNm = cdNm;
	}
	public String getEtc() {
		return etc;
	}
	public void setEtc(String etc) {
		this.etc = etc;
	}
	public String getOrd() {
		return ord;
	}
	public void setOrd(String ord) {
		this.ord = ord;
	}
	public String getDelYn() {
		return delYn;
	}
	public void setDelYn(String delYn) {
		this.delYn = delYn;
	}
	public String getEdtId() {
		return edtId;
	}
	public void setEdtId(String edtId) {
		this.edtId = edtId;
	}
	public String getEdtDt() {
		return edtDt;
	}
	public void setEdtDt(String edtDt) {
		this.edtDt = edtDt;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}


}
