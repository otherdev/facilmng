package com.gti.pms.cmm.service;

/**
 * @Class Name : WttFverMaVO.java
 * @Description : WttFverMa VO class
 * @Modification Information
 *
 * @author DRCTS
 * @since 2018-07-01
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */
public class FmsFileMstVO {
    
    private java.lang.Integer filSeq;
    private java.lang.Integer seq;
    private java.lang.String filNm;
    private java.lang.String creYmd;
    private java.lang.String creUsr;
    private java.lang.String updYmd;
    private java.lang.String updUsr;
    private java.lang.String id;
    
    
    
    
	public java.lang.Integer getFilSeq() {
		return filSeq;
	}
	public void setFilSeq(java.lang.Integer filSeq) {
		this.filSeq = filSeq;
	}
	public java.lang.Integer getSeq() {
		return seq;
	}
	public void setSeq(java.lang.Integer seq) {
		this.seq = seq;
	}
	public java.lang.String getFilNm() {
		return filNm;
	}
	public void setFilNm(java.lang.String filNm) {
		this.filNm = filNm;
	}
	public java.lang.String getCreYmd() {
		return creYmd;
	}
	public void setCreYmd(java.lang.String creYmd) {
		this.creYmd = creYmd;
	}
	public java.lang.String getCreUsr() {
		return creUsr;
	}
	public void setCreUsr(java.lang.String creUsr) {
		this.creUsr = creUsr;
	}
	public java.lang.String getUpdYmd() {
		return updYmd;
	}
	public void setUpdYmd(java.lang.String updYmd) {
		this.updYmd = updYmd;
	}
	public java.lang.String getUpdUsr() {
		return updUsr;
	}
	public void setUpdUsr(java.lang.String updUsr) {
		this.updUsr = updUsr;
	}
	public java.lang.String getId() {
		return id;
	}
	public void setId(java.lang.String id) {
		this.id = id;
	}
    

    
    
}
