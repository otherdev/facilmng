package com.gti.pms.cmm.web;


import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.gti.pms.cmm.service.CmmService;
import com.gti.pms.cmm.service.impl.CmmDAO;
import com.gti.pms.util.QrUtil;

import egovframework.let.utl.fcc.service.EgovStringUtil;
import egovframework.rte.fdl.property.EgovPropertyService;


@Controller
@RequestMapping("/pms/pdf")
public class PdfController extends CodeController{

    
	@Resource(name = "cmmService")
	private CmmService cmmService;
	
	@Resource(name = "cmmDAO")
	private CmmDAO cmmDAO;
    
    /** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;	
    
    /**
     * 상수공사대장 출력
     */
    @RequestMapping(value="/cnstMngDtl.do")
    public ModelAndView costMngDtl(
    		@RequestParam(value="cntNum", required=false) String cntNum,
    		ModelAndView mav) throws Exception {	
    	
    	
    	Map<String, Object> parameterMap = new HashMap<String, Object>();

    	Map<String, Object> map = new HashMap<String, Object>();
		map.put("cntNum", cntNum);
		
		List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectWttConsMa", map);	
					
    	JRDataSource JRdataSource = new JRBeanCollectionDataSource(list);    	
    	parameterMap.put( "datasource", JRdataSource);
    	
    	try {
    		Connection conn = cmmDAO.getDataSource().getConnection();
            parameterMap.put( "REPORT_CONNECTION" , conn);
	    } catch (Exception e) {
            e.printStackTrace();
	    }
    	
    	parameterMap.put( "pCntNum", cntNum);    	
    	
    	//pdfReport bean has ben declared in the jasper-views.xml file
    	mav = new ModelAndView("cnstMngDtl" , parameterMap);
    	return mav;
    }
	
	
	/**
     * 상수관로 출력
     */
    @RequestMapping(value="/wtpipMngDtl.do")
    public ModelAndView wtpipMngDtl(
    		@RequestParam(value="ftrCde", required=false) String ftrCde,
    		@RequestParam(value="ftrIdn", required=false) String ftrIdn,
			ModelAndView mav) throws Exception {	
    	
    			
    	Map<String, Object> parameterMap = new HashMap<String, Object>();

    	Map<String, Object> map = new HashMap<String, Object>();
		map.put("ftrCde", ftrCde);
		map.put("ftrIdn", ftrIdn);
		
		List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectWtPipenDtl", map);	
		
		if(list.size() > 0){
			list.get(0).put("barCde", ftrCde + "-" + ftrIdn);
		}
			
    	JRDataSource JRdataSource = new JRBeanCollectionDataSource(list);    	
    	parameterMap.put( "datasource", JRdataSource);
    	
    	try {
    		Connection conn = cmmDAO.getDataSource().getConnection();
            parameterMap.put( "REPORT_CONNECTION" , conn);
	    } catch (Exception e) {
            e.printStackTrace();
	    }
    	
    	parameterMap.put( "pFtrCde", ftrCde);
    	parameterMap.put( "pFtrIdn", ftrIdn);
    	parameterMap.put( "pBarCde", ftrCde + "-" + ftrIdn );		
		
        //pdfReport bean has ben declared in the jasper-views.xml file
        mav = new ModelAndView("wtpipMngDtl" , parameterMap);
 		return mav;
    }
	
    
 
    /**
     * 변류시설 출력
     */
    @RequestMapping(value="/valvFacView.do")
    public ModelAndView valvFacView(
    		@RequestParam(value="ftrCde", required=false) String ftrCde,
    		@RequestParam(value="ftrIdn", required=false) String ftrIdn,
    		ModelAndView mav) throws Exception {	
    	
    	Map<String, Object> parameterMap = new HashMap<String, Object>();

    	Map<String, Object> map = new HashMap<String, Object>();
		map.put("ftrCde", ftrCde);
		map.put("ftrIdn", ftrIdn);
		
		List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectValvFacView", map);	
		
		if(list.size() > 0){
			list.get(0).put("barCde", ftrCde + "-" + ftrIdn);
		}
			
    	JRDataSource JRdataSource = new JRBeanCollectionDataSource(list);    	
    	parameterMap.put( "datasource", JRdataSource);
    	
    	try {
    		Connection conn = cmmDAO.getDataSource().getConnection();
            parameterMap.put( "REPORT_CONNECTION" , conn);
	    } catch (Exception e) {
            e.printStackTrace();
	    }
    	
    	parameterMap.put( "pFtrCde", ftrCde);
    	parameterMap.put( "pFtrIdn", ftrIdn);
    	parameterMap.put( "pBarCde", ftrCde + "-" + ftrIdn );
    	    	
    	//pdfReport bean has ben declared in the jasper-views.xml file
    	mav = new ModelAndView("valvFacView" , parameterMap);
    	    	
    	return mav;
    }
    
    /**
     * 소방시설 출력
     */
    @RequestMapping(value="/fireFacView.do")
    public ModelAndView fireFacView(
    		@RequestParam(value="ftrCde", required=false) String ftrCde,
    		@RequestParam(value="ftrIdn", required=false) String ftrIdn,
    		ModelAndView mav) throws Exception {	
    	
    	Map<String, Object> parameterMap = new HashMap<String, Object>();

    	Map<String, Object> map = new HashMap<String, Object>();
		map.put("ftrCde", ftrCde);
		map.put("ftrIdn", ftrIdn);
		
		List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectFireView", map);	
		
		if(list.size() > 0){
			list.get(0).put("barCde", ftrCde + "-" + ftrIdn);
		}
			
    	JRDataSource JRdataSource = new JRBeanCollectionDataSource(list);    	
    	parameterMap.put( "datasource", JRdataSource);
    	
    	try {
    		Connection conn = cmmDAO.getDataSource().getConnection();
            parameterMap.put( "REPORT_CONNECTION" , conn);
	    } catch (Exception e) {
            e.printStackTrace();
	    }
    	
    	parameterMap.put( "pFtrCde", ftrCde);
    	parameterMap.put( "pFtrIdn", ftrIdn);
    	parameterMap.put( "pBarCde", ftrCde + "-" + ftrIdn );
    	    	
    	//pdfReport bean has ben declared in the jasper-views.xml file
    	mav = new ModelAndView("fireFacView" , parameterMap);
    	    	
    	return mav;
    }
    
    /**
     * 상수맨홀 출력
     */
    @RequestMapping(value="/wtsMnhoView.do")
    public ModelAndView wtsMnhoView(
    		@RequestParam(value="ftrCde", required=false) String ftrCde,
    		@RequestParam(value="ftrIdn", required=false) String ftrIdn,
    		ModelAndView mav) throws Exception {	
    	
    	Map<String, Object> parameterMap = new HashMap<String, Object>();

    	Map<String, Object> map = new HashMap<String, Object>();
		map.put("ftrCde", ftrCde);
		map.put("ftrIdn", ftrIdn);
		
		List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectWtsMnhoView", map);	
		
		if(list.size() > 0){
			list.get(0).put("barCde", ftrCde + "-" + ftrIdn);
		}
			
    	JRDataSource JRdataSource = new JRBeanCollectionDataSource(list);    	
    	parameterMap.put( "datasource", JRdataSource);
    	
    	try {
    		Connection conn = cmmDAO.getDataSource().getConnection();
            parameterMap.put( "REPORT_CONNECTION" , conn);
	    } catch (Exception e) {
            e.printStackTrace();
	    }
    	
    	parameterMap.put( "pFtrCde", ftrCde);
    	parameterMap.put( "pFtrIdn", ftrIdn);
    	parameterMap.put( "pBarCde", ftrCde + "-" + ftrIdn );
    	    	
    	//pdfReport bean has ben declared in the jasper-views.xml file
    	mav = new ModelAndView("wtsMnhoView" , parameterMap);
    	    	
    	return mav;
    }
    
    
    /**
     * 유량계 출력
     */
    @RequestMapping(value="/flowMtList.do")
    public ModelAndView flowMtList(
    		@RequestParam(value="ftrCde", required=false) String ftrCde,
    		@RequestParam(value="ftrIdn", required=false) String ftrIdn,
    		ModelAndView mav) throws Exception {	
    	
    	
    	Map<String, Object> parameterMap = new HashMap<String, Object>();

    	Map<String, Object> map = new HashMap<String, Object>();
		map.put("ftrCde", ftrCde);
		map.put("ftrIdn", ftrIdn);
		
		List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectFlowMtView", map);	
		
		if(list.size() > 0){
			list.get(0).put("barCde", ftrCde + "-" + ftrIdn);
		}
			
    	JRDataSource JRdataSource = new JRBeanCollectionDataSource(list);    	
    	parameterMap.put( "datasource", JRdataSource);
    	
    	try {
    		Connection conn = cmmDAO.getDataSource().getConnection();
            parameterMap.put( "REPORT_CONNECTION" , conn);
	    } catch (Exception e) {
            e.printStackTrace();
	    }
    	
    	parameterMap.put( "pFtrCde", ftrCde);
    	parameterMap.put( "pFtrIdn", ftrIdn);
    	parameterMap.put( "pBarCde", ftrCde + "-" + ftrIdn );
    	
    	//pdfReport bean has ben declared in the jasper-views.xml file
    	mav = new ModelAndView("flowMtList" , parameterMap);
    	return mav;
    }
    
    /**
     * 스탠드 파이프 출력
     */
    @RequestMapping(value="/stndPiView.do")
    public ModelAndView stndPiView(
    		@RequestParam(value="ftrCde", required=false) String ftrCde,
    		@RequestParam(value="ftrIdn", required=false) String ftrIdn,
    		ModelAndView mav) throws Exception {	
    	
    	
    	Map<String, Object> parameterMap = new HashMap<String, Object>();

    	Map<String, Object> map = new HashMap<String, Object>();
		map.put("ftrCde", ftrCde);
		map.put("ftrIdn", ftrIdn);
		
		List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectStndPiView", map);	
		
		if(list.size() > 0){
			list.get(0).put("barCde", ftrCde + "-" + ftrIdn);
		}
			
    	JRDataSource JRdataSource = new JRBeanCollectionDataSource(list);    	
    	parameterMap.put( "datasource", JRdataSource);
    	
    	try {
    		Connection conn = cmmDAO.getDataSource().getConnection();
            parameterMap.put( "REPORT_CONNECTION" , conn);
	    } catch (Exception e) {
            e.printStackTrace();
	    }
    	
    	parameterMap.put( "pFtrCde", ftrCde);
    	parameterMap.put( "pFtrIdn", ftrIdn);
    	parameterMap.put( "pBarCde", ftrCde + "-" + ftrIdn );
    	
    	//pdfReport bean has ben declared in the jasper-views.xml file
    	mav = new ModelAndView("stndPiView" , parameterMap);
    	return mav;
    }
    
    /**
     * 수압계 출력
     */
    @RequestMapping(value="/wtprMtView.do")
    public ModelAndView wtprMtView(
    		@RequestParam(value="ftrCde", required=false) String ftrCde,
    		@RequestParam(value="ftrIdn", required=false) String ftrIdn,
    		ModelAndView mav) throws Exception {	
    	
    	
    	Map<String, Object> parameterMap = new HashMap<String, Object>();

    	Map<String, Object> map = new HashMap<String, Object>();
		map.put("ftrCde", ftrCde);
		map.put("ftrIdn", ftrIdn);
		
		List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectWtprMtView", map);	
		
		if(list.size() > 0){
			list.get(0).put("barCde", ftrCde + "-" + ftrIdn);
		}
			
    	JRDataSource JRdataSource = new JRBeanCollectionDataSource(list);    	
    	parameterMap.put( "datasource", JRdataSource);
    	
    	try {
    		Connection conn = cmmDAO.getDataSource().getConnection();
            parameterMap.put( "REPORT_CONNECTION" , conn);
	    } catch (Exception e) {
            e.printStackTrace();
	    }
    	
    	parameterMap.put( "pFtrCde", ftrCde);
    	parameterMap.put( "pFtrIdn", ftrIdn);
    	parameterMap.put( "pBarCde", ftrCde + "-" + ftrIdn );
    	
    	//pdfReport bean has ben declared in the jasper-views.xml file
    	mav = new ModelAndView("wtprMtView" , parameterMap);
    	return mav;
    }
    
    /**
     * 수원지 출력
     */
    @RequestMapping(value="/wtrSourView.do")
    public ModelAndView wtrSourView(
    		@RequestParam(value="ftrCde", required=false) String ftrCde,
    		@RequestParam(value="ftrIdn", required=false) String ftrIdn,
    		ModelAndView mav) throws Exception {	
    	
    	
    	Map<String, Object> parameterMap = new HashMap<String, Object>();

    	Map<String, Object> map = new HashMap<String, Object>();
		map.put("ftrCde", ftrCde);
		map.put("ftrIdn", ftrIdn);
		
		List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectWtrSourView", map);	
		
		if(list.size() > 0){
			list.get(0).put("barCde", ftrCde + "-" + ftrIdn);
		}
			
    	JRDataSource JRdataSource = new JRBeanCollectionDataSource(list);    	
    	parameterMap.put( "datasource", JRdataSource);
    	
    	try {
    		Connection conn = cmmDAO.getDataSource().getConnection();
            parameterMap.put( "REPORT_CONNECTION" , conn);
	    } catch (Exception e) {
            e.printStackTrace();
	    }
    	
    	parameterMap.put( "pFtrCde", ftrCde);
    	parameterMap.put( "pFtrIdn", ftrIdn);
    	parameterMap.put( "pBarCde", ftrCde + "-" + ftrIdn );
    	
    	//pdfReport bean has ben declared in the jasper-views.xml file
    	mav = new ModelAndView("wtrSourView" , parameterMap);
    	return mav;
    }
    
    /**
     * 취수장 출력
     */
    @RequestMapping(value="/intkStView.do")
    public ModelAndView intkStView(
    		@RequestParam(value="ftrCde", required=false) String ftrCde,
    		@RequestParam(value="ftrIdn", required=false) String ftrIdn,
    		ModelAndView mav) throws Exception {	
    	
    	
    	Map<String, Object> parameterMap = new HashMap<String, Object>();

    	Map<String, Object> map = new HashMap<String, Object>();
		map.put("ftrCde", ftrCde);
		map.put("ftrIdn", ftrIdn);
		
		List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectIntkStView", map);	
		
		if(list.size() > 0){
			list.get(0).put("barCde", ftrCde + "-" + ftrIdn);
		}
			
    	JRDataSource JRdataSource = new JRBeanCollectionDataSource(list);    	
    	parameterMap.put( "datasource", JRdataSource);
    	
    	try {
    		Connection conn = cmmDAO.getDataSource().getConnection();
            parameterMap.put( "REPORT_CONNECTION" , conn);
	    } catch (Exception e) {
            e.printStackTrace();
	    }
    	
    	parameterMap.put( "pFtrCde", ftrCde);
    	parameterMap.put( "pFtrIdn", ftrIdn);
    	parameterMap.put( "pBarCde", ftrCde + "-" + ftrIdn );
    	
    	//pdfReport bean has ben declared in the jasper-views.xml file
    	mav = new ModelAndView("intkStView" , parameterMap);
    	return mav;
    }
    
    /**
     * 배수지 출력
     */
    @RequestMapping(value="/wtrSupView.do")
    public ModelAndView wtrSupView(
    		@RequestParam(value="ftrCde", required=false) String ftrCde,
    		@RequestParam(value="ftrIdn", required=false) String ftrIdn,
    		ModelAndView mav) throws Exception {	
    	
    	
    	Map<String, Object> parameterMap = new HashMap<String, Object>();

    	Map<String, Object> map = new HashMap<String, Object>();
		map.put("ftrCde", ftrCde);
		map.put("ftrIdn", ftrIdn);
		
		List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectWtrSupView", map);	
		
		if(list.size() > 0){
			list.get(0).put("barCde", ftrCde + "-" + ftrIdn);
		}
			
    	JRDataSource JRdataSource = new JRBeanCollectionDataSource(list);    	
    	parameterMap.put( "datasource", JRdataSource);
    	
    	try {
    		Connection conn = cmmDAO.getDataSource().getConnection();
            parameterMap.put( "REPORT_CONNECTION" , conn);
	    } catch (Exception e) {
            e.printStackTrace();
	    }
    	
    	parameterMap.put( "pFtrCde", ftrCde);
    	parameterMap.put( "pFtrIdn", ftrIdn);
    	parameterMap.put( "pBarCde", ftrCde + "-" + ftrIdn );
    	
    	//pdfReport bean has ben declared in the jasper-views.xml file
    	mav = new ModelAndView("wtrSupView" , parameterMap);
    	return mav;
    }
    
    
    
    /**
     * 정수장 출력
     */
    @RequestMapping(value="/filtPltView.do")
    public ModelAndView filtPltView(
    		@RequestParam(value="ftrCde", required=false) String ftrCde,
    		@RequestParam(value="ftrIdn", required=false) String ftrIdn,
    		ModelAndView mav) throws Exception {	
    	
    	
    	Map<String, Object> parameterMap = new HashMap<String, Object>();

    	Map<String, Object> map = new HashMap<String, Object>();
		map.put("ftrCde", ftrCde);
		map.put("ftrIdn", ftrIdn);
		
		List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectFiltPltView", map);	
		
		if(list.size() > 0){
			list.get(0).put("barCde", ftrCde + "-" + ftrIdn);
		}
			
    	JRDataSource JRdataSource = new JRBeanCollectionDataSource(list);    	
    	parameterMap.put( "datasource", JRdataSource);
    	
    	try {
    		Connection conn = cmmDAO.getDataSource().getConnection();
            parameterMap.put( "REPORT_CONNECTION" , conn);
	    } catch (Exception e) {
            e.printStackTrace();
	    }
    	
    	parameterMap.put( "pFtrCde", ftrCde);
    	parameterMap.put( "pFtrIdn", ftrIdn);
    	parameterMap.put( "pBarCde", ftrCde + "-" + ftrIdn );
    	
    	//pdfReport bean has ben declared in the jasper-views.xml file
    	mav = new ModelAndView("filtPltView" , parameterMap);
    	return mav;
    }
    
    
    /**
     * 가압장 출력
     */
    @RequestMapping(value="/prsPmpView.do")
    public ModelAndView prsPmpView(
    		@RequestParam(value="ftrCde", required=false) String ftrCde,
    		@RequestParam(value="ftrIdn", required=false) String ftrIdn,
    		ModelAndView mav) throws Exception {	
    	
    	
    	Map<String, Object> parameterMap = new HashMap<String, Object>();

    	Map<String, Object> map = new HashMap<String, Object>();
		map.put("ftrCde", ftrCde);
		map.put("ftrIdn", ftrIdn);
		
		List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selecPrsPmpView", map);	
		
		if(list.size() > 0){
			list.get(0).put("barCde", ftrCde + "-" + ftrIdn);
		}
			
    	JRDataSource JRdataSource = new JRBeanCollectionDataSource(list);    	
    	parameterMap.put( "datasource", JRdataSource);
    	
    	try {
    		Connection conn = cmmDAO.getDataSource().getConnection();
            parameterMap.put( "REPORT_CONNECTION" , conn);
	    } catch (Exception e) {
            e.printStackTrace();
	    }
    	
    	parameterMap.put( "pFtrCde", ftrCde);
    	parameterMap.put( "pFtrIdn", ftrIdn);
    	parameterMap.put( "pBarCde", ftrCde + "-" + ftrIdn );    	
    	
    	//pdfReport bean has ben declared in the jasper-views.xml file
    	mav = new ModelAndView("prsPmpView" , parameterMap);
    	return mav;
    }
    
            
    /**
     * 급수관로 출력
     */
    @RequestMapping(value="/supDutView.do")
    public ModelAndView supDutView(
    		@RequestParam(value="ftrCde", required=false) String ftrCde,
    		@RequestParam(value="ftrIdn", required=false) String ftrIdn,
    		ModelAndView mav) throws Exception {	
    	
    	
    	Map<String, Object> parameterMap = new HashMap<String, Object>();

    	Map<String, Object> map = new HashMap<String, Object>();
		map.put("ftrCde", ftrCde);
		map.put("ftrIdn", ftrIdn);
		
		List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectWtlSplyLs", map);	
		
		if(list.size() > 0){
			list.get(0).put("barCde", ftrCde + "-" + ftrIdn);
		}
			
    	JRDataSource JRdataSource = new JRBeanCollectionDataSource(list);    	
    	parameterMap.put( "datasource", JRdataSource);
    	
    	try {
    		Connection conn = cmmDAO.getDataSource().getConnection();
            parameterMap.put( "REPORT_CONNECTION" , conn);
	    } catch (Exception e) {
            e.printStackTrace();
	    }
    	
    	parameterMap.put( "pFtrCde", ftrCde);
    	parameterMap.put( "pFtrIdn", ftrIdn);
    	parameterMap.put( "pBarCde", ftrCde + "-" + ftrIdn );    	
    	
    	//pdfReport bean has ben declared in the jasper-views.xml file
    	mav = new ModelAndView("supDutView" , parameterMap);
    	return mav;
    }
    
    /**
     * 급수전계량기 대장 출력
     */
    @RequestMapping(value="/hydtMetrView.do")
    public ModelAndView hydtMetrView(
    		@RequestParam(value="ftrCde", required=false) String ftrCde,
    		@RequestParam(value="ftrIdn", required=false) String ftrIdn,
    		ModelAndView mav) throws Exception {	
    	
    	
    	Map<String, Object> parameterMap = new HashMap<String, Object>();

    	Map<String, Object> map = new HashMap<String, Object>();
		map.put("ftrCde", ftrCde);
		map.put("ftrIdn", ftrIdn);
		
		List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectWtlMetaPs", map);	
		
		if(list.size() > 0){
			list.get(0).put("barCde", ftrCde + "-" + ftrIdn);
		}
			
    	JRDataSource JRdataSource = new JRBeanCollectionDataSource(list);    	
    	parameterMap.put( "datasource", JRdataSource);
    	
    	try {
    		Connection conn = cmmDAO.getDataSource().getConnection();
            parameterMap.put( "REPORT_CONNECTION" , conn);
	    } catch (Exception e) {
            e.printStackTrace();
	    }
    	
    	parameterMap.put( "pFtrCde", ftrCde);
    	parameterMap.put( "pFtrIdn", ftrIdn);
    	parameterMap.put( "pBarCde", ftrCde + "-" + ftrIdn );    	
    	
    	//pdfReport bean has ben declared in the jasper-views.xml file
    	mav = new ModelAndView("hydtMetrView" , parameterMap);
    	return mav;
    }
    
    
    /**
     * 저수조 출력
     */
    @RequestMapping(value="/wtrTrView.do")
    public ModelAndView wtrTrView(
    		@RequestParam(value="ftrCde", required=false) String ftrCde,
    		@RequestParam(value="ftrIdn", required=false) String ftrIdn,
    		ModelAndView mav) throws Exception {	
    	
    	
    	Map<String, Object> parameterMap = new HashMap<String, Object>();

    	Map<String, Object> map = new HashMap<String, Object>();
		map.put("ftrCde", ftrCde);
		map.put("ftrIdn", ftrIdn);
		
		List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectWtlRsrvPs", map);	
		
		if(list.size() > 0){
			list.get(0).put("barCde", ftrCde + "-" + ftrIdn);
		}
			
    	JRDataSource JRdataSource = new JRBeanCollectionDataSource(list);    	
    	parameterMap.put( "datasource", JRdataSource);
    	
    	try {
    		Connection conn = cmmDAO.getDataSource().getConnection();
            parameterMap.put( "REPORT_CONNECTION" , conn);
	    } catch (Exception e) {
            e.printStackTrace();
	    }
    	
    	parameterMap.put( "pFtrCde", ftrCde);
    	parameterMap.put( "pFtrIdn", ftrIdn);
    	parameterMap.put( "pBarCde", ftrCde + "-" + ftrIdn );    	
    	
    	//pdfReport bean has ben declared in the jasper-views.xml file
    	mav = new ModelAndView("wtrTrView" , parameterMap);
    	return mav;
    }
    
    
    
    
    
    
    /**
     * QR생성
     * genQr
     * @param data
     * @throws Exception
     * @return QR Image (PNG)
     */
    @RequestMapping("/common/genQr.do")
    public void genQr (HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
    	String data = EgovStringUtil.nullConvert(request.getParameter("data"));
    	if("" == data) {
    		data = "none";
    	}
    	QrUtil.genQr(response, data);
    }
    
 
  
}