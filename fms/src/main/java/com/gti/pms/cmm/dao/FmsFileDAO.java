package com.gti.pms.cmm.dao;

import org.springframework.stereotype.Repository;

import com.gti.pms.cmm.service.FmsFileMstVO;

import egovframework.rte.psl.dataaccess.EgovAbstractDAO;

/**
 * @Class Name : WttFverMaDAO.java
 * @Description : WttFverMa DAO Class
 * @Modification Information
 *
 * @author DRCTS
 * @since 2018-07-01
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Repository("fmsFileDAO")
public class FmsFileDAO extends EgovAbstractDAO {

    /******************************************
     * FMS 파일관리
     */

    
    public Integer insertFileMst(FmsFileMstVO vo) throws Exception {
        return (Integer)insert("insertFileMst", vo);
    }

    public void updateFileMst(FmsFileMstVO vo) throws Exception {
        update("updateFileMst", vo);
    }

    
}
