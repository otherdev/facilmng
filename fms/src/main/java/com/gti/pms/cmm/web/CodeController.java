package com.gti.pms.cmm.web;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import com.gti.pms.cmm.service.CmmService;
import com.gti.pms.cmm.util.JsonUtils;
import com.ibm.icu.text.SimpleDateFormat;

import egovframework.let.utl.fcc.service.EgovStringUtil;

/**
 * 사업관리 
 * @author 공통서비스 개발팀 
 * @see
 *
 */
@Controller
public class CodeController {


	/** cmmService */
	@Resource(name = "cmmService")
	private CmmService cmmService;

	@Resource(name = "jsonUtils")
	private JsonUtils jsonUtils;

	
	
	
	
	/************************************************
	 * 1.공통코드
	 */
	
	
	
    /**
     * 공사구분 (CNT_CDE)
     * getCntCdeListModel
     * @param model
     * @throws Exception 
     * @return ${cntCdeList} ${mapCntCde}
     */
    public void getCntCdeListModel(Model model) throws Exception {
    	/*공사구분*/
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put("typeCode", "CNT_CDE");
		List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectCodeList", param);
    	model.addAttribute("cntCdeList", lst);  
    	
    	/// 공통코드명을 화면에서 보여주기위해서 맵형태로 변환함
    	Map<String, Object> map = convertCodeMap(lst);
    	model.addAttribute("mapCntCde", map);
    }
	
    
    /**
     * 관리기관코드 (MNG_CDE)
     * getMngCdeListModel
     * @param model
     * @throws Exception 
     * @return ${dataCodeList} ${mapMngCde}
     */
    public void getMngCdeListModel(Model model) throws Exception {
    	/*관리기관*/
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put("typeCode", "MNG_CDE");
		List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectCodeList", param);
    	model.addAttribute("dataCodeList", lst);  
    	
    	/// 공통코드명을 화면에서 보여주기위해서 맵형태로 변환함
    	Map<String, Object> map = convertCodeMap(lst);
    	model.addAttribute("mapMngCde", map);
    }
    
    
    /**
     * 계약방법 (CTT_CDE)
     * getCttCdeListModel
     * @param model
     * @throws Exception 
     * @return ${cttCodeList} ${mapCttCde}
     */
    public void getCttCdeListModel(Model model) throws Exception {
    	/*계약방법*/
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put("typeCode", "CTT_CDE");
		List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectCodeList", param);
    	model.addAttribute("cttCodeList", lst);  
    	
    	/// 공통코드명을 화면에서 보여주기위해서 맵형태로 변환함
    	Map<String, Object> map = convertCodeMap(lst);
    	model.addAttribute("mapCttCde", map);
    }

    
    /**
     * 지급구분 (PTY_CDE)
     * getPtyCdeListModel
     * @param model
     * @throws Exception 
     * @return ${ptyCodeList} ${mapPtyCde}
     */
    public void getPtyCdeListModel(Model model) throws Exception {
    	/*지급구분*/
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put("typeCode", "PTY_CDE");
		List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectCodeList", param);
    	model.addAttribute("ptyCodeList", lst);  
    	
    	/// 공통코드명을 화면에서 보여주기위해서 맵형태로 변환함
    	Map<String, Object> map = convertCodeMap(lst);
    	model.addAttribute("mapPtyCde", map);
    }
    
    
    
    /**
     * 배수지 관리방법 (SAG_CDE)
     * getSagCdeListModel
     * @param model
     * @throws Exception 
     * @return ${sagCdeList} ${mapSagCde}
     */
    public void getSagCdeListModel(Model model) throws Exception {
    	/*관리기관*/
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put("typeCode", "SAG_CDE");
		List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectCodeList", param);
    	model.addAttribute("sagCdeList", lst);  
    	
    	/// 공통코드명을 화면에서 보여주기위해서 맵형태로 변환함
    	Map<String, Object> map = convertCodeMap(lst);
    	model.addAttribute("mapSagCde", map);
    }
    
    
    /**
     * 수원구분코드 (WSR_CDE)
     * getWsrCdeListModel
     * @param model
     * @throws Exception 
     * @return ${wsrCdeList} ${mapWsrCde}
     */
    public void getWsrCdeListModel(Model model) throws Exception {
    	/*수원구분*/
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put("typeCode", "WSR_CDE");
    	List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectCodeList", param);
    	model.addAttribute("wsrCdeList", lst);  
    	
    	/// 공통코드명을 화면에서 보여주기위해서 맵형태로 변환함
    	Map<String, Object> map = convertCodeMap(lst);
        model.addAttribute("mapWsrCde", map);
    } 
    
    /**
     * 관용도코드 (SAA_CDE)
     * getSaaCdeListModel
     * @param model
     * @throws Exception 
     * @return ${saaCdeList} ${mapSaaCde}
     */
    public void getSaaCdeListModel(Model model) throws Exception {
    	/*관용도*/
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put("typeCode", "SAA_CDE");
    	List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectCodeList", param);
    	model.addAttribute("saaCdeList", lst);  
    	
    	/// 공통코드명을 화면에서 보여주기위해서 맵형태로 변환함
    	Map<String, Object> map = convertCodeMap(lst);
        model.addAttribute("mapSaaCde", map);
    } 
    
    
    /**
     * 관재질코드 (MOP_CDE)
     * getMopCdeListModel
     * @param model
     * @throws Exception 
     * @return ${mopCdeList} ${mapMopCde}
     */
    public void getMopCdeListModel(Model model) throws Exception {
    	/*관재질*/
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put("typeCode", "MOP_CDE");
    	List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectCodeList", param);
    	model.addAttribute("mopCdeList", lst);  
    	
    	/// 공통코드명을 화면에서 보여주기위해서 맵형태로 변환함
    	Map<String, Object> map = convertCodeMap(lst);
        model.addAttribute("mapMopCde", map);
    } 
    
    
    /**
     * 접합종류코드 (JHT_CDE)
     * getJhtCdeListModel
     * @param model
     * @throws Exception 
     * @return ${jhtCdeList} ${mapJhtCde}
     */
    public void getJhtCdeListModel(Model model) throws Exception {
    	/*접합종류*/
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put("typeCode", "JHT_CDE");
    	List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectCodeList", param);
    	model.addAttribute("jhtCdeList", lst);  
    	
    	/// 공통코드명을 화면에서 보여주기위해서 맵형태로 변환함
    	Map<String, Object> map = convertCodeMap(lst);
        model.addAttribute("mapJhtCde", map);
    }
    
    
    /**
     * 업종코드 (SBI_CDE)
     * getSbiCdeListModel
     * @param model
     * @throws Exception 
     * @return ${sbiCdeList} ${mapJhtCde}
     */
    public void getSbiCdeListModel(Model model) throws Exception {
    	/*업종*/
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put("typeCode", "SBI_CDE");
    	List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectCodeList", param);
    	model.addAttribute("sbiCdeList", lst);  
    	
    	/// 공통코드명을 화면에서 보여주기위해서 맵형태로 변환함
    	Map<String, Object> map = convertCodeMap(lst);
        model.addAttribute("mapSbiCde", map);
    } 
    
    
    /**
     * 형식코드 (MOF_CDE)
     * getMofCdeListModel
     * @param model
     * @throws Exception 
     * @return ${mofCdeList} ${mapMofCde}
     */
    public void getMofCdeListModel(Model model) throws Exception {
    	/*형식*/
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put("typeCode", "MOF_CDE");
    	List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectCodeList", param);
    	model.addAttribute("mofCdeList", lst);  
    	
    	/// 공통코드명을 화면에서 보여주기위해서 맵형태로 변환함
    	Map<String, Object> map = convertCodeMap(lst);
        model.addAttribute("mapMofCde", map);
    } 
    
    
    /**
     * 교체구분코드 (GCW_CDE)
     * getMofCdeListModel
     * @param model
     * @throws Exception 
     * @return ${gcwCdeList} ${mapGcwCde}
     */
    public void getGcwCdeListModel(Model model) throws Exception {
    	/*형식*/
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put("typeCode", "GCW_CDE");
    	List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectCodeList", param);
    	model.addAttribute("gcwCdeList", lst);  
    	
    	/// 공통코드명을 화면에서 보여주기위해서 맵형태로 변환함
    	Map<String, Object> map = convertCodeMap(lst);
    	model.addAttribute("mapGcwCde", map);
    } 
    
    
    /**
     * 구경코드 (MET_CDE)
     * 계량기종류 (MET_CDE)
     * getMofCdeListModel
     * @param model
     * @throws Exception 
     * @return ${metCdeList} ${mapMetCde}
     */
    public void getMetCdeListModel(Model model) throws Exception {
    	/*구경*/
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put("typeCode", "MET_CDE");
    	List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectCodeList", param);
    	model.addAttribute("metCdeList", lst);  
    	
    	/// 공통코드명을 화면에서 보여주기위해서 맵형태로 변환함
    	Map<String, Object> map = convertCodeMap(lst);
        model.addAttribute("mapMetCde", map);
    } 

    
    /**
     * 건물유형 (BLS_CDE)
     * getMofCdeListModel
     * @param model
     * @throws Exception 
     * @return ${blsCdeList} ${mapBlsCde}
     */
    public void getBlsCdeListModel(Model model) throws Exception {
    	/*건물유형*/
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put("typeCode", "BLS_CDE");
    	List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectCodeList", param);
    	model.addAttribute("blsCdeList", lst);  
    	
    	/// 공통코드명을 화면에서 보여주기위해서 맵형태로 변환함
    	Map<String, Object> map = convertCodeMap(lst);
        model.addAttribute("mapBlsCde", map);
    } 
    
    
    /**
     * 제수변회전방향코드 (SAE_CDE)
     * getSaeCdeListModel
     * @param model
     * @throws Exception 
     * @return ${blsCdeList} ${mapSaeCde}
     */
    public void getSaeCdeListModel(Model model) throws Exception {
    	/*제수변회전방향*/
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put("typeCode", "SAE_CDE");
    	List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectCodeList", param);
    	model.addAttribute("saeCdeList", lst);  
    	
    	/// 공통코드명을 화면에서 보여주기위해서 맵형태로 변환함
    	Map<String, Object> map = convertCodeMap(lst);
        model.addAttribute("mapSaeCde", map);
    }
    
    
    /**
     * 제수변구동방법코드 (MTH_CDE)
     * getMthCdeListModel
     * @param model
     * @throws Exception 
     * @return ${blsCdeList} ${mapMthCde}
     */
    public void getMthCdeListModel(Model model) throws Exception {
    	/*제수변구동방법*/
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put("typeCode", "MTH_CDE");
    	List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectCodeList", param);
    	model.addAttribute("mthCdeList", lst);  
    	
    	/// 공통코드명을 화면에서 보여주기위해서 맵형태로 변환함
    	Map<String, Object> map = convertCodeMap(lst);
    	model.addAttribute("mapMthCde", map);
    }
    
    
    /**
     * 시설물형태코드 (FOR_CDE)
     * getForCdeListModel
     * @param model
     * @throws Exception 
     * @return ${blsCdeList} ${mapForCde}
     */
    public void getForCdeListModel(Model model) throws Exception {
    	/*시설물형태*/
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put("typeCode", "FOR_CDE");
    	List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectCodeList", param);
    	model.addAttribute("forCdeList", lst);  
    	
    	/// 공통코드명을 화면에서 보여주기위해서 맵형태로 변환함
    	Map<String, Object> map = convertCodeMap(lst);
    	model.addAttribute("mapForCde", map);
    }
    
    
    /**
     * 이상상태코드 (CST_CDE)
     * getCstCdeListModel
     * @param model
     * @throws Exception 
     * @return ${blsCdeList} ${mapCstCde}
     */
    public void getCstCdeListModel(Model model) throws Exception {
    	/*이상상태*/
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put("typeCode", "CST_CDE");
    	List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectCodeList", param);
    	model.addAttribute("cstCdeList", lst);  
    	
    	/// 공통코드명을 화면에서 보여주기위해서 맵형태로 변환함
    	Map<String, Object> map = convertCodeMap(lst);
    	model.addAttribute("mapCstCde", map);
    }
    
    
    /**
     * 개폐여부코드 (OFF_CDE)
     * getOffCdeListModel
     * @param model
     * @throws Exception 
     * @return ${blsCdeList} ${mapOffCde}
     */
    public void getOffCdeListModel(Model model) throws Exception {
    	/*개폐여부*/
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put("typeCode", "OFF_CDE");
    	List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectCodeList", param);
    	model.addAttribute("offCdeList", lst);  
    	/// 공통코드명을 화면에서 보여주기위해서 맵형태로 변환함
    	Map<String, Object> map = convertCodeMap(lst);
    	model.addAttribute("mapOffCde", map);
    }
    
    
    /**
     * 유량계종류 (GAG_CDE)
     * getGagCdeListModel
     * @param model
     * @throws Exception 
     * @return ${blsCdeList} ${mapGagCde}
     */
    public void getGagCdeListModel(Model model) throws Exception {
    	/*유량계종류*/
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put("typeCode", "GAG_CDE");
    	List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectCodeList", param);
    	model.addAttribute("gagCdeList", lst);  
    	/// 공통코드명을 화면에서 보여주기위해서 맵형태로 변환함
    	Map<String, Object> map = convertCodeMap(lst);
    	model.addAttribute("mapGagCde", map);
    }

    
    /**
     * 맨홀종류 (SOM_CDE)
     * getSomCdeListModel
     * @param model
     * @throws Exception 
     * @return ${somCdeList} ${mapSomCde}
     */
    public void getSomCdeListModel(Model model) throws Exception {
    	/*유량계종류*/
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put("typeCode", "SOM_CDE");
    	List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectCodeList", param);
    	model.addAttribute("somCdeList", lst);  
    	/// 공통코드명을 화면에서 보여주기위해서 맵형태로 변환함
    	Map<String, Object> map = convertCodeMap(lst);
    	model.addAttribute("mapSomCde", map);
    }

    
    /**
     * 맨홇형태 (MHS_CDE)
     * getMhsCdeListModel
     * @param model
     * @throws Exception 
     * @return ${mhsCdeList} ${mapMhsCde}
     */
    public void getMhsCdeListModel(Model model) throws Exception {
    	/*유량계종류*/
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put("typeCode", "MHS_CDE");
    	List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectCodeList", param);
    	model.addAttribute("mhsCdeList", lst);  
    	/// 공통코드명을 화면에서 보여주기위해서 맵형태로 변환함
    	Map<String, Object> map = convertCodeMap(lst);
    	model.addAttribute("mapMhsCde", map);
    }
    

    /**
     * 수압계종류 (PGA_CDE)
     * getPgaCdeListModel
     * @param model
     * @throws Exception 
     * @return ${blsCdeList} ${mapPgaCde}
     */
    public void getPgaCdeListModel(Model model) throws Exception {
    	/*수압계종류*/
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put("typeCode", "PGA_CDE");
    	List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectCodeList", param);
    	model.addAttribute("pgaCdeList", lst);  
    	/// 공통코드명을 화면에서 보여주기위해서 맵형태로 변환함
    	Map<String, Object> map = convertCodeMap(lst);
    	model.addAttribute("mapPgaCde", map);
    }
    
    
    /**
     * 취수방법 (WGW_CD)
     * getWgwCdeListModel
     * @param model
     * @throws Exception 
     * @return ${blsCdeList} ${mapWgwCde}
     */
    public void getWgwCdeListModel(Model model) throws Exception {
    	/*취수방법*/
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put("typeCode", "WGW_CD");
    	List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectCodeList", param);
    	model.addAttribute("wgwCdeList", lst);  
    	/// 공통코드명을 화면에서 보여주기위해서 맵형태로 변환함
    	Map<String, Object> map = convertCodeMap(lst);
    	model.addAttribute("mapWgwCde", map);
    }

    
    /**
     * 도수방법 (WRW_CDE)
     * getWrwCdeListModel
     * @param model
     * @throws Exception 
     * @return ${blsCdeList} ${mapWrwCde}
     */
    public void getWrwCdeListModel(Model model) throws Exception {
    	/*도수방법*/
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put("typeCode", "WRW_CDE");
    	List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectCodeList", param);
    	model.addAttribute("wrwCdeList", lst);  
    	/// 공통코드명을 화면에서 보여주기위해서 맵형태로 변환함
    	Map<String, Object> map = convertCodeMap(lst);
    	model.addAttribute("mapWrwCde", map);
    }
    
    
    /**
     * 배수지제어방법 (SCW_CDE)
     * getScwCdeListModel
     * @param model
     * @throws Exception 
     * @return ${blsCdeList} ${mapScwCde}
     */
    public void getScwCdeListModel(Model model) throws Exception {
    	/*배수지제어방법*/
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put("typeCode", "SCW_CDE");
    	List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectCodeList", param);
    	model.addAttribute("scwCdeList", lst);  
    	/// 공통코드명을 화면에서 보여주기위해서 맵형태로 변환함
    	Map<String, Object> map = convertCodeMap(lst);
    	model.addAttribute("mapScwCde", map);
    }

    

    /**
     * 보수구분 (REP_CDE)
     * getReqCdeListModel
     * @param model
     * @throws Exception 
     * @return ${repCdeList} ${mapReqCde}
     */
    public void getReqCdeListModel(Model model) throws Exception {
    	/*보수구분*/
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put("typeCode", "REP_CDE");
    	List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectCodeList", param);
    	model.addAttribute("repCdeList", lst);  
    	/// 공통코드명을 화면에서 보여주기위해서 맵형태로 변환함
    	Map<String, Object> map = convertCodeMap(lst);
    	model.addAttribute("mapReqCde", map);
    }
    
    
    /**
     * 보수원인 (SBJ_CDE)
     * getSbjCdeListModel
     * @param model
     * @throws Exception 
     * @return ${sbjCdeList} ${mapReqCde}
     */
    public void getSbjCdeListModel(Model model) throws Exception {
    	/*보수원인*/
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put("typeCode", "SBJ_CDE");
    	List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectCodeList", param);
    	model.addAttribute("sbjCdeList", lst);  
    	/// 공통코드명을 화면에서 보여주기위해서 맵형태로 변환함
    	Map<String, Object> map = convertCodeMap(lst);
    	model.addAttribute("mapSbjCde", map);
    }
    
    
    
    /**
     * 민원구분 (APL_CDE)
     * getAplCdeListModel
     * @param model
     * @throws Exception 
     * @return ${splCdeList} ${mapAplCde}
     */
    public void getAplCdeListModel(Model model) throws Exception {
    	/*민원구분*/
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put("typeCode", "APL_CDE");
    	List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectCodeList", param);
    	model.addAttribute("aplCdeList", lst);  
    	/// 공통코드명을 화면에서 보여주기위해서 맵형태로 변환함
    	Map<String, Object> map = convertCodeMap(lst);
    	model.addAttribute("mapAplCde", map);
    }
    
    
    /**
     * 누수원인 (LRS_CDE)
     * getLrsCdeListModel
     * @param model
     * @throws Exception 
     * @return ${lrsCodeList} ${mapLrsCde}
     */
    public void getLrsCdeListModel(Model model) throws Exception {
    	/*누수원인구분*/
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put("typeCode", "LRS_CDE");
    	List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectCodeList", param);
    	model.addAttribute("lrsCodeList", lst);  
    	/// 공통코드명을 화면에서 보여주기위해서 맵형태로 변환함
    	Map<String, Object> map = convertCodeMap(lst);
    	model.addAttribute("mapLrsCde", map);
    }

    
    /**
     * 누수부위 (LEP_CDE)
     * getLepCdeListModel
     * @param model
     * @throws Exception 
     * @return ${lepCodeList} ${mapLepCde}
     */
    public void getLepCdeListModel(Model model) throws Exception {
    	/*지급구분*/
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put("typeCode", "LEP_CDE");
    	List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectCodeList", param);
    	model.addAttribute("lepCodeList", lst);  
    	/// 공통코드명을 화면에서 보여주기위해서 맵형태로 변환함
    	Map<String, Object> map = convertCodeMap(lst);
    	model.addAttribute("mapLepCde", map);
    }
    
    /**
     * 개통상태 (SOO_CDE)
     * getSooCdeListModel
     * @param model
     * @throws Exception 
     * @return ${SooCdeList} ${mapSooCde}
     */
    public void getSooCdeListModel(Model model) throws Exception {
    	
		/*개통상태*/		
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put("typeCode", "SOO_CDE");
    	List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectCodeList", param);
    	model.addAttribute("sooCdeList", lst);  
    	/// 공통코드명을 화면에서 보여주기위해서 맵형태로 변환함
    	Map<String, Object> map = convertCodeMap(lst);
    	model.addAttribute("mapSooCde", map);
    }
    
    
    /**
     * 탐사 (IQT_CDE)
     * getIqtCdeListModel
     * @param model
     * @throws Exception 
     * @return ${IqtCdeList} ${mapIqtCde}
     */
    public void getIqtCdeListModel(Model model) throws Exception {
    	
    	/*탐사*/	
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put("typeCode", "IQT_CDE");
    	List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectCodeList", param);
    	model.addAttribute("iqtCdeList", lst);  
    	/// 공통코드명을 화면에서 보여주기위해서 맵형태로 변환함
    	Map<String, Object> map = convertCodeMap(lst);
    	model.addAttribute("mapIqtCde", map);
    }
	

    
    
    
    
    
    
    
    
    /*********************************************
     * 2.일반코드
     */
	
    
    /**
     * 지형지물코드
     * getFtrcMaListModel
     * @param model
     * @throws Exception 
     * @return ${cmtFtrcMaList} ${mapFtrcMa}
     */
    public void getFtrcMaListModel(Model model) throws Exception {
    	/*지형지물*/
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put("ftrCde", "");
		List<Map<String, String>> lst = (List<Map<String, String>>) cmmService.selectList("selectCmtFtrcMaList", param);
    	
        model.addAttribute("cmtFtrcMaList", lst);
        
        Map<String, String> retMap = new HashMap<String, String>();
        for (Map<String, String> object: lst) {
        	retMap.put(object.get("ftrCde"), object.get("ftrNam"));
		}
        model.addAttribute("mapFtrcMa", retMap);
    }
    
    
    
    /**
     * 행정동코드
     * getAdarMaListModel
     * @param model
     * @throws Exception 
     * @return ${cmtAdarMaList} ${mapAdarMa}
     */
    public void getAdarMaListModel(Model model) throws Exception {
    	/*행정동  */
    	Map<String, Object> param = new HashMap<String, Object>();
		List<Map<String, String>> lst = (List<Map<String, String>>) cmmService.selectList("selectCmtAdarMaList", param);
    	
		model.addAttribute("cmtAdarMaList", lst);
    	
		HashMap<String, String> retMap = new HashMap<String, String>();
        for (Map<String, String> object: lst) {
        	retMap.put(object.get("hjdCde"), object.get("hjdNam"));
		}
        model.addAttribute("mapAdarMa", retMap);
    }
    
    

    /**
     * 법정동코드
     * getLgarMaListModel
     * @param model
     * @throws Exception 
     * @return ${cmtLgarMaList} ${mapLgarMa}
     */
    public void getLgarMaListModel(Model model) throws Exception {
    	/*행정동*/
    	Map<String, Object> param = new HashMap<String, Object>();
		List<Map<String, String>> lst = (List<Map<String, String>>) cmmService.selectList("selectCmtLgarMaList", param);
    	model.addAttribute("cmtLgarMaList", lst);
    	
    	Map<String, String> retMap = new HashMap<String, String>();
        for (Map<String, String> object: lst) {
        	retMap.put(object.get("bjdCde"), object.get("bjdNam"));
		}
        model.addAttribute("mapLgarMa", retMap);
    }
    
    
    
    /**
     * 소모품/예비품 (PDJT_DT)
     * getPdjtDtModel
     * @param model
     * @throws Exception 
     * @return ${pdjtDtList}+type ${mapPdjtMa}
     */
    public void getPdjtDtModel(String type, Model model) throws Exception {
    	
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put("type", type);
		List<Map<String, String>> lst = (List<Map<String, String>>) cmmService.selectList("selectWttPdjtMaDtList", param);
    	model.addAttribute("pdjtDtList"+type, lst);
    	
    	Map<String, String> retMap = new HashMap<String, String>();
        for (Map<String, String> map: lst) {
        	retMap.put(map.get("pdtNam"), map.get("pdtNam"));
		}
        model.addAttribute("mapPdjtMa"+type, retMap);
    }
    
    
    /**
     * 소모품 (PDJT_DT_0)
     * getPdjtDtModel0
     * @param model
     * @throws Exception 
     * @return ${pdjtDtList} ${mapPdjtMa}
     */
    public void getPdjtDtModel0(Model model) throws Exception {
    	getPdjtDtModel("0", model);
    }
    
    /**
     * 예비품 (PDJT_DT_1)
     * getPdjtDtModel1
     * @param model
     * @throws Exception 
     * @return ${pdjtDtList} ${mapPdjtMa}
     */
    public void getPdjtDtModel1(Model model) throws Exception {
    	getPdjtDtModel("1", model);
    }
    
    

    
    /**
     * 작업그룹관리코드
     * getCkmcMaListModel
     * @param model
     * @throws Exception 
     * @return ${cmtCkmcMaList} ${mapCkmcMa}
     */
    public void getCkmcMaListModel(Model model) throws Exception {

    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put("useChk", "1");
		List<Map<String, String>> lst = (List<Map<String, String>>) cmmService.selectList("selectCmtCkmcMaList", param);
    	model.addAttribute("cmtCkmcMaList", lst);
    	
    	Map<String, String> retMap = new HashMap<String, String>();
        for (Map<String, String> map: lst) {
        	retMap.put(map.get("ckmCde"), map.get("ckmNam"));
		}
        model.addAttribute("mapCkmcMa", retMap);
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /*****************************************************************
     * 기타 FUNCTION
     */
	
	
	/**
	 * 공통코드리스트를 맵형태로 변환
	 * @param comCodeList
	 * @return
	 * @throws Exception
	 */
    public Map<String, Object> convertCodeMap(List<Map<String, Object>> comCodeList) throws Exception {
    	Map<String, Object> comCodeMap = new HashMap<String, Object>();
    	comCodeMap.clear();
    	
        for (Map<String, Object> map: comCodeList) {
        	comCodeMap.put((String)map.get("codeCode"), map.get("codeAlias"));
		}
        return comCodeMap;
    }
	

    
    
    
    
    
    /**
     * 파라미터가 포함된 요청 URL을 만들어줌
     * @param request
     * @return
     */
    public String getRefererPath(HttpServletRequest request) {
    	String Referer = request.getHeader("referer");
    	URL pathUrl = null;
    	try {
			pathUrl = new URL(Referer);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	String returnUrl = pathUrl.getPath().replace(request.getContextPath(), "");
    	String queryString = pathUrl.getQuery();

    	return returnUrl + "?" + queryString;
    }
    
    
    
    //    NFC태그 정보    
    public void getNfcId(HttpServletRequest request, Model model) throws Exception {
    	String ftrCde = EgovStringUtil.nullConvert(request.getParameter("ftrCde"));
    	String ftrIdn = EgovStringUtil.nullConvert(request.getParameter("ftrIdn"));
    	String nfcId = "";
    	SimpleDateFormat nfcDate = new SimpleDateFormat("yyyy-MM-dd");
    	
    	if(ftrIdn.equals("")) {
    		ftrIdn = "0";
    	}
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put("ftrCde", ftrCde);
    	param.put("ftrIdn", BigDecimal.valueOf(Integer.valueOf(ftrIdn)));
		List<Map<String, String>> lst = (List<Map<String, String>>) cmmService.selectList("selectWttNfcdDt", param);
    	
    	
    	
    	if(lst != null && !lst.isEmpty()) {
    		nfcId = lst.get(0).get("ftrCde") + "-" + lst.get(0).get("ftrIdn");
    		nfcDate.format(lst.get(0).get("nfcYmd"));
    	}
    	
    	model.addAttribute("strNfcId", nfcId);
    }
    
    
	
}
