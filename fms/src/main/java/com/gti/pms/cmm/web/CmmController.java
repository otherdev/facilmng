package com.gti.pms.cmm.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.gti.pms.cmm.service.CmmService;
import com.gti.pms.cmm.service.MenuManageVO;
import com.gti.pms.cmm.util.JsonUtils;

/**
 * 사업관리 
 * @author 공통서비스 개발팀 
 * @see
 *
 */
@Controller
public class CmmController {


	/** cmmService */
	@Resource(name = "cmmService")
	private CmmService cmmService;

	@Resource(name = "jsonUtils")
	private JsonUtils jsonUtils;

	
	
	

	
	/**
	 * 공통조회  - ajax
	 * @param request
	 * @param commandMap : sqlId - 쿼리아이디, sqlId2 - 쿼리아이디2 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/cmm/selectList.do")
	public ModelAndView selectList(HttpServletRequest request, CommandMap commandMap) throws Exception {
		ModelAndView mv = new ModelAndView("/proc/proc");
		Map<String, Object> map = commandMap.getMap();
		
		JSONObject result = new JSONObject();
		
		try {
			result.put("result", true);
			result.put("data", jsonUtils.List2JSONArr((List<Map<String, Object>>) cmmService.selectList(map.get("sqlId").toString(), map)));
			
			// 다른 조회건이 있으면 data2로 보낸다
			if(map.get("sqlId2") != null && !"".equals(map.get("sqlId2").toString()) ){
				result.put("data2", jsonUtils.List2JSONArr((List<Map<String, Object>>) cmmService.selectList(map.get("sqlId2").toString(), map)));
			}			
			// 다른 조회건이 있으면 data2로 보낸다
			if(map.get("sqlId3") != null && !"".equals(map.get("sqlId2").toString()) ){
				result.put("data3", jsonUtils.List2JSONArr((List<Map<String, Object>>) cmmService.selectList(map.get("sqlId3").toString(), map)));
			}
			// 다른 조회건이 있으면 data2로 보낸다
			if(map.get("sqlId4") != null && !"".equals(map.get("sqlId2").toString()) ){
				result.put("data4", jsonUtils.List2JSONArr((List<Map<String, Object>>) cmmService.selectList(map.get("sqlId4").toString(), map)));
			}
			// 다른 조회건이 있으면 data2로 보낸다
			if(map.get("sqlId5") != null && !"".equals(map.get("sqlId2").toString()) ){
				result.put("data5", jsonUtils.List2JSONArr((List<Map<String, Object>>) cmmService.selectList(map.get("sqlId5").toString(), map)));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			result.put("result", false);
			result.put("error", e.getMessage());
		}
		
		
		
		mv.addObject("result", result.toJSONString());
		
		return mv;
	}
	
	
	
	
	
	/**
	 * ajax 단건 공통저장처리
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/pms/upd.do")
	@ResponseBody
	public ModelAndView upd(@RequestBody Map<String,Object> param) throws Exception {
		ModelAndView mv = new ModelAndView("/proc/proc");

		JSONObject ret = new JSONObject();

		try{
			cmmService.saveData(param);
			ret.put("result", true);
			
		}catch(DuplicateKeyException due){
			ret.put("result", false);
			ret.put("error", "중복된 키가 있습니다.");
		}catch(DataIntegrityViolationException de){
			// 자식레코드 발견
			ret.put("result", false);
			ret.put("error", "데이터 무결성 예외가 발생했습니다.");
		}
		catch(Exception e){
			ret.put("result", false);
			ret.put("error", e.getMessage());
		}
		
		mv.addObject("result", ret.toJSONString());
		return mv;
	}
	
	/**
	 * ajax 리스트 공통저장처리
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/pms/updList.do")
	@ResponseBody
	public ModelAndView updList(@RequestBody Map<String,Object> param) throws Exception {
		ModelAndView mv = new ModelAndView("/proc/proc");
		String result = "";
		
		try{
			result = cmmService.saveCmmList(param);
		}catch(Exception e){
			JSONObject ret = new JSONObject();
			ret.put("result", false);
			ret.put("error", e.getMessage());
			result = ret.toJSONString();
		}
		
		mv.addObject("result", result);
		return mv;
	}
	
	
	
	/**
	 * ajax 단건 form 데이터 공통저장처리
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/pms/formUpd.do")
	public ModelAndView upd(CommandMap commandMap, HttpServletRequest request) throws Exception {
		ModelAndView mv = new ModelAndView("/proc/proc");

		String result = "";
		try{
			result = cmmService.saveFormData(commandMap.getMap());
		}catch(Exception e){
			JSONObject ret = new JSONObject();
			ret.put("result", false);
			ret.put("error", e.getMessage());
			result = ret.toJSONString();
		}
		
		mv.addObject("result", result);

		return mv;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
    /**
     * 좌측메뉴를 조회한다.
     * @param menuManageVO MenuManageVO
     * @param vStartP      String
     * @return 출력페이지정보 "main_left"
     * @exception Exception
     */
    @RequestMapping(value="/pms/main/Left.do")
    public String selectMainMenuLeft(
    		@ModelAttribute("menuManageVO") MenuManageVO menuManageVO,ModelMap model
			)
            throws Exception {

    	//menuManageVO.setUpperMenuNo(upperMenuNo);
    	
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("upperMenuNo", "0");
    	List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectMainMenuLeft", map);
		model.addAttribute("list_menulist", list);

		
      	return "main/Left";
    }
	
	
	
	
}
