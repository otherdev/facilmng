package com.gti.pms.cmm.service;

import java.util.List;

/**
 * @Class Name : WttFlgpMaService.java
 * @Description : WttFlgpMa Business class
 * @Modification Information
 *
 * @author DRCTS
 * @since 2018-07-01
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */
public interface FmsFileService {
	
	
    
    public String saveFmsFile(FmsFileMstVO fmst, List<FmsFileDtlVO> fdtlList) throws Exception;
}
