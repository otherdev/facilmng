package com.gti.pms.cmm.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.json.simple.JSONObject;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.gti.pms.cmm.service.CmmService;
import com.gti.pms.cmm.service.ComDtlVO;

import egovframework.com.cmm.LoginVO;
import egovframework.com.cmm.SessionUtil;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;

/**
 * @Class Name : CmmServiceImpl.java
 * @Description : 공통코드등 전체 업무에서 공용해서 사용해야 하는 서비스를 정의하기위한 서비스 구현 클래스
 * @Modification Information
 * @see
 *
 */
@Service("cmmService")
public class CmmServiceImpl extends EgovAbstractServiceImpl implements CmmService {

	@Resource(name = "cmmDAO")
	private CmmDAO cmmDAO;


	/**
	 * 목록을 조회
	 * @param sqlId - 쿼리아이디, map - 파라미터 정보
	 * 
	 */
	public List<?> selectList(String sqlId, Map<String, Object> param) throws Exception {
		return cmmDAO.list(sqlId, param);
	}
	
	/**
	 * 목록을 조회
	 * @param sqlId - 쿼리아이디, parameterObject - 파라미터 정보 vo
	 * 
	 */
	public List<?> selectList(String sqlId, Object parameterObject) throws Exception {
		return cmmDAO.list(sqlId, parameterObject);
	}
	
	/**
	 * 단건을 조회
	 * @param sqlId - 쿼리아이디, map - 파라미터 정보
	 * 
	 */
	public Object select(String sqlId, Map<String, Object> param) throws Exception {
		return cmmDAO.select(sqlId, param);
	}
	
	
	/**
	 * ajax 단건데이터 저정 공통처리
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@Override
	public String saveData(Map<String,Object> param) throws Exception {
		
		// 1.단건데이터  저장
		String sqlId = (String)param.get("sqlId");
		Map<String, Object> data = (Map<String, Object>) param.get("data");
		try{
			data.put("id", SessionUtil.getId());
		}catch(Exception e){
			data.put("id", "nosession");
		}
		
		int cnt = 0;
		try{
			int r = cmmDAO.update(sqlId, data);
			if(r>0)	cnt++;
			
		}
		catch(Exception e){
			throw e;
		}
		
		// 2 쿼리있으면수행
		if( param.get("sqlId2")!= null && !"".equals(param.get("sqlId2"))){
			String sqlId2 = (String)param.get("sqlId");
			Map<String, Object> data2 = (Map<String, Object>) param.get("data2");
			try{
				data2.put("id", SessionUtil.getId());
			}catch(Exception e){
				data2.put("id", "nosession");
			}

			try{
				int r = cmmDAO.update(sqlId2, data2);
				if(r>0)	cnt++;
				
			}catch(Exception e){
				throw e;
			}
		}
		
		return "OK";
	}	
	
	
	/**
	 * ajax 리스트 데이터 저정 공통처리
	 */
	@Override
	public String saveCmmList(Map<String,Object> param) throws Exception {
		JSONObject result = new JSONObject();
		
		
		// 1.리스트  저장
		String sqlId = (String)param.get("sqlId");
		List<Map<String, Object>> lst = (List<Map<String, Object>>) param.get("lst");
		
		int cnt = 0;
		for (Map<String, Object> map : lst) {
			if(map == null)	continue;
			
			map.put("id", SessionUtil.getId());
			try{
				int r = cmmDAO.update(sqlId, map);//신규,수정처리
				if(r>0)	cnt++;
				
			}catch(DataIntegrityViolationException de){
				// 자식레코드 발견
				result.put("result", false);
				result.put("error", "데이터 무결성 예외가 발생했습니다.");
				return result.toJSONString();
			}
			catch(Exception e){
				throw e;
			}
		}
		
		
		// 2.리스트2 저장
		if( param.get("sqlId2")!= null && !"".equals(param.get("sqlId2"))){
			String sqlId2 = (String)param.get("sqlId2");
			List<Map<String, Object>> lst2 = (List<Map<String, Object>>) param.get("lst2");
			
			for (Map<String, Object> map : lst2) {
				map.put("id", SessionUtil.getId());
				try{
					int r = cmmDAO.update(sqlId2, map);
					if(r>0)	cnt++;
					
				}catch(DataIntegrityViolationException de){
					// 자식레코드 발견
					result.put("result", false);
					result.put("error", "데이터 무결성 예외가 발생했습니다.");
					return result.toJSONString();
				}
				catch(Exception e){
					throw e;
				}
			}
		}
		
		
		result.put("result", true);
		result.put("cnt", cnt);
		return result.toJSONString();
	}
	
	
	
	
	/**
	 * ajax 단건데이터 저정 공통처리
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@Override
	public String saveFormData(Map<String,Object> param) throws Exception {
		
		// 1.단건데이터  저장
		String sqlId = (String)param.get("sqlId");
		param.put("id", SessionUtil.getId());
		
		int cnt = 0;
		try{
			int r = cmmDAO.update(sqlId, param);
			if(r>0)	cnt++;
			
		}catch(Exception e){
			throw e;
		}
		
		JSONObject result = new JSONObject();
		result.put("result", true);
		result.put("cnt", cnt);
		return result.toJSONString();
	}	
	
		
	
	
	
	
	
	/**
	 * 공통코드명 가져오기
	 */
	@Override
	public String selectDtlCdNm(String dtlCd) throws Exception {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("dtlCd", dtlCd);
		List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmDAO.select("selectComDtlCdList", param);
		
		String dtlCdNm = "";
		if(lst != null && !lst.isEmpty()){
			dtlCdNm = (String) lst.get(0).get("cdNm");
		}
		
		return dtlCdNm;
	}

	
	/**
	 * 행정동명 가져오기
	 */
	@Override
	public String selectHjdNm(String hjdCde) throws Exception {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("hjdCde", hjdCde);
		List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmDAO.select("selectHjdList", param);
		
		String hjdNam = "";
		if(lst != null && !lst.isEmpty()){
			hjdNam = (String) lst.get(0).get("hjdNam");
		}
		
		return hjdNam;
	}
	
	
	
	
	/**
	 * 해당사업의 관리자여부 체크	
	 */
	@Override
	public String isBizMngYn(Map<String,Object> map) throws Exception {
		map.put("id", SessionUtil.getId());
		
		List<Map<String,Object>> lst = (List<Map<String, Object>>) cmmDAO.list("selectBizAdminList", map);
		if(lst != null && !lst.isEmpty()){
			return "Y";
		}
		else{
			return "N";
		}
	}
	
	@Override
	public void insertLoginLog(LoginVO vo) throws Exception {
		
	}
	@Override
	public void insertLogoutLog() throws Exception {
		
	}

}
