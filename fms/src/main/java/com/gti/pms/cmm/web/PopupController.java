package com.gti.pms.cmm.web;


import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.gti.pms.cmm.service.CmmService;
import com.gti.pms.cmpl.vo.CmplVO;
import com.gti.pms.cnst.vo.CnstVO;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;


@Controller
@RequestMapping("/pms/popup")
public class PopupController extends CodeController{
	private static final Logger logger = LoggerFactory.getLogger(PopupController.class);
    
			
	/** cmmService */
	@Resource(name = "cmmService")
	private CmmService cmmService;

    
    /** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;

	/**
 	 * 부속시설 등록팝업을 호출한다.(popup)
 	 */
	@RequestMapping("/attFacCreatePopup.do")
	public String attFacCreatePopup(
    		HttpServletRequest request,
    		HttpServletResponse response, 
    		@RequestParam(value="ftrCde", required=false) String ftrCde,
			@RequestParam(value="ftrIdn", required=false) BigDecimal ftrIdn,
			@RequestParam(value="attSeq", required=false) BigDecimal attSeq,
    		Model model)
			throws Exception {

    	model.addAttribute("ftrCde", ftrCde);
    	model.addAttribute("ftrIdn", ftrIdn);
    	model.addAttribute("attSeq", attSeq);

    	/*지형지물*/
        getFtrcMaListModel(model);        
		/*관리기관*/
		getMngCdeListModel(model);		
		/*행정동*/
		getAdarMaListModel(model);		
		/*공사구분*/
		getCntCdeListModel(model);		
		/*계약방법*/
		getCttCdeListModel(model);		
		
    	return "pms/popup/attFacCreatePopup";
	}	

	/**
 	 * 부속시설 상세팝업을 호출한다.(popup)
 	 */
	@RequestMapping("/attFacViewPopup.do")
	public String attFacViewPopup(
    		HttpServletRequest request,
    		HttpServletResponse response, 
    		@RequestParam(value="ftrCde", required=false) String ftrCde,
    		@RequestParam(value="ftrIdn", required=false) BigDecimal ftrIdn,
    		@RequestParam(value="attaSeq", required=false) BigDecimal attaSeq,		
    		Model model) throws Exception {
    		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("ftrCde", ftrCde);
		map.put("ftrIdn", ftrIdn);
		map.put("attaSeq", attaSeq);
				
		Map<String, Object> rmap = (Map<String, Object>) cmmService.select("selectPopupWttAttaDt", map);		
		model.addAttribute("resultVO", rmap);

		model.addAttribute("ftrCde", rmap.get("ftrCde"));
		model.addAttribute("ftrIdn", rmap.get("ftrIdn"));
		model.addAttribute("attaSeq", rmap.get("attaSeq"));

		/*지형지물*/
        getFtrcMaListModel(model);        
		/*관리기관*/
		getMngCdeListModel(model);		
		/*행정동*/
		getAdarMaListModel(model);		
		/*공사구분*/
		getCntCdeListModel(model);		
		/*계약방법*/
		getCttCdeListModel(model);

    	return "pms/popup/attFacViewPopup";
	}



    /*    
     *  상수공사대장 Start
     */
	/**
	 * 상수공사대장  목록 조회 (paging)
	 * @param searchVO - 조회할 정보가 담긴 CnstVO
	 * @return /pms/popup/cnstMngPopup.do
	 * @exception Exception
	 */
	@RequestMapping("/cnstMngPopup.do")
	public String consMaListPopup (HttpServletRequest request, 
			@ModelAttribute("searchVO") CnstVO searchVO, Model model) throws Exception {
		
		searchVO.setPageUnit(propertiesService.getInt("pageUnit")); 
		searchVO.setPageSize(propertiesService.getInt("pageSize"));
		
		//paging
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());
		
		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		
		//조회
		List<Map<String, Object>> wttConsMaList = (List<Map<String, Object>>) cmmService.selectList("selectWttConsMaList", searchVO);
		model.addAttribute("resultList", wttConsMaList);
		
		//공통코드내려주기
		/*지형지물*/
        getFtrcMaListModel(model);        
		/*관리기관*/
		getMngCdeListModel(model);		
		/*행정동*/
		getAdarMaListModel(model);		
		/*공사구분*/
		getCntCdeListModel(model);		
		/*계약방법*/
		getCttCdeListModel(model);
		
		//페이징처리
		List<Map<String, Object>> listCnt = (List<Map<String, Object>>) cmmService.selectList("selectWttConsMaListTotCnt", searchVO);
		int totCnt = 0;
		try{
			totCnt = ((BigDecimal) listCnt.get(0).get("totCnt")).intValue();
		}catch(Exception e){
			logger.debug("..." + e.getMessage());
		};
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);

    	return "pms/popup/cnstMngPopup";
	}

    /*    
     *  상수공사대장 End
     */

	/*    
     *  급수공사대장 Start
     */
    @RequestMapping("/facetMngPopup.do")
    public String facetMngPopup (@ModelAttribute("searchVO") CnstVO searchVO, Model model) throws Exception {
    	
    	searchVO.setPageUnit(propertiesService.getInt("pageUnit")); 
    	searchVO.setPageSize(propertiesService.getInt("pageSize"));
    	
    	//paging
    	PaginationInfo paginationInfo = new PaginationInfo();
    	paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
    	paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
    	paginationInfo.setPageSize(searchVO.getPageSize());
    	
    	searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
    	searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
    	searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
    	    			
		/*행정동*/
		getAdarMaListModel(model);
		
		/*공사구분*/
		getCntCdeListModel(model);	
    	    	
        List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectWttSplyMaList", searchVO);
        model.addAttribute("resultList", list);
        
        List<Map<String, Object>> listCnt = (List<Map<String, Object>>) cmmService.selectList("selectWttSplyMaListTotCnt", searchVO);
		int totCnt = 0;
		try{
			totCnt = ((BigDecimal) listCnt.get(0).get("totCnt")).intValue();
		}catch(Exception e){
			logger.debug("..." + e.getMessage());
		};
		
		model.addAttribute("totCnt", totCnt);
		
    	paginationInfo.setTotalRecordCount(totCnt);
    	model.addAttribute("paginationInfo", paginationInfo);
    	
    	model.addAttribute("noQrImage", "true");
    	
    	return "pms/popup/facetMngPopup";
    }
    
    

	/**
	 *RSRV_DT 
	 */
	@RequestMapping("/wtrTrk/attFacCreatePopup.do")
	public String wtrTrk_attFacCreatePopup(
			HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value="ftrCde", required=false) String ftrCde,
			@RequestParam(value="ftrIdn", required=false) BigDecimal ftrIdn,
			Model model) throws Exception {				
		
		Map<String, Object> rmap = (Map<String, Object>) cmmService.select("selectPopupWttRsrvDtAttIdn", null);			
		
		int attIdn = 0;
		try{
			attIdn = ((BigDecimal) rmap.get("attIdn")).intValue();
		}catch(Exception e){
			logger.debug("..." + e.getMessage());
		};
		
		model.addAttribute("attNumVal", attIdn + 1);
		
		/*지형지물*/
        getFtrcMaListModel(model);        
		/*관리기관*/
		getMngCdeListModel(model);		
		/*행정동*/
		getAdarMaListModel(model);		
		/*공사구분*/
		getCntCdeListModel(model);		
		/*계약방법*/
		getCttCdeListModel(model);		
		
		model.addAttribute("ftrCde", ftrCde);
		model.addAttribute("ftrIdn", ftrIdn);
		
		/*관재질*/
		getMopCdeListModel(model);
		
		return "pms/popup/wtrTrk_attFacCreatePopup";
	}
	
	

	@RequestMapping("/wtrTrk/attFacViewPopup.do")
	public String wtrTrk_attFacViewPopup(
		HttpServletRequest request,
		HttpServletResponse response,
		@RequestParam(value="ftrCde", required=false) String ftrCde,
		@RequestParam(value="ftrIdn", required=false) BigDecimal ftrIdn,
		@RequestParam(value="rsrvSeq", required=false) BigDecimal rsrvSeq,		
		Model model) throws Exception {
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("ftrCde", ftrCde);
		map.put("ftrCde", ftrCde);
		map.put("rsrvSeq", rsrvSeq);
				
		Map<String, Object> rmap = (Map<String, Object>) cmmService.select("selectPopupWttRsrvDt", map);		
		model.addAttribute("resultVO", rmap);

		model.addAttribute("ftrCde", rmap.get("ftrCde"));
		model.addAttribute("ftrIdn", rmap.get("ftrIdn"));
		model.addAttribute("rsrvSeq", rmap.get("rsrvSeq"));
		
		/*관재질*/
		getMopCdeListModel(model);

		return "pms/popup/wtrTrk_attFacViewPopup";
	}

	/**
	 * WTT_WSER_MA 목록을 조회한다. (pageing)
	 * @param searchVO - 조회할 정보가 담긴 WttWserMaDefaultVO
	 * @return jspRoot + "/WttWserMaList"
	 * @exception Exception
	 */
    @RequestMapping(value="/cnstCmplPopup.do")
    public String cnstCmplPopup(HttpServletRequest request, @ModelAttribute("searchVO") CmplVO searchVO, 
    		Model model)
            throws Exception {
    	
    	/** EgovPropertyService.sample */
    	searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	searchVO.setPageSize(propertiesService.getInt("pageSize"));
    	
    	/** pageing */
    	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());
		
		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		
		List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectCnstCmplList", searchVO);
		model.addAttribute("resultList", list);
		
		List<Map<String, Object>> listCnt = (List<Map<String, Object>>) cmmService.selectList("selectCnstCmplListTotCnt", searchVO);
		int totCnt = 0;
		try{
			totCnt = ((BigDecimal) listCnt.get(0).get("totCnt")).intValue();
		}catch(Exception e){
			logger.debug("..." + e.getMessage());
		};

		model.addAttribute("totCnt", totCnt);
        
		paginationInfo.setTotalRecordCount(totCnt);
        model.addAttribute("paginationInfo", paginationInfo);
        
        /*지형지물*/
    	getFtrcMaListModel(model);    	
    	/*관리기관*/
    	getMngCdeListModel(model);    	
		/*행정동*/
    	getAdarMaListModel(model);    	
    	/*민원구분*/
    	getAplCdeListModel(model);
    	
        return "pms/popup/cnstCmplPopup";
    } 

}