package com.gti.pms.cmm.web;


import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.gti.pms.cmm.service.CmmService;
import com.gti.pms.cmm.service.LinkVO;
import com.gti.pms.cnst.vo.CnstVO;
import com.gti.pms.util.QrUtil;

import egovframework.let.utl.fcc.service.EgovStringUtil;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;


@Controller
@RequestMapping("/pms/link")
public class LinkController extends CodeController{

    
	@Resource(name = "cmmService")
	private CmmService cmmService;

    
    /** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;
    
	
	
    /**
     * 코드정의
     * @param model
     * @throws Exception
     */
    private void setCodeModels(HttpServletRequest request, Model model) throws Exception {
        /*지형지물*/
        getFtrcMaListModel(model);
        
		/*관리기관*/
		getMngCdeListModel(model);
		
		/*행정동*/
		getAdarMaListModel(model);
		
		/*공사구분*/
		getCntCdeListModel(model);		
		
		/*계약방법*/
		getCttCdeListModel(model);		
		
        /*보수구분*/
		getReqCdeListModel(model);
		
		/*보수원인*/
		getSbjCdeListModel(model);
		
    }
	
    
    
    
    

    
    
    /**
     * 도면사진
     */
    @RequestMapping("/lnkDwgPhotoList.do")
    public String lnkDwgPhotoList(
    		HttpServletRequest request,
    		HttpServletResponse response,
			@RequestParam(value="bizId", required=false) String bizId,
    		Model model) throws Exception {
    	
    	//String cntNum = EgovStringUtil.nullConvert(request.getParameter("cntNum"));

    	//============= 도면 및 사진 =============//		
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	map.put("bizId", bizId);
    	map.put("grpTyp", LinkVO.GRP_TYPE_CONS_DRAWING);
		
    	List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectFileMapList", map);			
		model.addAttribute("drawingList", list);
		model.addAttribute("bizId", bizId);
		model.addAttribute("grpTyp", LinkVO.GRP_TYPE_CONS_DRAWING);
						    	
		setCodeModels(request, model);
		return "pms/link/lnkDwgPhotoList";
    }
    
    /**
     * 참조자료 
     */
    @RequestMapping("/lnkCnstRefList.do")
    public String lnkCnstRefList(
    		HttpServletRequest request,
    		HttpServletResponse response,
    		@RequestParam(value="bizId", required=false) String bizId,
    		Model model) throws Exception {
    	
    	//String cntNum = EgovStringUtil.nullConvert(request.getParameter("cntNum"));
    	
    	//============= 도면 및 사진 =============//		
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	map.put("bizId", bizId);
    	map.put("grpTyp", LinkVO.GRP_TYPE_CONS_REFERENCE);
    	
    	List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectFileMapList", map);			
    	model.addAttribute("refList", list);
    	model.addAttribute("bizId", bizId);
    	model.addAttribute("grpTyp", LinkVO.GRP_TYPE_CONS_REFERENCE);
    	
    	setCodeModels(request, model);
    	return "pms/link/lnkCnstRefList";
    }
    

    /**
     * 부속시설
     */
    @RequestMapping("/lnkAttFacList.do")
	public String lnkAttFacList(
			HttpServletRequest request,
			HttpServletResponse response, 
			LinkVO paramVO, Model model) throws Exception {
    
    	paramVO.setPageUnit(propertiesService.getInt("pageUnit")); 
    	paramVO.setPageSize(propertiesService.getInt("pageSize"));
    	
    	//paging
    	PaginationInfo paginationInfo = new PaginationInfo();
    	paginationInfo.setCurrentPageNo(paramVO.getPageIndex());
    	paginationInfo.setRecordCountPerPage(paramVO.getPageUnit());
    	paginationInfo.setPageSize(paramVO.getPageSize());
    	
    	paramVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
    	paramVO.setLastIndex(paginationInfo.getLastRecordIndex());
    	paramVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
    			
		List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectCmmWttAttaDtList", paramVO);

		model.addAttribute("dtResultList", list);
    	
		model.addAttribute("noQrImage", "true");
		
		setCodeModels(request, model);
		
		return "pms/link/lnkAttFacList";
	}
    
	

    
    
    
	/**
	 * 지형지물 목록을 조회한다. (pageing)
	 * @param searchVO - 조회할 정보가 담긴 
	 * @return 
	 * @exception Exception
	 */
	@RequestMapping(value="/lnkFtrList.do")
	public String lnkFtrList (HttpServletRequest request, 
			@ModelAttribute("searchVO") CnstVO searchVO, Model model) throws Exception {
		
		searchVO.setPageUnit(propertiesService.getInt("pageUnit")); 
		searchVO.setPageSize(propertiesService.getInt("pageSize"));
		
		//paging
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());
		
		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		
		//조회
		List<Map<String, Object>> wttConsMaList = (List<Map<String, Object>>) cmmService.selectList("selectFtrAllList", searchVO);
		model.addAttribute("resultList", wttConsMaList);
		
        /*지형지물*/
        getFtrcMaListModel(model);
		/*행정동*/
		getAdarMaListModel(model);
		
		//페이징처리
		List<Map<String, Object>> listCnt = (List<Map<String, Object>>) cmmService.selectList("selectFtrAllListTotCnt", searchVO);
		int totCnt = 0;
		try{
			totCnt = ((BigDecimal) listCnt.get(0).get("totCnt")).intValue();
		}catch(Exception e){
		};
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);
		
		
		return "pms/link/lnkFtrList";
	}
	
	/**
     * 유지보수 링크(점검시설 결과)
     */
    @RequestMapping("/lnkChscList.do")
    public String lnkChscList (
    		HttpServletRequest request,
    		HttpServletResponse response,
    		@RequestParam(value="ftrCde", required=false) String ftrCde,
			@RequestParam(value="ftrIdn", required=false) BigDecimal ftrIdn,
			Model model) throws Exception {	
    	
    			
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("ftrCde", ftrCde);
		map.put("ftrIdn", ftrIdn);
		
		List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectCmmChscResSubList", map);

		model.addAttribute("fmsChscFtrResList", list);
		
		model.addAttribute("ftrCde", ftrCde);
		model.addAttribute("ftrIdn", ftrIdn);		
    	
		model.addAttribute("noQrImage", "true");
		
		return "pms/link/lnkChscList";
    }
	
    
 
    
    
    
    
    
    
    /**
     * QR생성
     * genQr
     * @param data
     * @throws Exception
     * @return QR Image (PNG)
     */
    @RequestMapping("/common/genQr.do")
    public void genQr (HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
    	String data = EgovStringUtil.nullConvert(request.getParameter("data"));
    	if("" == data) {
    		data = "none";
    	}
    	QrUtil.genQr(response, data);
    }
    
 
  
}