package com.gti.pms.cmm.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import egovframework.com.cmm.SessionUtil;
import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

/**
 * @Class Name : CmmUseDAO.java
 * @Description : 공통코드등 전체 업무에서 공용해서 사용해야 하는 서비스를 정의하기위한 데이터 접근 클래스
 * @Modification Information
 *
 *    수정일       수정자         수정내용
 *    -------        -------     -------------------
 *    2009. 3. 11.     
 *
 * @author 공통 서비스 개발팀 이삼섭
 * @since 2009. 3. 11.
 * @version
 * @see
 *
 */
@Repository("cmmDAO")
public class CmmDAO extends EgovComAbstractDAO {



	
	
	/**
	 * 리스트 형태의 update
	 * @param queryId
	 * @param list
	 * @return
	 */
	public int updateBatch(String queryId, List<Map<String, Object>> list) {
		int cnt = 0;
		for (Map<String, Object> map : list) {
			map.put("id", SessionUtil.getId());
			try{
				update(queryId, map);
				cnt++;
				
			}catch(Exception e){
				throw e;
			}
		}
		return cnt;
	}
	
	
	/**
	 * 인서트후 키값을 리턴한다.
	 * @param sqlId
	 * @param map
	 * @return
	 * @throws Exception
	 */
    public Integer insertReturn(String sqlId, Map<String, Object> map) throws Exception {
        return (Integer)insert(sqlId, map);
    }
	
	
}
