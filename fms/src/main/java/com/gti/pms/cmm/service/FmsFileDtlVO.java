package com.gti.pms.cmm.service;

/**
 * @Class Name : WttFileHtVO.java
 * @Description : WttFileHt VO class
 * @Modification Information
 *
 * @author DRCTS
 * @since 2018-07-01
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */
public class FmsFileDtlVO {
    
    private java.lang.Integer seq;
    private java.lang.Integer filSeq;
    private java.lang.String dwnNam;
    private java.lang.String upfNam;
    private java.lang.String filPth;
    private java.lang.String filTyp;
    private java.lang.String filSiz;
    private java.lang.String filRst;
    private java.lang.String filExp;
    private java.lang.String curTfs;
    private java.lang.String creYmd;
    private java.lang.String creUsr;
    private java.lang.String updYmd;
    private java.lang.String updUsr;
    private java.lang.String id;
    
    
    
    
	public java.lang.Integer getSeq() {
		return seq;
	}
	public void setSeq(java.lang.Integer seq) {
		this.seq = seq;
	}
	public java.lang.Integer getFilSeq() {
		return filSeq;
	}
	public void setFilSeq(java.lang.Integer filSeq) {
		this.filSeq = filSeq;
	}
	public java.lang.String getDwnNam() {
		return dwnNam;
	}
	public void setDwnNam(java.lang.String dwnNam) {
		this.dwnNam = dwnNam;
	}
	public java.lang.String getUpfNam() {
		return upfNam;
	}
	public void setUpfNam(java.lang.String upfNam) {
		this.upfNam = upfNam;
	}
	public java.lang.String getFilPth() {
		return filPth;
	}
	public void setFilPth(java.lang.String filPth) {
		this.filPth = filPth;
	}
	public java.lang.String getFilTyp() {
		return filTyp;
	}
	public void setFilTyp(java.lang.String filTyp) {
		this.filTyp = filTyp;
	}
	public java.lang.String getFilSiz() {
		return filSiz;
	}
	public void setFilSiz(java.lang.String filSiz) {
		this.filSiz = filSiz;
	}
	public java.lang.String getFilRst() {
		return filRst;
	}
	public void setFilRst(java.lang.String filRst) {
		this.filRst = filRst;
	}
	public java.lang.String getFilExp() {
		return filExp;
	}
	public void setFilExp(java.lang.String filExp) {
		this.filExp = filExp;
	}
	public java.lang.String getCurTfs() {
		return curTfs;
	}
	public void setCurTfs(java.lang.String curTfs) {
		this.curTfs = curTfs;
	}
	public java.lang.String getCreYmd() {
		return creYmd;
	}
	public void setCreYmd(java.lang.String creYmd) {
		this.creYmd = creYmd;
	}
	public java.lang.String getCreUsr() {
		return creUsr;
	}
	public void setCreUsr(java.lang.String creUsr) {
		this.creUsr = creUsr;
	}
	public java.lang.String getUpdYmd() {
		return updYmd;
	}
	public void setUpdYmd(java.lang.String updYmd) {
		this.updYmd = updYmd;
	}
	public java.lang.String getUpdUsr() {
		return updUsr;
	}
	public void setUpdUsr(java.lang.String updUsr) {
		this.updUsr = updUsr;
	}
	public java.lang.String getId() {
		return id;
	}
	public void setId(java.lang.String id) {
		this.id = id;
	}
    
    
    
    
    
}
