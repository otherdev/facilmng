package com.gti.pms.map.dao;

import org.springframework.stereotype.Repository;

import com.gti.pms.addr.vo.UpdateVo;

import egovframework.rte.psl.dataaccess.EgovAbstractDAO;

/**
 * @Class Name : MapDAO.java
 * @Description : Map DAO Class
 * @Modification Information
 *
 * @author DRCTS
 * @since 2018-07-01
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Repository("MapDAO")
public class MapDAO extends EgovAbstractDAO {




	public int updateGeom(UpdateVo updateVO) {
		return update("MapDAO.updateGeom", updateVO);
	}

	public void insertGeom(UpdateVo updateVO) {
		insert("MapDAO.insertGeom", updateVO);
	}

	public int deleteGeom(UpdateVo updateVO) {
		return delete("MapDAO.deleteGeom", updateVO);
	}

}
