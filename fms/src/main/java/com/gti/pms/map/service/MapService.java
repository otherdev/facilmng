package com.gti.pms.map.service;

import com.gti.pms.addr.vo.UpdateVo;

/**
 * @Class Name : MapService.java
 * @Description : Map Business class
 * @Modification Information
 *
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */
public interface MapService {
	

	int updateGeom(UpdateVo updateVO);

	void insertGeom(UpdateVo updateVO);

	int deleteGeom(UpdateVo updateVO);

}
