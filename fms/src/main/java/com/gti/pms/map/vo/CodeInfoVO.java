package com.gti.pms.map.vo;

import java.io.Serializable;

public class CodeInfoVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6677392357195025226L;
	
	// 형식코드
	private String mofCde;
	
	// 관리기관코드
	private String mngCde;
	
	// 지형지물부호
	private String ftrCde;
	
	// 지형지물부호
	private String pipCde;
	
	// 코드명
	private String codeNm;
	
	// 코드
	private String code;
	
	// 코드 타입명
	private String typeNm;
	
	// 코드 타입
	private String typeCde;
	
	// 행정동 코드
	private String hjdCde;
	
	// 행정동 명
	private String hjdNm;
	
	/*Getter & Setter*/
	
	public String getMofCde() {
		return mofCde;
	}

	public void setMofCde(String mofCde) {
		this.mofCde = mofCde;
	}

	public String getMngCde() {
		return mngCde;
	}

	public void setMngCde(String mngCde) {
		this.mngCde = mngCde;
	}

	public String getFtrCde() {
		return ftrCde;
	}

	public void setFtrCde(String ftrCde) {
		this.ftrCde = ftrCde;
	}

	public String getPipCde() {
		return pipCde;
	}

	public void setPipCde(String pipCde) {
		this.pipCde = pipCde;
	}

	public String getCodeNm() {
		return codeNm;
	}

	public void setCodeNm(String codeNm) {
		this.codeNm = codeNm;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getTypeNm() {
		return typeNm;
	}

	public void setTypeNm(String typeNm) {
		this.typeNm = typeNm;
	}

	public String getTypeCde() {
		return typeCde;
	}

	public void setTypeCde(String typeCde) {
		this.typeCde = typeCde;
	}

	public String getHjdCde() {
		return hjdCde;
	}

	public void setHjdCde(String hjdCde) {
		this.hjdCde = hjdCde;
	}

	public String getHjdNm() {
		return hjdNm;
	}

	public void setHjdNm(String hjdNm) {
		this.hjdNm = hjdNm;
	}
	
}
