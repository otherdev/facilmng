package com.gti.pms.map.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.gti.pms.addr.vo.UpdateVo;
import com.gti.pms.map.dao.MapDAO;
import com.gti.pms.map.service.MapService;

import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;

@Service("mapService")
public class MapServiceImpl extends EgovAbstractServiceImpl implements MapService {
        

    @Resource(name="MapDAO")
    private MapDAO MapDAO;
    
    /** ID Generation */
    //@Resource(name="{egovMapIdGnrService}")    
    //private EgovIdGnrService egovIdGnrService;


	@Override
	public int updateGeom(UpdateVo updateVO) {
		return MapDAO.updateGeom(updateVO);
	}

	@Override
	public void insertGeom(UpdateVo updateVO) {
		MapDAO.insertGeom(updateVO);
	}

	@Override
	public int deleteGeom(UpdateVo updateVO) {
		return MapDAO.deleteGeom(updateVO);
	}
}
