package com.gti.pms.map.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gti.pms.addr.service.AddrService;
import com.gti.pms.addr.vo.AddrVo;
import com.gti.pms.addr.vo.UpdateVo;
import com.gti.pms.cmm.service.CmmService;
import com.gti.pms.map.service.MapService;
import com.gti.pms.map.vo.CodeInfoVO;
import com.gti.pms.util.JsonModel;

import egovframework.rte.fdl.property.EgovPropertyService;


@Controller
//@SessionAttributes(types=AddrVo.class)
public class MapController {

	@Resource(name = "addrService")
	private AddrService addrService;
	
	@Resource(name = "mapService")
	private MapService mapService;
	
	@Resource(name = "cmmService")
	private CmmService cmmService;
	
	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;
	
	
	/**
	 * 지도메인화면
	 * @param searchVO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/cons/main.do")
	public String indexMap(@ModelAttribute("searchVO") AddrVo searchVO, ModelMap model) throws Exception {
		
		model.addAttribute("geoServerUrl", propertiesService.getString("map.geoServerUrl"));
		model.addAttribute("eMapServerUrl", propertiesService.getString("map.eMapServerUrl"));
		
		return "main/main";
	} 
	
	@RequestMapping(value="/printMap.do", method = RequestMethod.GET)
	public String printMap(HttpServletRequest request, 
			@RequestParam(value="layerList", required=false) String layerList, 
			@RequestParam(value="bbox", required=false) String bbox, ModelMap model) throws Exception {
		//System.out.println("layerList : " +  layerList + ", bbox : " + bbox);

		model.addAttribute("geoServerUrl", propertiesService.getString("map.geoServerUrl"));
		model.addAttribute("eMapServerUrl", propertiesService.getString("map.eMapServerUrl"));
		
		model.addAttribute("layerList", layerList);
		model.addAttribute("bbox", bbox);
		
		return "main/printMap";
	}
	
	
	
	
	@ResponseBody
	@RequestMapping(value="/map/getCodeNm.do", method = RequestMethod.POST)
	public JsonModel getCodeNm(HttpServletRequest request, @ModelAttribute("codeInfoVo") CodeInfoVO codeInfoVo, JsonModel model) throws Exception {
		Map<String, Object> param = new HashMap<String, Object>();
		
		param.put("dtlCd", codeInfoVo.getMngCde());
		List<?> getMngNm = cmmService.selectList("selectCmmCd",param); // 관리기관
		param.put("dtlCd", codeInfoVo.getMofCde());
		List<?> getMofNm = cmmService.selectList("selectCmmCd",param); ; // 형식
		param.put("dtlCd", codeInfoVo.getFtrCde());
		List<?> getFtrNm = cmmService.selectList("selectCmmCd",param); // 지형지물
		param.put("dtlCd", codeInfoVo.getPipCde());
		List<?> getPipNm = cmmService.selectList("selectCmmCd",param); // 관로지형지물
		
		param.put("hjdCde", codeInfoVo.getHjdCde());
		List<?> getHjdNm = cmmService.selectList("selectHjdList",codeInfoVo.getHjdCde()); // 행정동
		
		model.putData("mngNm", getMngNm);
		model.putData("mofNm", getMofNm);
		model.putData("ftrNm", getFtrNm);
		model.putData("pipNm", getPipNm);
		model.putData("hjdNm", getHjdNm);
		
		return model;
	}
	
	@ResponseBody
	@RequestMapping(value="/map/updateGeom.do", method = RequestMethod.POST)
	public JsonModel updateGeom(HttpServletRequest request, @ModelAttribute("UpdateVo") UpdateVo updateVO, JsonModel model) throws Exception {
		
		String layerNm = updateVO.getLayerNm();
		String geom = "";
		if (layerNm.contains("_PS")) {
			geom = "MDSYS.SDO_GEOMETRY(2001, 5187, MDSYS.SDO_POINT_TYPE(" + updateVO.getCoordinates() + ", NULL), NULL, NULL)";
		} else if (layerNm.contains("_AS")) {
			geom = "MDSYS.SDO_GEOMETRY(2003, 5187, NULL, MDSYS.SDO_ELEM_INFO_ARRAY(1, 1003, 1), MDSYS.SDO_ORDINATE_ARRAY(" + updateVO.getCoordinates() + "))";
		} else if (layerNm.contains("_LM") || layerNm.contains("_LS")) {
			geom = "MDSYS.SDO_GEOMETRY(2002, 5187, NULL, MDSYS.SDO_ELEM_INFO_ARRAY(1, 2, 1), MDSYS.SDO_ORDINATE_ARRAY(" + updateVO.getCoordinates() + "))";
		}
		updateVO.setCoordinates(geom);
		updateVO.setLayerNm(updateVO.getLayerNm().replaceAll("_Edit", ""));
		
		int result = 0;
		if (updateVO.getInsertYn().equals("U")) {
			result = mapService.updateGeom(updateVO);
		} else if (updateVO.getInsertYn().equals("D")) {
			result = mapService.deleteGeom(updateVO);
		} else {
			// 대장을 등록한 후이기 때문에 Insert는 없을것같음.
			mapService.insertGeom(updateVO);
		}

		model.putData("result", result);
		
		return model;
	} 
	
}
