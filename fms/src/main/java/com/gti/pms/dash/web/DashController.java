package com.gti.pms.dash.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gti.pms.cmm.service.CmmService;
import com.gti.pms.cmm.util.JsonUtils;
import com.gti.pms.cmm.web.CodeController;

import egovframework.com.cmm.SessionUtil;
import egovframework.rte.fdl.property.EgovPropertyService;

@Controller
@RequestMapping("/pms/dash")
public class DashController extends CodeController {
	
	private static final Logger logger = LoggerFactory.getLogger(DashController.class);

    @Resource(name = "cmmService")
    private CmmService cmmService;
    
    @Resource(name = "jsonUtils")
	private JsonUtils jsonUtils;
    
	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;
	/**
	 * 대시보드 기본페이지
	 * @param path
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/index.do")	
	public String index(HttpServletRequest request, Model model) throws Exception {		
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", SessionUtil.getId());
		List<Map<String, Object>> mnuList = (List<Map<String, Object>>) cmmService.selectList("selectDashMenuList", map);
		
		model.addAttribute("mnuList", mnuList);
		
		return "pms/dash/index";		
	}
	
	
}
