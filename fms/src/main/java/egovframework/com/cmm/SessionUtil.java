package egovframework.com.cmm;

import java.io.Serializable;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

/**
 * 세션 저장 클래스
 *  
 * <pre>
 * << 개정이력(Modification Information) >>
 * 
 *   수정일      수정자          수정내용
 *  -------    --------    ---------------------------
 *  
 *  </pre>
 */
public class SessionUtil implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4075967420077007205L;
	
	
	public String id;
	public String name;
	public String deptCd;
	public String ofcCd;
	public String posCd;
	public String authCd;
	public String isAdmYn;
	
	


	public static String getId() {
		return ((LoginVO)EgovUserDetailsHelper.getAuthenticatedUser()).getUserId();
	}
	public static String getName() {
		return ((LoginVO)EgovUserDetailsHelper.getAuthenticatedUser()).getUserNm();
	}
	public static String getDeptCd() {
		return ((LoginVO)EgovUserDetailsHelper.getAuthenticatedUser()).getDeptCd();
	}
	public static String getOfcCd() {
		return ((LoginVO)EgovUserDetailsHelper.getAuthenticatedUser()).getOfcCd();
	}
	public static String getPosCd() {
		return ((LoginVO)EgovUserDetailsHelper.getAuthenticatedUser()).getPosCd();
	}
	public static String getAuthCd() {
		return ((LoginVO)EgovUserDetailsHelper.getAuthenticatedUser()).getAuthCd();
	}
	public static String getIsAdmYn() {
		String _authCd = ((LoginVO)EgovUserDetailsHelper.getAuthenticatedUser()).getAuthCd();
		return "ROLE_ADMIN".equals(_authCd) ? "Y" : "N";
	}

}
