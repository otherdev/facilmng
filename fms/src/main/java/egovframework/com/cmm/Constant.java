package egovframework.com.cmm;

public class Constant {

	public final static String PRJ_PATH = "waterpipe";
	
	// 상수대장관리
	public final static String RSRV_PATH = PRJ_PATH + "/rsrv";			/** 저수조 		*/
	public final static String CONS_PATH = PRJ_PATH + "/cons";  		/** 상수공사대장 	*/
	
	// 상수관망관리
	public final static String PIPE_PATH = PRJ_PATH + "/pipe";  		/** 상수관로 		*/
	
	public final static String ATTA_PATH = PRJ_PATH + "/atta";			/**	상수부속시설관리 */
	
	// 상수부속시설관리
	public final static String HEAD_PATH = PRJ_PATH + "/head";			/** 수원지		*/
	public final static String WUTL_PATH = PRJ_PATH + "/wutl";			/** 유지보수대장	*/
	
	// 수용가시설관리
	public final static String SPLY_PATH = PRJ_PATH + "/sply";			/** 급수관로 		*/
	
	
	public final static String CHSC_PATH = PRJ_PATH + "/chsc";			/** 점검일정관리 	*/
	public final static String UPTM_PATH = PRJ_PATH + "/uptm";			/** 시설물가동시간관리*/

}
