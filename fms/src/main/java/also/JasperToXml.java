package also;

import java.io.File;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlWriter;

class JasperToXml {

    public static void main(String[] args) {
    	//String sourcePath = "D:/ulsdev/workspace/facilmng/fms/src/main/webapp/WEB-INF/report/cnstMngDtl.jasper";
    	File sourcePath = new File("D:/ulsdev/workspace/facilmng/fms/src/main/webapp/WEB-INF/report/cnstMngDtl.jasper");
    	String destinationPath = "D:/ulsdev/workspace/facilmng/fms/src/main/webapp/WEB-INF/report/cnstMngDtl.jrxml";

        try {
			JasperReport report = (JasperReport) JRLoader.loadObject(sourcePath);
			JRXmlWriter.writeReport(report, destinationPath, "UTF-8");
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}        
        
    }
}