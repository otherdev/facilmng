package also;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class CoordConversion {
	public String sgisApiCoordConversion(String requestXml) {
		final String apiurl = "http://sgis1.kostat.go.kr/SGisService/coordconversion";
		StringBuffer responseXml = new StringBuffer();

		try {
			URL url = new URL(apiurl);
			URLConnection conn = url.openConnection();
			conn.setDoOutput(true);

			OutputStreamWriter owr = new OutputStreamWriter(conn.getOutputStream());
			owr.write(requestXml);
			owr.flush();

			InputStream is = conn.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(is));

			String line;
			while ((line = br.readLine()) != null) {
				responseXml.append(line);
			}

		} catch (MalformedURLException me) {
			//2015-12-03 시큐어코딩
			//e.printStackTrace();
			System.out.println("서버에서 처리중 에러가 발생했습니다.:"+me);

		} catch (IOException ioe) {
			//2015-12-03 시큐어코딩
			//e.printStackTrace();
			System.out.println("서버에서 처리중 에러가 발생했습니다.:"+ioe);

		}
		return responseXml.toString();
	}
	
	public static void main(String[] args) throws Exception {
	CoordConversion api = new CoordConversion();
		
	//요청문서 작성 시작
	String reqXml = "<?xml version='1.0' encoding='UTF-8'?>";
		 reqXml +="<SgisCoordConversionRequest xmlns='http://service.gis.knso.org/sgistype'";
		 reqXml += " xmlns:xls='http://www.opengis.net/xls' xmlns:gml='http://www.opengis.net/gml'>";
	 reqXml +=  "<svcKey>api발급키</svcKey>";
	 reqXml +=  "<CoordConversionRequest>";
	 reqXml +=  "<CoordConvserionInfo fromSrs='LL_W' toSrs='TM_W'/>";
	 reqXml +=  "<gml:pos>236710 315220</gml:pos>";
	 reqXml +=  "<gml:pos>236890 315700</gml:pos>";
	 reqXml +=  "<gml:pos>236000 315900</gml:pos>";
	 reqXml +=  "<gml:pos>235250 315218</gml:pos>";
	 reqXml +=  "<gml:pos>235050 315018</gml:pos>";
	 reqXml +=  "<gml:pos>236710 315220</gml:pos>";
	 reqXml +=   "</CoordConversionRequest>";
	 reqXml +=   "</SgisCoordConversionRequest>";
                    //요청문서 작성 끝			
		System.out.println(reqXml);
		
		String response = api.sgisApiCoordConversion(reqXml);
		System.out.println(response);
	}

} 